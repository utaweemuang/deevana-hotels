<?php
$title = 'Phuket Attractions | Deevana Hotels & Resorts';
$desc = 'Phuket is Thailand largest island and one of the most popular tourist destinations in Southeast Asia. This island is rich in natural resources and has the perfect weather for agriculture while providing colorful tropical vistas.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'attraction phuket';
$cur_page = 'attraction-phuket';

$lang_en = '/attraction-phuket.php';
$lang_th = '/th/attraction-phuket.php';
$lang_zh = '/zh/attraction-phuket.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/attraction/phuket/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
	
		<section class="main-content">
			<div class="container">

				<h1 class="section-title underline">普吉島景點</h1>
				
				<div class="row row-content">
					<div class="col-w3 col-navigation">
						<div class="post-tabs tabs-group">
							<ul>
								<li class="tab active" data-tab="#big_buddha_phuket">天坛大佛</li>
								<li class="tab" data-tab="#old_phuket_town">舊普吉島鎮</li>
								<li class="tab" data-tab="#sino-portuguese_houses">中國葡萄牙語房屋</li>
								<li class="tab" data-tab="#phang_nga_road">攀牙路</li>
								<li class="tab" data-tab="#thalang_road_and_soi_rommanee">Thalang 路</li>
								<li class="tab" data-tab="#dibuk_road">Dibuk 路</li>
								<li class="tab" data-tab="#krabi_road">Krabi 路</li>
								<li class="tab" data-tab="#ranong_road">Ranong 路</li>
							</ul>
						</div>
					</div>
					
					<div class="col-w9 col-content tabs-content">
						<article class="article" id="big_buddha_phuket" data-tab-name="Big Buddha Phuket">
							<img class="thumbnail force block" src="images/attraction/phuket/big_buddha_phuket.png" />
							<h1>天坛大佛</h1>
							<p>普吉岛的大佛像是岛上最重要的和最令人尊敬的地标之一。这个巨大的建筑坐落在Nakkerd山上，这个山chalong和kata中间，有45米高，从很远的地方就可以看见它。</p>
							<p>这个高耸的地方可以让游客全方位360度来欣赏岛上的风光，可以看到普吉岛镇，karon海滩，chalong湾还有其他更多的风景。通过普吉岛六千米的主干道，我们就可以到达这里，这里是岛上必游景点之一。</p>
							<p>这里十分庄严肃穆，十分安静，在悠扬的佛教音乐中，你只能听到一些小铃铛的叮当声和黄色佛旗在风中飘扬的声音。</p>
							<p>the Phra Puttamingmongkol Akenakkiri Buddha泰国人众所周知，它有25米高，整个身体都由精致的缅甸大理石打造而成，所以能在太阳光下闪闪发光，意味着希望。这种景象，令人心动。</p>
						</article>
						
						<article class="article" id="old_phuket_town" data-tab-name="Old Phuket Town">
							<img class="thumbnail force block" src="images/attraction/phuket/old_phuket_town.png" />
							<h1>舊普吉島鎮</h1>
							<p>不像泰国其他省会城市，普吉镇闪烁着它独特的光芒，其中普吉古镇最为独特。在历史最为悠久的地区，你会发现圣地、庙（佛教）、装饰华丽的商店、古雅的咖啡店、迷你打印店、私人或公共的博物馆，甚至是以前的红灯区。</p>
							<p>普吉古镇建于上个世纪，那个时候金属锡被大规模发现，而且金属还是特别珍贵的商品。在四分之一的古镇中，你会看见很多宏伟的中式建筑，这些建筑都是100年前的普吉岛工业大鳄们的产业。普吉岛古镇布局足够紧凑，游人可以绕着古镇闲逛。最佳游览时间是不太热的早晨和傍晚。这里有足够多的餐厅和咖啡店，可以提供各种点心，所以游客们不用费心自己准备野餐了。</p>
						</article>
						
						<article class="article" id="sino-portuguese_houses" data-tab-name="Sino-Portuguese Houses">
							<img class="thumbnail force block" src="images/attraction/phuket/sino-portuguese_house.png" />
							<h1>中國葡萄牙語房屋</h1>
							<p>在普吉岛必须要体验的一件事就是去普吉岛城的古城区漫步，去他朗路、迪克路和甲米路闲逛。在这些路边的漂亮的建筑会带你重回一个世纪以前，带你领略当年的魅力风光。</p>
							<p>如果你住在这里，你会体验到普吉岛精彩的民族混合风光。这里有泰国人、马来西亚人、印度人、尼泊尔人，这里是一个年轻的、正在不断成长的欧亚社区，而且，这里还有独特的中国福建混合体，泰国人称为“Baba”。</p>
							<p>这“Baba”社区在普吉岛古镇的遗产，大多数是一些建筑、贸易产业、服饰和生活方式。这个古镇的核心是由五条主路组成的，还有一些小路。这些主路是瑞安萨达路、攀牙路、他朗路、迪克路和甲米路。这些地方都是历史遗迹，虽然多年未曾管理但是现在大家正在努力修复。</p>
							<p>一百年前，他朗路是一个很繁荣的集市，因为从事锡和煤炭工业的工人会前往此地购买生活必需品，贩卖锡和矿石，他们沉迷于这种并不是很典型的活动。他们过着艰难的生活，而且沉迷于酒精、鸦片、女人，还有机会通过赌博来获得额外收入，这与他们从事锡行业的美好前景形成鲜明的对比。</p>
						</article>
						
						<article class="article" id="phang_nga_road" data-tab-name="Phang Nga Road">
							<img class="thumbnail force block" src="images/attraction/phuket/phang_nga_road.png" />
							<h1>攀牙路</h1>
							<p>从yaowarat road 向右转，再向右转，就可以进入phang nga路。在你的左侧你可以在写着汉字的胡同的入口处，看见南风二手书店。</p>
							<p>这条胡同通往定光堂。定光堂建于1889年，这个花园确实是一个安静祥和的地方，它可以在你探索五颜六色的内部之前，先让你放松你的双腿。在这花园的左边，是一块巨大的大理石装饰板，上面写着帮助建造庙宇的捐赠者的名字以及捐赠数目。在胡同出口处的斜对面，你会看见一个具有想象力的创意饭店，</p>
							<p>沿原路返回至yaowarat路，然后向thalang路走，在它们的交汇处有一家中国露天花园式餐厅，卖炖甜猪肉。即使在整个街区，这道菜都是非常流行的，是畅销食品。</p>
							<p>即使路上没有顾客，店里的老板伙计们都会自娱自乐，开心地大笑，讲各种笑话。在离学校放学还有10分钟的时候，他们就会非常忙碌了。</p>
							<p>我们继续往前走，向右转就会进入krabi路。再往右走50米就会看见一个古老的二手书店，在书店后面还有一个泰国华馆。那里曾经是一个中文学校，现在已经变成了一个博物馆和一个可供展览的地方。这漂亮的建筑有它自己的后花园。在一些店房的旁边，一间完整的商店掩映在一些绿色的椰子树下。</p>
							<p>在水果店你可以买西瓜和其他各种水果。在镇招待所在水果店的旁边，你会觉得，这是一个非常有魅力的地方。</p>
						</article>
						
						<article class="article" id="thalang_road_and_soi_rommanee" data-tab-name="Thalang Road and Soi Rommanee">
							<img class="thumbnail force block" src="images/attraction/phuket/thalang_road_and_soi_rommanee.png" />
							<h1>Thalang 路</h1>
							<p>在这个街区的尽头向左转，就可以进入普吉路，经过对面看起来像破旧的房屋，这些房屋实际上是一座名声不太好的酒店。然后我们就可以向左转，进入thalang路，然后向东。现在，你就处于普吉古镇的中心，那个历史悠久、很有气氛的热闹街道。这里，拱门的世界开始了。这里被称为“五人行道”，它们大多数相互连接，提供了一个游人可以躲避太阳和大雨的漫步休闲场所，但是一些地方已经不再畅通，而且商品店铺比较杂乱。而且，色彩缤纷的设计和商业理念的混合，让这个地方有着令人印象深刻的景象。</p>
							<p>在这个街道的尽头，有两家烤肉店，一家中医诊所，一家白色T恤专卖店，一家自行车店和纺织出口店。继续往左前方走，就进入Soi Rommanee。这条后街有一个很有意思的过去，这里曾经是红灯区和娱乐区，很多中国工人会来这里娱乐。实际上，这个单词“romanee”大致被翻译成“与女人一起玩闹”。现如今，如果一个地方，很多房子和咖啡店被编码，街上全是字符的话，那么soi就是这样的例子。（二十世纪六十年代福特领事馆欢迎大家联系）</p>
						</article>
						
						<article class="article" id="dibuk_road" data-tab-name="Dibuk Road">
							<img class="thumbnail force block" src="images/attraction/phuket/dibuk_road.png" />
							<h1>Dibuk 路</h1>
							<p>向右转离开thalang路，穿过马路向左转，就到了dibuk路。这条路有很多翻新过的闪闪发光的中式建筑，而且还有一条很宽的路，可以容纳双向车道，不像那些古镇的狭窄的路，只有一个车道。这些相对来说宽敞的街道，让拍更多更好的照片成为可能。在dibuk路的尽头，你会来到与satun路的连接处。在你的斜对面是pheteow面条店。这个点每个星期吃午饭的时候总是很拥挤，因为许多办公室职员会来到这里吃饭，因为这里的饭很好吃。</p>
						</article>
						
						<article class="article" id="krabi_road" data-tab-name="Krabi Road">
							<img class="thumbnail force block" src="images/attraction/phuket/krabi_road.png" />
							<h1>Krabi 路</h1>
							<p>沿原路返回至yaowarat路，然后向thalang路走，在它们的交汇处有一家中国露天花园式餐厅，卖炖甜猪肉。即使在整个街区，这道菜都是非常流行的，是畅销食品。</p>
							<p>即使路上没有顾客，店里的老板伙计们都会自娱自乐，开心地大笑，讲各种笑话。在离学校放学还有10分钟的时候，他们就会非常忙碌了。
我们继续往前走，向右转就会进入krabi路。再往右走50米就会看见一个古老的二手书店，在书店后面还有一个泰国华馆。那里曾经是一个中文学校，现在已经变成了一个博物馆和一个可供展览的地方。这漂亮的建筑有它自己的后花园。在一些店房的旁边，一间完整的商店掩映在一些绿色的椰子树下。
在水果店你可以买西瓜和其他各种水果。在镇招待所在水果店的旁边，你会觉得，这是一个非常有魅力的地方。
</p>
						</article>
						
						<article class="article" id="phuket_market" data-tab-name="Phuket Market">
							<img class="thumbnail force block" src="images/attraction/phuket/phuket_market.png" />
							<h1>普吉島市場</h1>
							<p>沿原路返回至yaowarat路，然后向thalang路走，在它们的交汇处有一家中国露天花园式餐厅，卖炖甜猪肉。即使在整个街区，这道菜都是非常流行的，是畅销食品。</p>
							<p>即使路上没有顾客，店里的老板伙计们都会自娱自乐，开心地大笑，讲各种笑话。在离学校放学还有10分钟的时候，他们就会非常忙碌了。</p>
							<p>我们继续往前走，向右转就会进入krabi路。再往右走50米就会看见一个古老的二手书店，在书店后面还有一个泰国华馆。那里曾经是一个中文学校，现在已经变成了一个博物馆和一个可供展览的地方。这漂亮的建筑有它自己的后花园。在一些店房的旁边，一间完整的商店掩映在一些绿色的椰子树下。</p>
							<p>在水果店你可以买西瓜和其他各种水果。在镇招待所在水果店的旁边，你会觉得，这是一个非常有魅力的地方。</p>
						</article>
						
						<article class="article" id="ranong_road" data-tab-name="Ranong Road">
							<img class="thumbnail force block" src="images/attraction/phuket/ranong_road.png?ver=20160517" />
							<h1>Ranong 路</h1>
							<p>在soi的尽头向右转是ranong 路。这里，你会发现一个色彩缤纷的中国圣地，叫Jui Tui。这个圣地供奉的是中国道教的神灵，这里也是普吉岛年度素斋节的活动中心。</p>
							<p>这里也是供人们获得神旨的地方。问一个答案是或否的问题，然后把两个竹块扔向空中。如果两个竹块都是朝上的面相同，那么答案是“否”。如果两个竹块朝上的面相反，那么答案是肯定的。捐一些香火钱是十分受欢迎的。</p>
							<p>在jui tui圣地旁边是Pud Jow 中国的道教庙宇。它于200年前建造，100年前遭大火后翻新，这个景点是普吉岛上最古老的地方。</p>
							<p>现在，可以直接回到来时的地点，然后走过rannong路的尽头，你会发现你自己回到了rasada路。接着走到尽头，你会发现，你已经回到了原点。</p>
						</article>
					</div>
				</div>
				
			</div>
		</section><!--.main-content-->
		
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
            
            var targetPos = $('.site-main').offset().top;
			var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    @media (max-width: 768px) {
        .row-content [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin: 0 -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>