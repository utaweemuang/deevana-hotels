<?php
$title = 'Our Brands of Deevana Hotels & Resorts';
$desc = 'Deevana Hotels & Resorts – The best Thailand’s local hotel chain. Provide comfortable accommodation in Phuket and Krabi';
$keyw = 'Deevana Plaza Krabi, Deevana Plaza Phuket, Deevana Patong Resort & Spa, Deevana Krabi Resort, Ramada Phuket Deevana, Recenta Suite, Recenta Phuket, Recenta Style';

$html_class = '';
$body_class = 'offers';
$cur_page = 'our-brands';

$lang_en = '/our-brands.php';
$lang_th = '/th/our-brands.php';
$lang_zh = '/zh/our-brands.php';

include_once('_header.php');
?>
        
<div id="ourbrands_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/our_brands/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <div class="main-content">
            <div class="container">
				
               	<div class="row row-header">
               		<div class="col-w3">&nbsp;</div>
					<div class="col-w9">
						<h1 class="section-title underline">
							<span style="color: #9A7B12;">我們的品牌Deevana酒店和度假村</span>
						</h1>
					</div>
				</div>

                <div class="row row-content">
                    <div class="col-w3 col-navigation">
                        <div class="post-tabs tabs-group">
                            <ul>
                                <li class="tab active" data-tab="#landing" style="display: none;">降落</li>
                                <li class="tab logo-tab deevana-plaza" data-tab="#deevana_plaza">普吉岛芭东蒂瓦纳广场酒店</li>
                                <li class="tab logo-tab deevana" data-tab="#deevana">芭东蒂瓦纳度假水疗中心</li>
                                <li class="tab logo-tab recenta-suite" data-tab="#recenta_suite">普吉岛双廊瑞森塔套房酒店</li>
                                <li class="tab logo-tab recenta" data-tab="#recenta">普吉岛双廊瑞森塔酒店</li>
                                <li class="tab logo-tab recenta-express" data-tab="#recenta_express">普吉镇瑞森塔快捷酒店</li>
                                <li class="tab logo-tab ramada" data-tab="#ramada_phuket_deevana">瑞玛达普吉岛蒂瓦纳酒店</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-w9 col-content tabs-content">
                        <article class="article hide-tab" id="landing">
							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-deevana-plaza.png" width="192" height="47" alt="Deevana Plaza"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-deevana_plaza.png" width="258" height="76" alt="Your choice for all occasions" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>不管是商务出差还是休闲娱乐，我们全方位满足您的需求。</p>
								</div>
							</div>

							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-deevana-hotels.png" width="192" height="64" alt="Deevana Hotels"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-deevana.png" width="186" height="16" alt="Reward Your Journey" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>从您到的那一刻起，感受deevana的精神，享受一个放松的，恢复活力的旅程。</p>
								</div>
							</div>

							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta-suite.png" width="192" height="71" alt="Recenta Suite"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>一个中级的房间提供一间卧室，一个客厅，提供了更大的空间，更多的选择和更舒适的环境。Reventa套房一直为顾客提供超出他们想象和满意程度的服务，为他们提供标准的酒店和度假村，提供更多更好的服务和设施。在高级的设施和房间的基本功能的基础上，我们的顾客还会是我们的家人，会享受到更多的私人空间。</p>
								</div>
							</div>
                           
                           	<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta.png" width="192" height="71" alt="Recenta"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>Recenta酒店是一个中档酒店，你在这里度过你的假期十分适合。顾客们将会十分舒适，并且物超所值。所有的房间都很大，而且设施齐全。Recenta酒店提供高质量服务，而且设施适合所有年龄段和所有性别，我们在此可以以品牌保证。</p>
								</div>
							</div>
                           
                           	<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta-express.png" width="192" height="71" alt="Recenta Style"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>Recenta Style是一个中档酒店，提供有限有效的服务。顾客们可以享受全新的现代紧密小巧的房间。酒店的宗旨是让所有顾客都可以享受安全、安静和舒适的房间。我们房间有五星级的舒适的床，有基础设施和一些特殊服务，比如说帮顾客提行李，打扫房间，电视，冰箱，空调和免费的Wi-Fi.</p>
								</div>
							</div>
                          
							<h2 class="section-title underline">
								<span style="color: #9A7B12;">International Brand</span>
							</h2>
                           
                           	<div class="row">
                           		<div class="col-w12 col-brand">
                           			<div class="table table-brand">
                           				<div class="table-cell cell-logo">
                           					<img class="logo" src="images/our_brands/brands-slogan/logo-ramada.png" width="192" height="94" alt="Ramada Phuket Deevana" />
										</div>
										<div class="table-cell cell-slogan">
											<img class="slogan" src="images/our_brands/brands-slogan/slogan-ramada.png" width="217" height="18" alt="leave the rest to us" />
										</div>
									</div>
								</div>
								
								<div class="col-w12 col-caption">
									<p>我们的现代度假村洋溢着泰式风情，坐落于巴东海滩的心脏，集有活力的城市和海滩终极地于一身。被顶级旅游景点所包围环绕，游客们可以在美丽的白沙滩步行五分钟，享受充满活力的夜生活，体验购物的巨大乐趣。</p>
								</div>
							</div>
                            
                            
<!--
                            <div class="row row-brands">
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/deevana.png" alt="Deevana" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/deevana_plaza.png" alt="Deevana Plaza" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/ramada.png" alt="Ramada" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta_suite.png" alt="Recenta Suite" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta.png" alt="Recenta" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta_express.png" alt="Recenta Style" />
                                </div>
                            </div>
-->
                        </article>
                        
                        <article class="article" id="deevana_plaza" data-tab-name="Deevana Plaza">
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_phuket_patong.png" />
                            <h1 class="with-logo-deevana-plaza-phuket">所有的完美的度假元素，在我们这里都有。</h1>
                            <p>坐落于patong城的中心，只要走几步路就可以到世界闻名的patong海滩、购物中心和夜市，体验繁华的夜生活。Deevana plaza Phuket patong给patong海滩带来了不一样的生活方式和光彩。</p>
                            <p>我们酒店自身就是一个亮点，我们有249个高雅的单间与套房，有健康水疗，顾客还可以普吉岛巴东咖啡馆体验传统泰式菜和国际美食，更能体验以天文为主题的休闲娱乐活动和会议设施。酒店对来普吉岛游玩的不同类型游客来说，都是一个非常棒的选择。</p>
                            <p><span style="color: #ab8205">普吉岛芭东蒂瓦纳广场酒店 将会提供顶级的服务，让每位游客都会喜欢泰式独特的欢迎风格。</span></p>
                            <hr>
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_krabi_aonang.png" />
                            <h1 class="with-logo-deevana-plaza-krabi">甲米奥南蒂瓦纳广场酒店</h1>
                            <p>甲米岛奥南区的Deevana广场酒店距离奥南海滩和诺帕拉塔拉海滩都很近，它是一个别致的当代风格低层建筑群。有213间不同风格的房间和套间,每一个房间都有私人阳台,客人可以享受酒店的良好的设施和Deevana广场酒店友好的和个性化的服务团队。度假村的特色有额外的餐厅和酒吧,三个大型户外游泳池、儿童池,休闲保健按摩中心，杰出的会议和活动设施。</p>
                        </article>

                        <!-- <article class="article" id="deevana_plaza_krabi" data-tab-name="Deevana Plaza">
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_krabi_aonang.png" />
                            <h1 class="with-logo-deevana-plaza-krabi">甲米奥南蒂瓦纳广场酒店</h1>
                            <p>甲米岛奥南区的Deevana广场酒店距离奥南海滩和诺帕拉塔拉海滩都很近，它是一个别致的当代风格低层建筑群。有213间不同风格的房间和套间,每一个房间都有私人阳台,客人可以享受酒店的良好的设施和Deevana广场酒店友好的和个性化的服务团队。度假村的特色有额外的餐厅和酒吧,三个大型户外游泳池、儿童池,休闲保健按摩中心，杰出的会议和活动设施。</p>
                        </article> -->

                        <article class="article" id="deevana" data-tab-name="Deevana">
                            <img class="thumbnail force block" src="images/our_brands/deevana_patong_resort_and_spa.png" />
                            <h1 class="with-logo-deevana-phuket">芭东蒂瓦纳度假水疗中心</h1>
                            <p>芭东蒂瓦纳度假水疗中心 拥有最好的233个房间。</p>
                            <p><span style="color: #ab8205;">坐落于普吉岛最受欢迎的目的地——所有旅游的中心巴东海滩，从我们度假村只需要走几分钟，便可以来到著名的巴东路。在这里，会体验到巴东的夜生活，也可以到Jungceylon购物和娱乐，当然，也不能少了巴东美妙的金色沙滩。当你进入到我们的deevana的大堂，就会感觉到平静与优雅。我们度假村拥有美妙绝伦的花园和高雅的单间与套房，带你走进舒适平静的天堂，让你体验最绝妙的假期。</span></p>
                            <hr>
                            <img class="thumbnail force block" src="images/our_brands/deevana_krabi_resort.png?ver=20160517" />
                            <h1 class="with-logo-deevana-krabi">甲米蒂瓦纳度假村</h1>
                            <p>我们最新建造的独特房间，坐落于生态良好的丛林间，四周绿树成荫，可以观赏到Aonang – Krabi的绝妙风景。</p>
                            <p><span style="color: #ab8205;">我们有66个独立的房间，你可以选择户外浴缸或是标准浴缸。我们也提供免费的Wi-Fi和国际性的餐厅，还有两个游泳池和酒吧。你可以按照自己的喜好，选择去sala按摩或者去健身房健身。我们去krabi的旅游景点十分方便，仅仅步行10分钟，就可以到美丽的Aonang海滩和Noppharathara海滩。 </span></p>
                        </article>

                        <!-- <article class="article" id="deevana_krabi" data-tab-name="Deevana">
                            <img class="thumbnail force block" src="images/our_brands/deevana_krabi_resort.png?ver=20160517" />
                            <h1 class="with-logo-deevana-krabi">甲米蒂瓦纳度假村</h1>
                            <p>我们最新建造的独特房间，坐落于生态良好的丛林间，四周绿树成荫，可以观赏到Aonang – Krabi的绝妙风景。</p>
                            <p><span style="color: #ab8205;">我们有66个独立的房间，你可以选择户外浴缸或是标准浴缸。我们也提供免费的Wi-Fi和国际性的餐厅，还有两个游泳池和酒吧。你可以按照自己的喜好，选择去sala按摩或者去健身房健身。我们去krabi的旅游景点十分方便，仅仅步行10分钟，就可以到美丽的Aonang海滩和Noppharathara海滩。 </span></p>
                        </article> -->

                        <article class="article" id="recenta_suite" data-tab-name="Recenta Suite">
                            <img class="thumbnail force block" src="images/our_brands/recenta_suite_phuket_suanluang.png" />
                            <h1 class="with-logo-recenta-suite">普吉岛苏恩卢恩瑞森塔套房酒店</h1>
                            <p>一个中级酒店提供带有一间卧室和一间客厅的客房,提供更大的空间和更舒适居住环境。</p>
                            <p>度假村的标准总是令顾客超乎预期的满意。包括很多令人印象深刻的服务和设施，客房采用优质的设备，为您和家人提供更多的私密空间，让您有一种宾至如归的感觉。</p>
                        </article>

                        <article class="article" id="recenta" data-tab-name="Recenta">
                            <img class="thumbnail force block" src="images/our_brands/recenta_phuket_suanluang.png?ver=20160517" />
                            <h1 class="with-logo-recenta-suanluang">欢迎来到普吉岛苏恩卢恩瑞森塔酒店</h1>
                            <p>瑞森塔中级酒店旨在为你提供最适合你的舒适的，能负担的起的，物超所值的所有的场合和假期。所有房间都很大，有精心设计好的功能,因此以品牌担保，瑞森塔可以为所有年龄和性别的人提供高质量的服务和设施。</p>
                        </article>

                        <article class="article" id="recenta_express" data-tab-name="Recenta Style">
                            <img class="thumbnail force block" src="images/our_brands/recenta_express_phuket_town.png" />
                            <h1 class="with-logo-recenta-express">欢迎来到普吉镇瑞森塔快捷酒店</h1>
                            <p>Recenta快捷酒店是一个能提供有限的服务的中级酒店。享受新的和现代紧凑型房间,Recenta快捷酒店旨在希望所有的客人可以享有安全、清洁、舒适的的体验，级舒适床包括五星级舒适度的床、所需的基本设施和难忘的服务，如乘客电梯、房间服务、电视、冰箱、空调、免费无线网络。</p>
                        </article>
                        
                        <article class="article" id="ramada_phuket_deevana" data-tab-name="Ramada Phuket Deevana">
                            <img class="thumbnail force block" src="images/our_brands/ramada_phuket_deevana.png" />
                            <h1 class="with-logo-ramada">华美达普吉岛蒂瓦娜酒店</h1>
                            <p>一个洋溢着泰国魅力的现代化的度假村位于巴东海滩的心脏，一个充满活力的城市，游客选择最多的海滩目的地。游客在五分钟内漫步美丽的白沙海滩，周围有众多著名的旅游景点，充满活力的夜生活区和巨大的购物和娱乐商场就在其中。</p>
                            <p>华美达普吉岛Deevana是一个宁静和热情好客的地方期待着你的光临，这里帮你远离城市的喧嚣。在这里象艺术品遍布度假村的每一个角落和客房装饰，看起来喜庆而又有品位。华美达普吉岛Deevana巴东是无烟酒店,专为休闲和快乐的家庭而规定。度假村还配备了热带阳光下的室外游泳池，内置按摩浴缸的水疗中心以及孩子们的乐园。</p>
                            <p>在这里每天都可以吃到各种国际流行美食，可以驻足贝福烘烤店享用美味可口的点心和饮料。会议室承接各种宴会和商业活动。</p>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        $cs.find(atd).show();
        
        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });
        
        $cs.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            if( tabName !== undefined ) {
                $this.before('<span class="accordion-tab">'+tabName+'</span>');
                $this.prev('.accordion-tab').on('click', function() {
                    var i = $(this).index('.accordion-tab');
                    $(this).addClass('active').siblings().removeClass('active');
                    $this.slideDown(300, function() {
                        var pos = $(this).offset().top;
                        var offset = 50;
                        $('html, body').animate({
                            scrollTop: pos - offset,
                        }, 800);
                    }).siblings().not('.accordion-tab').slideUp(300);
                    $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                });
            };
        });
        
        $('.accordion-tab').eq(ati).addClass('active');
        
        
		$(window).on('load', function() {
			//Go to Landing section
			var pos = $('.site-main').offset().top;
			var offset = ( $(window).width() > 960 ) ? 144 : 0 ;
			scrollTo(0, Math.round(pos - offset) );
		});
    });
</script>

<style>
	#landing > .row {
		margin-bottom: 50px;
		text-align: center;
	}
	#landing > .row:last-of-type {
		margin-bottom: 0;
	}
	
	.table-brand {
		width: 100%;
		text-align: center;
	}
	.table-brand img {
		max-width: 100%;
		height: auto;
		display: block;
		margin: 0 auto;
	}
	.table-brand .cell-logo,
	.table-brand .cell-slogan {
		vertical-align: middle;
		width: 50%;
		padding: 1em;
	}
	.table-brand .cell-logo {
		position: relative;
	}
	.table-brand .cell-logo:after {
		content: '';
		display: block;
		width: 1px;
		height: 60px;
		background-color: #666;
		position: absolute;
		right: 0;
		top: 50%;
		margin-top: -30px;
		margin-right: 0.5px;
	}
	
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    
    /* Logo in post tab */
    .post-tabs .logo-tab:before {
        content: '';
        background-size: contain;
        background-repeat: no-repeat;
        background-position: right top;
        width: 100%;
        height: 76%;
        display: block;
        position: absolute;
        top:12%;
        right: 15px;
        -webkit-filter: grayscale(1) opacity(0.75);
        filter: grayscale(1) opacity(0.75);
        -webkit-transition: 200ms;
        transition: 200ms;
    }
    .post-tabs .deevana:before { background-image: url(images/our_brands/brands/deevana.png); }
    .post-tabs .deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .post-tabs .deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .post-tabs .deevana-plaza:before { background-image: url(images/our_brands/brands/deevana_plaza.png); }
    .post-tabs .deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .post-tabs .deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .post-tabs .ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .post-tabs .recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite.png); }
    .post-tabs .recenta:before { background-image: url(images/our_brands/brands/recenta.png); }
    .post-tabs .recenta-express:before { background-image: url(images/our_brands/brands/recenta_express.png); }
    
    .post-tabs .logo-tab:hover:before,
    .post-tabs .logo-tab.active:before {
        -webkit-filter: none;
        filter: none;
    }
    
    /* Logo in title */
    [class*="with-logo-"] {
        position: relative;
        padding-right: 80px;
    }

    [class*="with-logo-"]:before {
        content: '';
        background-repeat: no-repeat;
        background-position: right top;
        background-size: contain;
        display: block;
        position: absolute;
        top: -10px;
        right: 0;
        top: 50%;
        width: 100%;
        height: 40px;
        margin-top: -20px;
        z-index: -1;
    }

    .with-logo-deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .with-logo-deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .with-logo-deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .with-logo-deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .with-logo-ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .with-logo-recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite_with_location.png); }
    .with-logo-recenta-suanluang:before { background-image: url(images/our_brands/brands/recenta_suanluang_with_location.png); }
    .with-logo-recenta-express:before { background-image: url(images/our_brands/brands/recenta_express_with_location.png); }
    
    .row-brands {
        text-align: center;
        margin-bottom: 30px;
    }
    .row-brands .logo {
        max-width: 100%;
        height: auto;
        display: inline-block;
        vertical-align: middle;
        margin-bottom: 10px;
    }
    
    #landing {
        min-height: 400px;
    }

    @media (max-width: 1024px) {
        .post-tabs .logo-tab:before {
            content: none;
        }
    }
    
    @media (max-width: 768px) {
		.row-header > [class*="col-"],
        .row-content > [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 20px;
        }
        .tabs-content .article.hide-tab {
            padding-top: 0;
        }
        
        .row-brands [class*="col-"] {
            width: 50%;
        }
    }
	
	@media (max-width: 480px) {
		#landing > .row {
			margin-bottom: 0;
			padding: 40px 0;
			position: relative;
		}
		#landing > .row:before {
			content: '';
			position: absolute;
			bottom: 0;
			left: 40px;
			right: 40px;
			height: 1px;
			background-color: #ccc;
		}
		#landing > .row:last-of-type:before {
			content: none;
		}
		.table-brand .cell-logo,
		.table-brand .cell-slogan {
			width: 100%;
			display: block;
		}

		.table-brand .cell-logo:after {
			width: 60px;
			height: 1px;
			top: auto;
			left: 50%;
			right: auto;
			bottom: 0;
			margin: -0.5px 0 0 -30px;
		}
	}
</style>

<?php include_once('_footer.php'); ?>