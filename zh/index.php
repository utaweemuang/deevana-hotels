<?php

session_start();

$title = 'Phuket & Krabi Resort | Deevana Hotels & Resorts provide comfortable accommodation in Phuket and Krabi. The ideal choice of professionalism quality service, completely amenities and luxury wellness spa.';
$desc = 'Deevana Hotels & Resorts provide comfortable accommodation in Phuket and Krabi. The ideal choice of professionalism quality service, completely amenities and luxury wellness spa.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/';
$lang_th = '/th';
$lang_zh = '/zh';

include_once('_header.php');
?>

<style>
.mfp-zoom-out-cur,
.mfp-zoom-out-cur
.mfp-image-holder
.mfp-close {
    cursor: pointer !important;
}
</style>

<div id="home_slider" class="slider home-slider owl-carousel">
    <div class="item col-phuket" data-bg="images/home/hero_phuket-3.jpg">
        <a href="attraction-phuket.php"><img class="book" src="images/home/book_phuket-cn.png" width="343" height="221" /></a>
    </div>

    <div class="item col-krabi" data-bg="images/home/hero_krabi-3.jpg">
        <a href="attraction-krabi.php"><img class="book" src="images/home/book_krabi-cn.png" width="343" height="221" /></a>
    </div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        <section id="offers" class="section oval-shad">
            <div class="container">
                <h1 class="section-title underline"><span style="color:#766b58;">特价</span></h1>

                <div id="offer_slider" class="owl-carousel slider has-nav">

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/deevanakrabiresort/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/deevanakrabiresort_homepromo.jpg" alt="Deevana Krabi Resort" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                甲米蒂瓦纳度假村<br/>
                                <span>從 THB 1,421 <b>開放率</b></span>
                            </p>
                        </figcaption>
                    </figure>


                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/deevanaplazakrabi/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/02_deevana_plaza_krabi_aonang.jpg" alt="Deevana Plaza Krabi Ao Nang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                甲米奥南蒂瓦纳广场酒店<br/>
                                <span>從 THB 1,320</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/deevanapatong/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/04_deevana_patong_resort_spa.jpg" alt="Deevana Patong Resort & Spa" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                芭东蒂瓦纳度假水疗中心<br/>
                                <span>從 THB 2,080</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/deevanaplazaphuket/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/01_deevana_plaza_phuket_patong.jpg" alt="Deevana Plaza Phuket Patong" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                普吉岛芭东蒂瓦纳广场酒店<br/>
                                <span>從 THB 1,303</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="http://www.deevanahotels.com/ramadaphuketdeevana/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/ramada-phuket-deevana.jpg" alt="Ramada Phuket Deevana" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p> 瑞玛达普吉岛蒂瓦纳酒店
                                <br/>
                                <span>從 THB 1,200</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/recentasuitephuket/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/recenta_suite.jpg" alt="Recenta Suite Phuket Suanluang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                普吉岛双廊瑞森塔套房酒店<br/>
                                <span>從 THB 990</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/recentaphuket/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/07_recenta_suanluang.jpg" alt="Recenta Phuket Suanluang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                普吉岛双廊瑞森塔酒店<br/>
                                <span>從 THB 720</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="/zh/recentastyle/" target="_blank"><img src="http://www.deevanahotels.com/images/home/slide_offers/08_recenta_express.jpg" alt="Recenta Style Phuket Town" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                普吉镇瑞森塔快捷酒店<br/>
                                <span>從 THB 720</span>
                            </p>
                        </figcaption>
                    </figure>
                </div><!--#offer_slider-->
            </div>
        </section>

        <section id="awards" class="section pattern-noise">

            <!-- <h2 class="title"><span>E-Calendar</span></h2>
            <a target="_blank" href="http://online.fliphtml5.com/wdroo/pqxe/"><img src="http://www.deevanahotels.com/images/home/calendar2020.jpg" alt="Deevana Hotels Calendar 2020" width="250" height="250"></a>
            <br><br>
            <h3 class="header">Download</h3><a href="download/2020-calendar_edit_page.pdf" target="_blank" rel="noopener">
            <div class="btn btn-secondary"><i class="fa fa-file" aria-hidden="true"></i> Calendar </div></a><br> -->
            
            <div class="container">
                <h1 class="title"><span>在所有的度假村中获得最多的荣誉</span></h1>
                <p>为了成为不断创新和有创造力的度假村，我们尽了自己最大的努力，而且我们做得很好。<br>因此一年年来，我们可以从消费者和旅行出版社那里得到了一些非常著名的奖。<br> </p>
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tceb-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/atta-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/SHA_Logo.png" alt="" width="128" height="128"></li>
                </ul> 
            </div>
        </section>

        <section id="moments" class="section">
            <div class="container">
                <h1 class="section-title underline"><span style="color: #0072bb;">時刻在</span>DEEVANA</h1>

                <div id="moment_slider" class="owl-carousel fx-scale slider has-nav">
                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/cooking_class.jpg" alt="Cooking Class" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">厨艺课：</h2>
                            <p class="excerpt">体验一次私人亲密的主厨厨艺课堂吧。我们期待您的加入！</p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/romantic_private_dinner.jpg" alt="Romantic Private Dinner" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">享受浪漫私人晚餐：</h2>
                            <p class="excerpt">在星空下，在烛光旁享受浪漫私人晚餐，让自己沉浸在浪漫的氛围中，给自己的爱人一个惊喜。我们会提供特别的服务。</p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/spa.jpg" alt="Spa" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">温泉：</h2>
                            <p class="excerpt">一次极好的旅行，让自己重新找到肉体和心灵的平衡。</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </section>

        <section id="attraction" class="section wing-shad">
            <div class="container">
                <h1 class="section-title underline">
                    <span class="deco-map"><span style="color:#809a00;">PHUKET &amp; KRABI</span> 景點</span>
                </h1>

                <div class="row row-attraction">
                    <div class="col-w6 phuket">
                        <h2>Phuket 景點</h2>

                        <div class="row row-phuket">
                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Old Phuket Town" class="thumbnail force" src="images/home/old_phuket_town.jpg" />
                                    <h3 class="title">老普都岛</h3>
                                    <a class="more" href="attraction-phuket.php#old_phuket_town">學到更多</a>
                                </div>
                            </div>

                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Big Buddha Phuket" class="thumbnail force" src="images/home/big_buddha_phuket.jpg" />
                                    <h3 class="title">天坛大佛</h3>
                                    <a class="more" href="attraction-phuket.php#big_buddha_phuket">學到更多</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-w6 krabi">
                        <h2>Krabi 景點</h2>

                        <div class="row row-krabi">
                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Emerald Pool" class="thumbnail force" src="images/home/emerald_pool.jpg" />
                                    <h3 class="title">翡翠池</h3>
                                    <a class="more" href="attraction-krabi.php#emerald_pool">學到更多</a>
                                </div>
                            </div>

                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Elephant Ride" class="thumbnail force" src="images/home/elephant_ride.jpg" />
                                    <h3 class="title">大象骑</h3>
                                    <a class="more" href="attraction-krabi.php#elephant_ride">學到更多</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php

if (!isset($_SESSION['visited'])) { ?>
<?php }

$_SESSION['visited'] = "true";
?>
<div class="header-div <?php $visited?'':'first-visit'?>" style="display: none;">...</div>

<script>
    $(function() {
        $('#home_slider').owlCarousel({
            dots: false,
            nav: false,
            autoplay: 5000,
            pullDrag: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, loop: true, },
                720: { items: 2, loop: false, },
            },
        });

        $('#offer_slider').owlCarousel({
            items: 4,
            smartSpeed: 300,
            loop: true,
            dots: false,
            nav: true,
            navText: ['<span class="sprite-arrow-left"></span>', '<span class="sprite-arrow-right"></span>'],
            responsiveRefreshRate: 200,
            responsive: {
                0: { autoWidth: true, autoplay: 5000 },
                641: { items: 3, },
                720: { items: 4 },
            },
        });

        $('#moment_slider').owlCarousel({
            items: 3,
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            dots: false,
            navText: ['<span class="sprite-arrow-left"></span>', '<span class="sprite-arrow-right"></span>'],
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });
    });
</script>

<style>
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .site-main > .inner {
        padding: 0;
    }
    .section {
        padding: 50px 0;
    }
    .slider .item {
        margin: 0;
        padding: 0 10px;
    }
    #offers.oval-shad {
        display: block;
        margin: auto;
        position: relative;
        background: #fff;
        z-index: 1;
    }
    #offers.oval-shad:before {
        content: '';
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-image: url(images/home/bg_special_offers.jpg);
        background-position: center;
        background-size: cover;
        position: absolute;
        z-index: 0;
    }
    #offers.oval-shad:after {
        content: '';
        width: 80%;
        height: 10%;
        border-radius: 50%;
        box-shadow: 0 0 10px rgba(0,0,0,.5);
        display: block;
        position: absolute;
        z-index: -1;
        left: 10%;
        bottom: 0;
    }
    #offer_slider .thumbnail {
        border: 2px solid #f5f5f5;
    }
    #offer_slider .caption {
        font-size: 13px;
    }
    #offer_slider .caption span {
        font-family: 'Cinzel', serif;
        font-size: 15px;
        color: #304446;
    }
    #awards {
        text-align: center;
        color: #797266;
    }
    #awards .title {
        font-family: 'Roboto Condensed', sans-serif;
        font-size: 20px;
        font-weight: 300;
    }
    #awards .title span {
        font-family: 'Cinzel', serif;
        font-size: 1.8em;
        color: #373737;
    }
    #moments {
        background-image: url(images/home/bg_moment.png);
        background-position: center;
        background-size: cover;
    }
	#moment_slider .owl-nav.disabled {
		display: block; /*force show nav arrows*/
	}
    #moment_slider .thumbnail {
        border: 5px solid #f5f5f5;
        box-shadow: 0 0 3px rgba(0,0,0,.3);
    }
    #moment_slider .item {
        padding: 0;
    }
    #moment_slider .caption {
        font-size: 12px;
        padding: 8px;
    }
    #moment_slider .title {
        font-family: 'Cinzel', serif;
        font-weight: 400;
        font-size: 18px;
        color: #5c4d33;
        margin: 0;
    }
    #moment_slider .excerpt {
        margin: 5px 0;
    }
    #attraction {
        background-image: url(images/home/bg_attraction.jpg);
        background-position: center;
        background-size: cover;
        position: relative;
        padding: 60px 0 70px;
    }
    #attraction .section-title:after {
        margin-left: 40px;
    }
    .row-attraction:before {
        content: '';
        background-image: url(assets/elements/seperate_columns.png);
        width: 33px;
        height: 402px;
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -15px;
        margin-top: -140px;
    }
    .row-attraction .row-phuket {
        padding-right: 15px;
    }
    .row-attraction .row-krabi {
        padding-left: 15px;
    }
    .row-attraction h2 {
        margin-top: 0;
        margin-bottom: 15px;
        font-size: 18px;
        color: #304446;
    }
    .row-attraction .krabi h2 {
        text-align: right;
    }
    .row-attraction .inner {
       text-align: center;
    }
    .row-attraction .inner .thumbnail {
        border: 4px solid #fff;
        border-radius: 3px;
        overflow: hidden;
        display: block;
        position: relative;
        z-index: 2;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
    }
    .row-attraction .inner .title {
        background-color: #e5e5e5;
        margin: 0;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color: #ab8205;
        line-height: 18px;
        margin-left: 10px;
        margin-right: 10px;
        padding: 5px;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
        border-radius: 0 0 2px 2px;
        position: relative;
        z-index: 1;
    }
    .row-attraction .inner .more {
        display: block;
        background-color: #63b4d8;
        font-size: 12px;
        color: #fff;
        margin: 0 24px;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
        padding: 3px;
    }
    .row-attraction .inner .more:hover {
        background-color: #333;
    }
    .deco-map {
        position: relative;
        left: 80px;
    }
    .deco-map:before {
        content: '';
        position: absolute;
        top: -18px;
        left: -160px;
        width: 160px;
        height: 78px;
        background-image: url(assets/elements/deco-map.png);
    }
    @media (max-width: 960px) {
        .row-attraction:before {
            content: none;
        }
        .row-attraction > .phuket,
        .row-attraction > .krabi {
            padding-bottom: 30px;
        }
        .row-attraction > .phuket {
            border-right: 1px solid #ccc;
        }
        .row-attraction > .krabi {
            border-left: 1px solid #fff;
        }
    }
    @media (max-width: 720px) {
        #attraction .section-title {
            margin-top: 50px;
        }
        #attraction .section-title:after {
            margin-left: -40px;
            *top: 180%;
        }
        .row-attraction > [class*="col-"] {
            width: 100%;
            border: 0;
        }
        .row-attraction > .phuket,
        .row-attraction > .krabi {
            padding-bottom: 0;
        }
        .row-attraction .row-phuket {
            margin-bottom: 30px;
            padding-right: 0;
        }
        .row-attraction .row-krabi {
            padding-left: 0;
        }
        .row-attraction .phuket h2,
        .row-attraction .krabi h2 {
            text-align: center;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            top: -80px;
            left: 50%;
            margin-left: -80px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
