$(function() {
    
    /**
     * Simple Toggle Class
     */
    $.fn.simpleToggleClass = function(c, t) {
        // c = class, t = dom
        var $this = $(this);
        var target = $(t);
        
        $this.on('click', function() {
            if( ! $this.hasClass(c) ) {
                $this.addClass(c);
                target.addClass(c);
            } else {
                $this.removeClass(c);
                target.removeClass(c);
            }
        });
    }
    
    var $slider = $('.hero-slider');
    $slider.owlCarousel({
        items: 1,
        dots: false,
        nav: false,
        pullDrag: false,
        autoplay: true,
        responsiveRefreshRate: 200,
    });
    
    $('[data-bg]').each(function() {
        var $this = $(this);
        var imgUrl = $this.data('bg');
        $this.css({
            'background-image': 'url('+imgUrl+')',
            'background-size': 'cover',
            'background-position': 'center',
        });
    });
    
    $('.custom-select').each(function(e) {
        var $this = $(this);
        
        function openMenu() {
            $this.addClass('focus');
        }
        function closeMenu() {
            $this.removeClass('focus');
        }
        
        $this.on('click', function(e) {
            if( $this.hasClass('focus') ) {
                closeMenu();
            } else {
                openMenu();
            }
        });
        
        $(document).on('click', function(e) {
            var target = e.target;
            if( $(target).closest($this).length == 0 ) {
                closeMenu();
            }
        });
    });
    
    /**
     * Validation
     */
    $.validate({
        form: '#contact_form',
        modules: 'location, html5',
        onModulesLoaded: function() {
			$('input[name="country"]').suggestCountry();
		},
        
		onSuccess: function(form) {
            var url = $(form).attr('action');
            var formData = $(form).serialize();
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
            }).done(function(status) {
               if (status === 'ok') {
                   console.log('Sending Successful');
                   
                   $(form).find(':focus').blur();
                   $(form)[0].reset();
                   grecaptcha.reset();
                   
                   //Reset hotel select
                   $('input#hotel').val('');
                   $(form).find('.custom-select').each(function() {
                       var defaultText = $(this).find('.option [data-email]').eq(0).text();
                       $(this).find('.selected').text(defaultText);
                   });
                   
                   $(form).before('<div id="contact_result" />');
                   $('#contact_result').html('<i class="icon fa fa-check-circle"></i> Thank you for contact us.');
                   $('#contact_result').delay(200).slideDown(200);
               } else if( status === 'not ok' ) {
                   console.log('Sending Failed because Google Recaptcha');
                   $(form).find('.g-recaptcha-error').text('Please check the recaptcha');
               } else {
				   console.log('Unknowed error');
				   $(form).addClass('form-error');
				   $(form).find('#form_result').html('Sorry, There is something error, Please contact directly to <a href="mailto:info@atmindgroup.com">info@atmindgroup.com</a>').slideDown(200);
			   }
            });
            
            return false;
        },
    });
    
    $.validate({
        form: '#newsletter-form',
        onSuccess: function(form) {
            var url = $(form).attr('action');
            var formData = $(form).serialize();
            $.post(url, formData, function( data ) {
                if( data == 'ok' ) {
                    $(form)[0].reset();
                    $(form).find(':focus').blur();
                    $(form).addClass('completed');
                    
                    var $result = $(form).next('.form-result');
                    $result.addClass('success')
                    $result.html('<i class="icon fa fa-check-circle"></i> Subscription Successful');
                    $result.slideDown(300).delay(5000).slideUp(300);
                    
                    console.log( data );
                } else {
                    //$(form).after('<div><span style="color: tomato;">Subscription Failed.</span><br/>Please contact directly to <a href="mailto:info@atmindsukhumvit.com">info@atmindsukhumvit.com</a></div>');
                }
            });
            return false;
        }
    });
    
	var $lang = $('#site-lang');
	var $display = $lang.find('.default');
	var $selected = $lang.find('.option.selected a').html();
	$display.html($selected);

    /**
     * New Header Scroll FX
     */
    var $header = $('.site-header');
    var $bookingBar = $('.booking-bar');
    var $bookingBarInner = $bookingBar.children('.inner');
    $(window).on('scroll resize', $.throttle(16, function() {
        var winY = window.scrollY;
        var winW = window.innerWidth;
        if (winY >= 150 && winW >= 1140) {
            $header.addClass('mini');
        } else {
            $header.removeClass('mini');
        }

        var trigger = $bookingBar.offset().top - 70;
        if (winY >= trigger && winW >= 1140) {
            $bookingBar.addClass('fixed');
        } else {
            $bookingBar.removeClass('fixed');
        }
    }));

    /**
     * Side Panel
     */
    var $sidePanel = $('.side-panel');
    $sidePanel.find('.has-sub-menu > a').each(function() {
        var el = this;
        var pa = $(el).parent();
        var sm = $(el).next('.sub-menu');
        $(el).on('click', function(event) {
            event.preventDefault();
            if (pa.hasClass('show')) {
                pa.removeClass('show');
                sm.slideUp(350);
            } else {
                pa.addClass('show');
                sm.slideDown(350);
            }
        });
    });

    var currMenu = $sidePanel.find('.current');
    if (currMenu.length) {
        var container = currMenu.parents('.has-sub-menu');
        container.each(function() {
            var submenu = $(this).children('.sub-menu');
            submenu.show();
            $(this).addClass('show');
        });
    }

    $('.side-panel-trigger').on('click', function(event) {
        event.preventDefault();
        if (sidePanel()) {
            sidePanel('hide');
        } else {
            sidePanel('show');
        }
    });

    $('.side-panel-bg').on('click', function(event) {
        event.preventDefault();
        if (sidePanel()) {
            sidePanel('hide');
        } else {
            sidePanel('show');
        }
    });

    $('.side-panel-drag').swipe({
        swipeRight: function() {
            sidePanel('show');
        }
    });

    $('.side-panel-bg').swipe({
        swipeLeft: function() {
            sidePanel('hide');
        }
    });

    function sidePanel(methods) {
        switch (methods) {
            case 'show':
                $('body').addClass('side-panel-show');
                break;
            case 'hide':
                $('body').removeClass('side-panel-show');
                break;
            default:
                return $('body').hasClass('side-panel-show');
        }
    }

    $(window).on('resize', $.throttle(16, function() { 
        if (window.innerWidth >= 992) {
            sidePanel('hide');
        }
    }));

    /**
     * Booking Bar
     */
    $('#booking-form').booking({
        checkInSelector: '[name="checkin"]',
        checkOutSelector: '[name="checkout"]',
        adultSelector: '[name="numofadult"]',
        childSelector: '[name="numofchild"]',
        roomSelector: '[name="numofroom"]',
        codeSelector: '[name="accesscode"]',
        propertyId: '[name="propertyId"]',
        onlineId: 5,
        language: 'zh',
    });

    /**
     * Member Panel
     */
    var $m_display  = $('.member-display');
    var $m_btn      = $('.member-panel-trigger');
    var $m_panel    = $('.member-panel');
    var $m_form     = $('#form_member');
    var $m_close    = $m_panel.find('.btn-close');

    var memb  = $('#member');
    var mcont = $('.member-content');
    var mtrig = $('.member-content-trigger');
    var mclos = memb.find('.btn-close');

    $m_btn.on('click', function(e) {
        e.preventDefault();
        if (memberPanel()) {
            memberPanel('hide');
        } else {
            memberPanel('show');
        }
    });

    $m_close.on('click keypress', function(e) {
        e.preventDefault();
        memberPanel('hide');
        if (e.type == 'keypress' && e.keyCode == 32 || e.keyCode == 13)
            memberPanel('hide');
    });

    function memberPanel(methods) {
        switch (methods) {
            case 'show':
                $m_btn.addClass('show');
                $m_panel.slideDown(350, function() {
                    $m_panel.find('input[type="email"]').focus();
                });
                break;
            case 'hide':
                $m_btn.removeClass('show');
                $m_panel.slideUp(350);
                break;
            default:
                return $m_btn.hasClass('show');
        }
    }

    /**
     * Member Form
     */
    var memberCode = 'member';

    if (memberChecker()) {
        memberDisplay('member');
        $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
    } else {
        memberDisplay('guest');
    }

    $m_form.on('submit', function(event) {
        event.preventDefault();
        var email = $(this).find('[type="email"]').val();
        memberCreateCookies({
            code: 'member',
            expire: 7,
            user: {
                email: email,
            }
        });
        var url = this.baseURI;
        memberSendData({
            formAPI: '648466c4ac12000257e768ffedac2e73',
            formType: 2,
            customer: {
                email: email,
                message: 'This information send from Deevana Krabi Resort (Group) by Member form',
            }
        });

        memberPanel('hide');
        memberDisplay('member');
        window.open( 'https://reservation.travelanium.net/propertyibe2/?propertyId=386&onlineId=5&lang=zh&pgroup=QQMPUPYV&accesscode='+memberCode, '_blank' );

        memberAppendCode(memberCode);
        $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
    });

    function memberDisplay(methods) {
        switch (methods) {
            case 'guest':
                $m_display
                    .removeClass('is-memmber')
                    .addClass('is-guest');
                break;
            case 'member':
                $m_display
                    .removeClass('is-guest')
                    .addClass('is-member');
                break;
        }
    }
});