<?php

$url        = 'http://www.deevanahotels.com/zh/';
$name       = 'Deevana Hotels & Resorts | Service quality makes every stay unique.';
$version    = '20180419';
$email      = 'chatchai.n@deevanagroup.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/deevanahotelsandresorts/';
$twitter    = 'https://twitter.com/deevanahotels';
$googleplus = '#';
$youtube    = 'https://www.youtube.com/channel/UC_zIuBzbXVgu7ps388lY0Rg';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/deevanahotels/';
$flickr     = '#';
$pinterest  = '#';

$ibeID      = '';