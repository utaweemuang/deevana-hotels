<?php

require_once('_info.php');

function get_info( $var ) {
	global
        $url, $name, $version, $email, $author, $debugEmail, $ibeID,
        $facebook, $twitter, $googleplus, $youtube, $vimeo, $instagram, $flickr, $pinterest;
    
    if( $var == 'url' ) { return $url; }
    if( $var == 'name' ) { return $name; }
    if( $var == 'version' ) { return $version; }
    if( $var == 'email' ) { return $email; }
    if( $var == 'debugEmail' ) { return $debugEmail; }
    if( $var == 'author' ) { return $author; }
    if( $var == 'ibeid' ) { return $ibeID; }
    
    if( $var == 'facebook' ) { return $facebook; }
    if( $var == 'twitter' ) { return $twitter; }
    if( $var == 'googleplus' ) { return $googleplus; }
    if( $var == 'youtube' ) { return $youtube; }
    if( $var == 'vimeo' ) { return $vimeo; }
    if( $var == 'instagram' ) { return $instagram; }
    if( $var == 'flickr' ) { return $flickr; }
    if( $var == 'pinterest' ) { return $pinterest ; }
}

function web_title() {
    global $title; //set var $web_title in each page
    if( ! empty($title) || $title != NULL )
        echo $title;
    else
        echo get_info( 'name' );
}

function web_desc() {
    global $desc; //set var $web_desc in each page
    if( !empty($desc) || $desc != NULL )
        echo '<meta name="description" content="'.$desc.'">' . "\n";
}

function web_keyw() {
    global $keyw; //set var $web_keyw in each page
    if( !empty($keyw) || $keyw != NULL )
        echo '<meta name="keywords" content="'.$keyw.'">' . "\n";
}

function html_class() {
    global $html_class; //set var $html_class in each page
    if( !empty($html_class) || $html_class != NULL )
        echo $html_class;
}

function body_class() {
    global $body_class; //set var $body_class in each page
    if( !empty($body_class) )
        echo $body_class;
}

function get_current_class( $page = NULL ) {
    global $cur_page; //set var $cur_page in each page
    if( $page != NULL && $cur_page == $page )
        return 'current';
}

function ibe_url( $hid = NULL, $lang = 'en', $pid = NULL ) {
    global $ibeID;
    
    $hid = ( $hid !== NULL ) ? $hid : $ibeID;
    $page = ($pid !== NULL) ? 'hotelpage' : 'propertyibe2' ;
    
    $url = 'https://reservation.travelanium.net/'.$page.'/rates?propertyId='.$hid.'&onlineId=5&lang='.$lang;
    
    if( $pid !== NULL )
        $url .= '&pid='.$pid;
    
    echo $url;
}