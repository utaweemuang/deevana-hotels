<?php
$title = 'Attractions | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Attractions: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'attractions, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'attraction';
$cur_page = 'attraction';

$lang_en = '/deevanaplazakrabi/attraction.php';
$lang_th = '/th/deevanaplazakrabi/attraction.php';
$lang_zh = '/zh/deevanaplazakrabi/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#emerald_pool">翡翠池</li>
                            <!-- <li class="tab" data-tab="#elephant_ride">大象骑</li> -->
                            <li class="tab" data-tab="#hong_island">洪島</li>
                            <li class="tab" data-tab="#tiger_cave_temple">老虎洞寺庙</li>
                            <li class="tab" data-tab="#phi_phi_island">Phi Phi Island</li>
                            <li class="tab" data-tab="#thalay_wak">Thalay Wak</li>
							<li class="tab" data-tab="#nong_thale_canel">Nong Thale Canal</li>
							<li class="tab" data-tab="#koh_kai">Koh Kai</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="emerald_pool" data-tab-name="Emerald Pool">
                        <img class="thumbnail force" src="images/attraction/emerald_pool.png" alt="Emerald Pool" />
                        <h1 class="title">翡翠池</h1>
                        <p>翡翠池或“Sra Morakot'”在泰国是一个非常美妙的天然泳池，它的四周森林环绕，闪闪发光的清澈泉水源源不断地注入其中。翡翠池位于Khao Pra - Bang Khram 野生动物保护区,当地称为Khao Nor Chuchi 的低地森林,沿着4038号公路大概18公里就到Klong Thom.的 运河地区办公室。</p>
                        <p>到这里之后,大部分游客都迫不及待地跳进清澈翠绿的淡水池，享受让人神清气爽的游泳。</p>
                        <p>Sra Morakot是一个近乎是一个圆形池子，1 - 2米深,直径约20 - 25米。当阳光洒在水面上，水就会变成浅绿色或翡翠色。</p>
                        <p>Sra Morakot的水来自被称为“蓝池”的倒影池,距离山顶600米远。含有硫磺的碱性水碱性水从蓝池井,流经岩石裂缝到达翡翠池。因此,水总是清澈见的,因为高碳酸钙会使所有的悬浮物沉淀。此外,藻类无法在这种水里生长。</p>
                        <p>巴厘岛风格的茅草小屋已修建多年，当初是专门用来招待皇室的。</p>
                    </article>
                    
                    <!-- <article class="article" id="elephant_ride" data-tab-name="Elephant Ride">
                        <img class="thumbnail force" src="images/attraction/elephant_ride.png" alt="Elephant Ride" />
                        <h1 class="title">大象骑</h1>
                        <p>大象被看作是国家的象征,泰国人民的护身符。它们不仅有灰色皮肤皱纹和摇曳的鼻子,而且能把蛮力,温柔和非凡的敏捷性巧妙地融合在一起，,这使得它们获得了人们的尊敬和爱戴。</p>
                        <p>在泰国，野生的大象迅速消失,近距离地看到这些奇妙的动物的最好地方是在登山营地。在甲米，只有少数的营地有这样的资格，这些机构机构必须严格遵守国家畜牧部门设定的的指导方针——为动物们提供提供充足的食物、水和阴凉,以及适当的卫生保健。</p>
                        <p>长途跋涉将会使你直接体验到这一点,还有动物们的自然森林环境。为娱乐而载客意味着大象能挣到它们的生活费，而能够生活得自由:因为没有足够的空间来把它们放回大自然,他们需要非常昂贵的饲料(每天消耗200 - 300公斤的食物)，除此之外这些温和的巨人的只能选择是乞讨,非法砍伐或囚禁在动物园里。</p>
                        <p>徒步之旅——通常是一个小时的骑大象,选择性地结合另一个观光活动，可以在下面预定或通过任何本地代理。你也可以直接访问营地徒步或者只是观察和拍摄大象,即使不打算骑。</p>
                    </article> -->
                    
                    <article class="article" id="hong_island" data-tab-name="Hong Island">
                        <img class="thumbnail force" src="images/attraction/hong_island.png" alt="Hong Island" />
                        <h1 class="title">洪島</h1>
                        <p>宏岛位于攀牙湾，乘快艇大约30分钟到普吉岛的东海岸。“宏”在泰语里是“房间”的意思，,因为岛的中心是空心的，所以它就像个房间。这些“房间”是完全切断与外界的联系,只能通过划海上独木舟通过洞穴才能访问。有很多旅行社组织从普吉岛来的一日游,你可以探索的魔幻的“宏岛”和海洋洞穴。</p>
                    </article>
                    
                    <article class="article" id="tiger_cave_temple" data-tab-name="Tiger Cave Temple">
                        <img class="thumbnail force" src="images/attraction/tiger_cave_temple.png" alt="Tiger Cave Temple" />
                        <h1 class="title">老虎洞寺庙</h1>
                        <p>老虎早就消失了,只在山洞里留下脚印,现在建造了一个神龛用来供奉一个老虎的雕像和许多佛像。其他有趣的特色有：一个把鲸头骨和人类骸骨的巧妙组合，它可以用来帮助你思考生命的无常。这里明显受中国文化的影响，一个高大的中国风格宝塔内放置一个巨大的雕像观音——大乘佛教中救苦救难的“女神”。</p>
                        <p>殿里的主题画像是佛陀形象和一个镀金的古旧望台矗立在悬崖顶上。从这里你能看到美到窒息的景色——北边的Khao Phanom Bencha（高番本查国家公园）若隐若现，,陡峭的喀斯特悬崖把Railay 从陆地切断并向西部抬升，使得甲米河的河口向南流入大海。在一个晴朗的日子里,你可以看到到皮皮岛的所有景观。</p>
                    </article>

                    <article class="article" id="phi_phi_island" data-tab-name="Phi Phi Island">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/phi_phi_island.png" />
                        <h1>Phi Phi Island</h1>
                        <p>Phi Phi Island is Thailand's island-superstar. It's been in the movies. It's the topic of conversation for travelers all over Thailand. For some, it's the only reason to touchdown in Phuket. Even with all the hype, it doesn't disappoint. Phi Phi's beauty is a large chunk of the allure. The islands, when approached by boat, rise from the sea like a fortress. Sheer cliffs tower overhead, then give way to beach-fronted jungle. It's love at first sight.</p>
                        <p>The second part of the why-we-love-this-place story is attitude: few places on the planet are this laid-back. Of the two islands, one is completely free of human inhabitants (Phi Phi Leh), and the other is without roads (Phi Phi Don). There's no schedule, no hustle-and-bustle, no reason to be in a hurry.</p>
                    </article>

                    <article class="article" id="thalay_wak" data-tab-name="Thalay Wak">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/talay-wak.png" />
                        <h1>Thalay Wak</h1>
                        <p>Thalay Wak is a natural attraction in Krabi that has the title “Unseen Thailand”, famous across the world. A natural phenomenon occurs that causes the seas to part. When the tide is low, a Y-shaped sandbank connecting 3 islands appears; Tup Island, Mor Island, and Chicken Island. When the tide rises, the sandbank disappears into the ocean.</p>
                    </article>

                    <article class="article" id="nong_thale_canel" data-tab-name="Nong Thale Canal">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/Klongnongtale.png" />
                        <h1>Nong Thale Canal</h1>
                        <p>Nong Thale Canal is located at the boundary between Moo (Avenue) 1 and Moo (Avenue) 4, Tambon Nong Thale, Muang District, Krabi Province. In the past, this canal was a small freshwater canal. It looked like a swamp full of small trees. About 10 years ago, there were overflow weirs in the middle of the canal to slow down the water to use during the dry season and provide tap water to the people. From small canyons and peat swamps, it became a large basin with an area of about 100 acres, a depth of about 5 meters, and a width of about 200 meters.Nowadays, "Nong Talay Canal" or "Klong Root" has become a new ecotourism destination in Krabi. Nong Thale Subdistrict Administrative Organization in conjunction with the village headmen and villagers helped develop the route to accommodate ecological tourists. By kayaking (canoe), go upstream and enjoy the scenic rocky structures; similar to a small mountain with tree stumps.</p>
                    </article>

                    <article class="article" id="koh_kai" data-tab-name="Koh Kai">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/koh-kai.png" />
                        <h1>Koh Kai</h1>
                        <p>Koh Kai (sometimes spelled Koh Gai or Koh Khai and meaning ‘Chicken Island’ in Thai), is also called Koh Hua Khawan or Koh Poda Nok. It is a small island belonging to the Poda group of islands located about eight kilometres from Ao Nang in the province of Krabi. Koh Kai takes its name from the chicken-shaped rock forming its southern tip. Most island-hopping tours from Krabi and Phuket make a short stop at this picturesque limestone chicken, giving visitors a cool photo opportunity.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(t).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
            console.log(offset+', '+targetPos);
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: 100%;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>