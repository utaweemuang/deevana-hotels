<form id="room-booking-form" class="form">
    <h3 class="title">Book online</h3>

    <div class="row">
        <div class="col-w6 field field-checkin">
            <label class="label" for="checkin">Check-in</label>
            <input class="input-text" id="checkin" type="text" readonly />
        </div>

        <div class="col-w6 field field-checkin">
            <label class="label" for="checkout">Check-out</label>
            <input class="input-text" id="checkout" type="text" readonly />
        </div>

        <div class="col-w6 field field-adult">
            <select class="input-select" id="adults">
                <option value="1">1 adult</option>
                <option value="2" selected>2 adults</option>
                <option value="3">3 adults</option>
                <option value="4">4 adults</option>
                <option value="5">5 adults</option>
                <option value="6">6 adults</option>
                <option value="7">7 adults</option>
                <option value="8">8 adults</option>
                <option value="9">9 adults</option>
                <option value="10">10 adults</option>
                <option value="11">11 adults</option>
                <option value="12">12 adults</option>
            </select>
        </div>

        <div class="col-w6 field field-child">
            <select class="input-select" id="children">
                <option value="0" selected>0 children</option>
                <option value="1">1 child</option>
                <option value="2">2 children</option>
                <option value="3">3 children</option>
                <option value="4">4 children</option>
                <option value="5">5 children</option>
            </select>
        </div>

        <div class="col-w6 field field-room">
            <select class="input-select" id="rooms">
                <option value="1" selected>1 room</option>
                <option value="2">2 rooms</option>
                <option value="3">3 rooms</option>
                <option value="4">4 rooms</option>
                <option value="5">5 rooms</option>
                <option value="6">6 rooms</option>
                <option value="7">7 rooms</option>
                <option value="8">8 rooms</option>
                <option value="9">9 rooms</option>
            </select>
        </div>

        <div class="col-w6 field field-code">
            <input class="input-text" id="accesscode" placeholder="Promo Code" type="text" />
        </div>

        <div class="col-w12 field field-submit">
            <button class="button" id="submit" type="submit">Check Availibility</button>
            <p class="note">Enjoy exclusive benefits when you book direct with us.</p>
        </div>
    </div>
</form>


<form id="newsletter-form" class="form" action="forms/subscribe_form.php">
    <h3 class="title">Newsletter</h3>

    <div class="row">
        <div class="col-w8 field field-email">
            <input class="input-text" id="email" name="email" placeholder="Your email address" type="email" />
        </div>

        <div class="col-w4 field field-submit">
            <input type="hidden" name="sendto" value="<?php echo get_info('email'); ?>" />
            <button class="button" id="submit" type="submit">SUBMIT</button>
        </div>
    </div>
</form>

