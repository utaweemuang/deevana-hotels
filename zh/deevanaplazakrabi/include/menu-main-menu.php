<ul class="menu list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">家</a></li>
    <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">住宿</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">豪華房</a></li>
            <li class="<?php echo get_current_class('premier-room'); ?>"><a href="room-premier-room.php">尊貴間</a></li>
            <li class="<?php echo get_current_class('premier-pool-access'); ?>"><a href="room-premier-pool-access.php">高級泳池訪問</a></li>
            <li class="<?php echo get_current_class('family-room'); ?>"><a href="room-family-room.php">家庭房</a></li>
            <li class="<?php echo get_current_class('deluxe-suite'); ?>"><a href="room-deluxe-suite.php">複式套房</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">設施</a></li>
    <li class="<?php echo get_current_class('meetings'); ?>"><a href="meetings-and-event.php">會議與活動</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">景點</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('276', 'zh'); ?>" target="_blank">提升</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">畫廊</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">聯繫</a></li>
</ul>