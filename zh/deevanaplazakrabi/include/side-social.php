<div id="social_side">
    <ul>
        <li><a class="icon fb" href="<?php echo get_info('facebook'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-facebook"></i></a></li>
        <li><a class="icon yt" href="<?php echo get_info('youtube'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-youtube"></i></a></li>
    </ul>
</div>