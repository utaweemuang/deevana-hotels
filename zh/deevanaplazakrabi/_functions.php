<?php

require_once('_info.php');

function get_info( $var ) {
	global
        $url, $name, $version, $email, $author, $debugEmail, $ibeID,
        $facebook, $twitter, $googleplus, $youtube, $vimeo, $instagram, $flickr, $pinterest, $tripadvisor;
	
	switch( $var ) {
		case 'url': return $url; break;
		case 'name': return $name; break;
		case 'version': return $version; break;
		case 'email': return $email; break;
		case 'debugEmail': return $debugEmail; break;
		case 'author': return $author; break;
        case 'ibeID': return $ibeID; break;
            
		case 'facebook': return $facebook; break;
		case 'twitter': return $twitter; break;
		case 'googleplus': return $googleplus; break;
		case 'youtube': return $youtube; break;
		case 'vimeo': return $vimeo; break;
		case 'instagram': return $instagram; break;
		case 'flickr': return $flickr; break;
		case 'pinterest': return $pinterest; break;
		case 'tripadvisor': return $tripadvisor; break;
	}
}

function web_title() {
    global $title; //set var $web_title in each page
    if( ! empty($title) || $title != NULL )
        echo $title;
    else
        echo get_info( 'name' );
}

function web_desc() {
    global $desc; //set var $web_desc in each page
    if( !empty($desc) || $desc != NULL )
        echo '<meta name="description" content="'.$desc.'">' . "\n";
}

function web_keyw() {
    global $keyw; //set var $web_keyw in each page
    if( !empty($keyw) || $keyw != NULL )
        echo '<meta name="keywords" content="'.$keyw.'">' . "\n";
}

function html_class() {
    global $html_class; //set var $html_class in each page
    if( !empty($html_class) || $html_class != NULL )
        echo $html_class;
}

function body_class() {
    global $body_class; //set var $body_class in each page
    if( !empty($body_class) )
        echo $body_class;
}

function get_current_class( $page = NULL ) {
    global $cur_page; //set var $cur_page in each page
    global $par_page; //set var $par_page in each page (if need)
    if( $page != NULL && $cur_page == $page )
        return 'current';
    if( $page != NULL && $par_page == $page )
        return 'ancestor';
}

function ibe_url( $hid = NULL, $lang = 'zh', $pid = NULL ) {
    global $ibeID;
    
    $hid = ( $hid !== NULL ) ? $hid : $ibeID;
    $page = ($pid !== NULL) ? 'hotelpage' : 'propertyibe2' ;
    
    $url = 'https://reservation.travelanium.net/'.$page.'/rates?propertyId='.$hid.'&onlineId=5&lang='.$lang;
    
    if( $pid !== NULL )
        $url .= '&pid='.$pid;
    
    echo $url;
}