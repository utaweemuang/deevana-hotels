<?php
$title = 'Deluxe Suite | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Deluxe Suite: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'deluxe suite, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-suite';
$cur_page = 'deluxe-suite';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-deluxe-suite.php';
$lang_th = '/th/deevanaplazakrabi/room-deluxe-suite.php';
$lang_zh = '/zh/deevanaplazakrabi/room-deluxe-suite.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-01.jpg" alt="Deluxe Suite 01" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-02.jpg" alt="Deluxe Suite 02" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-03.jpg" alt="Deluxe Suite 03" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-04.jpg" alt="Deluxe Suite 04" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Suite <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-04.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">豪华套间</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>这里能满足选择甲米奥南蒂瓦娜广场的最挑剔的客人,66平方米超大豪华套房包括一个客厅和一个卧室，旨在提供一个清新和放松个人空间，这个大套房值得称赞的地方还有它精装的能看到整个度假村景色的阳台，一个美丽的有浴缸和淋浴的浴室和高端品牌化妆品，每个套间都有一个特大号床,还包括一个卧室里的32英寸的液晶电视和客厅里的40英寸的液晶电视,DVD播放器，充电底座，迷你冰箱和咖啡机，闹钟，浴室秤，化妆镜，熨斗和熨衣板，可以和贵宾房联系。</p>
                            <p>房间数量：4</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                     <!--   <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 66 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Pool view</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>