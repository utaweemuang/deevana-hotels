<?php

session_start();

$title = 'Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanaplazakrabi';
$lang_th = '/th/deevanaplazakrabi';
$lang_zh = '/zh/deevanaplazakrabi';

include '_header.php';
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>

        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php //include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">歡迎來到<br>
                            <span style="color:#244289;">甲米奥南蒂瓦纳广场酒店</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>甲米岛奥南区的Deevana广场酒店距离奥南海滩和诺帕拉塔拉海滩都很近，它是一个别致的当代风格低层建筑群。有213间不同风格的房间和套间,每一个房间都有私人阳台,客人可以享受酒店的良好的设施和Deevana广场酒店友好的和个性化的服务团队。度假村的特色有额外的餐厅和酒吧,三个大型户外游泳池、儿童池,休闲保健按摩中心，杰出的会议和活动设施。</p>
                        <p>从甲米迷人的自然风光和国家公园保护的丰富的野生动物中汲取灵感，甲米奥南德瓦纳广场酒店已经命名的餐厅，在鸟类如色彩鲜艳的犀鸟、翠鸟和会议设施的酒吧。这里距甲米国际机场30分钟车程，距甲米镇20分钟，到普吉国际机场2小时。甲米奥南德瓦纳广场酒店是休闲客人度蜜月，家庭，以及企业会议及奖励旅游的绝佳场所</p>
                        <p>甲米奥南德瓦纳广场酒店靠近甲米的美丽的岛屿和海滩，以及海鲜餐厅，当地市场的商品琳琅满目，还有充满活力的夜生活等等；这里是泰国一个最令人心驰神往心的风景胜地，一个真正难忘的假期或商务旅行圣地，绝对会让你不虚此行。</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/2ukuaeu19qQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">歡迎來到<br>
                            <span style="color:#244289;">甲米奥南蒂瓦纳广场酒店</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>甲米岛奥南区的Deevana广场酒店距离奥南海滩和诺帕拉塔拉海滩都很近，它是一个别致的当代风格低层建筑群。有213间不同风格的房间和套间,每一个房间都有私人阳台,客人可以享受酒店的良好的设施和Deevana广场酒店友好的和个性化的服务团队。度假村的特色有额外的餐厅和酒吧,三个大型户外游泳池、儿童池,休闲保健按摩中心，杰出的会议和活动设施。</p>
                        <p>从甲米迷人的自然风光和国家公园保护的丰富的野生动物中汲取灵感，甲米奥南德瓦纳广场酒店已经命名的餐厅，在鸟类如色彩鲜艳的犀鸟、翠鸟和会议设施的酒吧。这里距甲米国际机场30分钟车程，距甲米镇20分钟，到普吉国际机场2小时。甲米奥南德瓦纳广场酒店是休闲客人度蜜月，家庭，以及企业会议及奖励旅游的绝佳场所</p>
                        <p>甲米奥南德瓦纳广场酒店靠近甲米的美丽的岛屿和海滩，以及海鲜餐厅，当地市场的商品琳琅满目，还有充满活力的夜生活等等；这里是泰国一个最令人心驰神往心的风景胜地，一个真正难忘的假期或商务旅行圣地，绝对会让你不虚此行。</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/deevanaplazakrabi/images/banner/specialsave50.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>SPECIAL SAVE 50%</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>Free in room internet (Wifi connection)</li>
                                        <li>Late check-out until 14.00 (Subject to available)</li>
                                        <li>20% discount on spa treatment (except package)</li>
                                        <li>20% Discount on Food & Beverage ( Except wine and Mini-bar)</li>
                                        <!-- <li>10% Discount on laundry service</li>
                                        <li>FREE Krabi tour for 4 Islands ( Phranang Cave,Tup Island , Poda & Chicken Island )All Tours are exclude the National Park Entrance Fee.</li>
                                        <li>FREE round-trip transfer from Krabi international airport to hotel</li> -->
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=276&onlineId=5&pid=MDczMTA4NA%3D%3D&lang=zh">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=276&group=13&width=450&height=300&imageid=6874&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>MINIMUM 2 NIGHTS STAY (FREE AIRPORT ROUND-TRIP)</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 2 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>FREE round-trip transfer from Krabi international airport to hotel</li>
                                        <li>20% off on Spa A la carte treatment menu (exclude package)</li>
                                        <li>20% off on Food and Beverage at hotel restaurant (exclude alcohol and mini bar)</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=276&onlineId=5&pid=MDg0MDMy&lang=zh">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=276&group=13&width=450&height=300&imageid=8079&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>HONEYMOON PACKAGE</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 3 nights</p>
                                    <ul>
                                        <li>Daily breakfast at Kingfisher Restaurant.</li>
                                        <li>In-room internet</li>
                                        <li>Late check-out 14.00(Subject to room availability)</li>
                                        <li>Romantic set up with flower upon arrival day.</li>
                                        <li>One time of private Thai Set Dinner for 2 persons</li>
                                        <li>Thai cooking class Inclusive of menu recipe, apron and cap, Certificate and Photo with frame</li>
                                        <li>20% off on Spa A la carte treatment menu</li>
                                        <li>20% off on Food and Beverage at hotel restaurant (exclude alcohol and mini bar)</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=276&onlineId=5&pid=MDgzMzEz&lang=zh">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=276&group=13&width=450&height=300&imageid=6260&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>FAMILY PACKAGE</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 2 nights</p>
                                    <ul>
                                        <li>Daily Breakfast at King Fisher Restaurant.</li>
                                        <li>Free in room internet (Wifi connection)</li>
                                        <li>Late check-out 14.00(Subject to room availability)</li>
                                        <li>Complimentary breakfast for 2 child under 12 years.</li>
                                        <li>One Time Thai Set Dinner for 2 Adults & 2 Child</li>
                                        <li>Complimentary full mini bar ( exclude alcohol ) in guest room daily per package</li>
                                        <li>20% off on Spa A la carte treatment menu (exclude package)</li>
                                        <li>20% off on Food and Beverage at hotel restaurant (exclude alcohol and mini bar)</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=276&onlineId=5&pid=MDgzMzE0&lang=zh">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#7b9028;">独一无二的全套设施满足您的所有需求</h1>

                <div id="activities_slider" class="owl-carousel has-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-orientala_wellness_spa.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">Orientala Spa</h2>
                            <p class="description">典雅华贵的 Orientala Spa 邀请客人体验传统泰式按摩，包括芳香疗法和草药治疗。这是让你放松休息的完美场所，Orientala Spa 具有私人双人房的按摩浴缸和蒸汽房、2个独立单间，值得一提的是足疗和正宗的泰式按摩是露天设计的。</p>
                            <p><a class="button" href="facilities.php#swan_spa">閱讀更多<i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-swimming_pool.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">游泳池</h2>
                            <p class="description">在甲米奥南Deevana广场中心有一个大型泻湖风格的游泳池和按摩池。3个大型游泳池和一个舒适的儿童池。住在一楼的客人允许直接从他们的私人阳台上滑入温水，</p>
                            <p><a class="button" href="facilities.php#swimming_pool">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-restaurant.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">翠鸟餐厅</h2>
                            <p class="description">翠鸟是甲米奥南DEEVANA广场的招牌餐厅。在这里，客人可以享用不同风格的美食，并发现来自世界各地的烹饪口味和传统。食客可以在开放式自助早餐边或露天阳台上享受一个令人振奋的一天的开始与美味的亚洲和国际专业服务。</p>
                            <p><a class="button" href="facilities.php#pigeon_library_and_internet_corner">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title"><span class="deco-map deco-underline" style="display: inline-block;">KRABI 景點</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-hong_island.jpg" /></div>
                            <h2 class="title">HONG ISLAND</h2>
                            <a class="more" href="attraction.php#hong_island">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-tiger_cave_temple.jpg" /></div>
                            <h2 class="title">老虎洞寺庙</h2>
                            <a class="more" href="attraction.php#tiger_cave_temple">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-emerald_pool.jpg" /></div>
                            <h2 class="title">翡翠池</h2>
                            <a class="more" href="attraction.php#emerald_pool">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/thalay_wak.jpg" /></div>
                            <h2 class="title">Thalay Wak</h2>
                            <a class="more" href="attraction.php#thalay_wak">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/Asean_Green_Hotel_Standard.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/Asean_Mice_Venue_Standard.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/thma-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tceb-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/atta-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/nfi-awards.png" alt="" width="128" height="128"></li>
                    <li><a href="https://tourismawards.tourismthailand.org/ann_de?id=2" target="_blank"><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-award.png" alt="" width="128" height="128"></a></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<?php
if (!isset($_SESSION['visited'])) :
endif;
$_SESSION['visited'] = "true";
?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    #promotion_board img {
        *-webkit-transition: 150ms;
        *transition: 150ms;
    }
    #promotion_board img:hover {
        *-webkit-filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
        *filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php
include 'include/popup-image.php';
include '_footer.php';
?>
