<?php require_once('_functions.php'); ?>

<!DOCTYPE html>
<html lang="zh" class="<?php html_class(); ?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta property="og:image" content="http://www.deevanahotels.com/deevanaplazakrabi/images/home/home-slide-01.jpg" />
	<meta property="og:url" content="<?php echo get_info('url'); ?>" />
	<meta property="og:title" content="<?php echo get_info('name'); ?>" />
	<meta property="og:description" content="Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach." />

	<title><?php web_title(); ?></title>
    <?php web_desc(); ?>
	<?php web_keyw(); ?>

    <link rel="apple-touch-icon" sizes="57x57" href="assets/elements/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/elements/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/elements/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/elements/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/elements/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/elements/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/elements/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/elements/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/elements/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/elements/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/elements/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/elements/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/elements/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/elements/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/elements/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

	<meta name="google-site-verification" content="UmRmlyqknjntJVtP1REHk_4qNyRpdVKzk7hnVX2FJug" />

    <!--Utilities-->
    <link rel="stylesheet" href="assets/css/normalize.css" />
    <script src="assets/plugins/html5shiv/html5shiv.min.js"></script>
    <script src="assets/plugins/html5shiv/html5shiv-printshiv.min.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-grid.css'/>
    <!--jQuery-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script>

    <!--Validator-->
    <link rel="stylesheet" href="assets/plugins/form-validator/theme-default.min.css" />
    <script src="assets/plugins/form-validator/jquery.form-validator.min.js"></script>

    <!--JqueryTouch-->
    <script src="assets/js/jquery.touchSwipe.min.js"></script>

    <!--Debounce-->
    <script src="assets/js/jquery.ba-throttle-debounce.min.js"></script>

    <!--Resizeend-->
    <script src="assets/js/jquery.resizeend.min.js"></script>

    <!--KeepRatio-->
    <script src="assets/js/jquery.keep-ratio.min.js"></script>

    <!--OwlCarousel-->
    <link rel="stylesheet" href="assets/plugins/owl.carousel.2.0.0-beta.3/assets/owl.carousel.min.css" />
    <script src="assets/plugins/owl.carousel.2.0.0-beta.3/owl.carousel.min.js"></script>

    <!--Superslides-->
    <link rel="stylesheet" href="assets/plugins/superslides-0.6.2/dist/stylesheets/superslides.css" />
    <script src="assets/plugins/superslides-0.6.2/dist/jquery.superslides.min.js"></script>

    <!--MagnificPopup-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/magnific-popup.css" />
    <script src="assets/plugins/magnific-popup/magnific-popup.js"></script>

    <!--FooTable-->
    <link rel="stylesheet" href="assets/plugins/FooTable-2/css/footable.core.min.css" />
    <script src="assets/plugins/FooTable-2/dist/footable.min.js"></script>

    <!--Booking-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/js-cookie/latest/js.cookie.min.js'></script>
    <script src="assets/js/member.js"></script>
    <link rel="stylesheet" href="assets/plugins/booking-script/datepicker_theme.css" />
    <script src="assets/plugins/tl-booking-1.4.1/tl-booking.min.js"></script>

    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,300,500,500italic,700,700italic|Roboto+Condensed:300,300italic,400,400italic|Cinzel:400,700|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'/>

    <!--Core-->
    <link rel="stylesheet" href="assets/css/main.css?ver=<?php echo get_info('version'); ?>" />
    <script src="assets/js/main.js?ver=<?php echo get_info('version'); ?>"></script>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css" />
    <![endif]-->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PKNVGB5');</script>
    <!-- End Google Tag Manager -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81334137-1', 'auto', {'allowLinker': true});
        ga('require', 'linker');
        ga('linker:autoLink', ['travelanium.net'] );
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        
        fbq('init', '215754318902215');
        fbq('track', 'PageView');
        fbq('track', 'Lead');
    </script>
    <!-- End Facebook Pixel Code -->
</head>

<body class="<?php body_class(); ?>">
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=215754318902215&ev=PageView&noscript=1" /></noscript>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKNVGB5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <div id="page">
        <header class="site-header">
            <div class="container">
                <div class="site-branding">
                    <a href="<?php echo get_info('url'); ?>">
                        <img class="logo" src="assets/elements/logo.png" srcset="assets/elements/logo.png 1x, assets/elements/logo@2x.png 2x" width="160" height="65" />
                    </a>
                </div>

                <?php include_once('include/navigation.php'); ?>

                <div class="site-topbar">
                    <ul>
                        <li><i class="fa fa-fw fa-check-circle" aria-hidden="true"></i> BEST RATE GUARANTEE</li>
                        <li role="separator">|</li>
                        <li><a href="/zh"><i class="fa fa-fw fa-home" aria-hidden="true"></i> DEEVANA HOTELS</a></li>
                        <li role="separator">|</li>
                        <li>
                            <div class="lang" id="lang_select">
                                <a class="display"><i class="fa fa-fw fa-language" aria-hidden="true"></i> Chinese <i class="fa fa-angle-down"></i></a>
                                <ul class="list-lang">
                                    <li><a href="<?php echo $lang_en; ?>">English</a></li>
                                    <li><a href="<?php echo $lang_th; ?>">Thai</a></li>
                                    <li><a href="<?php echo $lang_zh; ?>">Chinese</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>

                <?php include 'include/member-panel.php'; ?>

                <a class="btn-menu" href="#" data-side-panel="toggle"><i class="fa fa-lg fa-bars" aria-hidden="true"></i></a>
                <a class="btn-home" href="/zh"><i class="fa fa-lg fa-home" aria-hidden="true"></i></a>
            </div>
        </header>