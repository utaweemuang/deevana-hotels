<?php
$title = 'Meetings and Event | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'meetings and event, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazakrabi/meetings-and-event.php';
$lang_th = '/th/deevanaplazakrabi/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazakrabi/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" width="1500" height="600" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">会议和活动</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#hornbill_grand_ballroom" class="tab active">犀鸟大宴会厅</span>
                    <span data-tab="#hornbill_ballroom_1" class="tab">犀鸟大宴会厅 1</span>
                    <span data-tab="#hornbill_ballroom_2" class="tab">犀鸟大宴会厅 2</span>
                    <span data-tab="#broadbill_meeting_room" class="tab">Broadbill会议室</span>
                    <span data-tab="#pita_biz_meeting_room" class="tab">Pitta Biz会议室</span>
                    <span data-tab="#meeting_room_dimension" class="tab">会议室尺寸</span>
                </div>
                
                <div class="tabs-content">
                    <article id="hornbill_grand_ballroom" class="article" data-tab-name="Hornbill Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_grand_ballroom.jpg" width="475" height="305" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">犀鸟大宴会厅</h1>
                                    <p>度假村的320平方米的犀鸟大宴会厅，泰国南部当代装饰适合最多300位客人坐在剧院式会议布局，300客人鸡尾酒会，180席宴会或150人晚餐 和舞蹈。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_1" class="article" data-tab-name="Hornbill Ballroom 1">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_1.jpg" width="475" height="305" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">犀鸟大宴会厅 1</h1>
                                    <p>犀鸟宴会厅1提供160平方米的空间，可容纳多达150位客人，采用剧院式会议布局，150人的鸡尾酒会，90人的宴会或70人的晚餐和舞蹈。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_2" class="article" data-tab-name="Hornbill Ballroom 2">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_2.jpg" width="475" height="305" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">犀鸟大宴会厅 2</h1>
                                    <p>犀鸟宴会厅2提供160平方米的空间，适合多达150位宾客坐在剧院式的会议布局，50人的鸡尾酒会，90座位的宴会，或70人的晚餐和舞蹈。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="broadbill_meeting_room" class="article" data-tab-name="Broadbill Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_broadbill_meeting_room.jpg" width="475" height="305" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Broadbill会议室</h1>
                                    <p>Broadbill会议室提供56平方米的空间，可容纳多达40位客人，采用剧院式会议布局，50人的鸡尾酒会，30人的宴会或35人的晚餐和舞蹈。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pita_biz_meeting_room" class="article" data-tab-name="Pita Biz Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_pitta_biz.jpg" width="475" height="305" />
                                </div>                            
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Pitta Biz会议室</h1>
                                    <p>Biz会议室可提供最多15位客人的行政会议，还可以使用日光，让您的活动成功放松。</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="meeting_room_dimension" class="article" data-tab-name="Meeting Room Dimension">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;会议室尺寸</h1>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>会议</th>
                                                <th>面积（平方米）</th>
                                                <th>天花高度（m）</th>
                                                <th data-hide="phone, tablet">表U形</th>
                                                <th data-hide="phone, tablet">样式矩形</th>
                                                <th data-hide="phone, tablet">风格剧院</th>
                                                <th data-hide="phone, tablet">风格教室</th>
                                                <th data-hide="phone, tablet">宴会</th>
                                                <th data-hide="phone, tablet">晚餐 &amp; 舞蹈</th>
                                                <th data-hide="phone, tablet">鸡尾酒</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>犀鸟大宴会厅</td>
                                                <td>320</td>
                                                <td>3.2</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>180</td>
                                                <td>200</td>
                                                <td>150</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>犀鸟宴会厅 1</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>犀鸟宴会厅 2</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Broadbill 会议 房间</td>
                                                <td>56</td>
                                                <td>2.8</td>
                                                <td>15</td>
                                                <td>-</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>-</td>
                                                <td>35</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>皮塔客房</td>
                                                <td>24</td>
                                                <td>2.8</td>
                                                <td>12</td>
                                                <td>-</td>
                                                <td>18</td>
                                                <td>-</td>
                                                <td>20</td>
                                                <td>-</td>
                                                <td>20</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                
                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
        
        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    #pita_biz_meeting_room {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>