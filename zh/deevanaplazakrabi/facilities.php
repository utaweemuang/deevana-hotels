<?php
$title = 'Facilities | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'facilities, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazakrabi/facilities.php';
$lang_th = '/th/deevanaplazakrabi/facilities.php';
$lang_zh = '/zh/deevanaplazakrabi/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/facilities-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">设备 &amp; 服务</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swan_spa" class="tab active">Orientala Spa</span>
                    <span data-tab="#argus_fitness" class="tab">阿格斯健身中心</span>
                    <span data-tab="#myna_kids_club" class="tab">八哥儿童俱乐部</span>
                    <span data-tab="#swimming_pool" class="tab">游泳池</span>
                    <span data-tab="#restaurant" class="tab">翠鸟餐厅</span>
                    <span data-tab="#pigeon_library_and_internet_corner" class="tab">鸽子图书馆和上网角</span>
                </div>
                
                <div class="tabs-content">
                    <article id="swan_spa" class="article" data-tab-name="Swan Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swan_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>典雅华贵的 Orientala Spa 邀请客人体验传统泰式按摩，包括芳香疗法和草药治疗。这是让你放松休息的完美场所，Orientala Spa 具有私人双人房的按摩浴缸和蒸汽房、2个独立单间，值得一提的是足疗和正宗的泰式按摩是露天设计的。</p>
                                    <p>Orientala Spa 提供锻炼身体、按摩服务等服务菜单，每一项都能有效的帮助身体治疗和放松。水疗中心的技术治疗师采取了最好的传统方法，并结合现代医疗科技创造一个卓越的健康技术。客人可以享受异国情调和放松的泰国草药油和其他卓越的保健产品，用于单项治疗或特殊的水疗套餐。</p>
                                    <p><span style="color: #516819;">Orientala Spa 让你拥有一段真正的放松和难忘的SPA之旅：营业时间从每天上午9点到晚上9点。地点：接待楼大堂</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="argus_fitness" class="article" data-tab-name="Argus Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_argus_fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">阿格斯健身中心</h1>
                                    <p>如果客人希望在度假期间保持身材，在甲米奥南Deevana广场可以免费使用设备齐全的健身中心。喜欢慢跑的在Noppharat Thara海滩晨跑可以欣赏沿途美丽的风光。</p>
                                    <p><span style="color: #516819;">阿格斯健身中心每天开放：从早上7点到晚上9点。地点：1号楼一楼</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="myna_kids_club" class="article" data-tab-name="Myna Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_myna_kid's_club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">八哥儿童俱乐部</h1>
                                    
                                    <p>Deevana广场甲米奥南邀请青少年前来丰富多彩和创意十足的八哥儿童俱乐部。4岁到12岁的孩子可以在训练有素的工作人员的关怀和注视下进行充满趣味的各种娱乐活动。还有电视、玩具和洋娃娃，以确保他们能玩得开心。</p>
                                    <p>孩子们也可以在特别的儿童游泳池和在室外的操场上玩，而妈妈和爸爸可以在这时充分的享受二人世界的时光。</p>
                                    <p><span style="color:#516819">八哥儿童俱乐部开放日：从上午9点到晚上6点。<br>
                                       地点：1号楼一楼（紧邻阿格斯健身）</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swimming_pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池</h1>
                                    <p>在甲米奥南Deevana广场中心有一个大型泻湖风格的游泳池和按摩池。3个大型游泳池和一个舒适的儿童池。住在一楼的客人允许直接从他们的私人阳台上滑入温水，并欢迎所有其他的度假酒店的客人再次享受从早到晚的美好时光。遮阳伞和舒适的太阳椅摆放的位置恰到好处，可以让你一整天躺在太阳椅上都不会受到阳光的直接照射。在旁边的太阳鸟泳池酒吧可以享用到可口的点心和美味的饭菜。</p>
                                    <p><span style="color: #516819;">游泳池每日开放时间:早上7点。- 晚上9点。</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
<article id="restaurant" class="article" data-tab-name="Restaurant & Bars">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_kingfisher_restaurant.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_sunbird_pool_bar.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_starling_lobby_lounge.jpg?ver=20160617" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">翠鸟餐厅</h1>
                                    <p>翠鸟是甲米奥南DEEVANA广场的招牌餐厅。在这里，客人可以享用不同风格的美食，并发现来自世界各地的烹饪口味和传统。食客可以在开放式自助早餐边或露天阳台上享受一个令人振奋的一天的开始与美味的亚洲和国际专业服务。广泛的午餐和晚餐的菜单提供了一个你最满意的选择，不管是泰国菜和国际菜。翠鸟的美味的菜肴和周到的服务，确保你有一个愉快的用餐体验。
烹饪：泰国菜和国际流行菜<br/>
每日开放时间：早上6点——晚上11点<br/>
地点：接待大楼一楼</p>

									<br/><br/><br/><br/><br/>
                                    <h1 class="title">太阳鸟池畔吧</h1>
                                    <p>太阳鸟池畔吧安静的坐落在泳池旁边，这里是一个绝佳的休息放松场所，在这里可以享受清凉的啤酒和饮料来为你消火降温，也可以从丰富的菜单上选择咖啡，茶等食品。美食:国际小吃和饮料。<br/>
每天开放:从上午10点到下午7点。<br/>
地点:游泳池边的建筑。</p>

									<br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h1 class="title">燕八哥大堂酒廊</h1>
                                    <p>伴随着热情好客的氛围，燕八哥大堂酒廊是一个抽时间陪伴家人和朋友的绝佳场所，无论是白天还是夜晚，随时都可以在这里享用美味的零食和饮料，我们可以花费很少的费用在自动点歌机点出自己喜爱的音乐让这个美妙的夜晚擦出更多的火花。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pigeon_library_and_internet_corner" class="article" data-tab-name="Pigeon Library &amp; Internet Corner">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pigeon_library_and_internet_corner.jpg?ver=20160525" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">鸽子图书馆和上网角</h1>
                                    <p>客人可以在度假胜地的储备丰富的图书馆享受安静的时刻。这里有广泛的书籍，本地和国际报纸和杂志。DVD也可以借走，到客房里舒适地观看。可以免费使用有互联网连接的计算机设备。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>