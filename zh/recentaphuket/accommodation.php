<?php
$title = 'Accommodaton | Recenta Phuket Suanluang l Official Hotel Group Website Thailand';
$desc = 'Accommodation: Guarantee best direct hotel rate starting from USD 30 per night; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'accommodation, recenta phuket, recenta suanluang, phuket, suan luang, Suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'accommodation room';
$cur_page = 'accommodation';

$lang_en = '/recentaphuket/accommodation.php';
$lang_th = '/th/recentaphuket/accommodation.php';
$lang_zh = '/zh/recentaphuket/accommodation.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/1500/room-01.jpg" alt="Room 01" />
                    <img src="images/accommodations/1500/room-02.jpg" alt="Room 02" />
                    <img src="images/accommodations/1500/room-03.jpg" alt="Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/600/room-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">房间</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/600/room-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">订房方式</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>我们的豪华房间有32平方米的空间，7英尺舒适的特大号床。私人卫浴，设备齐全，有免费高速互联网。</p>
                            <p><span style="color: #1a355e;">房间数量：66</span></p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">订房方式</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">房间设施</h2>
                                    <ul class="amenities-list">
                                       <li>带有独立遥控器的空调</li>
                                            <li>额外设备包括：身体护理套装、浴帽、棉签</li>
                                            <li>能收到有线频道的液晶电视</li>
                                            <li>有冷水和热水淋浴的私人浴室</li>
                                            <li>通用插头</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>设施</h2>
                                        <ul class="list-columns-2">
                                            <li>带有独立遥控器的空调</li>
                                            <li>额外设备包括：身体护理套装、浴帽、棉签</li>
                                            <li>能收到有线频道的液晶电视</li>
                                            <li>有冷水和热水淋浴的私人浴室</li>
                                            <li>通用插头</li>
                                            <li>伏的电压</li>
                                            <li>咖啡和茶设备</li>
                                            <li>两瓶免费饮用水</li>
                                            <li>吹风机</li>
                                            <li>保险箱</li>
                                            <li>免费无线网</li>
                                            <li>阳台/露台</li>
                                            <li>冰箱</li>
                                            <li>电水壶</li>
                                            <li>雨伞</li>
                                            <li>沙滩包</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>