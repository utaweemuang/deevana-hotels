<ul class="menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">家</a></li>
    <li class="<?php echo get_current_class('accommodation'); ?>"><a href="accommodation.php">住宿</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">設施</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">景點</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('294', 'zh'); ?>" target="_blank">提升</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">畫廊</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">聯繫</a></li>
</ul>
