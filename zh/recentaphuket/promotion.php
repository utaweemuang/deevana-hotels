<?php
$title = 'Promotion | Recenta Phuket Suanluang | Official Hotel Group Website Thailand';
$desc = 'Promotion: Guarantee best direct hotel rate starting from USD 30 per night; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'promotion, recenta phuket, recenta suanluang, phuket, suan luang, Suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'promotion';
$cur_page = 'promotion';

$lang_en = '/recentaphuket/promotion.php';
$lang_th = '/th/recentaphuket/promotion.php';
$lang_zh = '/zh/recentaphuket/promotion.php';

include_once('_header.php');
?>
        
<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section">
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url( get_info('ibeID'), 'en' ) ?>" target="_blank">
                                    <img class="force" src="images/promotion/special_rate.jpg" alt="Special Rate" />
                                </a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">SPECIAL RATE 10% Discount</h1>
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>In-room internet</li>
                                <li>Daily Shuttle Bus to Phuket Town</li>
                                <li>Daily tea and coffee in room</li>
                                <li>Early check-in at 11.00hrs</li>
                                <li>Late check-out until 14.00hrs</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes included<br>
                                    <span class="cost"><span class="discount">THB 1,080</span> per night</span><br>
                                    10% Discount
                                </h3>
                                <ul class="list-description">
                                    <li><span class="label">Room type:</span> Duluxe King Bed</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url( get_info('ibeID'), 'en' ) ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>
                
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<style>
    .site-main {
        background-image: url(images/promotion/bg-promotion.jpg);
        background-attachment: fixed;
        background-size: cover;
    }
    .article {
        background-color: #e1e1e1;
        margin-bottom: 30px;
        padding: 20px;
        color: #666;
        min-height: 190px;
    }
    .article .title {
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        font-weight: 700;
        color: #adc32b;
        margin-bottom: 15px;
    }
    .article .list-heading {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .article .list-heading + ul {
        font-size: 12px;
        margin-top: 3px;
    }
    .article .booking {
        background-color: #666;
        color: #cbcbcb;
        border-radius: 6px;
        padding: 12px;
        font-size: 12px;
    }
    .article .booking:after {
        content: '';
        display: block;
        clear: both;
    }
    .article .booking h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 12px;
    }
    .article .booking .discount {
        font-size: 22px;
        font-weight: 700;
        color: #bed62f;
    }
    .article .booking ul {
        list-style: none;
        margin-top: 5px;
        padding-left: 0;
    }
    .article .booking li {
        border-bottom: 1px dotted #cbcbcb;
        padding: 5px 0;
    }
    .article .booking .button {
        background-color: #8fc31c;
        background-image: linear-gradient(to bottom, #c0f844,#8fc31c);
        color: #fff;
        padding: 0 10px;
        line-height: 2;
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 14px;
        font-weight: 700;
    }
    @media (max-width: 720px) {
        .col-thumbnail,
        .col-info {
            width: 50%;
        }
        .col-booking {
            width: 100%;
        }
        .article .booking {
            margin-top: 20px;
            border-radius: 0;
        }
    }
    @media (max-width: 480px) {
        .col-thumbnail,
        .col-info {
            width: 100%;
        }
        .col-thumbnail {
            margin-bottom: 20px;
        }
        .col-booking .booking {
            border-radius: 0;
        }
        .article {
            padding: 12px;
        }
        .article .title {
            font-size: 24px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>