<?php

session_start();

$title = 'Recenta Style | Official Hotel Group Website Thailand';
$desc = '3 star chic hotel in Phuket town from USD 30 per night, enjoy best rate when book this city hotel directly.';
$keyw = 'Recenta Style phuket, Recenta Style, phuket, phuket city, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/recentastyle';
$lang_th = '/th/recentastyle';
$lang_zh = '/zh/recentastyle';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="http://www.deevanahotels.com/recentastyle/images/home/home-slide-01.jpg" /></div>
        </div>
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到普吉镇瑞森塔快捷酒店</span></span>
                        </h1>
                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" height="134" /></p>
                        <p>Recenta快捷酒店是一个能提供有限的服务的中级酒店。享受新的和现代紧凑型房间,Recenta快捷酒店旨在希望所有的客人可以享有安全、清洁、舒适的的体验，级舒适床包括五星级舒适度的床、所需的基本设施和难忘的服务，如乘客电梯、房间服务、电视、冰箱、空调、免费无线网络。</p>
                        <div class="clearfix"></div>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/_OrH30RLPWI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到普吉镇瑞森塔快捷酒店</span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
                        <p>Recenta快捷酒店是一个能提供有限的服务的中级酒店。享受新的和现代紧凑型房间,Recenta快捷酒店旨在希望所有的客人可以享有安全、清洁、舒适的的体验，级舒适床包括五星级舒适度的床、所需的基本设施和难忘的服务，如乘客电梯、房间服务、电视、冰箱、空调、免费无线网络。</p>
                        <div class="clearfix"></div>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('include/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/recentastyle/images/home/Re-Recenta-Style-update-02.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>BOOK DIRECT GET 10% DISCOUNT (10% DISCOUNT)</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>Late check-out by 14.00</li>
                                        <li>Welcome drink upon arrival</li>
                                        <li>Daily tea and coffee in room</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=310&onlineId=4&pid=MDcyNzI0OA%3D%3D&lang=zh&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/recentastyle/images/home/Re-Recenta-Style-update-01.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>BEST AVAILABLE RATE - ROOM ONLY</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>In-room internet</li>
                                        <li>Welcome drink upon arrival</li>
                                        <li>Daily tea and coffee in room</li>
                                        <li>Free Late check-out until 14.00 hrs.</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=310&onlineId=4&pid=MDYyNTkx&lang=zh&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#707270;">独一无二的全套设施满足您的所有需求</h1>

                <div id="activities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <!-- <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/slide-facilities/facility-daily_shuttle_bus_service.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">每日班车服务</h2>
                            <p class="description">带你到普吉镇的“PHA-THONG”，普吉岛的当地巴士.</p>
                            <p><a class="button" href="facilities.php#bus_service">阅读更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div> -->

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/slide-facilities/tour_desk.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">游泳池</h2>
                            <p class="description">家庭可以花时间在我们的成人和儿童尺寸的游泳池，或在池畔的太阳椅上社交。 当孩子们在水中飞溅时...</p>
                            <p><a class="button" href="facilities.php#tour_desk">阅读更多<i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/slide-facilities/facility-enjoy_our_service_at_all_hours.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">享受我们的服务在所有时间！</h2>
                            <p class="description">有用的工作人员，使您的访问有趣和容易我们的工作人员24小时提供满足您的需要</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title deco-underline"><span class="deco-map"><span style="color:#707270;">PHUKET</span>   景點</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-old_phuket_town.jpg" alt="Old Phuket Town"/></div>
                            <h2 class="title">芭东海滩</h2>
                            <a class="more" href="attraction.php#old_phuket_town">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" alt="Phromthep Cape"/></div>
                            <h2 class="title">神仙半岛</h2>
                            <a class="more" href="attraction.php#phromthep_cape">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phuket_walking_street.jpg" alt="Phuket Walking Street"/></div>
                            <h2 class="title">卡塔和卡伦海滩</h2>
                            <a class="more" href="attraction.php#phuket_walking_street">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" alt="Phuket Big Buddha"/></div>
                            <h2 class="title">天坛大佛</h2>
                            <a class="more" href="attraction.php#big_buddha">學到更多<i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-bronze-awards.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<style>
    #offers {
        background-color : #5e9205;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #111;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #707270;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #333;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #adadad;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });
        
        $.fn.calcMarginLeft = function() {
            var $this = $(this);
            
            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }
        
        $('.get-center').calcMarginLeft();
		
		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php include 'include/popup-image.php'; ?>
<?php include_once('_footer.php'); ?>