<div class="booking-bar">
    <div class="inner">
        <div class="container">
            <form id="booking-form" class="form">
                <span class="field field-header">
                    <img class="block responsive" src="assets/elements/book_online_best_rate_guarantee-cn.png" alt="Book Online Best rate guarantee" />
                </span>

                <span class="field field-checkin">
                    <input id="checkin" class="input-text date" placeholder="Check-in" type="text" readonly />
                </span>

                <span class="field field-checkout">
                    <input id="checkout" class="input-text date" placeholder="Check-out" type="text" readonly />
                </span>

                <span class="field field-adults">
                    <label class="label">成人</label>
                    <select id="adults" class="input-select">
                        <option value="1">1 成人</option>
                        <option value="2" selected>2 成人</option>
                        <option value="3">3 成人</option>
                        <option value="4">4 成人</option>
                        <option value="5">5 成人</option>
                        <option value="6">6 成人</option>
                        <option value="7">7 成人</option>
                        <option value="8">8 成人</option>
                        <option value="9">9 成人</option>
                        <option value="10">10 成人</option>
                        <option value="11">11 成人</option>
                        <option value="12">12 成人</option>
                    </select>
                </span>

                <span class="field field-children">
                    <label class="label">儿童</label>
                    <select id="children" class="input-select">
                        <option value="0" selected>0 儿童</option>
                        <option value="1">1 儿童</option>
                        <option value="2">2 儿童</option>
                        <option value="3">3 儿童</option>
                        <option value="4">4 儿童</option>
                        <option value="5">5 儿童</option>
                    </select>
                </span>

                <span class="field field-rooms">
                    <label class="label">客房</label>
                    <select id="rooms" class="input-select">
                        <option value="1" selected>1 客房</option>
                        <option value="2">2 客房</option>
                        <option value="3">3 客房</option>
                        <option value="4">4 客房</option>
                        <option value="5">5 客房</option>
                        <option value="6">6 客房</option>
                        <option value="7">7 客房</option>
                        <option value="8">8 客房</option>
                        <option value="9">9 客房</option>
                    </select>
                </span>

                <span class="field field-code">
                    <input id="accesscode" class="input-text" placeholder="Promo code" type="text" />
                </span>

                <span class="field field-submit">
                    <button id="submit" class="button" type="submit">立即预订</button>
                </span>
            </form>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#booking-form').booking({
            checkInSelector: '#checkin',
            checkOutSelector: '#checkout',
            adultSelector: '#adults',
            childSelector: '#children',
            roomSelector: '#rooms',
            validateSelector: '#hotel',
            propertyId: <?php echo get_info('ibeID'); ?>,
            onlineId: 5,
        });
    });
</script>
