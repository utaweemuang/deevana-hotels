<?php
$title = 'Accommodation | Recenta Style | Official Hotel Group Website Thailand';
$desc = 'Accommodation: 3 star chic hotel in Phuket town from USD 30 per night, enjoy best rate when book this city hotel directly.';
$keyw = 'accommodation, Recenta Style phuket, Recenta Style, phuket, phuket city, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'accommodation room';
$cur_page = 'accommodation';

$lang_en = '/recentastyle/accommodation.php';
$lang_th = '/th/recentastyle/accommodation.php';
$lang_zh = '/zh/recentastyle/accommodation.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/1500/room-01.jpg" alt="Recenta Style, 3-star hotel" />
                    <img src="images/accommodations/1500/room-02.jpg" alt="Recenta Style, 3-star hotel" />
                    <img src="images/accommodations/1500/room-03.jpg" alt="Recenta Style, 3-star hotel" />
                </div>
            </div>
            
            <div class="custom-hero-slide-nav"></div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/600/room-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">房间</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/600/room-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">房间预订方式</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>我们的标准房是21平方米，有非常舒适的床、设备完善的私人卫生间、免费的高速互联网</p>
                            <p><span style="color: #1a355e;">Rooms available: 46</span></p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">房间设施</h2>
                                    <ul class="amenities-list">
                                        <li>带有独立遥控器的空调</li>
                                        <li>额外设备包括：身体护理套装、浴帽、棉签</li>
                                        <li>能收到有线频道的液晶电视</li>
                                        <li>有冷水和热水淋浴的私人浴室</li>
                                        <li>通用插头</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>设施</h2>
                                        <ul class="list-columns-2">
                                            <li>带有独立遥控器的空调</li>
                                            <li>额外设备包括：身体护理套装、浴帽、棉签</li>
                                            <li>能收到有线频道的液晶电视</li>
                                            <li>有冷水和热水淋浴的私人浴室</li>
                                            <li>通用插头</li>
                                            <li>伏的电压</li>
                                            <li>吹风机</li>
                                            <li>保险箱</li>
                                            <li>免费无线网</li>
                                            <li>迷你吧台（冰箱</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>