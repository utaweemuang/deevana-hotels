<script>
    ;(function($) {
        /** Popup */
        var popup = {
            enabled: true,
            image: {
                src: 'http://www.ramadaphuketdeevana.com/images/banner/special-festive-package.jpg',
            },
            link: {
                enabled: true,
                href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=4&pid=MDg1MTk4&lang=zh',
                target: '_blank',
            }
        }
        if (popup.enabled) {
            $.magnificPopup.open({
                items: {
                    type: 'image',
                    src: popup.image.src,
                },
                callbacks: {
                    open: function() {
                        if (popup.link.enabled) {
                            $(this.content).find('.mfp-img').wrap('<a href="'+popup.link.href+'" target="'+popup.link.target+'" rel="noopener" />');
                            if (memberChecker()) {
                                memberAppendCode('member');
                            }
                        }
                    },
                },
            });
        }
    })(jQuery);
</script>