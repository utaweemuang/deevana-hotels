<?php
$title = 'Premier Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Premier Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'premier room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-premier-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-premier-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">

        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" />
                </div>
            </div>
        </div>

        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Premier Room <span>King size bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">贵宾房</h1>

                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />

                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>

                        <div class="col-w5 col-info">
                            <p>38平米的房间和七英尺的大床让你倍感自由，通过落地窗可以将具有热带风情的美景尽收眼底。暖色调的装饰和时尚别致的家具及设施让广大顾客赞不绝口。</p>
                            <p>清晨坐着宽敞的阳台上享受着可口的咖啡，为了晚上的外出狂欢在梳妆台前整理仪容。贵宾房每天都会额外免费赠送饮料和小吃，浴室还免费提供浴袍，洗漱包，剃须工具包还全套沐浴产品。</p>

                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>

                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>大床的床上用品</li>
                                        <li>4个枕头</li>
                                        <li>桌子和椅子的私人阳台</li>
                                        <li>47“LED电视</li>
                                        <li>卫星频道</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>大床的床上用品</li>
                                            <li>4个枕头</li>
                                            <li>桌子和椅子的私人阳台</li>
                                            <li>47“LED电视</li>
                                            <li>卫星频道</li>
                                            <li>LED台灯</li>
                                            <li>电子保险箱</li>
                                            <li>闹钟</li>
                                            <li>淋浴</li>
                                            <li>手电筒</li>
                                            <li>吹风机</li>
                                            <li>迷你冰箱</li>
                                            <li>梳妆镜</li>
                                            <li>熨斗</li>
                                            <li>写字台</li>
                                            <li>沙滩包</li>
                                            <li>浴袍和一次性拖鞋</li>
                                            <li>雨伞</li>
                                            <li>体重秤</li>
                                            <li>免费Wi Fi高速互联网</li>
                                            <li>免费的沐浴产品</li>
                                            <li>每天免费4瓶饮用水</li>
                                            <li>免费的迷你酒吧饮料和零食,每天补充</li>
                                            <li>免费的茶,每天补充</li>
                                            <!-- <li>免费新鲜的咖啡</li>
                                            <li>夜间打扫服务</li> -->
                                            <li>行李存放</li>
                                        </ul>
                                    </div>
                                </div>

                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>

            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>

    </div>
</main>

<?php include_once('_footer.php'); ?>
