<?php
$title = 'Facilities | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'facilities: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'facilities, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/ramadaphuketdeevana/facilities.php';
$lang_th = '/th/ramadaphuketdeevana/facilities.php';
$lang_zh = '/zh/ramadaphuketdeevana/facilities.php';

include_once('_header.php');
?>

<main class="site-main">

    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>

        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include('include/booking_bar.php'); ?>

    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">设备 &amp; 服务</h1>
            </header>

            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swimming_pool" class="tab active">游泳池</span>
                    <span data-tab="#pool_deck" class="tab">餐厅和酒吧</span>
                    <span data-tab="#fitness_centre" class="tab">健身中心</span>
                    <span data-tab="#kids_club" class="tab">孩子的俱乐部</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                    <span data-tab="#dining" class="tab">餐厅</span>
                    <span data-tab="#the_cafe" class="tab">咖啡馆</span>
                    <span data-tab="#bake_and_bev" class="tab">贝福烘培店</span>
                    <span data-tab="#room_service" class="tab">客房服务</span>
                </div>

                <div class="tabs-content">
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming_pool.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池</h1>
                                    <p>引起轰动的户外游泳池,它的特点是有一个专为孩子的浅水池和一些水流泡沫的按摩浴缸。泳池岸边为你准备了一排遮阳伞供你休息，旁边有一个清吧为你提供饮料和小吃让你保持体力神清气爽。</p>
                                    <p><span style="color: #516819;">开放:07.00到20.00</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="pool_deck" class="article" data-tab-name="Pool Deck">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool_deck.jpg" alt="Pool Deck" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">餐厅和酒吧</h1>
                                    <p>在休息室的旁边,这个能为你提供快乐的现代咖啡厅烘培店坐落在度假村花园里。这里全天候的为你服务。早餐是国际通用自助早餐，包括谷类食品、热带水果、面包类食品和一些煮熟的食品，多种多样的早餐同时符合亚洲和西方的口味。</p>
                                    <p><span style="color: #516819;">开放:10.00到19.00</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="fitness_centre" class="article" data-tab-name="Fitness Centre">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness_centre.jpg" alt="Fitness Centre" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">健身中心</h1>
                                    <p>健身中心的健身器材配备精良和五花八门的器材能使你的全身得到锻炼,健身房的位置为你提供了一个绝佳的角度能让你一边健身一边俯瞰泳池里的美好风光。所有健身人员必须至少16岁。</p>
                                    <p><span style="color: #516819;">开放:07.00到21.00</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="kids_club" class="article" data-tab-name="Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/kids_club.jpg" alt="Kid's Club" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">孩子的俱乐部</h1>
                                    <p>游泳池边的儿童俱乐部是一个能让孩子们愉快玩耍并结交新朋友的好地方。俱乐部有一个操场,配有各种玩具,能让孩子们尽情的嬉戏玩耍。所有在这里玩耍的儿童必须年龄在4 - 12岁之间。</p>
                                    <p><span style="color: #516819;">“开放:07.00到18.00</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/Orientala_wellness_spa.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>在Orientala Spa开始一个健康和放松的宁静旅程。温泉浴场有三个高雅的私人按摩房间,提供了蒸汽浴，全身按摩和其他多种治疗项目。本中心的金牌服务水疗项目是一个私人咨询专家治疗师运用火,空气、水和土等元素的融合对你进行治疗以使你达到独特的元素平衡,从而造成身心的和谐完美。</p>
                                    <p><span style="color: #516819;">开放:10.00到22.00</span></p>
                                    <!-- <p class="note">在2015年12月21日- 2月29日,2016年,水疗中心开放时间从10.00 - 20.00。</p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="dining" class="article" data-tab-name="Dining">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/dining.jpg" alt="Dining" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">餐厅</h1>
                                    <p>华美达普吉岛Deevana餐厅设有几个销售点以便让你享受具有泰国特色的和世界上时尚的知名的美食,你也可以叫客房服务，在自己的房间享用美食。</p>
                                    <p><span style="color: #516819;">Dinning is available during 06.30-23.00 hrs. (last order at 22.30 hrs.)
                                    <br>Buffet breakfast: opening during 06.30 – 10.30hrs.
                                    </span></p>
                                    <!-- <p><span style="color: #516819;">开放:07.00到20.00</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="the_cafe" class="article" data-tab-name="The Caf&eacute;">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/the_cafe.jpg" alt="The Caf&eacute;" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">咖啡馆</h1>
                                    <p>咖啡馆坐落在大堂,在这个现代化咖啡馆里你可以一边享用美食一边欣赏着度假村花园里的美丽风景。这里全天候的为你服务。早餐是国际通用自助早餐，包括谷类食品、热带水果、面包类食品和一些煮熟的食品，多种多样的早餐同时符合亚洲和西方的口味。</p>
                                    <p><span style="color: #516819;">Open: daily 06:30-23:00 hrs. (Last order at 22.30hrs)<br>Buffet breakfast: opening during 06:30-10:30 hrs.</span></p>
                                    <!-- <p><span style="color: #516819;">开放:早餐06.30到10.30</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="bake_and_bev" class="article" data-tab-name="Bake &amp; Bev">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/bake_and_bev.jpg" alt="Bake &amp; Bev" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">贝福烘培店</h1>
                                    <p>在这里选择的零食和食物可以直接送到你的房间,你可以在床上享用完美的早餐或者边看电影边享用来度过一个舒适的晚上。</p>
                                    <p><span style="color: #516819;">Open: daily 10.00 - 23.00 hrs. (Last order at 22.30hrs) </span></p>
                                    <!-- <p><span style="color: #516819;">开放:07.00到23.00</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="room_service" class="article" data-tab-name="ROOM SERVICE">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/room_service.jpg" alt="Room Service" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">客房服务</h1>
                                    <p>在这里选择的零食和食物可以直接送到你的房间,你可以在床上享用完美的早餐或者边看电影边享用来度过一个舒适的晚上。</p>
                                    <p><span style="color:#516819;">Open: 07.00-23.00. hrs. (Last order at 22.30hrs) </span></p>
                                    <!-- <p><span style="color:#516819;">开放:07.00到23.00</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
            </div>
        </section>
    </section>

</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;

        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #be0d3a;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
        overflow-x: auto;
        white-space: nowrap;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #be0d3a;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #be0d3a;
    }
    .col-cap p.note {
        background-color: #f5f5f5;
        padding: 5px 10px;
        font-size: 12px;
        display: inline-block;
        border-radius: 2px;
    }
    @media (max-width: 1024px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 600px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
