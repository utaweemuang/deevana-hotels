<?php
$title = 'Attractions | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'attractions: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'attractions, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'attraction';
$cur_page = 'attraction';

$lang_en = '/ramadaphuketdeevana/attraction.php';
$lang_th = '/th/ramadaphuketdeevana/attraction.php';
$lang_zh = '/zh/ramadaphuketdeevana/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#patong_beach">Patong Beach</li>
                            <li class="tab" data-tab="#phromthep_cape">Phromthep Cape</li>
                            <li class="tab" data-tab="#kata_and_karon_beaches">Kata and Karon Beaches</li>
                            <li class="tab" data-tab="#big_buddha">Big Buddha</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="patong_beach" data-tab-name="Patong Beach">
                        <img class="thumbnail force" src="images/attraction/patong_beach.png" alt="Patong Beach" />
                        <h1 class="title">芭东海滩</h1>
                        <p>巴东海滩以它的各种各样的活动和夜生活成为普吉岛最著名的海滩。到了晚上，镇上繁华的夜生活包括：数百个餐厅、酒吧和迪斯科舞厅。</p>
                    </article>
                    
                    <article class="article" id="phromthep_cape" data-tab-name="Phromthep Cape">
                        <img class="thumbnail force" src="images/attraction/phromthep_cape.png" alt="Phromthep Cape" />
                        <h1 class="title">神仙半岛</h1>
                        <p>神仙半岛，坐落在普吉岛的南端的顶部，这里能以一个最美丽的角度看日落，是普吉岛看日落的绝佳地点。橙色的阳光从蓝色的天空洒落在碧蓝的海面上，眼前的美景常常令人如痴如醉。</p>
                    </article>
                    
                    <article class="article" id="kata_and_karon_beaches" data-tab-name="Kata and Karon Beaches">
                        <img class="thumbnail force" src="images/attraction/karon_kata_beach.png" alt="Kata and Karon Beaches" />
                        <h1 class="title">卡塔和卡伦海滩</h1>
                        <p>卡塔和卡伦海滩在普吉岛上被称为最温暖、最干净，最如家庭般友好的地点。这些海滩是家庭出游中非常受欢迎，他们会选择在广阔的沙滩，在温暖的阳光下，享受他们的活动。五月到十月之间，冲浪者们喜欢来这里冲浪，而在十一月到四月直接，这里是充满阳光的海滩。</p>
                    </article>
                    
                    <article class="article" id="big_buddha" data-tab-name="Big Buddha">
                        <img class="thumbnail force" src="images/attraction/big_buddha_phuket.png" alt="Big Buddha" />
                        <h1 class="title">天坛大佛</h1>
                        <p>普吉岛的天坛大佛是岛上最重要和最受尊敬的地标之一。巨大的佛像矗立在卡塔和卡隆海滩之间的nakkerd山，高45米。我们能从很远的地方看到它。</p>
                        <p>这个高地提供了岛上最好的360度全景观景点（想想普吉镇、卡塔卡伦海滩，查龙寺等远景可以一览无余）可轻易通过16公里公路通往普吉岛的主要干道，这是普吉岛上一个必须参观的景点。</p>
                        <p>在靠近大佛的地方，这里非常安静，在这里你只会听到小铃铛被风吹动的丁呤声，黄色佛法佛旗在风中摇曳的声音和柔和悠扬的佛教音乐。</p>
                        <p>在泰国全称为Phra puttamingmongkol akenakkiri佛，它的基座有25米宽。整个身体是由分层的美丽的白色缅甸大理石铸就，在阳光照耀下，它成为一个希望的天然象征。佛像本身和周围的景色融为一体，看上去气势磅礴精彩绝伦。</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(t).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: 100%;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>