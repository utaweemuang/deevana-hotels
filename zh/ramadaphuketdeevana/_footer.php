        <footer class="site-footer">
            <style>
				.deevana_group2 { width: 100%; background-color: #c2c2c2; text-align: center; padding-top: 50px; padding-bottom: 50px; }
				.logo_our_1 { width: 100%;max-width: 980px; margin: 0 auto;height: 100px; text-align:left; vertical-align:middle }
				.logo_our_2 { width: 100%;max-width: 980px; margin: 0 auto;height: 100px; text-align:left; vertical-align:middle }

				.ft-bg-1 {width:75%;display: inline-block; background-color:#fff; padding: 12px 0% 9px 2.5%;}
				.ft-bg-2 {width:75%;display: inline-block; background-color:#fff; padding: 11px 0% 9px 2.5%;}
                
                /* .logo_main_ft { -webkit-filter: brightness(0) invert(1);filter: brightness(0) invert(1); } */
                .logo_main_ft:hover { -webkit-filter: none;filter: none; }

				.logo_main_devana {width:20%;display: block;  height: 90px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo_ft_deevana.png) no-repeat center center;background-size: contain; display: inline-block; vertical-align: middle;}
				.logo_main_spa {width:20%;display: block;  height: 83px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo_ft_spa.png) no-repeat center center;background-size: contain; display: inline-block; vertical-align: middle;}

				.logo-new-deevana-patong-resort-spa { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-patong-resort-spa-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-deevana-patong-resort-spa:hover{ -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-deevana-plaza-phuket { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-plaza-phuket-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-deevana-plaza-phuket:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-deevana-plaza-krabi { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-plaza-krabi-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-deevana-plaza-krabi:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-emeral { display: inline-block; width: 10%; height: 64px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/deevanakrabi.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-emeral:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-recenta { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-recenta.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-recenta:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-recenta2 { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-recenta2.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-recenta2:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-recenta3 { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanahotels.com/images/our_brands/recenta_express_phuket_town_sm.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-recenta3:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-ramada { display: inline-block; width: 9%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-ramada-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-ramada:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-orientala-spa { display: inline-block; width: 14%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-orientala-spa-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-orientala-spa:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
				.logo-new-orientala-wellness { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-orientala-wellness-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
				.logo-new-orientala-wellness:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}

                .brands .logo img { vertical-align: top; }
			</style>

            <div class="deevana_group2">
                <div class="logo_our_1">
                    <a href="http://www.deevanahotels.com/" target="_blank" class="logo_main_devana logo_main_ft"></a>
                    <div class="ft-bg-1">
                        <a href="http://www.deevanahotels.com/deevanaplazaphuket" target="_blank" class="logo-new-deevana-plaza-phuket"></a>
                        <a href="http://www.deevanahotels.com/deevanaplazakrabi" target="_blank" class="logo-new-deevana-plaza-krabi"></a>
                        <a href="http://www.deevanahotels.com/ramadaphuketdeevana" target="_blank" class="logo-new-ramada"></a>
                        <a href="http://www.deevanahotels.com/deevanapatong" target="_blank" class="logo-new-deevana-patong-resort-spa"></a>
                        <a href="http://www.deevanahotels.com/deevanakrabiresort" target="_blank" class="logo-new-emeral"></a>
                        <a href="http://www.deevanahotels.com/recentasuitephuket" target="_blank" class="logo-new-recenta"></a>
                        <a href="http://www.deevanahotels.com/recentaphuket" target="_blank" class="logo-new-recenta2"></a>
                        <a href="http://www.deevanahotels.com/recentastyle" target="_blank" class="logo-new-recenta3"></a>
                    </div>
                </div>
                <div class="logo_our_2">
                    <a href="http://www.orientalaspa.com/" target="_blank" class="logo_main_spa logo_main_ft"></a>
                    <div class="ft-bg-2">
                        <!-- <a href="http://www.orientalaspa.com/orientala-wellness-spa/" target="_blank" class="logo-new-orientala-wellness"></a> -->
                        <a href="https://www.orientalaspa.com" target="_blank" class="logo-new-orientala-spa"></a>
                    </div>
                </div>
            </div>

            <div class="footer-2 connect clearfix">
                <div class="container">
                    <div class="social left">
                        <span class="label">Let's be friend</span>
                        <span class="facebook"><a href="<?php echo get_info('facebook'); ?>" target="_blank" rel="nofollow"><i class="icon fa fa-facebook"></i></a></span>
                        <span class="instagram"><a href="<?php echo get_info('instagram'); ?>" target="_blank" rel="nofollow"><i class="icon fa fa-instagram"></i></a></span>
                    </div>

                    <div class="contact right">
                        <span><a href="contact.php">Contact</a></span>
                        <span><a href="https://reservation.travelanium.net/propertyibe2/booking-management?propertyId=278&onlineId=5&lang=zh" target="_blank">Manage Reservation</a></span>
                    </div>
                </div>
            </div>

            <div class="footer-3 colophon">
                <div class="container">
                    <div class="address">
                        <address class="address-info">
                            45/1 Raj‐U‐Thid 200 Pee Road, Patong, Kathu District Phuket, 83150 Thailand<br>
							Tel: +66 (0) 76 207 500 | Fax: +66 (0) 76 207 599 | Email: <a href="mailto:info@ramadaphuketdeevana.com">info@ramadaphuketdeevana.com</a>
                        </address>
                    </div>

                    <div class="license">
                        Hotel Web Design by <a href="http://www.travelanium.com/" target="_blank">Travelanium</a>
                    </div>
                </div>
            </div>
        </footer>

        <?php include 'include/side-social.php'; ?>
        <?php include_once('include/navigation-offside.php'); ?>
    </div>

    <!--
    # Google Code for Remarketing Tag
    Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories.
    See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
    -->
    <script type="text/javascript">
        var google_conversion_id = 877663496;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/877663496/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
</body>

</html>
