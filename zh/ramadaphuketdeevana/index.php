<?php

session_start();

$title = 'Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Enjoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea';
$keyw = 'deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/ramadaphuketdeevana';
$lang_th = '/th/ramadaphuketdeevana';
$lang_zh = '/zh/ramadaphuketdeevana';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01_cr.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>

        <!-- <div id="promotion_board" class="promotion get-center">
            <a href="<?php //ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">
                <img class="block responsive" src="images/home/ramada_discount.png" all="Ramada Phuket Deevnaam 4-star hotel" />
            </a>
        </div> -->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">WELCOME TO <br><span style="color:#c40032;">RAMADA BY WYNDHAM PHUKET DEEVANA PATONG</span></span>
                            <!-- <span style="font-size: 26px;">歡迎來到 <br><span style="color:#c40032;">瑞玛达普吉岛蒂瓦纳酒店</span></span> -->
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>一个洋溢着泰国魅力的现代化的度假村位于巴东海滩的心脏，一个充满活力的城市，游客选择最多的海滩目的地。游客在五分钟内漫步美丽的白沙海滩，周围有众多著名的旅游景点，充满活力的夜生活区和巨大的购物和娱乐商场就在其中。</p>
                        <p>华美达普吉岛Deevana是一个宁静和热情好客的地方期待着你的光临，这里帮你远离城市的喧嚣。在这里象艺术品遍布度假村的每一个角落和客房装饰，看起来喜庆而又有品位。华美达普吉岛Deevana巴东是无烟酒店,专为休闲和快乐的家庭而规定。度假村还配备了热带阳光下的室外游泳池，内置按摩浴缸的水疗中心以及孩子们的乐园。</p>
                        <p>在这里每天都可以吃到各种国际流行美食，可以驻足贝福烘烤店享用美味可口的点心和饮料。会议室承接各种宴会和商业活动。</p>
                        <!-- <p>A contemporary style hotel situated at the heart of Patong Beach, a lively city and beach destination in one. Surrounded by top attractions guests are within five minutes stroll of the beautiful white sand beach, vibrant nightlife district and huge shopping and entertainment mall to name just a few.</p>
                        <p>Ramada by Wyndham Phuket Deevana Patong is a serene escape from the city streets where tranquillity and warm hospitality awaits. The delightful elephant artwork throughout the resort will add a smile to your stay and the tastefully furnished guestrooms are full of Ramada by Wyndham Phuket Deevana Patong is non-smoking hotel, designed for leisure and pleasure with something for the whole family. Splash around under the tropical sunshine at the outdoor swimming pool with a built-in Jacuzzi and keep the little ones entertained at the kid's club while you indulge in a rejuvenating treatment at the wellness spa.</p>
                        <p>Dine on international cuisine at the all day restaurant and stop by the lobby Bake & Bev for light refreshments. Meeting rooms and business services are also provided for banquets and events.</p> -->
                        <p class="d-none"><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/imVmlDY3SHQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content col-w8">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">歡迎來到 <br><span style="color:#c40032;">瑞玛达普吉岛蒂瓦纳酒店</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>一个洋溢着泰国魅力的现代化的度假村位于巴东海滩的心脏，一个充满活力的城市，游客选择最多的海滩目的地。游客在五分钟内漫步美丽的白沙海滩，周围有众多著名的旅游景点，充满活力的夜生活区和巨大的购物和娱乐商场就在其中。</p>
                        <p>华美达普吉岛Deevana是一个宁静和热情好客的地方期待着你的光临，这里帮你远离城市的喧嚣。在这里象艺术品遍布度假村的每一个角落和客房装饰，看起来喜庆而又有品位。华美达普吉岛Deevana巴东是无烟酒店,专为休闲和快乐的家庭而规定。度假村还配备了热带阳光下的室外游泳池，内置按摩浴缸的水疗中心以及孩子们的乐园。</p>
                        <p>在这里每天都可以吃到各种国际流行美食，可以驻足贝福烘烤店享用美味可口的点心和饮料。会议室承接各种宴会和商业活动。</p>

                        <p class="d-none"><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>

                        <div class="row">
                            <div class="col-auto">
                                <div id="TA_certificateOfExcellence325" class="TA_certificateOfExcellence"><ul id="yEFh7dA4NI" class="TA_links j8i90iYgjFXa"><li id="V6r1oM" class="89MQD0VNgji"><a target="_blank" href="https://www.tripadvisor.com/Hotel_Review-g297930-d8275148-Reviews-Ramada_Phuket_Deevana-Patong_Kathu_Phuket.html"><img src="https://www.tripadvisor.com/img/cdsi/img2/awards/CoE2017_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=325&amp;locationId=8275148&amp;lang=zh&amp;year=2017&amp;display_version=2"></script>

                                <div id="TA_rated905" class="TA_rated"><ul id="9ADbaSwkU" class="TA_links inPcBnd"><li id="R9ucTKt" class="jPt04X8Vyf6X"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/badges/ollie-11424-2.gif" alt="TripAdvisor"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=905&amp;locationId=8275148&amp;lang=zh&amp;display_version=2"></script>
                            </div>
                            <div class="col-auto">
                                <div id="TA_selfserveprop616" class="TA_selfserveprop"><ul id="5atf3rrVZ" class="TA_links vMbqXvVhCy"><li id="JieTl4" class="z8fGeGpxtJ"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=616&amp;locationId=8275148&amp;lang=zh&amp;rating=true&amp;nreviews=0&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=false&amp;display_version=2"></script>
                            </div>
                        </div>
                    </div>

                    <div class="col-countdown col-w4">
                    	<a href="./fb-promotion.php"><img class="responsive" src="./images/002 Promotion.jpg" alt="Cooking-class" width="420" height="630" /></a>
                    </div>
                </div> -->
            </div>
        </section>
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.ramadaphuketdeevana.com/images/promotion/Loykratong.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Loy Krathong Package</b></h2>
                                    <p class="description" style="color:yellow;">Mimimum 1 night Stay</p>
                                    <ul>
                                        <li>FREE One time dinner set at The Café' Restaurant per stay for 2 people (Stay during 11th November, dinner shall be taken on 11th November)</li>
                                        <li>FREE Hotel Souvenir</li>
                                        <li>10% discount on Food and Beverage at hotel restaurant except alcohol</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=5&pid=MDg1MDg1&lang=zh">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.ramadaphuketdeevana.com/images/promotion/Honeymoon-package-600x400-100.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Honeymoon Package 4D3N</b></h2>
                                    <p class="description" style="color:yellow;">Minimum stay 3 nights</p>
                                    <ul>
                                        <li>Round-trip Airport transfer</li>
                                        <li>One time Honeymoon dinner set </li>
                                        <li>Complimentary a bottle of local beer or a glass of Ramada's mocktail per couple per stay</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=4&pid=MDg0NzIz">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.ramadaphuketdeevana.com/images/promotion/Direct-Deals.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>DIRECT DEAL WITH BREAKFAST</b></h2>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>10% discount on Food and Beverage at hotel restaurant except alcohol</li>
                                        <li>Early check-in and Late check-out until 16.00hrs (Subject to room available.)</li>
                                        <li>Free one child under 12 years stays free of charge when using existing beds</li>
                                        <li>Ramada Welcome Drink at Bake & Bev</li>
                                        <li>Stay Min 3 nights, Get additional: Thai massage for one hour – two people, Get 20% discount on laundry service</li>
                                        <li>Stay Min 3nights - FREE transfer-in from Phuket international airport to hotel, guest must be booked exclusively by direct website ONLY</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=4&pid=MDcyMzUzOQ%3D%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.ramadaphuketdeevana.com/images/banner/Happy-Holiday-450x300.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Happy Holiday</b></h2>
                                    <ul>
                                        <li>In-room internet</li>
                                        <li>10% discount on Food and Beverage at hotel restaurant except alcohol</li>
                                        <li>Early check-in and Late check-out until 16.00hrs (Subject to room available.)</li>
                                        <li>Free one child under 12 years stays free of charge when using existing beds</li>
                                        <li>Ramada Welcome Drink at Bake & Bev</li>
                                        <li>Stay Min 3 nights, Get additional: Thai massage for one hour – two people, Get 20% discount on laundry service</li>
                                        <li>Stay Min 3nights - FREE transfer-in from Phuket international airport to hotel, guest must be booked exclusively by direct website ONLY</li>
                                        <li>No recurring</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=4&pid=MDcxODM5MnwwNzIzNTM5">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#c40032;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="activities_slider" class="owl-carousel has-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-swimming_pool.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">游泳池</h2>
                            <p class="description">引起轰动的户外游泳池,它的特点是有一个专为孩子的浅水池和一些水流泡沫的按摩浴缸。泳池岸边为你准备了一排遮阳伞供你休息，旁边有一个清吧为你提供饮料和小吃让你保持体力神清气爽。</p>
                            <p><a class="button" href="facilities.php#swimming_pool">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-orientala_wellness_spa.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">Orientala Spa</h2>
                            <p class="description">在Orientala Spa开始一个健康和放松的宁静旅程。温泉浴场有三个高雅的私人按摩房间,提供了蒸汽浴，全身按摩和其他多种治疗项目。本中心的金牌服务水疗项目是一个私人咨询专家治疗师运用火,空气、水和土等元素的融合对你进行治疗以使你达到独特的元素平衡,从而造成身心的和谐完美。</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-pool_deck.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">餐厅和酒吧</h2>
                            <p class="description">在休息室的旁边,这个能为你提供快乐的现代咖啡厅烘培店坐落在度假村花园里。这里全天候的为你服务。早餐是国际通用自助早餐，包括谷类食品、热带水果、面包类食品和一些煮熟的食品，多种多样的早餐同时符合亚洲和西方的口味。</p>
                            <p><a class="button" href="facilities.php#pool_deck">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title"><span class="deco-map deco-underline" style="display: inline-block;"><span style="color:#c40032;">PHUKET</span> 景點</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" /></div>
                            <h2 class="title">芭东海滩</h2>
                            <a class="more" href="attraction.php#patong_beach">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" /></div>
                            <h2 class="title">神仙半岛</h2>
                            <a class="more" href="attraction.php#phromthep_cape">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" /></div>
                            <h2 class="title">卡塔和卡伦海滩</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" /></div>
                            <h2 class="title">天坛大佛</h2>
                            <a class="more" href="attraction.php#big_buddha">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-silver-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/halal-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/thai-food-awards.png" alt="" width="128" height="128"></li>
                    <li><a href="http://www.ramadaphuketdeevana.com/images/awards/thailand-tourism-award-3.jpg" target="_blank"><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-award.png" alt="" width="128" height="128"></a></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<style>
    #offers {
        background-color : #8e1b34;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        *width: 100%;
        *padding-right: 340px;
    }
    .row-intro .col-countdown {
        *position: absolute;
        *top: 0;
        *right: 15px;
        *width: 290px;
		margin-top: 70px;
    }
	.row-intro .col-countdown img {
		opacity: 1;
		-webkit-transition: 200ms;
		transition: 200ms;
	}
	.row-intro .col-countdown a:hover img {
		opacity: 0.85;
	}
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #c40032;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #c40032;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #333;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #c40032;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            *float: none;
        }

        .row-intro .col-content {
            *padding-right: 10px;
        }

        .row-intro .col-countdown {
            *position: static;
            *margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
	@media (max-width: 540px) {
		.row-intro > [class*="col-"] {
			width: 100%;
			margin-top: 0;
		}
		.row-intro .col-countdown img {
			display: block;
			margin: auto;
		}
	}
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}

        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<?php include 'include/popup-image.php'; ?>
<?php include_once('_footer.php'); ?>
