<?php
$title = 'Junior Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Junior Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'junior room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-room';
$cur_page = 'junior-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-junior-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-junior-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-junior-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/junior-suite/1500/junior-suite-01.jpg" alt="Junior Room 01" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-02.jpg" alt="Junior Room 02" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-03.jpg" alt="Junior Room 03" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-04.jpg" alt="Junior Room 04" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-05.jpg" alt="Junior Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Junior Suite <span>King size bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior-suite/600/junior-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">普通套房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior-suite/600/junior-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>置身于56平米的宽敞豪华的套房之中充分享受这种异域风情。房间里时髦光鲜的家具，宽大明亮的落地窗外就是属于你自己的私人阳台。</p>
                            <p>休息室包括一个舒适的沙发和超大47“LED电视和卫星娱乐频道。卧室有一个6英尺特大号床,几个内嵌式储物柜方便你存储大量的个人物品。房间设施的最大亮点是在阳台上的长椅和浴缸，这为你提供了更多美好而又浪漫的时光。</p>
                            <p>套房每天都会为你在小吧台里补充免费的饮品，还有咖啡机，大冰箱供你使用，浴室里还免费提供浴袍浴巾拖鞋和全套的沐浴产品。</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>大床的床上用品</li>
                                        <li>4个枕头</li>
                                        <li>私人阳台的浴缸</li>
                                        <li>47“LED电视</li>
                                        <li>卫星频道</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>大床的床上用品</li>
                                            <li>4个枕头</li>
                                            <li>私人阳台的浴缸</li>
                                            <li>47“LED电视</li>
                                            <li>卫星频道</li>
                                            <li>LED台灯</li>
                                            <li>电子保险箱</li>
                                            <li>闹钟</li>
                                            <li>淋浴</li>
                                            <li>手电筒</li>
                                            <li>吹风机</li>
                                            <li>冰箱</li>
                                            <li>梳妆镜</li>
                                            <li>熨斗</li>
                                            <li>写字台</li>
                                            <li>沙滩包</li>
                                            <li>浴袍和一次性拖鞋</li>
                                            <li>雨伞</li>
                                            <li>体重秤</li>
                                            <li>免费Wi Fi高速互联网</li>
                                            <li>免费的全套沐浴产品</li>
                                            <li>每天免费4瓶饮用水</li>
                                            <li>免费的迷你酒吧饮料和零食,每天补充</li>
                                            <li>免费的茶和设施,每天补充</li>
                                            <li>免费新鲜的咖啡</li>
                                            <li>浓缩咖啡机</li>
                                            <li>夜间打扫服务</li>
                                            <li>行李存放</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>