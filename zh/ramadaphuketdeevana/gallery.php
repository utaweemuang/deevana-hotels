<?php
$title = 'Gallery | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Gallery: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'gallery, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$body_class = 'gallery';
$cur_page = 'gallery';

$lang_en = '/ramadaphuketdeevana/gallery.php';
$lang_th = '/th/ramadaphuketdeevana/gallery.php';
$lang_zh = '/zh/ramadaphuketdeevana/gallery.php';

include_once( '_header.php' );
?>

<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section section-gallery">
                
                <div class="gal-types">
                    <span class="tab active" data-tab="#photo"><i class="icon fa fa-picture-o"></i>PHOTOS</span>
                    <span class="tab" data-tab="#video"><i class="icon fa fa-film"></i>VIDEO</span>
                </div>
                
                <div class="gal-conts">
                    <section id="photo" class="gal-cont">
                        <div class="cat-tabs">
                            <span class="tab active" data-cat="*">ALL</span>
                            <span class="tab" data-cat=".rooms">ROOMS</span>
                            <span class="tab" data-cat=".cafe">CAFE</span>
                            <span class="tab" data-cat=".restaurant">RESTAURANT</span>
                            <span class="tab" data-cat=".spa">SPA</span>
                            <span class="tab" data-cat=".facilities">FACILITIES</span>
                            <span class="tab" data-cat=".awards">AWARDS</span>
                        </div>
                        
                        <div class="content">
                            <div class="masonry-gal">
                                <div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/gal-01.jpg"><img src="images/gallery/set-02/restaurant/600/gal-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/gal-02.jpg"><img src="images/gallery/set-02/restaurant/600/gal-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/gal-03.jpg"><img src="images/gallery/set-02/restaurant/600/gal-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/gal-04.jpg"><img src="images/gallery/set-02/restaurant/600/gal-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-02.jpg"><img src="images/gallery/set-01/600/gal-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-03.jpg"><img src="images/gallery/set-01/600/gal-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-04.jpg"><img src="images/gallery/set-01/600/gal-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-01.jpg"><img src="images/gallery/set-02/room/600/gal-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-02.jpg"><img src="images/gallery/set-02/room/600/gal-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-03.jpg"><img src="images/gallery/set-02/room/600/gal-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-04.jpg"><img src="images/gallery/set-02/room/600/gal-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-05.jpg"><img src="images/gallery/set-02/room/600/gal-05.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set-02/room/1500/gal-06.jpg"><img src="images/gallery/set-02/room/600/gal-06.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-08.jpg"><img src="images/gallery/set-01/600/gal-08.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-09.jpg"><img src="images/gallery/set-01/600/gal-09.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-10.jpg"><img src="images/gallery/set-01/600/gal-10.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-11.jpg"><img src="images/gallery/set-01/600/gal-11.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-12.jpg"><img src="images/gallery/set-01/600/gal-12.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-13.jpg"><img src="images/gallery/set-01/600/gal-13.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-14.jpg"><img src="images/gallery/set-01/600/gal-14.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-15.jpg"><img src="images/gallery/set-01/600/gal-15.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-16.jpg"><img src="images/gallery/set-01/600/gal-16.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-01/1500/gal-17.jpg"><img src="images/gallery/set-01/600/gal-17.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-01.jpg"><img src="images/gallery/set-02/spa/600/spa-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-02.jpg"><img src="images/gallery/set-02/spa/600/spa-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-03.jpg"><img src="images/gallery/set-02/spa/600/spa-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-04.jpg"><img src="images/gallery/set-02/spa/600/spa-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-05.jpg"><img src="images/gallery/set-02/spa/600/spa-05.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-06.jpg"><img src="images/gallery/set-02/spa/600/spa-06.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item spa"><a href="images/gallery/set-02/spa/1500/spa-07.jpg"><img src="images/gallery/set-02/spa/600/spa-07.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-01.jpg"><img src="images/gallery/set-02/cafe/600/cafe-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-02.jpg"><img src="images/gallery/set-02/cafe/600/cafe-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-03.jpg"><img src="images/gallery/set-02/cafe/600/cafe-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-04.jpg"><img src="images/gallery/set-02/cafe/600/cafe-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-05.jpg"><img src="images/gallery/set-02/cafe/600/cafe-05.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-06.jpg"><img src="images/gallery/set-02/cafe/600/cafe-06.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-07.jpg"><img src="images/gallery/set-02/cafe/600/cafe-07.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item cafe"><a href="images/gallery/set-02/cafe/1500/cafe-08.jpg"><img src="images/gallery/set-02/cafe/600/cafe-08.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								
								<div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/restaurant-01.jpg"><img src="images/gallery/set-02/restaurant/600/restaurant-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/restaurant-02.jpg"><img src="images/gallery/set-02/restaurant/600/restaurant-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/restaurant-03.jpg"><img src="images/gallery/set-02/restaurant/600/restaurant-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								<div class="item restaurant"><a href="images/gallery/set-02/restaurant/1500/restaurant-04.jpg"><img src="images/gallery/set-02/restaurant/600/restaurant-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
								
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-01.jpg"><img src="images/gallery/set-02/facilities/600/facilities-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-02.jpg"><img src="images/gallery/set-02/facilities/600/facilities-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-03.jpg"><img src="images/gallery/set-02/facilities/600/facilities-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-04.jpg"><img src="images/gallery/set-02/facilities/600/facilities-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-05.jpg"><img src="images/gallery/set-02/facilities/600/facilities-05.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set-02/facilities/1500/facilities-06.jpg"><img src="images/gallery/set-02/facilities/600/facilities-06.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>

                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-01.jpg"><img src="images/gallery/set-02/awards/600/awards-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-02.jpg"><img src="images/gallery/set-02/awards/600/awards-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-03.jpg"><img src="images/gallery/set-02/awards/600/awards-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-04.jpg"><img src="images/gallery/set-02/awards/600/awards-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-05.jpg"><img src="images/gallery/set-02/awards/600/awards-05.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-06.jpg"><img src="images/gallery/set-02/awards/600/awards-06.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-07.jpg"><img src="images/gallery/set-02/awards/600/awards-07.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-08.jpg"><img src="images/gallery/set-02/awards/600/awards-08.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-09.jpg"><img src="images/gallery/set-02/awards/600/awards-09.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-10.jpg"><img src="images/gallery/set-02/awards/600/awards-10.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/awards-11.jpg"><img src="images/gallery/set-02/awards/600/awards-11.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/ctrip-1.jpg"><img src="images/gallery/set-02/awards/600/ctrip-1.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/Kinnari.jpg"><img src="images/gallery/set-02/awards/600/Kinnari.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                                <div class="item awards"><a href="images/gallery/set-02/awards/1500/labour.jpg"><img src="images/gallery/set-02/awards/600/labour.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></a></div>
                            </div>
                        </div>
                    </section>
                    
                    <section id="video" class="gal-cont">
                        <div class="content">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="ratio-wide">
                                        <iframe class="item" width="560" height="315" src="https://www.youtube.com/embed/imVmlDY3SHQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<!--Isotopr-->
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
    $(function() {
        var $gCont = $('.gal-conts');
        var $gType = $('.gal-types');
        var $ctabs = $('.cat-tabs');
        
        function masonryFX( cat = '*' ) {
            var $mgal = $('.masonry-gal');
            $mgal.isotope({
                itemSelector: '.item',
                filter: cat,
            }).imagesLoaded().progress( function() {
                $mgal.isotope('layout');
            });
        }
        
        function galleryFX( cat = '.item' ) {
            $gCont.find(cat).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [1, 2],
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('gallery-open');
                    },
                    close: function() {
                        $('html').removeClass('gallery-open');
                    },
                },
                removalDelay: 300,
                mainClass: 'mfp-fade',
            });
        }
        
        // Define active tab
        var active = $gType.find('.active').data('tab');
        $gCont.find( active ).show();
        
        $gType.on('click', '.tab', function(e) {
            e.preventDefault();
			if( $(this).hasClass('active') ) return;
			
            var $this = $(this);
            var data = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $gCont.find(data).fadeIn(300).siblings().hide();
            
            masonryFX();
        });
        
        $ctabs.on('click', '.tab', function(e) {
            var $this = $(this);
            var cat = $this.data('cat');
            $this.addClass('active').siblings().removeClass('active');
            
            galleryFX(cat);
            masonryFX(cat);
        });
        
        galleryFX();
        masonryFX();
    });
</script>

<style>
    .site-main {
        background-image: url(images/gallery/bg-gallery.jpg);
        background-attachment: fixed;
        background-position: center;
        background-size: cover;
    }
    .gal-conts {
        width: 100%;
        clear: both;
        background-color: #fff;
        margin-bottom: 30px;
        border-top: 2px solid #C40032;
        position: relative;
    }
    .gal-cont {
        display: none;
        position: relative;
    }
    .gal-cont .content {
        overflow: hidden;
        padding: 10px 10px 0;
    }
    .gal-types,
    .cat-tabs {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
    }
    .gal-types {
        float: right;
    }
    .gal-types .tab {
        background-color: #fff;
        color: #333;
        border-radius: 3px 3px 0 0;
        display: inline-block;
        text-align: center;
        padding-top: 10px;
        width: 70px;
        height: 50px;
    }
    .gal-types .tab.active {
        background-color: #C40032;
        color: #fff;
    }
    .gal-types .tab .icon {
        display: block;
    }
    .cat-tabs {
        position: absolute;
        top: -30px;
        left: 0;
    }
    .cat-tabs .tab {
        background-color: transparent;
        color: #333;
        line-height: 28px;
        font-size: 16px;
        padding: 0 8px;
        display: inline-block;
        border-radius: 3px 3px 0 0;
    }
    .cat-tabs .tab.active {
        background-color: #C40032;
        color: #fff;
    }
    .masonry-gal {
        margin-left: -5px;
        margin-right: -5px;
    }
    .masonry-gal .item {
        width: 33.33%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .masonry-gal .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    #video {
        padding-bottom: 10px;
    }
    
    html .site-header {
        transition-property: background-color, height, right;
        transition-duration: 300ms, 300ms, 0ms;
    }
    html.gallery-open .site-header {
        right: 17px;
    }
    @media (max-width: 640px) {
        .section-gallery {
            padding-top: 0;
        }
        .masonry-gal .item {
            width: 50%;
        }
        .gal-conts {
            margin-top: 42px;
        }
        .gal-types {
            float: none;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        .gal-types:after {
            content: '';
            display: block;
            clear: both;
        }
        .gal-types .tab {
            width: 50%;
            float: left;
            padding: 6px;
            height: auto;
            border-radius: 0;
        }
        .gal-types .tab .icon {
            display: inline-block;
            margin-right: 3px;
        }
        .cat-tabs {
            overflow-x: auto;
            display: flex;
            flex-flow: row nowrap;
            background-color: #fff;
            top: -34px;
            left: 0;
            right: 0;
            border-radius: 3px 3px 0 0;
        }
        .cat-tabs .tab {
            border-radius: 0;
            font-size: 12px;
            line-height: 32px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>