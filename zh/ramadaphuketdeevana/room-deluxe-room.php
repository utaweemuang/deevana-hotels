<?php
$title = 'Deluxe Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'deluxe room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-deluxe-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-deluxe-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">豪华房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>富有魅力的35平方米豪华房间，有大床房和双床房两种房型，配有一把舒适的椅子，一张咖啡桌和一面梳妆镜。天然的气氛灯营造出一种轻松的氛围，延伸出去的户外阳台让人能放松的享受这里温暖的气候。</p>
                            <p>茶点中心包括一个迷你冰箱和热饮设施，墙上安装平板电视有方便的遥控器来浏览卫星频道。有一个内嵌式的保险箱来储存笔记本等私人物品和财物，让你能放心的出门游玩。</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>大床或双床的床上用品</li>
                                        <li>4个枕头</li>
                                        <li>桌子和椅子的私人阳台</li>
                                        <li>42寸液晶电视</li>
                                        <li>卫星频道</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>大床或双床的床上用品</li>
                                            <li>4个枕头</li>
                                            <li>桌子和椅子的私人阳台</li>
                                            <li>42寸液晶电视</li>
                                            <li>卫星频道</li>
                                            <li>LED台灯</li>
                                            <li>电子保险箱</li>
                                            <li>闹钟</li>
                                            <li>淋浴</li>
                                            <li>手电筒</li>
                                            <li>吹风机</li>
                                            <li>迷你冰箱</li>
                                            <li>梳妆镜</li>
                                            <li>熨斗</li>
                                            <li>写字台</li>
                                            <li>沙滩包</li>
                                            <li>浴袍和一次性拖鞋</li>
                                            <li>雨伞</li>
                                            <li>免费Wi Fi高速互联网</li>
                                            <li>免费的沐浴产品</li>
                                            <li>每天免费2瓶饮用水</li>
                                            <li>免费的咖啡和茶设施,每天补充</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>