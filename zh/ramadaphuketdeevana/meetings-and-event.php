<?php
$title = 'Meetings and Event | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'meetings and event, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/ramadaphuketdeevana/meetings-and-event.php';
$lang_th = '/th/ramadaphuketdeevana/meetings-and-event.php';
$lang_zh = '/zh/ramadaphuketdeevana/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            
            <header class="section-header">
                <div class="container">
                    <h1 class="section-title">会议和事件</h1>
                    <p class="description">华美达普吉岛Deevana的时尚的会议室是公司活动,商务会议、宴会、研讨会、派对等活动的理想场所。会议室可以容纳90人举行会议并举办宴会，教室可以容纳63人也可以分开举办各种小型会议。 这里拥有先进的视听设备,有多种美食点心供你选择。</p>
                </div>
            </header>
            
            <article class="article">
                <div class="container">
                    <div class="row">
                        <div class="col-w5 col-pic">
                            <img class="force thumbnail" src="images/meetings/meeting_room.jpg" alt="Meeting Room" />
                        </div>
                        
                        <div class="col-w7 col-cap">
                            <h1 class="title">其他设施</h1>
                            <ul>
                                <li>互联网角落——位于大堂旁边有24小时免费上网服务</li>
                                <li>免费的wi - fi服务,覆盖度假村的所有地区</li>
                                <li>洗衣服务——快速可靠（收取一定额外的服务费用）</li>
                                <li>提供55个室内安全的汽车停车位</li>
                                <li>安全措施,24小时保安和各领域的闭路电视摄影机</li>
                            </ul>
                        </div>

                        <div class="col-w12" style="padding:15px 0;text-align:center;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/b-shape.jpg" alt="" width="1050" height="713"></div>
                        <div class="col-w12" style="padding:15px 0;text-align:center;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/u-shape.jpg" alt="" width="1050" height="713"></div>
                    </div>
                </div>
            </article>
            
        </section>
    </section>
        
</main>

<style>
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #222;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
	.col-cap .sub-title {
		font-size: 14px;
		margin-top: 1em;
		margin-bottom: 3px;
	}
    @media (max-width: 480px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>