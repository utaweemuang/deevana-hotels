<?php
$title = 'Deluxe Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room | Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'deluxe room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-deluxe-room.php';
$lang_th = '/th/deevanaplazaphuket/room-deluxe-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" width="1500" height="1000" />
                    <img src="images/accommodations/deluxe/1500/deluxe-02.jpg" alt="Deluxe Room 02" width="1500" height="1000" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" width="1500" height="1000" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="400" width="600" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-02.jpg" height="400" width="600" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="400" width="600" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">豪华房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>这里为个人或夫妻提供了一个宁静的港湾，豪华客房功能齐全，空间灵活，无论是室内和室外都能度过一个悠闲的下午。有双人大床房和双床房两种选择，从房间的私人阳台上可以看到芭东市或Deevana广场普吉岛池景。以白色为主的内部空间里，在浴室内透过半高的玻璃墙向外望去，室外风景在墙纸上不断变幻，形成难以想象的图案。</p>
                            <p>
                                房间可用：209<br/>
                                面积：35平方米
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">豪华房的</h2>
                                    <ul class="amenities-list">
                                        <li>特大号床(6英尺)或两张单人床(4英尺)</li>
                                        <li>42英寸液晶电视</li>
                                        <li>生活的椅子</li>
                                        <li>免费的无线网络</li>
                                        <li>国际直拨电话和语音邮件</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>房间里的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>特大号床(6英尺)或两张单人床(4英尺)</li>
                                            <li>42英寸液晶电视</li>
                                            <li>生活的椅子</li>
                                            <li>免费的无线网络</li>
                                            <li>国际直拨电话和语音邮件</li>
                                            <li>泳池或城市的景观</li>
                                            <li>齐全的迷你酒吧</li>
                                            <li>免费的茶和咖啡</li>
                                            <li>烟雾报警器、探测器</li>
                                            <li>喷水灭火系统</li>
                                            <li>电子保险箱</li>
                                            <li>闹钟</li>
                                            <li>台灯</li>
                                            <li>水壶或咖啡机</li>
                                            <li>通用电源插座</li>
                                        </ul>
                                        
                                        <h2>浴室配件</h2>
                                        <ul class="list-columns-2">
                                            <li>吹风机</li>
                                            <li>浴缸和独立淋浴隔间</li>
                                            <li>放大镜</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>