<?php
$title = 'Facilities | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'facilities, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazaphuket/facilities.php';
$lang_th = '/th/deevanaplazaphuket/facilities.php';
$lang_zh = '/zh/deevanaplazaphuket/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/cover-facilities-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">设施及服务 &amp; 餐厅</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#restaurant" class="tab active">餐厅</span>
                    <span data-tab="#bar_and_lounge" class="tab">酒吧 &amp; 休息室</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                    <span data-tab="#fitness_center" class="tab">健身 中央</span>
                    <span data-tab="#kids_club" class="tab">孩子们 俱乐部</span>
                    <span data-tab="#swimming_pool" class="tab">游泳池</span>
                </div>
                
                <div class="tabs-content">
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/cafe.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">餐厅</h1>
                                    
                                    <h2 class="sub-title">普吉岛咖啡馆 </h2>
                                    <p>一场美味的冒险，融合了天体元素与大众的口味，你可以在普吉岛咖啡馆、太阳酒吧、屋顶露台、休息室尽情享用，在芭东普吉岛Deevana广场的美食体验绝非一般。房间送餐服务在晚上十一点三十分结束。
Bars & Lounge酒吧及酒廊 The Lounge休息室</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="bar_and_lounge" class="article" data-tab-name="Bar &amp; Lounge">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/the-lounge.jpg" alt="the lounge"/><br/>
                                    <img class="force thumbnail" src="images/facilities/sun-bar.jpg" alt="sun bar"/><br/>
                                    <img class="force thumbnail" src="images/facilities/rooftop-terrace.jpg" alt="Rooftop Terrace"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">酒吧及酒廊</h1>
                                    
                                    <h2 class="sub-title">休息室</h2>
                                    <p>“天空之家”点缀着无数的“行星和恒星”，大堂休息室以及附近区域全新时尚的同心品牌的新设计和新款鸡尾酒，给酒店客人带来了耳目一新的体验，开放的酒吧储存了优质烈酒和香槟，与舞台表演一起引爆全场。</p>
                                    <p>营业时间：早上十点至午夜</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">太阳酒吧</h2>
                                    <p>在太阳酒吧里，提供鸡尾酒和无酒精饮料，你可以开怀畅饮或者细细品尝，你可以选择池中畅游或者躺在岸边的太阳椅上歇息。在主池附近设有酒店的服务点，提供国际知名的饮料和点心。</p>
                                    <p>营业时间早上十点至晚上七点</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">屋顶露台</h2>
                                    <p>在第六层的屋顶阳台上，这里各种设施一应俱全。酒吧欢迎夫妻或者朋友们在一起欣赏日落，或者再晚点可以仰望星空。这里是私人聚会的绝佳场所。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa。</h1>
                                    <p>一种恢复身体和放松身心的神奇体验，在本酒店水疗馆提供广泛的治疗方案，保证呵护您的健康，缓解你身体的劳累，放松身心的紧张。</p>                                 
                                    <h2 class="sub-title">Orientala Spa 设施</h2>
                                    <ul class="custom-list-dashed">
                                        <li>五个房间（三个单人间两个双人间，都配备有按摩浴缸）</li>
                                        <li>男女储物柜和蒸浴</li>
                                        <li>面部护理和购物</li>
                                        <li>两个面部护理站</li>
                                    </ul>
                                    
                                    <h2 class="sub-title">Orientala Spa 菜单</h2>
                                    
                                    <h3 class="list-heading">
                                        温泉极乐<br>
                                        <small>Time: 150 Minutes Price = 3,000 BAHT</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>草药茶</li>
                                        <li>汗蒸</li>
                                        <li>Body Scrub</li>
                                        <li>精油按摩</li>
                                        <li>泰式按摩  </li>
                                        <li>饼干和茶</li>
                                    </ul>
                                    <p class="note">Remark : Additional complimentary steam of 15 minutes</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        恢复活力<br>
                                        <small>Time: 150 Minutes Price = 3,000 BAHT</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>草药茶</li>
                                        <li>Steam</li>
                                        <li>Body Scrub</li>
                                        <li>精油按摩</li>
                                        <li>Aloe Vera Cool Facial</li>
                                        <li>饼干和茶</li>
                                    </ul>
                                    <p class="note">Remark : Additional complimentary steam of 15 minutes</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        按摩<br>
                                        <small>Time: 120 Minutes Price = 1,700 BAHT</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>草药茶  </li>
                                        <li>Steam</li>
                                        <li>泰式按摩  </li>
                                        <li>精油按摩</li>
                                        <li>足底按摩</li>
                                        <li>饼干和茶</li>
                                    </ul>
                                    <p class="note">Remark : Additional complimentary steam of 15 minutes</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        回归自然<br>
                                        <small>Time: 150 Minutes Price = 2,400 BAHT</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>Herbal Tea</li>
                                        <li>Steam</li>
                                        <li>Body Scrub</li>
                                        <li>泰式按摩  </li>
                                        <li>足底按摩</li>
                                        <li>饼干和茶</li>
                                    </ul>
                                    <p class="note">备注：额外赠送15分钟的汗蒸</p>
                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="fitness_center" class="article" data-tab-name="Fitness Center">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">健身中心</h1>
                                    <p>位于酒店的第三楼，现代化设备齐全，在健身房可以欣赏到芭东市的美丽风景。</p>
                                    
                                    <h2 class="sub-title list-heading">设备</h2>
                                    <ul class="custom-list-dashed">
                                        <li>2 跑步机 </li>
                                        <li>1 椭圆训练机</li>
                                        <li>1 直立自行车</li>
                                        <li>1 划船机</li>
                                        <li>1 可调式哑铃椅 – 1 屈腿训练机</li>
                                        <li>1 Adjustable Recline Bench</li>
                                        <li>1 乒乓球</li>
                                        <li>1 双层哑铃架</li>
                                        <li>1 各种胸推器</li>
                                        <li>1 各种腹肌板</li>
                                        <li>1 Seated Leg Extension / Curl</li>
                                    </ul>
                                    <p>
                                        小时营业<br>
                                        提供毛巾和储物柜<br>
                                        不允许12岁以下儿童进入<br>
                                        12岁到16岁之间的孩子必须有一个成年人陪同
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="kids_club" class="article" data-tab-name="Kids Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/kids-club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">儿童乐园</h1>
                                    <p>在儿童俱乐部一个充满乐趣和兴奋的世界，等待着年轻的客人，在那里释放创造力和想象力，可以模拟野外生存训练。位于靠近大堂的位置，儿童俱乐部允许直接进入儿童游泳池。“儿童俱乐部欢迎从4岁到12岁的孩子在家长的陪同下进行各种活动”。</p>
                                    
                                    <h2 class="sub-title list-heading">Facilities</h2>
                                    <ul class="custom-list-dashed">
                                        <li>液晶电视</li>
                                        <li>视频游戏</li>
                                        <li>桌上足球游戏</li>
                                        <li>活动表</li>
                                        <li>棋类游戏</li>
                                    </ul>
                                    <p>Opening hours : 09.00 ‐ 18.00 hrs.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池</h1>
                                    <p>游泳池开放时间：从早上7点到晚上9点。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    .col-cap .list-heading {
        font-size: 14px;
        margin-top: 1em;
        margin-bottom: 3px;
    }
    .col-cap .list-heading + ul {
        margin-top: 0;
    }
    .col-cap p.note {
        background-color: #eee;
        padding: 5px 10px;
        border-radius: 2px;
        color: #666;
        font-size: 12px;
        display: inline-block;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>