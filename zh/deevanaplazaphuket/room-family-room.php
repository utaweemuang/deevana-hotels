<?php
$title = 'Family Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Family Room: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'family room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-family-room';
$cur_page = 'family-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-family-room.php';
$lang_th = '/th/deevanaplazaphuket/room-family-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-family-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/family/1500/family-01.jpg" alt="Family Room 01" width="1500" height="1008" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 02" width="1500" height="1000" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 03" width="1500" height="1000" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Family Room <span>King size and Bunk beds</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/family/600/family-01.jpg" width="600" height="400" /></li>
                    <li><img src="images/accommodations/family/600/family-02.jpg" height="400" width="600" /></li>
                    <li><img src="images/accommodations/family/600/family-03.jpg" height="400" width="600" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">家庭房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/family/600/family-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>普吉岛巴东DEEVANA广场酒店的家庭室的理念是家人在一起分享彼此的时间，晚上爸爸妈妈的床和孩子的双层床离得很近，但是却彼此独立互不打扰。欣赏着城市的风光，客房提供了一个放松和亲密的空间，家庭成员可以尽情享受对方的陪伴。</p>
                            <p>
                                房间可用：10<br>
                                面积：44平方米
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">房间里的设施</h2>
                                    <ul class="amenities-list">
                                        <li>特大号床(6英尺)和双层床</li>
                                        <li>42英寸液晶电视</li>
                                        <li>视频游戏控制台</li>
                                        <li>餐桌和椅子</li>
                                        <li>免费的无线网络</li>
                                        <li>国际直拨电话和语音邮件</li>
                                        <li>城市的景观</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>房间里的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>特大号床(6英尺)和双层床</li>
                                            <li>42英寸液晶电视</li>
                                            <li>视频游戏控制台</li>
                                            <li>餐桌和椅子</li>
                                            <li>免费的无线网络</li>
                                            <li>国际直拨电话和语音邮件</li>
                                            <li>城市的景观</li>
                                            <li>城市的景观</li>
                                            <li>齐全的迷你酒吧</li>
                                            <li>烟雾报警器、探测器</li>
                                            <li>喷水灭火系统</li>
                                            <li>电子保险箱</li>
                                            <li>通用电源插座</li>
                                            <li>台灯</li>
                                            <li>咖啡机</li>
                                        </ul>
                                        
                                        <h2>浴室配件</h2>
                                        <ul class="list-columns-2">
                                            <li>浴缸和独立淋浴隔间</li>
                                            <li>放大镜</li>
                                            <li>吹风机</li>
                                            <li>体重秤</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>