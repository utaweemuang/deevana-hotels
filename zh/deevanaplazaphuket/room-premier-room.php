<?php
$title = 'Premier Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Premier Room: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'premier room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-premier-room.php';
$lang_th = '/th/deevanaplazaphuket/room-premier-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" width="1500" height="1000" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" width="1500" height="1000" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" width="1500" height="1000" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Premier Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="400" width="600" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="400" width="600" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="400" width="600" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">贵宾房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>享受独特的客房设施和额外的服务，如每日下午在行政酒廊休息并享受免费的饮料，使你获得前所未有的舒适感。</p>
                            <p>
                                房间可用：26<br>
                                面积：35平方米
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">客房设施</h2>
                                    <ul class="amenities-list">
                                        <li>特大号床(6英尺)或两张单人床(4英尺)</li>
                                        <li>42英寸液晶电视</li>
                                        <li>国际直拨电话和语音邮件</li>
                                        <li>泳池的景观</li>
                                        <li>烟雾报警器、探测器</li>
                                        <li>电子保险箱</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>In-room amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>特大号床(6英尺)或两张单人床(4英尺)</li>
                                            <li>42英寸液晶电视</li>
                                            <li>生活的椅子</li>
                                            <li>生活的沙发</li>
                                            <li>闹钟</li>
                                            <li>国际直拨电话和语音邮件</li>
                                            <li>泳池的景观</li>
                                            <li>免费的茶和咖啡</li>
                                            <li>烟雾报警器、探测器</li>
                                            <li>喷水灭火系统</li>
                                            <li>电子保险箱</li>
                                            <li>熨斗</li>
                                            <li>台灯</li>
                                            <li>咖啡机</li>
                                            <li>通用电源插座</li>
                                        </ul>
                                        
                                        <h2>浴室配件</h2>
                                        <ul class="list-columns-2">
                                            <li>吹风机</li>
                                            <li>浴缸和独立淋浴隔间</li>
                                            <li>放大镜</li>
                                            <li>体重秤</li>
                                        </ul>
                                        
                                        <h2>当你升级到总理的房间以后额外的特权</h2>
                                        <ul class="list-columns-2">
                                            <li>赠送受欢迎的饮料</li>
                                            <li>赠送受欢迎的热带水果拼盘</li>
                                            <li>赠送迷你吧台里的酒精饮料和零食</li>
                                            <li>下午茶在15.30到16.30之间</li>
                                            <li>额外的洗浴设施</li>
                                            <li>额外的房间设施</li>
                                            <li>每天赠送一套清洗衣物并熨烫的服务(不适用于干洗)</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>