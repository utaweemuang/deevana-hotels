<script>
    ;(function($) {
        var popup = {
            enabled: true, //Change this to false for disabled popup
            src: 'http://www.deevanaplazaphuket.com/images/banner/BEST_RATE_GURANTEE_BANNER.jpg',
            link: {
                enabled: false,
                href: 'https://reservation.travelanium.net/propertyibe2/?propertyId=275&onlineId=5&accesscode=dppm',
                target: '_blank',
            }
        }
        if (popup.enabled === true) {
            $.magnificPopup.open({
                items: {
                    src: popup.src,
                    type: 'image',
                },
                mainClass: 'mfp-fade',
                removalDelay: 300,
                callbacks: {
                    open: function () {
                        if (popup.link.enabled === true) {
                            var $img = $(this.content).find('.mfp-img');
                            $img.wrap('<a href="' + popup.link.href + '" target="' + popup.link.target + '" />');
                        }
                    }
                }
            });
        }
    })(jQuery);
</script>