<?php
$title = 'Deluxe Suite | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Suite: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'deluxe suite, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-suite';
$cur_page = 'deluxe-suite';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-deluxe-suite.php';
$lang_th = '/th/deevanaplazaphuket/room-deluxe-suite.php';
$lang_zh = '/zh/deevanaplazaphuket/room-deluxe-suite.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-01.jpg" alt="Deluxe Suite 01" width="1500" height="1000" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-02.jpg" alt="Deluxe Suite 02" width="1500" height="1000" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-03.jpg" alt="Deluxe Suite 03" width="1500" height="1000" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Suite <span>King size bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">豪华套房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>使你从繁忙的生活中解脱出来，豪华套房为你提供最舒适的能使你完全放松的私人休息室和一个阳台，客人可以静静地远距离观察芭东的人文生活。隐藏分区的墙后面是一个宁静的卧室，有一张温暖舒适的大床，房间的装饰和色调能使你有良好的睡眠。</p>
                            <p>
                                房间可用：4<br>
                                面积：75平方米
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'zh' ); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">房间里的设施</h2>
                                    <ul class="amenities-list">
                                        <li>特大号床(6英尺)</li>
                                        <li>卧室里42英寸液晶电视和客厅里49英寸液晶电视工作桌</li>
                                        <li>餐桌和椅子</li>
                                        <li>餐桌和椅子</li>
                                        <li>免费的无线网络</li>
                                        <li>国际直拨电话和语音邮件</li>
                                        <li>DVD播放器</li>
                                        <li>泳池的景观</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>房间里的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>特大号床(6英尺)</li>
                                            <li>卧室里42英寸液晶电视和客厅里49英寸液晶电视工作桌</li>
                                            <li>工作桌</li>
                                            <li>餐桌和椅子</li>
                                            <li>免费的无线网络</li>
                                            <li>国际直拨电话和语音邮件</li>
                                            <li>DVD播放器</li>
                                            <li>泳池的景观</li>
                                            <li>齐全的迷你酒吧</li>
                                            <li>免费的茶和咖啡</li>
                                            <li>烟雾报警器、探测器</li>
                                            <li>喷水灭火系统</li>
                                            <li>电子保险箱</li>
                                            <li>意大利浓缩咖啡机</li>
                                            <li>通用电源插座</li>
                                            <li>闹钟</li>
                                            <li>熨斗</li>
                                        </ul>
                                        
                                        <h2>浴室配件</h2>
                                        <ul class="list-columns-2">
                                            <li>浴缸和独立淋浴隔间</li>
                                            <li>放大镜</li>
                                            <li>衣帽间</li>
                                            <li>吹风机</li>
                                            <li>体重秤</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>