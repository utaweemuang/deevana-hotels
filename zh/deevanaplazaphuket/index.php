<?php

session_start();

$title = 'Deevana Plaza Phuket Patong | Official Hotel Group Website Thailand';
$desc = '4 star hotel near Jungceylon and bangla street; Guarantee best direct hotel rate and best location on Patong Beach';
$keyw = 'deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach, Bangla Road, phuket night market, Jungceylon';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanaplazaphuket';
$lang_th = '/th/deevanaplazaphuket';
$lang_zh = '/zh/deevanaplazaphuket';

include '_header.php';
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" width="1500" height="600" /></div>
        </div>

        <!--div id="promotion_board" class="promotion get-center">
            <a href="<?php ibe_url(275, 'en'); ?>" target="_blank">
                <img class="block responsive" src="images/home/hero_banner/banner.png" />
            </a>
        </div-->
        <?php //include_once('include/tl-sticky-banner.php'); ?>

        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到普吉岛芭东蒂瓦娜广场酒店</span></span>
                        </h1>
                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
                        <p>在巴东海滩的中央有一个完美的岛屿度假的所有元素，Deevana广场在普吉岛巴东城市和中部的位置，这里距举世闻名的巴东海滩购物中心,夜市,普吉岛的繁华的夜生活仅几步之遥,。它以其独特的风格和现代化的设施使你在芭东海滩尽享轻松和舒适的生活。</p>
						<p>这里有249个高雅的客房和套房，Orientala Spa，在芭东地区的普吉岛咖啡馆一个独特的原始的泰国和国际美食为您提供别具一格的就餐体验，还有以天文为主题的休闲活动和技术会议和研讨会的设施，酒店本身就是–亮点——它是个人，夫妻，家庭或商业旅行者在普吉岛度假的不二选择。</p>
                        <p><a href="facilities.php" class="button luxury-style" style="max-width: 300px;">独一无二的全套设施满足您的所有需求</a></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/hQN7RaQBBA4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到普吉岛芭东蒂瓦娜广场酒店</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="210" height="140" /></p>
						<p>在巴东海滩的中央有一个完美的岛屿度假的所有元素，Deevana广场在普吉岛巴东城市和中部的位置，这里距举世闻名的巴东海滩购物中心,夜市,普吉岛的繁华的夜生活仅几步之遥,。它以其独特的风格和现代化的设施使你在芭东海滩尽享轻松和舒适的生活。</p>
						<p>这里有249个高雅的客房和套房，Orientala Spa，在芭东地区的普吉岛咖啡馆一个独特的原始的泰国和国际美食为您提供别具一格的就餐体验，还有以天文为主题的休闲活动和技术会议和研讨会的设施，酒店本身就是–亮点——它是个人，夫妻，家庭或商业旅行者在普吉岛度假的不二选择。</p>
                        <p><a href="facilities.php" class="button luxury-style" style="max-width: 300px;">独一无二的全套设施满足您的所有需求</a></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=275&group=13&width=450&height=300&imageid=9017&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>ROMANTIC SURPRISE - HONEYMOON PACKAGE</b></h2>
                                    <p class="description" style="color:yellow;">Stay before 31 Oct 2019</p>
                                    <ul>
                                        <li>FREE Daily Buffet Breakfast 2 persons</li>
                                        <li>FREE Wifi Internet Access</li>
                                        <li>FREE Round Trip Airport Transfer</li>
                                        <li>Candle Light Set Dinner for a Couple</li>
                                        <li>One Time of 60 Minutes Thai Massage</li>
                                        <li>Free Mini Bar, replenished daily</li>
                                        <li>Honeymoon Set Up</li>
                                        <li>20% discount on Food and Beverage at Phuket Cafe</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDgxMjky">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=275&group=13&width=450&height=300&imageid=4618&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Summer Package</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 3 nights</p>
                                    <ul>
                                        <li>FREE Daily Buffet Breakfast</li>
                                        <li>FREE WiFi Internet</li>
                                        <li>FREE One way transfer from Hotel to Phuket Airport</li>
                                        <li>FREE Late Check-out 16:00 hrs.</li>
                                        <li>FREE one Glass of Fresh Cocktails per person</li>
                                        <li>20% discount on Food and Beverage at Phuket Cafe</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDgyMzE2">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#7b9028;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="activities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-orientala_wellness_spa.jpg" width="400" height="300" />
                        </div>
                        <div class="caption">
                            <h2 class="title">Orientala Spa。</h2>
                            <p class="description">一种恢复身体和放松身心的神奇体验，在本酒店水疗馆提供广泛的治疗方案，保证呵护您的健康，缓解你身体的劳累，放松身心的紧张。</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-kid_club.jpg" width="400" height="300" />
                        </div>
                        <div class="caption">
                            <h2 class="title">儿童乐园</h2>
                            <p class="description">在儿童俱乐部一个充满乐趣和兴奋的世界，等待着年轻的客人，在那里释放创造力和想象力，可以模拟野外生存训练。位于靠近大堂的位置，儿童俱乐部允许直接进入儿童游泳池。“儿童俱乐部欢迎从4岁到12岁的孩子在家长的陪同下进行各种活动”。</p>
                            <p><a class="button" href="facilities.php#kids_club">閱讀更多<i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-restaurant.jpg" width="400" height="300" />
                        </div>
                        <div class="caption">
                            <h2 class="title">普吉岛咖啡馆</h2>
                            <p class="description">一场美味的冒险，融合了天体元素与大众的口味，你可以在普吉岛咖啡馆、太阳酒吧、屋顶露台、休息室尽情享用，在芭东普吉岛Deevana广场的美食体验绝非一般。房间送餐服务在晚上十一点三十分结束。</p>
                            <p><a class="button" href="facilities.php#restaurant">閱讀更多<i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title"><span class="deco-map" style="display: inline-block;"><span class="deco-underline" style="color:#7b9028;">PHUKET</span> 景點</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" width="246" height="220" /></div>
                            <h2 class="title">芭东海滩</h2>
                            <a class="more" href="attraction.php#patong_beach">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" width="246" height="220" /></div>
                            <h2 class="title">神仙半岛</h2>
                            <a class="more" href="attraction.php#phromthep_cape">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" width="246" height="220" /></div>
                            <h2 class="title">卡塔和卡伦海滩</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" width="246" height="220" /></div>
                            <h2 class="title">天坛大佛</h2>
                            <a class="more" href="attraction.php#big_buddha">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanaplazaphuket.com/images/awards/TCEB.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/pha-awards.jpg" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tica-awards.jpg" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/pta-awards.png" alt="" width="128" height="128"></li>
                    <li><a class="image-popup-awards" href="http://www.deevanaplazaphuket.com/images/awards/GMP.jpg"><img src="http://www.deevanahotels.com/images/awards/nfi-awards.png" alt="" width="128" height="128"></a></li>
                    <li><a href="https://www.tripadvisor.com/Hotel_Review-g297930-d754312-Reviews-Deevana_Plaza_Phuket_Patong-Patong_Kathu_Phuket.html" target="_blank"><img src="http://www.deevanaplazaphuket.com/images/awards/tripadvisor-deevana.png" alt="" width="128" height="128"></a></li>
                    <li><a href="https://tourismawards.tourismthailand.org/ann_de?id=2" target="_blank"><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-award.png" alt="" width="128" height="128"></a></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<?php

if (!isset($_SESSION['visited'])) : ?>
<?php endif; ?>
<?php $_SESSION['visited'] = "true"; ?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 40px 25px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    #intro {
        min-height: 500px;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
	#activities_slider .owl-nav {
		top: 31%;
    }
    #offers_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #offers_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #offers_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #offers_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #offers_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #offers_slider .caption .button:hover {
        opacity: 0.9;
    }
    #offers_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('.image-popup-awards').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });
        
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<?php
include 'include/popup-image.php';
include '_footer.php';
?>