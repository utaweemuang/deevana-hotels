<?php
$title = 'Meetings and Event | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'meetings and event, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazaphuket/meetings-and-event.php';
$lang_th = '/th/deevanaplazaphuket/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazaphuket/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/slide-meeting-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-02.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-03.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">会议和活动</h1>
            </header>

            <div class="section-content container">
                <p class="excerpt">普吉岛巴东Deevana广场承接任何规模的会议、研讨会或其他活动。无论是企业的还是私人的，本地的还是其他地区的,室内的还是室外的,大型广场大宴会或者室内小型宴会，本酒店都能按要求帮您定制空间，轻松满足您的需求。</p>
                <p>如有其他查询，请致电 <a href="tel:+6676302100">+66 (0)76 302 100</a> 或发送电子邮件至: <a href="mailto:info@deevanaplazaphuket.com">info@deevanaplazaphuket.com</a></p>
                <p>
                    <a class="button-default" href="http://www.deevanaplazaphuket.com/download/Meeting-Room-Floor-Plan.pdf" target="_blank">查看平面图</a>
                    <a class="button-default" href="http://www.deevanaplazaphuket.com/images/facilities/MEETING-PACKAGE-004.jpg" target="_blank">会议与交流 婚礼套餐</a>
                </p>
            </div>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#plaza_grand_ballroom" class="tab active">广场大宴会厅</span>
                    <span data-tab="#square_i_meeting_room" class="tab">广场一号会议室</span>
                    <span data-tab="#square_ii_meeting_room" class="tab">广场二号会议室</span>
                    <span data-tab="#meeting_room_capacity" class="tab">会议室容量</span>
                </div>
                
                <div class="tabs-content">
                    <article id="plaza_grand_ballroom" class="article" data-tab-name="Plaza Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/Plaza-Grand-Ballroom.jpg" width="1100" height="733" alt="Plaza Grand Ballroom" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">广场大宴会厅</h1>
                                    <p>一个国家级的富有戏剧特色艺术风格的舞厅配备有先进的视听设备确保任何大型规模的商务会议或学术论谈都能正常流畅的进行。</p>
                                    
                                    <h2 class="sub-title list-heading">设备</h2>
                                    <ul class="custom-list-dashed">
                                        <li>投影机</li>
                                        <li>液晶显示器</li>
                                        <li>高端照明系统</li>
                                        <li>音频设备和放大器</li>
                                        <li>2个独立的控制室</li>
                                    </ul>
                                    <p>
                                        面积:565平方米。<br>
                                        容量:500人
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="square_i_meeting_room" class="article" data-tab-name="Square I Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/Square-Meeting-Room.jpg" width="1100" height="733" alt="Square I Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">广场一号会议室</h1>
                                    <p>对于小型会议可以把会议室一分为二，变成两个会议室并共享一个休息室，休息室面朝花园，花园里的美丽风景尽收眼底。</p>
                                    <p>面积:81.5 -113平方米。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="square_ii_meeting_room" class="article" data-tab-name="Square II Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/gallery-51.jpg" width="1500" height="936" alt="Square II Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">广场二号会议室</h1>
                                    <p>对于小型会议可以把会议室一分为二，变成两个会议室并共享一个休息室，休息室面朝花园，可以花园里的美丽风景尽收眼底。</p>
                                    <p>面积:81.5 -113平方米。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="meeting_room_capacity" class="article" data-tab-name="Meeting Room Capacity">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">会议室容量</h1>
                                    <p>商务会议室能容纳15人进行常务会议和休息，使你事半功倍。</p>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>会议室</th>
                                                <th>面积（平方米）</th>
                                                <th>天花高度（m）</th>
                                                <th data-hide="phone,tablet">U形</th>
                                                <th data-hide="phone,tablet">长方形</th>
                                                <th data-hide="phone,tablet">剧院</th>
                                                <th data-hide="phone,tablet">课堂</th>
                                                <th data-hide="phone,tablet">宴会</th>
                                                <th data-hide="phone,tablet">鸡尾酒</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>广场大气球</td>
                                                <td>565</td>
                                                <td>5</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>500</td>
                                                <td>200</td>
                                                <td>300</td>
                                                <td>600</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>广场 I</td>
                                                <td>323</td>
                                                <td>5</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>120</td>
                                                <td>180</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>广场 II</td>
                                                <td>242</td>
                                                <td>5</td>
                                                <td>40</td>
                                                <td>40</td>
                                                <td>200</td>
                                                <td>100</td>
                                                <td>120</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>广场 I</td>
                                                <td>113</td>
                                                <td>2.4</td>
                                                <td>30</td>
                                                <td>30</td>
                                                <td>50</td>
                                                <td>35</td>
                                                <td>50</td>
                                                <td>60</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>广场 II</td>
                                                <td>81.5</td>
                                                <td>2.4</td>
                                                <td>20</td>
                                                <td>20</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>40</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                
                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
        
        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .section-title {
        margin-bottom: 0;
    }
    .section-header .excerpt {
        color: #666;
    }
    .section-content {
        margin-bottom: 60px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    .col-cap .list-heading {
        font-size: 14px;
        margin-top: 1em;
        margin-bottom: 3px;
    }
    .col-cap p.note {
        background-color: #eee;
        padding: 5px 10px;
        border-radius: 2px;
        color: #666;
        font-size: 12px;
        display: inline-block;
    }
    #meeting_room_capacity {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>