<?php
$title = 'Junior Suite with Jacuzzi | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Junior Suite with Jacuzzi: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'junior suite with jacuzzi, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-jacuzzi';
$cur_page = 'junior-with-jacuzzi';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-junior-with-jacuzzi.php';
$lang_th = '/th/deevanapatong/room-junior-with-jacuzzi.php';
$lang_zh = '/zh/deevanapatong/room-junior-with-jacuzzi.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-01.jpg" alt="Junior Suite with Jacuzzi 01" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-02.jpg" alt="Junior Suite with Jacuzzi 02" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-03.jpg" alt="Junior Suite with Jacuzzi 03" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-04.jpg" alt="Junior Suite with Jacuzzi 04" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-05.jpg" alt="Junior Suite with Jacuzzi 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Junior Suite with Jacuzzi <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">有按摩浴缸的初级套房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>55平方米的套房，是家庭共享或夫妇奢侈度假的最佳选择，配备有私人的按摩浴缸。宽敞的房间里有一个7英尺的大床，旁边配备冰箱、茶/咖啡设施，生活区配备42寸液晶电视，户外座位，桌子、梳妆台和一个带浴缸和淋浴的大浴室。可容纳四人的要求进行额外加床。</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>个人控制的空气调节系统</li>
                                        <li>国际直拨电话</li>
                                        <li>彩色电视卫星频道与国际新闻</li>
                                        <li>电力220 V</li>
                                        <li>免费Wi Fi网络</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>个人控制的空气调节系统</li>
                                            <li>浴缸</li>
                                            <li>浴室设施:从头到脚浴液、浴帽、毛巾、棉花花蕾</li>
                                            <li>针线包</li>
                                            <li>拖鞋,凉鞋</li>
                                            <li>伞</li>
                                            <li>沙滩包</li>
                                            <li>行李架</li>
                                            <li>国际直拨电话</li>
                                            <li>彩色电视卫星频道与国际新闻</li>
                                            <li>冰箱里有免费饮用水:每天2瓶</li>
                                            <li>电力220 V。</li>
                                            <li>私人阳台和泳池风光</li>
                                            <li>烧水壶和咖啡机</li>
                                            <li>吹风机</li>
                                            <li>保险箱</li>
                                            <li>免费Wi Fi网络</li>
                                            <li>互相连接的房间</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>