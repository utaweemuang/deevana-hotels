<?php
$title = 'Deluxe Room with Jacuzzi | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room with Jacuzzi: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deluxe room with jacuzzi, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-with-jacuzzi';
$cur_page = 'deluxe-with-jacuzzi';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-deluxe-with-jacuzzi.php';
$lang_th = '/th/deevanapatong/room-deluxe-with-jacuzzi.php';
$lang_zh = '/zh/deevanapatong/room-deluxe-with-jacuzzi.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-01.jpg" alt="Deluxe with Jacuzzi 01" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-02.jpg" alt="Deluxe with Jacuzzi 02" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-03.jpg" alt="Deluxe with Jacuzzi 03" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-04.jpg" alt="Deluxe with Jacuzzi 03" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-05.jpg" alt="Deluxe with Jacuzzi 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe with Jacuzzi <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">豪华按摩浴缸房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>带按摩浴缸的44平方米豪华房间，包括阳台上的一个愉快的私人按摩浴缸。房间有双床房和大床房两种房型。冰箱、茶/咖啡及配套设施、浴室淋浴、咖啡桌和平板电视等一应俱全。客人还可以享受从阳台的按摩浴缸直接到泳池的乐趣。</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>个人控制的空气调节系统</li>
                                        <li>国际直拨电话</li>
                                        <li>彩色电视卫星频道与国际新闻</li>
                                        <li>电力220 V</li>
                                        <li>免费Wi Fi网络</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>个人控制的空气调节系统</li>
                                            <li>浴室设施:从头到脚浴液、浴帽、毛巾、棉花花蕾</li>
                                            <li>针线包</li>
                                            <li>拖鞋,凉鞋</li>
                                            <li>伞</li>
                                            <li>沙滩包</li>
                                            <li>行李架</li>
                                            <li>国际直拨电话</li>
                                            <li>彩色电视卫星频道与国际新闻</li>
                                            <li>冰箱里有免费饮用水:每天2瓶</li>
                                            <li>电力220 V。</li>
                                            <li>景观花园</li>
                                            <li>烧水壶和咖啡机</li>
                                            <li>吹风机</li>
                                            <li>保险箱</li>
                                            <li>免费Wi Fi网络</li>
                                            <li>互相连接的房间</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>