<?php
$title = 'Facilities | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'facilities, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanapatong/facilities.php';
$lang_th = '/th/deevanapatong/facilities.php';
$lang_zh = '/zh/deevanapatong/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <?php for($i = 1; $i <= 6; $i++) : ?>
            <?php $index = ($i<10) ? '0'.$i : $i; ?>
            <div class="item"><img src="images/facilities/facilities-slide-<?php echo $index; ?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <?php endfor; ?>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">设备 &amp; 服务</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#restaurant" class="tab active">餐厅</span>
                    <span data-tab="#swimming_pool" class="tab">游泳池</span>
                    <span data-tab="#leisure_and_tours" class="tab">休闲旅游</span>
                    <span data-tab="#mice_facilities" class="tab">会展设施</span>
                    <span data-tab="#wedding" class="tab">婚礼</span>
                    <span data-tab="#cooking_class" class="tab">泰式烹饪课程</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                </div>
                
                <div class="tabs-content">
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-01.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-02.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-03.jpg" alt="Restaurent" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">餐厅</h1>
                                    
                                    <h2 class="sub-title">Dalah Cuisine</h2>
                                    <p>你可以选择泰国或者欧式早餐开始你伟大的一天，在dalah美食餐厅有国际上流行的各种食品，谷物，水果，茶，咖啡和更多的美味自助早餐。柔和的DéCOR、泳池和露天布置保证微风餐厅提供一个清新的一天的开始。<br/>
                                        开放：06:00–11:00小时</p>
                                        <br/><br/><br/><br/><br/><br/><br/>
                                    
                                    <h2 class="sub-title">阳台咖啡馆与互联网吧</h2>
                                    <p>在热带的绿色花园和游泳池的旁边，宜人的阳台餐厅为你提供丰盛的午餐和晚餐。休闲餐厅通风的墙体让新鲜空气流通，让人有一种露天的感觉。选择同一种柔软舒适的扶手椅或时髦的餐桌。阳台的布置精巧漂亮，使人能更好的享受一杯新鲜的咖啡和自制的蛋糕，或美味的晚餐。<br/>
                                        开放：18:00–22:00小时</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">豪华泳池酒吧</h2>
                                    <p>你可以完全放松的酥在你的椅子上，或游到豪华泳池酒吧去品味一个清爽的水果奶昔的，新鲜的椰青和冰饮料或者轻餐和小吃。<br/>
                                        开放：10:00–20:00小时。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool/pool-01.jpg" alt="Swimming Pool"/>
                                    <br/>
                                    <img class="force thumbnail" src="images/facilities/pool/pool-02.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池</h1>
                                    
                                    <h2 class="sub-title">豪华泳池</h2>
                                    <p>豪华水蓝色游泳池是这个度假圣地的焦点，所有房间的阳台都面对着它。大矩形水池旁边有一个专为孩子们提供的浅水池，旁边有两个迷人的具有泰国特色的大象喷泉。泳池边太阳椅林立，有很大的空间，旁边一个方便的泳池酒吧，客人可以在游泳的闲暇享受清凉的饮料。</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">花园游泳池</h2>
                                    <p>在华丽的绿树环绕下，花园里的游泳池是一个悠度时光的好地方。最近翻新的花园游泳池底部是倾斜的，使它适合各种游泳水平的人。巨大的游泳池配备了令人愉快的按摩浴缸和现代喷泉。孩子们的游泳池也有一个可爱的喷泉，可以让小朋友们愉快地玩耍。</p>
                                    <p>开放：10:00–20:00小时。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="leisure_and_tours" class="article" data-tab-name="Leisure &amp; Tours">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tours/tours-01.jpg" alt="Leisure &amp; Tours" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">休闲旅游</h1>
                                    
                                    <p>除了在岛上的辉煌的海滩上放松，在普吉岛上还有很多事情可以做。探索周围的热带岛屿，在一个不同的水下世界进行潜水探险。参观一些著名的寺庙大佛和Wat Chalong的宗教场所。参观旧镇与历史建筑和博物馆。乘坐一头大象去看岛上的许多美丽的风景点。这里有一些推荐的景点。</p>
                                    
                                    <h2 class="sub-title">普吉旧城</h2>
                                    <p>大约100年前，普吉岛是一个繁荣的锡矿业社区，许多中国移民在这个行业工作。随着经济的蓬勃发展，建设的豪宅和交易商店中出现了葡萄牙风格，商店狭长木门和中国的镂空雕刻。许多豪宅和镇店的房子已被保存和恢复，成为普吉岛古镇古朴的咖啡馆、餐厅、画廊、精品店等。漫步在Dibuk，攀‐NGA，Yaowarat，Thaland和甲米的道路上看到这些可爱的建筑。其他古老的欧洲风格的建筑当中值得注意是省厅（Sala Klang），普吉岛法院（三府），和那空銮泰国银行。</p>
                                    
                                    <h2 class="sub-title">查龙寺</h2>
                                    <p>这个维护保养良好的寺庙是普吉岛人民的一个崇敬的地方，是一个受欢迎的寺庙，游客参观。住在寺是Luang Pho Chaem的雕像和Luang Pho Chuang，在1876年的普吉岛锡矿工起义期间这里帮助和救治那些受伤的群众。</p>
                                    
                                    <h2 class="sub-title">卡隆的景点</h2>
                                    <p>从卡塔Nai Harn到卡隆的山间，建设了大量的观景台，凉亭和简易房。游客可以坐下来欣赏壮丽的景色，俯瞰小卡塔海滩，卡塔和卡隆海滩，以及小螃蟹岛。这里提供洗手间和小吃零食。</p>
                                    
                                    <h2 class="sub-title">旅游柜台</h2>
                                    <p>度假，旅游柜台提供充分的选择游览包括普吉岛古镇，皮皮岛，007岛、潜水、浮潜、钓鱼、狩猎和大象，私人快艇游览，独木舟的体验，普吉岛幻想曲等等。我们的旅游工作人员有丰富的当地知识，对你进行无障碍的帮助，其中包括：</p>
                                    <ul class="list-columns-3 custom-list-dashed">
                                        <li>水上运动</li>
                                        <li>迷你高尔夫</li>
                                        <li>打高尔夫球</li>
                                        <li>4国际课程</li>
                                        <li>卡丁车</li>
                                        <li>R攀岩</li>
                                        <li>山地自行车</li>
                                        <li>高空彈跳</li>
                                        <li>生态旅游</li>
                                        <li>丛林与大象</li>
                                        <li>漂流</li>
                                        <li>离岛游</li>
                                        <li>骑马</li>
                                        <li>射击场</li>
                                        <li>皮皮岛</li>
                                        <li>James Bond Island（攀牙湾‐）</li>
                                        <li>珊瑚岛</li>
                                        <li>皇帝岛</li>
                                        <li>鸡蛋岛</li>
                                        <li>斯米兰岛</li>
                                        <li>朗岛</li>
                                        <li>私人快艇游览</li>
                                        <li>潜水</li>
                                        <li>浮潜</li>
                                        <li>钓鱼比赛</li>
                                        <li>巡航和赛琳</li>
                                        <li>海上独木舟</li>
                                        <li>帆船</li>
                                    </ul>
                                    <p>and many more…</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="mice_facilities" class="article" data-tab-name="MICE Facilities">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                <img class="force thumbnail" src="images/facilities/mice/meeting-01.jpg" alt="MICE Facilities"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">会展设施</h1>
                                    
                                    <h2 class="sub-title">会议及会议</h2>
                                    <p>蒂瓦娜芭东度假村酒店为大型会议和活动提供最佳的设施和服务。与美丽的大厅，宽敞的花园和专业装备的中联的房间，度假村承接各种各样的活动和会议。</p>
                                    
                                    <h2 class="sub-title">中联的房间</h2>
                                    <p>中联会议室位于地下，可从豪华的侧廊通过大堂和停车场</p>
</p>
                                    
                                    <ul class="list-description">
                                        <li><span class="label">面积:</span> 7.70 m &times; 14.40 m (110sqm)</li>
                                        <li><span class="label">天花板的高度:</span> 2.60 m</li>
                                    </ul>
                                    
                                    <table style="margin: 0;" width="100%">
                                        <thead>
                                            <tr>
                                                <th>设置</th>
                                                <th>剧院</th>
                                                <th>教室</th>
                                                <th>u型房</th>
                                                <th>黑板室</th>
                                                <th>自助餐</th>
                                                <th>鸡尾酒会</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <th>可容纳人数</th>
                                                <td>60</td>
                                                <td>40</td>
                                                <td>20</td>
                                                <td>10</td>
                                                <td>50</td>
                                                <td>80</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <h2 class="sub-title">照明</h2>
                                    <p>可调节的照明和相关的设施/控制。调光控制器位于中联会议室。</p>
                                    
                                    <h2 class="sub-title">视听设备</h2>
                                    <p>
                                        桌子，立杆和无线麦克风<br>
                                        便携式扬声器广播系统<br>
                                        多媒体系统电视<br>
                                        激光光盘播放机，光盘，光盘<br>
                                        液晶投影机可供出租
                                    </p>
                                    
                                    <h2 class="sub-title">会议简报设备</h2>
                                    <p>白板，翻转图，留言板，投影仪屏幕。</p>
                                    
                                    <h2 class="sub-title">无线‐FI系统</h2>
                                    <p>通过电话线的互联网接入是免费的，当连接到普吉岛的服务器
信号区域接收在其他地区的其他服务器的互联网接入</p>
                                    
                                    <h2 class="sub-title">会议服务人员</h2>
                                    <p>
                                        信息员<br>
                                        电工、工，修理工<br>
                                        音频‐视觉设备操作员<br>
                                        高级员工和管理人员，以确保您流畅使用所有设备
                                    </p>
                                    
                                    <h2 class="sub-title">组织者的办公室：</h2>
                                    <p>（关于报价）：幻灯片投影仪，偶然听到的投影仪，液晶投影仪，电脑，打印机，打印机，摄影师和传真机。</p>
                                    
                                    <h2 class="sub-title">团队建设</h2>
                                    <p>DEEVANA度假村内有很多企业利用一流的会议设施和设施来举行各种活动。在度假村的专业活动组织者可以定制一个别开生面的活动，促进团队工作和信任，改善工作关系。它的高科技设施和度假的理由是一个成功的团队建设日的理想的举办场所。有关更多信息，请联系我们的项目团队在sales@deevanapatong.com。</p>
                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="wedding" class="article" data-tab-name="Wedding">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/wedding/wedding-01.jpg" alt="wedding"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">婚礼</h1>
                                    <p>在Deevana酒店经验丰富的婚礼策划师能永远为珍惜幸福的夫妇和客人创造一个美好的一天。风景如画的和宽敞的度假地为一个特殊的婚礼提供了完美的背景，别致的餐饮和接待区，可以迎合小或大型的婚礼聚会。团队在Deevana可以组织一个包括和尚的祝福在内的泰国仪式婚礼或者有家人和朋友出席的西式婚礼。有关更多信息，请联系我们的团队在sales@deevanapatong.com。</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="cooking_class" class="article" data-tab-name="Thai Cooking Class">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/cooking_class/cooking_class-01.jpg" alt="Thai Cooking Class"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">泰式烹饪课程</h1>
                                    <p>跟着拥有超过10年的经验的泰国厨师学习美味的泰国菜的秘密，‐烹饪课。学习如何烹饪一些受欢迎的泰国菜的技能，知识和食谱，并享受烹饪带给你的乐趣，通过午餐或晚餐来享受你的烹饪成果。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/spa/spa-01.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>在 Orientala Spa 中心，训练有素的治疗师会在你放松和恢复过程中让你享受到愉悦的感官感受。使用古老与现代技术相结合的手法，消除你的所有压力，让你舒适放松。治疗房间都配备一个由轻松氛围的装饰的优雅舒缓池。夫妇可以享受私人浪漫的双治疗床房间来一起享受这种美妙的体验。</p>
                                    <p>沉醉在长达180分钟的幸福治疗当中，还可以从菜单中选择其他种类繁多的服务，包括传统泰式按摩、泰式推拿，香薰按摩等多种手法。还可以试试洗面、拔罐和刮痧。客人还可以使用一个蒸汽室，健身室附近还可以使用额外的设施，按摩，美容美发。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>