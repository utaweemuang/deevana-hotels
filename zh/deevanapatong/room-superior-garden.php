<?php
$title = 'Superior Garden Room | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Superior Garden Room: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'superior garden room, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-superior-garden';
$cur_page = 'superior-garden';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-superior-garden.php';
$lang_th = '/th/deevanapatong/room-superior-garden.php';
$lang_zh = '/zh/deevanapatong/room-superior-garden.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/superior/1500/superior-garden-01.jpg" alt="Superior Garden Room 01" />
                    <img src="images/accommodations/superior/1500/superior-garden-02.jpg" alt="Superior Garden Room 02" />
                    <img src="images/accommodations/superior/1500/superior-garden-03.jpg" alt="Superior Garden Room 03" />
                    <img src="images/accommodations/superior/1500/superior-garden-04.jpg" alt="Superior Garden Room 04" />
                    <img src="images/accommodations/superior/1500/superior-garden-05.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-06.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-07.jpg" alt="Superior Garden Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Superior Garden Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/superior/600/superior-garden-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-06.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-07.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">高级花园房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/superior/600/superior-garden-02.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>27平方米高级花园房有豪华双床房和6英尺大床房两种选择。经典的深色实木家具和房间的装修形成了鲜明对比，房间干净整洁，泰国经典的装饰和各种家具搭配相得益彰。拥有独立的浴室和卫生间。</p>                            
                            
                            <p class="note">
                                <strong>注意：</strong><br>
                                在贝尔柜台的海滩毛巾服务。<br>
                                泳池边的游泳池毛巾服务。
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'zh'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>个人控制的空气调节系统</li>
                                        <li>国际直拨电话</li>
                                        <li>彩色电视卫星频道与国际新闻</li>
                                        <li>电力220 V。</li>
                                        <li>保险箱</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>所有的设施</h2>
                                        <ul class="list-columns-2">
                                            <li>个人控制的空气调节系统</li>
                                            <li>浴室设施:从头到脚浴液、浴帽、毛巾、棉花花蕾</li>
                                            <li>针线包</li>
                                            <li>拖鞋,凉鞋</li>
                                            <li>伞</li>
                                            <li>沙滩包</li>
                                            <li>行李架</li>
                                            <li>国际直拨电话</li>
                                            <li>彩色电视卫星频道与国际新闻</li>
                                            <li>冰箱里有免费饮用水:每天2瓶</li>
                                            <li>电力220 V。</li>
                                            <li>景观花园</li>
                                            <li>烧水壶和咖啡机</li>
                                            <li>吹风机</li>
                                            <li>保险箱</li>
                                            <li>互相连接的房间</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>