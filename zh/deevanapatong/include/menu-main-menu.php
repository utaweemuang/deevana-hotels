<ul class="menu list-menu level-0">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">家</a></li>
    <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">住宿</a>
        <ul class="sub-menu level-1">
            <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a class="label" href="#">景閣</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('superior-garden'); ?>"><a href="room-superior-garden.php">高級花園房</a></li>
                </ul>
            </li>
            <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a class="label" href="#">豪華翼</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('deluxe'); ?>"><a href="room-deluxe.php">豪華房</a></li>
                    <li class="<?php echo get_current_class('deluxe-with-jacuzzi'); ?>"><a href="room-deluxe-with-jacuzzi.php">豪華帶按摩浴缸房</a></li>
                    <li class="<?php echo get_current_class('junior-suite'); ?>"><a href="room-junior-suite.php">小型套房</a></li>
                    <li class="<?php echo get_current_class('junior-with-jacuzzi'); ?>"><a href="room-junior-with-jacuzzi.php">小型套房配有按摩浴缸</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">設施</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">景點</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('277', 'zh'); ?>" target="_blank">提升</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">畫廊</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">聯繫</a></li>
</ul>