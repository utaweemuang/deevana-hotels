<script>
    ;(function ($) {
        var popup = {
            enabled: true, //Change this to false for disabled popup
            src: 'http://www.deevanahotels.com/deevanapatong/images/banner/holiday_package_20180307.jpg',
            link: {
                enabled: true,
                href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=277&onlineId=4&pid=MDgxODE3',
                target: '_blank',
            }
        }
        if (popup.enabled === true) {
            $.magnificPopup.open({
                items: {
                    src: popup.src,
                    type: 'image',
                },
                mainClass: 'mfp-fade',
                removalDelay: 300,
                callbacks: {
                    open: function () {
                        if (popup.link.enabled === true) {
                            var $img = $(this.content).find('.mfp-img');
                            $img.wrap('<a href="' + popup.link.href + '" target="' + popup.link.target + '" />');
                        }
                    }
                }
            });
        }
    })(jQuery);
</script>