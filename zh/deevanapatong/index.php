<?php

session_start();

$title = 'Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanapatong';
$lang_th = '/th/deevanapatong';
$lang_zh = '/zh/deevanapatong';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
        </div>

       	<!--
        <div id="promotion_board" class="promotion get-center">
            <a href="<?php //ibe_url('277', 'en', 'MDgxNjE1'); ?>" target="_blank">
                <img class="block responsive" src="images/home/discount_bann.png" />
            </a>
        </div>
        -->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">歡迎來到 <br><span style="color:#236198;">芭东蒂瓦纳度假水疗中心</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>蒂瓦娜芭东度假村酒店有235个房间。位于普吉岛最受欢迎的目的地–芭东,这里是所有活动的中心，同时占据两大位置优势。到度假胜地著名的Bangla路的夜生活和Jungceylon购物和娱乐中心都只有几分钟的路程，当然，离辉煌的巴东海滩美妙的金色沙滩也非常的近。然而，当进入Deevana大堂，会让你有一种泰国特有的平静和安宁的感觉，宽敞的热带花园和雅致的客房和套房，仿佛带你去一个宁静的绿洲。作为一个令人轻松的度假圣地，这里的一切配套设施都设置的那么完美。</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/JYxbwXpbExQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">歡迎來到 <br><span style="color:#236198;">芭东蒂瓦纳度假水疗中心</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>蒂瓦娜芭东度假村酒店有235个房间。位于普吉岛最受欢迎的目的地–芭东,这里是所有活动的中心，同时占据两大位置优势。到度假胜地著名的Bangla路的夜生活和Jungceylon购物和娱乐中心都只有几分钟的路程，当然，离辉煌的巴东海滩美妙的金色沙滩也非常的近。然而，当进入Deevana大堂，会让你有一种泰国特有的平静和安宁的感觉，宽敞的热带花园和雅致的客房和套房，仿佛带你去一个宁静的绿洲。作为一个令人轻松的度假圣地，这里的一切配套设施都设置的那么完美。</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=277&group=13&width=450&height=300&imageid=4812&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>'HOLIDAY PACKAGES' MINIMUM 2 NIGHTS, AIRPORT TRANSFER,THAI MASSAGE,AND ANY MORE.</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 2 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>Free Wi-Fi</li>
                                        <li>Free cash voucher 300 THB (Food only)</li>
                                        <li>Free laundry service 2 pieces per day</li>
                                        <li>Free Thai Massage one hour per person per stay.</li>
                                        <li>Free a set of drink in room on arrival date (2 local beer & 2 soft drink)</li>
                                        <li>Free A cup of Coffee or Tea and piece of cake at Coffee Corner (one set per person per stay)</li>
                                        <li>Late check out until 16:00hrs for staying period 20 Mar'19 to 31 Oct'19)</li>
                                        <li>Free round trip transfer from and to airport between 06:00am to 09:00pm *Airport transfer with additional charge 350 THB per way between 09:00pm to 06:00am.</li>
                                        <li>Remark : For a free airport pick up, please advise flight detail at least 24hrs before arrival time.</li>
                                        <li>*Minimum 7 nights stay Free A selection of Buffet Dinner, Thai set Lunch or Thai set Dinner (one time per stay)</li>
                                        <li>*Minimum 7 nights stay Free 1 time Cooking Class per stay</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=277&onlineId=4&pid=MDgxODE3">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="facilities" class="section section-facilities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#236198;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="facilities_slider" class="owl-carousel has-nav fx-scale force-nav">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-01.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">RESTAURANT</h2>
                            <p class="description">Dalah Cuisine, Start your day with a great selection of Thai and continental breakfasts, with all the cooked international favourites, cereals, fruit, tea, coffee and much more ...</p>
                            <p><a class="button" href="facilities.php#restaurant">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-02.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ORIENTALA SPA</h2>
                            <p class="description">Treat your senses to delightful new sensations at the Orientala Spa where the trained therapists will take you on a journey of relaxation and rejuvenation. ...</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">閱讀更多 <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-03.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">SWIMMING POOL</h2>
                            <p class="description">Deluxe Pool, The aqua‐blue swimming pool at the Deluxe Wing is a focal point of the resort with all balconies facing the glistening water ...</p>
                            <p><a class="button" href="facilities.php#swimming_pool">閱讀更多<i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title">
                    <span class="deco-map deco-underline" style="display: inline-block;"><span style="color:#236198;">PHUKET</span> 景點</span>
                </h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" /></div>
                            <h2 class="title">PATONG BEACH</h2>
                            <a class="more" href="attraction.php#patong_beach">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" /></div>
                            <h2 class="title">PHROMTHEP CAPE</h2>
                            <a class="more" href="attraction.php#phromthep_cape">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" /></div>
                            <h2 class="title">KATA AND KARON BEACHES</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" /></div>
                            <h2 class="title">BIG BUDDHA</h2>
                            <a class="more" href="attraction.php#big_buddha">學到更多 <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/green-leaf-awards.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>

    </section>
</main>
<?php

if (!isset($_SESSION['visited'])) { ?>
<?php }
$_SESSION['visited'] = "true";
?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #facilities {
        background-image: url(images/home/bg-facilities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #facilities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #facilities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #facilities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #facilities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #facilities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #facilities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #facilities_slider .center .caption .button {
        background-color: #00adb7;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.force-nav .owl-nav,
        .owl-carousel.force-nav .owl-nav.disabled {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });

        $('#facilities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php include 'include/popup-image.php'; ?>
<?php include_once('_footer.php'); ?>
