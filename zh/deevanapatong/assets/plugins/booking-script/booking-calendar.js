/* Booking Function */
(function($) {
    $.fn.booking = function( options ) {
        var settings = $.extend({
            checkInSelector: '#checkin_date',
            checkOutSelector: '#checkout_date',
            adultSelector: '#adult',
            childSelector: '#child',
            roomSelector: '#room',
            codeSelector: '#accesscode',
            submitSelector: '#submit',
            validateSelector: '#validate',
            propertyId: '#property',
            onlineId: '5',
            language: 'zh',
            validateError: function(){},
            beforeSubmit: function(){},
            afterSubmit: function(){},
        }, options);

        var $this = $(this);
        var thisIndex = $this.index();
        var $checkIn = $this.find(settings.checkInSelector);
        var $checkOut = $this.find(settings.checkOutSelector);
        var $submit = $this.find(settings.submitSelector);
        var $validate = $this.find(settings.validateSelector);
        
        
        
        //Define property
        function propertyVal(raw) {
            if ( $.isNumeric(raw) === true ) {
                return raw;
            } else {
                return $this.find(raw).val();
            }
        }
        
        
        
        //Validation
        function validation(elm) {
            var $x = $(elm);
            var val = $x.val();
            if(val === '') {
                return false;
            } else {
                return true;
            }
        }
        
        
        
        //Insert hidden field
        var alt_checkin_id = 'checkin_alt_val_'+thisIndex;
        var alt_checkout_id = 'checkout_alt_val_'+thisIndex;
        $checkIn.after('<input id="'+alt_checkin_id+'" type="hidden" />');
        $checkOut.after('<input id="'+alt_checkout_id+'" type="hidden" />');
        
        
        
        //Datepicker
        $checkIn.datepicker({
            minDate: new Date(),
            changeMonth: false,
            changeYear: false,
            dateFormat: 'dd M yy',
            altFormat: 'yy-mm-dd',
            altField: '#'+alt_checkin_id,
            numberOfMonths: 1,
            onClose: function (selectedDate) {
                onSelectedCheckinDate($checkOut, selectedDate);
            }
        });

        $checkOut.datepicker({
            minDate: '+1d',
            changeMonth: false,
            changeYear: false,
            dateFormat: 'dd M yy',
            altFormat: 'yy-mm-dd',
            altField: '#'+alt_checkout_id,
            numberOfMonths: 1,
            beforeShow: function (dateText, inst) {
                beforeShowCheckOutDate($checkIn, $checkOut, inst);
            }
        });
        
        
        
        //Set Date
        var today = new Date();
        var afterTomorrow = new Date(today.getTime() + 172800000); //2 days
        $checkIn.datepicker('setDate', today);
        $checkOut.datepicker('setDate', afterTomorrow);
        
        
        
        //Mobile Detect
        var isMobile = false;
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
        
        function deviceDetect() {
            if (isMobile === true) {
                return 'propertyibemobile';
            } else {
                return 'propertyibe2';
            }
        }
        
        

        $submit.on('click', function(e) {
            e.preventDefault();

            var baseurl = 'https://reservation.travelanium.net/propertyibe2/rates?';
            var propertyDevice = deviceDetect();
            var propertyIdParam = 'propertyId=';
            var onlineIdParam = 'onlineId=';
            var checkInParam = 'checkin=';
            var checkOutParam = 'checkout=';
            var adultParam = 'numofadult=';
            var childParam = 'numofchild=';
            var roomParam = 'numofroom=';
            var accesscodeParam = 'accesscode=';
            var pgroupParam = 'pgroup=';
            
            var propertyIdValue = propertyVal(settings.propertyId);
            var onlineIdValue = settings.onlineId;
            
            var languageParam = 'lang=';
            var language = settings.language;
            
            var checkInValue = $('#'+alt_checkin_id).val();
            var checkOutValue = $('#'+alt_checkout_id).val();
            var adultValue = $this.find(settings.adultSelector).val();
            var childValue = $this.find(settings.childSelector).val();
            var roomValue = $this.find(settings.roomSelector).val();
            var accesscodeValue = $this.find(settings.codeSelector).val();
            
            var redirectUrl = baseurl + propertyIdParam + propertyIdValue + "&" + onlineIdParam + onlineIdValue + "&" + pgroupParam + 'QQMPUPYV';
            
            redirectUrl += "&" + checkInParam + checkInValue;
            redirectUrl += "&" + checkOutParam + checkOutValue;
            
            if (language !== null) {
                redirectUrl += '&' + languageParam + language;
            }

            if (roomValue !== '') {
                redirectUrl += "&" + roomParam + roomValue;
            }

            if (adultValue !== '') {
                redirectUrl += "&" + adultParam + adultValue;
            }

            if (childValue !== '') {
                redirectUrl += "&" + childParam + childValue;
            }

            if (accesscodeValue !== '') {
                redirectUrl += "&" + accesscodeParam + accesscodeValue;
            }
            
            function gotoURL() {
                window.open(decorateGACrossDomainTracking(redirectUrl));
            }
			
			$.beforeSubmit = settings.beforeSubmit;
            $.beforeSubmit();
            
            if($validate.length !== 0) {
                $validate.each(function(i, e) {
                    var $elm = $this.find(e);
                    if(validation($elm) === false) {
                        $elm.addClass('error');
                        $.error = settings.validateError;
                        $.error();
                        return false;
                    } else {
                        $elm.removeClass('error');
                        gotoURL();
                    }
                });
            } else {
                gotoURL();
            }
            
            $.afterSubmit = settings.afterSubmit;
            $.afterSubmit();
        });
    };



    // Initialize checkin and checkout date picker.
    function onSelectedCheckinDate(targetId, selectedDate) {
        if (selectedDate) {
            var tmpDate = new Date(selectedDate);
            var minCheckoutDateInMilliSecond = tmpDate.getTime() + 86400000;
            tmpDate = new Date(minCheckoutDateInMilliSecond);
            targetId.datepicker("option", "minDate", tmpDate);
            targetId.datepicker("show");
        }
    }



    // Initialize checkout picker.
    function beforeShowCheckOutDate(calendarCheckinId, calendarCheckoutId, inst) {
        var checkInDate = calendarCheckinId.val();
        var tmpDate = new Date();
        if (checkInDate) {
            tmpDate = new Date(checkInDate);
        }

        var minCheckoutDateInMilliSecond = tmpDate.getTime() + 86400000;
        tmpDate = new Date(minCheckoutDateInMilliSecond);
        calendarCheckoutId.datepicker("option", "minDate", tmpDate);
    }
    
    
    
    // Generate Google tracking script
    function decorateGACrossDomainTracking(url) {
        var output = url;
        try {
            if(!ga) return output;
            ga(function (tracker) {
                if(!tracker) return output;
                if(!tracker.get('linkerParam')) return output;
                var linker = new window.gaplugins.Linker(tracker);
                output = linker.decorate(url);
            });
        } catch (e) {
            console.log(e.message);
        }
        return output;
    }

})(jQuery);