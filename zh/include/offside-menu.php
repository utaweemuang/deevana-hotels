<div id="m-ourhotels-menu" class="offside-menu">
    <h2>OUR HOTELS</h2>
    
    <h3>PHUKET</h3>
    <ul class="sub-menu level-2">
        <li><a href="/deevanaplazaphuket">Deevana Plaza Phuket Patong</a></li>
        <li><a href="/deevanapatong">Deevana Patong Resort &amp; Spa</a></li>
        <li><a href="/ramadaphuketdeevana">Ramada Phuket Deevana</a></li>
        <li><a href="/recentasuitephuket">Recenta Suite Phuket Suanluang</a></li>
		<li><a href="/recentaphuket">Recenta Phuket Suanluang</a></li>
		<li><a href="/recentastyle">Recenta Style Phuket Town</a></li>
    </ul>
    
    <h3>KRABI</h3>
    <ul class="sub-menu level-2">
        <li><a href="/deevanaplazakrabi">Deevana Plaza Krabi Aonang</a></li>
        <li><a href="/deevanakrabiresort">Deevana Krabi Resort</a></li>
    </ul>
    
    <span class="close">&times;</span>
</div>

<div id="m-offers-menu" class="offside-menu">
	<h2>SPECIAL OFFERS</h2>
    
    <h3>PHUKET</h3>
    <ul class="sub-menu level-2">
        <li><a href="<?php ibe_url( '275', 'en' ); ?>" target="_blank">Deevana Plaza Phuket Patong</a></li>
		<li><a href="<?php ibe_url( '277', 'en' ); ?>" target="_blank">Deevana Patong Resort &amp; Spa</a></li>
		<li><a href="<?php ibe_url( '278', 'en' ); ?>" target="_blank">Ramada Phuket Deevana</a></li>
		<li><a href="<?php ibe_url( '299', 'en' ); ?>" target="_blank">Recenta Suite Phuket Suanluang</a></li>
		<li><a href="<?php ibe_url( '294', 'en' ); ?>" target="_blank">Recenta Phuket Suanluang</a></li>
		<li><a href="<?php ibe_url( '310', 'en' ); ?>" target="_blank">Recenta Style Phuket Town</a></li>
    </ul>
    
    <h3>KRABI</h3>
    <ul class="sub-menu level-2">
        <li><a href="<?php ibe_url( '276', 'en' ); ?>" target="_blank">Deevana Plaza Krabi Aonang</a></li>
        <li class="hidden"><a href="#">Deevana Krabi Resort</a></li>
    </ul>
    
    <span class="close">&times;</span>
</div>