<div class="booking-bar">
    <div class="inner">
        <div class="container">
            <form id="booking-form" class="form">
                <div class="table">

                    <div class="table-cell table-cell-header">
                        <span class="field field-header">
                            <img class="block responsive" src="assets/elements/book_online_best_rate_guarantee-cn.png" alt="Book Online Best rate guarantee" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-hotel">
                        <span class="field field-hotel">
                            <select id="hotel" class="input-select" name="propertyId" required
                                    data-error-msg="please select hotel">
                                <option value="" selected>- 选择酒店 -</option>
                                <optgroup label="PHUKET">
                                    <option value="275">普吉岛芭东蒂瓦纳广场酒店</option>
                                    <option value="277">芭东蒂瓦纳度假水疗中心</option>
                                    <option value="278">瑞玛达普吉岛蒂瓦纳酒店</option>
                                    <option value="299">普吉岛双廊瑞森塔套房酒店</option>
                                    <option value="294">普吉岛双廊瑞森塔酒店</option>
                                    <option value="310">普吉镇瑞森塔快捷酒店</option>
                                </optgroup>
                                <optgroup label="KRABI">
                                    <option value="276">甲米奥南蒂瓦纳广场酒店</option>
                                    <option value="386">甲米蒂瓦纳度假村</option>
                                </optgroup>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkin">
                        <span class="field field-checkin">
                            <input id="checkin" class="input-text date" name="checkin" placeholder="Check-in" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkout">
                        <span class="field field-checkout">
                            <input id="checkout" class="input-text date" name="checkout" placeholder="Check-out" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-adults">
                        <span class="field field-adults">
                            <label class="label">成人</label>
                            <select id="adults" class="input-select" name="numofadult">
                                <option value="1">1 成人</option>
                                <option value="2" selected>2 成人</option>
                                <option value="3">3 成人</option>
                                <option value="4">4 成人</option>
                                <option value="5">5 成人</option>
                                <option value="6">6 成人</option>
                                <option value="7">7 成人</option>
                                <option value="8">8 成人</option>
                                <option value="9">9 成人</option>
                                <option value="10">10 成人</option>
                                <option value="11">11 成人</option>
                                <option value="12">12 成人</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-children">
                        <span class="field field-children">
                            <label class="label">兒童</label>
                            <select id="children" class="input-select" name="numofchild">
                                <option value="0" selected>0 兒童</option>
                                <option value="1">1 兒童</option>
                                <option value="2">2 兒童</option>
                                <option value="3">3 兒童</option>
                                <option value="4">4 兒童</option>
                                <option value="5">5 兒童</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-rooms">
                        <span class="field field-rooms">
                            <label class="label">客房</label>
                            <select id="rooms" class="input-select" name="numofroom">
                                <option value="1" selected>1 客房</option>
                                <option value="2">2 客房</option>
                                <option value="3">3 客房</option>
                                <option value="4">4 客房</option>
                                <option value="5">5 客房</option>
                                <option value="6">6 客房</option>
                                <option value="7">7 客房</option>
                                <option value="8">8 客房</option>
                                <option value="9">9 客房</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-code">
                        <span class="field field-code">
                            <input id="accesscode" class="input-text" name="accesscode" placeholder="促銷代碼" type="text" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-submit">
                        <span class="field field-submit">
                            <button id="submit" class="button" type="submit">现在预定</button>
                        </span>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>