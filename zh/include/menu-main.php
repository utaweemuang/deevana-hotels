<ul class="list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">主页</a></li>
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">我们的酒店</a>
        <ul class="sub-menu level-1">
            <li class="label">Phuket</li>
            <li><a href="/zh/deevanaplazaphuket">普吉岛芭东蒂瓦纳广场酒店</a></li>
            <li><a href="/zh/deevanapatong">芭东蒂瓦纳度假水疗中心</a></li>
            <li><a href="/zh/ramadaphuketdeevana">瑞玛达普吉岛蒂瓦纳酒店</a></li>
            <li><a href="/zh/recentasuitephuket">普吉岛双廊瑞森塔套房酒店</a></li>
            <li><a href="/zh/recentaphuket">普吉岛双廊瑞森塔酒店</a></li>
            <li><a href="/zh/recentastyle">普吉镇瑞森塔快捷酒店</a></li>
            <li class="label">Krabi</li>
            <li><a href="/zh/deevanaplazakrabi">甲米奥南蒂瓦纳广场酒店</a></li>
            <li><a href="/zh/deevanakrabiresort">甲米蒂瓦纳度假村</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('our-brand'); ?>"><a href="our-brands.php">我们的品牌</a></li>
    <li class="has-sub-menu">
        <a href="/zh/meeting-events.php">會議與活動</a>
        <ul class="sub-menu">
            <li class="label">Phuket</li>
            <li><a href="/zh/deevanaplazaphuket/meetings-and-event.php" target="_blank">普吉岛芭东蒂瓦纳广场酒店</a></li>
            <li><a href="/zh/deevanapatong/facilities.php#mice_facilities">芭东蒂瓦纳度假水疗中心</a></li>
            <li><a href="/zh/ramadaphuketdeevana/meetings-and-event.php" target="_blank">瑞玛达普吉岛蒂瓦纳酒店</a></li>
            <li class="label">Krabi</li>
            <li><a href="/zh/deevanaplazakrabi/meetings-and-event.php" target="_blank">甲米奥南蒂瓦纳广场酒店</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">景点</a>
        <ul class="sub-menu level-1">
            <li class="<?php echo get_current_class('attraction-phuket'); ?>"><a href="attraction-phuket.php">PHUKET</a></li>
            <li class="<?php echo get_current_class('attraction-krabi'); ?>"><a href="attraction-krabi.php">KRABI</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">特别优惠</a>
        <ul class="sub-menu level-1">
            <li class="label">Phuket</li>
            <li><a href="<?php ibe_url(275, 'zh'); ?>" target="_blank">普吉岛芭东蒂瓦纳广场酒店</a></li>
            <li><a href="<?php ibe_url(277, 'zh'); ?>" target="_blank">芭东蒂瓦纳度假水疗中心</a></li>
            <li><a href="<?php ibe_url(278, 'zh'); ?>" target="_blank">瑞玛达普吉岛蒂瓦纳酒店</a></li>
            <li><a href="<?php ibe_url(299, 'zh'); ?>" target="_blank">普吉岛双廊瑞森塔套房酒店</a></li>
            <li><a href="<?php ibe_url(294, 'zh'); ?>" target="_blank">普吉岛双廊瑞森塔酒店</a></li>
            <li><a href="<?php ibe_url(310, 'zh'); ?>" target="_blank">普吉镇瑞森塔快捷酒店</a></li>
            <li class="label">Krabi</li>
            <li><a href="<?php ibe_url(276, 'zh'); ?>" target="_blank">甲米奥南蒂瓦纳广场酒店</a></li>
            <li><a href="<?php ibe_url(386, 'zh'); ?>">甲米蒂瓦纳度假村</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">联系我们</a></li>
</ul>