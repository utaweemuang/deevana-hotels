<?php
$title = 'Facilities | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc = 'Facilities: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw = 'facilities, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanakrabiresort/facilities.php';
$lang_th = '/th/deevanakrabiresort/facilities.php';
$lang_zh = '/zh/deevanakrabiresort/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/facilities-slide-02.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-03.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-04.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-05.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">设备 &amp; 服务</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <!-- <span data-tab="#swan_spa" class="tab active">Orientala Spa</span> -->
                    <!--span data-tab="#argus_fitness" class="tab">Argus Fitness</span-->
                    <!-- <span data-tab="#myna_kids_club" class="tab">Myna 儿童俱乐部</span> -->
                    <span data-tab="#swimming_pool" class="tab active">游泳池 (Swimming Pool)</span>
                    <span data-tab="#restaurant" class="tab">Restaurant &amp; Bar</span>
                    <span data-tab="#the_port_library" class="tab">港口图书馆</span>
                    <!-- <span data-tab="#pigeon_library_and_internet_corner" class="tab">鸽子 图书馆 &amp; 互联网角</span> -->
                </div>
                
                <div class="tabs-content">
                    <!-- <article id="swan_spa" class="article" data-tab-name="Swan Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swan_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Elegant and luxurious, Orientala Spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and herbal treatments. The perfect place to find respite, Orientala Spa features a private twin room with Jacuzzi and steam room, 2 private single rooms, and  special set up in open-air space for foot reflexology and authentic Thai massage.</p>
                                    <p>Orientala Spa offers an extensive menu of rejuvenating massage and body services, each created to aid relaxation and improve wellbeing. The spa’s skilled therapists have taken the best indigenous knowledge and combined it with leading contemporary practice to create a superior wellness experience. Guests can enjoy the exotic and relaxing qualities of Thai herbs oils and other superior wellness products used in individual treatments or special spa packages.</p>
                                    <p><span style="color: #516819;">Orientala Spa ensures a truly indulgent and memorable spa journey daily : from 9 am. – 9 pm.<br> Location: Lobby level of the Reception Building</span></p>
                                </div>
                            </div>
                        </div>
                    </article> -->
                    
                    <!--article id="argus_fitness" class="article" data-tab-name="Argus Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_argus_fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Argus Fitness</h1>
                                    <p>Guests wishing to keep in shape during their holiday at Deevana Plaza Krabi Aonang can use the well- equipped fitness center at  free of charge. Joggers can also enjoy an early morning run along the beautiful Noppharat Thara Beach.</p>
                                    <p><span style="color: #516819;">Argus Fitness Centre is open daily : from 7 am. – 9 pm.<br> Location : Ground floor of Building 1</span></p>
                                </div>
                            </div>
                        </div>
                    </article-->
                    
                    <!-- <article id="myna_kids_club" class="article" data-tab-name="Myna Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_myna_kid's_club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Myna 儿童俱乐部</h1>
                                    <p>希望在Deevana Plaza Krabi Aonang度假期间保持身材的客人可以免费使用设备齐全的健身中心. 慢跑者还可以沿着美丽的Noppharat Thara海滩享受清晨的时光.</p>
                                    <p><span style="color: #516819;">Argus健身中心每日开放：上午7点。 - 晚上9点.<br> 位置：1楼1楼</p>
                                </div>
                            </div>
                        </div>
                    </article> -->
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swimming_pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池 (Swimming Pool)</h1>
                                    <p>豪华的游泳池在两个酒吧旁边，围绕泳池的一些树木可以遮光，在泳池旁我们为您提供了日光躺椅来休息，这是一个完美的来休闲放松的好地方。我们在泳池旁的酒吧为您提供饮品与小吃。</p>
                                    <p><span style="color: #516819;">开放时间：早上7点到晚上7点</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pakarang_bar.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">泳池旁酒吧 (Pakarang Pool Bar)</h1>
                                    <p>本酒吧为您提供了冰镇啤机、软饮与鸡尾酒，这是一个绝妙的放松场所。另外，我们还有轻食菜单与小吃可供选择。</p>
                                    <p><span style="color: #516819;">开放时间：早上10点到晚上7点</span></p>
                                </div>
                            </div>
                            <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_chaolay.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title"> 餐厅 (Chao Lay Restaurant)</h1>
                                    <p>本餐厅以传统的“渔夫的秘密”为装修风格，在这里顾客可以品尝到来自世界各地的美味。丰富的传统与国际早餐自助供客人在室外与室内享用，午餐和午餐也提供了丰富的选择，包括了餐厅内有名的特色泰餐与国外食物，在这里你可以体验到优质的服务与愉悦舒心的用餐环境。</p>
                                    <p><span style="color: #516819;">每日开放时间：早上6点到晚上11点</span></p>
                                </div>
                            </div>
                            <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_boat_bar.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">船家酒吧 (Boat Bar)</h1>
                                    <p>这家酒吧在游泳旁边，是一个非常棒的地方。渔民风格的装修非常独特，酒吧提供各种饮品，包括咖啡、啤酒、鸡尾酒、无酒精的鸡尾酒，还有一些国际化的美味食物和小吃。</p>
                                    <p><span style="color: #516819;">开放时间：早上10点到晚上12点</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <!-- <article id="pigeon_library_and_internet_corner" class="article" data-tab-name="Pigeon Library &amp; Internet Corner">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pigeon_library_and_internet_corner.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">鸽子 图书馆 &amp; 互联网角</h1>
                                    <p>客人可以在度假村储藏丰富的图书馆享受安静的时光。 提供各种书籍，本地和国际报纸和杂志。 也可以借用DVD光碟在舒适的客房内观看。 互联网连接和电脑设备也可免费在Pigeon Conner购买.</p>
                                </div>
                            </div>
                        </div>
                    </article> -->

                    <article id="the_port_library" class="article" data-tab-name="The Port Library">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_liabary.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">港口图书馆 (The Port Library)</h1>
                                    <p>顾客可以在度假村享受一个安宁的时光，本图书馆藏书丰富，有当地和国际性报纸，也提供各种杂志。另外，舒适的待客室供客人观看DVD视频，我们也为您提供网络与电脑设备。</p>
                                    <p><span style="color: #516819;">开放时间：早上7点到晚上9点</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>