(function($) {
    //FastClick.attach(document.body);
    
    $(function() {
        
        /* =Simple Toggle
        --------------------------------------------------------------------- */
        
        $.fn.simpleToggle = function(activeClass, target, outFocus) {
            var $this = $(this);
            var $target = $(target);
            var aClass = activeClass;
            
            $this.on('click', function() {
                if( $this.hasClass(aClass) ) {
                    $this.removeClass(aClass);
                    $target.removeClass(aClass);
                } else {
                    $this.addClass(aClass);
                    $target.addClass(aClass);
                }
            });
            
            if( outFocus === true ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass(aClass);
                    }
                });
            }
        }
        
        /* =Scroll Effect
        --------------------------------------------------------------------- */
        
        $.fn.scrollFX = function( options ) {
            var defaults = $.extend({
                toggleClass: 'scroll-fx',
                moreTarget: null,
                trigger: window,
                offset: 0,
                disableOnWidth: 0,
                debug: false,
            }, options);
            
            var $this = $(this);
            var tClass = defaults.toggleClass;
            var trg = defaults.trigger;
            var off = defaults.offset;
            var tar = defaults.moreTarget;
            
            if( $this.length > 0 ) {
                var type = $.type(trg);
                
                function getPosition() {
                    var val;
                    switch(type) {
                        case 'number': val = trg; break;
                        case 'string': val = $(trg).offset().top; break;
                        case 'object': val = $(trg).scrollTop(); break;
                    }
                    return val;
                }

                function runFX() {
                    var objPos = Math.floor( $this.offset().top );
                    var trgPos = Math.floor( getPosition() );
                    var winWi = Math.floor( $(window).width() );
                    
                    if( type == 'number' ) {
                        if( winWi > defaults.disableOnWidth ) {
                            if( objPos >= (trgPos - off) ) {
                                $this.addClass(tClass);
                                $(tar).addClass(tClass);
                            } else {
                                $this.removeClass(tClass);
                                $(tar).removeClass(tClass);
                            }
                        } else {
                            $this.removeClass(tClass);
                            $(tar).removeClass(tClass);
                        }
                    }
                    
                    if( type == 'object' ) {
                        if( winWi > defaults.disableOnWidth ) {
                            if(trgPos >= (objPos - off)) {
                                $this.addClass(tClass);
                                $(tar).addClass(tClass);
                            } else {
                                $this.removeClass(tClass);
                                $(tar).removeClass(tClass);
                            }
                        } else {
                            $this.removeClass(tClass);
                            $(tar).removeClass(tClass);
                        }
                    }
                    
                    if( defaults.debug == true ) {
                        console.log( 'Trigger: '+trgPos+', Object: '+objPos+', with Offset: '+(objPos - off)+', Offset only: '+off);
                    }
                }

                $(window).on('load', function() {
                    runFX();
                });

                $(window).on('resize', function() {
                    runFX();
                });

                $(window).on('scroll',function() {
                    runFX();
                });
                
            };
        }
        
        /* =Room Scripts
        ---------------------------------------------------------------------- */
        
        if( $('body').hasClass('room') ) {
            var $slide = $('.room-slides');
            var $tnav = $('.room-slides-thumbs .thumbs');

            function slideFX( autoplay ) {
                var items = $slide.find('img').length;
                var autoplay = ( items > 1 ) ? autoplay : 0;
                $slide.superslides({
                    inherit_width_from: '.room-slides-wrap',
                    inherit_height_from: '.room-slides-wrap',
                    play: autoplay,
                    pagination: false,
                    animation: 'fade',
                });

                $slide.on('animated.slides', function() {
                    var i = $slide.superslides('current');
                    $tnav.find('li').eq(i).addClass('current').siblings().removeClass('current');;
                });

                $tnav.on('click', 'li', function() {
                    var $thm = $(this);
                    var i = $thm.index();
                    if( ! $thm.hasClass('current') ) {
                        $thm.addClass('current').siblings().removeClass('current');
                        $slide.superslides('animate', i);
                    }
                });
                
                $slide.swipe({
                    swipeRight: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('prev');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                    swipeLeft: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('next');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                });
            }

            slideFX(8000);

            function hideRoomContent() {
                $('html').addClass('hide-content');
            }
            
            function showRoomContent() {
                $('html').removeClass('hide-content');
            }
            
            $('#hide_content').on('click', function() {
                hideRoomContent();
            });

            $('#toggle_content').on('click', function() {
                $('html').toggleClass('hide-content');
            });

            function roomContentPos( breakpoint, offset ) {
                var breakpoint = breakpoint || 0;
                var $rcon = $('.room-info');
                var rconHi = $rcon.outerHeight();
                var rconWi = $rcon.outerWidth();
                var offset = offset || 0;

                if( $(window).width() > breakpoint ) {
                    $rcon.css({
                        marginLeft: -rconWi/2,
                        marginTop: -(rconHi/2) + offset,
                    });
                } else {
                    $rcon.removeAttr('style');
                }
            }

            function roomScript( breakpoint ) {
                var breakpoint = breakpoint || 0;
                $(window).on('resize', $.throttle( 100, function() {
                    var winWi = $(window).width();
                    if( winWi > breakpoint ) {
                        $('html').addClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                    } else {
                        $('html').removeClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                        showRoomContent();
                    }
                })).trigger('resize');
            }

            roomScript(960);
            
            // Room booking
            $('#room-booking-form').booking({
                checkInSelector: '#checkin',
                checkOutSelector: '#checkout',
                adultSelector: '#adults',
                childSelector: '#children',
                roomSelector: '#rooms',
                codeSelector: '#accesscode',
                submitSelector: '#submit',
				propertyId: 386,
				onlineId: 5,
                language: 'zh',
            });

            // Amenities Popup
            $('.more').magnificPopup({
                delegate: 'a',
                type: 'inline',
                mainClass: 'mfp-fade',
                removalDelay: 300,
            });
        }
        
        /* =Validation
        ---------------------------------------------------------------------- */
        
        $.validate({
            form: '#contact_form',
            modules: 'location, html5',
            onModulesLoaded: function() {
                $('input[name="country"]').suggestCountry();
            },

            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                }).done(function(status) {
                   if (status === 'ok') {
                       //console.log('Sending Successful');

                       $(form).find(':focus').blur();
                       $(form)[0].reset();
                       grecaptcha.reset();
                       
                       //Display form result
                       $(form).find('#contact_result').addClass('success');
                       $('#contact_result').html('<i class="icon fa fa-check-circle"></i> Thank you for getting in touch!');
                       $('#contact_result').delay(200).slideDown(200);
                   } else if( status === 'not' ) {
                       //console.log('Sending Failed because Google Recaptcha');
                       
                       $(form).find('.g-recaptcha-error').text('Please check the recaptcha');
                   } else {
                       console.log('Unknowed error');
                       //$(form).addClass('form-error');
                       //$(form).find('#form_result').html('Sorry, There is something error, Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a>').slideDown(200);
                   }
                });

                return false;
            },
        });

        $.validate({
            form: '#newsletter-form',
            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.post(url, formData, function( data ) {
                    if( data == '' ) {
                        $(form)[0].reset();
                        $(form).find(':focus').blur();
                        $(form).addClass('completed').after('<div class="subscribe-form-result" />');
                        $(form).next('.subscribe-form-result').html('<i class="icon fa fa-check-circle"></i> Subscription Successful');
                    } else {
                        $(form).after('<div class="form-error-log"><span style="color: tomato;">Subscription Failed.</span><br/>Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a></div>');
                    }
                });
                return false;
            }
        });

        $('#booking-form').booking({
            checkInSelector: '#checkin',
            checkOutSelector: '#checkout',
            adultSelector: '#adults',
            childSelector: '#children',
            roomSelector: '#rooms',
            codeSelector: '#accesscode',
            propertyId: 386,
            onlineId: 5,
            language: 'zh',
        });
        
        $('.site-header').scrollFX({
            toggleClass: 'mini',
            trigger: 150,
            disableOnWidth: 1200,
        });
        
        $('.booking-bar').scrollFX({
            offset: 60,
            toggleClass: 'fixed',
            disableOnWidth: 1200,
        });
        
        var $heroSlider = $('.hero-slider');
        var $loop = ( $heroSlider.find('.item').length > 1 ) ? true : false;
        $('.hero-slider').owlCarousel({
            items: 1,
            loop: $loop,
            pullDrag: false,
            smartSpeed: 800,
            dots: false,
            nav: true,
            navContainer: '.custom-hero-slide-nav',
            navText: ['<span class="sprite arrow-left"></span>', '<span class="sprite arrow-right"></span>'],
        });
        
        //Switch languages function
        var $langSelector = $('#language_select');
        var defaultLang = $('#language_select').find('.option.selected a').html();
        $langSelector.simpleToggle('visible', null, true);
        $langSelector.find('.active').html(defaultLang);
        
        $('.toggle-sub-menu').each(function() {
            var $this = $(this);
            
            $this.children('a').on('click', function(e) {
                e.preventDefault();
                if( $this.hasClass('visible') ) {
                    $this.removeClass('visible');
                } else {
                    $this.addClass('visible');
                }
            });
            
            //Run script in .site-navigation only
            if( $this.closest('.site-navigation').length === 1 ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass('visible');
                    }
                });
            }
        });
        
        var $currentMenu = $('#offside-menu').find('.current');
        if( $currentMenu.closest('.ancestor').length > 0 ) {
            $currentMenu.parents('.ancestor').addClass('visible');
        }

        //
        // New side menu
        //
        $('.side-panel').on('click', '.has-sub-menu', function() {
            if ($(this).hasClass('visible')) {
                $(this).removeClass('visible');
                $(this).children('.sub-menu').slideUp(350);
            } else {
                $(this).addClass('visible');
                $(this).children('.sub-menu').slideDown(350);
            }
        });

        $('.side-panel-trigger').on('click', function() {
            if (sidePanel()) {
                sidePanel('hide');
            } else {
                sidePanel('show');
            }
        });

        $('.side-panel-bg').on('click', function() {
            if (sidePanel()) {
                sidePanel('hide');
            } else {
                sidePanel('show');
            }
        })

        $('.side-panel-drag').swipe({
            swipeRight: function() {
                sidePanel('show');
            }
        });

        $('.side-panel-bg').swipe({
            swipeLeft: function() {
                sidePanel('hide');
            }
        })

        function sidePanel(methods) {
            switch (methods) {
                case 'show':
                    $('body').addClass('side-panel-show');
                    break;
                case 'hide':
                    $('body').removeClass('side-panel-show');
                    break;
                default:
                    return $('body').hasClass('side-panel-show');
            }
        }

        $(window).on('resize', $.throttle(16, function() {
            if (window.innerWidth >= 992) {
                sidePanel('hide');
            }
        }));

        //
        // Member Panel
        //
        var $m_display  = $('.member-display');
        var $m_btn      = $('.member-panel-trigger');
        var $m_panel    = $('.member-panel');
        var $m_form     = $('#form_member');
        var $m_close    = $m_panel.find('.btn-close');

        var memb  = $('#member');
        var mcont = $('.member-content');
        var mtrig = $('.member-content-trigger');
        var mclos = memb.find('.btn-close');

        $m_btn.on('click', function(e) {
            e.preventDefault();
            if (memberPanel()) {
                memberPanel('hide');
            } else {
                memberPanel('show');
            }
        });

        $m_close.on('click keypress', function(e) {
            e.preventDefault();
            memberPanel('hide');
            if (e.type == 'keypress' && e.keyCode == 32 || e.keyCode == 13)
                memberPanel('hide');
        });

        function memberPanel(methods) {
            switch (methods) {
                case 'show':
                    $m_btn.addClass('show');
                    $m_panel.slideDown(350, function() {
                        $m_panel.find('input[type="email"]').focus();
                    });
                    break;
                case 'hide':
                    $m_btn.removeClass('show');
                    $m_panel.slideUp(350);
                    break;
                default:
                    return $m_btn.hasClass('show');
            }
        }

        /**
         * Member Form
         */
        var memberCode = 'member';

        if (memberChecker()) {
            memberDisplay('member');
            $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
        } else {
            memberDisplay('guest');
        }

        $m_form.on('submit', function(event) {
            event.preventDefault();
            var email = $(this).find('[type="email"]').val();
            memberCreateCookies({
                code: 'member',
                expire: 7,
                user: {
                    email: email,
                }
            });
            var url = this.baseURI;
            memberSendData({
                formAPI: '648466c4ac12000257e768ffedac2e73',
                formType: 2,
                customer: {
                    email: email,
                    message: 'This information send from Deevana Krabi Resort (Group) by Member form',
                }
            });

            memberPanel('hide');
            memberDisplay('member');
            window.open( 'https://reservation.travelanium.net/propertyibe2/?propertyId=386&onlineId=5&lang=zh&pgroup=QQMPUPYV&accesscode='+memberCode, '_blank' );

            memberAppendCode(memberCode);
            $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
        });

        function memberDisplay(methods) {
            switch (methods) {
                case 'guest':
                    $m_display
                        .removeClass('is-memmber')
                        .addClass('is-guest');
                    break;
                case 'member':
                    $m_display
                        .removeClass('is-guest')
                        .addClass('is-member');
                    break;
            }
        }
    });
    
    $(window).on('load', function() {
        $('html').removeClass('preload');
    });
})(jQuery);