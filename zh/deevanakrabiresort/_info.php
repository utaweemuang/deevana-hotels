<?php

$url        = 'http://www.deevanahotels.com/deevanakrabiresort/';
$name       = 'Deevana Krabi Resort';
$version    = '20180402';
$email      = 'sco@deevanaplazakrabi.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/DeevanaKrabiResort/?fref=ts';
$twitter    = '#';
$googleplus = '#';
$youtube    = 'https://www.youtube.com/channel/UC_zIuBzbXVgu7ps388lY0Rg';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/deevanakrabiresort/';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = '#';

$ibeID      = '';