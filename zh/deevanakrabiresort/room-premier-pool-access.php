<?php
$title = 'Premier Pool Access | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc = 'Premier Pool Access: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw = 'premier pool access, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-premier-pool-access';
$cur_page = 'premier-pool-access';
$par_page = 'rooms';

$lang_en = '/deevanakrabiresort/room-premier-pool-access.php';
$lang_th = '/th/deevanakrabiresort/room-premier-pool-access.php';
$lang_zh = '/zh/deevanakrabiresort/room-premier-pool-access.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier-pool-access/room-01.jpg" alt="Premier Pool Access 01" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Premier Pool Access <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier-pool-access/room-01-thm.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Premier Pool Access</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier-pool-access/room-01-thm.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Guests of the spacious 34-square-metre Premier Pool Access rooms can slip off a private terrace and into the warm waters of the resort’s inviting lagoon pool. Rooms include a king size or twin beds, a large bathroom with a bathtub and Premier branded toiletries. Each room is equipped with a flat screen LCD with satellite channels and DVD player, Free Wi-Fi connection, IDD telephone with voicemail, coffee and tea making facilities, and safety deposit box, mini bar, alarm clock, make-up mirror, iron and ironing board plus a special terrace at the pool side with large chair for couple to relax day and night.</p>
                            <p>Rooms available: 28</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 34 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Fully sea view / beach door step</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>