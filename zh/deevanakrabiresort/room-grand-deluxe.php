<?php
$title = 'Grand Deluxe Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc = 'Grand Deluxe Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw = 'Grand Deluxe Room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-grand-deluxe';
$cur_page = 'grand-deluxe';
$par_page = 'rooms';

$lang_en = '/deevanakrabiresort/room-grand-deluxe.php';
$lang_th = '/th/deevanakrabiresort/room-grand-deluxe.php';
$lang_zh = '/zh/deevanakrabiresort/room-grand-deluxe.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="1500" height="986" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="1500" height="925" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="1500" height="954" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Grand Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">超豪华房间</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url('386', 'zh'); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>你会置身于一个充满地域特色和实用性的手工制作装饰品的环境。在一楼有个30平方米的大阳台，包含一个巨大的圆形浴缸和放松椅供您享用。有大床房和双床房可供你选择。干湿分离的淋浴设施供您使用，还有很多品质自然友好的其它设施。</p>
                            <p>房间数量：34</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url('386', 'zh'); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                            <li>40寸数字电视</li>
                                            <li>免费无线网</li>
                                            <li>茶喝咖啡设备</li>
                                            <li>保险箱</li>
                                            <li>迷你吧台</li>
                                            <li>吹风机</li>
                                        <li class="more clickable">
                                        	<a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>房间设施：</h2>
                                        <ul class="list-columns-2">
                                            <li>40寸数字电视</li>
                                            <li>免费无线网</li>
                                            <li>茶喝咖啡设备</li>
                                            <li>保险箱</li>
                                            <li>迷你吧台</li>
                                            <li>吹风机</li>
                                            <li>热/冷水</li>
                                            <li>浴袍</li>
                                            <li>雨伞</li>
                                            <li>沙滩包</li>
                                            <li>熨斗和熨衣板</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>