<?php
$title = 'Standard Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc = 'Standard Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw = 'Standard room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/deevanakrabiresort/room-deluxe-room.php';
$lang_th = '/th/deevanakrabiresort/room-deluxe-room.php';
$lang_zh = '/zh/deevanakrabiresort/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/standard-room/1500/Standard-Room-1.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-2.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-3.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-4.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-5.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-6.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-7.jpg" alt="Standard Room" width="1500" height="1045" />
                  
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">标准房<span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
					<li class="current"><img src="images/accommodations/standard-room/600/Standard-Room-1.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-2.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-3.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-4.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-5.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-6.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-7.jpg" alt="Standard Room" width="600" height="400" /></li>
					<!-- <li><img src="images/accommodations/deluxe-room/600/deluxe-room-06.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-03.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-04.jpg" alt="Deluxe Room" width="600" height="400" /></li> -->
				
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">标准房</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-room/600/deluxe-room-01.jpg" alt="Deluxe Room" width="600" height="400" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url('386', 'zh'); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>天花板有令人放松的独特的设计和温馨的装饰。26平方米大的私人阳台为你提供安静思索的慰藉。大床房和双床房可供您选择，还有泰国风格的座椅。有干湿分离的淋浴和盆池设施以及自然温和的化妆品。</p>
                            <p>房间数量：27</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url('386', 'zh'); ?>">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                            <li>寸数字电视</li>
                                            <li>免费无线网</li>
                                            <li>茶喝咖啡设备</li>
                                            <li>保险箱</li>
                                            <li>迷你吧台</li>
                                            <li>吹风机</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>房间设施：</h2>
                                        <ul class="list-columns-2">
                                            <li>寸数字电视</li>
                                            <li>免费无线网</li>
                                            <li>茶喝咖啡设备</li>
                                            <li>保险箱</li>
                                            <li>迷你吧台</li>
                                            <li>吹风机</li>
                                            <li>热/冷水</li>
                                            <li>浴袍</li>
                                            <li>雨伞</li>
                                            <li>沙滩包</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>