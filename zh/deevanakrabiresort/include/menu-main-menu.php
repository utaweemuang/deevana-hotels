<ul class="menu list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="../deevanakrabiresort/">家</a></li>
    <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">住宿</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">标准房</a></li>
            <li class="<?php echo get_current_class('grand-deluxe'); ?>"><a href="room-grand-deluxe.php">尊貴豪華客房</a></li>
            <li class="<?php echo get_current_class('duplex'); ?>"><a href="room-duplex.php">複式房</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">设备</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">景點</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('386', 'zh'); ?>">提升</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">畫廊</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">聯繫</a></li>
</ul>
