<?php

session_start();

$title = 'Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc = 'Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw = 'deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanakrabiresort';
$lang_th = '/th/deevanakrabiresort';
$lang_zh = '/zh/deevanakrabiresort';

include_once('_header.php'); ?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/slides-hero/home-slide-01.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-1.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-2.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-3.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-4.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
        </div>

        <!--div id="promotion_board" class="promotion get-center">
            <a href="https://reservation.travelanium.net/propertyibe2/?propertyId=386&onlineId=4">
                <img class="block responsive" src="images/home/discount_bann.png" />
            </a>
        </div-->
        <?php //include 'include/tl-sticky-banner.php'; ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到<br><span style="color:#244289;">甲米蒂瓦娜度假胜地</span></span>
                        </h1>

                        <p><img width="220" class="align-left" style="border: 3px solid #fff;" src="images/home/content-img1.jpg" /></p>
						<p>度假村位于甲米的奥南海滩，最新升级的渔夫的秘密的特点的独特民居坐落在半山腰处原始种植园里，绿林环绕，鸟语花香。66间风格各异配有室外浴缸的客房，任您自由选择。另外还有免费的wi - fi服务,国际美食餐厅,两个独立的游泳池和酒吧、按摩大厅等。到甲米的著名旅游景点——美丽的Aonang海滩和Noppharathara海滩，步行只需10分钟。</p>
                        <p><span class="button luxury-style">独一无二的全方位服务满足您所有的需求</span></p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="d-block" style="text-align: right;">
                            <img src="./images/home/adult-only.png" alt="" width="424" height="120">
                        </div>
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/_Xxx5Ddl0lY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">欢迎来到<br><span style="color:#244289;">甲米蒂瓦娜度假胜地</span></span>
                        </h1>

                        <p><img width="220" class="align-left" style="border: 3px solid #fff;" src="images/home/content-img1.jpg" /></p>
						<p>度假村位于甲米的奥南海滩，最新升级的渔夫的秘密的特点的独特民居坐落在半山腰处原始种植园里，绿林环绕，鸟语花香。66间风格各异配有室外浴缸的客房，任您自由选择。另外还有免费的wi - fi服务,国际美食餐厅,两个独立的游泳池和酒吧、按摩大厅等。到甲米的著名旅游景点——美丽的Aonang海滩和Noppharathara海滩，步行只需10分钟。</p>
                        <p><span class="button luxury-style">独一无二的全方位服务满足您所有的需求</span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include 'modules/widget-countdown/countdown.php'; ?>
                    </div>
                </div> -->
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/deevanakrabiresort/images/promotion/special-save-50-DKR-01.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Special Deal Offer</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>Free late check out till 14.00 hrs</li>
                                        <li>20% discount on Food and Beverage</li>
                                        <li>20% discount Spa</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDczMTEyMg%3D%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/deevanakrabiresort/images/promotion/stay-more-save-more.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Stay More Save More</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 3 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet(Wifi connection)</li>
                                        <li>Late check-out until 14.00 (Subject to available.)</li>
                                        <li>20% discount on Food and Beverage at hotel restaurant(except alcohol and mini bar)</li>
                                        <li>20% discount on Spa treatment (except package)</li>
                                        <li>THB 300 cash voucher per stay</li>
                                        <li>Stay 7 nights or more get round transfer from Krabi international airport to hotel</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDg0NTY1">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=276&group=13&width=450&height=300&imageid=8079&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>HONEYMOON PACKAGE</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 3 nights</p>
                                    <ul>
                                        <li>Daily breakfast at Chaolay Restaurant.</li>
                                        <li>In-room internet</li>
                                        <li>Late check-out until 14.00 (Subject to available)</li>
                                        <li>Romantic set up in room upon arrival day.</li>
                                        <li>One time of private Thai Set Dinner for 2 persons</li>
                                        <li>Thai cooking class Inclusive of menu recipe, apron and cap, Certificate and Photo with frame</li>
                                        <li>Thai massage 1 hour for 2 persons per stay</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDgyMzI4&lang=en&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <section id="facilities" class="section section-facilities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#7b9028;">独一无二的全方位服务满足您所有的需求</span></h1>

                <div id="facilities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_swimming_pool.jpg" />
                        </div>

                        <div class="caption">
                            <h2 class="title">Swimming Pool</h2>
                            <!-- <p class="description">Elegant and luxurious, Swan Spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and  herbal treatments.</p> -->
                            <p><a class="button" href="facilities.php#swimming_pool">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_liabary.jpg" />
                        </div>
                        
                        <div class="caption">
                            <h2 class="title">The Port Library</h2>
                            <!-- <p class="description">Elegant and luxurious, orientala spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and  herbal treatments.</p> -->
                            <p><a class="button" href="facilities.php#restaurant">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_chaolay.jpg" />
                        </div>

                        <div class="caption">
                            <h2 class="title">RESTAURANT</h2>
                            <!-- <p class="description">Deevana Plaza Krabi Aonang is home to three popular restaurant and bars. Guests can enjoy authentic Thai and delicious international</p> -->
                            <p><a class="button" href="facilities.php#restaurant">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title"><span class="deco-map">KRABI 景點</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phi_phi.jpg" /></div>
                            <h2 class="title">发埃发埃海岛</h2>
                            <a class="more" href="attraction.php#phi_phi_island">學到更多<i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-tiger_cave_temple.jpg" /></div>
                            <h2 class="title">老虎洞寺庙</h2>
                            <a class="more" href="attraction.php#tiger_cave_temple">學到更多<i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-emerald_pool.jpg" /></div>
                            <h2 class="title">翡翠池</h2>
                            <a class="more" href="attraction.php#emerald_pool">學到更多<i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/thalay_wak.jpg" /></div>
                            <h2 class="title">Thalay Wak</h2>
                            <a class="more" href="attraction.php#thalay_wak">學到更多<i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/thma-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<?php
if (!isset($_SESSION['visited'])) {

}
$_SESSION['visited'] = "true";
?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    #promotion_board img {
        *-webkit-transition: 150ms;
        *transition: 150ms;
    }
    #promotion_board img:hover {
        *-webkit-filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
        *filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
    }
	#intro {
		min-height: 500px;
	}
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #facilities {
        background-image: url(images/home/bg-facilities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #facilities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #facilities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #facilities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #facilities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #facilities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #facilities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #facilities_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#facilities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);
            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
        }
        
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<?php include 'include/popup-image.php'; ?>
<?php require('_footer.php'); ?>