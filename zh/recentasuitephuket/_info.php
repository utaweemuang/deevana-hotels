<?php

$url        = 'http://www.deevanahotels.com/zh/recentasuitephuket/';
$name       = 'Recenta Suite Phuket Suanluang';
$version    = '20180403';
$email      = 'rvn@recentahotels.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/Recenta-Suite-Phuket-Suanluang-1660248590930264';
$twitter    = '#';
$googleplus = '#';
$youtube    = '#';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/recentahotels/';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = 'https://www.tripadvisor.com/Hotel_Review-g2315814-d8776936-Reviews-Recenta_Suite_Phuket_Suanluang-Wichit_Phuket_Town_Phuket.html';

$ibeID      = '299';
