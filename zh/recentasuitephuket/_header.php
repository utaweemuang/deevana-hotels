<?php require_once('_functions.php'); ?>

<!DOCTYPE html>
<html lang="zh" class="<?php html_class(); ?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:image" content="http://deevanahotels.com/recentasuitephuket/images/gallery/set_20160510/1500/0002.jpg" />
	<meta property="og:url" content="<?php echo get_info('url'); ?>" />
	<meta property="og:title" content="Recenta Suite Phuket SuanLuang | Official Hotel Group Website Thailand" />
	<meta property="og:description" content="Enjoy best direct hotel rate; 3 star chic hotel in Phuket town near Suanluang park and city center." />

	<title><?php web_title(); ?></title>
    <?php web_desc(); ?>
	<?php web_keyw(); ?>

    <link rel="apple-touch-icon" sizes="57x57" href="assets/elements/favicon/apple-icon-57x57.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/elements/favicon/apple-icon-60x60.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/elements/favicon/apple-icon-72x72.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/elements/favicon/apple-icon-76x76.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/elements/favicon/apple-icon-114x114.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/elements/favicon/apple-icon-120x120.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/elements/favicon/apple-icon-144x144.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/elements/favicon/apple-icon-152x152.png?ver=<?php echo get_info('version'); ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/elements/favicon/apple-icon-180x180.png?ver=<?php echo get_info('version'); ?>">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/elements/favicon/android-icon-192x192.png?ver=<?php echo get_info('version'); ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/elements/favicon/favicon-32x32.png?ver=<?php echo get_info('version'); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/elements/favicon/favicon-96x96.png?ver=<?php echo get_info('version'); ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/elements/favicon/favicon-16x16.png?ver=<?php echo get_info('version'); ?>">
	<link rel="manifest" href="assets/elements/favicon/manifest.json?ver=<?php echo get_info('version'); ?>">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/elements/favicon/ms-icon-144x144.png?ver=<?php echo get_info('version'); ?>">
	<meta name="theme-color" content="#ffffff">
	
	<meta name="google-site-verification" content="UmRmlyqknjntJVtP1REHk_4qNyRpdVKzk7hnVX2FJug" />

    <!--Utilities-->
    <link rel="stylesheet" href="assets/css/normalize.css" />
    <script src="assets/plugins/html5shiv/html5shiv.min.js"></script>
    <script src="assets/plugins/html5shiv/html5shiv-printshiv.min.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-grid.css'/>

    <!--jQuery-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script>

    <!--Validator-->
    <link rel="stylesheet" href="assets/plugins/form-validator/theme-default.min.css" />
    <script src="assets/plugins/form-validator/jquery.form-validator.min.js"></script>

    <!--JqueryTouch-->
    <script src="assets/js/jquery.touchSwipe.min.js"></script>

    <!--Debounce-->
    <script src="assets/js/jquery.ba-throttle-debounce.min.js"></script>

    <!--Resizeend-->
    <script src="assets/js/jquery.resizeend.min.js"></script>

    <!--KeepRatio-->
    <script src="assets/js/jquery.keep-ratio.min.js"></script>

    <!--OwlCarousel-->
    <link rel="stylesheet" href="assets/plugins/owl.carousel.2.0.0-beta.3/assets/owl.carousel.min.css" />
    <script src="assets/plugins/owl.carousel.2.0.0-beta.3/owl.carousel.min.js"></script>

    <!--Superslides-->
    <link rel="stylesheet" href="assets/plugins/superslides-0.6.2/dist/stylesheets/superslides.css" />
    <script src="assets/plugins/superslides-0.6.2/dist/jquery.superslides.min.js"></script>

    <!--MagnificPopup-->
    <link rel="stylesheet" href="assets/plugins/magnific-popup/magnific-popup.css" />
    <script src="assets/plugins/magnific-popup/magnific-popup.js"></script>

    <!--FooTable-->
    <link rel="stylesheet" href="assets/plugins/FooTable-2/css/footable.core.css" />
    <script src="assets/plugins/FooTable-2/dist/footable.min.js"></script>

    <!--Booking-->
    <link rel="stylesheet" href="assets/plugins/booking-script/datepicker_theme.css" />
    <script src="assets/plugins/booking-script/booking-calendar.js"></script>

    <!--Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,300,500,500italic,700,700italic|Roboto+Condensed:300,300italic,400,400italic|Cinzel:400,700|Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'/>

    <!--Core-->
    <link rel="stylesheet" href="assets/css/main.css?ver=<?php echo get_info('version'); ?>" />
    <script src="assets/js/main.js?ver=<?php echo get_info('version'); ?>"></script>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css" />
    <![endif]-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-81334137-1', 'auto', {'allowLinker': true});
        ga('require', 'linker');
        ga('linker:autoLink', ['travelanium.net'] );
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        
        fbq('init', '215754318902215');
        fbq('track', 'PageView');
        fbq('track', 'Lead');
    </script>
    <!-- End Facebook Pixel Code -->

    <?php include('include/google_analytics.php'); ?>
</head>

<body class="<?php body_class(); ?>">
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=215754318902215&ev=PageView&noscript=1" /></noscript>
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M2SHMZ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    <div id="page">
        <header class="site-header">
            <div class="m-topbar">
                <div class="container">
                <span class="m-group-home">
                    <a href="/zh"><i class="icon-home"></i> Deevana Hotels &amp; Resorts</a>
                </span>

                <span class="m-social">
                    <a class="facebook" href="<?php echo get_info('facebook'); ?>" target="_blank"><i class="icon fa fa-facebook"></i></a>
                </span>
                </div>
            </div>

            <div class="main-header">
                <div class="container">
                    <div class="site-branding">
                        <a href="<?php echo get_info('url'); ?>">
                            <img src="assets/elements/logo-with-location.png" srcset="assets/elements/logo-with-location.png 1x, assets/elements/logo-with-location@2x.png 2x" alt="<?php echo get_info('name'); ?>" class="logo" width="190" height="84">
                        </a>
                    </div>

                    <div class="site-action">
                        <span class="button group-home">
                            <a href="/zh"><i class="icon-home"></i> Deevana Hotels &amp; Resorts</a>
                        </span>

                        <span class="social">
                            <a class="facebook" href="<?php echo get_info( 'facebook' ); ?>" target="_blank"><i class="icon fa fa-facebook"></i></a>
                        </span>

                        <span id="language_select" class="language">
                            <span class="active"></span>
                            <ul class="options">
                                <li class="option "><a href="<?php echo $lang_en; ?>">English</a></li>
                                <li class="option "><a href="<?php echo $lang_th; ?>">Thai</a></li>
                                <li class="option selected"><a href="<?php echo $lang_zh; ?>">Chinese</a></li>
                            </ul>
                        </span>

                        <span id="toggle_offside_menu" class="toggle-menu-button">
                            <span class="label">MENU <i class="icon fa fa-bars"></i></span>
                        </span>
                    </div>

                    <?php include_once('include/navigation.php'); ?>
                </div>
            </div>
        </header>
