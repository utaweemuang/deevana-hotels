<?php
$title = 'Facilities | Recenta Suite Phuket SuanLuang | Official Hotel Group Website Thailand';
$desc = 'Facilities: Enjoy best direct hotel rate; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'facilities, recenta suite, phuket, suan luang, suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/recentasuitephuket/facilities.php';
$lang_th = '/th/recentasuitephuket/facilities.php';
$lang_zh = '/zh/recentasuitephuket/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-01.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-02.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-03.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-04.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
        </div>
		
		<div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swimming_pool" class="tab active">游泳池</span>
                    <span data-tab="#fitness" class="tab">健身</span>
                    <span data-tab="#tour_desk" class="tab">旅游接待处</span>
                    <span data-tab="#playground" class="tab">游乐场</span>
                    <span data-tab="#transportation" class="tab">交通</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                    <span data-tab="#car_park" class="tab">停车场</span>
                </div>
                
                <div class="tabs-content">
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游泳池</h1>
                                    
                                    <p>您和您的家人可以花时间在成人、儿童游泳池里或游泳池旁边的沙滩椅上进行交流，当孩子在水中水花飞溅玩得不亦乐乎的时候，你可以躺在阳光下享受冷饮和阳光，同时仍能密切关注着他们。</p>
                                    <p><span style="color: #516819;">开放时间：早7点——晚七点<br>
                                    请注意：没有救生员</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="fitness" class="article" data-tab-name="Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">健身</h1>
                                    <p>我们的健身房位于一楼，可以俯瞰游泳池和花园,为了健身爱好者可以随时过来健身，房间每天从07:00 - 21.00小时开</p>
                                    <p><span style="color: #516819;">12岁以下儿童不允许使用健身器材<br>
                                        12—16岁必须有成年人陪伴使用</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="tour_desk" class="article" data-tab-name="Tour Desk">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tour_desk.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">旅游接待处</h1>
                                    <p>不管你是想要探索普吉岛及周边海域众多景点的一日欢乐游，还是在普吉岛古镇了解泰国文化或歌舞表演等等，我们都可以为您安排一切包括从酒店接送。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="playground" class="article" data-tab-name="Playground">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/playground.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">游乐场</h1>
                                    <p>孩子们娱乐和放松的地方</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="transportation" class="article" data-tab-name="Transportation">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/transportation.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">交通</h1>
                                    <p>我们普吉岛机场的班车是安全的可以信任的。请联系客游客服务中心安排出租车服务和机场班车和其他地方，我们有可靠的随时待命的司机安全地带你去你的目的地。</p>
                                </div>
                            </div>
                        </div>
                    </article>
					
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Message &amp; Spa: by Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/orientala_wellness_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Orientala Spa是一个是你身心放松和焕发生机的地方，我们非常自豪地推荐我们的服务——健康按摩，它会帮助你缓解压力,放松你的生活。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="car_park" class="article" data-tab-name="Car Park">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/car_park.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">停车场</h1>
                                    <p>免费私家停车场，方便出入。</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #707270;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #707270;
    }
    @media (max-width: 840px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            cursor: pointer;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>