<?php
$title = 'Krabi Attractions | Deevana Hotels & Resorts';
$desc = 'Krabi is the most relaxing part to be in all of Thailand, it is a province that has the most stunning scenery imaginable, beautiful white beaches that stretch on for miles, a jungle and over 200 islands just of the coast.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'attraction krabi';
$cur_page = 'attraction-krabi';

$lang_en = '/attraction-krabi.php';
$lang_th = '/th/attraction-krabi.php';
$lang_zh = '/zh/attraction-krabi.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/attraction/krabi/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        <div class="container">
		
			<h1 class="section-title underline">甲米景點</h1>
            
            <div class="row row-content">
                <div class="col-w3 col-navigation">
                    <div class="post-tabs tabs-group">
                        <ul>
							<li class="tab active" data-tab="#emerald_pool">翡翠池</li>
							<!-- <li class="tab" data-tab="#elephant_ride">大象骑</li> -->
							<li class="tab" data-tab="#phi_phi_island">發射島</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="emerald_pool" data-tab-name="Emerald Pool">
                        <img class="thumbnail force block" src="images/attraction/krabi/emerald_pool.png" />
                        <h1>翡翠池</h1>
                        <p>绿宝石池子或Sra Morakot在泰语中是一个真正的绝妙天然池子，它在森林的中心，拥有闪闪发亮的清澈的泉水。绿宝石池子坐落于Khao Pra - Bang Khram野生动物保护区。当地人把它叫做Khao Nor Chuchi 低洼森林，从klong thom办公区沿着4038号公路走18公里就可以找到。</p>
                        <p>游客们一到目的地，就会迫不及待地跳入清澈见底的泉水中，来享受放松身心的游泳。</p>
                        <p>Sra Morakot大概有1-2米深，直径20-25米。泉水的颜色会在阳光的照射下，变成浅绿色或者是宝石绿。</p>
                        <p>Sra Morakot的泉水，来自于一个名叫“蓝池”，“蓝池”在60米以外的山上。蓝池里的含有硫磺的、呈碱性的水经岩石的缝隙慢慢流到绿宝石池子里。而且，水总是很清澈，因为有高碳酸钙这种物质，使所有的悬浮颗粒沉降到池子底部。此外，藻类也不能生长在水中。</p>
                        <p>这里还建造了巴厘岛风情的小屋，是多年前专门为皇室准备的。</p>
                    </article>
                    
                    <!-- <article class="article" id="elephant_ride" data-tab-name="Elephant Ride">
                        <img class="thumbnail force block" src="images/attraction/krabi/elephant_ride.png" />
                        <h1>大象骑</h1>
                        <p>大象被视为泰国的象征，也是泰国人的护身符。大象身上有着褶皱的灰色皮肤，非常像树干，但是它们集蛮力与温柔于一身，非常敏捷，让人尊敬和爱戴。</p>
                        <p>随着野生大象的迅速消失，现在可以近距离接触这些迷人大象的最佳地点就在一些登山营地。在甲米只有几个这样的机构，而且必须要遵循严格的准则，这些准则由泰国国家畜牧部门所制定，为的是可以给动物们提供足够的食物、水和栖息地，让它们可以健康成长。</p>
                        <p>这些机构会让你近距离接触这些动物，包括体验它们生活的自然环境。让大象载着游客游玩意味着大象可以自己赚钱来让自己生存，而且可以最大限度地自由自在地生活。因为野外已经没有地方让它们自由生存，它们每天只可以吃昂贵的饲料。对于这些温和的巨型动物，唯一的选择就是乞讨、参与非法采伐或者是被困在动物园。</p>
                        <p>徒步旅行——一般我们可以骑一个小时的大象，也可以结合其他观光项目。可以即时预定，也可以在当地任何一个旅游机构预定。如果你不打算骑大象也没有关系，你可以直接去机构参观，或者给大象拍照。</p>
                    </article> -->
                    
                    <article class="article" id="phi_phi_island" data-tab-name="Phi Phi Island">
                        <img class="thumbnail force block" src="images/attraction/krabi/phi_phi_island.png" />
                        <h1>發射島</h1>
                        <p>皮皮岛是泰国岛屿中的超级明星。它早就已经出现在电影里面，并且它是全泰国讨论的话题。对于一些人来说，皮皮岛是他们去普吉的唯一理由。即使它被炒作得很厉害，游客们前往时也不会失望。皮皮岛的美丽会让人上瘾。当我们乘船靠近这些岛屿时，这些岛屿就好像是海上的城堡。头顶上是陡峭的悬崖，海滩后面是茂密的丛林。这种神奇美丽的景色，大家都会一见钟情。</p>
                        <p>我们为什么会爱这个地方，第二点就是悠闲的心情。在这两个岛上，phi phi leh完全没有生活区，另一个岛屿则是完全没有人工建造的路。没有时间表，没有忙碌的生活，我们在这里没有慌乱着急的理由。</p>
                    </article>
                </div>
            </div>
            
        </div>
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
            
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
			console.log(offset);
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    @media (max-width: 768px) {
        .row-content [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin: 0 -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
    }
</style>

<?php include_once('_footer.php'); ?>