var {task, src, dest, watch, series, parallel} = require('gulp');
var browser = require('browser-sync').create();

task('server', () => {
  return browser.init({
    open: false,
    server: false,
    proxy: {
      target: 'http://localhost/deevana-hotels',
    },
    port: 4000,
    ignore: ['node_modules'],
  });
});

var reload = done => {
  browser.reload();
  done();
}

task('watch', () => {
  watch([
    '**/*.html',
    '**/*.php',
    '!node_modules/**/*.html',
    '!node_modules/**/*.php',
  ], series(reload));
});

task('serve', parallel('server', 'watch'));