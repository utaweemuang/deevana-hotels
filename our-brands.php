<?php
$title = 'Our Brands of Deevana Hotels & Resorts';
$desc = 'Deevana Hotels & Resorts – The best Thailand’s local hotel chain. Provide comfortable accommodation in Phuket and Krabi';
$keyw = 'Deevana Plaza Krabi, Deevana Plaza Phuket, Deevana Patong Resort & Spa, Deevana Krabi Resort, Ramada Phuket Deevana, Recenta Suite, Recenta Phuket, Recenta Style';

$html_class = '';
$body_class = 'offers';
$cur_page = 'our-brands';

$lang_en = '/our-brands.php';
$lang_th = '/th/our-brands.php';
$lang_zh = '/zh/our-brands.php';

include_once('_header.php');
?>
        
<div id="ourbrands_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/our_brands/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <div class="main-content">
            <div class="container">
				
               	<div class="row row-header">
               		<div class="col-w3">&nbsp;</div>
					<div class="col-w9">
						<h1 class="section-title underline">
							<span style="color: #9A7B12;">Our Brands of Deevana Hotels &amp; Resorts</span>
						</h1>
					</div>
				</div>

                <div class="row row-content">
                    <div class="col-w4 col-navigation">
                        <div class="post-tabs tabs-group">
                            <ul>
                                <li class="tab active" data-tab="#landing" style="display: none;">Landing</li>
                                <li class="tab logo-tab deevana-plaza" data-tab="#deevana_plaza">Deevana Plaza</li>
                                <!-- <li class="tab logo-tab deevana-plaza-krabi" data-tab="#deevana_plaza_krabi">Deevana Plaza Krabi Aonang</li> -->
                                <li class="tab logo-tab deevana" data-tab="#deevana">Deevana</li>
                                <!-- <li class="tab logo-tab deevana-krabi" data-tab="#deevana_krabi">Deevana Krabi Resort </li> -->
                                <li class="tab logo-tab recenta-suite" data-tab="#recenta_suite">Recenta Suite Phuket Suanluang </li>
                                <li class="tab logo-tab recenta" data-tab="#recenta">Recenta Phuket Suanluang</li>
                                <li class="tab logo-tab recenta-express" data-tab="#recenta_express">Recenta Style Phuket Town </li>
                                <li class="tab logo-tab ramada" data-tab="#ramada_phuket_deevana">Ramada By Wyndham Phuket Deevana</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-w8 col-content tabs-content">
                        <article class="article hide-tab" id="landing">
							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-deevana-plaza.png" width="192" height="47" alt="Deevana Plaza"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-deevana_plaza.png" width="258" height="76" alt="Your choice for all occasions" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>Whether staying for business or pure pleasure, we take care of all your needs.</p>
								</div>
							</div>

							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-deevana-hotels.png" width="192" height="64" alt="Deevana Hotels"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-deevana.png" width="186" height="16" alt="Reward Your Journey" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>From the moment you arrive, embrace the spirit of Deevana and enjoy a relaxing and rejuvenating experience.</p>
								</div>
							</div>

							<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta-suite.png" width="192" height="71" alt="Recenta Suite"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>A midscale hotel offering 1 Bedroom and 1 Living room, provides more space, more extensive and more comfortable. Recenta Suite always provides guests exceed expectations and satisfactions with hi standard hotels and resorts, including more impressive services and facilities. Based on premium setting and room function, our guests staying at Recenta Suite would perfectly be families and people who love more space and more privacy.</p>
								</div>
							</div>
                           
                           	<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta.png" width="192" height="71" alt="Recenta"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>Recenta is a midscale hotel designed to best fit for all your occasions and vacations with comfortable, affordable and valuable for money. All rooms are big and are well designed with good functions, so Recenta therefore provides high quality services and facilities for all ages and all genders that can be assured with the brand.</p>
								</div>
							</div>
                           
                           	<div class="row">
								<div class="col-w12 col-brand">
									<div class="table table-brand">
										<div class="table-cell cell-logo">
											<img class="logo" src="images/our_brands/brands-slogan/logo-recenta-express.png" width="192" height="71" alt="Recenta Style"/>
										</div>
										<div class="table-cell cell-slogan">
											<img class="solgan" src="images/our_brands/brands-slogan/slogan-recenta.png" width="232" height="58" alt="A Perfect Combination" />
										</div>
									</div>
								</div>

								<div class="col-w12 col-caption">
									<p>Recenta Style is a midscale hotel with limited services. Enjoying new and modern compact rooms, Recenta Style aims to that all guests can stay safe, clean and comfortable with 5 star comfort beds including the basic needed facilities and impressive services such as passenger lift, housekeeping services, TV, refrigerator, air conditioner and Free internet Wi-Fi.</p>
								</div>
							</div>
                          
							<h2 class="section-title underline">
								<span style="color: #9A7B12;">International Brand</span>
							</h2>
                           
                           	<div class="row">
                           		<div class="col-w12 col-brand">
                           			<div class="table table-brand">
                           				<div class="table-cell cell-logo">
                           					<img class="logo" src="images/our_brands/brands-slogan/logo-ramada.png" width="192" height="94" alt="Ramada Phuket Deevana" />
										</div>
										<div class="table-cell cell-slogan">
											<img class="slogan" src="images/our_brands/brands-slogan/slogan-ramada.png" width="217" height="18" alt="leave the rest to us" />
										</div>
									</div>
								</div>
								
								<div class="col-w12 col-caption">
									<p>A contemporary resort brimming with Thai charm situated at the heart of Patong Beach, a lively city and beach destination in one. Surrounded by top attractions guests are within five minutes stroll of the beautiful white sand beach, vibrant nightlife district and huge shopping and entertainment mall to name just a few.</p>
								</div>
							</div>
                            
                            
<!--
                            <div class="row row-brands">
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/deevana.png" alt="Deevana" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/deevana_plaza.png" alt="Deevana Plaza" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/ramada.png" alt="Ramada" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta_suite.png" alt="Recenta Suite" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta.png" alt="Recenta" />
                                </div>
                                <div class="col-w4">
                                    <img class="logo" width="150" src="images/our_brands/brands/recenta_express.png" alt="Recenta Style" />
                                </div>
                            </div>
-->
                        </article>
                        
                        <article class="article" id="deevana_plaza" data-tab-name="Deevana Plaza Phuket Patong">
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_phuket_patong.png" />
                            <h1 class="with-logo-deevana-plaza-phuket">Deevana Plaza Phuket Patong</h1>
                            <p>All the elements of a perfect island holidays in the central of Patong Beach, Phuket</p>
                            <p>Location in central Patong City and only steps away from world-famous Patong beach, shopping center, night market, and Phuket ’s bustling nightlife. Deevana Plaza Phuket Patong  brings its distinctive style and contemporary lightness to the otherwise down-to-earth in Patong Beach.</p>
                            <p>With 249 tastefully appointed rooms and suites, Orientala Wellness Spa treatments, offbeat dining experience of an original Thai &amp; International Cuisine at the Phuket Cafe in Patong area, astronomical-themed leisure activities and hi-tech meeting &amp; seminar facilities, the Hotel is a highlight in itself – and a must-stay for individuals, couples, families and business travelers on vacation in Phuket.</p>
                            <p><span style="color: #ab8205">Deevana Plaza Phuket Patong Beach  ensures top level of professionalism and service quality that makes every stay unique with a Thai style welcomed.</span></p>
                            <hr>
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_krabi_aonang.png" />
                            <h1 class="with-logo-deevana-plaza-krabi">Deevana Plaza Krabi Aonang</h1>
                            <p>Deevana Plaza Krabi Aonang offers chic accommodation in contemporary low-rise buildings located a short distance to Aonang Beach and Noppharat Thara Beach. With 213 stylish rooms and suites, each with a private balcony, guests can enjoy the resort’s excellent facilities and benefit from gracious and personalized service from the welcoming Deevana Plaza team. The resort features an exceptional restaurant and bars, three large outdoor swimming pools, a kid’s pool, a restful and rejuvenating spa, and outstanding meetings and events facilities.</p>
                            <p>Drawing inspiration from Krabi’s stunning natural beauty and the abundance wildlife protected within its national parks, Deevana Plaza Krabi Aonang has named its restaurant, bars and meetings facilities after birds such as the colorful hornbill and kingfisher. Conveniently located a 30 minute drive from Krabi International Airport,  20 minutes from Krabi Town, and 2 hours from Phuket International Airport.  Deevana Plaza Krabi Aonang is the ideal choice for leisure guests, honeymooners, family, as well as corporate meetings and incentive travelers.</p>
                            <p>Deevana Plaza Krabi Aonang is close to Krabi’s beautiful islands and beaches, as well as seafood restaurants, fascinating local markets, vibrant nightlife, and much more; everything in fact for a truly memorable holiday or rewarding business trip in one of Thailand’s most inspiring and scenic destinations.</p>
                        </article>

                        <!-- <article class="article" id="deevana_plaza_krabi" data-tab-name="Deevana Plaza Krabi Aonang">
                            <img class="thumbnail force block" src="images/our_brands/deevana_plaza_krabi_aonang.png" />
                            <h1 class="with-logo-deevana-plaza-krabi">Deevana Plaza Krabi Aonang</h1>
                            <p>Deevana Plaza Krabi Aonang offers chic accommodation in contemporary low-rise buildings located a short distance to Aonang Beach and Noppharat Thara Beach. With 213 stylish rooms and suites, each with a private balcony, guests can enjoy the resort’s excellent facilities and benefit from gracious and personalized service from the welcoming Deevana Plaza team. The resort features an exceptional restaurant and bars, three large outdoor swimming pools, a kid’s pool, a restful and rejuvenating spa, and outstanding meetings and events facilities.</p>
                            <p>Drawing inspiration from Krabi’s stunning natural beauty and the abundance wildlife protected within its national parks, Deevana Plaza Krabi Aonang has named its restaurant, bars and meetings facilities after birds such as the colorful hornbill and kingfisher. Conveniently located a 30 minute drive from Krabi International Airport,  20 minutes from Krabi Town, and 2 hours from Phuket International Airport.  Deevana Plaza Krabi Aonang is the ideal choice for leisure guests, honeymooners, family, as well as corporate meetings and incentive travelers.</p>
                            <p>Deevana Plaza Krabi Aonang is close to Krabi’s beautiful islands and beaches, as well as seafood restaurants, fascinating local markets, vibrant nightlife, and much more; everything in fact for a truly memorable holiday or rewarding business trip in one of Thailand’s most inspiring and scenic destinations.</p>
                        </article> -->

                        <article class="article" id="deevana" data-tab-name="Deevana Patong Resort">
                            <img class="thumbnail force block" src="images/our_brands/deevana_patong_resort_and_spa.png" />
                            <h1 class="with-logo-deevana-phuket">Deevana Patong Resort &amp; Spa</h1>
                            <p>Get the best of both worlds at Deevana Patong Resort &amp; Spa with 233 rooms.</p>
                            <p><span style="color: #ab8205;">Located at the centre of all the actions in Phuket’s most popular destination – Patong, the resort is just minutes away from the famous Bangla Road’s nightlife, Jungceylon shopping and entertainment centre and of course the wonderful golden sand of the glorious Patong Beach.Yet when entering the grand lobby of Deevana, the feeling of calm and tranquillity waits as the elegant Thai resort, with spacious tropical gardens and tasteful guest rooms and suites; takes you to an oasis of calm, the perfect setting for a relaxing holiday gateway.</span></p>
                            <hr>
                            <img class="thumbnail force block" src="images/our_brands/deevana_krabi_resort.png?ver=20160517" />
                            <h1 class="with-logo-deevana-krabi">Deevana Krabi Resort</h1>
                            <p>Latest upgrading of the uniqueness dwellings in the unspoiled plantations and green trees nestled on a hill -side with Secret of Fisherman’ characteristic and sense in Aonang – Krabi.</p>
                            <p><span style="color: #ab8205;">66 rooms distinctively either with stunning out door bathtub or naturally for your selection. Free Wi-Fi,  an International cuisines restaurant, two separate swimming pools and bars, massage in the Sala, and fitness room are provided for your convenience. Simply access to famous tourist attractions of Krabi, merely 10 minutes - walk to the beautiful   Aonang beach and Noppharathara Beach.</span></p>
                        </article>

                        <!-- <article class="article" id="deevana_krabi" data-tab-name="Deevana Krabi Resort">
                            <img class="thumbnail force block" src="images/our_brands/deevana_krabi_resort.png?ver=20160517" />
                            <h1 class="with-logo-deevana-krabi">Deevana Krabi Resort</h1>
                            <p>Latest upgrading of the uniqueness dwellings in the unspoiled plantations and green trees nestled on a hill -side with Secret of Fisherman’ characteristic and sense in Aonang – Krabi.</p>
                            <p><span style="color: #ab8205;">66 rooms distinctively either with stunning out door bathtub or naturally for your selection. Free Wi-Fi,  an International cuisines restaurant, two separate swimming pools and bars, massage in the Sala, and fitness room are provided for your convenience. Simply access to famous tourist attractions of Krabi, merely 10 minutes - walk to the beautiful   Aonang beach and Noppharathara Beach.</span></p>
                        </article> -->

                        <article class="article" id="recenta_suite" data-tab-name="Recenta Suite">
                            <img class="thumbnail force block" src="images/our_brands/recenta_suite_phuket_suanluang.png" />
                            <h1 class="with-logo-recenta-suite">Recenta Suite Phuket Suanluang</h1>
                            <p>a midscale hotel offering 1 Bedroom and 1 Living room, provides more space, more extensive and more comfortable.</p>
                            <p>Recenta Suite always provides guests exceed expectations and satisfactions with hi standard hotels and resorts, including more impressive services and facilities. Based on premium setting and room function, our guests staying at Recenta Suite would perfectly be families and people who love more space and more privacy.</p>
                        </article>

                        <article class="article" id="recenta" data-tab-name="Recenta">
                            <img class="thumbnail force block" src="images/our_brands/recenta_phuket_suanluang.png?ver=20160517" />
                            <h1 class="with-logo-recenta-suanluang">Recenta Phuket Suanluang</h1>
                            <p>Recenta is a midscale hotel designed to best fit for all your occasions and vacations with comfortable, affordable and valuable for money. All rooms are big and are well designed with good functions, so Recenta therefore provides high quality services and facilities for all ages and all genders that can be assured with the brand.</p>
                        </article>

                        <article class="article" id="recenta_express" data-tab-name="Recenta Style">
                            <img class="thumbnail force block" src="images/our_brands/recenta_express_phuket_town.png" />
                            <h1 class="with-logo-recenta-express">Recenta Style Phuket Town</h1>
                            <p>Recenta Style is a midscale hotel with limited services. Enjoying new and modern compact rooms, Recenta Style aims to that all guests can stay safe, clean and comfortable with 5 star comfort beds including the basic needed facilities and impressive services such as passenger lift, housekeeping services, TV, refrigerator, air conditioner and Free internet Wi-Fi.</p>
                        </article>
                        
                        <article class="article" id="ramada_phuket_deevana" data-tab-name="Ramada Phuket Deevana">
                            <img class="thumbnail force block" src="images/our_brands/ramada_phuket_deevana.png" />
                            <h1 class="with-logo-ramada">Ramada By Wyndham Phuket Deevana</h1>
                            <p>A contemporary resort brimming with Thai charm situated at the heart of Patong Beach, a lively city and beach destination in one. Surrounded by top attractions guests are within five minutes stroll of the beautiful white sand beach, vibrant nightlife district and huge shopping and entertainment mall to name just a few.</p>
                            <p>Ramada By Wyndham Phuket Deevana is a serene escape from the city streets where tranquillity and warm hospitality awaits. The delightful elephant artwork throughout the resort will add a smile to your stay and the tastefully furnished guestrooms are full of modern amenities for a comfortable and restful night's sleep.</p>
                            <p>Ramada By Wyndham Phuket Deevana is non-smoking hotel, designed for leisure and pleasure with something for the whole family. Splash around under the tropical sunshine at the outdoor swimming pool with a built-in Jacuzzi and keep the little ones entertained at the kid's club while you indulge in a rejuvenating treatment at the wellness spa.</p>
                            <p>Dine on international cuisine at the all day restaurant and stop by the lobby Bake & Bev for light refreshments. Meeting rooms and business services are also provided for banquets and events.</p>
                        </article>
                    </div>
                </div>

            </div>
        </div>
        
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        $cs.find(atd).show();
        
        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });
        
        $cs.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            if( tabName !== undefined ) {
                $this.before('<span class="accordion-tab">'+tabName+'</span>');
                $this.prev('.accordion-tab').on('click', function() {
                    var i = $(this).index('.accordion-tab');
                    $(this).addClass('active').siblings().removeClass('active');
                    $this.slideDown(300, function() {
                        var pos = $(this).offset().top;
                        var offset = 50;
                        $('html, body').animate({
                            scrollTop: pos - offset,
                        }, 800);
                    }).siblings().not('.accordion-tab').slideUp(300);
                    $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                });
            };
        });
        
        $('.accordion-tab').eq(ati).addClass('active');
        
        
		$(window).on('load', function() {
			//Go to Landing section
			var pos = $('.site-main').offset().top;
			var offset = ( $(window).width() > 960 ) ? 144 : 0 ;
			scrollTo(0, Math.round(pos - offset) );
		});
    });
</script>

<style>
	#landing > .row {
		margin-bottom: 50px;
		text-align: center;
	}
	#landing > .row:last-of-type {
		margin-bottom: 0;
	}
	
	.table-brand {
		width: 100%;
		text-align: center;
	}
	.table-brand img {
		max-width: 100%;
		height: auto;
		display: block;
		margin: 0 auto;
	}
	.table-brand .cell-logo,
	.table-brand .cell-slogan {
		vertical-align: middle;
		width: 50%;
		padding: 1em;
	}
	.table-brand .cell-logo {
		position: relative;
	}
	.table-brand .cell-logo:after {
		content: '';
		display: block;
		width: 1px;
		height: 60px;
		background-color: #666;
		position: absolute;
		right: 0;
		top: 50%;
		margin-top: -30px;
		margin-right: 0.5px;
	}
	
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    
    /* Logo in post tab */
    .post-tabs .logo-tab:before {
        content: '';
        background-size: contain;
        background-repeat: no-repeat;
        background-position: right top;
        width: 100%;
        height: 76%;
        display: block;
        position: absolute;
        top:12%;
        right: 15px;
        -webkit-filter: grayscale(1) opacity(0.75);
        filter: grayscale(1) opacity(0.75);
        -webkit-transition: 200ms;
        transition: 200ms;
    }
    .post-tabs .deevana:before { background-image: url(images/our_brands/brands/deevana.png); }
    .post-tabs .deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .post-tabs .deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .post-tabs .deevana-plaza:before { background-image: url(images/our_brands/brands/deevana_plaza.png); }
    .post-tabs .deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .post-tabs .deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .post-tabs .ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .post-tabs .recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite.png); }
    .post-tabs .recenta:before { background-image: url(images/our_brands/brands/recenta.png); }
    .post-tabs .recenta-express:before { background-image: url(images/our_brands/brands/recenta_express.png); }
    
    .post-tabs .logo-tab:hover:before,
    .post-tabs .logo-tab.active:before {
        -webkit-filter: none;
        filter: none;
    }
    
    /* Logo in title */
    [class*="with-logo-"] {
        position: relative;
        padding-right: 80px;
    }

    [class*="with-logo-"]:before {
        content: '';
        background-repeat: no-repeat;
        background-position: right top;
        background-size: contain;
        display: block;
        position: absolute;
        top: -10px;
        right: 0;
        top: 50%;
        width: 100%;
        height: 40px;
        margin-top: -20px;
        z-index: -1;
    }

    .with-logo-deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .with-logo-deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .with-logo-deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .with-logo-deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .with-logo-ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .with-logo-recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite_with_location.png); }
    .with-logo-recenta-suanluang:before { background-image: url(images/our_brands/brands/recenta_suanluang_with_location.png); }
    .with-logo-recenta-express:before { background-image: url(images/our_brands/brands/recenta_express_with_location.png); }
    
    .row-brands {
        text-align: center;
        margin-bottom: 30px;
    }
    .row-brands .logo {
        max-width: 100%;
        height: auto;
        display: inline-block;
        vertical-align: middle;
        margin-bottom: 10px;
    }
    
    #landing {
        min-height: 400px;
    }

    @media (max-width: 1024px) {
        .post-tabs .logo-tab:before {
            content: none;
        }
    }
    
    @media (max-width: 768px) {
		.row-header > [class*="col-"],
        .row-content > [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 20px;
        }
        .tabs-content .article.hide-tab {
            padding-top: 0;
        }
        
        .row-brands [class*="col-"] {
            width: 50%;
        }
    }
	
	@media (max-width: 480px) {
		#landing > .row {
			margin-bottom: 0;
			padding: 40px 0;
			position: relative;
		}
		#landing > .row:before {
			content: '';
			position: absolute;
			bottom: 0;
			left: 40px;
			right: 40px;
			height: 1px;
			background-color: #ccc;
		}
		#landing > .row:last-of-type:before {
			content: none;
		}
		.table-brand .cell-logo,
		.table-brand .cell-slogan {
			width: 100%;
			display: block;
		}

		.table-brand .cell-logo:after {
			width: 60px;
			height: 1px;
			top: auto;
			left: 50%;
			right: auto;
			bottom: 0;
			margin: -0.5px 0 0 -30px;
		}
	}
</style>

<?php include_once('_footer.php'); ?>