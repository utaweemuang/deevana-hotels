<?php

if (empty($_POST)) {
    die('Post data should not be empty!');
}

#
# Verify captcha
$post_data = http_build_query(
    array(
        'secret' => '6LfVdLwUAAAAAMh53JDWMLwMMlebJ4qpFBIl00RS',
        'response' => $_POST['g-recaptcha-response']
    )
);
$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $post_data
    )
);
$context  = stream_context_create($opts);
$response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
$result = json_decode($response);

if (!$result->success) {
    die($result['error-codes']);
}

// $to         = "websupport@travelanium.com";

$to         = $_POST['hotels'];
$name       = $_POST['name'];
$email      = $_POST['email'];
$tel        = $_POST['phone'];
$message    = $_POST['message'];
$date       = $_POST['date'];
$time       = $_POST['time'];
$guest      = $_POST['guest'];


$subject = "Contact website form $email";

define( 'SMTPHOST', 'smtpm.csloxinfo.com' );
define( 'SMTPUSER', 'mice@deevana.com' );
define( 'SMTPPASS', 'DHR@9/m!c3' );
define( 'SMTPPORT', 25 );

//Load composer's autoloader
require '../forms/PHPMailer-5.2.25/PHPMailerAutoload.php';

$mail = new PHPMailer;                              // Passing `true` enables exceptions
//Server settings
$mail->CharSet = 'UTF-8';
$mail->SMTPDebug = 0;                                 // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = SMTPHOST;
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = SMTPUSER;
$mail->Password = SMTPPASS;
// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = SMTPPORT;

$mail->set('Timeout', 11);

//Content
$mail->Subject = $subject;

$messages  = "Meeting Form" . "\r\n\r\n";
$messages .= $message . "\r\n\r\n";
$messages .= "Name: $name" .  "\r\n";
$messages .= "Email: $email" . "\r\n";
$messages .= "Phone: $tel" . "\r\n";
$messages .= "Date: $date" . "\r\n";
$messages .= "Time: $time" . "\r\n";
$messages .= "Number of people: $guest" . "\r\n\r\n";


$mail->Body    = $messages;
//Recipients
$mail->setFrom('mice@deevana.com', 'Deevana Hotels & Resorts Group');
$mail->addAddress($to);
// $mail->addCC($cc);
$mail->addReplyTo($email, $name);

if(!$mail->send()){
    jsonResponse( 'error', 'Message could not be sent.', $mail->ErrorInfo );
} else {
    jsonResponse('success','Message has been sent.');
}

function jsonResponse($status, $msg, $detail=NULL) {
    $data = array(
        'status'    => $status,
        'message'   => $msg,
        'detail'    => $detail
    );
    echo json_encode($data);
}
