<?php
$title = 'Gallery | Recenta Style | Official Hotel Group Website Thailand';
$desc = 'Gallery: 3 star chic hotel in Phuket town from USD 30 per night, enjoy best rate when book this city hotel directly.';
$keyw = 'gallery, Recenta Style phuket, Recenta Style, phuket, phuket city, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'gallery';
$cur_page = 'gallery';

$lang_en = '/recentastyle/gallery.php';
$lang_th = '/th/recentastyle/gallery.php';
$lang_zh = '/zh/recentastyle/gallery.php';

include_once( '_header.php' );
?>

<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section section-gallery">
                
                <div class="gal-types">
                    <span class="tab active" data-tab="#photo"><i class="icon fa fa-picture-o"></i>PHOTOS</span>
                    <span class="tab" data-tab="#video"><i class="icon fa fa-film"></i>VIDEO</span>
                </div>
                
                <div class="gal-conts">
                    <section id="photo" class="gal-cont">
                        <div class="cat-tabs">
                            <span class="tab active" data-cat="*">ALL</span>
                            <span class="tab" data-cat=".overview">OVERVIEW</span>
                            <span class="tab" data-cat=".rooms">ROOMS</span>
                            <span class="tab" data-cat=".facilities">FACILITIES</span>
                        </div>
                        
                        <div class="content">
                            <div class="masonry-gal">
                                <div class="item overview"><a href="images/gallery/set_20160511/1500/gal_01.jpg"><img src="images/gallery/set_20160511/600/gal_01.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <!-- <div class="item"><a href="images/gallery/set_20160511/1500/gal_02.jpg"><img src="images/gallery/set_20160511/600/gal_02.jpg" alt="Recenta Style, 3-star hotel" /></a></div> -->
                                <!-- <div class="item overview"><a href="images/gallery/set_20160511/1500/gal_03.jpg"><img src="images/gallery/set_20160511/600/gal_03.jpg" alt="Recenta Style, 3-star hotel" /></a></div> -->
                                <!-- <div class="item overview"><a href="images/gallery/set_20160511/1500/gal_04.jpg"><img src="images/gallery/set_20160511/600/gal_04.jpg" alt="Recenta Style, 3-star hotel" /></a></div> -->
                                <!-- <div class="item overview"><a href="images/gallery/set_20160511/1500/gal_05.jpg"><img src="images/gallery/set_20160511/600/gal_05.jpg" alt="Recenta Style, 3-star hotel" /></a></div> -->
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_06.jpg"><img src="images/gallery/set_20160511/600/gal_06.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_07.jpg"><img src="images/gallery/set_20160511/600/gal_07.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_08.jpg"><img src="images/gallery/set_20160511/600/gal_08.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_09.jpg"><img src="images/gallery/set_20160511/600/gal_09.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_10.jpg"><img src="images/gallery/set_20160511/600/gal_10.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_11.jpg"><img src="images/gallery/set_20160511/600/gal_11.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_12.jpg"><img src="images/gallery/set_20160511/600/gal_12.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_13.jpg"><img src="images/gallery/set_20160511/600/gal_13.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_14.jpg"><img src="images/gallery/set_20160511/600/gal_14.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item rooms"><a href="images/gallery/set_20160511/1500/gal_15.jpg"><img src="images/gallery/set_20160511/600/gal_15.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_16.jpg"><img src="images/gallery/set_20160511/600/gal_16.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_17.jpg"><img src="images/gallery/set_20160511/600/gal_17.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_18.jpg"><img src="images/gallery/set_20160511/600/gal_18.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_19.jpg"><img src="images/gallery/set_20160511/600/gal_19.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_20.jpg"><img src="images/gallery/set_20160511/600/gal_20.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                                <div class="item facilities"><a href="images/gallery/set_20160511/1500/gal_21.jpg"><img src="images/gallery/set_20160511/600/gal_21.jpg" alt="Recenta Style, 3-star hotel" /></a></div>
                            </div>
                        </div>
                    </section>
                    
                    <section id="video" class="gal-cont">
                        <div class="content">
                            <div class="youtube-responsive">
								<div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/_OrH30RLPWI?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
									<!-- VDO -->
								</div>
							</div>
                        </div>
                    </section>
                </div>
                
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<!--Isotopr-->
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
    $(function() {
        var $gCont = $('.gal-conts');
        var $gType = $('.gal-types');
        var $ctabs = $('.cat-tabs');
        
        function masonryFX( cat = '*' ) {
            var $mgal = $('.masonry-gal');
            $mgal.isotope({
                itemSelector: '.item',
                filter: cat,
            }).imagesLoaded().progress( function() {
                $mgal.isotope('layout');
            });
        }
        
        function galleryFX( cat = '.item' ) {
            $gCont.find(cat).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [1, 2],
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('gallery-open');
                    },
                    close: function() {
                        $('html').removeClass('gallery-open');
                    },
                },
                removalDelay: 300,
                mainClass: 'mfp-fade',
            });
        }
        
        // Define active tab
        var active = $gType.find('.active').data('tab');
        $gCont.find( active ).show();
        
        $gType.on('click', '.tab', function(e) {
            e.preventDefault();
            if( $(this).hasClass('active') ) return;
            
            var $this = $(this);
            var data = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $gCont.find(data).fadeIn(300).siblings().hide();
            
			var cat = $ctabs.find('.active').data('cat');
			galleryFX(cat);
            masonryFX(cat);
        });
        
        $ctabs.on('click', '.tab', function(e) {
            var $this = $(this);
            var cat = $this.data('cat');
            $this.addClass('active').siblings().removeClass('active');
            
            galleryFX(cat);
            masonryFX(cat);
        });
        
        galleryFX();
        masonryFX();
    });
</script>

<style>
    .site-main {
        background-image: url(images/gallery/bg-gallery.jpg);
        background-attachment: fixed;
        background-position: center;
        background-size: cover;
    }
    .gal-conts {
        width: 100%;
        clear: both;
        background-color: #fff;
        margin-bottom: 30px;
        border-top: 2px solid #FFA035;
        position: relative;
    }
    .gal-cont {
        display: none;
        position: relative;
    }
    .gal-cont .content {
        overflow: hidden;
        padding: 10px 10px 0;
    }
    .gal-types,
    .cat-tabs {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
    }
    .gal-types {
        float: right;
    }
    .gal-types .tab {
        background-color: #fff;
        color: #333;
        border-radius: 3px 3px 0 0;
        display: inline-block;
        text-align: center;
        padding-top: 10px;
        width: 70px;
        height: 50px;
    }
    .gal-types .tab.active {
        background-color: #FFA035;
        color: #fff;
    }
    .gal-types .tab .icon {
        display: block;
    }
    .cat-tabs {
        position: absolute;
        top: -30px;
        left: 0;
    }
    .cat-tabs .tab {
        background-color: transparent;
        color: #333;
        line-height: 28px;
        font-size: 16px;
        padding: 0 8px;
        display: inline-block;
        border-radius: 3px 3px 0 0;
    }
    .cat-tabs .tab.active {
        background-color: #FFA035;
        color: #fff;
    }
    .masonry-gal {
        margin-left: -5px;
        margin-right: -5px;
    }
    .masonry-gal .item {
        width: 33.3333%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .masonry-gal .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    #video {
        padding-bottom: 10px;
    }
    
    html .site-header {
        transition-property: background-color, height, right;
        transition-duration: 300ms, 300ms, 0ms;
    }
    html.gallery-open .site-header {
        right: 17px;
    }
	
	.youtube-responsive {
		max-width: 800px;
		height: auto;
		margin: 1em auto;
	}
	
	.youtube-responsive .video-container {
		width: 100%;
		padding-bottom: 56.25%;
		position: relative;
	}
	
	.youtube-responsive iframe {
		position: absolute;
		width: 100%;
		height: 100%;
	}
    @media (max-width: 640px) {
        .section-gallery {
            padding-top: 0;
        }
        .masonry-gal .item {
            width: 50%;
        }
        .gal-conts {
            margin-top: 42px;
        }
        .gal-types {
            float: none;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        .gal-types:after {
            content: '';
            display: block;
            clear: both;
        }
        .gal-types .tab {
            width: 50%;
            float: left;
            padding: 6px;
            height: auto;
            border-radius: 0;
        }
        .gal-types .tab .icon {
            display: inline-block;
            margin-right: 3px;
        }
        .cat-tabs {
            overflow-x: auto;
            display: flex;
            flex-flow: row nowrap;
            background-color: #fff;
            top: -34px;
            left: 0;
            right: 0;
            border-radius: 3px 3px 0 0;
        }
        .cat-tabs .tab {
            border-radius: 0;
            font-size: 12px;
            line-height: 32px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>