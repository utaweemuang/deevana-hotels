<?php

$url        = 'http://www.deevanahotels.com/recentastyle/';
$name       = 'Recenta Style Phuket Town';
$version    = '20180403';
$email      = 'rvn@recentahotels.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/Recenta-Express-Phuket-Town-746646278799484';
$twitter    = '#';
$googleplus = '#';
$youtube    = '#';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/recentahotels/';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = '#';

$ibeID      = '310';
