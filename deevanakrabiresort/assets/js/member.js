/**
 * Module Name: Member
 * Description: This plugin will prepare member system by using browser cookies
 * Version: 0.1.2
 * Dependencies: jQuery, CookieJS
 */

/**
 * @desc Check member cookies in browser
 * @return {string || boolean} are users member?
 */
function memberChecker() {
    var m = Cookies.get('member');
    if (m!==undefined)
        return Cookies.getJSON('member');
    else
        return false;
}

/**
 * @desc Create cooking for member
 */
function memberCreateCookies(options) {
    var defaults = {
        code: 'member',
        expire: 3,
        user: {
            email: null,
            name: null,
        }
    }
    var value = $.extend(defaults, options);

    var fname = value.user.name;
    var email = value.user.email;
    var username = (fname!==undefined) ? fname : email.substring(0, email.indexOf('@'));

    Cookies.set(value.code, {
        code: value.code,
        email: value.user.email,
        name: username,
    }, {
        expires: value.expire,
    });
}

/**
 * @desc This function will modify all url in page to use member code except url that already has accesscode parameter.
 * @param {string} val
 */
function memberAppendCode( val ) {
    var origin = $('a[href^="https://reservation.travelanium.net/"]');
    origin.each( function() {
        var href = this.href;
        var paramExist = (href.indexOf('accesscode=') > -1) ? true : false;
        var newHref = href;

        if (!paramExist) {
            var param = (href.indexOf('?') > -1) ? '&accesscode=' : '?accesscode=';
            newHref = href + param + val;
        }

        this.href = newHref;
    });
}

/**
 * @desc This function will send customer data through Travelanium Form API
 * @param {string} options
 * @return {boolean}
 */
function memberSendData(options) {
    var defaults = {
        base:       'https://hotelservices.travelanium.net/crs-customer-form-rs/restresources/CRSCustomerFormDataService',
        formAPI:    null,
        formType:   1,
        customer: {
            name:       null,
            phone:      null,
            email:      null,
            message:    null,
            country: {
                code:       null,
                name:       null
            }
        }
    }, data = $.extend(true, defaults, options);

    var formData = JSON.stringify({
        formKey:            data.formAPI,
        customerFormType:   data.formType,
        name:               data.customer.name,
        phone:              data.customer.phone,
        email:              data.customer.email,
        countryCode:        data.customer.country.code,
        countryName:        data.customer.country.name,
        message:            data.customer.message
    });

    var sendAjax = $.ajax({
        type: 'POST',
        url: data.base,
        data: formData,
        contentType: 'application/json',
        cache: false,
    });
}