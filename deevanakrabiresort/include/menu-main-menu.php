<ul class="menu list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="../deevanakrabiresort/">Home</a></li>
    <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">Accommodation</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">Standard Room</a></li>
            <li class="<?php echo get_current_class('grand-deluxe'); ?>"><a href="room-grand-deluxe.php">Grand Deluxe Room</a></li>
            <li class="<?php echo get_current_class('duplex'); ?>"><a href="room-duplex.php">Duplex Room</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">Facilities</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">Attractions</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('386', 'en'); ?>">Promotion</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">Gallery</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>
