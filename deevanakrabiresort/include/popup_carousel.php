<style>
	.mfp-bg {
	    opacity: 0;
	    transition: 1s;
	}
	.mfp-bg.mfp-ready.fadeIn {
	    opacity: 0.8;
	}
	#popup_rotate {
		margin: auto;
		width: 600px;
		max-width: 100%;
		position: relative;
		background-color: #fff;
	}
	#popup_rotate .mfp-close {
		top: -44px;
		color: #fff;
		text-align: right;
		padding-top: 6px;
	}
	#popup_rotate .owl-dots {
		top: 15px;
		left: 15px;
		bottom: auto;
		text-align: left;
		height: 0;
	}
	#popup_rotate .owl-dot span {
		background-color: #fff;
		border: 0;
		opacity: 0.6;
		width: 10px;
		height: 10px;
	}
	#popup_rotate .owl-dot.active span {
		opacity: 1;
	}
	#popup_rotate img {
		opacity: 1;
		-webkit-transition: 200ms;
		transition: 200ms;
	}
	#popup_rotate a:hover img {
		opacity: 0.85;
	}
</style>

<div class="mfp-hide" id="popup_rotate">
    <div class="owl-carousel">
    	<a class="item" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDg1NDY4" target="_blank"><img class="img-cover" src="http://www.deevanahotels.com/deevanakrabiresort/images/banners/escape-holiday.jpg" title="Escape Holiday"></a>
    	<a class="item" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDgyMzI3" target="_blank"><img class="img-cover" src="http://www.deevanahotels.com/deevanakrabiresort/images/banners/hideaway-package.jpg" title="Hideaway Package"></a>
	    <!-- <img src="http://www.deevanahotels.com/deevanakrabiresort/images/banners/chim-shop-chai.jpg" alt="deevanakrabiresort" width="800" height="534"> -->
	    <!-- <a target="_blank" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDg1NDY4"><img src="http://www.deevanahotels.com/deevanakrabiresort/images/banners/Holiday-01.jpg" alt="deevanakrabiresort" width="800" height="534">
	    <a target="_blank" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDczMTEyMg%3D%3D"><img src="http://www.deevanahotels.com/deevanakrabiresort/images/banners/special-save-50.jpg" alt="deevanakrabiresort" width="800" height="534">	 -->    
	</div>
</div>

<script>
	$(window).on('load', function() {
		var popup = $('#popup_rotate');
		$.magnificPopup.open({
			items: {
				type: 'inline',
				src: popup,
			},
			removalDelay: 1000,
			callbacks: {
				open: function() {
					var back = $(this.bgOverlay);
					var cont = $(this.contentContainer);
					back.addClass('fadeIn');
					cont.addClass('fadeInDown animated');
				},
				beforeClose: function() {
					var back = $(this.bgOverlay);
					var cont = $(this.contentContainer);
					back.removeClass('fadeIn');
					cont.addClass('fadeOut');
				},
			}
		});

		popup.find('.owl-carousel').owlCarousel({
			items: 1,
			loop: 1,
			autoplay: 1,
			autoplayTimeout: 5000,
			smartSpeed: 600,
		});
	});
</script>