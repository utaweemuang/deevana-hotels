<?php
$title      = 'Standard Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Standard Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'Standard room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page   = 'deluxe-room';
$par_page   = 'rooms';

$lang_en    = '/deevanakrabiresort/room-deluxe-room.php';
$lang_th    = '/th/deevanakrabiresort/room-deluxe-room.php';
$lang_zh    = '/zh/deevanakrabiresort/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">

        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/standard-room/1500/Standard-Room-1.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-2.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-3.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-4.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-5.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-6.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-7.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-8.jpg" alt="Standard Room" width="1500" height="1045" />
<!--                     <img src="images/accommodations/deluxe-room/1500/deluxe-room-06.jpg" alt="Deluxe Room" width="1500" height="1045" />
                    <img src="images/accommodations/deluxe-room/1500/deluxe-room-03.jpg" alt="Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/deluxe-room/1500/deluxe-room-04.jpg" alt="Deluxe Room" width="1500" height="937" /> -->
                   
                </div>
            </div>
        </div>

        <div class="room-slides-thumbs">
            <h2 class="title">Standard Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
					<li class="current"><img src="images/accommodations/standard-room/600/Standard-Room-1.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-2.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-3.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-4.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-5.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-6.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-7.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-8.jpg" alt="Standard Room" width="600" height="400" /></li>
<!-- 					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-06.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-03.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-04.jpg" alt="Deluxe Room" width="600" height="400" /></li> -->
					
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Standard Room </h1>

                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/standard-room/600/Standard-Room-8.jpg" alt="Standard Room" width="600" height="400" />

                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>

                        <div class="col-w5 col-info">
                            <p>Simply and cozy room with 26 square meters, located on the 2nd floor with an unique designed and warmth decoration plus a balcony for your comfort and chill out. Thai traditional style of beds setting with King-size bed or Twin beds is available for your selection. Separate rain-shower, restroom and basin come with full amenities of nature friendly are provided for your convenience.</p>
                            <p>Standard Room 	:	  27 rooms </p>

                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>

                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">In room Facilities</h2>
                                    <ul class="amenities-list">
                                        <li>LED 40” TV</li>
                                        <li>Free WIFI</li>
                                        <li>Tea &amp; Coffee making facility</li>
                                        <li>Safety Box</li>
                                        <li>Mini Bar</li>
                                        <li>Hair dryer</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Digital 40” TV</li>
											<li>Free WIFI</li>
											<li>Tea &amp; Coffee making facility</li>
											<li>Safety Box</li>
											<li>Mini Bar</li>
											<li>Hair dryer</li>
											<li>Hot &amp; Cold water</li>
											<li>Umbrellas</li>
											<li>Beach Bag</li>
											<li>Sandals</li>
                                        </ul>
                                    </div>
                                </div>

                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>

            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>

    </div>
</main>

<?php include_once('_footer.php'); ?>
