<?php
$title      = 'Attractions | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Attractions: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'attractions, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'attraction';
$cur_page   = 'attraction';

$lang_en    = '/deevanakrabiresort/attraction.php';
$lang_th    = '/th/deevanakrabiresort/attraction.php';
$lang_zh    = '/zh/deevanakrabiresort/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#emerald_pool">Emerald Pool</li>
							<li class="tab" data-tab="#tiger_cave_temple">Tiger Cave Temple</li>
                            <!-- <li class="tab" data-tab="#elephant_ride">Elephant Ride</li> -->
                            <li class="tab" data-tab="#phi_phi_island">Phi Phi Island</li>
                            <li class="tab" data-tab="#thalay_wak">Thalay Wak</li>
							<li class="tab" data-tab="#nong_thale_canel">Nong Thale Canal</li>
							<li class="tab" data-tab="#koh_kai">Koh Kai</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="emerald_pool" data-tab-name="Emerald Pool">
                        <img class="thumbnail force" src="images/attraction/emerald_pool.png" alt="Emerald Pool" />
                        <h1 class="title">Emerald Pool</h1>
                        <p>Emerald pool or 'Sra Morakot' in Thai is a truly wonderful natural pool in the middle of the forest filled with glistening clear spring water. The Emerald Pool is situated in Khao Pra - Bang Khram Wildlife Sanctuary, locally called Khao Nor Chuchi Lowland Forest at Ban Bang Tieo, 18 kms along the public road No. 4038 from the district office of Klong Thom.</p>
                        <p>Upon arrival, most visitors can’t wait to leap into the clear emerald green fresh water of the pool and enjoy a refreshing swim.</p>
                        <p>Sra Morakot is almost a round pool about 1-2 meters deep and 20-25 meters in diameter. The color of water changes into pale green or emerald upon the reflections of the sunlight.</p>
                        <p>The water of Sra Morakot comes from the spring-fed pool known as “the Blue Pool”, 600 meters away on the hill. Sulfurous, alkaline water from the Blue Pool wells up and flows through cracks in the rocks to cascade down to Emerald Pool. Consequently, the water is always clear because of the high calcium carbonate that makes all the suspended particles settle to the bottom. In addition, algae cannot grow in the water.</p>
                        <p>A Balinese-style thatched cottage was built and was exclusively reserved for the Royal Family visits years ago.</p>
                    </article>
					
					<article class="article" id="tiger_cave_temple" data-tab-name="Tiger Cave Temple">
                        <img class="thumbnail force" src="images/attraction/tiger_cave_temple.png" alt="Tiger Cave Temple" />
                        <h1 class="title">Tiger Cave Temple</h1>
						<p>The tigers have long since vanished, leaving paw prints in the cave that now houses a shrine with a statue of a tiger and numerous Buddha images. Other interesting features include an oddly placed whale skull and human skeletons used to aid in contemplating the impermanence of life. Signaling the area’s Chinese influence, a towering Chinese‐style pagoda houses a giant statue of Kuan Yin, the Mahayana “goddess” of compassion.</p>
                        <p>The temple’s main draw is a Buddha image and golden chedi perched atop the cliff with a large lookout platform. The views from here are breathtaking — Khao Phanom Bencha looms to the north, the steep karst cliffs that cut Railay off from the rest of the mainland rise to the west and the mouth of the Krabi River empties into the sea to the south. On a clear day, you can see all the way to Ko Phi Phi.</p>
                    </article>
                    
                    <!-- <article class="article" id="elephant_ride" data-tab-name="Elephant Ride">
                        <img class="thumbnail force" src="images/attraction/elephant_ride.png" alt="Elephant Ride" />
                        <h1 class="title">Elephant Ride</h1>
                        <p>The elephant is seen as a symbol of the nation and a talisman for the Thai people. With their wrinkly grey skin and swaying trunk, they are a fascinating combination of brute force, gentleness and remarkable agility that inspires both respect and affection.</p>
                        <p>With elephants fast disappearing from the wild in Thailand, the best place to see these fascinating animals up close is in a trekking camp. There are only a few camps in Krabi – such establishments must follow strict guidelines as set by the National Livestock Department regarding the provision of adequate food, water and shade for their animals, as well as proper health care.</p>
                        <p>A trek will allow you to experience this first-hand, as well as the animals’ natural forest environment. Carrying tourists for fun means the elephants are able to earn their keep, while living as freely as is possible: as there is not enough space to release them into the wild, and they are very costly to feed (consuming 200-300kg of food per day) the only alternatives for these gentle giants are begging, illegal logging, or inactivity in a zoo.</p>
                        <p>Trekking tours – usually an hour’s ride on the elephant, optionally combined with another sightseeing activity – are bookable below, or through any local agent. You can also visit the camps directly for a trek or just to observe and photograph the elephants, even if not planning to ride</p>
                    </article> -->
                    
                    <article class="article" id="phi_phi_island" data-tab-name="Phi Phi Island">
                        <img class="thumbnail force" src="images/attraction/phi_phi_island.png" alt="Phi Phi Island" />
                        <h1 class="title">Phi Phi Island</h1>
                        <p>Phi Phi Island is Thailand's island-superstar. It's been in the movies. It's the topic of conversation for travelers all over Thailand. For some, it's the only reason to touchdown in Phuket. Even with all the hype, it doesn't disappoint. Phi Phi's beauty is a large chunk of the allure. The islands, when approached by boat, rise from the sea like a fortress. Sheer cliffs tower overhead, then give way to beach-fronted jungle. It's love at first sight.</p>
                        <p>The second part of the why-we-love-this-place story is attitude: few places on the planet are this laid-back. Of the two islands, one is completely free of human inhabitants (Phi Phi Leh), and the other is without roads (Phi Phi Don). There's no schedule, no hustle-and-bustle, no reason to be in a hurry.</p>
                    </article>

                    <article class="article" id="thalay_wak" data-tab-name="Thalay Wak">
                        <img class="thumbnail force" src="images/attraction/talay-wak.png" />
                        <h1>Thalay Wak</h1>
                        <p>Thalay Wak is a natural attraction in Krabi that has the title “Unseen Thailand”, famous across the world. A natural phenomenon occurs that causes the seas to part. When the tide is low, a Y-shaped sandbank connecting 3 islands appears; Tup Island, Mor Island, and Chicken Island. When the tide rises, the sandbank disappears into the ocean.</p>
                    </article>

                    <article class="article" id="nong_thale_canel" data-tab-name="Nong Thale Canal">
                        <img class="thumbnail force" src="images/attraction/Klongnongtale.png" />
                        <h1>Nong Thale Canal</h1>
                        <p>Nong Thale Canal is located at the boundary between Moo (Avenue) 1 and Moo (Avenue) 4, Tambon Nong Thale, Muang District, Krabi Province. In the past, this canal was a small freshwater canal. It looked like a swamp full of small trees. About 10 years ago, there were overflow weirs in the middle of the canal to slow down the water to use during the dry season and provide tap water to the people. From small canyons and peat swamps, it became a large basin with an area of about 100 acres, a depth of about 5 meters, and a width of about 200 meters.Nowadays, "Nong Talay Canal" or "Klong Root" has become a new ecotourism destination in Krabi. Nong Thale Subdistrict Administrative Organization in conjunction with the village headmen and villagers helped develop the route to accommodate ecological tourists. By kayaking (canoe), go upstream and enjoy the scenic rocky structures; similar to a small mountain with tree stumps.</p>
                    </article>

                    <article class="article" id="koh_kai" data-tab-name="Koh Kai">
                        <img class="thumbnail force" src="images/attraction/koh-kai.png" />
                        <h1>Koh Kai</h1>
                        <p>Koh Kai (sometimes spelled Koh Gai or Koh Khai and meaning ‘Chicken Island’ in Thai), is also called Koh Hua Khawan or Koh Poda Nok. It is a small island belonging to the Poda group of islands located about eight kilometres from Ao Nang in the province of Krabi. Koh Kai takes its name from the chicken-shaped rock forming its southern tip. Most island-hopping tours from Krabi and Phuket make a short stop at this picturesque limestone chicken, giving visitors a cool photo opportunity.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(t).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: auto;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>