<?php

session_start();

$title      = 'Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'home';
$cur_page   = 'home';

$lang_en    = '/deevanakrabiresort';
$lang_th    = '/th/deevanakrabiresort';
$lang_zh    = '/zh/deevanakrabiresort';

include_once('_header.php'); ?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/slides-hero/home-slide-01.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-1.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-2.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-3.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
            <div class="item"><img src="images/home/slides-hero/slide-4.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></div>
        </div>

        <!--div id="promotion_board" class="promotion get-center">
            <a href="https://reservation.travelanium.net/propertyibe2/?propertyId=386&onlineId=4">
                <img class="block responsive" src="images/home/discount_bann.png" />
            </a>
        </div-->
        <?php //include('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">Welcome to <br><span style="color:#244289;">Deevana Krabi Resort</span></span>
                        </h1>

                        <p><img width="220" class="align-left" style="border: 3px solid #fff;" src="images/home/content-img1.jpg" /></p>
						<p><strong>Retreat</strong> at the latest upgrading of the uniqueness dwellings in the unspoiled plantations and rich of natural green trees. Nestled on hillside with the modern vernacular décor in <strong>"Secret of Fisherman"</strong> characteristic, in Aonang of Krabi - Thailand.</p>
                        <p>66 new distinctively rooms with stunning decoration of local touch for nature lovers to stay privately in highlight of <strong>"Reward your Journey"</strong>. Free Wi-Fi in whole resort, A Seafood &amp; International Restaurant, Two Bars by poolside, Two separate Swimming Pools with Jacuzzi, and Beautiful Green Gardens are provided for your convenience.</p>
                        <p>Simply access to famous tourist attractions of Krabi, merely 10 minutes - walk to the beautiful of Aonang Beach</p>
                        <!-- <p><b>If you want a getaway in unique surroundings, prepare to be enchanted by what’s on offer at our adults only hotel, for couples, singles and families with age over 13 years old are allowed.</b></p> -->
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <!-- <div class="d-block" style="text-align: right;">
                            <img src="./images/home/adult-only.png" alt="" width="424" height="120">
                        </div> -->
                        <div class="resp-container">
                            <iframe width="100%" height="440<!-- 315 -->" src="https://www.youtube.com/embed/_Xxx5Ddl0lY?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/deevanakrabiresort/images/promotion/DKR-Promotion-on-WEB-03.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Deevana ที่...คิดถึง</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>อาหารเช้าแบบ new normal สำหรับ 2 ท่าน</li>
                                        <li>ฟรี WIFI ในห้องพัก</li>
                                        <li>เข้าห้องพักได้ทันที เมื่อห้องพักพร้อมเรียบร้อย</li>
                                        <li>ขอ Late Check-out ได้ ถ้าไม่มีแขกใหม่รอเข้าพักต่อ</li>
                                        <!-- <li>20% discount on Spa treatment (except package)</li>
                                        <li>THB 300 cash voucher per stay</li>
                                        <li>Stay 7 nights or more get round transfer from Krabi international airport to hotel</li> -->
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDg2MDA5">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="http://www.deevanahotels.com/deevanakrabiresort/images/promotion/DKR-Relax-&-Experience-600-x-400.jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Relax & Experience</b></h2>
                                    <p class="description" style="color:yellow;">3 days 2 nights</p>
                                    <ul>
                                        <li>Standard Room with daily breakfast </li>
                                        <li>Free Wifi</li>
                                        <li>One time of Thai Set Dinner </li>
                                        <li>Late check-out by 15.00 hrs.</li>
                                        <li>Free upgrade to Grand Deluxe Room</li>              
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDg2NDAx">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=276&group=13&width=450&height=300&imageid=8079&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>HONEYMOON PACKAGE</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 3 nights</p>
                                    <ul>
                                        <li>Daily breakfast at Chaolay Restaurant.</li>
                                        <li>In-room internet</li>
                                        <li>Late check-out until 14.00 (Subject to available)</li>
                                        <li>Romantic set up in room upon arrival day.</li>
                                        <li>One time of private Thai Set Dinner for 2 persons</li>
                                        <li>Thai cooking class Inclusive of menu recipe, apron and cap, Certificate and Photo with frame</li>
                                        <li>Thai massage 1 hour for 2 persons per stay</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=386&onlineId=4&pid=MDgyMzI4&lang=en&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <section id="facilities" class="section section-facilities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#7b9028;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="facilities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_swimming_pool.jpg" />
                        </div>

                        <div class="caption">
                            <h2 class="title">Swimming Pool</h2>
                            <!-- <p class="description">Elegant and luxurious, Swan Spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and  herbal treatments.</p> -->
                            <p><a class="button" href="facilities.php#swimming_pool">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_liabary.jpg" />
                        </div>
                        
                        <div class="caption">
                            <h2 class="title">The Port Library</h2>
                            <!-- <p class="description">Elegant and luxurious, orientala spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and  herbal treatments.</p> -->
                            <p><a class="button" href="facilities.php#restaurant">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/facilities/facilities_chaolay.jpg" />
                        </div>

                        <div class="caption">
                            <h2 class="title">RESTAURANT</h2>
                            <!-- <p class="description">Deevana Plaza Krabi Aonang is home to three popular restaurant and bars. Guests can enjoy authentic Thai and delicious international</p> -->
                            <p><a class="button" href="facilities.php#restaurant">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title">
                    <span style="display: inline-block;" class="deco-map">KRABI ATTRACTIONS</span>
                </h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phi_phi.jpg" /></div>
                            <h2 class="title">PHI PHI ISLAND</h2>
                            <a class="more" href="attraction.php#phi_phi_island">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-tiger_cave_temple.jpg" /></div>
                            <h2 class="title">TIGER CAVE TEMPLE</h2>
                            <a class="more" href="attraction.php#tiger_cave_temple">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-emerald_pool.jpg" /></div>
                            <h2 class="title">EMERALD POOL</h2>
                            <a class="more" href="attraction.php#emerald_pool">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/thalay_wak.jpg" /></div>
                            <h2 class="title">THALAY WAK</h2>
                            <a class="more" href="attraction.php#thalay_wak">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/thma-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/deevanakrabiresort/images/home/Krabi-Resort-SHA.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    #promotion_board img {
        *-webkit-transition: 150ms;
        *transition: 150ms;
    }
    #promotion_board img:hover {
        *-webkit-filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
        *filter: drop-shadow( 0 0 10px rgba(255,255,255,.5) );
    }
	#intro {
		min-height: 500px;
	}
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #facilities {
        background-image: url(images/home/bg-facilities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #facilities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #facilities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #facilities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #facilities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #facilities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #facilities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #facilities_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<?php
if (!isset($_SESSION['visited'])) {
    
}
$_SESSION['visited'] = "true";
?>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });

        $('#facilities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
    });
    var $banner = $('.tl-sticky-banner'),
        $toggle = $('.tl-sticky-banner-toggle'),
        $close = $('.tl-sticky-banner .content-close');

    $toggle.on('click', function() {
        $toggle.addClass('show');
        $banner.addClass('show');
    });

    $close.on('click', function() {
        $toggle.removeClass('show');
        $banner.removeClass('show');
    });

    if( window.innerWidth >= 320 ) {
        $toggle.addClass('show');
        $banner.addClass('show');
    }

    $(window).on('load', function() {
        setTimeout(function() {
            $banner.addClass('ready');
            $toggle.addClass('ready');
        }, 1000);
    });

    $('.sticky-banner-carousel').owlCarousel({
        items: 1,
        loop: 1,
        autoplay: 1,
        smartSpeed: 800,
        margin: 10,
        nav: 1,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        dots: false,
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php include '_footer.php'; ?>

<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.pkgd.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.16/moment-timezone-with-data-2012-2022.min.js'></script>
<script>
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      var bannerGallery = {
        active: true,
        data: [
            {
            active: true,
            src: 'http://www.deevanahotels.com/deevanakrabiresort/images/banners/DKR-Pop-up-Banner.jpg',
            alt: '',
            link: {
              href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=386&onlineId=4',
              target: '_blank',
            }
          },
          {
            active: true,
            src: 'http://www.deevanahotels.com/deevanakrabiresort/images/banners/DKR-Promotion-999.jpg',
            alt: '',
            link: {
              href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=386&onlineId=4',
              target: '_blank',
            }
          },
          {
            active: false,
            src: 'http://www.deevanahotels.com/deevanakrabiresort/images/banners/New-Normal-of-Deevana-02-DKR.jpg',
            alt: '',
            link: {
              href: '',
              target: '_blank',
            }
          },
          {
            active: false,
            src: 'images/banner/Holiday-package-032020.jpg',
            alt: '',
            link: {
              href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=277&onlineId=4&checkinauto=0&numofnight=2&numofadult=2&numofchild=0&numofroom=1&pid=MDgxODE3',
              target: '_blank',
            }
          }
        ],
      }
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      // ADD BANNER HERE
      var bannerLength = 0;
      var bannerTemplate = '';
  
      bannerTemplate += '<div id="promotion_carosuel_container" class="mx-auto p-relative" style="max-width: 600px;margin:auto;width:100%;height:auto;"><div id="promotion_carousel" class="owl-carousel">';
      $.each(bannerGallery.data, function(i, el) {
          if (!!el.active) {
            bannerTemplate += getBannerGalleryHTML(i);
            bannerLength = ++i;
          }
      });
      bannerTemplate += '</div></div>';
  
      if (bannerGallery.active && dateChecker('2018-07-13', '2020-12-31')) {
          runSlidePopupBanner();
      }
  
      function runSlidePopupBanner() {
          return $.magnificPopup.open({
              items: {
                  type: 'inline',
                  src: bannerTemplate,
              },
              callbacks: {
                  open: function() {
                      var moreThanOne = (bannerLength > 1) ? true : false;
                      $('#promotion_carousel').imagesLoaded().progress(function() {
                          $('#promotion_carousel').owlCarousel({
                              items: 1,
                              autoHeight: 1,
                              loop: moreThanOne,
                              autoplay: moreThanOne,
                              mouseDrag: moreThanOne,
                              pullDrag: moreThanOne,
                              touchDrag: moreThanOne,
                              smartSpeed: 350,
                              animateOut: 'fadeOut',
                              animateTimeout: 3000,
                          });
                      });
                  }
              },
              mainClass: 'mfp-fade',
              removalDelay: 350,
              closeBtnInside: 0,
          });
      }
  
      function getBannerGalleryHTML(index) {
          var html    = '';
          var image   = bannerGallery.data[index];
  
          if (image.link.href !== undefined) {
              html += '<a href="'+image.link.href+'" target="'+image.link.target+'">';
          }
          html += '<img src="'+image.src+'" width="'+image.width+'" height="'+image.height+'" alt="'+image.alt+'" />';
          if (!image.link.href !== undefined) {
              html += '</a>';
          }
          return html;
      }
  
      function dateChecker(dateFrom, dateTo) {
          var result = false;
          if (moment().tz('Asia/Bangkok').isBetween(dateFrom, dateTo))
              result = true;
          return result;
      }
  </script>
