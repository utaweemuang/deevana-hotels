<?php
$title = 'Contact Deevana Hotels & Resorts';
$desc = 'Deevana Hotels & Resorts - Book here now for best rates guaranteed throughout Phuket and Krabi';
$keyw = 'Thailand, Phuket beach, resort, accommodation, Phuket Hotels, Hotels Phuket, Hotel Patong, Patong Hotel, holiday Phuket, conferences, phuket vacations, phuket spa, krabi hotel';

$html_class = '';
$body_class = 'contact';
$cur_page = 'contact';

$lang_en = '/contact.php';
$lang_th = '/th/contact.php';
$lang_zh = '/zh/contact.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/contact/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <section class="main-content sidebar-left">
            <div class="container">
                <div class="row">
                    <div class="col-w8 col-content">
                        <div class="site-content">
                            
                            <section class="section section-information">
                                <header class="section-header">
                                    <h1 class="section-title">Contact Information</h1>

                                    <span class="custom-select" id="detail-select">
                                        <span class="selected">Quick Search Hotels</span>
                                        
                                        <div class="options-box">
                                            <ul class="options">
                                                <li class="option active" data-value="#hotel-1">Deevana Hotels &amp; Resorts</li>
                                                <li class="option-group">
                                                    <ul class="sub-options">
                                                        <li class="option-label">PHUKET</li>
                                                        <li class="option" data-value="#hotel-2">Deevana Plaza Phuket Patong</li>
                                                        <li class="option" data-value="#hotel-3">Deevana Patong Resort &amp; Spa</li>
                                                        <li class="option" data-value="#hotel-4">Ramada by Wyndham Phuket Deevana Patong </li>
                                                        <li class="option" data-value="#hotel-5">Recenta Suite Phuket Suanluang</li>
														<li class="option" data-value="#hotel-6">Recenta Phuket Suanluang</li>
														<li class="option" data-value="#hotel-7">Recenta Style Phuket Town</li>
                                                    </ul>
                                                </li>

                                                <li class="option-group">
                                                    <ul class="sub-options">
                                                        <li class="option-label">KRABI</li>
                                                        <li class="option" data-value="#hotel-8">Deevana Plaza Krabi Aonang</li>
                                                        <li class="option" data-value="#hotel-9">Deevana Krabi Resort &amp; Spa</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </span>
                                </header>

                                <div class="content tabs-group">
                                    
                                    <div id="hotel-1" class="tab-content default">
										<div class="gmap">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.8394287594865!2d100.53025651521561!3d13.72816950153842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f2c880c2dad%3A0x2c04c3ade1dd3f6f!2sKasemkij+Building!5e0!3m2!1sen!2sth!4v1463026705419" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>
										
                                        <h2><span class="font-roboto" style="font-weight: 300;">Deevana Hotels &amp; Resorts</span></h2>
                                        <dl>
                                            <!-- <dt>Bangkok Office:</dt>
                                            <dd>4th Floor, Room 404, Kasemkij Building, 120 Silom Road, Bangkok 10500, Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+66 (0) 2632 6661">+66 (0) 2632 6661</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+66 (0) 2632 6676">+66 (0) 2632 6676</a></dd> -->

                                            <dt>Sales Office:</dt>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:sales.clusterphuket@deevanahotels.com">sales.clusterphuket@deevanahotels.com</a></dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+66864708961">+66 (0) 86 470 8961</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Deevana Plaza Phuket Patong</span></h2>
                                        <dl>
                                            <dt>Address:</dt>
                                            <dd>239/14 Raj-U-Thid 200 Pee Road - Patong, Phuket, 83150 Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676302100">+66 (0) 76 302 100</a></dd>
                                            <dt>Reservation:</dt>
                                            <dd><a href="tel:+6676302100">+66 (0) 76 302 100</a>/ (Office hours: 08:30 – 16:00 hrs.)</dd>
                                            <dt>Hotline:</dt>
                                            <dd><a href="tel:+66661212298">+66 (0) 66 121 2298</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676302111">+66 (0) 76 302 111</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info.dpp@deevanahotels.com">info.dpp@deevanahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Deevana Patong Resort &amp; Spa</span></h2>
                                        <dl>
                                            <dt>Address:</dt>
                                            <dd>43/2 Raj-U-Thid 200 Pee Road, Patong Beach, Kathu, Phuket 83150 Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676317179">+66 (0) 76 317 179</a></dd>
                                            <dt>Reservation:</dt>
                                            <dd><a href="tel:+6676317165">+66 (0) 76 317 165</a>/ (Office hours: 08:30 – 16:00 hrs.)</dd>
                                            <dt>Hotline:</dt>
                                            <dd><a href="tel:+66815377875">+66 (0) 81 537 7875</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676341706">+66 (0) 76 341 706</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info.dpr@deevanahotels.com">info.dpr@deevanahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Ramada by Wyndham Phuket Deevana Patong </span></h2>
                                        <dl>
                                            <dt>Address:</dt>
                                            <dd>45/1 Raj-U-Thid 200 Pee Road, Patong Beach, Kathu, Phuket 83150 Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676207500">+66 (0) 76 207 500</a></dd>
                                            <dt>Reservation:</dt>
                                            <dd><a href="tel:+6676207555">+66 (0) 76 207 555</a>/ (Office hours: 08:30 – 16:00 hrs.)</dd>
                                            <dt>Hotline:</dt>
                                            <dd><a href="tel:+66992871639">+66 (0) 99 287 1639</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676207599">+66 (0) 76 207 599</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info.rpd@deevanahotels.com">info.rpd@deevanahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Recenta Suite Phuket Suanluang</span></h2>
                                        <dl>
                                            <dt>Sales Office:</dt>
                                            <dd>60/81 Wichit, Muang Phuket, Phuket, Thailand 83000</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676201000">+66(0) 76 201 000</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 76 201 039</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Recenta Phuket Suanluang</span></h2>
                                        <dl>
                                            <dt>Sales Office:</dt>
                                            <dd>60/80 Moo2, Wichit, Muang Phuket, Phuket, Thailand 83000</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676201000">+66(0) 7620 1000</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 7620 1039</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Recenta Style Phuket Town</span></h2>
                                        <dl>
                                            <dt>Sales Office:</dt>
                                            <dd>10/1 Rattanakorsin 200 Pee Road, TaladNua, Muang Phuket, Phuket Thailand 83000</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6676214344">+66(0) 76 214 344</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 76 201 039</a></dd>
                                            <dt>Email:</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Deevana Plaza Krabi Aonang</span></h2>
                                        <dl>
                                            <dt>Sales Office:</dt>
                                            <dd>4th Floor, Room 404, Kasemkij Building, 120 Silom Road, Bangkok 10500, Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6626329474-5">+66(0) 2632 9474-5</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6622336144">+66(0) 2233 6144</a></dd>
                                        </dl>
										
										<hr>
										
										<h2><span class="font-roboto" style="font-weight: 300;">Deevana Krabi Resort</span></h2>
                                        <dl>
                                            <dt>Sales Office:</dt>
                                            <dd>4th Floor, Room 404, Kasemkij Building, 120 Silom Road, Bangkok 10500, Thailand</dd>
                                            <dt>Tel:</dt>
                                            <dd><a href="tel:+6626329474-5">+66(0) 2632 9474-5</a></dd>
                                            <dt>Fax:</dt>
                                            <dd><a href="tel:+6622336144">+66(0) 2233 6144</a></dd>
                                        </dl>
                                    </div>
									
                                </div>
                            </section>
                        </div>
                    </div>

                    <div class="col-w4 col-sidebar">
                        <div class="sidebar">
                            <aside class="aside">
                                <h3 class="title">Hotel Fact Sheet</h3>
                                <p><a class="download-button" href="#"><i class="icon fa fa-cloud-download"></i> English</a></p>
                            </aside>

                            <aside class="aside">
                                <h3 class="title">Quick Link</h3>
                                <ul>
                                    <li><a href="#">Media Contact</a></li>
                                    <li><a href="#">Careers</a></li>
                                </ul>
                            </aside>

                            <aside class="aside">
                            <h3 class="title">LINE Official</h3>
                            
                                <a href="https://line.me/R/ti/p/@919hutuw" target="_blank"><img src="http://www.deevanapatong.com/images/line-oa.jpg" width="auto"></a>
                            
                            </aside>

                            <aside class="aside">
                                <h3 class="title">Let Us Know What You Think</h3>
                                <p>Your feedback is important to us, please share with us any comments you may have about our website, your stay at Deevana Hotels &amp; Resorts</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $(function() {
        $('#detail-select').on('click', '[data-value]', function() {
            var $this = $(this);
            var tar = $this.data('value');
            $('.tabs-group').find(tar).fadeIn(300).siblings().hide();
            
            $this.addClass('active');
            $this.parents('ul.options').find('li.option').not($this).removeClass('active');
        });
        $('.tabs-group').find('.tab-content.default').show();
    });
</script>

<style>
    .section {
        border: 1px solid #ccc;
        margin-bottom: 30px;
    }
    .section-header {
        background-color: #1a355e;
        color: #fff;
        padding: 10px 30px;
        position: relative;
    }
    .section-title {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        color: #fff;
        text-align: left;
    }
    .section .content {
        padding: 20px 30px 10px;
    }
    
    #contact_form .field {
        display: block;
    }
    #contact_form .input-text,
    #contact_form .input-select,
    #contact_form .input-textarea {
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    #contact_form #submit {
        background-color: #9a7b12;
        border-radius: 4px;
        color: #fff;
        border: 0;
        height: 32px;
        line-height: 32px;
        width: 100px;
        text-align: center;
    }
    #contact_form #submit:hover {
        background-color: #333;
    }
    #detail-select {
        position: absolute;
        top: 50%;
        right: 30px;
        margin-top: -18px;
    }
	.gmap {
		margin-top: -20px;
		margin-left: -30px;
		margin-right: -30px;
		margin-bottom: 20px;
		border-bottom: 1px solid #ccc;
	}
	.gmap iframe {
		display: block;
		width: 100%;
		height: 200px;
	}
    .download-button {
        background-color: #333;
        padding: 0 12px;
        border-radius: 2px;
        display: inline-block;
        line-height: 2;
        color: #fff;
    }
    .download-button .icon {
        margin-right: 3px;
    }
    .download-button:hover {
        background-color: #444;
        color: #fff;
    }
    .tabs-group .tab-content {
        display: none;
    }
    @media (max-width: 640px) {
        .section-header {
            padding-left: 15px;
            padding-right: 15px;
        }
        .section .content {
            padding: 15px 15px 10px;
        }
        .row-contact-form .col-w6 {
            width: 100%;
        }
    }
    @media (max-width: 600px) {
        #detail-select {
            position: relative;
            top: inherit;
            right: inherit;
            margin-top: 10px;
            width: 100%;
        }
    }
</style>

<?php include_once('_footer.php'); ?>