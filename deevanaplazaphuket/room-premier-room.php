<?php
$title = 'Premier Waterfront | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Premier Waterfront: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'premier room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphulet/room-premier-room.php';
$lang_th = '/th/deevanaplazaphulet/room-premier-room.php';
$lang_zh = '/zh/deevanaplazaphulet/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Premier Waterfront <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Premier Waterfront</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Enjoy the unique design of the guestrooms facilities and extra services such as the complimentary soft drinks and daily afternoon break at Executive Lounge that makes your stay more productive and as pleasant as possible.</p>
                            <p>
                                Rooms available: 26<br>
                                Area: 35 sq.m.
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>King size bed (6ft) or twin beds (4ft)</li>
                                        <li>42 inches LCD TV</li>
                                        <li>Living chair</li>
                                        <li>Living sofa</li>
                                        <li>Alarm clock</li>
                                        <li>IDD Telephone with voice mail</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>In-room amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King size bed (6ft) or twin beds (4ft)</li>
                                            <li>42 inches LCD TV</li>
                                            <li>Living chair</li>
                                            <li>Living sofa</li>
                                            <li>Alarm clock</li>
                                            <li>IDD Telephone with voice mail</li>
                                            <li>Pool view</li>
                                            <li>Complimentary tea & coffee</li>
                                            <li>Smoke alarm & detector</li>
                                            <li>Sprinkler system</li>
                                            <li>Electronic Safe deposit box</li>
                                            <li>Iron with iron board</li>
                                            <li>Reading lamp</li>
                                            <li>Universal power outlets</li>
                                            <li>Bathroom fittings</li>
                                            <li>Hair dryer</li>
                                            <li>Bathtub and separate shower stall</li>
                                            <li>Magnifying mirror</li>
                                            <li>Weight scale</li>
                                        </ul>

                                        <h2>Extra privileges when you are upgrading to Premier Waterfront</h2>
                                        <ul class="list-columns-2">
                                            <li>Welcome platter on arrival date</li>
                                            <li>Complimentary welcome drink</li>
                                            <li>Complimentary mini bar</li>
                                            <li>Afternoon tea during 15.30-16.30hrs</li>
                                            <li>Daily afternoon tea (2 person per room)</li>
                                            <li>Daily mini bar (soft drink & snack)</li>
                                            <li>Additional bath amenities</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>