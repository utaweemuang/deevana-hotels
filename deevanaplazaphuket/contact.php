<?php
$title = 'Contact | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Contact: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'contact, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'contact';
$cur_page = 'contact';

$lang_en = '/deevanaplazaphuket/contact.php';
$lang_th = '/th/deevanaplazaphuket/contact.php';
$lang_zh = '/zh/deevanaplazaphuket/contact.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/contact/contact-slide-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>

        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content sidebar-left pattern-fibers">
        <div class="container">
            <div class="row">
                <div class="col-w8 col-content">
                    
                    <section class="section section-information">
                        <header class="section-header">
                            <h1 class="section-title">Hotel Contact Information</h1>
                        </header>

                        <div class="content">
                            <div class="row row-contact">
                                <div class="col-w6">
                                    <h2>Deevana Plaza Phuket Patong</h2>
                                    <dl>
                                        <dt>GPS:</dt>
                                        <dd>N 7°53'12.8 E 98°17'39.5</dd>

                                        <dt>Address:</dt>
                                        <dd>239/14 Raj‐U‐Thid 200 Pee Road, Patong, Phuket, 83150 Thailand</dd>
                                        
                                        <dt>Tel:</dt>
                                        <dd><a href="tel:+6676302100">+66 (0) 76302 100</a></dd>

                                        <dt>Reservation:</dt>
                                        <dd><a href="tel:+6676302100">+66 (0) 76302 100 / (Office hours: 08:30 – 16:00 hrs.)</a></dd>
                                        
                                        <dt>Hotline:</dt>
                                        <dd><a href="tel:+661212298">+66 (0) 66 121 2298</a></dd>

                                        <dt>Fax:</dt>
                                        <dd><a href="tel:+6676302111">+66 (0) 76302 111</a></dd>
                                        
                                        <dt>Email:</dt>
                                        <dd><a href="mailto:info.dpp@deevanahotels.com">info.dpp@deevanahotels.com</a></dd>
                                    </dl>
                                    
                                    <hr>
                                    
                                    <!-- <h2>Sales Office</h2>
                                    <dl>
                                        <dt>Address:</dt>
                                        <dd>4th Floor, Room 404, Kasemkij Building, 120 Silom Road, Bangkok 10500, Thailand</dd>

                                        <dt>Tel:</dt>
                                        <dd><a href="tel:+66 (0) 2632 8565">+66 (0) 2632 8565</a></dd>

                                        <dt>Fax:</dt>
                                        <dd><a href="tel:+66 (0) 2233 6144">+66 (0) 2233 6144</a></dd>

                                        <dt>Email:</dt>
                                        <dd><a href="mailto:salesco@deevanaplazaphuket.com">salesco@deevanaplazaphuket.com</a></dd> -->
                                    </dl>
                                </div>

                                <div class="col-w6">
                                    <h2><span class="font-roboto" style="font-size: 18px; color: #78a321; font-weight: 300;">Resort Location &amp; Map</span></h2>
                                    <p>
                                        <iframe class="gmap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.0914722295925!2d98.29187831527655!3d7.885498308017599!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30503ab0e3c69d55%3A0x84c3be8f3965563e!2sDeevana+Plaza+Phuket+Patong!5e0!3m2!1sen!2sth!4v1462263061351" frameborder="0" allowfullscreen></iframe>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="section section-contact">
                        <header class="section-header">
                            <h1 class="section-title">Drop Us A Message</h1>
                        </header>

                        <div class="content">
                            <form id="contact_form" class="form" action="forms/contact_form.php">
                                <div id="contact_result"></div>
                                <div class="row row-contact-form">
                                    <div class="col-w6">
                                        <div class="form-group">
                                            <span class="field field-name field-required">
                                                <label class="label" for="name">Name</label>
                                                <input class="input-text" id="name" name="name" type="text" required />
                                            </span>
                                        </div>

                                        <div class="form-group">
                                            <span class="field field-email field-required">
                                                <label class="label" for>Email</label>
                                                <input class="input-text" id="email" name="email" type="email" required />
                                            </span>
                                        </div>

                                        <div class="form-group">
                                            <span class="field field-contry field-required">
                                                <label class="label" for="country">Country</label>
                                                <input class="input-text" id="country" name="country" type="text" required />
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-w6">
                                        <div class="form-group">
                                            <span class="field field-meesage field-required">
                                                <label class="label" for="message">Message</label>
                                                <textarea class="input-textarea" id="message" name="message" required></textarea>
                                            </span>
                                        </div>

                                        <div class="form-group">
                                            <span class="field field-recaptcha field-required">
                                                <div class="g-recaptcha" data-sitekey="6LeT9RkTAAAAAG5Xj-B4P_fvYQr5HMppJYW1FWup"></div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-w12">
                                        <div class="form-group">
                                            <span class="field field-submit">
                                                <input type="hidden" name="sendto" value="<?php echo get_info('email'); ?>" />
                                                <button class="button" id="submit" type="submit">SEND</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                    
                </div>

                <div class="col-w4 col-sidebar">
                    
                    <div class="sidebar">
                        <aside class="aside">
                            <h3 class="title">Hotel Fact Sheet</h3>
                            <p><a class="download-button" href="#"><i class="icon fa fa-cloud-download"></i> English</a></p>
                        </aside>

                        <aside class="aside">
                            <h3 class="title">Quick Link</h3>
                            <ul>
                                <li><a href="#">Media Contact</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </aside>

                        <aside class="aside">
                            <h3 class="title">LINE Official</h3>
                            
                                <a href="https://line.me/R/ti/p/@919hutuw" target="_blank"><img src="http://www.deevanapatong.com/images/line-oa.jpg" width="auto"></a>
                            
                        </aside>

                        <aside class="aside aside-let-you-know">
                            <h3 class="title">Let Us Know What You Think</h3>
                            <p>Your feedback is important to us, please share with us any comments you may have about our website, your stay at Deevana Hotels &amp; Resorts</p>
                        </aside>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
        
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .section {
        border: 1px solid #ccc;
        margin-bottom: 30px;
    }
    .section-header {
        background-color: #1a355e;
        color: #fff;
        padding: 10px 30px;
        position: relative;
    }
    .section-title {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        color: #fff;
        text-align: left;
    }
    .section .content {
        padding: 20px 30px 10px;
    }
    .section-information,
    .section-contact {
        background-color: #fff;
    }
    .row-contact h2 {
        font-family: 'Roboto', sans-serif;
        font-size: 18px;
        font-weight: 300;
        color: #78a321;
    }
    .row-contact .gmap {
        width: 100%;
        height: 300px;
        border: 0;
    }
    #contact_form .field {
        display: block;
    }
    #contact_form .input-text,
    #contact_form .input-select,
    #contact_form .input-textarea {
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    #contact_form #submit {
        background-color: #1a355e;
        border-radius: 4px;
        color: #fff;
        border: 0;
        height: 32px;
        line-height: 32px;
        width: 100px;
        text-align: center;
    }
    #contact_form #submit:hover {
        background-color: #264370;
    }
    #contact_result { display: none; }
    #contact_result.success {
        border: 2px solid yellowgreen;
        margin-bottom: 10px;
        padding: 5px 10px;
        border-radius: 4px;
    }
    #contact_result.success .icon {
        color: yellowgreen;
    }
    /* Download Button */
    .download-button {
        background-color: #333;
        padding: 0 12px;
        border-radius: 2px;
        display: inline-block;
        line-height: 2;
        color: #fff;
    }
    .download-button .icon {
        margin-right: 3px;
    }
    .download-button:hover {
        background-color: #444;
        color: #fff;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 640px) {
        .section-header {
            padding-left: 15px;
            padding-right: 15px;
        }
        .section .content {
            padding: 15px 15px 10px;
        }
        .row-contact .col-w6,
        .row-contact-form .col-w6 {
            width: 100%;
        }
    }
</style>

<?php include_once('_footer.php'); ?>