<?php
$title = 'Attractions | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Attractions: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'attractions, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'attraction';
$cur_page = 'attraction';

$lang_en = '/deevanaplazaphuket/attraction.php';
$lang_th = '/th/deevanaplazaphuket/attraction.php';
$lang_zh = '/zh/deevanaplazaphuket/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#patong_beach">Patong Beach</li>
                            <li class="tab" data-tab="#phromthep_cape">Phromthep Cape</li>
                            <li class="tab" data-tab="#kata_and_karon_beaches">Kata and Karon Beaches</li>
                            <li class="tab" data-tab="#big_buddha">Big Buddha</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="patong_beach" data-tab-name="Patong Beach">
                        <img class="thumbnail force" src="images/attraction/patong_beach.png" alt="Patong Beach" />
                        <h1 class="title">Patong Beach</h1>
                        <p>Patong Beach is possibly the most famous beach on Phuket, with its wide variety of activities and nightlife. By night the town has a bustling nightlife, which includes hundreds of restaurants.</p>
                    </article>
                    
                    <article class="article" id="phromthep_cape" data-tab-name="Phromthep Cape">
                        <img class="thumbnail force" src="images/attraction/phromthep_cape.png" alt="Phromthep Cape" />
                        <h1 class="title">Phromthep Cape</h1>
                        <p>Phromthep cape, set an top of a hill on the Southernmost tip of the island, is a beautiful viewpoint to watch the sunset. The orange sun falling from blue skies into blue seas is undoubtedly mesmerizing.</p>
                    </article>
                    
                    <article class="article" id="kata_and_karon_beaches" data-tab-name="Kata and Karon Beaches">
                        <img class="thumbnail force" src="images/attraction/karon_kata_beach.png" alt="Kata and Karon Beaches" />
                        <h1 class="title">Kata and Karon Beaches</h1>
                        <p>Kata and Karon Beaches are known as the warmest, clearest, and most family friendly locations on Phuket. These beaches are popular amongst families, who choose to enjoy their activities on the long expansive sands, under the warm island sun. Between May and October, surfers will flock to catch waves, while during November and April the beaches are alive with sun seekers.</p>
                    </article>
                    
                    <article class="article" id="big_buddha" data-tab-name="Big Buddha">
                        <img class="thumbnail force" src="images/attraction/big_buddha_phuket.png" alt="Big Buddha" />
                        <h1 class="title">Big Buddha</h1>
                        <p>Phuket's Big Buddha is one of the island's most important and revered landmarks. The huge image sits on top of the Nakkerd Hills between Chalong and Kata and at 45 metres high it is easily seen from far away.</p>
                        <p>The lofty site offers the best 360-degree views of the island (think sweeping vistas of Phuket Town, Kata, Karon beaches, Chalong Bay and more.) Easily reachable via a six-kilometre road leading from Phuket's main artery, it's a must-visit island destination.</p>
                        <p>Close up to the image itself it is very peaceful and the only noises you will hear are the tinkling of small bells and the yellow Buddhist flags in the compound flapping in the wind plus soft background dharma music.</p>
                        <p>Known among Thais as the Phra Puttamingmongkol Akenakkiri Buddha in full, it is 25 meters across at the base. The whole body is layered with beautiful white Burmese marble that shines in the sun, making it a natural symbol of hope. The views, and the actual image itself are all breathtaking.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: 100%;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>