<?php
$title = 'Promotion | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Promotion: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'promotion, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$body_class = 'promotion';
$cur_page = 'promotion';

$lang_en = '/deevanaplazaphuket/promotion.php';
$lang_th = '/th/deevanaplazaphuket/promotion.php';
$lang_zh = '/zh/deevanaplazaphuket/promotion.php';

include_once('_header.php');
?>
        
<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section">
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="#"><img class="force" src="images/promotion/20160516/special_offer.jpg" alt="Special Offer" /></a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">SUMMER PACKAGE</h1>
                            
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>3 nights in Deluxe City View</li>
                                <li>Daily Buffet Breakfast at Phuket Cafe</li>
                                <li>In-room internet</li>
                                <li>one Set Menu Dinner for 2 persons</li>
                                <li>one hour of Thai Massage for 2 persons</li>
                                <li>One Bottle of Local Beer for 2 persons</li>
                                <li>NOT combine with Best Deal Guarantee Benefit and all other promotions</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes are included<br>
                                    <span class="discount">THB 7,750</span> per package
                                </h3>
                                <ul>
                                    <li><span class="label">Room type:</span> Deluxe City View</li>
                                    <li><span class="label">Period:</span> Now - 30 September 2016</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url(275, 'en', 'MDgxNDgz'); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>

            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<style>
    .site-main {
        background-image: url(images/promotion/bg-promotion.jpg);
        background-attachment: fixed;
        background-size: cover;
    }
    .article {
        background-color: #e1e1e1;
        margin-bottom: 30px;
        padding: 20px;
        color: #666;
        min-height: 190px;
    }
    .article .title {
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        font-weight: 700;
        color: #adc32b;
        margin-bottom: 15px;
    }
    .article .list-heading {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .article .list-heading + ul {
        font-size: 12px;
        margin-top: 3px;
    }
    .article .booking {
        background-color: #666;
        color: #cbcbcb;
        border-radius: 6px;
        padding: 12px;
        font-size: 12px;
    }
    .article .booking:after {
        content: '';
        display: block;
        clear: both;
    }
    .article .booking h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 12px;
    }
    .article .booking .discount {
        font-size: 22px;
        font-weight: 700;
        color: #bed62f;
    }
    .article .booking ul {
        list-style: none;
        margin-top: 5px;
        padding-left: 0;
    }
    .article .booking li {
        border-bottom: 1px dotted #cbcbcb;
        padding: 5px 0;
    }
    .article .booking .label {
        display: inline-block;
        width: 80px;
    }
    .article .booking .button {
        background-color: #8fc31c;
        background-image: linear-gradient(to bottom, #c0f844,#8fc31c);
        color: #fff;
        padding: 0 10px;
        line-height: 2;
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 14px;
        font-weight: 700;
    }
    @media (max-width: 720px) {
        .col-thumbnail,
        .col-info {
            width: 50%;
        }
        .col-booking {
            width: 100%;
        }
        .article .booking {
            margin-top: 20px;
            border-radius: 0;
        }
    }
    @media (max-width: 480px) {
        .col-thumbnail,
        .col-info {
            width: 100%;
        }
        .col-thumbnail {
            margin-bottom: 20px;
        }
        .col-booking .booking {
            border-radius: 0;
        }
        .article {
            padding: 12px;
        }
        .article .title {
            font-size: 24px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>