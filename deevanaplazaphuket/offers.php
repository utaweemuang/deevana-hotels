<?php
$title = 'Special Offers | Official Hotel Group Website Thailand';
$desc = '4 star hotel near Jungceylon and bangla street; Guarantee best direct hotel rate and best location on Patong Beach';
$keyw = 'special offers, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach, Bangla Road, phuket night market, Jungceylon';

$html_class = '';
$body_class = 'offer';
$cur_page = 'offer';

$lang_en = '/deevanaplazaphuket/offers.php';
$lang_th = '/th/deevanaplazaphuket/offers.php';
$lang_zh = '';

include '_header.php';
?>
<style>
    .site-content > .container {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .section-header {
        text-align: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
</style>

<style>
    :root {
        --accent-color: hsl(217, 55%, 24%);
        --accent-color-darker: hsl(217, 55%, 19%);
    }
    .offer-item {
        background-color: #fff;
        border-radius: .25rem;
        overflow: hidden;
        border: 1px solid #ddd;
        margin-bottom: 30px;
        padding: 20px 25px;
    }
    .offer-item p,
    .offer-item ul {
        margin: 0 0 1rem;
    }
    .offer-item__image {
        margin: -20px -25px 20px;
    }
    .offer-item__image img {
        display: block;
        width: 100%;
        height: auto;
    }
    .offer-item__heading {
        font-size: 1.5rem;
        font-weight: 700;
        margin: 0 0 .5rem;
        color: var(--accent-color);
    }
    .offer-item__content {
        font-size: 0.875rem;
        font-weight: 300;
    }
    .offer-item__content > *:last-child {
        margin-bottom: 0;
    }
    .offer-item__cta {
        margin-top: 1rem;
    }
    .offer-item a {
        color: var(--accent-color);
    }
    .offer-item a:hover,
    .offer-item a:active {
        color: var(--accent-color-darker);
    }
    .offer-item .btn-cta {
        background-color: var(--accent-color);
        color: #fff;
        transition: .2s ease-in-out;
        display: block;
        padding: 0.375rem 0.75rem 0.425rem;
        text-decoration: none;
        text-align: center;
        text-transform: uppercase;
        border-radius: 0.25rem;
    }
    .offer-item .btn-cta:hover,
    .offer-item .btn-cta:active {
        background-color: var(--accent-color-darker);
        color: #fff;
    }
</style>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once 'include/booking_bar.php'; ?>

    <section class="site-content">

        <div class="container">
            <div class="section-header">
                <h1>Special Offers</h1>
            </div>

            <div class="row" id="offer_query">
                <div class="text-center">Loading...</div>
            </div>
        </div>

    </section>
</main>

<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/template7@1.4.2/dist/template7.min.js" integrity="sha256-AR7mrm2lYY5i4wWxxAznbMCTVOCMD5kvqLUmtW/6iCY=" crossorigin="anonymous"></script>

<script id="offer_template" type="text/template7">
    {{#each posts}}
    	<div class="col-12 col-md-6 col-xl-4 mb-4">
    		<div class="offer-item">
    			{{#if image}}
    				<figure class="offer-item__image">
    					{{img className="w-100 h-auto d-block" image}}
    				</figure>
    			{{/if}}

    			<div class="p-4">
    				<h2 class="offer-item__heading">{{title}}</h2>
    				<div class="offer-item__content">
    					{{content}}
    				</div>
    				{{#if button.enable}}
    					<div class="offer-item__cta">
    						<a class="btn-cta" href="{{button.link_url}}" target="{{button.link_target}}">{{button.link_text}}</a>
    					</div>
    				{{/if}}
    			</div>
    		</div>
    	</div>
    {{/each}}
</script>

<script>
    (function($) {
        var origin = 'https://webdemo2.travelanium.net/deevana/cockpit';
        var token = '3fa7cf86aa69b4028f4d0adbe6c735';
        var collection = 'deevanaPlazaPatongOffers';
        var path = '%base%/api/collections/get/%collection%?token=%token%&rspc=1'
          .replace('%base%',origin)
          .replace('%collection%',collection)
          .replace('%token%',token);
              
        var data = $.getJSON(path);
  
        Template7.registerHelper('img', function(image, options) {
          var imgApi = origin + '/api/cockpit/image';
          var imgPath = origin + '/storage/uploads' + image.path;
          var url = '%base%?token=%token%&src=%path%&rspc=1&w=600&m=bestFit&q=82&o=true'
            .replace('%base%', imgApi)
            .replace('%token%', token)
            .replace('%path%', imgPath);

          return '<img class="%className%" src="%src%" loading="lazy" />'
            .replace('%className%', options.hash.className)
            .replace('%src%', url);
        });
  
        data.then(function(res) {
          var template = $('#offer_template').html();
          var items = res.entries.filter(function(item) {
            return item.published
          });
          if (items.length) {
            var compiled = Template7.compile(template);
            var html = compiled({
              posts: items,
            });
            $('#offer_query').html(html);
          }
        });
    })(jQuery);
</script>

<?php include '_footer.php'; ?>