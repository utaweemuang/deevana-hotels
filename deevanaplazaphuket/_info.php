<?php

$url        = 'http://www.deevanahotels.com/deevanaplazaphuket/';
$name       = 'Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$version    = '20180330';
$email      = 'es@deevanaplazaphuket.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/DeevanaPlazaPhuket';
$twitter    = '#';
$googleplus = '#';
$youtube    = 'https://www.youtube.com/watch?v=osblJhCzAD8';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/deevanaplazaphuket';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = 'http://www.tripadvisor.com/Hotel_Review-g297930-d754312-Reviews-Deevana_Plaza_Phuket-putong_kathu_Phuket.html';

$ibeID      = '275';