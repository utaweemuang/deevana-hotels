<?php
$title = 'Family Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Family Room: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'family room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-family-room';
$cur_page = 'family-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-family-room.php';
$lang_th = '/th/deevanaplazaphuket/room-family-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-family-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/family/1500/family-01.jpg" alt="Family Room 01" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 02" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Family Room <span>King size and Bunk beds</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/family/600/family-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Family Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/family/600/family-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Sharing quality time is the concept behind Deevana Plaza Phuket Patong ‘s family rooms, with individual space for Mom and Dad to cuddle up at night and bunk beds in the adjoining area for children. Facing the city view, the rooms offer a relaxing escape and intimate space for family members to enjoy each other’s company.</p>
                            <p>
                                Rooms available: 10<br>
                                Area: 44 sq.m.
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>King size bed (6ft) and bunk beds</li>
                                        <li>42 inches LCD TV</li>
                                        <li>Video game console on request</li>
                                        <li>Dining tables with chairs</li>
                                        <li>Free WIFI Internet</li>
                                        <li>IDD Telephone with voice mail</li>
                                        <li>City view</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>In‐room amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King size bed (6ft) and bunk beds</li>
                                            <li>42 inches LCD TV</li>
                                            <li>Video game console on request</li>
                                            <li>Dining tables with chairs</li>
                                            <li>Free WIFI Internet</li>
                                            <li>IDD Telephone with voice mail</li>
                                            <li>City view</li>
                                            <li>No Balcony</li>
                                            <li>Complimentary tea & coffee</li>
                                            <li>Smoke alarm & detector</li>
                                            <li>Sprinkler system</li>
                                            <li>Electronic Safe deposit box</li>
                                            <li>Universal power outlets</li>
                                            <li>Reading lamp</li>
                                            <li>Dripping coffee machine</li>
                                            <li>Bathroom fittings</li>
                                            <li>Bathtub and separate shower stall</li>
                                            <li>Make-up/magnifying mirror</li>
                                            <li>Hair dryer</li>
                                            <li>Weight scale</li>
                                        </ul>

                                        <h2>Extra privileges when you are upgrading to Family room</h2>
                                        <ul class="list-columns-2">
                                            <li>Welcome platter on arrival date</li>
                                            <li>Daily afternoon tea (2 person per room)</li>
                                            <li>Daily mini bar (soft drink & snack)</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>