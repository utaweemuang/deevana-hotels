var {task, src, dest, watch, parallel, series} = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('dart-sass');
var postcss = require('gulp-postcss');
var postcssPreserEnv = require('postcss-preset-env');
var cssnano = require('cssnano');

task('style', () => {
  return src('./assets/scss/main.scss')
    .pipe(sass.sync())
    .pipe(postcss([
      postcssPreserEnv({
        autoprefixer: true,
      }),
      cssnano(),
    ]))
    .pipe(dest('./assets/css'));
});

task('watch:scss', () => {
  watch([
    './assets/scss/**/*.scss',
  ], parallel('style'))
});