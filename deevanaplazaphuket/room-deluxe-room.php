<?php
header("location: http://www.deevanahotels.com/deevanaplazaphuket/deluxe-city-view.php", true, 301);
?>

<?php
$title = 'Deluxe Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room | Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'deluxe room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-deluxe-room.php';
$lang_th = '/th/deevanaplazaphuket/room-deluxe-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-02.jpg" alt="Deluxe Room 02" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>A peaceful island escape for individuals and couples, the deluxe rooms feature flexible space for spending a leisurely afternoon both indoor and outdoor. With a choice of either twin or double bed, the room’s private balcony looks out to the Patong city or the Deevana Plaza Phuket pool view. The mainly white and spacious interior captures imagination with a half‐height glass‐paneled bathroom wall that features the outdoor scenery as an ever‐changing wallpaper pattern.</p>
                            <p>
                                Rooms available: 209<br/>
                                Area: 35 sq.m.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>King size bed (6ft) or twin beds (4ft)</li>
                                        <li>42 inches LCD TV</li>
                                        <li>Living chair</li>
                                        <li>Free WIFI Internet</li>
                                        <li>IDD Telephone with voice mail</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>In‐room amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King size bed (6ft) or twin beds (4ft)</li>
                                            <li>42 inches LCD TV</li>
                                            <li>Living chair</li>
                                            <li>Free WIFI Internet</li>
                                            <li>IDD Telephone with voice mail</li>
                                            <li>City view</li>
                                            <li>Complimentary tea & coffee</li>
                                            <li>Smoke alarm & detector</li>
                                            <li>Sprinkler system</li>
                                            <li>Electronic Safe deposit box</li>
                                            <li>Alarm clock</li>
                                            <li>Reading lamp</li>
                                            <li>Kettle or dripping coffee machine</li>
                                            <li>Universal power outlets</li>
                                            <li>Bathroom fittings</li>
                                            <li>Hair dryer</li>
                                            <li>Bathtub and separate shower stall</li>
                                            <li>Magnifying mirror</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>