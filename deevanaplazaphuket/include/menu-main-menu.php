<ul class="menu list-menu">
    <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">Accommodation</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-city-view'); ?>"><a href="deluxe-city-view.php">Superior Room</a></li>
            <li class="<?php echo get_current_class('deluxe-pool-view'); ?>"><a href="deluxe-pool-view.php">Deluxe Room</a></li>
            <li class="<?php echo get_current_class('premier-room'); ?>"><a href="room-premier-room.php">Premier Waterfront</a></li>
            <li class="<?php echo get_current_class('family-room'); ?>"><a href="room-family-room.php">Family Room</a></li>
            <li class="<?php echo get_current_class('deluxe-suite'); ?>"><a href="room-deluxe-suite.php">Deluxe Suite</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('spa'); ?>"><a href="facilities.php#orientala_wellness_spa">Spa</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">Facilities</a></li>
    <li class="<?php echo get_current_class('meetings'); ?>"><a href="meetings-and-event.php">Meetings &amp; Event</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">Attractions</a></li>
    <li class="<?php echo get_current_class('offers'); ?>"><a href="offers.php">Offers</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">Gallery</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>