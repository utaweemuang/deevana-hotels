<div class="side-panel-bg"></div>
<div class="side-panel">
    <?php include('menu-main-menu.php'); ?>
    <div class="divider"></div>
    <ul class="list-menu">
        <li><a href="/"><i class="fa fa-fw fa-home" aria-hidden="true"></i> DEEVANA HOTELS</a></li>
    </ul>
    <div class="divider"></div>
    <div class="languages">
        <h3>Language</h3>
        <ul>
            <li><a href="<?php echo $lang_en; ?>">English</a></li>
            <li role="separator">/</li>
            <li><a href="<?php echo $lang_th; ?>">Thai</a></li>
            <li role="separator">/</li>
            <li><a href="<?php echo $lang_zh; ?>">Chinese</a></li>
        </ul>
    </div>
</div>