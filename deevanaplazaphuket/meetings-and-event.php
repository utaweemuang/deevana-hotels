<?php
$title = 'Meetings and Event | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'meetings and event, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazaphuket/meetings-and-event.php';
$lang_th = '/th/deevanaplazaphuket/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazaphuket/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/slide-meeting-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-02.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-03.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title" style="">MEETINGS &amp; EVENT</h1>
            </header>

            <div class="section-content container">
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <p style="text-align:justify">Customizable space to accommodate any meeting functions Private or corporate, local or regional, indoor or outdoor, one large Plaza Grand Ballroom or small break‐up rooms, Deevana Plaza Phuket Patong provides tailor‐made solutions for any scale of meetings, seminar, or conferences.</p>
                        <p>For further inquiries, please call <a href="tel:+6676302100">+66 (0)76 302 100</a> or e-mail: <a href="mailto:info@deevanaplazaphuket.com">info@deevanaplazaphuket.com</a></p>
                        <p>
                            <a class="button-default" href="http://www.deevanaplazaphuket.com/download/Meeting-Room-Floor-Plan.pdf" target="_blank">View Floor Plan</a>
                            <a class="button-default" href="http://www.deevanaplazaphuket.com/images/facilities/MEETING-PACKAGE-004.jpg" target="_blank">Meeting &amp; Wedding package</a>
                        </p>
                    </div>
                    <div class="col-12 col-lg-7">
                        <div class="ratio-wide">
                            <div class="ratio-item">
                                <iframe class="video-cover" width="560" height="315" src="https://www.youtube.com/embed/DuuYt3jNKAE?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#plaza_grand_ballroom" class="tab active">Plaza Grand Ballroom</span>
                    <span data-tab="#plaza_i_meeting_room" class="tab">Plaza I Meeting Room</span>
                    <span data-tab="#plaza_ii_meeting_room" class="tab">Plaza II Meeting Room</span>
                    <span data-tab="#square_i_meeting_room" class="tab">Square I Meeting Room</span>
                    <span data-tab="#square_ii_meeting_room" class="tab">Square II Meeting Room</span>
                    <span data-tab="#meeting_room_capacity" class="tab">Meeting Room Capacity</span>
                </div>
                
                <div class="tabs-content">
                    <article id="plaza_grand_ballroom" class="article" data-tab-name="Plaza Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="./uploads/2020/12/Plaza-Grand-Ballroom.jpg" width="1100" height="733" alt="Plaza Grand Ballroom" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Plaza Grand Ballroom</h1>
                                    <p>A theatre‐style ballroom with state‐of‐the‐art audio and visual equipment ensures an uneventful run of any large‐scale conferences or meetings.</p>
                                    
                                    <h2 class="sub-title list-heading">Equipment</h2>
                                    <ul class="custom-list-dashed">
                                        <li>Video projection and equipment</li>
                                        <li>LCD monitors</li>
                                        <li>Hi‐tech lighting system</li>
                                        <li>Audio equipment and amplifiers</li>
                                        <li>2 independent control rooms</li>
                                    </ul>
                                    <p>
                                        Area: 565 sq.m.<br>
                                        Capacity: 500 persons
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="plaza_i_meeting_room" class="article" data-tab-name="Plaza I Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="http://www.deevanaplazaphuket.com/images/event/Plaza-l.jpg" width="1100" height="733" alt="Square I Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Plaza I Meeting Room</h1>
                                    <p><img src="http://www.deevanaplazaphuket.com/images/event/2HY.png" width="128" height="128">
                <img src="http://www.deevanaplazaphuket.com/images/event/Mice-Venue-TCEB.png" width="128" height="128"><br><!-- For small to medium functions or break-out meetings, the two meeting rooms – the Square I and Square II – share a large foyer and a garden that looks out to the all-encompassing views down below. --></p>
                                    <p>Area : 323 sq.m.</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="plaza_ii_meeting_room" class="article" data-tab-name="Plaza II Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="http://www.deevanaplazaphuket.com/images/event/Plaza-ll.jpg" width="1100" height="733" alt="Square I Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Plaza II Meeting Room</h1>
                                    <p><!-- For small to medium functions or break-out meetings, the two meeting rooms – the Square I and Square II – share a large foyer and a garden that looks out to the all-encompassing views down below. --></p>
                                    <p>Area : 242 sq.m.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="square_i_meeting_room" class="article" data-tab-name="Square I Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="./uploads/2020/12/Square-Meeting-Room.jpg" width="1100" height="733" alt="Square I Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Square I Meeting Room</h1>
                                    <p>For small to medium functions or break-out meetings, the two meeting rooms – the Square I and Square II – share a large foyer and a garden that looks out to the all-encompassing views down below.</p>
                                    <p>Area : 81.5 -113 sq.m.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="square_ii_meeting_room" class="article" data-tab-name="Square II Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="./uploads/2020/12/gallery-51.jpg" width="1500" height="936" alt="Square II Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Square II Meeting Room</h1>
                                    <p>For small to medium functions or break-out meetings, the two meeting rooms – the Square I and Square II – share a large foyer and a garden that looks out to the all-encompassing views down below.</p>
                                    <p>Area : 81.5 -113 sq.m.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="meeting_room_capacity" class="article" data-tab-name="Meeting Room Capacity">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">Meeting Room Capacity</h1>
                                    <p>Biz meeting room provides for executive meeting for up to 500 guests and day light also available to make your event success with relax</p>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>Conference rooms</th>
                                                <th>Area (sqm)</th>
                                                <th>Celling Height (m)</th>
                                                <th data-hide="phone,tablet">U-Shape</th>
                                                <th data-hide="phone,tablet">Rectangle</th>
                                                <th data-hide="phone,tablet">Theatre</th>
                                                <th data-hide="phone,tablet">Classroom</th>
                                                <th data-hide="phone,tablet">Banquet</th>
                                                <th data-hide="phone,tablet">Cocktail</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>Plaza Grand Balloon</td>
                                                <td>565</td>
                                                <td>5</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>500</td>
                                                <td>200</td>
                                                <td>300</td>
                                                <td>600</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Plaza I</td>
                                                <td>323</td>
                                                <td>5</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>120</td>
                                                <td>180</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Plaza II</td>
                                                <td>242</td>
                                                <td>5</td>
                                                <td>40</td>
                                                <td>40</td>
                                                <td>200</td>
                                                <td>100</td>
                                                <td>120</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Square I</td>
                                                <td>113</td>
                                                <td>2.4</td>
                                                <td>30</td>
                                                <td>30</td>
                                                <td>50</td>
                                                <td>35</td>
                                                <td>50</td>
                                                <td>60</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Square II</td>
                                                <td>81.5</td>
                                                <td>2.4</td>
                                                <td>20</td>
                                                <td>20</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>40</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                
                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
        
        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .section-title {
        margin-bottom: 15px;
    }
    .excerpt {
        text-align: justify;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .section-header .section-title {
        margin-bottom: 0;
    }
    .section-header .excerpt {
        color: #666;
    }
    .section-content {
        margin-bottom: 60px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    .col-cap .list-heading {
        font-size: 14px;
        margin-top: 1em;
        margin-bottom: 3px;
    }
    .col-cap p.note {
        background-color: #eee;
        padding: 5px 10px;
        border-radius: 2px;
        color: #666;
        font-size: 12px;
        display: inline-block;
    }
    #meeting_room_capacity {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>