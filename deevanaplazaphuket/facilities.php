<?php
$title = 'Facilities | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'facilities, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazaphuket/facilities.php';
$lang_th = '/th/deevanaplazaphuket/facilities.php';
$lang_zh = '/zh/deevanaplazaphuket/facilities.php';

include_once('_header.php');
?>

<main class="site-main">

  <section class="page-cover">
    <div id="contact_slider" class="owl-carousel hero-slider">
      <div class="item"><img src="images/facilities/cover-facilities-01.jpg"
          alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
    </div>

    <div class="custom-hero-slide-nav"></div>
  </section>

  <?php include('include/booking_bar.php'); ?>

  <section class="site-content pattern-fibers">
    <section class="section">
      <header class="section-header">
        <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
      </header>

      <div class="tabs-group">
        <div class="tabs-nav">
          <span data-tab="#restaurant" class="tab active">Restaurant</span>
          <span data-tab="#bar_and_lounge" class="tab">Bar &amp; Lounge</span>
          <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
          <span data-tab="#fitness_center" class="tab">Fitness Center</span>
          <span data-tab="#kids_club" class="tab">Kids Club</span>
          <span data-tab="#swimming_pool" class="tab">Swimming Pool</span>
        </div>

        <div class="tabs-content">
          <article id="restaurant" class="article" data-tab-name="Restaurant">
            <h1 class="title">Restaurant</h1>

            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="./uploads/2020/12/phuket-cafe-600x400.jpg" width="600"
                    height="400" />
                </div>
                <div class="col-w7 col-cap">
                  <h2 class="sub-title">Phuket Cafe (TEMPORARY CLOSED)</h2>
                  <p>A delectable adventure that fuses celestial elements with earthly flavors, the culinary experience
                    at Deevana Plaza Phuket Patong is anything but ordinary. The in-room dining service is open from
                    08:00 hrs. – 21:00 hrs. (last order at 20:30 hrs.)</p>
                  <p>Open Hours: 07:00 hrs. - 10.00 hrs. (Available for breakfast only)</p>
                </div>
              </div>
            </div>
          </article>

          <article id="bar_and_lounge" class="article" data-tab-name="Bar &amp; Lounge">
            <h1 class="title">Bars &amp; Lounge</h1>
            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="./uploads/2020/12/the-lounge-600x400.jpg" width="600" height="400"
                    alt="The Lounge" />
                </div>
                <div class="col-w7 col-cap">
                  <h2 class="sub-title">The Lounge (TEMPORARY CLOSED)</h2>
                  <p>The “celestial home” to the planets and stars, The Lounge by the Lobby brings a refreshing boost to
                    the hotel’s guests with its stylish concentric brand new designed and innovative cocktail list. The
                    open bar stocks premium spirits and champagnes, and features live on‐stage performances.</p>
                  <p>Opening hours: 10.00 hrs. – 19:00 hrs.</p>
                </div>
              </div>

              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="./uploads/2020/12/sunbar-600x400.jpg" width="600" height="400"
                    alt="sun bar" />
                </div>
                <div class="col-w7 col-cap">
                  <h2 class="sub-title">Sun Bar (TEMPORARY CLOSED)</h2>
                  <p>At Sun Bar, luxury is in the choice – quick bites or big bites, cocktails or mocktails, by the pool
                    or in the pool. Or, even if the choice is to lie down on a sun loungers and do absolutely nothing.
                    Set in the vicinity of the main pool, and essentially in the middle of everything that goes on at
                    the Hotel, Sun Bar serves drinks and snacks with an international origin.</p>
                  <p>Opening hours: 10.00‐19:00 hrs.</p>
                </div>
              </div>

              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="./uploads/2020/12/rooftop-terrace-600x400.jpg" width="600"
                    height="400" alt="Rooftop Terrace" />
                </div>
                <div class="col-w7 col-cap">
                  <h2 class="sub-title">Rooftop Terrace (TEMPORARY CLOSED)</h2>
                  <p>At the Rooftop Terrace on the 6th floor, the setting is everything. The bar welcomes couples and friends to catch a glimpse of the sunset or, later, gaze at the stars. It is available for private events.</p>
                </div>
              </div>
            </div>
          </article>

          <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="images/facilities/spa.jpg" />
                </div>
                <div class="col-w7 col-cap">
                  <h1 class="title">Orientala Spa</h1>

                  <p>
                    <strong>Orientala Spa Deevana Plaza Phuket Patong Branch</strong> offers hotel guests and non-guests the opportunity to unwind and be pampered with professional spa treatments. The luxury Phuket spa has a modern Asian inspired design with a spacious and comfortable spa reception to browse the packages and treatments while enjoying a delicious welcome drink.
                  </p>

                  <p>
                    <strong>Deevana Plaza Phuket Patong</strong> is just a few minutes from the beach, a convenient location for anyone staying in Patong. Whether you are seeking a single treatment to refresh and revitalize or a package that takes you on a journey to rejuvenation, we have it covered. The Patong romantic spa suites all include a Jacuzzi tub and steam facility and soothing views of the resort gardens and pool.
                  </p>

                  <h2 class="sub-title">Orientala Spa Facilites</h2>
                  <ul>
                    <li>5 Treatment rooms (3 single and 2 twin rooms with Jacuzzi)</li>
                    <li>Ladies and Gents private lockers and steamed bath</li>
                    <!-- <li>Facial treatment and shop</li>
                    <li>2 Facial treatment stations</li> -->
                    <li>Orientala Wellness Spa Menu (<a href="http://www.deevanaplazaphuket.com/download/Spa-Menu.pdf" target="_blank">Download</a>)</li>
                  </ul>

                  <p><a href="mailto:info.dpp@deevanahotels.com" target="_blank">For more details click here</a></p>
                </div>
              </div>
            </div>
          </article>

          <article id="fitness_center" class="article" data-tab-name="Fitness Center">
            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                </div>
                <div class="col-w7 col-cap">
                  <h1 class="title">Fitness Center (TEMPORARY CLOSED)</h1>
                  <p>Located on the hotel’s third floor, this modern, fully equipped gym looks out to the amazing view of Patong City.</p>

                  <h2 class="sub-title">Equipment</h2>
                  <ul>
                    <li>2 Treadmills</li>
                    <li>1 Elliptical Trainer</li>
                    <li>1 Adjustable AB / Recline Bench – 1 Seated Leg Extension / Curl</li>
                    <li>1 Double-tier Dumbbell Rack</li>
                    <li>1 Multi Press</li>
                    <li>1 Multi Lat</li>
                    <li>1 Seated Leg Extension / Curl</li>
                    <li>Opening hours: 6:00 hrs. – 19:00 hrs.</li>
                    <li>Towels and lockers are provided.</li>
                    <li>Children below 16 years old are not allowed.</li>
                  </ul>
                </div>
              </div>
            </div>
          </article>

          <article id="kids_club" class="article" data-tab-name="Kids Club">
            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="images/facilities/kids-club.jpg" />
                </div>
                <div class="col-w7 col-cap">
                  <h1 class="title">Kids Club (TEMPORARY CLOSED)</h1>
                  <p>A world of fun and excitement awaits young guests at Kids Club, where unleashed creativity and imagination can run wild. Located close to the lobby level, the Kids Club allows direct access to children’s pool. “Kids Club at Deevana Plaza Phuket Patong welcomes children from 4 to 12 years old and Kids are remaining under parents’ responsibilities”</p>

                  <h2 class="sub-title">Facilities</h2>
                  <ul>
                    <li>LCD TV</li>
                    <li>Video game</li>
                    <li>Football table</li>
                    <li>Activity table</li>
                  </ul>
                </div>
              </div>
            </div>
          </article>

          <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
            <div class="container">
              <div class="row row-content-tab">
                <div class="col-w5 col-pic">
                  <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                </div>
                <div class="col-w7 col-cap">
                  <h1 class="title">Swimming Pool (TEMPORARY CLOSED)</h1>
                  <p>Opening hours: 07:00 hrs. – 19:00 hrs.</p>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    </section>
  </section>

</main>

<script>
$(function() {
  var $tnav = $('.tabs-nav');
  var $tcon = $('.tabs-content');
  var $tact = $tnav.find('.tab.active');
  var data = $tact.data('tab');
  var indx = $tact.index();
  var hash = window.location.hash;

  function preferPos(pos) {
    scrollTo(0, pos);
  }

  if (hash) {
    $tcon.find(hash).show();
    $tnav.find('[data-tab="' + hash + '"]').addClass('active').siblings().removeClass('active');

    var pos = $('.booking-bar').offset().top;
    preferPos(pos);
  } else {
    $tcon.find(data).show();
  }

  $tnav.on('click', '[data-tab]', function(e) {
    e.preventDefault();
    var $this = $(this);
    var i = $this.index();
    var t = $this.data('tab');
    $this.addClass('active').siblings().removeClass('active');
    $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
    $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
  });

  $tcon.find('article').each(function() {
    var $this = $(this);
    var tabName = $this.data('tab-name');
    $this.before('<span class="accordion-tab">' + tabName + '</span>');
    $this.prev('.accordion-tab').on('click', function() {
      var i = $(this).index('.accordion-tab');
      $(this).addClass('active').siblings().removeClass('active');
      $this.slideDown(300, function() {
        var pos = $(this).offset().top;
        var offset = 50;
        $('html, body').animate({
          scrollTop: pos - offset,
        }, 800);
      }).siblings().not('.accordion-tab').slideUp(300);
      $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
    });
  });

  $('.accordion-tab').eq(indx).addClass('active');
});
</script>

<style>
.section-header {
  text-align: center;
  padding: 50px 0;
  color: #24467b;
}

.tabs-content .article,
.tabs-content .accordion-tab {
  display: none;
}

.tabs-nav {
  text-align: center;
}

.tabs-nav .tab {
  display: inline-block;
  padding: 0 16px;
  background-color: #c3c3c3;
  line-height: 36px;
  border-radius: 4px 4px 0 0;
  color: #fff;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.tabs-nav .tab.active {
  color: #1a355e;
  background-color: #fff;
}

.tabs-content {
  background-color: #fff;
  padding-top: 60px;
  padding-bottom: 40px;
}

.tabs-content .container {
  max-width: 1200px;
}

.article>.title {
  text-align: center;
  color: #78a321;
  margin-bottom: 40px;
}

.row-content-tab {
  margin-left: -15px;
  margin-right: -15px;
}

.row-content-tab:not(:last-child) {
  margin-bottom: 30px;
}

.row-content-tab>[class*="col-"] {
  padding-left: 15px;
  padding-right: 15px;
}

.col-pic .thumbnail {
  border-radius: 12px;
  border: 6px solid #fff;
  -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, .3);
  box-shadow: 0 0 2px rgba(0, 0, 0, .3);
}

.col-cap .title {
  color: #78a321;
}

.col-cap .sub-title {
  color: #236198;
  font-size: 18px;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  text-transform: uppercase;
  margin-top: 3px;
  margin-bottom: 10px;
}

.col-cap p {
  margin-top: 3px;
}

.col-cap .list-heading {
  font-size: 14px;
  margin-top: 1em;
  margin-bottom: 3px;
}

.col-cap .list-heading+ul {
  margin-top: 0;
}

.col-cap p.note {
  background-color: #eee;
  padding: 5px 10px;
  border-radius: 2px;
  color: #666;
  font-size: 12px;
  display: inline-block;
}

@media (max-width: 740px) {
  .tabs-nav {
    display: none;
  }

  .tabs-content {
    padding: 0;
  }

  .tabs-content .accordion-tab {
    position: relative;
    display: block;
    background-color: #eee;
    padding: 5px 15px;
    border-bottom: 1px solid #ccc;
  }

  .tabs-content .accordion-tab:after {
    content: '\f055';
    font-family: 'FontAwesome';
    float: right;
  }

  .tabs-content .accordion-tab.active:after {
    content: '\f056';
  }

  .tabs-content .row {
    padding-top: 20px;
    padding-bottom: 20px;
  }

  .col-pic,
  .col-cap {
    width: 100%;
  }

  .col-pic {
    margin-bottom: 20px;
  }
}
</style>

<?php include_once('_footer.php'); ?>