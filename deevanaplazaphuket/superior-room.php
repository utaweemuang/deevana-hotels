<?php
//
// Room information
// 
$room_title     = "Superior Room";
$room_desc      = "<p>A peaceful island escape for individuals and couples, the deluxe rooms feature 
flexible space for spending a leisurely afternoon both indoor and outdoor. With a choice of either twin or double bed, 
the room’s private balcony looks out to the Patong city. The mainly white and spacious interior captures imagination 
with a half-height glass-paneled bathroom wall that features the outdoor scenery as an ever-changing wallpaper pattern.</p>
<p><strong>Our Superior Room room is Non Smoking room</strong></p>";
$room_available = "209";
$room_space     = "35 sq.m.";
$room_beds      = "King size or Twin bed";
$room_amenities = array(
  "King size bed (6ft) or twin beds (4ft)",
  "42 inches LCD TV",
  "Living chair",
  "Free WIFI Internet",
  "IDD Telephone with voice mail",
  "City view",
  "Complimentary tea & coffee",
  "Smoke alarm & detector",
  "Sprinkler system",
  "Electronic Safe deposit box",
  "Alarm clock",
  "Reading lamp",
  "Kettle or dripping coffee machine",
  "Universal power outlets",
  "Bathroom fittings",
  "Hair dryer",
  "Bathtub and separate shower stall",
  "Magnifying mirror",
);
$room_featured = array("./uploads/2021/01/deluxe-city-view-02-600x400.jpg", 600, 400, $room_title);
$room_gallery_large = array(
  array("./uploads/2021/01/deluxe-city-view-01.jpg", 1500, 1000, "{$room_title} 1" ),
  array("./uploads/2021/01/deluxe-city-view-02.jpg", 1500, 984, "{$room_title} 2" ),
);
$room_gallery_thumb = array(
  array("./uploads/2021/01/deluxe-city-view-01-600x400.jpg", 600, 400, "{$room_title} 1" ),
  array("./uploads/2021/01/deluxe-city-view-02-600x400.jpg", 600, 400, "{$room_title} 2" ),
);
// 
// End room information
// 
?>

<?php
$title = "{$room_title} | Deevana Plaza Phuket | Official Hotel Group Website Thailand";
$desc = "{$room_title} | Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street";
$keyw = "{strtolower($room_title}, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach";

$html_class = '';
$body_class = 'room room-deluxe-city-view';
$cur_page = 'deluxe-city-view';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/deluxe-city-view.php';
$lang_th = '/th/deevanaplazaphuket/deluxe-city-view.php';
$lang_zh = '/zh/deevanaplazaphuket/deluxe-city-view.php';

include_once('_header.php');
?>

<main class="site-main">
  <div class="room-content">

    <div class="room-slides-wrap disable-touch">
      <div class="room-slides">
        <div class="slides-container">
          <?php foreach( $room_gallery_large as $image ) : ?>
          <img src="<?= $image[0] ?>" width="<?= $image[1] ?>" height="<?= $image[2] ?>" alt="<?= $image[3] ?>" />
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <div class="room-slides-thumbs">
      <h2 class="title"><?= $room_title ?> <span><?= $room_beds ?></span></h2>
      <div class="thumbs">
        <ul>
          <?php foreach( $room_gallery_large as $index => $image ) : ?>
          <li class="<?php if ($index===0) {echo "current";} ?>"><img src="<?= $image[0] ?>" width="<?= $image[1] ?>" height="<?= $image[2] ?>" alt="<?= $image[3] ?>" /></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
    </div>

    <div class="room-info">
      <div class="table">
        <div class="content table-cell">
          <h1 class="title"><?= $room_title ?></h1>

          <div class="row">
            <div class="col-w3 col-thumbnail">
              <?php echo sprintf('<img class="room-preview force" src="%s" width="%s" height="%s" alt="%s" />',
                  $room_featured[0],
                  $room_featured[1],
                  $room_featured[2],
                  $room_featured[3]
                ); ?>

              <a class="button clickable book-this-room-button desktop"
                href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
            </div>

            <div class="col-w5 col-info">
              <?= $room_desc ?>
              <p>
                Area: <?= $room_space ?>
              </p>
              <a class="button clickable book-this-room-button mobile"
                href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
            </div>

            <div class="col-w4 col-amenities">
              <div class="amenities">
                <div class="shad-over">
                  <h2 class="title">Room Amenities</h2>
                  <ul class="amenities-list">
                    <?php
                    for($i = 0; $i < 5; $i++) {
                      echo "<li>{$room_amenities[$i]}</li>";
                    }
                    ?>
                    <li class="more clickable">
                      <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                    </li>
                  </ul>

                  <div id="all_amenities" class="mfp-hide popup">
                    <h2>In‐room amenities</h2>
                    <ul class="list-columns-2">
                      <?php
                      foreach( $room_amenities as $item ) {
                        echo "<li>{$item}</li>";
                      }
                      ?>
                    </ul>
                  </div>
                </div>

                <span class="shad-left"></span>
                <span class="shad-right"></span>
              </div>
            </div>
          </div>
        </div>

        <div class="booking table-cell">
          <?php include('include/room-booking-form.php'); ?>
        </div>
      </div>

      <span id="hide_content" class="close clickable">&times; Hide content</span>
    </div>

  </div>
</main>

<?php include_once('_footer.php'); ?>