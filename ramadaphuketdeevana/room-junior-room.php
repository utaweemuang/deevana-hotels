<?php
$title = 'Junior Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Junior Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'junior room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-room';
$cur_page = 'junior-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-junior-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-junior-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-junior-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/junior-suite/1500/junior-suite-01.jpg" alt="Junior Room 01" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-02.jpg" alt="Junior Room 02" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-03.jpg" alt="Junior Room 03" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-04.jpg" alt="Junior Room 04" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-05.jpg" alt="Junior Room 05" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-06.jpg" alt="Junior Room 06" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Junior Suite <span>King size bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior-suite/600/junior-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-06.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Junior Suite</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior-suite/600/junior-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Pamper yourself in the plush 56 sq. metre Junior Suite featuring a spacious bedroom and living area for a home away from home experience. The Suite is bright and modern with floor-to-ceiling windows that open onto the spacious private balcony.</p>
                            <p>The lounge includes a comfortable sofa and extra-large 47″ LED TV and satellite channels for entertainment. The bedroom has a 6ft king-size bed, plenty of built-in storage for personal items and a highlight of the suite is the daybed and bathtub on the balcony adding romance to your stay.</p>
                            <p>The Junior Suite has extra features for guests’ pleasure including a full complimentary mini-bar replenished daily, a coffee machine, large refrigerator, soft bathrobe and slippers and additional bath products for a relaxing soak in the tub.</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>King bedding</li>
                                        <li>4 Pillow (soft/firm)</li>
                                        <li>Private balcony with Bathtub</li>
                                        <li>47″ LED TV</li>
                                        <li>Satellite Channel</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King bedding</li>
                                            <li>4 Pillow (soft/firm)</li>
                                            <li>Private balcony with Bathtub</li>
                                            <li>47″ LED TV</li>
                                            <li>Satellite Channel</li>
                                            <li>LED Reading Lamp</li>
                                            <li>Electronic Safety box</li>
                                            <li>Alarm Clock</li>
                                            <li>Bathroom with rain shower</li>
                                            <li>Flash light</li>
                                            <li>Hair Dryer</li>
                                            <li>Refrigerator</li>
                                            <li>Make up/Shaving mirror</li>
                                            <li>Wall mounted iron and board</li>
                                            <li>Writing desk</li>
                                            <li>Beach bag</li>
                                            <li>Bathrobe and disposable slippers</li>
                                            <li>Umbrellas</li>
                                            <li>Weight Scale</li>
                                            <li>Free Wi‐Fi high speed Internet</li>
                                            <li>Free Full set of Bath Products</li>
                                            <li>Free 4 bottles of drinking water daily</li>
                                            <li>Free set of mini bar drinks and snacks, replenished daily</li>
                                            <li>Free Tea and facilities, replenished daily</li>
                                            <li>Free Fresh coffee</li>
                                            <li>Espresso Machine</li>
                                            <li>Turn down service</li>
                                            <li>Luggage stand</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>