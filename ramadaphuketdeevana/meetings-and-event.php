<?php
$title = 'Meetings and Event | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'meetings and event, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/ramadaphuketdeevana/meetings-and-event.php';
$lang_th = '/th/ramadaphuketdeevana/meetings-and-event.php';
$lang_zh = '/zh/ramadaphuketdeevana/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            
            <header class="section-header">
                <div class="container">
                    <h1 class="section-title">MEETINGS &amp; EVENT</h1>
                    <p class="description">The stylish meeting room at Ramada by Wyndham Phuket Deevana Patong is ideal for corporate events, business meetings, banquets, seminars, parties and more. The meeting room has a capacity of 90 people seated banquet style and 63 in classroom layout, and can be divided for smaller events such as board meetings. State-of-the-art audio visual equipment is included and there are several options for refreshments and catering services.</p>
                </div>
            </header>
            
            <article class="article">
                <div class="container">
                    <div class="row">
                        <div class="col-w5 col-pic">
                            <img class="force thumbnail" src="images/meetings/meeting_room.jpg" alt="Meeting Room" />
                        </div>
                        
                        <div class="col-w7 col-cap">
                            <h1 class="section-title" style="font-size:1.25rem;">MEETING PACKAGE</h1>
                            <p><b>Full Day Meeting Package</b> : THB 1,700.- per person per day</p>
                            <h1 class="title">Rate are inclusive of : </h1>
                            <ul>
                                <li>1 Lunch (Buffet Lunch will be provide on a minimum of 30 persons)</li>
                                <li>2 Coffee Breaks with Fruit Juice and Snack</li>
                                <li>1 Main Meeting room and equipment: 1 LCD projector, screen,</li>
                                <li>2 Flipchart, 1 white board, notepad, pen, drinking water mints…etc. </li>
                                <li>1 Welcome Banner at the entrance</li>
                            </ul>
                            <p><b>Half day Meeting Package</b> : THB 1,300.- per person per day</p>
                            <h1 class="title">Rate are inclusive of : </h1>
                            <ul>
                                <li>1 Lunch (Buffet lunch will be provide on a minimum of 30 persons)</li>
                                <li>1 Coffee Break with Fruit Juice and Snack</li>
                                <li>1 Main meeting room and equipment: 1 LCD projector, screen </li>
                                <li>2 Flipchart, 1 white board, notepad, pen, drinking water and mints…etc. </li>
                                <li>1 Welcome Banner at the entrance</li>
                            </ul>
                            <h1 class="section-title" style="font-size:1.25rem;">MEETING ROOM RENTAL </h1>
                            <p style="margin-bottom:0;">Room Rental Half Day (08.00-12.00hrs. Or 13.00-17.00hrs) 1 Room THB 5,000 </p>
                            <p>Room Rental Full Day (08.00-12.00hrs. & 13.00-17.00hs.)   1 Room THB 8,500 </p>
                            <h1 class="title">Room rental are included :</h1>
                            <ul>
                                <li>Drinking Water 2. Audio visual plus 1 wiles microphone</li>
                                <li>Podium 1 piece</li>
                                <li>Stage set up (120x240) (maximum 2 pieces)</li>
                            </ul>
                            <h1 class="title">Other Facilities :</h1>
                            <ul>
                                <li>Free Wi-Fi – Accessible in all areas of the resort</li>
                                <li>Parking space – secure indoor spaces for 55 vehicles</li>
                            </ul>
                            <!-- <h1 class="title">Other Facilities</h1>
                            <ul>
                                <li>Internet corner – free service available 24 hours located next to the lobby</li>
                                <li>Free Wi-Fi – Accessible in all areas of the resort</li>
                                <li>Laundry service – fast reliable service for an extra fees</li>
                                <li>Parking space – secure indoor spaces for 55 vehicles</li>
                                <li>Security – 24 hour security guards and CCTV cameras in all areas</li>
                            </ul> -->
                        </div>

                        <div class="col-w12" style="padding:5px 0;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/b-shape.jpg" alt="" width="1050" height="713"></div>
                        <div class="col-w12" style="padding:5px 0;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/u-shape.jpg" alt="" width="1050" height="713"></div>
                    </div>
                </div>
            </article>
            
        </section>
    </section>
        
</main>

<style>
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #222;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
	.col-cap .sub-title {
		font-size: 14px;
		margin-top: 1em;
		margin-bottom: 3px;
	}
    @media (max-width: 480px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>