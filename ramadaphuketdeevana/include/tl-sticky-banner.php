<style>
/* Sticky Banner */
@-webkit-keyframes buttonBlinking {
        0% {
          -webkit-filter: brightness(1);
          filter: brightness(1);
        }
        50% {
          -webkit-filter: brightness(1.5);
          filter: brightness(1.5);
        }
    }
    @keyframes buttonBlinking {
        0% {
          -webkit-filter: brightness(1);
          filter: brightness(1);
        }
        50% {
          -webkit-filter: brightness(1.5);
          filter: brightness(1.5);
        }
    }

    .tl-sticky-banner-toggle {
        display: none;
    }

    @media (min-width: 320px) {
        .tl-sticky-banner-toggle {
          position: fixed;
          z-index: 20;
          bottom: 20px;
          left: 15px;
          display: block;
          width: 48px;
          height: auto;
          font-size: 24px;
          line-height: 48px;
          color: black;
          border: 0;
          border-radius: 50%;
          background-color: gold;
          -webkit-box-shadow: 0 0 1rem rgba(0, 0, 0, 0.5);
          box-shadow: 0 0 1rem rgba(0, 0, 0, 0.5);
          -webkit-animation: buttonBlinking 2s infinite linear;
          animation: buttonBlinking 2s infinite linear;
          cursor: pointer;
          opacity: 1;
          visibility: visible;
          -webkit-transition: 350ms;
          -o-transition: 350ms;
          transition: 350ms;
        }
        .tl-sticky-banner-toggle:before, .tl-sticky-banner-toggle:after {
          opacity: 0;
          visibility: hidden;
          -webkit-transition: 350ms;
          -o-transition: 350ms;
          transition: 350ms;
        }
        .tl-sticky-banner-toggle:before {
          content: 'Deals';
          position: absolute;
          top: 50%;
          left: calc(100% + 12px);
          height: 26px;
          margin-top: -13px;
          padding: 0 8px;
          background-color: #222;
          color: gold;
          font-size: 14px;
          line-height: 26px;
          border-radius: 0.25rem;
        }
        .tl-sticky-banner-toggle:after {
          content: '';
          position: absolute;
          top: 50%;
          left: calc(100% + 0px);
          width: 0;
          height: 0;
          margin-top: -6px;
          border-width: 6px 6px 6px 6px;
          border-style: solid;
          border-color: transparent #222 transparent transparent;
        }
        .tl-sticky-banner-toggle:hover:before, .tl-sticky-banner-toggle:hover:after {
          opacity: 1;
          visibility: visible;
        }
        .tl-sticky-banner-toggle.ready.show {
          opacity: 0;
          visibility: hidden;
          -webkit-transform: translate(70px, 0);
          -ms-transform: translate(70px, 0);
          transform: translate(70px, 0);
          -webkit-transform: translate3d(70px, 0, 0);
          transform: translate3d(70px, 0, 0);
        }
    }

    .tl-sticky-banner {
        display: none;
    }

    @media (min-width: 320px) {
        .tl-sticky-banner {
          position: fixed;
          left: 1rem;
          bottom: 1rem;
          display: block;
          z-index: 20;
          width: 320px;
          height: auto;
          max-width: calc(100% - 2rem);
          /* margin-left: 1rem; */
          padding: 8px;
          background-color: rgba(255, 255, 255, 0.85);
          -webkit-box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
          box-shadow: 0 1px 10px rgba(0, 0, 0, 0.2);
          opacity: 0;
          visibility: hidden;
          -webkit-transform: translate(10px, 0);
          -ms-transform: translate(10px, 0);
          transform: translate(10px, 0);
          -webkit-transform: translate3d(10px, 0, 0);
          transform: translate3d(10px, 0, 0);
          -webkit-transition: 350ms;
          -o-transition: 350ms;
          transition: 350ms;
        }
    }

    @media (min-width: 1200px) and (max-width: 767px) {
        .tl-sticky-banner {
          bottom: calc(1rem + 46px);
        }
    }

    @media (min-width: 320px) {
        .tl-sticky-banner.ready.show {
          opacity: 1;
          visibility: visible;
          -webkit-transform: translate(0, 0);
          -ms-transform: translate(0, 0);
          transform: translate(0, 0);
          -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
        }
        .tl-sticky-banner .content {
          position: relative;
        }
        .tl-sticky-banner .content-close {
          position: absolute;
          z-index: 10;
          top: 0;
          right: 0;
          background-color: rgba(255, 255, 255, 0.85);
          border: 0;
          cursor: pointer;
        }
        .tl-sticky-banner .item {
          display: block;
        }
        .tl-sticky-banner .owl-nav {
          top: 30px;
          left: 10px;
          right: auto;
          bottom: auto;
          opacity: 0.4;
          position: absolute;
        }
        .tl-sticky-banner .owl-nav:hover {
          opacity: 1;
        }
        .tl-sticky-banner .owl-nav .owl-next, .tl-sticky-banner .owl-nav .owl-prev {
          background-color: rgba(0, 0, 0, 0.6);
          color: #fff;
          display: inline-block;
          width: 32px;
          height: 32px;
          line-height: 32px;
          text-align: center;
        }
        .tl-sticky-banner .owl-nav .owl-next {
          margin-left: 3px;
        }
    }
</style>

<div class="tl-sticky-banner">
    <div class="content">
        <div class="owl-carousel sticky-banner-carousel">
            <a class="item" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=4&pid=MDg0NDE1" target="_blank"><img class="img-cover" src="http://www.ramadaphuketdeevana.com/images/promotion/Super-Sales-AUS.jpg" width="450" height="300" alt=""></a>
            <a class="item" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=5&pid=MDgyMDg3" target="_blank"><img class="img-cover" src="http://www.ramadaphuketdeevana.com/images/promotion/offer-1.jpg" width="450" height="300" alt=""></a>
            <a class="item" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=5&pid=MDcxODM5MXwwNzE4Mzky" target="_blank"><img class="img-cover" src="http://www.ramadaphuketdeevana.com/images/promotion/Happy-Holiday-on-Nov002.jpg" width="450" height="300" alt=""></a>
            <a class="item" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=5&pid=MDcyMzUzOQ%3D%3D" target="_blank"><img class="img-cover" src="http://www.ramadaphuketdeevana.com/images/promotion/Basic-Deal-002.jpg" width="450" height="300" alt=""></a>
        </div>
        <button class="content-close"><i class="fa fa-times" aria-hidden="true"></i> close</button>
    </div>
</div>
<button class="tl-sticky-banner-toggle"><i class="fa fa-gift" aria-hidden="true"></i></button>
<div class="custom-hero-slide-nav"></div>