<?php
$title = 'Promotion | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'promotion: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'promotion, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$body_class = 'promotion';
$cur_page = 'promotion';

$lang_en = '/ramadaphuketdeevana/promotion.php';
$lang_th = '/th/ramadaphuketdeevana/promotion.php';
$lang_zh = '/zh/ramadaphuketdeevana/promotion.php';

include_once('_header.php');
?>
        
<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section">
			
				<article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url( get_info('ibeID'), 'en', 'MDY2ODE%3D' ); ?>" target="_blank">
                                    <img class="force" src="images/promotion/room_only.jpg" alt="Room Only" />
                                </a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">Room Only</h1>
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>In-room internet</li>
                                <li>Ramada Signature Drink at Bake &amp; Bev</li>
                                <li>Stay 1 night get 20% discount on Food and Beverage at hotel restaurant except alcohol</li>
                                <li>Stay More Get More Benefits!</li>
                                <li>Free One child under 12 years stays free of charge when using existing beds</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes included<br>
                                    <span class="cost"><span class="discount">THB 1,900</span> per night</span>
                                </h3>
                                <ul class="list-description">
                                    <li><span class="label">Room type:</span> Deluxe Room with Balcony</li>
                                    <li><span class="label">Period:</span> Now - 18 May 2017</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url( get_info('ibeID'), 'en', 'MDY2ODE%3D' ); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url( get_info('ibeID'), 'en', 'MDc3NzAw' ); ?>" target="_blank">
                                    <img class="force" src="images/promotion/special_shoulder_season.jpg" alt="Special Shoulder Season" />
                                </a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">Special Shoulder Season</h1>
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>1 night in Deluxe Room with Balcony</li>
                                <li>Daily breakfast</li>
                                <li>Late check-out by 14.00</li>
                                <li>Ramada Signature Drink at Bake &amp; Bev</li>
                                <li>Stay longer receive More Benefits!</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes included<br>
                                    <span class="cost"><span class="discount">THB 2,448</span> per night</span><br>
                                    <span>32% Discount</span>
                                </h3>
                                <ul class="list-description">
                                    <li><span class="label">Room type:</span> Deluxe Room with Balcony</li>
                                    <li><span class="label">Period:</span> 01 Jul - 31 Oct 2016</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url( get_info('ibeID'), 'en', 'MDc3NzAw' ); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>

            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<style>
    .site-main {
        background-image: url(images/promotion/bg-promotion.jpg);
        background-attachment: fixed;
        background-size: cover;
    }
    .article {
        background-color: #e1e1e1;
        margin-bottom: 30px;
        padding: 20px;
        color: #666;
        min-height: 190px;
    }
    .article .title {
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        font-weight: 700;
        color: #adc32b;
        margin-bottom: 15px;
    }
    .article .list-heading {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .article .list-heading + ul {
        font-size: 12px;
        margin-top: 3px;
    }
    .article .booking {
        background-color: #666;
        color: #cbcbcb;
        border-radius: 6px;
        padding: 12px;
        font-size: 12px;
    }
    .article .booking:after {
        content: '';
        display: block;
        clear: both;
    }
    .article .booking h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 12px;
    }
    .article .booking .discount {
        font-size: 22px;
        font-weight: 700;
        color: #bed62f;
    }
    .article .booking ul {
        list-style: none;
        margin-top: 5px;
        padding-left: 0;
    }
    .article .booking li {
        border-bottom: 1px dotted #cbcbcb;
        padding: 5px 0;
    }
    .article .booking .button {
        background-color: #8fc31c;
        background-image: linear-gradient(to bottom, #c0f844,#8fc31c);
        color: #fff;
        padding: 0 10px;
        line-height: 2;
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 14px;
        font-weight: 700;
    }
    @media (max-width: 720px) {
        .col-thumbnail,
        .col-info {
            width: 50%;
        }
        .col-booking {
            width: 100%;
        }
        .article .booking {
            margin-top: 20px;
            border-radius: 0;
        }
    }
    @media (max-width: 480px) {
        .col-thumbnail,
        .col-info {
            width: 100%;
        }
        .col-thumbnail {
            margin-bottom: 20px;
        }
        .col-booking .booking {
            border-radius: 0;
        }
        .article {
            padding: 12px;
        }
        .article .title {
            font-size: 24px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>