<?php
$title = 'F&amp;B Promotion | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = '';
$keyw = '';

$html_class = '';
$body_class = 'fb-promotion';
$cur_page = 'fb-promotion';

$lang_en = '/ramadaphuketdeevana/cnn-promotion.php';
$lang_th = '/th/ramadaphuketdeevana/cnn-promotion.php';
$lang_zh = '/zh/ramadaphuketdeevana/cnn-promotion.php';

include_once('_header.php');
?>

<main class="site-main no-head pattern-fibers">
    <section class="site-content">
       
        <header class="section-header">
            <div class="container">
                <h1 class="section-title">Chinese new year dinner Promotion</h1>
            </div>
        </header>
        
        <article class="article">
            <div class="container">
                <div class="row">
                    <div class="col-w12">
                    	<div id="fb_promotion">
                    		<img src="./images/home/promo/CNN-menu.jpg" alt="Chinese new year Menu" width="1000" height="1415" />
						</div>
					</div>
                </div>
            </div>
        </article>
        
    </section>
       
	<?php include('include/booking_bar.php'); ?>
        
</main>

<style>
	.site-main.no-head {
		padding-top: 130px;
	}
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
	.masonry-items {
		margin: 0 -5px;
	}
	.masonry-items .item {
		padding: 5px;
		width: 33.3333%;
	}
	.masonry-items .item img {
		opacity: 1;
		-wekbit-transition: 200ms;
		transition: 200ms;
	}
	.masonry-items .item a:hover img {
		opacity: 0.85;
	}
	@media(max-width: 1070px) {
		.site-main.no-head {
			padding-top: inherit;
		}
	}
	@media(max-width: 768px) {
		.masonry-items .item { width: 50%; }
	}
	@media(max-width: 480px) {
		.masonry-items .item { width: 100%; }
	}
</style>

<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
	var mi = $('.masonry-items');
	mi.isotope();
	mi.imagesLoaded().progress(function() {
		mi.isotope('layout');
	});
</script>

<?php include_once('_footer.php'); ?>