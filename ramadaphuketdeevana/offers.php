<?php
$title = 'Special Offers | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Special Offers njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'Special Offers, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'offers';
$cur_page = 'offers';

$lang_en = '/ramadaphuketdeevana/offers.php';
$lang_th = '/th/ramadaphuketdeevana/offers.php';
$lang_zh = '';

include_once '_header.php';
?>
<style>
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
</style>

<style>
    :root {
    	--accent-color: hsl(345, 100%, 38%);
    	--accent-color-darker: hsl(345, 100%, 33%);
    }
    .offer-item {
    	background-color: #fff;
    	border-radius: .25rem;
    	overflow: hidden;
    	border: 1px solid #ddd;
        padding: 20px 25px;
        margin-bottom: 30px;
    }
    .offer-item figure {
        margin: -20px -25px 20px;
    }
    .offer-item p,
    .offer-item ul {
        margin: 0 0 1rem;
    }
    .offer-item .heading {
        font-size: 1.5rem;
        font-weight: 700;
        margin: 0 0 .25rem;
        color: var(--accent-color);
    }
    .offer-item .description {
    	font-size: 0.875rem;
    	font-weight: 300;
    }
    .offer-item a {
    	color: var(--accent-color);
    }
    .offer-item a:hover,
    .offer-item a:active {
    	color: var(--accent-color-darker);
    }
    .offer-item .btn-cta {
    	background-color: var(--accent-color);
    	color: #fff;
    	transition: .2s ease-in-out;
    	display: block;
    	padding: 0.375rem 0.75rem 0.425rem;
    	text-decoration: none;
    	text-align: center;
    	text-transform: uppercase;
    	border-radius: 0.25rem;
    }
    .offer-item .btn-cta:hover,
    .offer-item .btn-cta:active {
    	background-color: var(--accent-color-darker);
    	color: #fff;
    }
</style>

<main class="site-main">
    <?php include 'include/booking_bar.php'; ?>

    <section class="site-content pattern-fibers">
        <header class="section-header">
        	<div class="container">
        		<h1 class="section-title">Special Offers</h1>
        		<p class="description"></p>
        	</div>
        </header>
        <article class="article">
        	<div class="container">


        		<div class="row" id="offer_query">
                    <div class="text-center">Loading...</div>
                </div>


        	</div>
        </article>
    </section>
</main>

<script src="https://cdn.jsdelivr.net/npm/template7@1.4.2/dist/template7.min.js" integrity="sha256-AR7mrm2lYY5i4wWxxAznbMCTVOCMD5kvqLUmtW/6iCY=" crossorigin="anonymous"></script>
<script type="text/template7" id="offer_template">
    {{#each posts}}
    	<div class="col-12 col-md-6 col-xl-4">
    		<div class="offer-item">
    			{{#if image}}
    				<figure>
                        {{img className="w-100 h-auto d-block" image}}
    				</figure>
    			{{/if}}
    			<div class="content">
    				<h2 class="heading main-color h4 mb-2 font-weight-bold">{{title}}</h2>
    				<div class="description">
    					{{content}}
    				</div>
    				{{#if button.enable}}
    					<div>
    						<a class="btn-cta" href="{{button.link_url}}" target="{{button.link_target}}">{{button.link_text}}</a>
    					</div>
    				{{/if}}
    			</div>
    		</div>
    	</div>
    {{/each}}
</script>
<script>
    (function($) {
    	var origin = 'https://webdemo2.travelanium.net/deevana/cockpit';
        var token = '3fa7cf86aa69b4028f4d0adbe6c735';
        var collection = 'ramadaPhuketDeevanaOffers';
        var path = '%base%/api/collections/get/%collection%?token=%token%&rspc=1'
            .replace('%base%',origin)
            .replace('%collection%',collection)
            .replace('%token%',token);
    	var data = $.getJSON(path);
        Template7.registerHelper('img', function(image, options) {
            var imgApi = origin + '/api/cockpit/image';
            var imgPath = origin + '/storage/uploads' + image.path;
            var url = '%base%?token=%token%&src=%path%&rspc=1&w=600&m=bestFit&q=82&o=true'
                .replace('%base%', imgApi)
                .replace('%token%', token)
                .replace('%path%', imgPath);
            return '<img class="%className%" src="%src%" loading="lazy" />'
                .replace('%className%', options.hash.className)
                .replace('%src%', url);
        });
    	data.then(function(res) {
    		var template = $('#offer_template').html();
    		var items = res.entries.filter(function(item) {
    			return item.published
    		});
    		if (items.length) {
    			var compiled = Template7.compile(template);
    			var html = compiled({
    				posts: items,
    			});
    			$('#offer_query').html(html);
    		}
    	});
    })(jQuery);
</script>

<?php include_once '_footer.php'; ?>