<?php
$title = 'Facilities | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'facilities: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'facilities, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/ramadaphuketdeevana/facilities.php';
$lang_th = '/th/ramadaphuketdeevana/facilities.php';
$lang_zh = '/zh/ramadaphuketdeevana/facilities.php';

include_once('_header.php');
?>

<main class="site-main">

    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>

        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include('include/booking_bar.php'); ?>

    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>

            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swimming_pool" class="tab active">Swimming Pool</span>
                    <span data-tab="#pool_deck" class="tab">Restaurant & Bar</span>
                    <span data-tab="#fitness_centre" class="tab">Fitness Centre</span>
                    <span data-tab="#kids_club" class="tab">Kid's Club</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                    <span data-tab="#dining" class="tab">Dining</span>
                    <span data-tab="#the_cafe" class="tab">The Caf&eacute;</span>
                    <span data-tab="#bake_and_bev" class="tab">Bake &amp; Bev</span>
                    <span data-tab="#room_service" class="tab">Room Service</span>
                </div>

                <div class="tabs-content">
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming_pool.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Swimming Pool</h1>
                                    <p>Make a splash at the outdoor pool featuring a shallow area for children and whirlpool bath with massaging bubbles. The pool deck is lined with loungers and parasols and there is a pool bar to keep you refreshed with drinks and snacks.</p>
                                    <p><span style="color: #516819;">Open : 07.00 to 20.00 hrs</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="pool_deck" class="article" data-tab-name="Pool Deck">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool_deck.jpg" alt="Pool Deck" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Restaurant & Bar</h1>
                                    <p>Situated alongside the lobby, this contemporary The Café & Bake & Bev offer pleasant views of the resort gardens. The all-day dining venue opens with an international buffet breakfast including cereals, tropical fruits, bakery goods and cooked breakfast for both Asian and Western tastes.</p>
                                    <p><span style="color: #516819;">Open : 10.00 to 19.00 hrs</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="fitness_centre" class="article" data-tab-name="Fitness Centre">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness_centre.jpg" alt="Fitness Centre" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Fitness Centre</h1>
                                    <p>The fitness centre is well equipped with exercise machines and weights for a total body workout, and the location overlooking the pool provides a perfect distraction while keeping fit. All members must be at least 16 years of age.</p>
                                    <p><span style="color: #516819;">Open : 07.00 to 21.00 hrs</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="kids_club" class="article" data-tab-name="Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/kids_club.jpg" alt="Kid's Club" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Kid's Club</h1>
                                    <p>The poolside Kid’s Club is a great place for children to play and make new friends. The club features a playground, toys, games and the schedule of daily activities keeps them entertained. Based on availability, all children must be between the ages of 4- 12 years old.</p>
                                    <p><span style="color: #516819;">'Open : 07.00 to 18.00 hrs</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/Orientala_wellness_spa.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Embark a personal journey of wellness and relaxation at the serene Orientala Spa. The elegant spa features three private massage rooms with soothing steam baths and offers a wide range of treatments, massages and packages. A signature service at the spa is a private consultation by expert therapists to discover your unique balance of elements; fire, air, water and earth, resulting the perfect therapy for harmony of body and mind.</p>
                                    <p><span style="color: #516819;">Open : 10.00 to 22.00 hrs</span></p>
                                    <p class="note">“Spa will be close during period on 18 – 31 March 2020”</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="dining" class="article" data-tab-name="Dining">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/dining.jpg" alt="Dining" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Dining</h1>
                                    <p>There are several outlets at Ramada Phuket Deevana to enjoy delicious local and international cuisine in a modern and stylish setting; alternatively room service is also available for dining in the privacy of your accommodation.</p>
                                    <p><span style="color: #516819;">Dinning is available during 06.30-23.00 hrs. (last order at 22.30 hrs.)<br>
                                    Buffet breakfast: opening during 06.30 – 10.30hrs.
                                    </span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="the_cafe" class="article" data-tab-name="The Caf&eacute;">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/the_cafe.jpg" alt="The Caf&eacute;" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">The Caf&eacute;</h1>
                                    <p>Situated alongside the lobby, this contemporary The Café offers pleasant views of the resort gardens. The all-day dining venue opens with an international buffet breakfast including cereals, tropical fruits, bakery goods and cooked breakfast for both Asian and Western tastes.</p>
                                    <p><span style="color: #516819;">Open: daily 06:30-23:00 hrs. (Last order at 22.30hrs)<br>Buffet breakfast: opening during 06:30-10:30 hrs.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="bake_and_bev" class="article" data-tab-name="Bake &amp; Bev">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/bake_and_bev.jpg" alt="Bake &amp; Bev" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Bake &amp; Bev</h1>
                                    <p>A selection of snacks and meals can be delivered directly to your room, perfect for breakfast in bed or a cosy night in with a movie.</p>
                                    <p><span style="color: #516819;">Open: daily 10.00 - 23.00 hrs. (Last order at 22.30hrs) </span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="room_service" class="article" data-tab-name="ROOM SERVICE">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/room_service.jpg" alt="Room Service" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ROOM SERVICE</h1>
                                    <p>A selection of snacks and meals can be delivered directly to your room, perfect for breakfast in bed or a cosy night in with a movie.</p>
                                    <p><span style="color:#516819;">Open: 07.00-23.00. hrs. (Last order at 22.30hrs) </span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
            </div>
        </section>
    </section>

</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;

        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #be0d3a;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
        overflow-x: auto;
        white-space: nowrap;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #be0d3a;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #be0d3a;
    }
    .col-cap p.note {
        background-color: #f5f5f5;
        padding: 5px 10px;
        font-size: 12px;
        display: inline-block;
        border-radius: 2px;
    }
    @media (max-width: 1024px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 600px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
