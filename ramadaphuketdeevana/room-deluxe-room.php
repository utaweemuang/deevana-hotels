<?php
$title = 'Deluxe Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'deluxe room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-deluxe-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-deluxe-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-1.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-2.jpg" alt="Deluxe Room 02" />
                    <img src="images/accommodations/deluxe/1500/deluxe-3.jpg" alt="Deluxe Room 03" />
                    <img src="images/accommodations/deluxe/1500/deluxe-4.jpg" alt="Deluxe Room 04" />
                    <img src="images/accommodations/deluxe/1500/deluxe-5.jpg" alt="Deluxe Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-1.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-2.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-3.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-4.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-5.jpg" height="50" /></li>
                </ul>
            </div> 
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-1.jpg" />
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>The inviting Deluxe Room is 35 sq. metres and features a 6ft king-size or twin 4ft single beds, a cosy chair with coffee table and a vanity table with mirror. The air conditioning, natural colours and mood lighting create a relaxing ambiance and the private balcony extends the indoor living space providing an outdoor area to relax and enjoy the warm climate.</p>
                            <p>The refreshments centre includes a mini-fridge and hot drinks facility and the wall mounted flat screen TV has a handy remote control to browse the satellite channels. There is plenty of built-in storage for personal items and a personal safe large enough for laptop computers for added peace of mind.</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>King/ Twin bedding</li>
                                        <li>4 Pillow (soft/firm)</li>
                                        <li>Private balcony with table and chairs</li>
                                        <li>42″ LED TV</li>
                                        <li>Satellite Channel</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King/ Twin bedding</li>
                                            <li>4 Pillow (soft/firm)</li>
                                            <li>Private balcony with table and chairs</li>
                                            <li>42″ LED TV</li>
                                            <li>Satellite Channel</li>
                                            <li>LED Reading Lamp</li>
                                            <li>Electronic Safety box</li>
                                            <li>Alarm Clock</li>
                                            <li>Bathroom with rain shower</li>
                                            <li>Flash light</li>
                                            <li>Hair Dryer</li>
                                            <li>Mini‐Fridge</li>
                                            <li>Make up/Shaving mirror</li>
                                            <li>Wall mounted iron and board</li>
                                            <li>Writing desk</li>
                                            <li>Beach bag</li>
                                            <li>Bathrobe</li>
                                            <li>Umbrellas</li>
                                            <li>Free Wi‐Fi high speed Internet</li>
                                            <li>Free Bath Products</li>
                                            <li>Free 2 bottles of drinking water daily</li>
                                            <li>Free coffee and tea facilities, replenished daily</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>