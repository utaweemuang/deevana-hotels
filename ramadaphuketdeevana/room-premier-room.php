<?php
$title = 'Premier Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Premier Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'premier room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-premier-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-premier-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">

        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" />
                    <img src="images/accommodations/premier/1500/premier-04.jpg" alt="Premier Room 04" />
                    <img src="images/accommodations/premier/1500/premier-05.jpg" alt="Premier Room 05" />
                    <img src="images/accommodations/premier/1500/premier-06.jpg" alt="Premier Room 06" />
                </div>
            </div>
        </div>

        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Premier Room <span>King size bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-06.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Premier Room</h1>

                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />

                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>

                        <div class="col-w5 col-info">
                            <p>The 38 sq. metres Premier Room with the 7ft king-size bed and floor-to-ceiling windows to enjoy the tropical views. The warm colours of the décor and chic furnishings are complimented with a wide range of amenities for a comfortable stay.</p>
                            <p>Get ready for a night out at the vanity table and relax with a morning fresh coffee on the spacious private balcony. The extra features of the Premier include a complimentary set of mini bar drinks and snacks daily, cosy bathrobes plus additional bath products; body lotion, dental and shaving kit.</p>

                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>

                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>King bedding</li>
                                        <li>4 Pillow (soft/firm)</li>
                                        <li>Private balcony with table and chairs</li>
                                        <li>47″ LED TV</li>
                                        <li>Satellite Channel</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>King bedding</li>
                                            <li>4 Pillow (soft/firm)</li>
                                            <li>Private balcony with table and chairs</li>
                                            <li>47″ LED TV</li>
                                            <li>Satellite Channel</li>
                                            <li>LED Reading Lamp</li>
                                            <li>Electronic Safety box</li>
                                            <li>Alarm Clock</li>
                                            <li>Bathroom with rain shower</li>
                                            <li>Flash light</li>
                                            <li>Hair Dryer</li>
                                            <li>Mini‐Fridge</li>
                                            <li>Make up/Shaving mirror</li>
                                            <li>Wall mounted iron and board</li>
                                            <li>Writing desk</li>
                                            <li>Beach bag</li>
                                            <li>Bathrobe and disposal slippers</li>
                                            <li>Umbrellas</li>
                                            <li>Weight Scale</li>
                                            <li>Free Wi‐Fi high speed Internet</li>
                                            <li>Free Bath Products</li>
                                            <li>Free 4 bottles of drinking water daily</li>
                                            <li>Free set of mini bar drinks and snacks, replenished daily</li>
                                            <li>Free Tea, replenished daily</li>
                                            <li>Kettle</li>
                                            <!-- <li>Free Fresh coffee</li> -->
                                            <!-- <li>Turn down service</li> -->
                                            <li>Luggage stand</li>
                                        </ul>
                                    </div>
                                </div>

                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>

            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>

    </div>
</main>

<?php include_once('_footer.php'); ?>
