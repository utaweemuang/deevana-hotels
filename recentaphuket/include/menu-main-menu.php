<ul class="menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">Home</a></li>
    <li class="<?php echo get_current_class('accommodation'); ?>"><a href="accommodation.php">Accommodation</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">Facilities</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">Attractions</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('294', 'en'); ?>" target="_blank">Promotion</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">Gallery</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>
