<?php
$title = 'Accommodaton | Recenta Phuket Suanluang l Official Hotel Group Website Thailand';
$desc = 'Accommodation: Guarantee best direct hotel rate starting from USD 30 per night; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'accommodation, recenta phuket, recenta suanluang, phuket, suan luang, Suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'accommodation room';
$cur_page = 'accommodation';

$lang_en = '/recentaphuket/accommodation.php';
$lang_th = '/th/recentaphuket/accommodation.php';
$lang_zh = '/zh/recentaphuket/accommodation.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/1500/room-01.jpg" alt="Room 01" />
                    <img src="images/accommodations/1500/room-02.jpg" alt="Room 02" />
                    <img src="images/accommodations/1500/room-03.jpg" alt="Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/600/room-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/600/room-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Our Deluxe Rooms is 32 Square meters of Space with 7 feet comfortable king size Bed. The private bathroom has full amenities. Complimentary high speed internet.</p>
                            <p><span style="color: #1a355e;">Rooms available: 66</span></p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Air‐conditioning with individual control</li>
                                        <li>LED TV with cable TV</li>
                                        <li>Private bathroom with hot &amp; cold shower</li>
                                        <li>Universal plug</li>
                                        <li>Electricity 220V.</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Air‐conditioning with individual control</li>
                                            <li>Bathroom amenities included: Shampoo, Soap, Bath Gel, Shower Cap</li>
                                            <li>LED TV with cable TV</li>
                                            <li>Private bathroom with hot &amp; cold shower</li>
                                            <li>Universal plug</li>
                                            <li>Electricity 220V.</li>
                                            <li>Coffee and Tea facilities</li>
                                            <li>Complimentary two bottles of drinking water</li>
                                            <li>Hair dryer</li>
                                            <li>Slipper / Sandal</li>
                                            <li>Safety Box</li>
                                            <li>Free Wifi Internet</li>
                                            <li>Balcony / Terrace</li>
                                            <li>Refrigerator</li>
                                            <li>Kettle</li>
                                            <li>Umbrella</li>
                                            <li>Beach Bag</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>