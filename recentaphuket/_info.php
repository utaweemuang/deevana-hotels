<?php

$url        = 'http://www.deevanahotels.com/recentaphuket/';
$name       = 'Recenta Phuket Suanluang';
$version    = '20180403';
$email      = 'rvn@recentahotels.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/recentaphuketsuanluang';
$twitter    = '#';
$googleplus = '#';
$youtube    = '#';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/recentahotels/';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = '#';

$ibeID      = '294';
