<?php
$title = 'Meeting Seminar Events Mice in Phuket & Krabi';
$desc = 'Deevana Hotels & Resorts – The best Thailand’s local hotel chain. Provide comfortable accommodation in Phuket and Krabi';
$keyw = 'Deevana Plaza Krabi, Deevana Plaza Phuket, Deevana Patong Resort & Spa, Deevana Krabi Resort, Ramada Phuket Deevana, Recenta Suite, Recenta Phuket, Recenta Style';

$html_class = '';
$body_class = 'offers';
$cur_page = 'meeting-events';

$lang_en = '/meeting-events.php';
$lang_th = '/th/meeting-events.php';
$lang_zh = '/zh/meeting-events.php';

include_once('_header.php');
?>

<!-- Swiper -->
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css'/>
<script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js'></script>
        
<div id="ourbrands_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/meeting/main-slide-01.jpg" /></div>
    <div class="item"><img src="images/meeting/main-slide-02.jpg" /></div>
    <div class="item"><img src="images/meeting/main-slide-03.jpg" /></div>
    <div class="item"><img src="images/meeting/main-slide-04.jpg" /></div>
    <div class="item"><img src="images/meeting/main-slide-05.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <div class="main-content">
            <div class="container">
				
               	<div class="row row-header">
					<div class="col-w12">
						<h1 class="section-title underline">
							<span style="color: #9A7B12;">Meeting & Events</span>
						</h1>
                        <p>To explore your experience, discover a meeting rooms, seminar room, conference room, banquet room, event space in Phuket and Krabi with beyond expectation services at Deevana Hotels and Resorts. Our meeting rooms with a modern decoration and facility which can be fit to minimum 10 to 600 delegates, space is highly configurable to the specific requirement. Our hotels are closed to shopping center and entertainment zone in Patong, Phuket and Aonang, Krabi which you are very easy to connect. Apart from that, our location is in the heart of convenient area which you can reach our hotels easily. 7 graceful hotels under branding Deevana Hotels and Resort are warmly welcome you to fulfill our heart and explore your experience.</p>
					</div>
				</div>

                <div class="row row-content">
                    <div class="col-w12">
                        <div class="header-panel phuket-color">Patong Beach, Phuket</div>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPP/01.jpg" alt="Deevana Plaza Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPP/02.jpg" alt="Deevana Plaza Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPP/03.jpg" alt="Deevana Plaza Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPP/04.jpg" alt="Deevana Plaza Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPP/05.jpg" alt="Deevana Plaza Phuket" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="phuket-link" href="http://www.deevanahotels.com/deevanaplazaphuket/meetings-and-event.php"><b>Deevana Plaza Phuket Patong</b></a></dd>
                            <dt>Address :</dt>
                            <dd>239/14 Raj‐U‐Thid 200 Pee Road, Patong, Phuket, 83150 Thailand</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="phuket-link">Patong Beach</b> :  7 minutes by walking <br><b class="phuket-link">Phuket Airport</b>  :  60 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>6 Rooms</dd>
                            <dt>Largest room :</dt>
                            <dd>565 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>249 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>20 - 600 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Deevana-Plaza-Phuket-Patong.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.deevanahotels.com/deevanaplazaphuket/meetings-and-event.php"><img class="force logo-thumb" src="./images/our_brands/brands/deevana_plaza_patong.png" alt="" width="300" height="149"></a>
                    </div>
                    <div class="col-w12">
                        <hr>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPR/01.jpg" alt="Deevana Patong" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPR/02.jpg" alt="Deevana Patong" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPR/03.jpg" alt="Deevana Patong" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPR/04.jpg" alt="Deevana Patong" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPR/05.jpg" alt="Deevana Patong" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="phuket-link" href="http://www.deevanahotels.com/deevanapatong/facilities.php"><b>Deevana Patong Resort & Spa</b></a></dd>
                            <dt>Address :</dt>
                            <dd>43/2 Raj‐U‐Thid 200 Pee Road, Patong Beach, Kathu, Phuket 83150 Thailand</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="phuket-link">Patong Beach</b> :  8 minutes by walking <br><b class="phuket-link">Phuket Airport</b>  :  50 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>1 Room</dd>
                            <dt>Largest room :</dt>
                            <dd>112 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>235 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>20 - 80 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Deevana-Patong-Resort-Spa.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.deevanahotels.com/deevanapatong/facilities.php"><img class="force logo-thumb" src="./images/our_brands/brands/deevana_patong.png" alt="" width="300" height="149"></a>
                    </div>
                    <div class="col-w12">
                        <hr>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RMD/01.jpg" alt="Ramada Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RMD/02.jpg" alt="Ramada Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RMD/03.jpg" alt="Ramada Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RMD/04.jpg" alt="Ramada Phuket" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RMD/05.jpg" alt="Ramada Phuket" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="phuket-link" href="http://www.deevanahotels.com/ramadaphuketdeevana/meetings-and-event.php"><b>Ramada By Wyndham Phuket Deevana </b></a></dd>
                            <dt>Address :</dt>
                            <dd>45/1 Raj‐U‐Thid 200 Pee Road, Patong, Kathu District Phuket, 83150 Thailand</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="phuket-link">Patong Beach</b> :  7 minutes by walking <br><b class="phuket-link">Phuket Airport</b>  :  50 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>2 Rooms</dd>
                            <dt>Largest room :</dt>
                            <dd>80 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>206 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>20 - 100 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Ramada-by-Wyndham-Phuket-Deevana-Patong.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.deevanahotels.com/ramadaphuketdeevana/meetings-and-event.php"><img class="force logo-thumb" src="./images/our_brands/brands/ramada.png" alt="" width="300" height="149"></a>
                    </div>
                    <div class="col-w12">
                        <hr>
                    </div>

                    <div class="col-w12">
                        <div class="header-panel krabi-color">Aonang Beach, Krabi</div>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPK/01.jpg" alt="Deevana Plaza Krabi" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPK/02.jpg" alt="Deevana Plaza Krabi" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPK/03.jpg" alt="Deevana Plaza Krabi" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPK/04.jpg" alt="Deevana Plaza Krabi" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DPK/05.jpg" alt="Deevana Plaza Krabi" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="krabi-link" href="http://www.deevanahotels.com/deevanaplazakrabi/meetings-and-event.php"><b>Deevana Plaza Krabi Aonang</b></a></dd>
                            <dt>Address :</dt>
                            <dd>186 Moo 3, Aonang Soi 8, Aonang Beach, Aonang, Muang, Krabi 81180 Thailand</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="krabi-link">Aonang Beach</b> :  10 minutes by walking <br><b class="krabi-link">Krabi Airport</b>  :  30 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>5 Rooms</dd>
                            <dt>Largest room :</dt>
                            <dd>320 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>213 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>15 - 300 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Deevana-Plaza-Krabi-Aonang.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.deevanahotels.com/deevanaplazakrabi/meetings-and-event.php"><img class="force logo-thumb" src="./images/our_brands/brands/deevana_plaza_krabi.png" alt="" width="300" height="149"></a>
                    </div>
                    <div class="col-w12">
                        <hr>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DKR/01.jpg" alt="Deevana Krabi Resort" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DKR/02.jpg" alt="Deevana Krabi Resort" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DKR/03.jpg" alt="Deevana Krabi Resort" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DKR/04.jpg" alt="Deevana Krabi Resort" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/DKR/05.jpg" alt="Deevana Krabi Resort" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="krabi-link" href="http://www.deevanahotels.com/deevanakrabiresort/"><b>Deevana Krabi Resort</b></a></dd>
                            <dt>Address :</dt>
                            <dd>90 Moo 3 , Aonang Soi 8, Aonang Beach ,Aonang, Muang, Krabi 81180 Thailand</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="krabi-link">Aonang Beach</b> :  10 minutes by walking <br><b class="krabi-link">Krabi Airport</b>  :  30 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>1 Rooms</dd>
                            <dt>Largest room :</dt>
                            <dd>22 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>66 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>12 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Deevana-Krabi-Resort.PDF">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.deevanahotels.com/deevanakrabiresort/"><img class="force logo-thumb" src="./images/our_brands/brands/deevana_krabi.png" alt="" width="300" height="149"></a>
                    </div>
                    <div class="col-w12">
                        <hr>
                    </div>
                  <div class="col-w12">
                        <div class="header-panel town-color">Phuket Town, Phuket</div>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RSP/01.jpg" alt="Recenta Suite Phuket Suanluang" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RSP/02.jpg" alt="Recenta Suite Phuket Suanluang" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RSP/03.jpg" alt="Recenta Suite Phuket Suanluang" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RSP/04.jpg" alt="Recenta Suite Phuket Suanluang" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/RSP/05.jpg" alt="Recenta Suite Phuket Suanluang" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                    <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a class="town-link" href="http://www.recentahotels.com/recenta-suite-phuket-suanluang/meeting.html "><b>Recenta Suite Phuket Suanluang </b></a></dd>
                            <dt>Address :</dt>
                            <dd>60/80 Moo2, Wichit, Muang Phuket, Phuket, Thailand 83000</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="town-link">Phuket Town</b> :  15 minutes by driving <br><b class="town-link">Phuket Airport</b>  :  50 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>2 Rooms</dd>
                            <dt>Largest room :</dt>
                            <dd>141 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>48 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>15 - 50 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Recenta-Hotels-Group.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2"> <a href="http://www.recentahotels.com/recenta-suite-phuket-suanluang/meeting.html "><img class="force logo-thumb" src="./images/our_brands/brands/recenta_suite_with_location.png" alt="" width="300" height="149"></a>
                    </div>

                    <div class="col-w12">
                        <hr>
                    </div>
                    <div class="col-w4">
                        <div class="swiper-container swiper-default">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img class="force" src="./images/meeting/REP/01.jpg" alt="Recenta Style Phuket Town" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/REP/02.jpg" alt="Recenta Style Phuket Town" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/REP/03.jpg" alt="Recenta Style Phuket Town" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/REP/04.jpg" alt="Recenta Style Phuket Town" width="421" height="288"></div>
                                <div class="swiper-slide"><img class="force" src="./images/meeting/REP/05.jpg" alt="Recenta Style Phuket Town" width="421" height="288"></div>
                            </div>
                            <div class="carousel-pagination swiper-pagination swiper-pagination-bullets"></div>
                        </div>
                    </div>
                  <div class="col-w6">
                        <dl class="info-hotel">
                            <dt>Hotel Name :</dt>
                            <dd><a href="http://www.recentahotels.com/recenta-express-phuket-town/" class="town-link"><b>Recenta Style Phuket Town</b></a></dd>
                            <dt>Address :</dt>
                            <dd>10/1 Rattanakorsin 200 Pee Road, TaladNua, Muang Phuket, Phuket Thailand 83000</dd>
                            <dt>Destination	:</dt>
                            <dd><b class="town-link">Phuket Town</b> : 5 minutes by driving <br><b class="town-link">Phuket Airport</b> : 50 minutes by driving</dd>
                            <dt>Meeting/Event rooms :</dt>
                            <dd>1 Room</dd>
                            <dt>Largest room :</dt>
                            <dd>14 sq mtr.</dd>
                            <dt>Guest rooms :</dt>
                            <dd>46 Rooms</dd>
                            <dt>Capacity :</dt>
                            <dd>10 Persons</dd>
                        </dl>
                        <div class="buttons">
                            <a target="_blank" class="btn btn-default-01" href="./images/meeting/PDF/Factsheet-Recenta-Hotels-Group.pdf">Download Factsheet</a>
                            <a target="_blank" class="btn btn-default-02 popup-form" href="#meeting_popup">Request Proposal</a>
                        </div>
                    </div>
                    <div class="col-w2">
                        <a href="http://www.recentahotels.com/recenta-express-phuket-town/"><img class="force logo-thumb" src="./images/our_brands/brands/recenta_express_with_location.png" alt="" width="300" height="149"></a>
                    </div>
                    
              </div>

            </div>
        </div>
        
    </div>
</main>

<div id="meeting_popup" class="meeting-form mfp-hide">
    <h2 class="title">Request for Proposal</h2>
    <form id="meeting_form" action="./forms/meeting_form.php" method="POST">
        <div class="form-result alert" style="display: none;"></div>
        <div class="row row-content">
            <div class="col-w12">
                <div class="form-group">
                    <select class="form-control" type="text" name="hotels" required>
                        <option value="" selected>- Select Hotel -</option>
                        <option value="info@deevanaplazaphuket.com">Deevana Plaza Phuket Patong</option>
                        <option value="info@deevanapatong.com">Deevana Patong Resort & Spa</option>
                        <option value="info@ramadaphuketdeevana.com">Ramada by Wyndham Phuket Deevana </option>
                        <option value="info@recentahotels.com">Recenta Suite Phuket Suanluang</option>
                        <option value="info@recentahotels.com">Recenta Phuket Suanluang</option>
                        <option value="info@recentahotels.com">Recenta Style Phuket Town</option>
                        <option value="info@deevanaplazakrabi.com">Deevana Plaza Krabi Aonang</option>
                        <option value="info@deevanakrabiresort.com">Deevana Krabi Resort</option>
                    </select>
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <input class="form-control" type="text" name="name" placeholder="Name*" required="">
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <input class="form-control" type="text" name="email" placeholder="Email*" data-validation="email" required="">
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <input class="form-control" type="text" name="phone" placeholder="Phone*" data-validation="number" required="">
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <textarea name="message" class="form-control" placeholder="Message*" required></textarea>
                </div>
            </div>
            <div class="col-w6" style="padding-right: 5px">
                <div class="form-group">
                    <input class="form-control" type="text" name="date" placeholder="Date*" required="">
                </div>
            </div>
            <div class="col-w6" style="padding-left: 5px">
                <div class="form-group">
                    <input class="form-control" type="text" name="time" placeholder="Time*" required="">
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <input class="form-control" type="text" name="guest" placeholder="Number of people*" data-validation="number" required="">
                </div>
            </div>
            <div class="col-w12">
                <div class="form-group">
                    <input data-validation="recaptcha" data-validation-recaptcha-sitekey="6LfVdLwUAAAAANa9--FiZr3lZOgZz0AdJAfa5skC">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default-02">SEND <span class="loading-indicator"></span></button>
                </div>
            </div>
        </div>
    </form>
</div>
<style>
    .site-main:after {
        right: 100%;    
    }
    .meeting-form {
        background-color: #fff;
        padding: 2rem 1rem;
        position: relative;
        width: 100%;
        height: auto;
        max-width: 380px;
        margin: 0 auto;
        z-index: 2;
    }
    .meeting-form .title {
        color: #9A7B12;
        font-size: 24px;
        font-family: 'Cinzel', serif;
        margin-bottom: 2rem;
        text-transform: uppercase;
    }
    #meeting_form .alert-success {
        background-color: lightgreen;
        border-radius: .25rem;
        color: darkgreen;
    }
    #meeting_form .alert-danger {
        background-color: pink;
        border-radius: .25rem;
        color: brown;
    }
    #meeting_form textarea {
        height: 90px !important;
    }
    #meeting_form .form-group {
        margin-bottom: 1rem;
    }
    #meeting_form .form-control {
        display: block;
        width: 100%;
        height: calc(1.5em + .75rem + 2px);
        padding: .375rem .75rem;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        -webkit-transition: border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
        transition: border-color .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
        -o-transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out,-webkit-box-shadow .15s ease-in-out;
    }
    .header-panel {
        border-radius: .25rem;
        color: #fff;
        display: inline-block;
        font-family: 'Cinzel', serif;
        font-size: 20px;
        padding: .625rem 1.5rem;
        text-transform: uppercase;
        margin: 1.75rem 0;
    }
    .logo-thumb {
        display: none !important;
    }
    @media(min-width: 768px){
        .logo-thumb {
            display: block !important;
        }
    }
    .phuket-color {
        background-color: #1b4489;
    }
    .krabi-color {
        background-color: #00b0f0;
    }
    .town-color {
        background-color: #f7941d;
    }
    .info-hotel {
        position: relative;
        width: 100%;
        height: auto;
    }
    .info-hotel dd {
        margin-bottom: 1rem;
        margin-left: 160px;
    }
    .phuket-link {
        color: #236198;
    }
    .krabi-link {
        color: #00b0f0;
    }
    .town-link {
        color: #f7941d;
    }
    .buttons {
        position: relative;
        padding-top: 2rem;
    }
    .btn-default-01 {
        background-color: #597f71;
        border-radius: .25rem;
        border: none;
        color: #fff;
        display: inline-block;
        padding: .625rem 1.25rem;
        margin-bottom: 1rem;
        text-transform: uppercase;
    }
    .btn-default-01:hover { 
        background-color: #4d6d61;
        color: #fff;
    }
    .btn-default-02 {
        background-color: #1b4489;
        border-radius: .25rem;
        border: none;
        color: #fff;
        display: inline-block;
        padding: .625rem 1.25rem;
        margin-bottom: 1rem;
        text-transform: uppercase;
    }
    .btn-default-02:hover { 
        background-color: #112c5a;
        color: #fff;
    }
    .loading-indicator{display:none;vertical-align:middle;width:18px;height:18px;margin-left:6px;border-radius:50%;border-width:3px;border-style:solid;border-color:transparent #fff #fff;-webkit-animation:spin 1s infinite linear;animation:spin 1s infinite linear}.loading-indicator.show{display:block;position:absolute;top:55px;left:50%;border-color:transparent #000 #000}@-webkit-keyframes spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{to{-webkit-transform:rotate(360deg);transform:rotate(360deg)}}.form-sending [type=submit]{pointer-events:none}.form-sending .loading-indicator{display:inline-block}.form-result{display:none;margin-bottom:1rem;padding:0.5rem 1rem;border-radius:0.25rem}
</style>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        $cs.find(atd).show();
        
        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });
        
        $cs.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            if( tabName !== undefined ) {
                $this.before('<span class="accordion-tab">'+tabName+'</span>');
                $this.prev('.accordion-tab').on('click', function() {
                    var i = $(this).index('.accordion-tab');
                    $(this).addClass('active').siblings().removeClass('active');
                    $this.slideDown(300, function() {
                        var pos = $(this).offset().top;
                        var offset = 50;
                        $('html, body').animate({
                            scrollTop: pos - offset,
                        }, 800);
                    }).siblings().not('.accordion-tab').slideUp(300);
                    $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                });
            };
        });
        
        $('.accordion-tab').eq(ati).addClass('active');
        
        
		$(window).on('load', function() {
			//Go to Landing section
			var pos = $('.site-main').offset().top;
			var offset = ( $(window).width() > 960 ) ? 144 : 0 ;
			scrollTo(0, Math.round(pos - offset) );
		});
        
        $('.popup-form').each(function(){
            $(this).magnificPopup({
                type: 'inline',
                callbacks : {
                    open : function(){
                        // Fishing Form 
                        var $fishingForm = $('#meeting_form');
                        var $fishingFormResult = $fishingForm.find('.form-result');
                        var emailto = 'info@deevanahotels.com';
                        $.validate({
                            form: $fishingForm,
                            modules: 'html5, security',
                            onSuccess: function($form){
                                var form    = $form.get(0);
                                var url     = form.action;
                                var data    = new FormData(form);

                                data.append('base', form.baseURI);

                                var postAjax = $.ajax({
                                    type: 'POST',
                                    url: url,
                                    data: data,
                                    processData: false,
                                    contentType: false,
                                    beforeSend: function() {
                                        $form.addClass('form-sending');
                                    }
                                });

                                postAjax.done(function(data, status, jqXHR) {
                                    var data = JSON.parse(data);
                                    if (data.status=='success') {
                                        displayResult($fishingFormResult, 'alert-success', 'Thank You! Your information has been sent and we should be in touch with you soon.');
                                        form.reset();
                                    } else {
                                        displayResult($fishingFormResult, 'alert-danger', 'Message cannot be sent. Please contact directly to <a class="alert-link" href="mailto:'+ emailto +'">'+ emailto +'</a> <br/>We apologize for any inconvenience.');
                                    }
                                });

                                postAjax.fail(function(jqXHR) {
                                    displayResult($fishingFormResult, 'alert-danger', 'Sorry, Message cannot be sent!')
                                });

                                postAjax.always(function() {
                                    $form.removeClass('form-sending');
                                });

                                return false;
                            }
                        });
                    }
                }
            });
        });
        

        function displayResult(selector, addClass, message) {
            var $el = $(selector);
            $el.removeClass('alert-success alert-danger')
            .addClass(addClass)
            .html(message)
            .fadeIn();
        }

        $('.swiper-default').each(function(){
            var _this = this;
            new Swiper(_this, {
                loop: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                // autoplay: {
                //     delay: 3000,
                // },
                // speed : 1200,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        });
    });
</script>

<style>
	#landing > .row {
		margin-bottom: 50px;
		text-align: center;
	}
	#landing > .row:last-of-type {
		margin-bottom: 0;
	}
	
	.table-brand {
		width: 100%;
		text-align: center;
	}
	.table-brand img {
		max-width: 100%;
		height: auto;
		display: block;
		margin: 0 auto;
	}
	.table-brand .cell-logo,
	.table-brand .cell-slogan {
		vertical-align: middle;
		width: 50%;
		padding: 1em;
	}
	.table-brand .cell-logo {
		position: relative;
	}
	.table-brand .cell-logo:after {
		content: '';
		display: block;
		width: 1px;
		height: 60px;
		background-color: #666;
		position: absolute;
		right: 0;
		top: 50%;
		margin-top: -30px;
		margin-right: 0.5px;
	}
	
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    
    /* Logo in post tab */
    .post-tabs .logo-tab:before {
        content: '';
        background-size: contain;
        background-repeat: no-repeat;
        background-position: right top;
        width: 100%;
        height: 76%;
        display: block;
        position: absolute;
        top:12%;
        right: 15px;
        -webkit-filter: grayscale(1) opacity(0.75);
        filter: grayscale(1) opacity(0.75);
        -webkit-transition: 200ms;
        transition: 200ms;
    }
    .post-tabs .deevana:before { background-image: url(images/our_brands/brands/deevana.png); }
    .post-tabs .deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .post-tabs .deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .post-tabs .deevana-plaza:before { background-image: url(images/our_brands/brands/deevana_plaza.png); }
    .post-tabs .deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .post-tabs .deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .post-tabs .ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .post-tabs .recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite.png); }
    .post-tabs .recenta:before { background-image: url(images/our_brands/brands/recenta.png); }
    .post-tabs .recenta-express:before { background-image: url(images/our_brands/brands/recenta_express.png); }
    
    .post-tabs .logo-tab:hover:before,
    .post-tabs .logo-tab.active:before {
        -webkit-filter: none;
        filter: none;
    }
    
    /* Logo in title */
    [class*="with-logo-"] {
        position: relative;
        padding-right: 80px;
    }

    [class*="with-logo-"]:before {
        content: '';
        background-repeat: no-repeat;
        background-position: right top;
        background-size: contain;
        display: block;
        position: absolute;
        top: -10px;
        right: 0;
        top: 50%;
        width: 100%;
        height: 40px;
        margin-top: -20px;
        z-index: -1;
    }

    .with-logo-deevana-krabi:before { background-image: url(images/our_brands/brands/deevana_krabi.png); }
    .with-logo-deevana-phuket:before { background-image: url(images/our_brands/brands/deevana_patong.png); }
    .with-logo-deevana-plaza-krabi:before { background-image: url(images/our_brands/brands/deevana_plaza_krabi.png); }
    .with-logo-deevana-plaza-phuket:before { background-image: url(images/our_brands/brands/deevana_plaza_patong.png); }
    .with-logo-ramada:before { background-image: url(images/our_brands/brands/ramada.png); }
    .with-logo-recenta-suite:before { background-image: url(images/our_brands/brands/recenta_suite_with_location.png); }
    .with-logo-recenta-suanluang:before { background-image: url(images/our_brands/brands/recenta_suanluang_with_location.png); }
    .with-logo-recenta-express:before { background-image: url(images/our_brands/brands/recenta_express_with_location.png); }
    
    .row-brands {
        text-align: center;
        margin-bottom: 30px;
    }
    .row-brands .logo {
        max-width: 100%;
        height: auto;
        display: inline-block;
        vertical-align: middle;
        margin-bottom: 10px;
    }
    
    #landing {
        min-height: 400px;
    }

    @media (max-width: 1024px) {
        .post-tabs .logo-tab:before {
            content: none;
        }
    }
    
    @media (max-width: 768px) {
		.row-header > [class*="col-"],
        .row-content > [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 20px;
        }
        .tabs-content .article.hide-tab {
            padding-top: 0;
        }
        
        .row-brands [class*="col-"] {
            width: 50%;
        }
    }
	
	@media (max-width: 480px) {
		#landing > .row {
			margin-bottom: 0;
			padding: 40px 0;
			position: relative;
		}
		#landing > .row:before {
			content: '';
			position: absolute;
			bottom: 0;
			left: 40px;
			right: 40px;
			height: 1px;
			background-color: #ccc;
		}
		#landing > .row:last-of-type:before {
			content: none;
		}
		.table-brand .cell-logo,
		.table-brand .cell-slogan {
			width: 100%;
			display: block;
		}

		.table-brand .cell-logo:after {
			width: 60px;
			height: 1px;
			top: auto;
			left: 50%;
			right: auto;
			bottom: 0;
			margin: -0.5px 0 0 -30px;
		}
	}
</style>

<?php include_once('_footer.php'); ?>
