<?php
$title = 'Superior Garden Room | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Superior Garden Room: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'superior garden room, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-superior-garden';
$cur_page = 'superior-garden';
$par_page = 'rooms';

$lang_en = 'room-superior-garden.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/superior/1500/superior-garden-01.jpg" alt="Superior Garden Room 01" />
                    <img src="images/accommodations/superior/1500/superior-garden-02.jpg" alt="Superior Garden Room 02" />
                    <img src="images/accommodations/superior/1500/superior-garden-03.jpg" alt="Superior Garden Room 03" />
                    <img src="images/accommodations/superior/1500/superior-garden-04.jpg" alt="Superior Garden Room 04" />
                    <img src="images/accommodations/superior/1500/superior-garden-05.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-06.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-07.jpg" alt="Superior Garden Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">Superior Garden Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/superior/600/superior-garden-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-06.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-07.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Superior Garden Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/superior/600/superior-garden-02.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>The 27‐sqm Superior Garden Room comes with a choice of twin beds or a 6 ft king‐sized bed. The classic dark wood furniture contrasts well with the fresh clean look of the room that has a touch of Thai décor with classic decorations and soft furnishings. The en‐suite bathroom is complete with a shower and closet.</p>                            
                            
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                    <li>Air-conditioning with individual control</li>
                                    <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                    <li>Luggage rack</li>
                                    <li>International direct dial telephone</li>
                                    <li>Color TV satellite channel with international news</li>
                                    <li class="more clickable">
                                        <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                    </li>
                                </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                        <li>Air-conditioning with individual control</li>
                                        <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                        <li>Luggage rack</li>
                                        <li>International direct dial telephone</li>
                                        <li>Color TV satellite channel with international news</li>
                                        <li>Refrigerator with complimentary drinking water: 2 bottles per day</li>
                                        <li>Private bathroom with hot & cold shower</li>
                                        <li>Electricity 220 V.</li>
                                        <li>Private garden view balcony (Garden Wing)</li>
                                        <li>Coffee/tea making facilities</li>
                                        <li>Hair dryer</li>
                                        <li>Safety Box</li>
                                        <li>Interconnecting room</li>
                                        <ul><b>Remarks:</b>
                                            <li>Beach Towels service at bell counter.</li>
                                            <li>Pool Towels service at Pool side.</li>
                                        </ul>
                                    </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>