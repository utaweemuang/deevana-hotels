<?php
$title = 'Junior Suite with Jacuzzi | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Junior Suite with Jacuzzi: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'junior suite with jacuzzi, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-jacuzzi';
$cur_page = 'junior-with-jacuzzi';
$par_page = 'rooms';

$lang_en = 'room-junior-with-jacuzzi.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
           			<img src="images/accommodations/junior/1500/junior-06.jpg" alt="Junior Suite with Jacuzzi 03" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-3.jpg" alt="Junior Suite with Jacuzzi 03" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-03.jpg" alt="Junior Suite with Jacuzzi 03" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-04.jpg" alt="Junior Suite with Jacuzzi 04" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-05.jpg" alt="Junior Suite with Jacuzzi 05" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-4.jpg" alt="Junior Suite with Jacuzzi 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Junior Suite with Jacuzzi <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>   
                    <li class="current"><img src="images/accommodations/junior/600/junior-06.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-4.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Junior Suite with Jacuzzi</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior/600/junior-06.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Indulge with and Junior Suite with Jacuzzi complete with its own private Jacuzzi. At 55 sqm, the suite is a great option for families sharing or couples looking for a little extravagance on their holiday. The spacious room has a 7ft king‐sized bed, refrigerator, tea / coffee making facilities a day bed, living area with 42” LCD TV, outdoor seating, desk / dressing table and a large bathroom with a bathtub and shower. Additional beds are available upon request accommodating a total of four people.</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                        <div class="amenities">
                            <div class="shad-over">
                                <h2 class="title">Guest Room Amenities</h2>
                                <ul class="amenities-list">
                                <li>Air-conditioning with individual control</li>
                                <li>Bathroom amenities: Bathtub with shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                <li>Luggage rack</li>
                                <li>International direct dial telephone</li>
                                <li>Color TV satellite channel with international news</li>
                                <li class="more clickable">
                                    <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                </li>
                            </ul>

                                <div id="all_amenities" class="mfp-hide popup">
                                    <h2>All Amenities</h2>
                                    <ul class="list-columns-2">
                                    <li>Air-conditioning with individual control</li>
                                    <li>Bathroom amenities: Bathtub with shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                    <li>Luggage rack</li>
                                    <li>International direct dial telephone</li>
                                    <li>Color TV satellite channel with international news</li>
                                    <li>Refrigerator with complimentary drinking water: 3 bottles per day</li>
                                    <li>Private bathroom &amp; bathtub attached hot &amp; cold shower</li>
                                    <li>A living area</li>
                                    <li>Electricity 220 V.</li>
                                    <li>Pool View balcony (Deluxe Wing)</li>
                                    <li>Coffee/tea making facilities</li>
                                    <li>Hair dryer</li>
                                    <li>Safety Box</li>
                                    <li>Interconnecting room</li>
                                    <ul><b>Remarks:</b>
                                        <li>Beach Towels service at bell counter.</li>
                                        <li>Pool Towels service at Pool side.</li>
                                    </ul>
                                </ul>
                                </div>
                            </div>
                            
                            <span class="shad-left"></span>
                            <span class="shad-right"></span>
                        </div>
                    </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>