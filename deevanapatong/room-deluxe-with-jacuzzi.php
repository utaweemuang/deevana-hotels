<?php
$title = 'Deluxe Room with Jacuzzi | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room with Jacuzzi: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deluxe room with jacuzzi, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-with-jacuzzi';
$cur_page = 'deluxe-with-jacuzzi';
$par_page = 'rooms';

$lang_en = 'room-deluxe-with-jacuzzi.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe_jacuzzi/1500/dlxj-1.jpg" alt="Deluxe with Jacuzzi 01" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/dlxj-2.jpg" alt="Deluxe with Jacuzzi 02" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/dlxj-3.jpg" alt="Deluxe with Jacuzzi 03" />
                     <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-04.jpg" alt="Deluxe with Jacuzzi 04" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-02.jpg" alt="Deluxe with Jacuzzi 05" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-03.jpg" alt="Deluxe with Jacuzzi 06" /> 
                    <img src="images/accommodations/deluxe_jacuzzi/1500/deluxe-jacuzzi-05.jpg" alt="Deluxe with Jacuzzi 07" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/dlxj-4.jpg" alt="Deluxe with Jacuzzi 08" />
                    <img src="images/accommodations/deluxe_jacuzzi/1500/dlxj-5.jpg" alt="Deluxe with Jacuzzi 09" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe with Jacuzzi <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe_jacuzzi/600/dlxj-1.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/dlxj-2.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/dlxj-3.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/deluxe-jacuzzi-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/dlxj-4.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe_jacuzzi/600/dlxj-5.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe with Jacuzzi</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe_jacuzzi/600/dlxj-1.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>The Deluxe with Jacuzzi are 44 sqm included a delightful private Jacuzzi on the balcony. The room features twin or king‐sized beds, refrigerator ,tea / coffee making facilities, bathroom with shower, coffee table and flat screen TV. Guests can also enjoy the pool view from the Jacuzzi and balcony.</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                    <li>Air-conditioning with individual control</li>
                                    <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                    <li>Luggage rack</li>
                                    <li>International direct dial telephone</li>
                                    <li>Color TV satellite channel with international news</li>
                                    <li class="more clickable">
                                        <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                    </li>
                                </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                        <li>Air-conditioning with individual control</li>
                                        <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                        <li>Luggage rack</li>
                                        <li>International direct dial telephone</li>
                                        <li>Color TV satellite channel with international news</li>
                                        <li>Refrigerator with complimentary drinking water: 3 bottles per day</li>
                                        <li>Private bathroom with hot & cold shower</li>
                                        <li>Electricity 220 V.</li>
                                        <li>Pool View balcony (Deluxe Wing)</li>
                                        <li>Coffee/tea making facilities</li>
                                        <li>Hair dryer</li>
                                        <li>Safety Box</li>
                                        <li>Interconnecting room</li>
                                        <ul><b>Remarks:</b>
                                            <li>Beach Towels service at bell counter.</li>
                                            <li>Pool Towels service at Pool side.</li>
                                        </ul>
                                    </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>