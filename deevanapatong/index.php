<?php

session_start();

$title = 'Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanapatong';
$lang_th = '/th/deevanapatong';
$lang_zh = '/zh/deevanapatong';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <!-- <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div> -->
            <div class="item"><img src="images/home/bg-home-0.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-0-1.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-1.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-2.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-3.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-4.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-5.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <div class="item"><img src="images/home/bg-home-6.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
        </div>

       	<!--
        <div id="promotion_board" class="promotion get-center">
            <a href="<?php ibe_url('277', 'en', 'MDgxNjE1'); ?>" target="_blank">
                <img class="block responsive" src="images/home/discount_bann.png" />
            </a>
        </div>
        -->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">Welcome to <br><span style="color:#236198;">Deevana Patong Resort &amp; Spa</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>Get the best of both worlds at Deevana Patong Resort & Spa with 235 rooms. Located at the centre of all the actions in Phuket’s most popular destination – Patong, the resort is just minutes away from the famous Bangla Road’s nightlife, Jungceylon shopping and entertainment centre and of course the wonderful golden sand of the glorious Patong Beach.Yet when entering the grand lobby of Deevana, the feeling of calm and tranquillity waits as the elegant Thai resort, with spacious tropical gardens and tasteful guest rooms and suites; takes you to an oasis of calm, the perfect setting for a relaxing holiday gateway.</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/9TNa8S0FHwU?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">Welcome to <br><span style="color:#236198;">Deevana Patong Resort &amp; Spa</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>Get the best of both worlds at Deevana Patong Resort & Spa with 235 rooms. Located at the centre of all the actions in Phuket’s most popular destination – Patong, the resort is just minutes away from the famous Bangla Road’s nightlife, Jungceylon shopping and entertainment centre and of course the wonderful golden sand of the glorious Patong Beach.Yet when entering the grand lobby of Deevana, the feeling of calm and tranquillity waits as the elegant Thai resort, with spacious tropical gardens and tasteful guest rooms and suites; takes you to an oasis of calm, the perfect setting for a relaxing holiday gateway.</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=277&group=23&width=600&height=400&imageid=23078&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Basic Deal with FREE Breakfast </b></h2>
                                    <p class="description" style="color:yellow;">You will get...</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>20% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits</li>
                                        <li>Deevana Welcome Drink</li>
                                        <li>Free Internet WIFI</li>
                                    </ul>
                                    <p class="description" style="color:green">FREE cancellation / Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=277&onlineId=4&pid=MDczNzQ1OXwwNzQxNjYw">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=277&group=23&width=600&height=400&imageid=23077&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Basic Deal - Room Only </b></h2>
                                    <p class="description" style="color:yellow;">You will get...</p>
                                    <ul>
                                        <li>20% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits</li>
                                        <li>Deevana Welcome Drink</li>
                                        <li>Free Internet WIFI</li>
                                    </ul>
                                    <p class="description" style="color:green">FREE cancellation / Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=277&onlineId=4&pid=MDczNzQ1OHwwNzQxNjYx">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=277&group=13&width=600&height=400&imageid=16100&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Test & Go Package </b></h2>
                                    <p class="description" style="color:yellow;">You will get...</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>One way Transfer from Phuket International airport to Hotel</li>
                                        <li>Covid-19 RT-PCR test at the airport (1-time / person)</li>
                                        <li>20% discount on laundry service</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits</li>
                                        <li>Free late check out until 15.00 hrs.</li>
                                    </ul>
                                    <p class="description" style="color:green">Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=277&onlineId=4&pid=MDg4NjM3">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=277&group=13&width=600&height=400&imageid=15038&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>'STAY 3 PAY 2</b></h2>
                                    <p class="description" style="color:yellow;"> FREE TRANSFER-IN + THAI MASSAGE + LATE CHECK-OUT</p>
                                    <ul>
                                        <li>3 nights in Deluxe Room</li>
                                        <li>Daily breakfast</li>
                                        <li>FREE One way Transfer-in from Phuket internation airport to Hotel</li>
                                        <li>FREE 60 Minutes Thai Massage for couple at Orientala Spa per stay</li>
                                        <li>FREE late check out until 15.00 hrs</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol and special menu</li>
                                        <li>20% discount on laundry service</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=277&onlineId=4&checkin=2021-10-01&checkout=2021-10-04&numofadult=2&numofchild=0&numofroom=1&pid=MDg4MDM4">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=277&group=13&width=450&height=300&imageid=14587&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>'Holiday Escape</b></h2>
                                    <p class="description" style="color:yellow;">MINIMUM STAY 7,14,21 NIGHTS</p>
                                    <ul>
                                        <li>FREE Breakfast for 2 people</li>
                                        <li>Free Wi-Fi</li>
                                        <li>Round Trip Transfer from Phuket internation airport to Hotel</li>
                                        <li>60 Minutes Thai Massage for couple at Orientala Spa per stay</li>
                                        <li>2 Times Thai or International dinner set at Hotel Restaurant per couple</li>
                                        <li>One day trip Phuket City Tour</li>
                                        <li>Complimentary a set of Afternoon tea per couple (2 coffee or tea and 2 pieces of cake per stay)</li>
                                        <li>Complimentary a set of drink per couple (2 local beer and 2 soft drink per stay)</li>
                                        <li>Complimentary Cash Coupon THB 300 to spend at hotel bar & restaurant</li>
                                        <li>20% discount on laundry service</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol and special menu</li>
                                        <li>Free late check out until 15.00 hrs</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=277&onlineId=4&checkin=2021-10-01&checkout=2021-10-08&numofadult=2&numofchild=0&numofroom=1&pid=MDg3Nzc4fDA4Nzc3OXwwODc3ODA%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=277&group=12&imageid=13067&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>'HOLIDAY PACKAGES' MINIMUM 2 NIGHTS, AIRPORT TRANSFER,THAI MASSAGE,AND ANY MORE.</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 2 nights</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>Free Wi-Fi</li>
                                        <li>Free Thai Massage one hour per person per stay</li>
                                        <li>Free a set of drink per stay (2 beers and 2 soft drinks)</li>
                                        <li>Free Afternoon break one set per person per stay (a cup of coffee or tea and a piece of cake)</li>
                                        <li>Free roudtrip transfer from/to airport between 06:00 am to 09:00 pm (Additional charge 350 THB per way between 09:00 pm to 06:00 am)</li>
                                        <li>Remark* Free airport pickup service, please advise flight detail at least 24 hours before arrival time.</li>
                                        <li>Minimum stay 5 nights; Free a selection of Buffet Dinner or Set Dinner (one dinner per person per stay) </li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=277&onlineId=4&pid=MDg1OTAx">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
        <section id="facilities" class="section section-facilities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#236198;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="facilities_slider" class="owl-carousel has-nav fx-scale force-nav">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-01.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">RESTAURANT</h2>
                            <p class="description">Dalah Cuisine, Start your day with a great selection of Thai and continental breakfasts, with all the cooked international favourites, cereals, fruit, tea, coffee and much more ...</p>
                            <p><a class="button" href="facilities.php#restaurant">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-02.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ORIENTALA SPA</h2>
                            <p class="description">Treat your senses to delightful new sensations at the Orientala Spa where the trained therapists will take you on a journey of relaxation and rejuvenation. ...</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-slide-03.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">SWIMMING POOL</h2>
                            <p class="description">Deluxe Pool, The aqua‐blue swimming pool at the Deluxe Wing is a focal point of the resort with all balconies facing the glistening water ...</p>
                            <p><a class="button" href="facilities.php#swimming_pool">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title deco-underline">
                    <span class="deco-map" style="display: inline-block;"><span style="color:#236198;">PHUKET</span> ATTRACTIONS</span>
                </h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" /></div>
                            <h2 class="title">PATONG BEACH</h2>
                            <a class="more" href="attraction.php#patong_beach">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" /></div>
                            <h2 class="title">PHROMTHEP CAPE</h2>
                            <a class="more" href="attraction.php#phromthep_cape">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" /></div>
                            <h2 class="title">KATA AND KARON BEACHES</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" /></div>
                            <h2 class="title">BIG BUDDHA</h2>
                            <a class="more" href="attraction.php#big_buddha">LEARN MORE <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <!-- <li><img src="http://www.deevanahotels.com/images/awards/green-leaf-awards.png" alt="" width="128" height="128"></li> -->
                    <li><img src="http://www.deevanahotels.com/images/awards/SHA+DPT.png" alt="" width="200" height="auto"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/safe-travel.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>
    </section>
</main>
<?php

if (!isset($_SESSION['visited'])) { ?>
<?php }
$_SESSION['visited'] = "true";
?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #facilities {
        background-image: url(images/home/bg-facilities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #facilities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #facilities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #facilities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #facilities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #facilities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #facilities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #facilities_slider .center .caption .button {
        background-color: #00adb7;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.force-nav .owl-nav,
        .owl-carousel.force-nav .owl-nav.disabled {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });

        $('#facilities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
    });
    var $banner = $('.tl-sticky-banner'),
        $toggle = $('.tl-sticky-banner-toggle'),
        $close = $('.tl-sticky-banner .content-close');

    $toggle.on('click', function() {
        $toggle.addClass('show');
        $banner.addClass('show');
    });

    $close.on('click', function() {
        $toggle.removeClass('show');
        $banner.removeClass('show');
    });

    if( window.innerWidth >= 320 ) {
        $toggle.addClass('show');
        $banner.addClass('show');
    }

    $(window).on('load', function() {
        setTimeout(function() {
            $banner.addClass('ready');
            $toggle.addClass('ready');
        }, 1000);
    });

    $('.sticky-banner-carousel').owlCarousel({
        items: 1,
        loop: 1,
        autoplay: 1,
        smartSpeed: 800,
        margin: 10,
        nav: 1,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        dots: false,
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<?php include 'include/popup-image.php'; ?>
<?php include '_footer.php'; ?>
