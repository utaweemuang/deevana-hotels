<div id="countdown">
    <div class="timer" data-countdown="2017/01/31 23:59:59">
    </div>

    <div class="content">
        <div id="countdown_slider" class="owl-carousel">
            <div class="item">
                <a href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=277&onlineId=4&pid=MDgxODE3" target="_blank">
                    <img class="force" src="modules/widget-countdown/images/superior-room-garden-wing-twin-bed.jpg" />
                    <div class="item-caption">HOLIDAY PACKAGES</div>
                </a>
            </div>
        </div>
        
        <div class="detail">
            <ul>
                <li><i class="icon fa fa-angle-right"></i> Free airport transfer</li>
                <li><i class="icon fa fa-angle-right"></i> Thai massage</li>
                <li><i class="icon fa fa-angle-right"></i> A set of drink in room</li>
                <li><i class="icon fa fa-angle-right"></i> Cash voucher 300 THB</li>
            </ul>
        </div>
    </div>
</div>

<script src="modules/widget-countdown/assets/moment/moment.js"></script>
<script src="modules/widget-countdown/assets/moment/moment-timezone-with-data.js"></script>
<script src="modules/widget-countdown/assets/jquery.countdown-2.1.0/jquery.countdown.min.js"></script>
<script>
    $(function() {
        var now = moment();
        var nowTZ = moment.tz(now, 'Asia/Bangkok');
        var $countdown = $('[data-countdown]');
        
        $countdown.each(function() {
            var $this = $(this);
            var time = $this.data('countdown');
            $this.countdown( time, function(event) {
                $this.html(event.strftime(
                    '<div class="time-group">'+
                    '<div class="box day">%D <span>Days</span></div>'+
                    '<div class="box hr">%H <span>Hours</span></div>'+
                    '<div class="box min">%M <span>Minutes</span></div>'+
                    '<div class="box sec">%S <span>Seconds</span></div>'+
                    '</div>'+
                    '<div class="caption"><span>Hurry!</span> Only %D Days Left</div>'
                ));
            });
        });
        
        $('#countdown_slider').owlCarousel({
            items: 1,
            autoplay: 5000,
            pullDrag: false,
            dots: false,
        })
    });
</script>

<style>
    #countdown {
        background-color: #fff;
        box-shadow: 0 0 3px rgba(0,0,0,.5);
        width: 100%;
    }
    #countdown .timer {
        background-color: #333438;
        color: #fff;
        padding: 0 10px;
        text-align: center;
    }
    #countdown .timer .time-group {
        padding: 10px 0;
    }
    #countdown .timer .box {
        display: inline-block;
        width: 50px;
        font-size: 26px;
        padding-right: 2px;
        position: relative;
        line-height: 1;
    }
    #countdown .timer .box:after {
        content: ':';
        position: absolute;
        top: 0;
        right: 0;
        width: 1px;
        font-size: 20px;
    }
    #countdown .timer .box:last-of-type:after {
        content: none;
        padding-right: 0;
    }
    #countdown .timer .box span {
        display: block;
        font-size: 8px;
        text-transform: uppercase;
        font-weight: 500;
        margin-top: 2px;
    }
    #countdown .caption {
        background-color: #03bcf4;
        margin: 0 -10px;
        padding: 3px 10px;
        font-size: 12px;
        text-transform: uppercase;
    }
    #countdown .caption span {
        font-weight: bolder;
        font-size: 1.2em;
    }
    #countdown .content .item {
        position: relative;
    }
    #countdown .content .item-caption {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        background-color: #222;
        background-color: rgba(0,0,0,.8);
        color: #fff;
        text-align: center;
        padding: 5px;
    }
    #countdown .detail ul {
        margin: 0;
        padding: 5px;;
        list-style: none;
    }
    #countdown .detail li {
        background-color: #e1e0de;
        margin-bottom: 5px;
        padding: 3px 8px;
        font-weight: 500;
    }
    #countdown .detail li:last-child {
        margin-bottom: 0;
    }
    #countdown .detail li .icon {
        font-weight: bold;
        margin-right: 3px;
    }
</style>