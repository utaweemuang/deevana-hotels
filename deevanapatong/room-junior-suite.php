<?php
$title = 'Junior Suite | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Junior Suite: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'junior suite, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-suite';
$cur_page = 'junior-suite';
$par_page = 'rooms';

$lang_en = 'room-junior-suite.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
              	    <img src="images/accommodations/junior/1500/junior-06.jpg" alt="Junior Suite 03" />
                    <img src="images/accommodations/junior/1500/junior-02.jpg" alt="Junior Suite 02" />
                    <img src="images/accommodations/junior/1500/junior-03.jpg" alt="Junior Suite 03" />
                    <img src="images/accommodations/junior/1500/junior-04.jpg" alt="Junior Suite 03" />
                    <img src="images/accommodations/junior/1500/junior-05.jpg" alt="Junior Suite 03" />
                    
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Junior Suite <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior/600/junior-06.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior/600/junior-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior/600/junior-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior/600/junior-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior/600/junior-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Junior Suite</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior/600/junior-06.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>The Junior Suites offer a comfortable space for groups and families to stay together. The 55 sqm suite has a king‐sized bed (with an option for two extra beds) The room features 7ft king‐sized beds, refrigerator, tea / coffee making facilities, bathroom with shower, coffee table and flat screen TV. Guests can also enjoy the pool view from balcony. And a living area with stylish furniture and classic Thai soft furnishings and artwork. The suite has a large bathroom with bathtub and separate shower with a delightful range of complimentary beauty products. The private balcony has a pleasant seating area to enjoy the views of the swimming pool.</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                        <div class="amenities">
                            <div class="shad-over">
                                <h2 class="title">Guest Room Amenities</h2>
                                <ul class="amenities-list">
                                <li>Air-conditioning with individual control</li>
                                <li>Bathroom amenities: Bathtub with shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                <li>Luggage rack</li>
                                <li>International direct dial telephone</li>
                                <li>Color TV satellite channel with international news</li>
                                <li class="more clickable">
                                    <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                </li>
                            </ul>

                                <div id="all_amenities" class="mfp-hide popup">
                                    <h2>All Amenities</h2>
                                    <ul class="list-columns-2">
                                    <li>Air-conditioning with individual control</li>
                                    <li>Bathroom amenities: Bathtub with shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                    <li>Luggage rack</li>
                                    <li>International direct dial telephone</li>
                                    <li>Color TV satellite channel with international news</li>
                                    <li>Refrigerator with complimentary drinking water: 3 bottles per day</li>
                                    <li>Private bathroom &amp; bathtub attached hot &amp; cold shower</li>
                                    <li>A living area</li>
                                    <li>Electricity 220 V.</li>
                                    <li>Pool View balcony (Deluxe Wing)</li>
                                    <li>Coffee/tea making facilities</li>
                                    <li>Hair dryer</li>
                                    <li>Safety Box</li>
                                    <li>Interconnecting room</li>
                                    <ul><b>Remarks:</b>
                                        <li>Beach Towels service at bell counter.</li>
                                        <li>Pool Towels service at Pool side.</li>
                                    </ul>
                                </ul>
                                </div>
                            </div>
                            
                            <span class="shad-left"></span>
                            <span class="shad-right"></span>
                        </div>
                    </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>