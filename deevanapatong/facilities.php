<?php
$title = 'Facilities | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'facilities, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanapatong/facilities.php';
$lang_th = '/th/deevanapatong/facilities.php';
$lang_zh = '/zh/deevanapatong/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <?php for($i = 1; $i <= 6; $i++) : ?>
            <?php $index = ($i<10) ? '0'.$i : $i; ?>
            <div class="item"><img src="images/facilities/facilities-slide-<?php echo $index; ?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <?php endfor; ?>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#restaurant" class="tab active">Restaurant</span>
                    <span data-tab="#swimming_pool" class="tab">Swimming Pool</span>
                    <span data-tab="#leisure_and_tours" class="tab">Leisure &amp; Tours</span>
                    <span data-tab="#mice_facilities" class="tab">MICE Facilities</span>
                    <span data-tab="#wedding" class="tab">Wedding</span>
                    <span data-tab="#cooking_class" class="tab">Cooking Class</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">Orientala Spa</span>
                </div>
                
                <div class="tabs-content">
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-01.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-02.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-03.jpg" alt="Restaurent" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Restaurant</h1>
                                    
                                    <h2 class="sub-title">Dalah Cuisine</h2>
                                    <p>Start your day with a great selection of Thai and continental breakfasts, with all the cooked international favourites, cereals, fruit, tea, coffee and much more from the tasty breakfast buffet cart at the Dalah Cuisine restaurant. The mellow décor, pool view and open air layout ensure the breezy restaurant offers a refreshing start to the day.<br/>
                                        Daily Breakfast 06:30 - 10:00 hrs.</p>
                                        <br/><br/><br/><br/><br/><br/><br/>
                                    
                                    <h2 class="sub-title">Balcony Thai Cuisine</h2> <b>Temporarily closed</b>
                                    <p>Located at the Spa Wing, the delightful Balcony restaurant is open for lunch and dinner offering lovely views over the tropical green gardens and swimming pools. The casual diner has an outdoor terrace feeling with an open wall allowing the fresh air to circulate. With a choice of soft comfy arm chairs or stylish set tables. The Balcony is a great setting to enjoy a cup of fresh coffee and homemade cake, or delicious evening meal.<br/>
                                        Open: 18:00 – 22:00 hrs</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">Deluxe Pool Bar</h2> <b>Temporarily closed</b>
                                    <p>Relax on your lounger or swim up to the Deluxe Pool Bar and sip on a refreshing fruit shake, fresh young coconut and selection of long cold drinks, with light meals and snacks also available.<br/>
                                        Open: 10:00 – 20:00 hrs.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool/pool-01.jpg" alt="Swimming Pool"/>
                                    <br/>
                                    <img class="force thumbnail" src="images/facilities/pool/pool-02.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Swimming Pool</h1>
                                    
                                    <h2 class="sub-title">Deluxe Pool</h2>
                                    <p>The aqua‐blue swimming pool at the Deluxe Wing is a focal point of the resort with all balconies facing the glistening water. The large rectangular pool sits alongside a shallow pool for children and features two charming elephant water fountains. There is plenty of space pool side for sun loungers and a convenient pool bar where guests can swim up and enjoy a refreshing cool drink.</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">Garden Pool</h2>
                                    <p>Surrounded by gorgeous greenery, the Garden Wing swimming pool is a relaxing place to spend the day. The recently refurbished garden pool has sloping steps making it easy to walk in for a swim. The pool is huge and has delightful features such as the Jacuzzi side seats and modern fountain. The children’s pool has a lovely water fountain that will keep little ones happily entertained.</p>
                                    <p>Open: 10:00 – 20:00 hrs.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="leisure_and_tours" class="article" data-tab-name="Leisure &amp; Tours">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tours/tours-01.jpg" alt="Leisure &amp; Tours" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Leisure &amp; Tours</h1>
                                    
                                    <p>There is so much to see and do in Phuket besides relaxing on the island’s glorious beaches. Explore some of the surrounding tropical islands that offer amazing snorkeling with a diverse underwater world. Visit some of the religious sites with famous temples like Big Buddha and Wat Chalong. Go back in time in the Old Town with historical buildings and museums. Take a ride on an elephant or see the island from one of the many beautiful view points. Here are some recommended attractions.</p>
                                    
                                    <h2 class="sub-title">Phuket Old Town</h2>
                                    <p>Going back around 100 years, Phuket was a thriving tin mining community with many Chinese settlers working in the industry. As the economy boomed, the construction of mansions and trading shop houses emerged built in Sino‐Portuguese style. The shops are narrow yet long with wooden doors and Chinese fretwork carving. Many of the mansions and shop houses in town have been preserved and restored and have become Phuket Old Town with quaint cafes, restaurants, galleries, boutiques and more. Take a stroll around Dibuk, Phang‐Nga, Yaowarat, Thaland and Krabi roads to see this delightful architecture. Other old European‐style buildings of note are the Provincial Hall (Sala Klang), the Phuket Courthouse (San Changwat), and Nakhon Luang Thai Bank.</p>
                                    
                                    <h2 class="sub-title">Chalong Temple</h2>
                                    <p>This well maintained temple is a revered place of worship for the people of Phuket and a popular temple for tourists to visit. Housed at the temple are the statues of Luang Pho Chaem and Luang Pho Chuang who helped the injured people of Phuket with their knowledge of herbal medicine during the tin miner’s rebellion in 1876.</p>
                                    
                                    <h2 class="sub-title">Karon Viewpoint</h2>
                                    <p>On the hill leading from Kata to Nai Harn is the Karon Viewpoint, with a purpose built pavilion that provides amazing views. From the shelter, visitors can sit and enjoy the glorious sight that looks out over Kata Noi, Kata and Karon Beaches, as well as the small Crab Island. Toilets and refreshments are available.</p>
                                    
                                    <h2 class="sub-title">Tour Counter</h2>
                                    <p>To resort, tour counter offers a full selection of excursions including Phuket Old Town, Phi Phi Island, James Bong Island, scuba diving, snorkeling, game fishing, safari and elephant trekking, private speed boat tours, the canoe experience, Phuket Fantasia and much more. Our tours staff have the local knowledge to help find the right tour for you which include:</p>
                                    <ul class="list-columns-3 custom-list-dashed">
                                        <li>Water Sports</li>
                                        <li>Mini Golf</li>
                                        <li>Golfing</li>
                                        <li>4 international courses</li>
                                        <li>Go Karting</li>
                                        <li>Rock Climbing</li>
                                        <li>Mountain Biking</li>
                                        <li>Bungy Jumping</li>
                                        <li>Eco Tours</li>
                                        <li>Jungle &amp; Elephant Trekking</li>
                                        <li>Rafting</li>
                                        <li>Island &amp; City Tour</li>
                                        <li>Horse Riding</li>
                                        <li>Shooting Range</li>
                                        <li>Phi Phi Island</li>
                                        <li>James Bond Island (Phang‐Nga Bay)</li>
                                        <li>Coral Island</li>
                                        <li>Raya Island</li>
                                        <li>Khai Island</li>
                                        <li>Similan Island</li>
                                        <li>Rang Yai Island</li>
                                        <li>Private Speedboat Tours</li>
                                        <li>Scuba Diving</li>
                                        <li>Snorkeling</li>
                                        <li>Game Fishing</li>
                                        <li>Cruising &amp; Sailin</li>
                                        <li>Sea Canoeing</li>
                                        <li>Junk Cruise</li>
                                    </ul>
                                    <p>and many more…</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="mice_facilities" class="article" data-tab-name="MICE Facilities">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                <img class="force thumbnail" src="images/facilities/mice/meeting-01.jpg" alt="MICE Facilities"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">MICE Facilities</h1>
                                    
                                    <h2 class="sub-title">Conferences &amp; Meetings</h2>
                                    <p>Deevana Patong Resort &amp; Spa provides the best facilities and services for events of a size. With the beautiful lobby, spacious gardens and professionally equipped Vanda Room, a wide variety of events and meetings can be hosted at the resort.</p>
                                    
                                    <h2 class="sub-title">Vanda Room</h2>
                                    <p>Vanda Conference Room is located on the ground floor, accessible through the lobby, from the Deluxe Wing and parking lot. 
</p>
                                    
                                    <ul class="list-description">
                                        <li><span class="label">Dimension:</span> 7.70 m &times; 14.40 m (110sqm)</li>
                                        <li><span class="label">Ceiling Height:</span> 2.60 m</li>
                                    </ul>
                                    
                                    <table style="margin: 0;" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Set up</th>
                                                <th>Theatre</th>
                                                <th>Classroom</th>
                                                <th>U-shape</th>
                                                <th>Board</th>
                                                <th>Buffet</th>
                                                <th>Cocktail</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <th>Persons</th>
                                                <td>60</td>
                                                <td>40</td>
                                                <td>20</td>
                                                <td>10</td>
                                                <td>50</td>
                                                <td>80</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <h2 class="sub-title"><u>Equipment and Facilities</u></h2>

                                    <h2 class="sub-title">Lighting</h2>
                                    <p>Lighting associated facilities/controls are available.  </p>
                                    
                                    <h2 class="sub-title">Audio – Visual Equipment</h2>
                                    <p>
                                        Table, free standing and wireless microphones<br>
                                        P.A. system with portable loudspeakers<br>
                                        Multi‐system TV<br>
                                        Laser disc player, DVD<br>
                                        LCD Projector available for rental.
                                    </p>
                                    
                                    <h2 class="sub-title">Conference Presentation</h2>
                                    <p>White boards, Flip chart, Projector screen.</p>
                                    
                                    <h2 class="sub-title">Wi‐Fi System</h2>
                                    <p>Internet access through the telephone line is available free of charge when connecting to server in Phuket<br>
                                        Internet access to other servers in other areas will be charged by telephone area</p>
                                    
                                    <h2 class="sub-title">Conference Service Personnel</h2>
                                    <p>
                                        Electricians<br>
                                        Audio‐Visual equipment operators<br>
                                        Senior staff and management are available to ensure the success of your functions
                                    </p>
                                    
                                    <h2 class="sub-title">Organiser’s Office</h2>
                                    <p>(On quotation): LCD, Laptop Computer, Printer, Photographer, Copying</p>
                                    
                                    <h2 class="sub-title">Team Building</h2>
                                    <p>Companies and organizations can take advantage of the superb meeting facilities and amenities at Deevana Resort &amp; Spa by taking part in a team building activity day. The professional event organizers at the resort can tailor make a fun day of unforgettable activities that promote team work and trust for improved working relationships. The Vanda Conference Centre with its high tech facilities and the resort grounds are the ideal setting for a successful team building day. For more information, please contact our events team at <a href="mailto:sales.clusterphuket@deevanahotels.com"> sales.clusterphuket@deevanahotels.com</a></p>
                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="wedding" class="article" data-tab-name="Wedding">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/wedding/wedding-01.jpg" alt="wedding"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Wedding</h1>
                                    <p>The experienced wedding planners at Deevana Resort &amp; Spa can create a wonderful day for the happy couple and guests to cherish forever. The picturesque and spacious resort grounds provide the perfect backdrop for a special wedding with chic dining and reception areas that can cater to small or large wedding parties. The team at Deevana can organize a wedding of choice whether it be a Thai ceremony including a monk’s blessing or a western style wedding in the presence of family and friends. For more information, please contact our team at <a href="mailto:sales.clusterphuket@deevanahotels.com"> sales.clusterphuket@deevanahotels.com</a></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="cooking_class" class="article" data-tab-name="Thai Cooking Class">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/cooking_class/cooking_class-01.jpg" alt="Thai Cooking Class"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Thai Cooking Class</h1> <b>Temporarily closed</b>
                                    <p>Learn the secrets of delicious Thai cuisine with a cooking class by the professional chef who has over 10‐year experience. Take away the skills, knowledge and recipes of how to cook some popular Thai dishes, and enjoy your culinary efforts by sampling your creations at the end of the class for lunch or dinner.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/spa/spa-01.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Discover a serene spa sanctuary amongst the tropical gardens of Deevana Patong Resort & Spa situated at the heart of Patong Beach. The hideaway Patong spa resort offers guests an escape from the hustle and bustle of the city to relax and relieve stress.</p>
                                    <p>The luxury treatment rooms have elegant Thai design style with traditional fabrics and modern furnishings and are ideal for couples with rain showers, bubbling Jacuzzi tubs, steam rooms and treatment beds for two. Our caring and professional therapists are ready to guide you through the health and well-being</p>
                                <h2 class="sub-header">Orientala Spa Facilities</h2>
                                <ul>
                                    <li>5 Treatment rooms (3 single and 2 twin rooms with Jacuzzi)</li>
                                    <li>Ladies and Gents private lockers and steamed bath</li>
                                    <!-- <li>2 Facial treatment stations</li> -->
                                    <li>Orientala Wellness Spa Menu (<a style="color:#ff0000; font-weight:bold" target="_blank" href="http://www.deevanapatong.com/download/Spa-Menu.pdf">Download</a>)</li>
                                  </ul><a style="margin-bottom: 2rem;display: block;" href="mailto:info.clusterphuket@deevanahotels.com" target="_blank" rel="noopener">For more information please contact</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>