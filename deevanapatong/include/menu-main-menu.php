<ul class="menu list-menu level-0">
    <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">Accommodation</a>
        <ul class="sub-menu level-1">
            <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a href="#">Garden Wing</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('superior-garden'); ?>"><a href="room-superior-garden.php">Superior Garden Room</a></li>
                </ul>
            </li>
            <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a href="#">Deluxe Wing</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('deluxe'); ?>"><a href="room-deluxe.php">Deluxe Room</a></li>
                    <li class="<?php echo get_current_class('deluxe-with-jacuzzi'); ?>"><a href="room-deluxe-with-jacuzzi.php">Deluxe with Jacuzzi Room</a></li>
                    <li class="<?php echo get_current_class('junior-suite'); ?>"><a href="room-junior-suite.php">Junior Suite</a></li>
                    <li class="<?php echo get_current_class('junior-with-jacuzzi'); ?>"><a href="room-junior-with-jacuzzi.php">Junior Suite with Jacuzzi</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('spa'); ?>"><a href="facilities.php#orientala_wellness_spa">Spa</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">Facilities</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">Attractions</a></li>
    <li class="<?php echo get_current_class('offers'); ?>"><a href="offers.php">Offers</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">Gallery</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>