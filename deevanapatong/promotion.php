<?php
$title = 'Promotion | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Promotion: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'promotion, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$body_class = 'promotion';
$cur_page = 'promotion';

$lang_en = 'promotion.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>
        
<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section">
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url( get_info('ibeID'), 'en', 'MDgxNDIz'); ?>"><img class="force" src="images/promotion/20160524/thai_residents_offer.jpg" alt="Super Save Package Minimum 2 Nights" /></a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">Super Save Package Minimum 2 Nights</h1>
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>2 nights in Superior Garden Room</li>
                                <li>Daily breakfast</li>
                                <li>Free welcome drink</li>
                                <li>Free Fruit Plate in room</li>
                                <li>Free cash voucher 300 THB</li>
                                <li>Free drinks in room on arrival date (2 beer &amp; 2 soft drink)</li>
                                <li>Free late check out 16:00hrs</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes are included<br/>
                                    <span class="cost"><span class="discount">THB 3,400</span> per night</span><br/>
                                    <strong>please stay at least <span style="color:#BED63E;">2 nights</span></strong>
                                </h3>
                                <ul>
                                    <li><span>Room type:</span> Superior Garden Room</li>
                                    <li><span>Period:</span> Stay before 31 Oct 2016</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url( get_info('ibeID'), 'en', 'MDgxNDIz'); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url(get_info('ibeID'), 'en', 'MDc2MTg1'); ?>" target="_blank"><img class="force" src="images/promotion/20160524/thai_residents_offer.jpg" alt="Book Ahead Save More" /></a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">Book Ahead Save More</h1>
                            <h2 class="list-heading">You will get...</h2>
                            <ul>
                                <li>1 night in Superior Garden Room</li>
                                <li>Daily breakfast</li>
                                <li>wifi</li>
                                <li>Free late check-out until 16:00hrs</li>
                                <li>Free welcome drink</li>
                                <li>Free fruit plate on arrival date</li>
                                <li>Free cash voucher 300 THB per stay</li>
                                <li>Free one way transfer from airport to hotel for minimum stay 3 nights</li>
                                <li>Free round trip airport transfer from/to hotel for minimum stay 5 nights</li>
                                <li>Free one dinner (buffet or set dinner, exclude beverage) for minimum stay 7 nights</li>
                                <li>Free daily drinks in room (2 local beers &amp; 2 soft drinks)</li>
                                <li>Noted** Free airport transfer available time from 06:00am to 09:00pm. Additional charge at 350 THB per way between 09:00pm to 06:00am.</li>
                            </ul>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>All taxes are included<br/>
                                    <span class="cost"><span class="discount">THB 1,811</span> per night</span><br>
									<small>42% DISCOUNT</small>
                                </h3>
                                <ul>
                                    <li><span>Room type:</span> Superior Garden Room</li>
                                    <li><span>Period:</span> Stay from 01 Jun - 31 Oct 2016</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url(get_info('ibeID'), 'en', 'MDc2MTg1'); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>
			
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<style>
    .site-main {
        background-image: url(images/promotion/bg-promotion.jpg);
        background-attachment: fixed;
        background-size: cover;
    }
    .article {
        background-color: #e1e1e1;
        margin-bottom: 30px;
        padding: 20px;
        color: #666;
        min-height: 190px;
    }
    .article .title {
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        font-weight: 700;
        color: #adc32b;
        margin-bottom: 15px;
    }
    .article .list-heading {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .article .list-heading + ul {
        font-size: 12px;
        margin-top: 3px;
    }
    .article .booking {
        background-color: #666;
        color: #cbcbcb;
        border-radius: 6px;
        padding: 12px;
        font-size: 12px;
    }
    .article .booking:after {
        content: '';
        display: block;
        clear: both;
    }
    .article .booking h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 12px;
    }
    .article .booking .discount {
        font-size: 22px;
        font-weight: 700;
        color: #bed62f;
    }
    .article .booking ul {
        list-style: none;
        margin-top: 5px;
        padding-left: 0;
    }
    .article .booking li {
        border-bottom: 1px dotted #cbcbcb;
        padding: 5px 0;
    }
    .article .booking .button {
        background-color: #8fc31c;
        background-image: linear-gradient(to bottom, #c0f844,#8fc31c);
        color: #fff;
        padding: 0 10px;
        line-height: 2;
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 14px;
        font-weight: 700;
    }
    @media (max-width: 720px) {
        .col-thumbnail,
        .col-info {
            width: 50%;
        }
        .col-booking {
            width: 100%;
        }
        .article .booking {
            margin-top: 20px;
            border-radius: 0;
        }
    }
    @media (max-width: 480px) {
        .col-thumbnail,
        .col-info {
            width: 100%;
        }
        .col-thumbnail {
            margin-bottom: 20px;
        }
        .col-booking .booking {
            border-radius: 0;
        }
        .article {
            padding: 12px;
        }
        .article .title {
            font-size: 24px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>