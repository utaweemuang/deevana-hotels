<?php
$title = 'Deluxe Room | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deluxe room, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe';
$cur_page = 'deluxe';
$par_page = 'rooms';

$lang_en = 'room-deluxe.php';
$lang_th = '#';
$lang_zh = '#';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                	<img src="images/accommodations/deluxe/1500/main-pic-01.jpg" alt="Deluxe Room" />
                    <img src="images/accommodations/deluxe/1500/main-pic-02.jpg" alt="Deluxe Room" />
                    <img src="images/accommodations/deluxe/1500/main-pic-03.jpg" alt="Deluxe Room" />
                    <img src="images/accommodations/deluxe/1500/main-pic-04.jpg" alt="Deluxe Room" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/main-pic-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/main-pic-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/main-pic-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/main-pic-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                   
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/main-pic-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Suitable for couples or friends sharing, the 36‐sqm Deluxe Room is decorated with contemporary Asian influence and has a choice of double or twin beds. The room also provides a coffee table, flat screen TV with international channels and an en‐suite bathroom with a large sliding window. The private balcony offers great views over the lovely swimming pool.</p>
                            <p class="note">
                                <strong>Remarks:</strong><br>
                                Beach Towels service at bell counter.<br>
                                Pool Towels service at Pool side.
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Guest Room Amenities</h2>
                                    <ul class="amenities-list">
                                        <li>Air-conditioning with individual control</li>
                                        <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                        <li>Luggage rack</li>
                                        <li>International direct dial telephone</li>
                                        <li>Color TV satellite channel with international news</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Air-conditioning with individual control</li>
                                            <li>Bathroom amenities: Shower, shampoo, soap, shower cap, towels, sewing kit, cotton buds, slippers, umbrella / sandal</li>
                                            <li>Luggage rack</li>
                                            <li>International direct dial telephone</li>
                                            <li>Color TV satellite channel with international news</li>
                                            <li>Refrigerator with complimentary drinking water: 3 bottles per day</li>
                                            <li>Private bathroom with hot & cold shower</li>
                                            <li>Electricity 220 V.</li>
                                            <li>Pool View balcony (Deluxe Wing)</li>
                                            <li>Coffee/tea making facilities</li>
                                            <li>Hair dryer</li>
                                            <li>Safety Box</li>
                                            <li>Interconnecting room</li>
                                            <ul><b>Remarks:</b>
                                                <li>Beach Towels service at bell counter.</li>
                                                <li>Pool Towels service at Pool side.</li>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>