<?php
$title = 'Promotion | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Promotion: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'promotion, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$body_class = 'promotion';
$cur_page = 'promotion';

$lang_en = '/deevanaplazakrabi/promotion.php';
$lang_th = '/th/deevanaplazakrabi/promotion.php';
$lang_zh = '/zh/deevanaplazakrabi/promotion.php';

include_once('_header.php');
?>
        
<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section">
                
                <article class="article">
                    <div class="row row-promotion">
                        <div class="col-w4 col-thumbnail">
                            <div class="thumbnail">
                                <a href="<?php ibe_url('276', 'en'); ?>" target="_blank"><img class="force" src="images/promotion/promotion_banner.jpg" alt="Early Bird" /></a>
                            </div>
                        </div>

                        <div class="col-w4 col-info">
                            <h1 class="title">EARLY BIRD (Inclusive Breakfast)</h1>
                            <h2 class="list-heading">Inclusion</h2>
                            <ul>
                                <li>Inclusive Breakfast</li>
                                <li>Book 90 days in advance</li>
                            </ul>
                            
                            <p class="note">
                                Non refundable
                            </p>
                        </div>

                        <div class="col-w4 col-booking">
                            <div class="booking">
                                <h3>Limited rooms available!<br>
                                    <span class="cost"><span class="discount">30% Discount</span> per night</span>
                                </h3>
                                <ul>
                                    <li><span>Room type:</span> All Room Type</li>
                                    <li><span>Period:</span> Now - 30 Oct 2017</li>
                                </ul>
                                <a class="button clickable right" href="<?php ibe_url('276', 'en'); ?>" target="_blank">BOOK NOW!!</a>
                            </div>
                        </div>
                    </div>
                </article>

            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<style>
    .site-main {
        background-image: url(images/promotion/bg-promotion.jpg);
        background-attachment: fixed;
        background-size: cover;
    }
    .article {
        background-color: #e1e1e1;
        margin-bottom: 30px;
        padding: 20px;
        color: #666;
        min-height: 190px;
    }
    .article .title {
        font-family: 'Roboto', sans-serif;
        font-size: 16px;
        font-weight: 700;
        color: #adc32b;
        margin-bottom: 15px;
    }
    .article .list-heading {
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
    .article .list-heading + ul {
        font-size: 12px;
        margin-top: 3px;
    }
    .article .col-info p.note {
        border: 2px solid salmon;
        padding: 5px 10px;
        color: salmon;
        background-color: #f5f5f5;
        border-radius: 2px;
        font-size: 12px;
    }
    .article .booking {
        background-color: #666;
        color: #cbcbcb;
        border-radius: 6px;
        padding: 12px;
        font-size: 12px;
    }
    .article .booking:after {
        content: '';
        display: block;
        clear: both;
    }
    .article .booking h3 {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 12px;
    }
    .article .booking .discount {
        font-size: 22px;
        font-weight: 700;
        color: #bed62f;
    }
    .article .booking ul {
        list-style: none;
        margin-top: 5px;
        padding-left: 0;
    }
    .article .booking li {
        border-bottom: 1px dotted #cbcbcb;
        padding: 5px 0;
    }
    .article .booking .button {
        background-color: #8fc31c;
        background-image: linear-gradient(to bottom, #c0f844,#8fc31c);
        color: #fff;
        padding: 0 10px;
        line-height: 2;
        font-family: 'Open Sans Condensed', sans-serif;
        font-size: 14px;
        font-weight: 700;
    }
    @media (max-width: 720px) {
        .col-thumbnail,
        .col-info {
            width: 50%;
        }
        .col-booking {
            width: 100%;
        }
        .article .booking {
            margin-top: 20px;
            border-radius: 0;
        }
    }
    @media (max-width: 480px) {
        .col-thumbnail,
        .col-info {
            width: 100%;
        }
        .col-thumbnail {
            margin-bottom: 20px;
        }
        .col-booking .booking {
            border-radius: 0;
        }
        .article {
            padding: 12px;
        }
        .article .title {
            font-size: 24px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>