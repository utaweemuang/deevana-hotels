<ul class="menu list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="<?php echo get_info('url'); ?>">Home</a></li>
    <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">Accommodation</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">Deluxe Room</a></li>
            <li class="<?php echo get_current_class('premier-room'); ?>"><a href="room-premier-room.php">Premier Room</a></li>
            <li class="<?php echo get_current_class('premier-pool-access'); ?>"><a href="room-premier-pool-access.php">Premier Pool Access</a></li>
            <li class="<?php echo get_current_class('family-room'); ?>"><a href="room-family-room.php">Family Room</a></li>
            <li class="<?php echo get_current_class('deluxe-suite'); ?>"><a href="room-deluxe-suite.php">Deluxe Suite</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">Facilities</a></li>
    <li class="<?php echo get_current_class('meetings'); ?>"><a href="meetings-and-event.php">Meetings &amp; Event</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">Attractions</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('276', 'en'); ?>" target="_blank">Promotion</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">Gallery</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>