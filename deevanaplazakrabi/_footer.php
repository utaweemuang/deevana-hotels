        <footer class="site-footer">
            <style>
                .deevana_group2 { width: 100%; background-color: #c2c2c2; -moz-box-shadow: inset 0 0 40px #686868; -webkit-box-shadow: inset 0 0 40px #686868; box-shadow: inset 0 0 40px #686868; text-align: center; padding-top: 50px; padding-bottom: 50px; }
                .logo_our_1 { width: 100%;max-width: 980px; margin: 0 auto;height: 100px; text-align:left; vertical-align:middle }
                .logo_our_2 { width: 100%;max-width: 980px; margin: 0 auto;height: 100px; text-align:left; vertical-align:middle }

                .ft-bg-1 {width:75%;display: inline-block; background-color:#fff; padding: 12px 0% 9px 2.5%;}
                .ft-bg-2 {width:75%;display: inline-block; background-color:#fff; padding: 11px 0% 9px 2.5%;}

                /* .logo_main_ft {-webkit-filter: brightness(3000%);filter: brightness(3000%);} */
                .logo_main_ft:hover {-webkit-filter: brightness(100%);filter: brightness(100%);}

                .logo_main_devana {width:20%;display: block;  height: 90px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo_ft_deevana.png) no-repeat center center;background-size: contain; display: inline-block; vertical-align: middle;}
                .logo_main_spa {width:20%;display: block;  height: 83px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo_ft_spa.png) no-repeat center center;background-size: contain; display: inline-block; vertical-align: middle;}

                .logo-new-deevana-patong-resort-spa { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-patong-resort-spa-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-deevana-patong-resort-spa:hover{ -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-deevana-plaza-phuket { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-plaza-phuket-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-deevana-plaza-phuket:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-deevana-plaza-krabi { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-deevana-plaza-krabi-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-deevana-plaza-krabi:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-emeral { display: inline-block; width: 10%; height: 64px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/deevanakrabi.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-emeral:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-recenta { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-recenta.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-recenta:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-recenta2 { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-recenta2.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-recenta2:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-recenta3 { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanahotels.com/images/our_brands/recenta_express_phuket_town_sm.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-recenta3:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-ramada { display: inline-block; width: 9%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-ramada-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-ramada:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-orientala-spa { display: inline-block; width: 14%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-orientala-spa-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-orientala-spa:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}
                .logo-new-orientala-wellness { display: inline-block; width: 11%; height: 60px; background: url(http://www.deevanaplazaphuket.com/wp-content/themes/deevanaplazaphuket/images/logo-orientala-wellness-active.png) no-repeat center center;background-size: contain;vertical-align: middle; margin-right: 0.5%; }
                .logo-new-orientala-wellness:hover { -webkit-filter:grayscale(0%);filter:grayscale(0%);}

                .cdsROW {
                    background-color: #fff;
                    font-family: Arial,Verdana,"Bitstream Vera Sans",Helvetica,sans-serif;
                    width: 500px;
                    font-size: 14px;
                    height: auto;
                    margin: 0;
                    padding: 0;
                    position: relative;
                    border: none;
                    text-decoration: none;
                    outline: none;
                }

                .cdsROW.shadow { -moz-box-shadow: 0px 1px 4px 0px #999; -webkit-box-shadow: 0px 1px 4px 0px #999; box-shadow: 0px 1px 4px 0px #999; }
                .cdsROW.border { border: 1px solid #589446; }
                .cdsROW.gray { background-color: #f2f2f2; }

                .cdsROW a, .cdsROW a:hover { text-decoration: underline; }
                .cdsROW img { border-style: none; }
                .cdsROW .cdsRating span { vertical-align: middle; white-space: nowrap; }

                .cdsROW .cdsLocName a { font-weight: bold; color: #000; }
                .cdsROW .cdsRating { font-size: 11px; color: #898989; }
                .cdsROW .cdsRating img { max-width: 62px; width: 62px; height: auto; vertical-align: middle; }
                .cdsROW .logo img { max-width: 115px; width: 115px; height: auto; }

                /* narrow version*/
                .cdsROW.narrow { width: 500px; text-align: center; padding: 7px 12px 1px; }
                .cdsROW.narrow.border { padding: 6px 11px 0px; width: 500px; }
                .cdsROW.narrow .cdsLocName { padding-bottom: 10px; line-height: 100%; }
                .cdsROW.narrow .cdsRating { line-height: 100%; }
                .cdsROW.narrow .logo { padding-top: 3px; padding-bottom: 3px; }

                /* wide version*/
                .cdsROW.wide { width: 600px; height: 47px; }
                .cdsROW.wide .cdsROWContainer { display: inline-table; *display: inline; width: 600px; height: 47px; text-align: center; line-height: 0; }
                .cdsROW.wide .cdsROWContainer .cdsComponent { display: table-cell; height: 100%; vertical-align: middle; }
                .cdsROW.wide .logo { border-right: 1px solid #91b155; padding-left: 12px; padding-right: 12px; }
                .cdsROW.wide .cdsLocName { text-align: center; padding: 0px 7px 0px 9px; }
                .cdsROW.wide .cdsLocName { max-width: 145px; }
                .cdsROW.wide .cdsLocName a { line-height: 100%; }
                .cdsROW.wide .cdsRating { padding-right: 12px; white-space: nowrap; }
                .brands .logo img { vertical-align: top; }
            </style>

            <div class="container" style="overflow: auto;">
                <div id="TA_cdsratingsonlynarrow85" class="TA_cdsratingsonlynarrow" style="width: 500px;display: block;margin: auto;padding-top: 20px; padding-bottom: 20px;">
                    <ul id="NiS0NeVnW" class="TA_links 7h8Fzn">
                        <li id="OkhjForCezlY" class="VXjcEIj1Z6Ra">
                            <a target="_blank" href="https://www.tripadvisor.co.uk/"><img src="https://www.tripadvisor.co.uk/img/cdsi/img2/branding/tripadvisor_logo_transp_340x80-18034-2.png" alt="TripAdvisor"/></a>
                        </li>
                    </ul>
                </div>
            </div>

            <script src="https://www.jscache.com/wejs?wtype=cdsratingsonlynarrow&amp;uniq=85&amp;locationId=2293011&amp;lang=en_UK&amp;border=true&amp;shadow=true&amp;display_version=2"></script> 

            <div class="deevana_group2">
                <div class="logo_our_1">
                    <a href="http://www.deevanahotels.com/" target="_blank" class="logo_main_devana logo_main_ft"></a>
                    <div class="ft-bg-1">
                        <a href="http://www.deevanahotels.com/deevanaplazaphuket" target="_blank" class="logo-new-deevana-plaza-phuket"></a>
                        <a href="http://www.deevanahotels.com/deevanaplazakrabi" target="_blank" class="logo-new-deevana-plaza-krabi"></a>
                        <a href="http://www.deevanahotels.com/ramadaphuketdeevana" target="_blank" class="logo-new-ramada"></a>
                        <a href="http://www.deevanahotels.com/deevanapatong" target="_blank" class="logo-new-deevana-patong-resort-spa"></a>
                        <a href="http://www.deevanahotels.com/deevanakrabiresort" target="_blank" class="logo-new-emeral"></a>
                        <a href="http://www.deevanahotels.com/recentasuitephuket" target="_blank" class="logo-new-recenta"></a>
                        <a href="http://www.deevanahotels.com/recentaphuket" target="_blank" class="logo-new-recenta2"></a>
                        <a href="http://www.deevanahotels.com/recentastyle" target="_blank" class="logo-new-recenta3"></a>
                    </div>
                </div>
                <div class="logo_our_2">
                    <a href="http://www.orientalaspa.com/" target="_blank" class="logo_main_spa logo_main_ft"></a>
                    <div class="ft-bg-2">
                        <!-- <a href="http://www.orientalaspa.com/orientala-wellness-spa/" target="_blank" class="logo-new-orientala-wellness"></a> -->
                        <a href="https://www.orientalaspa.com" target="_blank" class="logo-new-orientala-spa"></a>
                    </div>
                </div>
            </div>

            <div class="footer-2 connect clearfix">
                <div class="container">
                    <div class="social left">
                        <span class="label">Let's be friend</span>
                        <span class="facebook"><a href="<?php echo get_info('facebook'); ?>" target="_blank"><i class="icon fa fa-facebook"></i></a></span>
                        <span class="youtube"><a href="<?php echo get_info('youtube'); ?>" target="_blank"><i class="icon fa fa-youtube"></i></a></span>
                    </div>

                    <div class="contact right">
                        <span><a href="contact.php">Contact</a></span>
                        <span><a href="https://reservation.travelanium.net/propertyibe2/booking-management?propertyId=276&onlineId=5" target="_blank">Manage Reservation</a></span>
                    </div>
                </div>
            </div>

            <div class="footer-3 colophon">
                <div class="container">
                    <div class="address">
                        <address class="address-info">
                            186 Moo 3, Aonang Soi 8, Aonang Beach, Aonang, Muang, Krabi 81180 Thailand<br>
                            Phone: +66 (0) 7563 9999 | Fax: +66 (0) 7563 9911 | Email: <a href="mainto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a>
                        </address>
                    </div>

                    <div class="license">
                        Hotel Web Design by <a href="http://www.travelanium.com/" target="_blank">Travelanium</a>
                    </div>
                </div>
            </div>
        </footer>

        <?php include 'include/side-social.php'; ?>
        <?php include 'include/navigation-offside.php'; ?>
    </div>



    <!--
    # Google Code for Remarketing Tag
    Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories.
    See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
    -->
    <script type="text/javascript">
        var google_conversion_id = 877663496;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/877663496/?value=0&amp;guid=ON&amp;script=0"/></div></noscript>
</body>

</html>
