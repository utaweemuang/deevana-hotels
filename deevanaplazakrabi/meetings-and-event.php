<?php
$title = 'Meetings and Event | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'meetings and event, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazakrabi/meetings-and-event.php';
$lang_th = '/th/deevanaplazakrabi/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazakrabi/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">MEETINGS &amp; EVENT</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#hornbill_grand_ballroom" class="tab active">Hornbill Grand Ballroom</span>
                    <span data-tab="#hornbill_ballroom_1" class="tab">Hornbill Ballroom 1</span>
                    <span data-tab="#hornbill_ballroom_2" class="tab">Hornbill Ballroom 2</span>
                    <span data-tab="#broadbill_meeting_room" class="tab">Broadbill Meeting Room</span>
                    <span data-tab="#pita_biz_meeting_room" class="tab">Pitta Biz Meeting Room</span>
                    <span data-tab="#meeting_room_dimension" class="tab">Meeting Room Dimension</span>
                </div>
                
                <div class="tabs-content">
                    <article id="hornbill_grand_ballroom" class="article" data-tab-name="Hornbill Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_grand_ballroom.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Hornbill Grand Ballroom</h1>
                                    <p>The resort’s 320-square-metre Hornbill Grand Ballroom, Southern Thai Contemporary decoration is suitable for up to 300 guests seated in a theatre-style meeting layout, a 300-guest cocktail reception, a 180-seat banquet, or a 150-person dinner and dance.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_1" class="article" data-tab-name="Hornbill Ballroom 1">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_1.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Hornbill Ballroom 1</h1>
                                    <p>The Hornbill Ballroom 1 provides 160-square-metres of space suitable for up to 150 guests seated in a theatre-style meeting layout, a 150-guest cocktail reception, a 90-seat banquet, or a 70-person dinner and dance.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_2" class="article" data-tab-name="Hornbill Ballroom 2">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_2.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Hornbill Ballroom 2</h1>
                                    <p>The Hornbill Ballroom 2 provides 160-square-metres of space suitable for up to 150 guests seated in a theatre-style meeting layout, a 50-guest cocktail reception, a 90-seat banquet, or a 70-person dinner and dance.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="broadbill_meeting_room" class="article" data-tab-name="Broadbill Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_broadbill_meeting_room.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Broadbill Meeting Room</h1>
                                    <p>The Broadbill Meeting Room provides 56-sqaure-metres of space suitable for up to 40 guests seated in a theatre-style meeting layout, a 50-guest cocktail reception, a 30-seat banquet, or a 35-person dinner and dance.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pita_biz_meeting_room" class="article" data-tab-name="Pita Biz Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_pitta_biz.jpg" />
                                </div>                            
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Pitta Biz Meeting Room</h1>
                                    <p>Biz meeting room provides for executive meeting for upto 15 guests and day light also available to make your event success with relax.</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="meeting_room_dimension" class="article" data-tab-name="Meeting Room Dimension">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Meeting Room Dimension</h1>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>Conference</th>
                                                <th>Area (sqm)</th>
                                                <th>Celling Height (m)</th>
                                                <th data-hide="phone, tablet">Table U-Shape</th>
                                                <th data-hide="phone, tablet">Style Rectangle</th>
                                                <th data-hide="phone, tablet">Style Theatre</th>
                                                <th data-hide="phone, tablet">Style Classroom</th>
                                                <th data-hide="phone, tablet">Banquet</th>
                                                <th data-hide="phone, tablet">Dinner &amp; Dance</th>
                                                <th data-hide="phone, tablet">Cocktail</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>Hornbill Grand Ballroom</td>
                                                <td>320</td>
                                                <td>3.2</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>180</td>
                                                <td>200</td>
                                                <td>150</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Hornbill Ballroom 1</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Hornbill Ballroom 2</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Broadbill Meeting Room</td>
                                                <td>56</td>
                                                <td>2.8</td>
                                                <td>15</td>
                                                <td>-</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>-</td>
                                                <td>35</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>Pitta Biz Room</td>
                                                <td>24</td>
                                                <td>2.8</td>
                                                <td>12</td>
                                                <td>-</td>
                                                <td>18</td>
                                                <td>-</td>
                                                <td>20</td>
                                                <td>-</td>
                                                <td>20</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                
                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
        
        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    #pita_biz_meeting_room {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>