<?php
$title = 'Family Room | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Family Room: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'family room, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-family-room';
$cur_page = 'family-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-family-room.php';
$lang_th = '/th/deevanaplazakrabi/room-family-room.php';
$lang_zh = '/zh/deevanaplazakrabi/room-family-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/family/1500/family-01.jpg" alt="Family Room 01" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 02" />
                    <img src="images/accommodations/family/1500/family-03.jpg" alt="Family Room 03" />
                    <img src="images/accommodations/family/1500/family-04.jpg" alt="Family Room 04" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Family Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/family/600/family-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-04.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Family Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/family/600/family-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>The 46-square-metre Family Rooms benefit from extra space and one king size bed plus one bunk bed for two children. Guests enjoy a private balcony, all standard comforts and amenities, a large bathroom includes a refreshing rain shower and deluxe branded toiletries. Other features a bedside reading lamp, one 32” LCD TV with satellite channels and a DVD player plus one 26”LCD TV in the kid’s corner with satellite channels and a PlayStation, a selection of children’s toys, IDD telephones with voicemail,  Free Wi-Fi connection, coffee and tea making facilities, mini bar and safety deposit box and iron and Ironing board. Connecting to Deluxe or Premier room is also available.</p>
                            <p>Rooms available: 11</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                     <!--   <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 46 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Pool view</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>