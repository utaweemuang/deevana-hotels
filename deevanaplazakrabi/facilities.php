<?php
$title = 'Facilities | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'facilities, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazakrabi/facilities.php';
$lang_th = '/th/deevanaplazakrabi/facilities.php';
$lang_zh = '/zh/deevanaplazakrabi/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/facilities-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#Orientala_spa" class="tab active">Orientala Spa</span>
                    <span data-tab="#argus_fitness" class="tab">Argus Fitness</span>
                    <span data-tab="#myna_kids_club" class="tab">Myna Kid's Club</span>
                    <span data-tab="#swimming_pool" class="tab">Swimming Pool</span>
                    <span data-tab="#restaurant" class="tab">Restaurant & Bars</span>
                    <span data-tab="#pigeon_library_and_internet_corner" class="tab">Pigeon Library &amp; Internet Corner</span>
                </div>
                
                <div class="tabs-content">
                    <article id="Orientala_spa" class="article" data-tab-name="Orientala Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/orientala_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Elegant and luxurious, Orientala Spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and herbal treatments. The perfect place to find respite, Orientala Spa features a private twin room with Jacuzzi and steam room, 2 private single rooms, and  special set up in open-air space for foot reflexology and authentic Thai massage.</p>
                                    <p>Orientala Spa offers an extensive menu of rejuvenating massage and body services, each created to aid relaxation and improve wellbeing. The spa’s skilled therapists have taken the best indigenous knowledge and combined it with leading contemporary practice to create a superior wellness experience. Guests can enjoy the exotic and relaxing qualities of Thai herbs oils and other superior wellness products used in individual treatments or special spa packages.</p>
                                    <p><span style="color: #516819;">Orientala Spa ensures a truly indulgent and memorable spa journey daily : from 9 am. – 9 pm.<br> Location: Lobby level of the Reception Building</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="argus_fitness" class="article" data-tab-name="Argus Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_argus_fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Argus Fitness</h1>
                                    <p>Guests wishing to keep in shape during their holiday at Deevana Plaza Krabi Aonang can use the well- equipped fitness center at  free of charge. Joggers can also enjoy an early morning run along the beautiful Noppharat Thara Beach.</p>
                                    <p><span style="color: #516819;">Argus Fitness Centre is open daily : from 7 am. – 9 pm.<br> Location : Ground floor of Building 1</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="myna_kids_club" class="article" data-tab-name="Myna Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_myna_kid's_club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Myna Kid's Club</h1>
                                    
                                    <p>Deevana Plaza Krabi Aonang invites youngsters to the colorful and creative Myna Kid’s Club. Children from 4 to 12 years old can enjoy a wide range of fun‐filled activities under the watchful eye of the resort’s caring and well trained staff. There’s also a TV, dolls and toys to keep them entertained.</p>
                                    <p>Children can also have fun in the special children’s swimming pool and in an outdoor playground while mum and dad take advantage of some quality time together.</p>
                                    <p><span style="color:#516819">Myna Kid’s Club is open daily : from 9 am. – 6 pm. <br>
                                        Location : Ground floor of Building 1 ( Next to Argus Fitness)</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swimming_pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Swimming Pool</h1>
                                    <p>At the heart of Deevana Plaza Krabi Aonang is the large  lagoon-style swimming pools, complete with a Jacuzzi pool. 3 – large swimming pools and one comfortable  kid’s pool allow guests in the ground floor Premier Pool Access rooms to slip into the warm waters directly from their private terrace and welcome all other resorts guest to enjoy from morning to night time.
                                        With broad shade giving parasols and comfortable sun loungers at the poolside, it’s the perfect spot for a refreshing dip and relaxation throughout the day and early evening. Refreshments and delicious light meals are also available from the Sun Bird Pool Bar.</p>
                                    <p><span style="color: #516819;">The swimming pool is open daily : from 7 am. – 9 pm.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
<article id="restaurant" class="article" data-tab-name="Restaurant & Bars">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_kingfisher_restaurant.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_sunbird_pool_bar.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_starling_lobby_lounge.jpg?ver=20160617" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Kingfisher Restaurant</h1>
                                    <p>Kingfisher is Deevana Plaza Krabi Aonang’s signature restaurant. Here, guests can dine in style and discover culinary tastes and traditions from around the world. Diners can enjoy an uplifting start to the day with a delicious buffet breakfast of Asian and international specialties served in the open-sided or alfresco on the terrace. Extensive lunch and dinner menus offer an enticing selection of Thai and international favorites. Kingfisher’s delectable cuisine and attentive service ensure a delightful dining experience.<br/>
                                    Cuisine: Thai and international<br/>
                                    Open Kingfisher and Room service 06.00-22.00 Last order 21.30<br/>
                                    Location: Ground floor of the Reception  building</p>

									<br/><br/><br/><br/><br/>
                                    <h1 class="title">Sunbird Pool Bar</h1>
                                    <p>Sun Bird Pool Bar’s peaceful poolside setting is the perfect place to relax and enjoy ice-cold beers, refreshing thirst-quenchers and a selection of coffees, teas and enticing light bites from the extensive menu.
Cuisine: International snacks and beverages.<br/>
Open daily: from 10 am. – 7 pm.<br/>
Location:  Poolside at building 6.</p>

									<br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h1 class="title">Starling Lobby Lounge</h1>
                                    <p>With a warm and welcoming ambience, Starling Lobby Lounge is the perfect place to take time out with family and friends enjoying drinks and snacks, day or night. Beautiful music adds a little extra sparkle in the evenings.<br/>
Cuisine: International snacks and beverages<br/>
Open daily:  from 10 am. –  Midnight<br/>
Location: Lobby level of the Reception  building</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pigeon_library_and_internet_corner" class="article" data-tab-name="Pigeon Library &amp; Internet Corner">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pigeon_library_and_internet_corner.jpg?ver=20160525" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Pigeon Library &amp; Internet Corner</h1>
                                    <p>Guests can enjoy quiet moments in the resort’s well-stocked library. A wide range of books, local and international newspapers and magazines are available. DVDs can also be borrowed to watch in the comfort of guestrooms. Internet connection and computers equipment are also available on complimentary basis in the Pigeon Conner.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>