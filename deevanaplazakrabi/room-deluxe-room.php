<?php
$title = 'Deluxe Room | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'deluxe room, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-deluxe-room.php';
$lang_th = '/th/deevanaplazakrabi/room-deluxe-room.php';
$lang_zh = '/zh/deevanaplazakrabi/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/gallery/set-02/rooms/1500/room-26.jpg" alt="Deluxe Room 01" />
                    <img src="images/gallery/set-02/rooms/1500/room-27.jpg" alt="Deluxe Room 02" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/gallery/set-02/rooms/600/room-26.jpg" height="50" /></li>
                    <li><img src="images/gallery/set-02/rooms/600/room-27.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/gallery/set-02/rooms/600/room-27.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>The 33-square-metre Deluxe rooms are well equipped and benefit from all standard comforts and amenities. Guests enjoy the option of king size or twin- beds. A large bathroom includes a refreshing rain shower and Deluxe branded toiletries. Other features are a bedside reading lamp, flat screen LCD TV with satellite channels, IDD telephones with voicemail, Free Wi-Fi connection, coffee and tea making facilities, mini bar, safety deposit box, and iron & ironing board. Rooms also feature a private furnish balcony.</p>
                            <p>Rooms available: 115</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">Book This Room Category</a>
                        </div>
                        
                   <!--     <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 33 Sq.m. including balcony / terrace</li>
                                        <li>King size or twin-beds</li>
                                        <li>Pool view</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                       <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>