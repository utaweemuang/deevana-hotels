<?php

$url        = 'http://www.deevanahotels.com/deevanaplazakrabi/';
$name       = 'Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$version    = '20180330';
$email      = 'es@deevanaplazakrabi.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/DeevanaPlazaKrabiAonang';
$twitter    = '#';
$googleplus = '#';
$youtube    = 'https://www.youtube.com/channel/UC_zIuBzbXVgu7ps388lY0Rg';
$vimeo      = '#';
$instagram  = '#';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = '#';

$ibeID      = '276';