<?php
$title = 'Deluxe Suite | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Deluxe Suite: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'deluxe suite, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-suite';
$cur_page = 'deluxe-suite';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-deluxe-suite.php';
$lang_th = '/th/deevanaplazakrabi/room-deluxe-suite.php';
$lang_zh = '/zh/deevanaplazakrabi/room-deluxe-suite.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-01.jpg" alt="Deluxe Suite 01" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-02.jpg" alt="Deluxe Suite 02" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-03.jpg" alt="Deluxe Suite 03" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-04.jpg" alt="Deluxe Suite 04" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Deluxe Suite <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-04.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Deluxe Suite</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>The choice of Deevana Plaza Krabi Aonang most discerning guests, the generous 66-square-metre Deluxe Suites include one living room and one bedroom. Designed to provide a refreshing and relaxed personal space, the large suites are complimented by a furnished balconies with views across the resort, a beautiful bathroom with a bathtub and a separate rain shower, and Premier branded toiletries. Each suite features a king size bed, also include a 32” LCD TV in the bedroom and a 40”LCD TV in the living room with a DVD player, docking station, mini fridge and coffee machine, alarm clock, bathroom scales, make-up mirror, an iron and ironing board. Connecting to Premier room  is also available</p>
                            <p>Rooms available: 4</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                     <!--   <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 66 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Pool view</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>