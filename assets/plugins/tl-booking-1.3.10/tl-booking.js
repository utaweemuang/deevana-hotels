/**
 * Travelanium Booking Plugin
 * Version: 1.3.9
 * Author: Travelanium
 * Author URI: http://www.travelanium.com
 * Support: support@travelanium.com
 *
 */

(function($) {
    "use strict";

    $.fn.booking = function(options) {
        var settings = $.extend({
            checkInSelector: '[name="checkin"]',
            checkOutSelector: '[name="checkout"]',
            adultSelector: '[name="numofadult"]',
            childSelector: '[name="numofchild"]',
            roomSelector: '[name="numofroom"]',
            codeSelector: '[name="accesscode"]',
            builtinDatepicker: true,
            dateFormat: 'dd M yy',
            secretCode: null,
            submitSelector: '.btn-search',
            propertyId: '[name="propertyId"]',
            propertyGroup: null,
            onlineId: 4,
            language: null,
            currency: null,
            confl: null,
            beforeSubmit: function(){},
            afterSubmit: function(){},
            debug: false,
        }, options);

        return this.each(function(index, element) {
            var $this       = $(element),
                $checkIn    = $this.find(settings.checkInSelector),
                $checkOut   = $this.find(settings.checkOutSelector),
                $submit     = $this.find(settings.submitSelector);

            var checkInValue, checkOutValue;

            if( settings.builtinDatepicker ) {
                /**
                 * Insert hidden fields
                 */
                var alt_checkin = $('<input>').attr({
                    'type': 'hidden',
                    'id': 'alt_checkin_' + index,
                }), alt_checkout = $('<input>').attr({
                    'type': 'hidden',
                    'id': 'alt_checkout_' + index,
                });
    
                $checkIn.after(alt_checkin);
                $checkOut.after(alt_checkout);
    
                var today       = new Date(),
                    tomorrow    = nextday( today, 1 );
    
                $checkIn.datepicker({
                    minDate: today,
                    changeMonth: false,
                    changeYear: false,
                    dateFormat: settings.dateFormat,
                    altFormat: 'yy-mm-dd',
                    altField: alt_checkin,
                    numberOfMonths: 1,
                    onSelect: function (dateFormat, inst) {
                        $checkOut.datepicker('option', 'minDate', nextday(dateFormat, 1));
                        setTimeout(function() {
                            $checkOut.datepicker('show');
                        }, 350);
                    }
                });
    
                $checkOut.datepicker({
                    minDate: tomorrow,
                    changeMonth: false,
                    changeYear: false,
                    dateFormat: settings.dateFormat,
                    altFormat: 'yy-mm-dd',
                    altField: alt_checkout,
                    numberOfMonths: 1
                });
                
                /**
                 * Set default date
                 */
                $checkIn.datepicker('setDate', today);
                $checkOut.datepicker('setDate', tomorrow);
            }

            $this.on('submit', function(event) {
                event.preventDefault();

                if (settings.builtinDatepicker) {
                    checkInValue    = $checkIn.datepicker('option', 'altField').val();
                    checkOutValue   = $checkOut.datepicker('option', 'altField').val();
                } else {
                    checkInValue    = $checkIn.val();
                    checkOutValue   = $checkOut.val();
                }

                runBookingScript(checkInValue, checkOutValue);
            });
            
            $submit.on('click', function(event) {
                event.preventDefault();

                if (settings.builtinDatepicker) {
                    checkInValue    = $checkIn.datepicker('option', 'altField').val();
                    checkOutValue   = $checkOut.datepicker('option', 'altField').val();
                } else {
                    checkInValue    = $checkIn.val();
                    checkOutValue   = $checkOut.val();
                }

                runBookingScript(checkInValue, checkOutValue);
            });

            function runBookingScript(checkInValue, checkOutValue) {
                var baseurl             = 'https://reservation.travelanium.net/propertyibe2/?';
                var propertyIdParam     = 'propertyId=';
                var onlineIdParam       = 'onlineId=';
                var checkInParam        = 'checkin=';
                var checkOutParam       = 'checkout=';
                var adultParam          = 'numofadult=';
                var childParam          = 'numofchild=';
                var roomParam           = 'numofroom=';
                var accesscodeParam     = 'accesscode=';
                var propertyGroupParam  = 'pgroup=';
                var currencyParam       = 'currency=';
                var conflParam          = 'confl=';

                var propertyIdValue     = getPropertyNum(settings.propertyId);
                var onlineIdValue       = settings.onlineId;

                var languageParam       = 'lang=';
                var language            = settings.language;

                var checkInString       = checkInValue;
                var checkOutString      = checkOutValue;
                var adultValue          = $this.find(settings.adultSelector).val();
                var childValue          = $this.find(settings.childSelector).val();
                var roomValue           = $this.find(settings.roomSelector).val();
                var accesscode          = $this.find(settings.codeSelector).val();
                var secretcode          = settings.secretCode;
                var accesscodeValue     = (accesscode) ? accesscode : secretcode;
                var propertyGroupValue  = settings.propertyGroup;
                var currencyValue       = settings.currency;
                var confl               = settings.confl;
                var redirectUrl         = baseurl + propertyIdParam + propertyIdValue + "&" + onlineIdParam + onlineIdValue;

                redirectUrl             += "&" + checkInParam + checkInString;
                redirectUrl             += "&" + checkOutParam + checkOutString;

                if (language !== null)
                    redirectUrl += '&' + languageParam + language;

                if (roomValue !== '')
                    redirectUrl += "&" + roomParam + roomValue;

                if (adultValue !== '')
                    redirectUrl += "&" + adultParam + adultValue;

                if (childValue !== '')
                    redirectUrl += "&" + childParam + childValue;

                if (
                    accesscodeValue &&
                    accesscodeValue !== null &&
                    accesscodeValue !== undefined
                ) {
                    redirectUrl += "&" + accesscodeParam + accesscodeValue;
                }

                if (propertyGroupValue !== null)
                    redirectUrl += "&" + propertyGroupParam + propertyGroupValue;

                if (currencyValue !== null)
                    redirectUrl += "&" + currencyParam + currencyValue;

                if (confl !== null)
                    redirectUrl += "&" + conflParam + confl;

                $.beforeSubmit = settings.beforeSubmit;
                $.beforeSubmit();

                gotoURL(redirectUrl);

                $.afterSubmit = settings.afterSubmit;
                $.afterSubmit();
            }

            /**
             * Get property value
             */
            function getPropertyNum(string) {
                var pid;
                if( isFinite(string) ) {
                    pid = string;
                } else {
                    pid = $this.find(string).val();
                }
                return pid;
            }

            /**
             * Calcurate date
             */
            function nextday( date, plus ) {
                var cur = new Date( date ),
                    nxt = new Date( cur.getTime() + (plus * 24 * 60 * 60 * 1000) );
                return nxt;
            }

            function gotoURL( url ) {
                if (settings.debug === true) {
                    console.log( decorateGACrossDomainTracking(url) );
                } else {
                    window.open( decorateGACrossDomainTracking(url) );
                }
            }

            /**
             * Generate Google tracking script
             */
            function decorateGACrossDomainTracking(url) {
                var output = url;
                try {
                    if (!ga) return output;
                    ga(function (tracker) {
                        if (tracker == undefined)
                            tracker = ga.getAll()[0];
                        if (!tracker) return output;
                        if (!tracker.get('linkerParam')) return output;
                        var linker = new window.gaplugins.Linker(tracker);
                        output = linker.decorate(url);
                    });
                } catch (e) {
                    if (settings.debug === true)
                        console.log(e.message);
                }
                return output;
            }
        });
    };

})(jQuery);
