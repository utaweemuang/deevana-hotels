<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js'></script>
<script>
    ;(function ($) {
        var startDate = '2019-09-12 00:00:00';
        var finalDate = '2020-03-08 23:59:59';
        if (moment().tz('Asia/Bangkok').isBetween(startDate, finalDate)) {
            $.magnificPopup.open({
                items: {
                    type: 'image',
                    src: 'http://www.deevanahotels.com/images/home/DHR_Flyer.jpg',
                },
                callbacks: {
                    open: function() {
                        var imgUrl = 'https://reservation.travelanium.net/propertyibe2/?propertyId=386&onlineId=2&pgroup=QQMPUPYV',
                            imgTar = '_blank';
                        $(this.content).find('.mfp-img').wrap('<a href="'+imgUrl+'" target="'+imgTar+'" rel="noopener" />');
                    },
                },
            });
        }
        var popup = {
            enabled: false,
            src: '',
            link: {
                enabled: false,
                href: '',
                target: '_blank',
            }
        }
        if (popup.enabled === true) {
            $.magnificPopup.open({
                items: {
                    src: popup.src,
                    type: 'image',
                },
                mainClass: 'mfp-fade',
                removalDelay: 300,
                callbacks: {
                    open: function () {
                        if (popup.link.enabled === true) {
                            var $img = $(this.content).find('.mfp-img');
                            $img.wrap('<a href="' + popup.link.href + '" target="' + popup.link.target + '" />');
                        }
                    }
                }
            });
        }
    })(jQuery);
</script>