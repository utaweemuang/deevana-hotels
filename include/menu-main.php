<ul class="list-menu">
    <li class="has-sub-menu">
        <a href="#">Our Hotels</a>
        <ul class="sub-menu">
            <li class="label">Phuket</li>
            <li><a href="/deevanaplazaphuket">Deevana Plaza Phuket Patong</a></li>
            <li><a href="/deevanapatong">Deevana Patong Resort &amp; Spa</a></li>
            <li><a href="/ramadaphuketdeevana">Ramada By Wyndham Phuket Deevana</a></li>
            <li><a href="/recentasuitephuket">Recenta Suite Phuket Suanluang</a></li>
            <li><a href="/recentaphuket">Recenta Phuket Suanluang</a></li>
            <li><a href="/recentastyle">Recenta Style Phuket Town</a></li>
            <li class="label">Krabi</li>
            <li><a href="/deevanaplazakrabi">Deevana Plaza Krabi Aonang</a></li>
            <li><a href="/deevanakrabiresort">Deevana Krabi Resort</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('our-brands'); ?>"><a href="our-brands.php">Our Brands</a></li>
    <li class="has-sub-menu">
        <a href="meeting-events.php">Meeting &amp; Event</a>
        <ul class="sub-menu"">
            <li class="label">Phuket</li>
            <li><a href="/deevanaplazaphuket/meetings-and-event.php" target="_blank">Deevana Plaza Phuket Patong</a></li>
            <li><a href="/deevanapatong/facilities.php#mice_facilities">Deevana Patong Resort &amp; Spa</a></li>
            <li><a href="/ramadaphuketdeevana/meetings-and-event.php" target="_blank">Ramada By Wyndham Phuket Deevana</a></li>
            <li class="label">Krabi</li>
            <li><a href="/deevanaplazakrabi/meetings-and-event.php" target="_blank">Deevana Plaza Krabi Aonang</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#">Attraction</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('attraction-phuket'); ?>"><a href="attraction-phuket.php">Phuket</a></li>
            <li class="<?php echo get_current_class('attraction-krabi'); ?>"><a href="attraction-krabi.php">Krabi</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#">Special Offers</a>
        <ul class="sub-menu">
            <li class="label">Phuket</li>
            <li><a href="/deevanaplazaphuket/offers.php" target="_blank">Deevana Plaza Phuket Patong</a></li>
            <li><a href="/deevanapatong/offers.php" target="_blank">Deevana Patong Resort &amp; Spa</a></li>
            <li><a href="/ramadaphuketdeevana/offers.php" target="_blank">Ramada Phuket Deevana</a></li>
            <li><a href="<?php ibe_url( '299', 'en' ); ?>" target="_blank">Recenta Suite Phuket Suanluang</a></li>
            <li><a href="<?php ibe_url( '294', 'en' ); ?>" target="_blank">Recenta Phuket Suanluang</a></li>
            <li><a href="<?php ibe_url( '310', 'en' ); ?>" target="_blank">Recenta Style Phuket Town</a></li>
            <li class="label">Krabi</li>
            <li><a href="<?php ibe_url( '276', 'en' ); ?>" target="_blank">Deevana Plaza Krabi Aonang</a></li>
            <li><a href="<?php ibe_url( '386', 'en'); ?>">Deevana Krabi Resort</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">Contact</a></li>
</ul>