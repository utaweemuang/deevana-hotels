<div class="booking-bar">
    <div class="inner">
        <div class="container">
            <form id="booking-form" class="form">
                <div class="table">

                    <div class="table-cell table-cell-header">
                        <span class="field field-header">
                            <img class="block responsive" src="assets/elements/book_online_best_rate_guarantee.png" alt="Book Online Best rate guarantee" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-hotel">
                        <span class="field field-hotel">
                            <select id="hotel" class="input-select" name="propertyId" required
                                    data-error-msg="please select hotel">
                                <option value="" selected>- Select hotel -</option>
                                <optgroup label="PHUKET">
                                    <option value="275">Deevana Plaza Phuket Patong</option>
                                    <option value="277">Deevana Patong Resort &amp; Spa</option>
                                    <option value="278">Ramada by Wyndham Phuket Deevana </option>
                                    <option value="299">Recenta Suite Phuket Suanluang</option>
                                    <option value="294">Recenta Phuket Suanluang</option>
                                    <option value="310">Recenta Style Phuket Town</option>
                                </optgroup>
                                <optgroup label="KRABI">
                                    <option value="276">Deevana Plaza Krabi Aonang</option>
                                    <option value="386">Deevana Krabi Resort</option>
                                </optgroup>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkin">
                        <span class="field field-checkin">
                            <input id="checkin" class="input-text date" name="checkin" placeholder="Check-in" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkout">
                        <span class="field field-checkout">
                            <input id="checkout" class="input-text date" name="checkout" placeholder="Check-out" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-adults">
                        <span class="field field-adults">
                            <label class="label">Adults</label>
                            <select id="adults" class="input-select" name="numofadult">
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-children">
                        <span class="field field-children">
                            <label class="label">Children</label>
                            <select id="children" class="input-select" name="numofchild">
                                <option value="0" selected>0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-rooms">
                        <span class="field field-rooms">
                            <label class="label">Rooms</label>
                            <select id="rooms" class="input-select" name="numofroom">
                                <option value="1" selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-code">
                        <span class="field field-code">
                            <input id="accesscode" name="accesscode" class="input-text" placeholder="Promo code" type="text" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-submit">
                        <span class="field field-submit">
                            <button id="submit" class="button" type="submit">BOOK NOW</button>
                        </span>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
