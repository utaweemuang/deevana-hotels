<?php
require_once('_functions.php');
?>

<!doctype HTML>
<html class="preload <?php html_class(); ?>" lang="th">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="og:image" content="<?php echo get_info('url') . 'assets/elements/ogimage.jpg'; ?>" />
    <meta property="og:url" content="<?php echo get_info('url'); ?>" />
    <meta property="og:title" content="<?php echo get_info('name'); ?>" />
    <meta property="og:description" content="Deevana Hotels & Resorts provide comfortable accommodation in Phuket and Krabi. The ideal choice of professionalism quality service, completely amenities and luxury wellness spa." />

    <?php web_desc(); ?>
    <?php web_keyw(); ?>
    <title><?php web_title(); ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="assets/elements/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/elements/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/elements/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/elements/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/elements/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/elements/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/elements/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/elements/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/elements/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/elements/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/elements/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/elements/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/elements/favicons/favicon-16x16.png">
    <link rel="manifest" href="assets/elements/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/elements/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="google-site-verification" content="UmRmlyqknjntJVtP1REHk_4qNyRpdVKzk7hnVX2FJug" />

    <!--Utilities-->
    <script src="assets/plugins/html5shiv/html5shiv.min.js"></script>
    <script src="assets/plugins/html5shiv/html5shiv-printshiv.min.js"></script>
    <!--Normalize-->
    <link href="assets/css/normalize.css" rel="stylesheet" />
    <!--Fonts-->
    <link href="assets/fonts/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Cinzel:400,700,900|Roboto:400,700,400italic,700italic,300,300italic|Roboto+Condensed:300italic,300,400,400italic' rel='stylesheet' type='text/css'>
    <!--jQuery-->
    <script src="assets/js/jquery-1.11.3.min.js"></script>
    <script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <!--Debounce-->
    <script src="assets/js/jquery.ba-throttle-debounce.min.js"></script>
    <!--Validator-->
    <link rel="stylesheet" href="assets/plugins/form-validator/theme-default.min.css" />
    <script src="assets/plugins/form-validator/jquery.form-validator.min.js"></script>
    <!--OwlCarousel-->
    <script src="assets/plugins/owl.carousel.2.0.0-beta.3/owl.carousel.min.js"></script>
    <link rel="stylesheet" href="assets/plugins/owl.carousel.2.0.0-beta.3/assets/owl.carousel.min.css" />
    <!--LightGallery-->
    <link src="assets/plugins/lightGallery-master/dist/css/lightgallery.min.css" />
    <script src="assets/plugins/lightGallery-master/dist/js/lightgallery.min.js"></script>
    <script src="assets/plugins/lightGallery-master/dist/js/lg-fullscreen.min.js"></script>
    <script src="assets/plugins/lightGallery-master/dist/js/lg-thumbnail.min.js"></script>
    <!--KeepRatio-->
    <script src="assets/js/jquery.keep-ratio.min.js"></script>
    <!--ResizeEnd-->
    <script src="assets/js/jquery.resizeend.min.js"></script>
    <!--Throttle-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js'></script>
    <!--TouchSwipe-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js'></script>
    <!-- MagnificPopup -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <!--Member-->
    <script src="assets/js/member.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/js-cookie/latest/js.cookie.min.js'></script>
    <!--Booking-->
    <link rel="stylesheet" href="assets/plugins/booking-script/datepicker_theme.css" />
    <script src="assets/plugins/tl-booking-1.4.1/tl-booking.min.js"></script>
    <!--Core-->
    <link rel="stylesheet" href="assets/css/main.css?ver=<?php echo get_info('version'); ?>" />
    <script src="assets/js/main.js?ver=<?php echo get_info('version'); ?>"></script>
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PKNVGB5');
    </script>
    <!-- End Google Tag Manager -->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-81334137-1', 'auto', {'allowLinker': true});
        ga('require', 'linker');
        ga('linker:autoLink', ['travelanium.net'] );
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '215754318902215');
        fbq('track', 'PageView');
    </script>
    <!-- End Facebook Pixel Code -->
</head>

<body class="<?php body_class(); ?>">
    <!-- 
    <div id="fb-root"></div>
    <script>
    window.fbAsyncInit = function() {
        FB.init({
        appId            : '471118216675725',
        xfbml            : true,
        version          : 'v3.2'
        });
    };

    (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div class="fb-customerchat"
    attribution=setup_tool
    page_id="1550086251898990">
    </div> -->

    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=215754318902215&ev=PageView&noscript=1" /></noscript>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKNVGB5" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <div id="page">
        <header class="site-header">
            <div class="container">
                <div class="site-branding">
                    <a href="<?php echo get_info('url'); ?>">
                        <img class="logo" src="assets/elements/logo-deevana-group.png"
                             srcset="assets/elements/logo-deevana-group.png 1x, assets/elements/logo-deevana-group@2x.png 2x"
                             alt="Deevana Hotels"
                             width="200" height="49">
                    </a>
                </div>

                <div class="site-topbar">
                    <ul>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i> BEST RATE GUARANTEE</li>
                        <li role="separator">|</li>
                        <li>
                            <div class="lang" id="lang_select">
                                <a class="display"><i class="fa fa-fw fa-language" aria-hidden="true"></i> Thai <i class="fa fa-angle-down"></i></a>
                                <ul class="list-lang">
                                    <li><a href="<?php echo $lang_en; ?>">English</a></li>
                                    <li><a href="<?php echo $lang_th ?>">Thai</a></li>
                                    <li><a href="<?php echo $lang_zh ?>">Chinese</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>

                <a href="#" class="side-panel-trigger"><i class="fa fa-lg fa-bars" aria-hidden="true"></i></a>
                <?php include 'include/navigation.php'; ?>
                <?php include 'include/member-panel.php'; ?>
            </div>
        </header>
