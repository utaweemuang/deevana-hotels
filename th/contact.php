<?php
$title = 'Contact Deevana Hotels & Resorts';
$desc = 'Deevana Hotels & Resorts - Book here now for best rates guaranteed throughout Phuket and Krabi';
$keyw = 'Thailand, Phuket beach, resort, accommodation, Phuket Hotels, Hotels Phuket, Hotel Patong, Patong Hotel, holiday Phuket, conferences, phuket vacations, phuket spa, krabi hotel';

$html_class = '';
$body_class = 'contact';
$cur_page = 'contact';

$lang_en = '/contact.php';
$lang_th = '/th/attraction-phuket.php';
$lang_zh = '/zh/attraction-phuket.php';

include_once('_header.php');
?>

<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/contact/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">

        <section class="main-content sidebar-left">
            <div class="container">
                <div class="row">
                    <div class="col-w8 col-content">
                        <div class="site-content">

                            <section class="section section-information">
                                <header class="section-header">
                                    <h1 class="section-title">ข้อมูลสำหรับติดต่อ</h1>

                                    <span class="custom-select" id="detail-select">
                                        <span class="selected">โรงแรม</span>

                                        <div class="options-box">
                                            <ul class="options">
                                                <li class="option active" data-value="#hotel-1"></li>
                                                <li class="option-group">
                                                    <ul class="sub-options">
                                                        <li class="option-label">ภูเก็ต</li>
                                                        <li class="option" data-value="#hotel-2">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</li>
                                                        <li class="option" data-value="#hotel-3">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</li>
                                                        <li class="option" data-value="#hotel-4">รามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตอง </li>
                                                        <li class="option" data-value="#hotel-5">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</li>
														<li class="option" data-value="#hotel-6">รีเซ็นต้า ภูเก็ต สวนหลวง</li>
														<li class="option" data-value="#hotel-7">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</li>
                                                    </ul>
                                                </li>

                                                <li class="option-group">
                                                    <ul class="sub-options">
                                                        <li class="option-label">กระบี่</li>
                                                        <li class="option" data-value="#hotel-8">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</li>
                                                        <li class="option" data-value="#hotel-9">ดีวาน่า กระบี่ รีสอร์ท</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </span>
                                </header>

                                <div class="content tabs-group">

                                    <div id="hotel-1" class="tab-content default">
										<div class="gmap">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.8394287594865!2d100.53025651521561!3d13.72816950153842!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29f2c880c2dad%3A0x2c04c3ade1dd3f6f!2sKasemkij+Building!5e0!3m2!1sen!2sth!4v1463026705419" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>

                                        <h2><span class="font-roboto" style="font-weight: 300;"></span></h2>
                                        <dl>
                                            <dt>สำนักงานกรุงเทพฯ</dt>
                                            <dd>ชั้น 4 ห้อง 404 อาคารเกษมกิจ เลขที่ 120 ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 6661">+66 (0) 2632 6661</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 6676">+66 (0) 2632 6676</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>ชั้น 4 ห้อง 404 อาคารเกษมกิจ เลขที่ 120 ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 8565">+66 (0) 2632 8565</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+66 (0) 2233 6144">+66 (0) 2233 6144</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:salesco@deevanaplazaphuket.com">salesco@deevanaplazaphuket.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>ชั้น 4 ห้อง 404 อาคารเกษมกิจ เลขที่ 120 ถนนสีลม ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 6661">+66 (0) 2632 6661</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 6676">+66 (0) 2632 6676</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:sales@deevanapatong.com">sales@deevanapatong.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">รามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตอง </span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>ชั้น 4 ห้อง 404  อาคารเกษมกิจ เลขที่ 120 ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 9476">+66 (0) 2632 9476</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+66 (0) 2632 9477">+66 (0) 2632 9477</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:sco@ramadaphuketdeevana.com">sco@ramadaphuketdeevana.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>60/80 หมู่ 2, ตำบลวิชิต, อำเภอเมืองภูเก็ต, จังหวัดภูเก็ต, ประเทศไทย 83000</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+6676201000">+66(0) 76 201 000</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 76 201 039</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">รีเซ็นต้า ภูเก็ต สวนหลวง</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>60/80 หมู่ 2, ตำบลวิชิต, อำเภอเมืองภูเก็ต, จังหวัดภูเก็ต, ประเทศไทย 83000</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+6676201000">+66(0) 7620 1000</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 7620 1039</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>10/1  ถนน รัตนโกสินทร์ 200 ปี ตำบลตลาดเหนือ อำเภอเมืองภูเก็ต จังหวัดภูเก็ต 83000 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+6676214344">+66(0) 76 214 344</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+6676201039">+66(0) 76 201 039</a></dd>
                                            <dt>อีเมล์ :</dt>
                                            <dd><a href="mailto:info@recentahotels.com">info@recentahotels.com</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>ชั้น 4 ห้อง 404  อาคารเกษมกิจ เลขที่ 120 ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+6626329474-5">+66(0) 2632 9474-5</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+6622336144">+66(0) 2233 6144</a></dd>
                                        </dl>

										<hr>

										<h2><span class="font-roboto" style="font-weight: 300;">ดีวาน่า กระบี่ รีสอร์ท</span></h2>
                                        <dl>
                                            <dt>สำนักงานขาย :</dt>
                                            <dd>ชั้น 4 ห้อง 404  อาคารเกษมกิจ เลขที่ 120 ถนนสีลม กรุงเทพฯ 10500 ประเทศไทย</dd>
                                            <dt>เบอร์โทรศัพท์ :</dt>
                                            <dd><a href="tel:+6626329474-5">+66(0) 2632 9474-5</a></dd>
                                            <dt>เบอร์แฟกซ์ :</dt>
                                            <dd><a href="tel:+6622336144">+66(0) 2233 6144</a></dd>
                                        </dl>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </div>

                    <div class="col-w4 col-sidebar">
                        <div class="sidebar">
                            <aside class="aside">
                                <h3 class="title">ข้อมูลโรงแรม</h3>
                                <p><a class="download-button" href="#"><i class="icon fa fa-cloud-download"></i>อังกฤษ</a></p>
                            </aside>

                            <aside class="aside">
                                <h3 class="title">ลิงค์</h3>
                                <ul>
                                    <li><a href="#">ตืดต่อ</a></li>
                                    <li><a href="#">สมัครงาน</a></li>
                                </ul>
                            </aside>

                            <aside class="aside">
                                <h3 class="title">บอกให้เราทราบในสิ่งที่คุณคิด</h3>
                                <p>ความคิดเห็นของคุณคือสิ่งที่สำคัญสำหรับเรา โปรดร่วมกับเราในการแสดงความคิดเห็นใดๆ ที่คุณมีต่อเว็บไซต์ของเรา หรือช่วงเวลาที่คุณพักกับเราที่โรงแรมดีวาน่า แอนด์ รีสอร์ท</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $(function() {
        $('#detail-select').on('click', '[data-value]', function() {
            var $this = $(this);
            var tar = $this.data('value');
            $('.tabs-group').find(tar).fadeIn(300).siblings().hide();

            $this.addClass('active');
            $this.parents('ul.options').find('li.option').not($this).removeClass('active');
        });
        $('.tabs-group').find('.tab-content.default').show();
    });
</script>

<style>
    .section {
        border: 1px solid #ccc;
        margin-bottom: 30px;
    }
    .section-header {
        background-color: #1a355e;
        color: #fff;
        padding: 10px 30px;
        position: relative;
    }
    .section-title {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        color: #fff;
        text-align: left;
    }
    .section .content {
        padding: 20px 30px 10px;
    }

    #contact_form .field {
        display: block;
    }
    #contact_form .input-text,
    #contact_form .input-select,
    #contact_form .input-textarea {
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    #contact_form #submit {
        background-color: #9a7b12;
        border-radius: 4px;
        color: #fff;
        border: 0;
        height: 32px;
        line-height: 32px;
        width: 100px;
        text-align: center;
    }
    #contact_form #submit:hover {
        background-color: #333;
    }
    #detail-select {
        position: absolute;
        top: 50%;
        right: 30px;
        margin-top: -18px;
    }
	.gmap {
		margin-top: -20px;
		margin-left: -30px;
		margin-right: -30px;
		margin-bottom: 20px;
		border-bottom: 1px solid #ccc;
	}
	.gmap iframe {
		display: block;
		width: 100%;
		height: 200px;
	}
    .download-button {
        background-color: #333;
        padding: 0 12px;
        border-radius: 2px;
        display: inline-block;
        line-height: 2;
        color: #fff;
    }
    .download-button .icon {
        margin-right: 3px;
    }
    .download-button:hover {
        background-color: #444;
        color: #fff;
    }
    .tabs-group .tab-content {
        display: none;
    }
    @media (max-width: 640px) {
        .section-header {
            padding-left: 15px;
            padding-right: 15px;
        }
        .section .content {
            padding: 15px 15px 10px;
        }
        .row-contact-form .col-w6 {
            width: 100%;
        }
    }
    @media (max-width: 600px) {
        #detail-select {
            position: relative;
            top: inherit;
            right: inherit;
            margin-top: 10px;
            width: 100%;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
