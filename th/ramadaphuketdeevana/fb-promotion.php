<?php
$title = 'F&amp;B Promotion | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = '';
$keyw = '';

$html_class = '';
$body_class = 'fb-promotion';
$cur_page = 'fb-promotion';

$lang_en = '/ramadaphuketdeevana/fb-promotion.php';
$lang_th = '/th/ramadaphuketdeevana/fb-promotion.php';
$lang_zh = '/zh/ramadaphuketdeevana/fb-promotion.php';

include_once('_header.php');
?>

<main class="site-main no-head pattern-fibers">
    <section class="site-content">

        <header class="section-header">
            <div class="container">
                <h1 class="section-title">F&amp;B Promotion</h1>
            </div>
        </header>

        <article class="article">
            <div class="container">
                <div class="row">
                    <div class="col-w12">
                    	<div class="masonry-items" id="fb_promotion">
                    		<div class="item"><a href="#"><img class="block force" src="./images/002 Promotion.jpg" alt="Promotion" width="420" height="747" /></a></div>
							<div class="item"><a href="download/ValentineSet-Menu.pdf"><img class="block force" src="./images/home/promo/romance-dinner-20161123.jpg" alt="Romantic Dinner" width="421" height="595" /></a></div>
							<div class="item"><a href="#"><img class="block force" src="./images/home/promo/soft-drink-20161123.jpg" alt="Soft Drink" width="1080" height="1920" /></a></div>
						</div>
					</div>
                </div>
            </div>
        </article>

    </section>

	<?php include('include/booking_bar.php'); ?>

</main>

<style>
	.site-main.no-head {
		padding-top: 130px;
	}
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
	.masonry-items {
		margin: 0 -5px;
	}
	.masonry-items .item {
		padding: 5px;
		width: 33.3333%;
	}
	.masonry-items .item img {
		opacity: 1;
		-wekbit-transition: 200ms;
		transition: 200ms;
	}
	.masonry-items .item a:hover img {
		opacity: 0.85;
	}
	@media(max-width: 1070px) {
		.site-main.no-head {
			padding-top: inherit;
		}
	}
	@media(max-width: 768px) {
		.masonry-items .item { width: 50%; }
	}
	@media(max-width: 480px) {
		.masonry-items .item { width: 100%; }
	}
</style>

<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
	var mi = $('.masonry-items');
	mi.isotope();
	mi.imagesLoaded().progress(function() {
		mi.isotope('layout');
	});
</script>

<?php include_once('_footer.php'); ?>
