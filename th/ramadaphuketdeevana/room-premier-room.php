<?php
$title = 'Premier Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Premier Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'premier room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-premier-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-premier-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">

        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" />
                </div>
            </div>
        </div>

        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">ห้องพรีเมียร์ <span>เตียงคิงไซส์</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องพรีเมียร์</h1>

                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />

                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>

                        <div class="col-w5 col-info">
                            <p>ห้องพรีเมียร์มีขนาด 38 ตารางเมตร ประกอบด้วยเตียงคิงไซส์ขนาด 7 ฟุต และมีหน้าต่างที่สูงจากพื้นจรดเพดานเพื่อให้ท่านได้เพลิดเพลินไปกับทิวทัศน์ได้อยางเต็มที่ การตกแต่งด้วยโทนสีที่อบอุ่นและเฟอร์นิเจอร์ที่สวยงามทันสมัยรวมกับสิ่งอำนวยความสะดวกที่หลากหลาย จะทำให้การเข้าพักของท่านมีความสะดวกสบาย</p>
                            <p>เตรียมพร้อมสำหรับการออกไปเที่ยวยามค่ำคืนที่โต๊ะเครื่องแป้ง และพักผ่อนด้วยกาแฟสดยามเช้าในระเบียงส่วนตัวที่กว้างขวาง ลักษณะพิเศษของห้องพรีเมียร์ประกอบไปด้วยชุดมินิบาร์, เครื่องดื่ม, ขนมขบเคี้ยวที่เติมใหม่ทุกวันโดยไม่คิดค่าใช้จ่ายเพิ่มเติม เสื้อคลุมอาบน้ำที่นุ่มสบายรวมไปถึงผลิตภัณฑ์สำหรับอาบน้ำ, โลชั่นบำรุงผิว, อุปกรณ์ทำความสะอาดฟันและชุดโกนหนวด</p>

                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>

                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงเดี่ยวขนาดคิงไซส์ / เตียงคู่</li>
                                        <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                        <li>ระเบียงส่วนตัวพร้อมด้วยโต๊ะและเก้าอี้</li>
                                        <li>ทีวีแอลอีดีขนาด 42 นิ้ว</li>
                                        <li>ช่องสัญญาณดาวเทียม</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงเดี่ยวขนาดคิงไซส์</li>
                                            <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                            <li>ระเบียงส่วนตัวพร้อมด้วยโต๊ะและเก้าอี้</li>
                                            <li>ทีวีแอลอีดีขนาด 47 นิ้ว</li>
                                            <li>ช่องสัญญาณดาวเทียม</li>
                                            <li>โคมไฟอ่านหนังสือแอลอีดี</li>
                                            <li>ตู้นิรภัยอิเล็กทรอนิกส์</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>ห้องน้ำและฝักบัวอาบน้ำแบบสายฝน</li>
                                            <li>ไฟฉาย</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้เย็นขนาดเล็ก</li>
                                            <li>กระจกสำหรับแต่งหน้า/โกนหนวด</li>
                                            <li>เตารีดและโต๊ะสำหรับรีดแบบติดฝาผนัง</li>
                                            <li>โต๊ะเขียนหนังสือ</li>
                                            <li>ถุงชายหาด</li>
                                            <li>เสื้อคลุมอาบน้ำและรองเท้าแตะแบบใช้แล้วทิ้ง</li>
                                            <li>ร่ม</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สายความเร็วสูง (Wi-Fi)</li>
                                            <li>ฟรีผลิตภัณฑ์สำหรับอาบน้ำ</li>
                                            <li>ฟรีน้ำดื่ม 4 ขวดต่อวัน</li>
                                            <li>ฟรีอุปกรณ์สำหรับชงชาและกาแฟ, เติมใหม่ทุกวัน</li>
                                            <!-- <li>ฟรีกาแฟสด</li>
                                            <li>บริการเปิดเตียงตอนเย็น</li> -->
                                            <li>ที่ตั้งกระเป๋าเดินทาง</li>
                                        </ul>
                                    </div>
                                </div>

                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>

            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>

    </div>
</main>

<?php include_once('_footer.php'); ?>
