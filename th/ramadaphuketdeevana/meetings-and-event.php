<?php
$title = 'Meetings and Event | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'meetings and event, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/ramadaphuketdeevana/meetings-and-event.php';
$lang_th = '/th/ramadaphuketdeevana/meetings-and-event.php';
$lang_zh = '/zh/ramadaphuketdeevana/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/meetings/meeting-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            
            <header class="section-header">
                <div class="container">
                    <h1 class="section-title">ห้องประชุมและจัดเลี้ยง</h1>
                    <p class="description">ห้องประชุมที่ทันสมัยของโรงแรมรามาด้า ภูเก็ต ดีวาน่า ป่าตอง เป็นที่ที่เหมาะสำหรับการจัดกิจกรรมขององค์กร, การประชุมทางธุรกิจ, งานเลี้ยง, งานสัมมนา, งานรื่นเริงและอื่น ๆ อีกมากมาย ห้องประชุมสามารถรองรองผู้เข้าร่วมงานได้ถึง 60 ที่นั่งในรูปแบบงานจัดเลี้ยง และรองรับได้ 63 ที่นั่งในรูปแบบห้องเรียน, และยังสามารถแบ่งเป็นห้องจัดกิจกรรมเล็กๆ เช่น การประชุมคณะกรรมการ  พร้อมด้วยอุปกรณ์ภาพและเสียงที่ทันสมัย และยังมีตัวเลือกอีกมากมายสำหรับเครื่องดื่มและบริการจัดเลี้ยง </p>
                </div>
            </header>
            
            <article class="article">
                <div class="container">
                    <div class="row">
                        <div class="col-w5 col-pic">
                            <img class="force thumbnail" src="images/meetings/meeting_room.jpg" alt="Meeting Room" />
                        </div>
                        
                        <div class="col-w7 col-cap">
                            <h1 class="section-title" style="font-size:1.25rem;">แพ็คเก็จห้องประชุมและสัมมนา (ขั้นต่ำ 30 คน)</h1>
                            <p style="margin-bottom:0;">ประชุมแบบเต็มวัน (เบรค 2 มื้อ และอาหารกลางวัน 1 มื้อ) ราคา 1,700 บาท/คน</p>
                            <p>ประชุมแบบครึ่งวัน (เบรค 1 มื้อ และอาหารกลางวัน 1 มื้อ) ราคา 1,300 บาท/คน</p>
                            <h1 class="title">แพ็คเก็จห้องประชุมประกอบไปด้วย : </h1>
                            <ul>
                                <li>จอโปรเจคเตอร์พร้อมเครื่องฉายแอลซีดี </li>
                                <li>กระดานขาวพร้อมปากกาไวท์บอดร์ด</li>
                                <li>กระดานฟลิปชาร์ท 2 </li>
                                <li>กระดาษโน๊ตพร้อมปากกา </li>
                                <li>แผ่นผับการประชุม</li>
                                <li>น้ำดื่ม</li>
                            </ul>
                            <h1 class="section-title" style="font-size:1.25rem;">แพ็คเก็จค่าเช่าห้อง</h1>
                            <p style="margin-bottom:0;">เช่าครึ่งวัน 1 ห้องประชุม ราคา 5,000 บาท </p>
                            <p>เช่าเต็มวัน 1 ห้องประชุม ราคา 8,500 บาท</p>
                            <h1 class="title">แพ็คเก็จประกอบไปด้วย :</h1>
                            <ul>
                                <li>ระบบเครื่องเสียง </li>
                                <li>น้ำดื่ม</li>
                                <li>เวที</li>
                            </ul>
                            <h1 class="title">สิ่งอำนวนความสะดวกอื่นๆ :</h1>
                            <ul>
                                <li>มุมอินเตอร์เน็ต - ให้บริการตลอด 24 ชั่วโมงโดยไม่คิดค่าใช้จ่าย ตั้งอยู่ติดกับล็อบบี้</li>
                                <li>ฟรีอินเตอร์เน็ตไร้สาย (Wi-Fi) - ให้บริการในทุกพื้นที่ในบริเวณรีสอร์ท</li>
                                <li>บริการซักรีด - ให้บริการรวดเร็วและเชื่อถือได้ มีค่าใช้จ่ายเพิ่มเติม</li>
                                <li>ที่จอดรถ - ให้บริการที่จอดรถในร่มที่ปลอดภัย สามารถรองรับได้จำนวน 55 คัน</li>
                                <li>การรักษาความปลอดภัย - มียามรักษาความปลอดภัยตลอด 24 ชั่วโมง และกล้องวงจรปิดทั่วทุกพื้นที่ในบริเวณรีสอร์ท</li>
                            </ul>
                            <!-- <h1 class="title">สิ่งอำนวนความสะดวกอื่นๆ</h1>
                            <ul>
                                <li>มุมอินเตอร์เน็ต - ให้บริการตลอด 24 ชั่วโมงโดยไม่คิดค่าใช้จ่าย ตั้งอยู่ติดกับล็อบบี้</li>
                                <li>ฟรีอินเตอร์เน็ตไร้สาย (Wi-Fi) - ให้บริการในทุกพื้นที่ในบริเวณรีสอร์ท</li>
                                <li>บริการซักรีด - ให้บริการรวดเร็วและเชื่อถือได้ มีค่าใช้จ่ายเพิ่มเติม</li>
                                <li>ที่จอดรถ - ให้บริการที่จอดรถในร่มที่ปลอดภัย สามารถรองรับได้จำนวน 55 คัน</li>
                                <li>การรักษาความปลอดภัย - มียามรักษาความปลอดภัยตลอด 24 ชั่วโมง และกล้องวงจรปิดทั่วทุกพื้นที่ในบริเวณรีสอร์ท</li>
                            </ul> -->
                        </div>
                        <div class="col-w12" style="padding:15px 0;text-align:center;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/b-shape.jpg" alt="" width="1050" height="713"></div>
                        <div class="col-w12" style="padding:15px 0;text-align:center;"><img src="http://www.ramadaphuketdeevana.com/images/meeting/u-shape.jpg" alt="" width="1050" height="713"></div>
                    </div>
                </div>
            </article>
            
        </section>
    </section>
        
</main>

<style>
    .site-content {
        padding-bottom: 50px;
    }
    .section-header {
        text-align: center;
        padding: 50px 0;
    }
    .section-header .container:after {
        content: '';
        border-bottom: 1px dotted #333;
        width: 60%;
        display: block;
        margin: 20px 20% 0;
    }
    .section-title {
        color: #C40032;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #222;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        font-weight: 500;
    }
	.col-cap .sub-title {
		font-size: 14px;
		margin-top: 1em;
		margin-bottom: 3px;
	}
    @media (max-width: 480px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>