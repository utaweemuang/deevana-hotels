<?php

session_start();

$title = 'Ramada By Wyndham Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Enjoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea';
$keyw = 'deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/ramadaphuketdeevana';
$lang_th = '/th/ramadaphuketdeevana';
$lang_zh = '/zh/ramadaphuketdeevana';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">

        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="http://www.deevanahotels.com/ramadaphuketdeevana/images/home/home-slide-00.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="http://www.deevanahotels.com/ramadaphuketdeevana/images/home/home-slide-001.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/home/home-slide-01_cr.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>

        <!-- <div id="promotion_board" class="promotion get-center">
            <a href="<?php //ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">
                <img class="block responsive" src="images/home/ramada_discount.png" all="Ramada Phuket Deevnaam 4-star hotel" />
            </a>
        </div> -->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
        <!-- <div style="position: absolute;bottom: 45px;right: 15px;z-index: 10;">
          <img class="" src="http://ramadaphuketdeevana.com/images/awards/SHA-plus-Ramada.png" alt="reward" width="100" height="100" style="width:150px;height:auto;">
        </div> -->
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ยินดีต้อนรับสู่ โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตอง</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>โรงแรมตกแต่งแบบร่วมสมัย ตั้งอยู่ใจกลางหาดป่าตองที่มีชีวิตชีวา และชายหาดแห่งนี้ยังเป็นหนึ่งในจุดหมายปลายทางของการท่องเที่ยว ตัวโรงแรมรายล้อมไปด้วยสถานที่ท่องเที่ยวชั้นนำ ผู้เข้าพักใช้เวลาเพียง 5 นาทีก็สามารถเดินไปถึงชายหาดสีขาวที่สวยงาม, เขตสถานบันเทิงยามราตรีที่มีชีวิตชีวาและห้างสรรพสินค้าขนาดใหญ่เพื่อช้อปปิ้งและความบันเทิง เป็นต้น</p>
                        <p>โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตอง มีความเงียบสงบเหมาะสำหรับการหลีกหนีจากถนนและความวุ่นวายในตัวเมือง เราให้การต้อนรับที่อบอุ่นและเงียบสงบแก่ท่าน งานศิลปะรูปช้างที่สวยงามทั่วทั้งรีสอร์ทจะช่วยเพิ่มรอยยื้มให้กับการเข้าพักของคุณ รวมถึงห้องพักที่ตกแต่งอย่างมีรสนิยมเต็มรูปแบบ โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตองเป็นโรงแรมปลอดบุหรี่ ที่ได้ออกแบบมาเพื่อการพักผ่อนหย่อนใจอย่างมีความสุขสำหรับทั้งครอบครัว สระว่ายน้ำกลางแจ้งของเรามีอ่างน้ำวนในตัว และมีคิดส์คลับสำหรับให้ความสนุกสนานแก่ผู้เข้าพักตัวน้อย ในระหว่างที่คุณกำลังดื่มด่ำไปกับการดูแลฟื้นฟูที่เวลเนสสปา</p>
                        <p>คุณสามารถรับประทานอาหารนานาชาติได้ตลอดทั้งวันที่ห้องอาหารของเรา และบริเวณล็อบบี้ที่ร้านเบ้กแอนด์เบ้บยังมีเครื่องดื่มเบาๆที่ให้ความสดชื่นคอยให้บริการ เรายังมีบริการห้องประชุมสำหรับการจัดประชุมทางธุรกิจหรืองานเลี้ยงต่างๆอีกด้วย</p>
                        <p class="d-none"><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/0aeKbOVZMcA?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content col-w8">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ยินดีต้อนรับสู่ โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content-01.jpg" /></p>
                        <p>โรงแรมตกแต่งแบบร่วมสมัย ตั้งอยู่ใจกลางหาดป่าตองที่มีชีวิตชีวา และชายหาดแห่งนี้ยังเป็นหนึ่งในจุดหมายปลายทางของการท่องเที่ยว ตัวโรงแรมรายล้อมไปด้วยสถานที่ท่องเที่ยวชั้นนำ ผู้เข้าพักใช้เวลาเพียง 5 นาทีก็สามารถเดินไปถึงชายหาดสีขาวที่สวยงาม, เขตสถานบันเทิงยามราตรีที่มีชีวิตชีวาและห้างสรรพสินค้าขนาดใหญ่เพื่อช้อปปิ้งและความบันเทิง เป็นต้น</p>
                        <p>โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตอง มีความเงียบสงบเหมาะสำหรับการหลีกหนีจากถนนและความวุ่นวายในตัวเมือง เราให้การต้อนรับที่อบอุ่นและเงียบสงบแก่ท่าน งานศิลปะรูปช้างที่สวยงามทั่วทั้งรีสอร์ทจะช่วยเพิ่มรอยยื้มให้กับการเข้าพักของคุณ รวมถึงห้องพักที่ตกแต่งอย่างมีรสนิยมเต็มรูปแบบ โรงแรมรามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า ป่าตองเป็นโรงแรมปลอดบุหรี่ ที่ได้ออกแบบมาเพื่อการพักผ่อนหย่อนใจอย่างมีความสุขสำหรับทั้งครอบครัว สระว่ายน้ำกลางแจ้งของเรามีอ่างจากุซซี่ในตัว และมีคิดส์คลับสำหรับให้ความสนุกสนานแก่ผู้เข้าพักตัวน้อย ในระหว่างที่คุณกำลังดื่มด่ำไปกับการดูแลฟื้นฟูที่เวลเนสสปา</p>
                        <p>คุณสามารถรับประทานอาหารนานชาติได้ตลอดทั้งวันที่ห้องอาหารของเรา และบริเวณล็อบบี้ที่ร้านเบ้กแอนด์เบ้บยังมีเครื่องดื่มเบาๆที่ให้ความสดชื่นคอยให้บริการ เรายังมีบริการห้องประชุมสำหรับการจัดประชุมทางธุรกิจหรืองานเลี้ยงต่างๆอีกด้วย</p>

                        <p class="d-none"><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                        
                        <div class="row">
                            <div class="col-auto">
                                <div id="TA_certificateOfExcellence325" class="TA_certificateOfExcellence"><ul id="yEFh7dA4NI" class="TA_links j8i90iYgjFXa"><li id="V6r1oM" class="89MQD0VNgji"><a target="_blank" href="https://www.tripadvisor.com/Hotel_Review-g297930-d8275148-Reviews-Ramada_Phuket_Deevana-Patong_Kathu_Phuket.html"><img src="https://www.tripadvisor.com/img/cdsi/img2/awards/CoE2017_WidgetAsset-14348-2.png" alt="TripAdvisor" class="widCOEImg" id="CDSWIDCOELOGO"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=certificateOfExcellence&amp;uniq=325&amp;locationId=8275148&amp;lang=th_TH&amp;year=2017&amp;display_version=2"></script>

                                <div id="TA_rated905" class="TA_rated"><ul id="9ADbaSwkU" class="TA_links inPcBnd"><li id="R9ucTKt" class="jPt04X8Vyf6X"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/badges/ollie-11424-2.gif" alt="TripAdvisor"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=905&amp;locationId=8275148&amp;lang=th_TH&amp;display_version=2"></script>
                            </div>
                            <div class="col-auto">
                                <div id="TA_selfserveprop616" class="TA_selfserveprop"><ul id="5atf3rrVZ" class="TA_links vMbqXvVhCy"><li id="JieTl4" class="z8fGeGpxtJ"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"/></a></li></ul></div>
                                <script async src="https://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=616&amp;locationId=8275148&amp;lang=th_TH&amp;rating=true&amp;nreviews=0&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=false&amp;display_version=2"></script>
                            </div>
                        </div>
                    </div>

                    <div class="col-countdown col-w4">
                    	<a href="./fb-promotion.php"><img class="responsive" src="./images/002 Promotion.jpg" alt="Promotion" width="420" height="630" /></a>
                    </div>
                </div> -->
            </div>
        </section>
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>

                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="text-center">Loading...</div>
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#c40032;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="activities_slider" class="owl-carousel has-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-swimming_pool.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">สระว่ายน้ำ</h2>
                            <p class="description">ท่านสามารถเล่นน้ำอย่างสนุกสนานที่สระว่ายน้ำกลางแจ้งของเรา ซึ่งมีพื้นที่ตื้นๆไว้สำหรับเด็กๆ ริมสระน้ำเรียงรายไปด้วยเก้าอี้และร่มกันแดด และยังมีบาร์ริมสระน้ำที่จะช่วยเพิ่มความสดชื่นด้วยเครื่องดื่มและของว่าง</p>
                            <p><a class="button" href="facilities.php#swimming_pool">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-orientala_wellness_spa.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">โอเรียลทาล่า สปา</h2>
                            <p class="description">เริ่มต้นการเดินทางแห่งสุขภาพและการพักผ่อนของคุณที่โอเรียลทาล่า สปา ที่เงียบสงบ สปาที่หรูหราของเรามีห้องนวดส่วนตัว 3 ห้อง</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-pool_deck.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ร้านอาหารและบาร์</h2>
                            <p class="description">ร้านเดอะคาเฟ่และร้านเบคแอนด์เบฝตั้งอยู่ขนานไปกับล็อบบี้ ตกแต่งแบบร่วมสมัยและยังมองเห็นทิวทัศน์อันสวยงามของสวนในรีสอร์ท</p>
                            <p><a class="button" href="facilities.php#pool_deck">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title">
                    <span style="display: inline-block;" class="deco-map">
                        <span class="deco-underline">สถานที่ท่องเที่ยว ภูเก็ต</span>
                    </span>
                </h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" /></div>
                            <h2 class="title">หาดป่าตอง</h2>
                            <a class="more" href="attraction.php#patong_beach">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" /></div>
                            <h2 class="title">แหลมพรหมเทพ</h2>
                            <a class="more" href="attraction.php#phromthep_cape">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" /></div>
                            <h2 class="title">หากกะตะและหาดกะรน</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" /></div>
                            <h2 class="title">วัดพระใหญ่</h2>
                            <a class="more" href="attraction.php#big_buddha">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/SHA-plus-rmd.png"><img src="http://www.ramadaphuketdeevana.com/images/awards/SHA-plus-rmd.png" alt="" width="128" height=""></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/thailand-tourism-award-3.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/thailand-tourism-award.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/ctrip-1.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/Ctrip-logo.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-05.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/goibibo-logo.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-08.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/Make-My-Trip-Logo.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-07.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/Hotels.com-Logo.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/Green-hotel-002.jpg"><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-04.jpg"><img src="http://www.deevanahotels.com/images/awards/nfi-awards.png" alt="" width="128" height="128"></a></li>
                </ul>
                <ul class="list-awards">
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-12.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/Thai-Hotel-Association.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/new-awards/awards_2020.png"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/awards_2020.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/awards-03.jpg"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/Agoda-logo.png" alt="" width="128" height="128"></a></li>
                <li><a class="image-popup-awards" href="http://www.ramadaphuketdeevana.com/images/awards/new-awards/unesco-logo.png"><img src="http://www.ramadaphuketdeevana.com/images/awards/new-awards/unesco-logo.png" alt="" width="128" height="128"></a></li>
                <li><img src="http://www.deevanahotels.com/images/awards/green-silver-awards.png" alt="" width="128" height="128"></li>
                <li><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-awards.png" alt="" width="128" height="128"></li>
                <!-- <li><img src="http://www.deevanahotels.com/images/awards/halal-awards.png" alt="" width="128" height="128"></li> -->
                <li><img src="http://www.deevanahotels.com/images/awards/thai-food-awards.png" alt="" width="128" height="128"></li>
                <li><img src="http://www.ramadaphuketdeevana.com/images/awards/safetravel.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>

    </section>
</main>

<style>
    #offers {
        background-color : #8e1b34;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        *width: 100%;
        *padding-right: 340px;
    }
    .row-intro .col-countdown {
        *position: absolute;
        *top: 0;
        *right: 15px;
        *width: 290px;
		margin-top: 70px;
    }
	.row-intro .col-countdown img {
		opacity: 1;
		-webkit-transition: 200ms;
		transition: 200ms;
	}
	.row-intro .col-countdown a:hover img {
		opacity: 0.85;
	}
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #c40032;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #c40032;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #333;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #c40032;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            *float: none;
        }

        .row-intro .col-content {
            *padding-right: 10px;
        }

        .row-intro .col-countdown {
            *position: static;
            *margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
	@media (max-width: 540px) {
		.row-intro > [class*="col-"] {
			width: 100%;
			margin-top: 0;
		}
		.row-intro .col-countdown img {
			display: block;
			margin: auto;
		}
	}
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}

        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<script src="https://cdn.jsdelivr.net/npm/template7@1.4.2/dist/template7.min.js" integrity="sha256-AR7mrm2lYY5i4wWxxAznbMCTVOCMD5kvqLUmtW/6iCY=" crossorigin="anonymous"></script>
<script type="text/template7" id="offer_slider_item_template">
    {{#each posts}}
    <div class="item">
        <div class="row">
            <div class="col-12 col-md-6">
                {{img class="force" image}}
            </div>
            <div class="col-12 col-md-6">
                <div class="block-content-wrapper">
                    <h2 class="title">{{title_th}}</h2>
                    <div class="description">
                        {{content_th}}
                    </div>

                    {{#if button_th.enable}}
                    <a class="button" href="{{button_th.link_url}}" target="{{button_th.link_target}}">{{button_th.link_text}}</a>
                    {{/if}}
                </div>
            </div>
        </div>
    </div>
    {{/each}}
</script>

<script>
    (function($) {
    	var origin = 'https://webdemo2.travelanium.net/deevana/cockpit';
        var token = '3fa7cf86aa69b4028f4d0adbe6c735';
        var collection = 'ramadaPhuketDeevanaOffers';
        var path = '%base%/api/collections/get/%collection%?token=%token%&rspc=1'
            .replace('%base%',origin)
            .replace('%collection%',collection)
            .replace('%token%',token);
    	var data = $.getJSON(path);
        Template7.registerHelper('img', function(image, options) {
            var imgApi = origin + '/api/cockpit/image';
            var imgPath = origin + '/storage/uploads' + image.path;
            var url = '%base%?token=%token%&src=%path%&rspc=1&w=600&m=bestFit&q=82&o=true'
                .replace('%base%', imgApi)
                .replace('%token%', token)
                .replace('%path%', imgPath);
            return '<img class="%className%" src="%src%" loading="lazy" />'
                .replace('%className%', options.hash.className)
                .replace('%src%', url);
        });
    	data.then(function(res) {
            console.log(res);
    		var template = $('#offer_slider_item_template').html();
    		var items = res.entries.filter(function(item) {
    			return item.published
    		});
    		if (items.length) {
    			var compiled = Template7.compile(template);
    			var html = compiled({
    				posts: items,
    			});
    			$('#offers_slider').html(html);
    		}
    	});
    })(jQuery);
</script>

<?php include 'include/popup-image.php'; ?>
<?php include_once('_footer.php'); ?>
