<?php
$title = 'Deluxe Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'deluxe room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-deluxe-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-deluxe-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องดีลักซ์ <span>เตียงคิงไซส์และเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องดีลักซ์</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ห้องดีลักซ์ของเรามีขนาด 35 ตารางเมตร และโดดเด่นด้วยเตียงขนาด 6 ฟุต หรือเตียงคู่ขนาด 4 ฟุต, เก้าอี้ที่นุ่มสบายพร้อมด้วยโต๊ะกาแฟ และโต๊ะเครื่องแป้งพร้อมกระจก, เครื่องปรับอากาศ, สีตามธรรมชาติและแสงสว่างตามอารมณ์ (Mood Lighting)ภายในห้องพักจะช่วยสร้างบรรยากาศที่ผ่อนคลาย และระเบียงส่วนตัวซึ่งเชื่อมพื้นที่นั่งเล่นในร่มไปสู่พื้นที่กลางแจ้ง จะทำให้คุณสามารถพักผ่อนและเพลิดเพลินไปกับสภาพอากาศที่อบอุ่น</p>
                            <p>สิ่งอำนวยความสะดวกประกอบไปด้วยตู้เย็นขนาดเล็กและอุปกรณ์สำหรับเครื่องดื่มร้อน, ทีวีจอแบนติดฝาผนังพร้อมด้วยรีโมทและช่องสัญญาณดาวเทียม มีที่เก็บของที่มีขนาดกว้างขวางสำหรับเก็บของใช้ส่วนตัว และตู้นิรภัยส่วนตัวที่ใหญ่เพียงพอที่จะเก็บคอมพิวเตอร์พกพาเพื่อความสบายใจของคุณ</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงเดี่ยวขนาดคิงไซส์ / เตียงคู่</li>
                                        <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                        <li>ระเบียงส่วนตัวพร้อมด้วยโต๊ะและเก้าอี้</li>
                                        <li>ทีวีแอลอีดีขนาด 42 นิ้ว</li>
                                        <li>ช่องสัญญาณดาวเทียม</li>
                                        <li>อื่นๆ</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงเดี่ยวขนาดคิงไซส์ / เตียงคู่</li>
                                            <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                            <li>ระเบียงส่วนตัวพร้อมด้วยโต๊ะและเก้าอี้</li>
                                            <li>ทีวีแอลอีดีขนาด 42 นิ้ว</li>
                                            <li>ช่องสัญญาณดาวเทียม</li>
                                            <li>โคมไฟอ่านหนังสือแอลอีดี</li>
                                            <li>ตู้นิรภัยอิเล็กทรอนิกส์</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>ห้องน้ำและฝักบัวอาบน้ำแบบสายฝน</li>
                                            <li>ไฟฉาย</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้เย็นขนาดเล็ก</li>
                                            <li>กระจกสำหรับแต่งหน้า/โกนหนวด</li>
                                            <li>เตารีดและโต๊ะสำหรับรีดแบบติดฝาผนัง</li>
                                            <li>โต๊ะเขียนหนังสือ</li>
                                            <li>ถุงชายหาด</li>
                                            <li>เสื้อคลุมอาบน้ำและรองเท้าแตะแบบใช้แล้วทิ้ง</li>
                                            <li>ร่ม</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สายความเร็วสูง (Wi-Fi)</li>
                                            <li>ฟรีผลิตภัณฑ์สำหรับอาบน้ำ</li>
                                            <li>ฟรีน้ำดื่ม 2 ขวดต่อวัน</li>
                                            <li>ฟรีอุปกรณ์สำหรับชงชาและกาแฟ, เติมใหม่ทุกวัน</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>