<ul class="menu list-menu">
    <li class="has-sub-menu toggle-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">ห้องพัก</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">ห้องดีลักซ์</a></li>
            <li class="<?php echo get_current_class('premier-room'); ?>"><a href="room-premier-room.php">ห้องพรีเมียร์</a></li>
            <li class="<?php echo get_current_class('junior-room'); ?>"><a href="room-junior-room.php">ห้องจูเนียร์สวีท</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('spa'); ?>"><a href="facilities.php#orientala_wellness_spa">สปา</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">สิ่งอำนวยความสะดวก</a></li>
    <li class="<?php echo get_current_class('meetings'); ?>"><a href="meetings-and-event.php">ห้องประชุมและจัดเลี้ยง</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">สถานที่ท่องเที่ยว</a></li>
    <li class="<?php echo get_current_class('offers'); ?>"><a href="offers.php">โปรโมชั่น</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">รูปภาพ</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">ติดต่อเรา</a></li>
</ul>
