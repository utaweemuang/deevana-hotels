<!-- <script>
    ;(function($) {
        
        var popup = {
            enabled: true,
            image: {
                src: 'http://www.ramadaphuketdeevana.com/images/banner/Pop-up-re.jpg',
            },
            link: {
                enabled: true,
                href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=278&onlineId=4&pid=MDg1MTk4',
                target: '_blank',
            }
        }
        if (popup.enabled) {
            $.magnificPopup.close({
                items: {
                    type: 'image',
                    src: popup.image.src,
                },
                callbacks: {
                    open: function() {
                        if (popup.link.enabled) {
                            $(this.content).find('.mfp-img').wrap('<a href="'+popup.link.href+'" target="'+popup.link.target+'" rel="noopener" />');
                            if (memberChecker()) {
                                memberAppendCode('member');
                            }
                        }
                    },
                },
            });
        }
    })(jQuery);
</script> -->

<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.25/moment-timezone-with-data-2012-2022.min.js'></script>
<script>
    ;(function($) {
      var today = moment().tz('Asia/Bangkok');
      var popup1 = {
    enabled: false,
    image: {
      src: 'http://www.ramadaphuketdeevana.com/images/banner/Ramada1get1.jpg',
    },
    link: {
      enabled: true,
      href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=4&checkin=2022-01-03&checkout=2022-01-05&numofadult=2&numofchild=0&numofroom=1&pid=MDc0MDkxMnwwNzQwOTE0',
      target: '_blank',
    }
      }
      if (popup1.enabled===true && today.isBetween('2021-10-01 00:00:00', '2022-10-31 23:59:59')) {
        $.magnificPopup.close({
          items: {
            type: 'image',
            src: popup1.image.src,
          },
          callbacks: {
            open: function() {
              if (popup1.link.enabled===true) {
                $(this.content).find('.mfp-img').wrap('<a href="'+popup1.link.href+'" target="'+popup1.link.target+'" />');
              }
            },
          },
        });
      }
      var popup2 = {
    enabled: true,
    image: {
      src: 'http://www.ramadaphuketdeevana.com/images/banner/early2021.jpg',
    },
    link: {
      enabled: true,
      href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=278&onlineId=4&checkin=2022-06-01&checkout=2022-06-03&numofadult=2&numofchild=0&numofroom=1&pid=MDg4NzkxfDA4ODc5Mg%3D%3D',
      target: '_blank',
    }
      }
      if (popup2.enabled===true && today.isBetween('2021-12-01 00:00:00', '2021-12-31 23:59:59')) {
        $.magnificPopup.open({
          items: {
            type: 'image',
            src: popup2.image.src,
          },
          callbacks: {
            open: function() {
              if (popup2.link.enabled===true) {
                $(this.content).find('.mfp-img').wrap('<a href="'+popup2.link.href+'" target="'+popup2.link.target+'" />');
              }
            },
          },
        });
      }
    console.log(today.isBetween('2019-06-01 00:00:00', '2021-12-31 23:59:59'));
    })(jQuery);
</script>