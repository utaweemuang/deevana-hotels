<?php

$url        = 'http://www.deevanahotels.com/th/ramadaphuketdeevana/';
$name       = 'Ramada Phuket Deevana';
$version    = '20180410';
$email      = 'info@ramadaphuketdeevana.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/Ramada-Phuket-Deevana-Patong-587155034719365/';
$twitter    = '#';
$googleplus = '#';
$youtube    = '#';
$vimeo      = '#';
$instagram  = 'https://www.instagram.com/ramadabywyndhamphuketdeevana/?hl=th';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = '#';

$ibeID      = '278';