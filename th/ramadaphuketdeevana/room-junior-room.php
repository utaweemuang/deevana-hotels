<?php
$title = 'Junior Room | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'Junior Room: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'junior room, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-room';
$cur_page = 'junior-room';
$par_page = 'rooms';

$lang_en = '/ramadaphuketdeevana/room-junior-room.php';
$lang_th = '/th/ramadaphuketdeevana/room-junior-room.php';
$lang_zh = '/zh/ramadaphuketdeevana/room-junior-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/junior-suite/1500/junior-suite-01.jpg" alt="Junior Room 01" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-02.jpg" alt="Junior Room 02" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-03.jpg" alt="Junior Room 03" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-04.jpg" alt="Junior Room 04" />
                    <img src="images/accommodations/junior-suite/1500/junior-suite-05.jpg" alt="Junior Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องจูเนียร์สวีท <span>เตียงคิงไซส์</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior-suite/600/junior-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior-suite/600/junior-suite-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องจูเนียร์สวีท</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior-suite/600/junior-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>เอาใจตัวคุณเองด้วยห้องจูเนียร์สวีทที่หรูหราขนาด 56 ตารางเมตร ซึ่งโดดเด่นด้วยห้องนอนและพื้นที่นั่งเล่นที่กว้างขวาง ให้ความรู้สึกเหมือนเป็นบ้านอีกหลังของคุณ ห้องสวีทมีแสงส่องสว่างและทันสมัยด้วยหน้าต่างที่สูงจากพื้นจรดเพดานซึ่งสามารถเปิดออกไปสู่ระเบียงส่วนตัวที่กว้างขวาง</p>
                            <p>เลาจ์ประกอบด้วยโซฟานุ่นสบายและทีวีแอลอีดีขนาดใหญ่พิเศษขนาด 47 นิ้วและช่องสัญญาณดาวเทียมเพื่อความบันเทิง ห้องนอนมีเตียงคิงไซส์ขนาด 6 ฟุต มีห้องเก็บของในตัวที่กว้างขวางสำหรับเก็บของใช้ส่วนตัว และจุดเด่นของห้องสวีทคือเตียงสำหรับนอนเล่นและอ่างอาบน้ำบนระเบียงซึ่งช่วยเพิ่มความโรแมนติกให้การเข้าพักของคุณ</p>
                            <p>ห้องจูเนียร์สวีทมีความพิเศษอีกอย่างที่จะทำให้ผู้เข้าพักพอใจยิ่งขึ้น นั่นคือมินิบาร์ที่เติมใหม่ทุกวันโดยไม่คิดค่าใช้จ่าย, เครื่องทำกาแฟ, ตู้เย็นขนาดใหญ่, เสื้อคลุมอาบน้ำอ่อนนุ่ม และรองเท้าแตะ และยังมีผลิตภัณฑ์อาบน้ำที่ใช้สำหรับการแช่ในอ่างอาบน้ำอย่างผ่อนคลาย</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงเดี่ยวขนาดคิงไซส์</li>
                                        <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                        <li>ระเบียงส่วนตัวพร้อมด้วยอ่างอาบน้ำ</li>
                                        <li>ทีวีแอลอีดีขนาด 47 นิ้ว</li>
                                        <li>ช่องสัญญาณดาวเทียม</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงเดี่ยวขนาดคิงไซส์</li>
                                            <li>หมอน 4 ใบ (นุ่ม/แน่น)</li>
                                            <li>ระเบียงส่วนตัวพร้อมด้วยโต๊ะและเก้าอี้</li>
                                            <li>ทีวีแอลอีดีขนาด 47 นิ้ว</li>
                                            <li>ช่องสัญญาณดาวเทียม</li>
                                            <li>โคมไฟอ่านหนังสือแอลอีดี</li>
                                            <li>ตู้นิรภัยอิเล็กทรอนิกส์</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>ห้องน้ำและฝักบัวอาบน้ำแบบสายฝน</li>
                                            <li>ไฟฉาย</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้เย็น</li>
                                            <li>กระจกสำหรับแต่งหน้า/โกนหนวด</li>
                                            <li>เตารีดและโต๊ะสำหรับรีดแบบติดฝาผนัง</li>
                                            <li>โต๊ะเขียนหนังสือ</li>
                                            <li>ถุงชายหาด</li>
                                            <li>เสื้อคลุมอาบน้ำและรองเท้าแตะแบบใช้แล้วทิ้ง</li>
                                            <li>ร่ม</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สายความเร็วสูง (Wi-Fi)</li>
                                            <li>ฟรีผลิตภัณฑ์สำหรับอาบน้ำ</li>
                                            <li>ฟรีน้ำดื่ม 4 ขวดต่อวัน</li>
                                            <li>ฟรีมินิบาร์, เครื่องดื่ม และขนมขบเคิ้ยว, เติมใหม่ทุกวัน</li>
                                            <li>ฟรีอุปกรณ์สำหรับชงชาและกาแฟ, เติมใหม่ทุกวัน</li>
                                            <li>ฟรีกาแฟสด</li>
                                            <li>เครื่องทำกาแฟเอสเพรสโซ่</li>
                                            <li>บริการเปิดเตียงทำความสะอาดในรอบบ่าย</li>
                                            <li>ที่ตั้งกระเป๋าเดินทาง</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>