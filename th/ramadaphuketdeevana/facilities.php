<?php
$title = 'Facilities | Ramada Phuket Deevana Hotel | Official Hotel Group Website Thailand';
$desc = 'facilities: njoy best direct hotel rate and best location on Patong Beach; 4 star hotel under Ramada brand near Phuket Fantasea ';
$keyw = 'facilities, deevana, ramada deevana, ramada deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/ramadaphuketdeevana/facilities.php';
$lang_th = '/th/ramadaphuketdeevana/facilities.php';
$lang_zh = '/zh/ramadaphuketdeevana/facilities.php';

include_once('_header.php');
?>

<main class="site-main">

    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-01.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-02.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-03.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-04.jpg" alt="Ramada Phuket Deevena, 4-star hotel" /></div>
        </div>

        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include('include/booking_bar.php'); ?>

    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">สิ่งอำนวยความสะดวก</h1>
            </header>

            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swimming_pool" class="tab active">สระว่ายน้ำ</span>
                    <span data-tab="#pool_deck" class="tab">ร้านอาหารและบาร์</span>
                    <span data-tab="#fitness_centre" class="tab">ศูนย์ฟิตเนส</span>
                    <span data-tab="#kids_club" class="tab">คิดส์คลับ</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">โอเรียลทาล่า สปา</span>
                    <span data-tab="#dining" class="tab">ร้านอาหาร</span>
                    <span data-tab="#the_cafe" class="tab">ห้องอาหารเดอะ คาเฟ่</span>
                    <span data-tab="#bake_and_bev" class="tab">ร้านเบคแอนด์เบฝ</span>
                    <span data-tab="#room_service" class="tab">บริการรูมเซอร์วิส</span>
                </div>

                <div class="tabs-content">
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming_pool.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ</h1>
                                    <p>ท่านสามารถเล่นน้ำอย่างสนุกสนานที่สระว่ายน้ำกลางแจ้งของเรา ซึ่งมีพื้นที่ตื้นๆไว้สำหรับเด็กๆ ริมสระน้ำเรียงรายไปด้วยเก้าอี้และร่มกันแดด และยังมีบาร์ริมสระน้ำที่จะช่วยเพิ่มความสดชื่นด้วยเครื่องดื่มและของว่าง</p>
                                    <p><span style="color: #516819;">เวลาเปิดให้บริการ : 7.00 - 20.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="pool_deck" class="article" data-tab-name="Pool Deck">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool_deck.jpg" alt="Pool Deck" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ร้านอาหารและบาร์</h1>
                                    <p>ร้านเดอะคาเฟ่และร้านเบคแอนด์เบฝตั้งอยู่ขนานไปกับล็อบบี้ ตกแต่งแบบร่วมสมัยและยังมองเห็นทิวทัศน์อันสวยงามของสวนในรีสอร์ท การพบปะรับประทานอาหารตลอดทั้งวันนี้เริ่มต้นด้วยอาหารเช้านานาชาติแบบบุฟเฟต์ ประกอบด้วยซีเรียล, ผลไม้เขตร้อน, เบเกอรี่ และอาหารเช้าปรุ่งสุกที่มีทั้งรสชาติแบบเอเชียและตะวันตก</p>
                                    <p><span style="color: #516819;">เวลาเปิดให้บริการ : 10.00 - 19.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="fitness_centre" class="article" data-tab-name="Fitness Centre">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness_centre.jpg" alt="Fitness Centre" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ศูนย์ฟิตเนส</h1>
                                    <p>ศูนย์ฟิตเนสของมีเรามีอุปกรณ์ออกกำลังกายที่ครบครัน และยังมีอุปกรณ์เวทสำหรับการออกกำลังกายพร้อมกันทุกส่วน ที่ตั้งของศุนย์ฟิตเนสยังสามารถมองเห็นสระน้ำ ซึ่งช่วยหันเหความสนใจในระหว่างที่คุณกำลังออกกำลังกายได้เป็นอย่างดี สมาชิกทุกคนต้องมีอายุอย่างน้อย 16 ปี</p>
                                    <p><span style="color: #516819;">เวลาเปิดให้บริการ : 07.00 - 21.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="kids_club" class="article" data-tab-name="Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/kids_club.jpg" alt="Kid's Club" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">คิดส์คลับ</h1>
                                    <p>คิดส์คลับตั้งอยู่ริมสระน้ำ เป็นสถานที่เหมาะสำหรับเด็กๆที่มาเล่นสนุกและทำความรู้จักกับเพื่อนใหม่ๆ คิดส์คลับโดดเด่นด้วยสนามเด็กเล่น, ของเล่น, เกม และตารางสำหรับกิจกรรมในแต่ละวันเพื่อทำให้ความสนุกสนานกับเด็กๆ โดยกิจกรรมต่างๆจะขึ้นอยู่กับความพร้อม เด็กทุกคนจะต้องมีอายุระหว่าง 4 - 12 ปี</p>
                                    <p><span style="color: #516819;">เวลาเปิดให้บริการ : 07.00 - 18.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/Orientala_wellness_spa.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">โอเรียลทาล่า สปา</h1>
                                    <p>เริ่มต้นการเดินทางแห่งสุขภาพและการพักผ่อนของคุณที่โอเรียลทาล่า สปาที่เงียบสงบ สปาที่หรูหราของเรามีห้องนวดส่วนตัว 3 ห้อง พร้อมด้วยห้องอบไอน้ำที่จะช่วยให้คุณผ่อนคลาย เรามีแพ็คเกจเกี่ยวกับการนวด การบำรุงมากมายหลากหลาย บริการที่เป็นลักษณะพิเศษของเราคือการให้คำปรึกษาส่วนตัวโดยนักบำบัดผู้เชี่ยวชาญ ซึ่งจะช่วยค้นหาความสมดุลของธาตุในร่างกายคุณ นั่นคือไฟ, อากาศ, น้ำและดิน ส่งผลให้การบำบัดของคุณสมบูรณ์แบบและยังทำให้ร่างกายและจิตใจประสานกลมกลืนกัน</p>
                                    <p><span style="color: #516819;">เวลาเปิดให้บริการ : 10.00 - 22.00 น.</span></p>
                                    <p class="note">“Spa will be close during period on 18 – 31 March 2020”</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="dining" class="article" data-tab-name="Dining">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/dining.jpg" alt="Dining" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ร้านอาหาร</h1>
                                    <p>โรงแรมรามาด้า ภูเก็ต ดีวาน่ามีร้านอาหารมากมาย ซึ่งจะทำให้คุณได้เพลิดเพลินไปกับอาหารท้องถิ่นและอาหารนานาชาติในรูปแบบการจัดวางที่ทันสมัยและมีสไตล์ ท่านยังสามารถสั่งอาหารไปรับประทานในห้องพักเพื่อความเป็นส่วนตัวได้อีกด้วย</p>
                                    <p><span style="color: #516819;">Dinning is available during 06.30-23.00 hrs. (last order at 22.30 hrs.)
                                    <br>Buffet breakfast: opening during 06.30 – 10.30hrs.
                                    </span></p>
                                    <!-- <p><span style="color: #516819;">เวลาเปิดให้บริการ : 07.00 - 20.00 น.</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="the_cafe" class="article" data-tab-name="The Caf&eacute;">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/the_cafe.jpg" alt="The Caf&eacute;" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องอาหารเดอะ คาเฟ่</h1>
                                    <p>ตั้งอยู่บริเวณด้านข้างของล็อบบี้ เดอะคาเฟ่ตกแต่งแบบร่วมสมัย ท่านสามารถมองเห็นทิวทัศน์ของสวนของรีสอร์ทได้จากที่นี่ เปิดให้บริการอาหารเช้าแบบบุฟเฟต์นานาชาติ ประกอบด้วยซีเรียล, ผลไม้เมืองร้อน, เบเกอรี่, และอาหารเช้าแบบปรุงสุกทั้งรสชาติแบบเอเชียและตะวันตก</p>
                                    <p><span style="color: #516819;">Open: daily 06:30-23:00 hrs. (Last order at 22.30hrs)<br>Buffet breakfast: opening during 06:30-10:30 hrs.</span></p>
                                    <!-- <p><span style="color: #516819;">เวลาเปิดให้บริการ : 06.30 - 10.30 น.</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="bake_and_bev" class="article" data-tab-name="Bake &amp; Bev">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/bake_and_bev.jpg" alt="Bake &amp; Bev" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ร้านเบคแอนด์เบฝ</h1>
                                    <p>คุณสามารถเลือกของว่างและอาหาร ซึ่งสามารถส่งตรงไปยังห้องของคุณ ให้เป็นอาหารเช้าบนเตียงที่สมบูรณ์แบบหรือสำหรับค่ำคืนที่อุ่นสบายกับภาพยนตร์</p>
                                    <p><span style="color: #516819;">Open: daily 10.00 - 23.00 hrs. (Last order at 22.30hrs) </span></p>
                                    <!-- <p><span style="color: #516819;">เวลาเปิดให้บริการ : 07.00 - 23.00 น.</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="room_service" class="article" data-tab-name="ROOM SERVICE">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/room_service.jpg" alt="Room Service" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">บริการรูมเซอร์วิส</h1>
                                    <p>คุณสามารถเลือกของว่างและอาหาร ซึ่งสามารถส่งตรงไปยังห้องของคุณ ให้เป็นอาหารเช้าบนเตียงที่สมบูรณ์แบบหรือสำหรับค่ำคืนที่อุ่นสบายกับภาพยนตร์</p>
                                    <p><span style="color:#516819;">Open: 07.00-23.00. hrs. (Last order at 22.30hrs) </span></p>
                                    <!-- <p><span style="color:#516819;">เวลาเปิดให้บริการ : 07.00 - 23.00 น.</span></p> -->
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
            </div>
        </section>
    </section>

</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;

        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #be0d3a;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
        overflow-x: auto;
        white-space: nowrap;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #be0d3a;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #be0d3a;
    }
    .col-cap p.note {
        background-color: #f5f5f5;
        padding: 5px 10px;
        font-size: 12px;
        display: inline-block;
        border-radius: 2px;
    }
    @media (max-width: 1024px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 600px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
