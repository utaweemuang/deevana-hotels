<?php
$title = 'Facilities | Recenta Style | Official Hotel Group Website Thailand';
$desc = 'Facilities: 3 star chic hotel in Phuket town from USD 30 per night, enjoy best rate when book this city hotel directly.';
$keyw = 'facilities, Recenta Style phuket, Recenta Style, phuket, phuket city, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/recentastyle/facilities.php';
$lang_th = '/th/recentastyle/facilities.php';
$lang_zh = '/zh/recentastyle/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facilities-slide-01.jpg" alt="Recenta Style, 3-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">สิ่งอำนวยความสะดวกและบริการ</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#tour_desk" class="tab active">บริการนำเที่ยว</span>
                    <span data-tab="#transportation" class="tab">การเดินทาง</span>
                    <span data-tab="#free_wifi" class="tab">ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</span>
                    <span data-tab="#car_park" class="tab">สถานที่จอดรถ</span>
                    <span data-tab="#fitness" class="tab">ฟิตเนส</span>
                    <span data-tab="#swimming" class="tab">สระว่ายน้ำ</span>
                </div>
                
                <div class="tabs-content">
                    <article id="tour_desk" class="article" data-tab-name="Tour Desk">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tour_desk.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">บริการนำเที่ยว</h1>
                                    <p>สำรวจภูเก็ตซึ่งล้อมรอบไปด้วยผืนน้ำกับการเดินทางในช่วงกลางวันสู่ปลายทางที่งดงาม และเรียนรู้เกี่ยวกับวัฒนธรรมไทยที่เมืองเก่าภูเก็ต หรือชมการแสดงคาบาเรต์ และอื่นๆอีกมากมาย พวกเราสามารถจัดการทุกสิ่งทุกอย่างให้คุณรวมไปถึงกับรับส่งจากโรงแรม</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="transportation" class="article" data-tab-name="Transportation">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/transportation.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">การเดินทาง</h1>
                                    <p>บริการรับ-ส่งสนามบินจังหวัดภูเก็ตของเราปลอดภัยและเชื่อถือได้ กรุณาติดต่อเจ้าหน้าที่บริการลูกค้าเพื่อจัดการนัดหมายแท็กซี่รับส่งสนามบินและสถานที่อื่นๆ เรามีคนขับรถที่เชื่อถือได้รอให้บริการและนำท่านไปสู่ปลายทางอย่างปลอดภัย</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="free_wifi" class="article" data-tab-name="Free Wifi">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/free_wifi.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</h1>
                                    <p>ใช่แล้ว, ฟรี ! เราให้บริการอินเตอร์เน็ตไร้สาย (Wi-Fi) โดยไม่คิดค่าใช้จ่ายแก่ผู้เข้าพักของเราตลอดระยะเวลาการเข้าพัก</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="car_park" class="article" data-tab-name="Car Park">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/recentaa_2 re.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สถานที่จอดรถ</h1>
                                    <p>พวกเราให้บริการที่จอดรถส่วนตัวแก่ผู้เข้าพักของเราโดยไม่คิดค่าใช้จ่าย</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="fitness" class="article" data-tab-name="Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ฟิตเนส</h1>
                                    <p>ห้องฟิตเนสของเราตั้งอยู่บนชั้นแรก ผู้ที่ชื่นชอบการออกกำลังกายสามารถเข้ามาใช้บริการได้ตลอดเวลา เนื่องจากห้องฟิตเนสของเราเปิดให้บริการทุกวันตั้งแต่เวลา 07.00 - 21.00 น.</p>
                                    <p>ไม่อนุญาตให้เด็กอายุต่ำกว่า 12 ปีเข้าใช้บริการ</p>
                                    <p>เด็กอายุระหว่าง 12 ถึง 16 ปีต้องเข้ามาใช้บริการพร้อมกับผู้ใหญ่เท่านั้น</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="swimming" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ</h1>
                                    <p>ครอบครัวสามารถใช้เวลาด้วยกันในสระว่ายน้ำสำหรับผู้ใหญ่และสระว่ายน้ำสำหรับเด็กๆ</p>                                
                                    <p>เวลาเปิดให้บริการ : 7.00 - 21.00 น.</p>
                                    <p>หมายเหตุ : ไม่มีเจ้าหน้าที่ช่วยชีวิตอยู่ประจำการ</p>
                                </div>
                            </div>
                        </div>
                    </article>

                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #f6992a;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #707270;
    }
    @media (max-width: 840px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            cursor: pointer;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>