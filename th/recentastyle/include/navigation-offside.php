<div class="offside-menu-bg"></div>
<div id="offside-menu">
    <div class="inner">
        <div class="header">
            <a href="<?php echo get_info('url'); ?>">
                <img class="force" src="assets/elements/logo-with-location@2x.png" />
            </a>
        </div>

        <?php include('menu-main-menu.php'); ?>
    </div>

    <span class="close-menu clickable">
        <span class="bar bar-1"></span>
        <span class="bar bar-2"></span>
    </span>
</div>
