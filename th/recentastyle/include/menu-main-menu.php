<ul class="menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="index.php">หน้าหลัก</a></li>
    <li class="<?php echo get_current_class('accommodation'); ?>"><a href="accommodation.php">ห้องพัก</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">สิ่งอำนวยความสะดวก</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">สถานที่ท่องเที่ยว</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('310', 'en'); ?>" target="_blank">โปรโมชั่น</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">รูปภาพ</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">ติดต่อเรา</a></li>
</ul>
