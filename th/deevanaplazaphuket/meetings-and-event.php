<?php
$title = 'Meetings and Event | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'meetings and event, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazaphuket/meetings-and-event.php';
$lang_th = '/th/deevanaplazaphuket/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazaphuket/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">

    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/slide-meeting-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-02.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
            <div class="item"><img src="images/meetings/slide-meeting-03.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>

        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include('include/booking_bar.php'); ?>

    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">การประชุมและสัมมนา</h1>
            </header>

            <div class="section-content container">
                <p stlye="text-align:justify;">พื้นที่ที่สามารถปรับเปลี่ยนเพื่อจัดงานประชุมต่าง ๆ<br>ไม่ว่าจะเป็นการประชุมส่วนตัวหรือการประชุมบริษัท ระดับท้องถิ่นหรือภูมิภาค ภายในหรือภายนอกอาคาร ห้องพลาซ่า แกรนด์ บอลรูม ขนาดใหญ่หรือห้องพักขนาดเล็ก โรงแรมดีวาน่า พลาซ่า ภูเก็ต ป่าตอง ได้จัดเตรียมสิ่งอำนวยความสะดวกต่าง ๆ ภายในห้องประชุมที่สามารถปรับเปลี่ยนได้เพื่อการประชุมและการสัมมนาในรูปแบบต่าง ๆ</p>
                <p>สอบถามเพิ่มเติมได้ที่ โทร <a href="tel:+6676302100">+66 (0)76 302 100</a> หรืออีเมล <a href="mailto:info@deevanaplazaphuket.com">info@deevanaplazaphuket.com</a></p>
                <p>
                    <a class="button-default" href="http://www.deevanaplazaphuket.com/download/Meeting-Room-Floor-Plan.pdf" target="_blank">ดู Floor Plan</a>
                    <a class="button-default" href="http://www.deevanaplazaphuket.com/images/facilities/MEETING-PACKAGE-004.jpg" target="_blank">แพ็กเก็จ Meeting &amp; Wedding</a>
                </p>
            </div>

            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#plaza_grand_ballroom" class="tab active">พลาซ่า แกรนด์ บอลรูม</span>
                    <span data-tab="#square_i_meeting_room" class="tab">ห้องประชุมสแควร์ I</span>
                    <span data-tab="#square_ii_meeting_room" class="tab">ห้องประชุมสแควร์ II</span>
                    <span data-tab="#meeting_room_capacity" class="tab">ความจุของห้องประชุม</span>
                </div>

                <div class="tabs-content">
                    <article id="plaza_grand_ballroom" class="article" data-tab-name="Plaza Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/Plaza-Grand-Ballroom.jpg" width="1100" height="733" alt="Plaza Grand Ballroom" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">พลาซ่า แกรนด์ บอลรูม</h1>
                                    <p>ห้องบอลรูมในรูปแบบโรงภาพยนตร์พร้อมอุปกรณ์ระบบเสียงและภาพอันทันสมัยที่สุด เพื่อทำให้มั่นใจว่าสามารถใช้ระบบเสียงและภาพในการประชุมหรือสัมมนาขนาดใหญ่ได้อย่างราบรื่น</p>

                                    <h2 class="sub-title list-heading">อุปกรณ์</h2>
                                    <ul class="custom-list-dashed">
                                        <li>อุปกรณ์ฉายวิดีโอ</li>
                                        <li>จอ LCD</li>
                                        <li>ระบบแสงสว่างด้วยเทคโนโลยีระดับสูง</li>
                                        <li>อุปกรณ์ระบบเสียงและเครื่องขยายเสียง</li>
                                        <li>ห้องควบคุมแยกอิสระกัน 2 ห้อง</li>
                                    </ul>
                                    <p>
                                        พื้นที่ : 565 ตารางเมตร<br>
                                        ความจุ : 500 คน
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="square_i_meeting_room" class="article" data-tab-name="Square I Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/Square-Meeting-Room.jpg" width="1100" height="733" alt="Square I Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องประชุมสแควร์ I</h1>
                                    <p>สำหรับการจัดประชุมขนาดเล็กไปจนถึงขนาดกลางหรือใช้สำหรับแบ่งเป็นห้องประชุมย่อย โดยสองห้องนี้จะมีพื้นที่โถงหน้าห้องไว้สำหรับจัดอาหารว่าง ซึ่งสามารถมองเห็นสวนหย่อมข้างๆ ได้ </p>
                                    <p>พื้นที่ : 81.5 -113 ตารางเมตร</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="square_ii_meeting_room" class="article" data-tab-name="Square II Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="../../deevanaplazaphuket/uploads/2020/12/gallery-51.jpg" width="1500" height="936" alt="Square II Meeting Room" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องประชุมสแควร์ II</h1>
                                    <p>สำหรับการจัดประชุมขนาดเล็กไปจนถึงขนาดกลางหรือใช้สำหรับแบ่งเป็นห้องประชุมย่อย โดยสองห้องนี้จะมีพื้นที่โถงหน้าห้องไว้สำหรับจัดอาหารว่าง ซึ่งสามารถมองเห็นสวนหย่อมข้างๆ ได้</p>
                                    <p>พื้นที่ : 81.5 -113 ตารางเมตร</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="meeting_room_capacity" class="article" data-tab-name="Meeting Room Capacity">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">ความจุของห้องประชุม</h1>
                                    <p>ห้องประชุมของเราเหมาะสำหรับการจัดประชุมผู้บริหาร สัมมนา และงานแต่งงาน สามารถรองรับผู้ร่วมงานได้ถึง 500 ท่าน</p>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>ห้องประชุม</th>
                                                <th>พื้นที่ (ตร.ม.)</th>
                                                <th>ความสูงถึงเพดาน (ม)</th>
                                                <th data-hide="phone,tablet">รูปตัวยยู</th>
                                                <th data-hide="phone,tablet">สี่เหลี่ยมผืนผ้า</th>
                                                <th data-hide="phone,tablet">โรงภาพยนตร์</th>
                                                <th data-hide="phone,tablet">ห้องเรียน</th>
                                                <th data-hide="phone,tablet">งานเลี้ยง</th>
                                                <th data-hide="phone,tablet">ค็อกเทล</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>ห้องพลาซ่าแกรนด์บอลรูม</td>
                                                <td>565</td>
                                                <td>5</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>500</td>
                                                <td>200</td>
                                                <td>300</td>
                                                <td>600</td>
                                            </tr>

                                            <tr>
                                                <td>พลาซ่า I</td>
                                                <td>323</td>
                                                <td>5</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>120</td>
                                                <td>180</td>
                                                <td>300</td>
                                            </tr>

                                            <tr>
                                                <td>พลาซ่า II</td>
                                                <td>242</td>
                                                <td>5</td>
                                                <td>40</td>
                                                <td>40</td>
                                                <td>200</td>
                                                <td>100</td>
                                                <td>120</td>
                                                <td>300</td>
                                            </tr>

                                            <tr>
                                                <td>สแควร์ I</td>
                                                <td>113</td>
                                                <td>2.4</td>
                                                <td>30</td>
                                                <td>30</td>
                                                <td>50</td>
                                                <td>35</td>
                                                <td>50</td>
                                                <td>60</td>
                                            </tr>

                                            <tr>
                                                <td>สแควร์ II</td>
                                                <td>81.5</td>
                                                <td>2.4</td>
                                                <td>20</td>
                                                <td>20</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>40</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>

</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;

        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');

            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');

                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');

        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .section-header .excerpt {
        color: #666;
    }
    .section-header .section-title {
        margin-bottom: 0;
    }
    .section-content {
        margin-bottom: 60px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    .col-cap .list-heading {
        font-size: 14px;
        margin-top: 1em;
        margin-bottom: 3px;
    }
    .col-cap p.note {
        background-color: #eee;
        padding: 5px 10px;
        border-radius: 2px;
        color: #666;
        font-size: 12px;
        display: inline-block;
    }
    #meeting_room_capacity {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
