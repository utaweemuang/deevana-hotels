<?php
$title = 'Family Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Family Room: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'family room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-family-room';
$cur_page = 'family-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-family-room.php';
$lang_th = '/th/deevanaplazaphuket/room-family-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-family-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/family/1500/family-01.jpg" alt="Family Room 01" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 02" />
                    <img src="images/accommodations/family/1500/family-02.jpg" alt="Family Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องแฟมิลี่ <span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/family/600/family-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/family/600/family-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องแฟมิลี่</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/family/600/family-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>การได้ใช้เวลาร่วมกันเป็นแนวคิดในการออกแบบห้องแฟมิลี่ของโรงแรมดีวาน่า พลาซ่า ภูเก็ต โดยมีพื้นที่ส่วนตัวสำหรับคุณพ่อคุณแม่ พื้นที่สำหรับเด็กที่อยู่ติดกันซึ่งจะมีเตียงสองชั้นที่สามารถมองเห็นบรรยากาศตัวเมือง และพื้นที่สำหรับพักผ่อนเพื่อให้สมาชิกในครอบครัวได้นั่งเล่นและใช้เวลาสนุกสนานร่วมกัน
</p>
                            <p>
                                จำนวน : 10<br>
                                ห้องพื้นที่ : 44 ตารางเมตร
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงขนาดใหญ่ (6 ฟุต) พร้อมเตียงสองชั้น สำหรับเด็ก</li>
                                        <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                        <li>เครื่องเล่นเกมส์</li>
                                        <li>โต๊ะรับประทานอาหารพร้อมเก้าอี้</li>
                                        <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                        <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                        <li>บริการเสิร์ฟอาหารถึงห้องตลอด 24 ชั่วโมง</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i>อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงขนาดใหญ่ (6 ฟุต) พร้อมเตียงสองชั้น สำหรับเด็ก</li>
                                            <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                            <li>เครื่องเล่นเกมส์</li>
                                            <li>โต๊ะรับประทานอาหารพร้อมเก้าอี้</li>
                                            <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                            <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                            <li>บริการเสิร์ฟอาหารถึงห้องตลอด 24 ชั่วโมง</li>
                                            <li>วิวเมืองป่าตอง</li>
                                            <li>มินิบาร์</li>
                                            <li>เครื่องดื่มชาและกาแฟฟรี</li>
                                            <li>เครื่องเตือนและเครื่องตรวจจับควัน</li>
                                            <li>ระบบฉีดน้ำในห้องพักหากเกิดเพลิงไหม้</li>
                                            <li>ตู้เซฟระบบไฟฟ้า</li>
                                            <li>ปลั๊กไฟแบบสากล</li>
                                            <li>โคมไฟอ่านหนังสือ</li>
                                            <li>เครื่องชงกาแฟ</li>
                                            <li>นาฬิกาปลุก</li>
                                        </ul>
                                        
                                        <h2>อุปกรณ์ภายในห้องน้ำ</h2>
                                        <ul class="list-columns-2">
                                            <li>อ่างอาบน้ำและห้องอาบน้ำแยก</li>
                                            <li>เครื่องแต่งหน้า</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>เครื่องชั่งน้ำหนัก</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>