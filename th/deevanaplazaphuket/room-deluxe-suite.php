<?php
$title = 'Deluxe Suite | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Suite: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'deluxe suite, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-suite';
$cur_page = 'deluxe-suite';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-deluxe-suite.php';
$lang_th = '/th/deevanaplazaphuket/room-deluxe-suite.php';
$lang_zh = '/zh/deevanaplazaphuket/room-deluxe-suite.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-01.jpg" alt="Deluxe Suite 01" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-02.jpg" alt="Deluxe Suite 02" />
                    <img src="images/accommodations/deluxe-suite/1500/deluxe-suite-03.jpg" alt="Deluxe Suite 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องดีลักซ์สวีท<span>เตียงคิงไซส์</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe-suite/600/deluxe-suite-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องดีลักซ์สวีท</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-suite/600/deluxe-suite-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>หลีกหนีความวุ่นวายจากการทำงานในแต่ละวัน แล้วมาพักผ่อนยังเกาะที่มีแต่ความสะดวกสบายและคึกคักตลอดเวลา โดยห้องดีลักซ์สูทจะประกอบด้วยเลานจ์และระเบียงส่วนตัว ซึ่งผู้พักอาศัยสามารถนั่งมองสีสันและความเคลื่อนไหวของหาดป่าตองอย่างเงียบ ๆ ภายในห้อง และมีห้องนอนที่เงียบสงบอยู่หลังผนังกั้นซึ่งมีเตียงขนาดใหญ่ ตกแต่งด้วยสีเอิร์ทโทน เพื่อช่วยให้คุณนอนหลับฝันดี
</p>
                            <p>
                                จำนวน : 4 ห้อง<br>
                                พื้นที่ : 75 ตารางเมตร
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงขนาดใหญ่(6 ฟุต)</li>
                                        <li>โทรทัศน์ LCD ขนาด 42 นิ้ว ในห้องนอน และโทรทัศน์ LCD ขนาด 49 นิ้วในห้องรับแขก</li>
                                        <li>โต๊ะทำงาน</li>
                                        <li>โต๊ะรับประทานอาหารพร้อมเก้าอี้</li>
                                        <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                        <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                        <li>เครื่องเล่นดีวีดี</li>
                                        <li>บริการเสิร์ฟอาหารถึงห้องตลอด 24 ชั่วโมง</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i>อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงขนาดใหญ่(6 ฟุต)</li>
                                            <li>โทรทัศน์ LCD ขนาด 42 นิ้ว ในห้องนอน และโทรทัศน์ LCD ขนาด 49 นิ้วในห้องรับแขก</li>
                                            <li>โต๊ะทำงาน</li>
                                            <li>โต๊ะรับประทานอาหารพร้อมเก้าอี้</li>
                                            <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                            <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                            <li>เครื่องเล่นดีวีดี</li>
                                            <li>บริการเสิร์ฟอาหารถึงห้องตลอด 24 ชั่วโมง</li>
                                            <li>วิวสระว่ายน้ำ</li>
                                            <li>มินิบาร์</li>
                                            <li>เครื่องดื่มชาและกาแฟฟรี</li>
                                            <li>เครื่องเตือนและเครื่องตรวจจับควัน</li>
                                            <li>ระบบฉีดน้ำในห้องพักหากเกิดไฟไหม้</li>
                                            <li>ตู้เซฟระบบไฟฟ้า</li>
                                            <li>เครื่องชงกาแฟเอสเปรสโซ่</li>
                                            <li>ปลั๊กไฟแบบสากล</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>เตารีดและที่รองรีด</li>
                                        </ul>
                                        
                                        <h2>อุปกรณ์ภายในห้องน้ำ</h2>
                                        <ul class="list-columns-2">
                                            <li>อ่างอาบน้ำและห้องอาบน้ำแยก</li>
                                            <li>เครื่องแต่งหน้า</li>
                                            <li>มุมแต่งตัว</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>เครื่องชั่งน้ำหนัก</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>