<?php
header("location: ./deluxe-city-view.php", true, 301);
?>

<?php
$title = 'Deluxe Room | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room | Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'deluxe room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page = 'deluxe-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-deluxe-room.php';
$lang_th = '/th/deevanaplazaphuket/room-deluxe-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-02.jpg" alt="Deluxe Room 02" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องดีลักซ์<span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องดีลักซ์</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>หลีกหนีความวุ่นวายมายังเกาะที่เงียบสงบซึ่งเหมาะสำหรับการพักผ่อนส่วนตัวหรือคู่รัก ห้องดีลักซ์สามารถปรับพื้นที่ภายในห้องเพื่อการพักผ่อนยามบ่ายได้ทั้งภายในอาคารและนอกอาคาร มีให้เลือกทั้งเตียงเดี่ยวหรือเตียงคู่ ระเบียงห้องพักส่วนตัวที่สามารถมองเห็นวิวเมืองป่าตองหรือสระว่ายน้ำของโรงแรมดีวาน่า พลาซ่า ภูเก็ต การตกแต่งภายในจะเน้นโทนสีขาว และพื้นที่โล่งด้วยการตกแต่งจากจินตนาการ โดยผนังห้องน้ำครึ่งหนึ่งเป็นกระจกใสซึ่งสามารถมองเห็นวิวจากภายนอกห้อง เปรียบเสมือนวอลล์เปเปอร์ที่เปลี่ยนแปลงสีสันอยู่ตลอดเวลา</p>
                            <p>
                                จำนวน : 209 ห้อง<br/>
                                ขนาด : 35 ตารางเมตร
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงขนาดใหญ่ (6 ฟุต) หรือเตียงขนาดเล็ก 2 เตียง (4 ฟุต)</li>
                                        <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                        <li>โต๊ะกลาง</li>
                                        <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                        <li>โทรศัพท์ IDD พร้อมข้อความเสียง</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i>อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงขนาดใหญ่ (6 ฟุต) หรือเตียงขนาดเล็ก 2 เตียง (4 ฟุต)</li>
                                            <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                            <li>โต๊ะกลาง</li>
                                            <li>อินเทอร์เน็ตไร้สายฟรี</li>
                                            <li>โทรศัพท์ IDD พร้อมข้อความเสียง</li>
                                            <li>บริการเสิร์ฟอาหารถึงห้องตลอด 24 ชั่วโมง</li>
                                            <li>วิวสระว่ายน้ำหรือวิวเมืองป่าตอง</li>
                                            <li>มินิบาร์</li>
                                            <li>เครื่องดื่มชาและกาแฟฟรี</li>
                                            <li>เครื่องเตือนและเครื่องตรวจจับควัน</li>
                                            <li>ระบบฉีดน้ำในห้องพักหากเกิดเพลิงไหม้</li>
                                            <li>ตู้เซฟระบบไฟฟ้า</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>โคมไฟอ่านหนังสือ</li>
                                            <li>ปลั๊กไฟแบบสากล</li>
                                        </ul>
                                        
                                        <h2>อุปกรณ์ภายในห้องน้ำ</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องเป่าผม</li>
                                            <li>อ่างอาบน้ำและห้องอาบน้ำแยก</li>
                                            <li>กระจกแต่งหน้า</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>