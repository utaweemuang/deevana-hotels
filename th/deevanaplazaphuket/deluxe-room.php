<?php
//
// Room information
// 
$room_title     = "ห้องดีลักซ์";
$room_desc      = "<p>หลีกหนีความวุ่นวายมายังเกาะที่เงียบสงบซึ่งเหมาะสำหรับการพักผ่อนส่วนตัวหรือคู่รัก 
ห้องดีลักซ์สามารถปรับพื้นที่ภายในห้องเพื่อการพักผ่อนยามบ่ายได้ทั้งภายในอาคารและนอกอาคาร มีให้เลือกทั้งเตียงเดี่ยวหรือเตียงคู่ 
ระเบียงห้องพักส่วนตัวที่สามารถมองเห็นสระว่ายน้ำของโรงแรมดีวาน่า พลาซ่า ภูเก็ต การตกแต่งภายในจะเน้นโทนสีขาว 
และพื้นที่โล่งด้วยการตกแต่งจากจินตนาการ โดยผนังห้องน้ำครึ่งหนึ่งเป็นกระจกใสซึ่งสามารถมองเห็นวิวจากภายนอกห้อง 
เปรียบเสมือนวอลล์เปเปอร์ที่เปลี่ยนแปลงสีสันอยู่ตลอดเวลา</p>
<p><strong>ห้องดีลักซ์วิวสระน้ำเป็นห้องปลอดบุหรี่</strong></p>";
$room_available = "209";
$room_space     = "35 ตร.ม.";
$room_beds      = "เตียงขนาดคิงไซส์หรือเตียงคู่";
$room_amenities = array(
  "เตียงคิงไซซ์ขนาด 6 ฟุค หรือ เตียงขนาดเล็ก 2 เตียงขนาด 4 ฟุต",
  "โทรทัศน์ LCD ขนาด 42 นิ้ว",
  "โต๊ะกลาง",
  "อินเทอร์เน็ตไร้สาย",
  "โทรศัพท์ IDD พร้อมข้อความเสียง",
  "ทิวทัศน์สระน้ำ",
  "ชาและกาแฟฟรี",
  "เครื่องตรวจจับและส่งสัญญาณควันไฟ",
  "ระบบดับเพลิงด้วยสปริงเกิ้ล",
  "ตู้เซฟ",
  "นาฬิกาปลุก",
  "โคมไฟตั้งโต๊ะ",
  "กาต้มน้ำและเครื่องชงกาแฟ",
  "ปลั๊กไฟอเนกประสงค์",
  "อุปกรณ์ห้องน้ำ",
  "เครื่องเป่าผม",
  "อ่างอาบน้ำและฝากบัวแยกกัน",
  "กระจกเงาแบบขยาย",
);
$room_featured = array("../../deevanaplazaphuket/images/accommodations/deluxe/600/deluxe-01.jpg", 600, 400, $room_title);
$room_gallery_large = array(
  array("../../deevanaplazaphuket/images/accommodations/deluxe/1500/deluxe-01.jpg", 1500, 1000, "{$room_title} 1" ),
  array("../../deevanaplazaphuket/images/accommodations/deluxe/1500/deluxe-02.jpg", 1500, 1000, "{$room_title} 2" ),
  array("../../deevanaplazaphuket/images/accommodations/deluxe/1500/deluxe-03.jpg", 1500, 1000, "{$room_title} 3" ),
);
$room_gallery_thumb = array(
  array("../../deevanaplazaphuket/images/accommodations/deluxe/600/deluxe-01.jpg", 600, 400, "{$room_title} 1" ),
  array("../../deevanaplazaphuket/images/accommodations/deluxe/600/deluxe-02.jpg", 600, 400, "{$room_title} 2" ),
  array("../../deevanaplazaphuket/images/accommodations/deluxe/600/deluxe-03.jpg", 600, 400, "{$room_title} 3" ),
);
// 
// End room information
// 
?>

<?php
$title = "{$room_title} | Deevana Plaza Phuket | Official Hotel Group Website Thailand";
$desc = "{$room_title} | Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street";
$keyw = "{strtolower($room_title}, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach";

$html_class = '';
$body_class = 'room room-deluxe-pool-view';
$cur_page = 'deluxe-pool-view';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/deluxe-pool-view.php';
$lang_th = '/th/deevanaplazaphuket/deluxe-pool-view.php';
$lang_zh = '/zh/deevanaplazaphuket/deluxe-pool-view.php';

include_once('_header.php');
?>

<main class="site-main">
  <div class="room-content">

    <div class="room-slides-wrap disable-touch">
      <div class="room-slides">
        <div class="slides-container">
          <?php foreach( $room_gallery_large as $image ) : ?>
          <img src="<?= $image[0] ?>" width="<?= $image[1] ?>" height="<?= $image[2] ?>" alt="<?= $image[3] ?>" />
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <div class="room-slides-thumbs">
      <h2 class="title"><?= $room_title ?> <span><?= $room_beds ?></span></h2>
      <div class="thumbs">
        <ul>
          <?php foreach( $room_gallery_large as $index => $image ) : ?>
          <li class="<?php if ($index===0) {echo "current";} ?>"><img src="<?= $image[0] ?>" width="<?= $image[1] ?>" height="<?= $image[2] ?>" alt="<?= $image[3] ?>" /></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
    </div>

    <div class="room-info">
      <div class="table">
        <div class="content table-cell">
          <h1 class="title"><?= $room_title ?></h1>

          <div class="row">
            <div class="col-w3 col-thumbnail">
              <?php echo sprintf('<img class="room-preview force" src="%s" width="%s" height="%s" alt="%s" />',
                  $room_featured[0],
                  $room_featured[1],
                  $room_featured[2],
                  $room_featured[3]
                ); ?>

              <a class="button clickable book-this-room-button desktop"
                href="<?php ibe_url( get_info('ibeID'), 'th' ); ?>" target="_blank">จองห้องพัก</a>
            </div>

            <div class="col-w5 col-info">
              <?= $room_desc ?>
              <p>
                ขนาด: <?= $room_space ?>
              </p>
              <a class="button clickable book-this-room-button mobile"
                href="<?php ibe_url( get_info('ibeID'), 'th' ); ?>" target="_blank">จองห้องพัก</a>
            </div>

            <div class="col-w4 col-amenities">
              <div class="amenities">
                <div class="shad-over">
                  <h2 class="title">สิ่งอำนวยความสะดวกภายในห้อง</h2>
                  <ul class="amenities-list">
                    <?php
                    for($i = 0; $i < 5; $i++) {
                      echo "<li>{$room_amenities[$i]}</li>";
                    }
                    ?>
                    <li class="more clickable">
                      <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่น ๆ</a>
                    </li>
                  </ul>

                  <div id="all_amenities" class="mfp-hide popup">
                    <h2>สิ่งอำนวยความสะดวกภายในห้อง</h2>
                    <ul class="list-columns-2">
                      <?php
                      foreach( $room_amenities as $item ) {
                        echo "<li>{$item}</li>";
                      }
                      ?>
                    </ul>
                  </div>
                </div>

                <span class="shad-left"></span>
                <span class="shad-right"></span>
              </div>
            </div>
          </div>
        </div>

        <div class="booking table-cell">
          <?php include('include/room-booking-form.php'); ?>
        </div>
      </div>

      <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
    </div>

  </div>
</main>

<?php include_once('_footer.php'); ?>