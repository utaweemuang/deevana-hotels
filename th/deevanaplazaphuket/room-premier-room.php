<?php
$title = 'Premier Waterfront | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Premier Waterfront: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'premier room, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazaphuket/room-premier-room.php';
$lang_th = '/th/deevanaplazaphuket/room-premier-room.php';
$lang_zh = '/zh/deevanaplazaphuket/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Premier Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Premier Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Premier Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">ห้องพรีเมียร์ริมสระน้ำ<span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องพรีเมียร์ริมสระน้ำ</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>สะดวกสบายด้วยสิ่งอำนวยความสะดวกและการบริการพิเศษที่ออกแบบมาโดยเฉพาะ เช่น บริการน้ำอัดลมและของว่างยามบ่ายฟรีที่พรีเมียร์ เลานจ์ ซึ่งจะทำให้การพักอาศัยของคุณสมบูรณ์แบบและประทับมากยิ่งขึ้น
</p>
                            <p>
                                จำนวน: 26 ห้อง<br>
                                พื้นที่ : 35 ตารางเมตร
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                    <ul class="amenities-list">
                                        <li>เตียงขนาดใหญ่ (6 ฟุต) หรือเตียงขนาดเล็ก 2 เตียง (4 ฟุต)</li>
                                        <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                        <li>เก้าอี้ภายในห้องรับแขก</li>
                                        <li>นาฬิกาปลุก</li>
                                        <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                        <li>บริการเสิร์ฟอาหารและรับประทานอาหารภายในห้องตลอด 24 ชั่วโมง</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกภายในห้อง</h2>
                                        <ul class="list-columns-2">
                                            <li>เตียงขนาดใหญ่ (6 ฟุต) หรือเตียงขนาดเล็ก 2 เตียง (4 ฟุต)</li>
                                            <li>โทรทัศน์ LCD ขนาด 42 นิ้ว</li>
                                            <li>เก้าอี้ภายในห้องรับแขก</li>
                                            <li>นาฬิกาปลุก</li>
                                            <li>โทรศัพท์ IDD พร้อมฝากข้อความเสียง</li>
                                            <li>บริการเสิร์ฟอาหารและรับประทานอาหารภายในห้องตลอด 24 ชั่วโมง</li>
                                            <li>วิวสระว่ายน้ำ</li>
                                            <li>เครื่องดื่มชาและกาแฟฟรี</li>
                                            <li>เครื่องเตือนและเครื่องตรวจจับควัน</li>
                                            <li>ระบบฉีดน้ำในห้องพักหากเกิดเพลิงไหม้</li>
                                            <li>ตู้เซฟระบบไฟฟ้า</li>
                                            <li>เตารีดพร้อมที่รองรีด</li>
                                            <li>โคมไฟอ่านหนังสือ</li>
                                            <li>เครื่องชงกาแฟ</li>
                                            <li>ปลั๊กไฟแบบสากล</li>
                                        </ul>
                                        
                                        <h2>อุปกรณ์ภายในห้องน้ำ</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องเป่าผม</li>
                                            <li>อ่างอาบน้ำและห้องอาบน้ำแยก</li>
                                            <li>กระจกแต่งหน้า</li>
                                            <li>เครื่องชั่งน้ำหนัก</li>
                                        </ul>
                                        
                                        <h2>สิทธิพิเศษเพิ่มเติมเมื่อคุณอัปเกรดห้องพักเป็นห้องพรีเมียร์ ริมสระน้ำ</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องดื่มต้อนรับเมื่อมาถึงโรงแรม</li>
                                            <li>ผลไม้ตามฤดูกาลสำหรับการต้อนรับเมื่อมาถึงโรงแรม</li>
                                            <li>มินิบาร์ฟรี รวมถึงเครื่องดื่มแอลกอฮอล์และขนมขบเคี้ยว</li>
                                            <li>ชายามบ่ายที่พรีเมียร์ เลานจ์ (เวลา 15.30 น. ถึง 16.30 น.)</li>
                                            <li>บริการหนังสือ</li>
                                            <li>เครื่องใช้ในห้องน้ำเพิ่มเติม</li>
                                            <li>เครื่องใช้ภายในห้องเพิ่มเติม</li>
                                            <li>ซักหรือรีดชุดลำลองหนึ่งชุดต่อวันฟรี (ไม่รวมถึงการซักแห้ง)</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>