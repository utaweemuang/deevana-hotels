<?php
$title = 'Facilities | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'facilities, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazaphuket/facilities.php';
$lang_th = '/th/deevanaplazaphuket/facilities.php';
$lang_zh = '/zh/deevanaplazaphuket/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/cover-facilities-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">สิ่งอำนวยความสะดวก</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#restaurant" class="tab active">ร้านอาหาร</span>
                    <span data-tab="#bar_and_lounge" class="tab">บาร์และเลาจ์</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">โอเรียลทาล่า สปา</span>
                    <span data-tab="#fitness_center" class="tab">ศูนย์ฟิตเนส</span>
                    <span data-tab="#kids_club" class="tab">ห้องเด็กเล่น</span>
                    <span data-tab="#swimming_pool" class="tab">สระว่ายน้ำ</span>
                </div>
                
                <div class="tabs-content">
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <h1 class="title">ร้านอาหาร</h1>

                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/cafe.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h2 class="sub-title">ภูเก็ตคาเฟ่ (ปิดชั่วคราว)</h2>
                                    <p>การสัมผัสประสบการณ์ในการรับประทานอาหารที่ดีวาน่า พลาซ่า ภูเก็ต เป็นสิ่งที่คุณไม่ควรพลาด ตั้งแต่ร้านอาหารภูเก็ตคาเฟ่ ซันบาร์ รูฟท็อปเทอเรซ และเดอะเลานจ์ เรามีบริการเสิร์ฟอาหารถึงห้องพักซึ่งเปิดให้บริการตั้งแต่ 8:00 น. ถึง 21:00 น. (ปิดรับออร์เดอร์ 20:30 น.)</p>
                                    <p>เวลาทำการ: 07:00 น. - 22.00 น.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="bar_and_lounge" class="article" data-tab-name="Bar &amp; Lounge">
                        <h1 class="title">บาร์และเลาจ์</h1>

                        <div class="container">



                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/the-lounge.jpg" alt="the lounge"/><br/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h2 class="sub-title">เดอะเลานจ์ (ปิดชั่วคราว)</h2>
                                    <p>เดอะเลานจ์จะอยู่ใกล้กับล็อบบีที่ให้บริการเครื่องดื่ม เป็นมุมพักผ่อนเพื่อการพบปะสังสรรค์และผ่อนคลาย กับเครื่องดื่มค็อกเทลสูตรใหม่และเป็นเครื่องดื่มเฉพาะของโรงแรมที่มีให้เลือกมากมาย เครื่องดื่มแอลกอฮอล์และแชมเปญที่เสิร์ฟบนบาร์แบบเปิดพร้อมวงดนตรีเล่นสด</p>
                                    <p>เวลาที่เปิดให้บริการ : 10.00 น. ถึงเที่ยงคืน</p>
                                </div>
                            </div>




                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/sun-bar.jpg" alt="sun bar"/><br/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h2 class="sub-title">ซันบาร์ (ปิดชั่วคราว)</h2>
                                    <p>ที่ซันบาร์มีทั้งเครื่องดื่มและอาหารรสเลิศมากมายคอยให้บริการ ไม่ว่าจะเป็นอาหารจานด่วนหรือาหารจานหลัก ค็อกเทลหรือม็อกเทลที่เสิร์ฟริมสระน้ำหรือกลางสระน้ำ หรือเป็นตัวเลือกสำหรับผู้ที่ชื่นชอบการอาบแดดและพักผ่อนริมสระว่ายน้ำ ซันบาร์ตั้งอยู่ใกล้กับสระว่ายน้ำหลัก และอยู่ท่ามกลางสิ่งอำนวยความสะดวกต่าง ๆ ทั้งหมดของโรงแรม มีบริการเครื่องดื่มและขนมขบเคี้ยวตามแบบฉบับนานาชาติ</p>
                                    <p>เวลาที่เปิดให้บริการ : 10.00 ถึง 19.00 น.</p>
                                </div>
                            </div>



                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/rooftop-terrace.jpg" alt="Rooftop Terrace"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h2 class="sub-title">รูฟท็อปเทอเรซ (ปิดชั่วคราว)</h2>
                                    <p>รูฟท็อปเทอเรซตั้งอยู่บนชั้น 6 ซึ่งตกแต่งอย่างลงตัว บาร์ที่ต้อนรับคู่รักและเพื่อน ๆ ที่ต้องการนั่งดื่มขณะชมพระอาทิตย์ตกดินหรือนั่งมองดาวเวลากลางคืน นอกจากนี้ รูฟท็อปเทอเรซยังให้บริการจัดงานเลี้ยงส่วนตัวด้วยเช่นกัน</p>
                                </div>
                            </div>



                        </div>
                    </article>
                    
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">โอเรียลทาล่า สปา (ปิดชั่วคราว)</h1>
                                    <p>เพื่อช่วยฟื้นฟูความสมดุลระหว่างร่างกายและจิตใจ โอเรียนทาล่า สปาที่โรงแรมดีวาน่า พลาซ่า ภูเก็ต ป่าตอง จึงมีโปรแกรมบำบัดมากมายให้คุณเลือก เพื่อการฟื้นฟูและเพิ่มความสดใสทั้งทางร่างกายและจิตใจ</p>
                                    
                                    <h2 class="sub-title">สิ่งอำนวยความสะดวกภายในสปา</h2>
                                    <ul class="custom-list-dashed">
                                        <li>ห้องบำบัดสปา 5 ห้อง (ห้องเดี่ยว 3 ห้อง และห้องคู่ 2 ห้องพร้อมอ่างจากุซซี่)</li>
                                        <li>ล็อกเกอร์ส่วนตัวและห้องอบไอน้ำแยกชายและหญิง</li>
                                        <li>บริการบำรุงผิวหน้าและร้านขายสินค้าบำรุงผิวหน้า</li>
                                        <li>เครื่องบำรุงผิวหน้า 2 ชุด</li>
                                    </ul>
                                    
                                    <h2 class="sub-title">โอเรียลทาล่า สปา</h2>
                                    
                                    <h3 class="list-heading">
                                        สปาความสุข<br>
                                        <small>ระยะเวลา : 150 นาที ราคา = 3,000 บาท</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>ชาสมุนไพร</li>
                                        <li>อบไอน้ำ</li>
                                        <li>ขัดผิวตัว</li>
                                        <li>นวดน้ำมันอโรมา</li>
                                        <li>นวดไทย</li>
                                        <li>คุกกี้และชา</li>
                                    </ul>
                                    <p class="note">หมายเหตุ : อบไอน้ำเพิ่ม 15 นาที โดยไม่คิดค่าใช้จ่าย</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        การฟื้นฟู<br>
                                        <small>ระยะเวลา : 150 นาที ราคา = 3,000 บาท</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>ชาสมุนไพร</li>
                                        <li>อบไอน้ำ</li>
                                        <li>ขัดผิวตัว</li>
                                        <li>นวดน้ำมันอโรมา</li>
                                        <li>ทำให้ใบหน้าชุมฉ่ำด้วยว่านหางจระเข้</li>
                                        <li>คุกกี้และชา</li>
                                    </ul>
                                    <p class="note">หมายเหตุ : อบไอน้ำเพิ่ม 15 นาที โดยไม่คิดค่าใช้จ่าย</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        การบรรเทา<br>
                                        <small>ระยะเวลา : 120 นาที ราคา = 1,700 บาท</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>ชาสมุนไพร</li>
                                        <li>อบไอน้ำ</li>
                                        <li>นวดไทย</li>
                                        <li>นวดน้ำมันอโรมา</li>
                                        <li>นวดเท้า</li>
                                        <li>คุกกี้และชา</li>
                                    </ul>
                                    <p class="note">หมายเหตุ : อบไอน้ำเพิ่ม 15 นาที โดยไม่คิดค่าใช้จ่าย</p>
                                    
                                    <hr>
                                    
                                    <h3 class="list-heading">
                                        กลับคืนสู่ธรรมชาติ<br>
                                        <small>ระยะเวลา : 150 นาที ราคา = 2,400 บาท</small>
                                    </h3>
                                    <ul class="custom-list-dashed list-columns-2">
                                        <li>ชาสมุนไพร</li>
                                        <li>อบไอน้ำ</li>
                                        <li>ขัดผิวตัว</li>
                                        <li>นวดไทย</li>
                                        <li>นวดเท้า</li>
                                        <li>คุกกี้และชา</li>
                                    </ul>
                                    <p class="note">หมายเหตุ : อบไอน้ำเพิ่ม 15 นาที โดยไม่คิดค่าใช้จ่าย</p>
                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="fitness_center" class="article" data-tab-name="Fitness Center">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ศูนย์ฟิตเนส (ปิดชั่วคราว)</h1>
                                    <p>ตั้งอยู่บนชั้น 3 ของโรงแรม เพียบพร้อมไปด้วยอุปกรณ์ออกกำลังกายอันทันสมัยครบครันพร้อมวิวสุดประทับใจของชายหาดป่าตอง</p>
                                    
                                    <h2 class="sub-title list-heading">อุปกรณ์</h2>
                                    <ul class="custom-list-dashed">
                                        <li>ลู่วิ่ง 2 เครื่อง</li>
                                        <li>เครื่องเดินวงรี 1 เครื่อง</li>
                                        <li>จักรยานปั่นตั้งตรง 1 เครื่อง</li>
                                        <li>กรรเชียงบก 1 เครื่อง</li>
                                        <li>เก้าอี้ AB/สามารถปรับเอียง 1 ตัว ที่รองขาสามารถขาย/โค้ง 1 ชิ้น</li>
                                        <li>เก้าอี้ปรับเอียง 1 ตัว</li>
                                        <li>โต๊ะปิงปอง 1 ตัว</li>
                                        <li>ชั้นวางพร้อมดัมเบล 1 ชุด</li>
                                        <li>เครื่องยกน้ำหนักอเนกประสงค์ 1 ชุด</li>
                                        <li>เครื่องดึงอเนกประสงค์ 1 ชุด</li>
                                        <li>ที่นั่งพร้อมที่รองขาแบบยืด/โค้ง 1 ชุด</li>
                                    </ul>
                                    <p>
                                        เวลาที่เปิดให้บริการ : 24 ชั่วโมง<br>
                                        มีบริการผ้าขนหนูและล็อกเกอร์<br>
                                        ไม่อนุญาตให้เด็กอายุต่ำกว่า 12 ปีใช้บริการ<br>
                                        เด็กที่มีอายุระหว่าง 12 ถึง 16 ปี ต้องมีผู้ใหญ่ออกกำลังกายควบคู่ไปด้วย<br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="kids_club" class="article" data-tab-name="Kids Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/kids-club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องเด็กเล่น (ปิดชั่วคราว)</h1>
                                    <p>โลกของความสนุกและน่าตื่นเต้นกำลังรอให้บริการสำหรับแขกตัวน้อย ๆ ของเราที่สโมสรเด็ก ซึ่งเด็ก ๆ สามารถปลดปล่อยความคิดสร้างสรรค์และจิตนาการให้โลดแล่นอย่างไม่มีขีดจำกัด ตั้งอยู่ใกล้กับล็อบบีโรงแรม ภายในสโมสรเด็กสามารถออกไปยังสระว่ายน้ำสำหรับเด็กได้ทันที “สโมสรเด็กยินดีต้อนรับเด็ก ๆ ที่มีอายุตั้งแต่ 4 ถึง 12 ปี เด็ก ๆ จะต้องอยู่ในการดูแลของผู้ปกครอง</p>
                                    
                                    <h2 class="sub-title list-heading">สิ่งอำนวยความสะดวก</h2>
                                    <ul class="custom-list-dashed">
                                        <li>โทรทัศน์ LCD</li>
                                        <li>วิดีโอเกม</li>
                                        <li>โต๊ะฟุตบอล</li>
                                        <li>โต๊ะกิจกรรม</li>
                                    </ul>
                                    <p>เวลาเปิดให้บริการ : 09.00 ถึง 18.00 น.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ (ปิดชั่วคราว)</h1>
                                    <p>สระว่ายน้ำให้บริการทุกวันตั้งแต่เวลา : 7.00 - 21.00 น</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .article > .title {
        color: #78a321;
        text-align: center;
        margin-bottom: 40px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 18px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        text-transform: uppercase;
        margin-top: 3px;
        margin-bottom: 10px;
    }
    .col-cap p {
        margin-top: 3px;
        margin-bottom: 10px;
    }
    .col-cap .list-heading {
        font-size: 14px;
        margin-top: 1em;
        margin-bottom: 3px;
    }
    .col-cap .list-heading + ul {
        margin-top: 0;
    }
    .col-cap p.note {
        background-color: #eee;
        padding: 5px 10px;
        border-radius: 2px;
        color: #666;
        font-size: 12px;
        display: inline-block;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>