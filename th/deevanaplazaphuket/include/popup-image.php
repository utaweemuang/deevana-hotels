<!-- <script>
    ;(function($) {
        var popup = {
            enabled: true, //Change this to false for disabled popup
            src: 'http://www.deevanaplazaphuket.com/images/Lets-Make-fun-Winter-Story.jpg',
            link: {
                enabled: true,
                href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDg2OTk0',
                target: '_blank',
            }
        }
        if (popup.enabled === true) {
            $.magnificPopup.open({
                items: {
                    src: popup.src,
                    type: 'image',
                },
                mainClass: 'mfp-fade',
                removalDelay: 300,
                callbacks: {
                    open: function () {
                        if (popup.link.enabled === true) {
                            var $img = $(this.content).find('.mfp-img');
                            $img.wrap('<a href="' + popup.link.href + '" target="' + popup.link.target + '" />');
                        }
                    }
                }
            });
        }
    })(jQuery);
</script> -->

<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.25/moment-timezone-with-data-2012-2022.min.js'></script>
<script>
    ;(function($) {
      var today = moment().tz('Asia/Bangkok');
      var popup1 = {
    enabled: true,
    image: {
      src: 'http://deevanaplazaphuket.com/images/banner/Phuket-calling.jpg',
    },
    link: {
      enabled: true,
      href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDc0MjIzMA%3D%3D',
      target: '_blank',
    }
      }
      if (popup1.enabled===true && today.isBetween('2021-11-01 00:00:00', '2022-12-31 23:59:59')) {
        $.magnificPopup.close({
          items: {
            type: 'image',
            src: popup1.image.src,
          },
          callbacks: {
            open: function() {
              if (popup1.link.enabled===true) {
                $(this.content).find('.mfp-img').wrap('<a href="'+popup1.link.href+'" target="'+popup1.link.target+'" />');
              }
            },
          },
        });
      }
      var popup2 = {
    enabled: false,
    image: {
      src: 'http://deevanaplazaphuket.com/images/earlybird2021.jpg',
    },
    link: {
      enabled: true,
      href: 'https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&checkin=2022-02-25&checkout=2022-02-27&numofadult=2&numofchild=0&numofroom=1&pid=MDc0MTgwNXwwNzQxODA2',
      target: '_blank',
    }
      }
      if (popup2.enabled===true && today.isBetween('2021-12-01 00:00:00', '2021-12-31 23:59:59')) {
        $.magnificPopup.open({
          items: {
            type: 'image',
            src: popup2.image.src,
          },
          callbacks: {
            open: function() {
              if (popup2.link.enabled===true) {
                $(this.content).find('.mfp-img').wrap('<a href="'+popup2.link.href+'" target="'+popup2.link.target+'" />');
              }
            },
          },
        });
      }
    console.log(today.isBetween('2019-06-01 00:00:00', '2021-12-31 23:59:59'));
    })(jQuery);
</script>
