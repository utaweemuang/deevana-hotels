<?php

session_start();

$title = 'Deevana Plaza Phuket Patong | Official Hotel Group Website Thailand';
$desc = '4 star hotel near Jungceylon and bangla street; Guarantee best direct hotel rate and best location on Patong Beach';
$keyw = 'deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach, Bangla Road, phuket night market, Jungceylon';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/deevanaplazaphuket';
$lang_th = '/th/deevanaplazaphuket';
$lang_zh = '/zh/deevanaplazaphuket';

include '_header.php';
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Deevana Plaza Phuket Patong, 4-star hotel beach" /></div>
        </div>

        <!--div id="promotion_board" class="promotion get-center">
            <a href="<?php //ibe_url(275, 'en'); ?>" target="_blank">
                <img class="block responsive" src="images/home/hero_banner/banner.png" />
            </a>
        </div-->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</span></span>
                        </h1>
                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
                        <p>ความสมบูรณ์แบบที่ลงตัวสำหรับการพักผ่อนบนเกาะสวรรค์ใจกลางหาดป่าตอง ภูเก็ต
                        ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง ตั้งอยู่ใจกลางป่าตอง และอยู่ไม่ไกลจากหาดป่าตองที่มีชื่อเสียงระดับโลก ห้างสรรพสินค้า แหล่งซื้อของยามค่ำคืน และแหล่งบันเทิงยามราตรีของเกาะภูเก็ต ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง เป็นสถานที่รวมความแตกต่างและความร่วมสมัยบนความเรียบง่ายของป่าตองเข้าไว้ด้วยกันได้อย่างลงตัว
                        ห้องพักและห้องสูทตกแต่งอย่างมีรสนิยมทั้งหมด 249 ห้อง, โอเรียนทาลา สปา ทรีตเมนต์, ลิ้มลองอาหารไทยต้นตำรับและอาหารนานาชาติรสเลิศที่ห้องอาหารภูเก็ตคาเฟ่บริเวณป่าตอง และเทคโนโลยีสำหรับการประชุมและสัมมนาอันทันสมัย ตัวโรงแรมมีความโดดเด่นและเหมาะสำหรับการพักผ่อนแบบส่วนตัว คู่รัก ครอบครัว และนักธุรกิจที่มาทำธุรกิจซึ่งต้องการมาพักผ่อนที่ภูเก็ต
                        ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง ให้การบริการระดับมืออาชีพและมีคุณภาพ คุณจะได้รับการบริการและการต้อนรับแบบไทย ๆ อันเป็นเอกลักษณ์ของทางโรงแรมเมื่อคุณมาพักที่นี่</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/hQN7RaQBBA4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
						<p>ความสมบูรณ์แบบที่ลงตัวสำหรับการพักผ่อนบนเกาะสวรรค์ใจกลางหาดป่าตอง ภูเก็ต
ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง ตั้งอยู่ใจกลางป่าตอง และอยู่ไม่ไกลจากหาดป่าตองที่มีชื่อเสียงระดับโลก ห้างสรรพสินค้า แหล่งซื้อของยามค่ำคืน และแหล่งบันเทิงยามราตรีของเกาะภูเก็ต ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง เป็นสถานที่รวมความแตกต่างและความร่วมสมัยบนความเรียบง่ายของป่าตองเข้าไว้ด้วยกันได้อย่างลงตัว
ห้องพักและห้องสูทตกแต่งอย่างมีรสนิยมทั้งหมด 249 ห้อง, โอเรียนทาลา เวลเนส สปา ทรีตเมนต์, ลิ้มลองอาหารไทยต้นตำรับและอาหารนานาชาติรสเลิศที่ห้องอาหารภูเก็ตคาเฟ่บริเวณป่าตอง และเทคโนโลยีสำหรับการประชุมและสัมมนาอันทันสมัย ตัวโรงแรมมีความโดดเด่นและเหมาะสำหรับการพักผ่อนแบบส่วนตัว คู่รัก ครอบครัว และนักธุรกิจที่มาทำธุรกิจซึ่งต้องการมาพักผ่อนที่ภูเก็ต
ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง ให้การบริการระดับมืออาชีพและมีคุณภาพ คุณจะได้รับการบริการและการต้อนรับแบบไทย ๆ อันเป็นเอกลักษณ์ของทางโรงแรมเมื่อคุณมาพักที่นี่</p>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=600&height=400&imageid=23081&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Basic Deal with FREE Breakfast </b></h2>
                                    <p class="description" style="color:yellow;">You will get...</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>20% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits</li>
                                        <li>Deevana Welcome Drink</li>
                                        <li>Free Internet WIFI</li>
                                    </ul>
                                    <p class="description" style="color:green">FREE cancellation / Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&pid=MDc0MDAzNHwwNzQxNjMx">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=600&height=400&imageid=23082&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Basic Deal - Room Only </b></h2>
                                    <p class="description" style="color:yellow;">You will get...</p>
                                    <ul>
                                        <li>20% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits</li>
                                        <li>Deevana Welcome Drink</li>
                                        <li>Free Internet WIFI</li>
                                    </ul>
                                    <p class="description" style="color:green">FREE cancellation / Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&pid=MDc0MDAzNXwwNzQxNjMy">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=24&imageid=23194&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Early Bird Offer with Free Breakfast</b></h2>
                                    <p class="description" style="color:yellow;">Book 15 Days in advance, You will get 35% discount</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>20% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits.</li>
                                        <li>Deevana Welcome Drink</li>
                                        <li>Free Internet WIFI</li>
                                    </ul>
                                    <p class="description" style="color:green">FREE cancellation / Amendable booking</p>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&checkin=2022-02-25&checkout=2022-02-27&numofadult=2&numofchild=0&numofroom=1&pid=MDc0MTgwNXwwNzQxODA2">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=450&height=300&imageid=22605&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Buy 1 Get 1 Night FREE Stay with Breakfast</b></h2>
                                    <p class="description" style="color:yellow;">Book period : 1 - 31<sup>th</sup> October 2021<br>Stay period : 3<sup>rd</sup> Jan 2022 - 31<sup>st</sup> Oct 2022</p>
                                    <ul>
                                        <li>1 Free night, with free breakfast (Cumulative)</li>
                                        <li>Daily breakfast</li>
                                        <li>Free Internet WIFI</li>
                                        <li>FREE late check out until 15.00 hrs</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol and special menu</li>
                                        
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&checkin=2022-01-03&checkout=2022-01-05&numofadult=2&numofchild=0&numofroom=1&pid=MDc0MDkxNnwwNzQwOTE3">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=275&group=13&width=450&height=300&imageid=15039&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>STAY 3 PAY 2( Free Transfer-in + Thai massage + Late check-out)</b></h2>
                                    <p class="description" style="color:yellow;">Book period : Now - 31<sup>th</sup> August 2021<br>Stay period : 1<sup>st</sup> Dec 2021 - 31<sup>st</sup> Oct 2022</p>
                                    <ul>
                                        <li>3 nights in Deluxe Room</li>
                                        <li>Daily breakfast</li>
                                        <li>FREE One way Transfer-in from Phuket internation airport to Hotel</li>
                                        <li>FREE 60 Minutes Thai Massage for couple at Orientala Spa per stay</li>
                                        <li>FREE late check out until 15.00 hrs</li>
                                        <li>20% discount on laundry service</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol and special menu</li>
                                        
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&checkin=2021-12-01&checkout=2021-12-04&numofadult=2&numofchild=0&numofroom=1&pid=MDg4MDM5">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompackage?propertyid=275&group=13&width=450&height=300&imageid=14584&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>Holiday Escape</b></h2>
                                    <p class="description" style="color:yellow;">Book period : Now - 30<sup>th</sup> April 2021<br>Stay period : 1<sup>st</sup> Dec 2021 - 31<sup>st</sup> Oct 2022</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>Round Trip Transfer from Phuket internation airport to Hotel</li>
                                        <li>60 Minutes Thai Massage for couple at Orientala Spa per stay</li>
                                        <li>2 Times Thai or International dinner set at Hotel Restaurant per couple</li>
                                        <li>One day trip Phuket City Tour</li>
                                        <li>Complimentary a set of Afternoon tea per couple (2 coffee or tea and 2 pieces of cake per stay)</li>
                                        <li>Complimentary a set of drink per couple (2 local beer and 2 soft drink per stay)</li>
                                        <li>Complimentary Cash Coupon THB 300 to spend at hotel bar & restaurant</li>
                                        <li>20% discount on laundry service</li>
                                        <li>15% discount on Food and Beverage at hotel restaurant except alcohol and special menu</li>
                                        <li>Free late check out until 15.00 hrs</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/propertyibe2/rates?propertyId=275&onlineId=4&checkin=2021-10-01&checkout=2021-10-08&numofadult=2&numofchild=0&numofroom=1&pid=MDg3NzcyfDA4Nzc3M3wwODc3NzQ%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=450&height=300&imageid=21839&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>STAY IN STYLE - GET FREE DINNER</b></h2>
                                    <p class="description" style="color:yellow;">Book period : Now - 30<sup>th</sup> April 2021<br>Stay period : 1<sup>st</sup> Jan 2021 - 31<sup>st</sup> Oct 2022</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>FREE One Time Set Dinner for 2 Person at PHUKET CAFE Restuarant</li>
                                        <li>FREE Breakfast for 2 people</li>
                                        <li>FREE Late Check-out 15:00 hrs.</li>
                                        <li>FREE 10% discount on Food and Beverage at hotel restaurant except alcohol / cannot be combined with other hotel promotion or benefits.</li>
                                        <li>FREE WIFI</li>
                                        <li>Book now Pay later</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDc0MDI3OQ%3D%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=450&height=300&imageid=21666&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>HOTEL DEAL - Breakfast Included</b></h2>
                                    <p class="description" style="color:yellow;">Stay from 01 Oct 2021 - 31 Dec 2022</p>
                                    <ul>
                                        <li>Included Continental Breakfast</li>
                                        <li>FREE WiFi Internet</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDc0MDAzNA%3D%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=275&group=23&width=450&height=300&imageid=21667&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>HOTEL DEAL - Room Only</b></h2>
                                    <p class="description" style="color:yellow;">Stay from 01 Oct 2021 - 31 Dec 2022</p>
                                    <ul>
                                        <li>FREE WiFi Internet</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=275&onlineId=4&pid=MDc0MDAzNQ%3D%3D">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#7b9028;">The only all-inclusive</span> TO INCLUDE IT ALL</h1>

                <div id="activities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-orientala_wellness_spa.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">โอเรียลทาล่า สปา</h2>
                            <p class="description">เพื่อช่วยฟื้นฟูความสมดุลระหว่างร่างกายและจิตใจ โอเรียนทาล่า สปาที่โรงแรมดีวาน่า พลาซ่า ภูเก็ต ป่าตอง </p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-kid_club.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ห้องเด็กเล่น</h2>
                            <p class="description">โลกของความสนุกและน่าตื่นเต้นกำลังรอให้บริการสำหรับแขกตัวน้อย ๆ ของเราที่สโมสรเด็ก ซึ่งเด็ก ๆ สามารถปลดปล่อยความคิดสร้างสรรค์และจิตนาการให้โลดแล่นอย่างไม่มีขีดจำกัด</p>
                            <p><a class="button" href="facilities.php#kids_club">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-restaurant.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ร้านอาหาร &amp; บาร์</h2>
                            <p class="description">การสัมผัสประสบการณ์ในการรับประทานอาหารที่ดีวาน่า พลาซ่า ภูเก็ต เป็นสิ่งที่คุณไม่ควรพลาด ตั้งแต่ร้านอาหารภูเก็ตคาเฟ่ ซันบาร์ รูฟท็อปเทอเรซ และเดอะเลานจ์ </p>
                            <p><a class="button" href="facilities.php#restaurant">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title deco-underline"><span class="deco-map" style="display: inline-block;"><span style="color:#7b9028;">สถานที่ท่องเที่ยว</span>ภูเก็ต </span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-patong_beach.jpg" /></div>
                            <h2 class="title">หาดป่าตอง</h2>
                            <a class="more" href="attraction.php#patong_beach">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" /></div>
                            <h2 class="title">แหลมพรหมเทพ</h2>
                            <a class="more" href="attraction.php#phromthep_cape">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-kata_and_karon_beaches.jpg" /></div>
                            <h2 class="title">หากกะตะและหาดกะรน</h2>
                            <a class="more" href="attraction.php#kata_and_karon_beaches">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" /></div>
                            <h2 class="title">วัดพระใหญ่</h2>
                            <a class="more" href="attraction.php#big_buddha">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanaplazaphuket.com/images/awards/TCEB.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/pha-awards.jpg" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tica-awards.jpg" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/pta-awards.png" alt="" width="128" height="128"></li>
                </ul>
                <ul class="list-awards">
                    <li><a class="image-popup-awards" href="http://www.deevanaplazaphuket.com/images/awards/GMP.jpg"><img src="http://www.deevanahotels.com/images/awards/nfi-awards.png" alt="" width="128" height="128"></a></li>
                    <li><a href="https://www.tripadvisor.com/Hotel_Review-g297930-d754312-Reviews-Deevana_Plaza_Phuket_Patong-Patong_Kathu_Phuket.html" target="_blank"><img src="http://www.deevanaplazaphuket.com/images/awards/tripadvisor-deevana.png" alt="" width="128" height="128"></a></li>
                    <li><a href="https://tourismawards.tourismthailand.org/ann_de?id=2" target="_blank"><img src="http://www.deevanahotels.com/images/awards/thailand-tourism-award.png" alt="" width="128" height="128"></a></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/SHA+DPP.png" alt="" width="200" height="auto"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/safe-travel.png" alt="" width="128" height="128"></li>
                </ul>           
            </div>
        </section>
    </section>
</main>

<?php if (!isset($_SESSION['visited'])) : ?>

<?php endif; ?>
<?php $_SESSION['visited'] = "true"; ?>

<style>
    #offers {
        background-color : #1a355e;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 40px 25px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    #intro {
        min-height: 500px;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
	#activities_slider .owl-nav {
		top: 31%;
    }
    #offers_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #offers_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #offers_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #offers_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #offers_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #222;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #offers_slider .caption .button:hover {
        opacity: 0.9;
    }
    #offers_slider .center .caption .button {
        background-color: #93b006;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #ab8205;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #63b4d8;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('.image-popup-awards').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }
        });

        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php
include 'include/popup-image.php';
include '_footer.php';
?>