<?php
$title = 'Gallery | Deevana Plaza Phuket | Official Hotel Group Website Thailand';
$desc = 'Gallery: Guarantee best direct hotel rate and best location on Patong Beach; 4 star hotel near Jungceylon and bangla street';
$keyw = 'gallery, deevana plaza phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$body_class = 'gallery';
$cur_page = 'gallery';

$lang_en = '/deevanaplazaphuket/gallery.php';
$lang_th = '/th/deevanaplazaphuket/gallery.php';
$lang_zh = '/zh/deevanaplazaphuket/gallery.php';

include_once( '_header.php' );
?>

<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section section-gallery">

                <div class="gal-types">
                    <span class="tab active" data-tab="#photo"><i class="icon fa fa-picture-o"></i>PHOTOS</span>
                    <span class="tab" data-tab="#video"><i class="icon fa fa-film"></i>VIDEO</span>
                </div>

                <div class="gal-conts">
                    <section id="photo" class="gal-cont">
                        <div class="cat-tabs">
                            <span class="tab active" data-cat="*">ทั้งหมด</span>
                            <span class="tab" data-cat=".rooms">ที่พัก</span>
                            <span class="tab" data-cat=".bar">บาร์</span>
                            <span class="tab" data-cat=".restaurant">ห้องอาหาร</span>
                            <span class="tab" data-cat=".spa"> โอเรียนทาลา เวลเนส สปา</span>
                            <span class="tab" data-cat=".facilities">สิ่งอำนวยความสะดวก</span>
                        </div>

                        <div class="content">
                            <div class="masonry-gal">
                            <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-01.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-01.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-02.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-02.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-04.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-04.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-05.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-05.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-07.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-07.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-08.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-08.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-09.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-09.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-10.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-10.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-11.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-11.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-13.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-13.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-14.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-14.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-15.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-15.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-16.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-16.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-17.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-17.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-18.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-18.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item bar"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-19.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-19.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-20.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-20.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-21.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-21.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-22.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-22.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-23.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-23.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-24.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-24.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-25.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-25.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-26.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-26.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-27.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-27.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-28.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-28.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-29.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-29.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-31.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-31.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-32.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-32.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-33.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-33.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-34.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-34.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-35.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-35.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-36.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-36.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-37.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-37.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-38.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-38.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-39.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-39.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-40.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-40.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-41.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-41.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-42.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-42.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-43.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-43.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-44.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-44.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-45.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-45.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-46.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-46.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-47.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-47.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-49.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-49.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-53.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-53.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-54.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-54.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-55.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-55.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-56.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-56.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-57.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-57.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-58.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-58.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-59.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-59.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-60.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-60.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-61.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-61.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-62.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-62.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-63.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-63.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-64.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-64.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant01.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant01.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant02.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant02.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant03.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant03.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant04.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant04.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant05.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant05.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant06.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant06.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant07.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant07.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant08.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant08.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant09.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant09.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant10.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant10.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant11.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant11.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant12.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant12.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant13.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant13.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant14.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant14.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant15.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant15.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant16.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant16.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant17.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant17.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant18.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant18.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant19.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant19.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant20.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant20.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant21.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant21.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/gal-restaurant22.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gal-restaurant22.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/Tart.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/Tart.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/Tiramisu.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/Tiramisu.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/Pie.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/Pie.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/Choux-Cream.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/Choux-Cream.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item restaurant"><a href="../../deevanaplazaphuket/uploads/2021/01/Stawberry-and-Blueberry-Cake.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/Stawberry-and-Blueberry-Cake.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-65.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-65.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-66.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-66.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-67.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-67.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-68.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-68.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-69.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-69.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-70.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-70.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-71.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-71.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-72.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-72.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item spa"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-73.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-73.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-75.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-75.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-76.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-76.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-77.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-77.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-78.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-78.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-79.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-79.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-80.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-80.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-81.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-81.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item facilities"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-82.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-82.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-84.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-84.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-85.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-85.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-86.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-86.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-87.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-87.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-88.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-88.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-89.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-89.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-90.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-90.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-91.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-91.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-92.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-92.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                                <div class="item rooms"><a href="../../deevanaplazaphuket/uploads/2021/01/gallery-93.jpg"><img src="../../deevanaplazaphuket/uploads/2021/01/thumb/gallery-93.jpg" alt="Deevana Plaza Phuket-Patong" loading="lazy"></a></div>
                            </div>
                        </div>
                    </section>

                    <section id="video" class="gal-cont">
                        <div class="content">
                            <div class="youtube-container">
                                <div class="youtube-responsive">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/X1upLUeibOo" frameborder="0" allowfullscreen></iframe><br>

                                </div>

                                <div class="youtube-responsive">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/IcRMDdgABNI" frameborder="0" allowfullscreen></iframe><br>

                                </div>

                                <div class="youtube-responsive">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/osblJhCzAD8" frameborder="0" allowfullscreen></iframe><br>

                                </div>

                            </div>
                        </div>
                    </section>

                </div>

            </section>
        </div>
    </section>

    <?php include_once('include/booking_bar.php') ?>
</main>

<!--Isotopr-->
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
    $(function() {
        var $gCont = $('.gal-conts');
        var $gType = $('.gal-types');
        var $ctabs = $('.cat-tabs');

        function masonryFX( cat = '*' ) {
            var $mgal = $('.masonry-gal');
            $mgal.isotope({
                itemSelector: '.item',
                filter: cat,
            }).imagesLoaded().progress( function() {
                $mgal.isotope('layout');
            });
        }

        function galleryFX( cat = '.item' ) {
            $gCont.find(cat).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [1, 2],
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('gallery-open');
                    },
                    close: function() {
                        $('html').removeClass('gallery-open');
                    },
                },
                removalDelay: 300,
                mainClass: 'mfp-fade',
            });
        }

        // Define active tab
        var active = $gType.find('.active').data('tab');
        $gCont.find( active ).show();

        $gType.on('click', '.tab', function(e) {
            e.preventDefault();
            var $this = $(this);
            var data = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $gCont.find(data).fadeIn(300).siblings().hide();

            masonryFX();
        });

        $ctabs.on('click', '.tab', function(e) {
            var $this = $(this);
            var cat = $this.data('cat');
            $this.addClass('active').siblings().removeClass('active');

            galleryFX(cat);
            masonryFX(cat);
        });

        galleryFX();
        masonryFX();
    });
</script>

<style>
    .site-main {
        background-image: url(images/gallery/bg-gallery.jpg);
        background-attachment: fixed;
        background-position: center;
        background-size: cover;
    }
    .gal-conts {
        width: 100%;
        clear: both;
        background-color: #fff;
        margin-bottom: 30px;
        border-top: 2px solid #1a355e;
        position: relative;
    }
    .gal-cont {
        display: none;
        position: relative;
    }
    .gal-cont .content {
        overflow: hidden;
        padding: 10px 10px 0;
    }
    .gal-types,
    .cat-tabs {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
    }
    .gal-types {
        float: right;
    }
    .gal-types .tab {
        background-color: #fff;
        color: #333;
        border-radius: 3px 3px 0 0;
        display: inline-block;
        text-align: center;
        padding-top: 10px;
        width: 70px;
        height: 50px;
    }
    .gal-types .tab.active {
        background-color: #1A355E;
        color: #fff;
    }
    .gal-types .tab .icon {
        display: block;
    }
    .cat-tabs {
        position: absolute;
        top: -30px;
        left: 0;
    }
    .cat-tabs .tab {
        background-color: transparent;
        color: #333;
        line-height: 28px;
        font-size: 16px;
        padding: 0 8px;
        display: inline-block;
        border-radius: 3px 3px 0 0;
    }
    .cat-tabs .tab.active {
        background-color: #1a355e;
        color: #fff;
    }
    .masonry-gal {
        margin-left: -5px;
        margin-right: -5px;
    }
    .masonry-gal .item {
        width: 33.33%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .masonry-gal .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    #video {
        padding-bottom: 10px;
    }
    .youtube-container {
        max-width: 800px;
        margin: 1em auto;
    }
    .youtube-responsive {
        position: relative;
        width: 100%;
        display: block;
        padding-bottom: 56.25%;
    }
    .youtube-responsive iframe {
        position: absolute;
        width: 100%;
        height: 100%;
    }
    html .site-header {
        transition-property: background-color, height, right;
        transition-duration: 300ms, 300ms, 0ms;
    }
    html.gallery-open .site-header {
        right: 17px;
    }
    @media (max-width: 640px) {
        .section-gallery {
            padding-top: 0;
        }
        .masonry-gal .item {
            width: 50%;
        }
        .gal-conts {
            margin-top: 42px;
        }
        .gal-types {
            float: none;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        .gal-types:after {
            content: '';
            display: block;
            clear: both;
        }
        .gal-types .tab {
            width: 50%;
            float: left;
            padding: 6px;
            height: auto;
            border-radius: 0;
        }
        .gal-types .tab .icon {
            display: inline-block;
            margin-right: 3px;
        }
        .cat-tabs {
            overflow-x: auto;
            display: flex;
            flex-flow: row nowrap;
            background-color: #fff;
            top: -34px;
            left: 0;
            right: 0;
            border-radius: 3px 3px 0 0;
        }
        .cat-tabs .tab {
            border-radius: 0;
            font-size: 12px;
            line-height: 32px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
