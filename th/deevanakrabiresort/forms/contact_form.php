<?php

$recaptcha = $_POST['g-recaptcha-response'];

if ( $recaptcha ) {
    // send the actual mail
    $eol = "\n";
    if (strtoupper(substr(PHP_OS,0,3)=='WIN')) 
      $eol="\r\n"; 
    elseif (strtoupper(substr(PHP_OS,0,3)=='MAC')) 
      $eol="\r"; 
    else 
      $eol="\n"; 

    //Collect parameter
    $strsendto = $_POST['sendto'];
    $strname = $_POST['name'];
    $stremail = $_POST['email'];
    $strcountry = $_POST['country'];
    $strmessage = $_POST['message'];

    $to = $strsendto;
    $subject = "Customer Contact Information";
    $header = "MIME-Version: 1.0" . $eol;
    $header .= "Content-type: text/html; charset=UTF-8".$eol;
    $header .= "From: ". $strname ."<". $stremail .">".$eol;
    $header .= "Reply-To: " . $stremail;
    $body = "<strong>Deevana Customer Service,</strong> <br/>This is an inquiry form from website<br/>".$eol.

    "<br/><b>Name: </b> ".$strname.$eol.
    "<br/><b>Email: </b> ".$stremail.$eol.
    "<br/><b>Country: </b> ".$strcountry.$eol.
    "<br/><b>Message from customer:</b><br/>".$eol."".$strmessage.$eol.$eol.$eol.$eol;

    $flgSend = mail($to, $subject, $body, $header);
    
    echo 'ok';
} else {
    echo 'not';
}