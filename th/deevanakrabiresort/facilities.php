<?php
$title      = 'Facilities | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Facilities: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'facilities, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'facilities';
$cur_page   = 'facilities';

$lang_en    = '/deevanakrabiresort/facilities.php';
$lang_th    = '/th/deevanakrabiresort/facilities.php';
$lang_zh    = '/zh/deevanakrabiresort/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/facilities-slide-02.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-03.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-04.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
            <div class="item"><img src="images/facilities/facilities-slide-05.jpg" alt="Deevana Krabi Resort, 4-star hotel beach" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <!-- <span data-tab="#swan_spa" class="tab active">Orientala Spa</span> -->
                    <!--span data-tab="#argus_fitness" class="tab">Argus Fitness</span-->
                    <!-- <span data-tab="#myna_kids_club" class="tab">Myna Kid's Club</span> -->
                    <span data-tab="#swimming_pool" class="tab active">Swimming Pool</span>
                    <span data-tab="#restaurant" class="tab">Restaurant &amp; Bar</span>
                    <span data-tab="#the_port_library" class="tab">The Port Library</span>
                    <!-- <span data-tab="#pigeon_library_and_internet_corner" class="tab">Pigeon Library &amp; Internet Corner</span> -->
                </div>
                
                <div class="tabs-content">
                    <!-- <article id="swan_spa" class="article" data-tab-name="Swan Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swan_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Elegant and luxurious, Orientala Spa invites guests to experience the sublime pleasures of traditional Thai massage, aromatherapy and herbal treatments. The perfect place to find respite, Orientala Spa features a private twin room with Jacuzzi and steam room, 2 private single rooms, and  special set up in open-air space for foot reflexology and authentic Thai massage.</p>
                                    <p>Orientala Spa offers an extensive menu of rejuvenating massage and body services, each created to aid relaxation and improve wellbeing. The spa’s skilled therapists have taken the best indigenous knowledge and combined it with leading contemporary practice to create a superior wellness experience. Guests can enjoy the exotic and relaxing qualities of Thai herbs oils and other superior wellness products used in individual treatments or special spa packages.</p>
                                    <p><span style="color: #516819;">Orientala Spa ensures a truly indulgent and memorable spa journey daily : from 9 am. – 9 pm.<br> Location: Lobby level of the Reception Building</span></p>
                                </div>
                            </div>
                        </div>
                    </article> -->
                    
                    <!--article id="argus_fitness" class="article" data-tab-name="Argus Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_argus_fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Argus Fitness</h1>
                                    <p>Guests wishing to keep in shape during their holiday at Deevana Plaza Krabi Aonang can use the well- equipped fitness center at  free of charge. Joggers can also enjoy an early morning run along the beautiful Noppharat Thara Beach.</p>
                                    <p><span style="color: #516819;">Argus Fitness Centre is open daily : from 7 am. – 9 pm.<br> Location : Ground floor of Building 1</span></p>
                                </div>
                            </div>
                        </div>
                    </article-->
                    
                    <!-- <article id="myna_kids_club" class="article" data-tab-name="Myna Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_myna_kid's_club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Myna Kid's Club</h1>
                                    <p>Guests wishing to keep in shape during their holiday at Deevana Plaza Krabi Aonang can use the well- equipped fitness center at  free of charge. Joggers can also enjoy an early morning run along the beautiful Noppharat Thara Beach.</p>
                                    <p><span style="color: #516819;">Argus Fitness Centre is open daily : from 7 am. – 9 pm.<br> Location : Ground floor of Building 1</p>
                                </div>
                            </div>
                        </div>
                    </article> -->
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swimming_pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ</h1>
                                    <p>“ปะการัง พูลบาร์ ” และ “โบ๊ท บาร์” เป็นสระว่ายน้ำขนาดใหญ่ ด้วยร่มเงาที่ให้ร่มกันแดดที่ล้อมรอบด้วยธรรมชาติ มีเก้าอี้อาบแดดสำหรับการพักผ่อนที่ริมสระน้ำ เป็นสถานที่บริการเครื่องดื่มและ อาหารว่าง สำหรับการเติมความสดชื่นและ ผ่อนคลายตลอดทั้งวัน </p>
                                    <p><span style="color: #516819;">สระว่ายน้ำเปิดให้บริการทุกวันเวลา : 7.00 - 19.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <!-- <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pakarang_bar.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ปะการัง พูลบาร์</h1>
                                    <p>เป็นสถานที่ยอดเยี่ยมสำหรับการพักผ่อนและ เพลิดเพลินกับเบียร์เย็นๆ น้ำอัดลม,ม็อกเทลและ ค็อกเทลริมสระน้ำหรือ ในสระว่ายน้ำรวมถึงอาหารและ เครื่องดื่มหลากหลายเมนูที่มีให้บริการทุกวัน</p>
                                    <p><span style="color: #516819;">เปิดให้บริการทุกวันเวลา : 10.00 - 19.00 น.</span></p>
                                </div>
                            </div> -->
                            <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_chaolay.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องอาหาร ชาวเล</h1>
                                    <p>ห้องอาหาร ชาวเล ตกแต่งในสไตล์ดั้งเดิมในคอนเซป "ความลับของชาวประมง" ที่นี่ท่านสามารถเริ่มต้นมื้ออาหารด้วยบุฟเฟ่ต์อาหารเช้าแสนอร่อยหลากหลายเมนู รวมถึงอาหารกลางวันและอาหารเย็นมีให้เลือกมากมายรวมทั้งอาหารไทย อาหารนานาชาติ ด้วยวัตถุดิบที่คัดสรรค์มาอย่างดีพร้อมบริการที่จะมอบความประทับใจทั้งอาหารไทยและอาหารนานาชาติ </p>
                                    <!-- <p><span style="color: #516819;">เปิดให้บริการทุกวันเวลา :  06.00 - 23.00 น.</span></p> -->
                                    <p><span style="color: #516819;">Temporary Closed</span></p>
                                </div>
                            </div>
                            <div class="row row-content-tab" style="padding: 10px 0;">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_boat_bar.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">โบ๊ท บาร์</h1>
                                    <p>“โบ๊ท บาร์” ตั้งอยู่ริมสระน้ำ ซึ่งเป็นสถานที่ออกแบบด้วยสไตล์โมเดิร์นผสมสัญลักษณ์รูปเรือประมง ให้บริการเครื่องดื่มเช่น กาแฟ, เบียร์, ค็อกเทล, ม็อกเทล, อาหารและขนมขบเคี้ยวของทานเล่น</p>
                                    <!-- <p><span style="color: #516819;">เปิดให้บริการทุกวันเวลา : 10.00 - 00.00 น.</span></p> -->
                                    <p><span style="color: #516819;">Temporary Closed</span></p>
                                </div>
                            </div>
                            
                        </div>
                    </article>
                    
                    <!-- <article id="pigeon_library_and_internet_corner" class="article" data-tab-name="Pigeon Library &amp; Internet Corner">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pigeon_library_and_internet_corner.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Pigeon Library &amp; Internet Corner</h1>
                                    <p>Guests can enjoy quiet moments in the resort’s well-stocked library. A wide range of books, local and international newspapers and magazines are available. DVDs can also be borrowed to watch in the comfort of guestrooms. Internet connection and computers equipment are also available on complimentary basis in the Pigeon Conner.</p>
                                </div>
                            </div>
                        </div>
                    </article> -->

                    <article id="the_port_library" class="article" data-tab-name="The Port Library">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_liabary.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">เดอะ พอร์ต ไลบรารี่</h1>
                                    <p>ผู้เข้าพักสามารถเพลิดเพลินไปกับช่วงเวลาที่เงียบสงบในห้องสมุดของรีสอร์ทที่เต็มไปด้วยหนังสือมากมายหลากหลายประเภท รวมถึงหนังสือพิมพ์และนิตยสารในประเทศ ท่านยังสามารถยืมแผ่นดีวีดีเพื่อรับชมภาพยนตร์ในห้องพักที่สะดวกสบาย รวมถึงมีบริการเชื่อมต่ออินเตอร์เน็ตและ อุปกรณ์คอมพิวเตอร์โดยไม่คิดค่าใช้จ่าย</p>
                                    <p><span style="color: #516819;">เปิดให้บริการทุกวันเวลา :  07.00 - 21.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>