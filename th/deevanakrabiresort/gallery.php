<?php
$title      = 'Gallery | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Gallery: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'gallery, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$body_class = 'gallery';
$cur_page   = 'gallery';

$lang_en    = '/deevanakrabiresort/gallery.php';
$lang_th    = '/th/deevanakrabiresort/gallery.php';
$lang_zh    = '/zh/deevanakrabiresort/gallery.php';

include_once( '_header.php' );
?>

<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section section-gallery">
                
                <div class="gal-types">
                    <span class="tab active" data-tab="#photo"><i class="icon fa fa-picture-o"></i>PHOTOS</span>
                   <span class="tab" data-tab="#video"><i class="icon fa fa-film"></i>VIDEO</span>
                </div>
                
                <div class="gal-conts">
                    <section id="photo" class="gal-cont">
                        <div class="content">
                            <div class="cat-tabs">
                                <span class="tab active" data-cat="*">ALL</span>
                                <!--<span class="tab" data-cat=".overview">OVERVIEW</span>
                                <span class="tab" data-cat=".accommodations">ACCOMMODATIONS</span>
                                <span class="tab" data-cat=".restaurant">RESTAURANT</span>
                                <span class="tab" data-cat=".spa">SPA</span>
                                <span class="tab" data-cat=".facilities">FACILITIES</span>-->
                            </div>
                            
                            <div class="masonry-gal">
                                <?php for($i = 01; $i <= 45; $i++) : ?>
                                <?php $index = ($i<10) ? '0'.$i : $i; ?>
                                <div class="item"><a href="images/gallery/set-04/1500/gal-<?php echo $index; ?>.jpg"><img src="images/gallery/set-04/600/gal-<?php echo $index; ?>.jpg" alt="Deevana Krabi Resort, 4-star hotel" /></a></div>
                                <?php endfor; ?>
                            </div>
                            
                        </div>
                    </section>
                    
                    <section id="video" class="gal-cont">
                        <div class="content">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="youtube-responsive">
                                        <div class="youtube-container">
                                            <iframe src="https://www.youtube.com/embed/b7EH6lyvh1o?ecver=2" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="youtube-responsive">
                                        <div class="youtube-container">
                                            <iframe src="https://www.youtube.com/embed/_Xxx5Ddl0lY" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<!--Isotopr-->
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
    $(function() {
        var $gCont = $('.gal-conts');
        var $gType = $('.gal-types');
        var $ctabs = $('.cat-tabs');
        
        function masonryFX( cat = '*' ) {
            var $mgal = $('.masonry-gal');
            $mgal.isotope({
                itemSelector: '.item',
                filter: cat,
            }).imagesLoaded().progress( function() {
                $mgal.isotope('layout');
            });
        }
        
        function galleryFX( cat = '.item' ) {
            $gCont.find(cat).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [1, 2],
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('gallery-open');
                    },
                    close: function() {
                        $('html').removeClass('gallery-open');
                    },
                },
                removalDelay: 300,
                mainClass: 'mfp-fade',
            });
        }
        
        // Define active tab
        var active = $gType.find('.active').data('tab');
        $gCont.find( active ).show();
        
        $gType.on('click', '.tab', function(e) {
            e.preventDefault();
			if( $(this).hasClass('active') ) return;
			
            var $this = $(this);
            var data = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $gCont.find(data).fadeIn(300).siblings().hide();
            
            masonryFX();
        });
        
        $ctabs.on('click', '.tab', function(e) {
            var $this = $(this);
            var cat = $this.data('cat');
            $this.addClass('active').siblings().removeClass('active');
            
            galleryFX(cat);
            masonryFX(cat);
        });
        
        galleryFX();
        masonryFX();
    });
</script>

<style>
    .site-main {
        background-image: url(images/gallery/bg-gallery.jpg);
        background-attachment: fixed;
        background-position: center;
        background-size: cover;
    }
    .gal-conts {
        width: 100%;
        clear: both;
        background-color: #fff;
        margin-bottom: 30px;
        border-top: 2px solid #1a355e;
        position: relative;
    }
    .gal-cont {
        display: none;
        position: relative;
    }
    .gal-cont .content {
        overflow: hidden;
        padding: 10px 10px 0;
    }
    .gal-types,
    .cat-tabs {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
    }
    .gal-types {
        float: right;
    }
    .gal-types .tab {
        background-color: #fff;
        color: #333;
        border-radius: 3px 3px 0 0;
        display: inline-block;
        text-align: center;
        padding-top: 10px;
        width: 70px;
        height: 50px;
    }
    .gal-types .tab.active {
        background-color: #1A355E;
        color: #fff;
    }
    .gal-types .tab .icon {
        display: block;
    }
    .cat-tabs {
        position: absolute;
        top: -30px;
        left: 0;
    }
    .cat-tabs .tab {
        background-color: transparent;
        color: #333;
        line-height: 28px;
        font-size: 16px;
        padding: 0 8px;
        display: inline-block;
        border-radius: 3px 3px 0 0;
    }
    .cat-tabs .tab.active {
        background-color: #1a355e;
        color: #fff;
    }
    .masonry-gal {
        margin-left: -5px;
        margin-right: -5px;
    }
    .masonry-gal .item {
        width: 33.33%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .masonry-gal .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    #video {
        padding-bottom: 10px;
    }
    
    html .site-header {
        transition-property: background-color, height, right;
        transition-duration: 300ms, 300ms, 0ms;
    }
    html.gallery-open .site-header {
        right: 17px;
    }
	.youtube-responsive {
		max-width: 800px;
		margin: 1em auto;
		height: auto;
	}
	.youtube-container {
		position: relative;
		width: 100%;
		padding-bottom: 56.25%;
	}
	.youtube-responsive iframe {
		position: absolute;
		width: 100%;
		height: 100%;
	}
    @media (max-width: 640px) {
        .section-gallery {
            padding-top: 0;
        }
        .masonry-gal .item {
            width: 50%;
        }
        .gal-conts {
            margin-top: 42px;
        }
        .gal-types {
            float: none;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        .gal-types:after {
            content: '';
            display: block;
            clear: both;
        }
        .gal-types .tab {
            width: 50%;
            float: left;
            padding: 6px;
            height: auto;
            border-radius: 0;
        }
        .gal-types .tab .icon {
            display: inline-block;
            margin-right: 3px;
        }
        .cat-tabs {
            overflow-x: auto;
            display: flex;
            flex-flow: row nowrap;
            background-color: #fff;
            top: -34px;
            left: 0;
            right: 0;
            border-radius: 3px 3px 0 0;
        }
        .cat-tabs .tab {
            border-radius: 0;
            font-size: 12px;
            line-height: 32px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>