<div class="booking-bar">
    <div class="inner">
        <div class="container">
            <form id="booking-form" class="form">
                <span class="field field-header">
                    <img class="block responsive" src="assets/elements/book_online_best_rate_guarantee.png" alt="Book Online Best rate guarantee" />
                </span>

                <span class="field field-checkin">
                    <input id="checkin" class="input-text date" placeholder="Check-in" type="text" readonly />
                </span>

                <span class="field field-checkout">
                    <input id="checkout" class="input-text date" placeholder="Check-out" type="text" readonly />
                </span>

                <span class="field field-adults">
                    <label class="label">ผู้ใหญ่</label>
                    <select id="adults" class="input-select">
                        <option value="1">1 ผู้ใหญ่</option>
                        <option value="2" selected>2 ผู้ใหญ่</option>
                        <option value="3">3 ผู้ใหญ่</option>
                        <option value="4">4 ผู้ใหญ่</option>
                        <option value="5">5 ผู้ใหญ่</option>
                        <option value="6">6 ผู้ใหญ่</option>
                        <option value="7">7 ผู้ใหญ่</option>
                        <option value="8">8 ผู้ใหญ่</option>
                        <option value="9">9 ผู้ใหญ่</option>
                        <option value="10">10 ผู้ใหญ่</option>
                        <option value="11">11 ผู้ใหญ่</option>
                        <option value="12">12 ผู้ใหญ่</option>
                    </select>
                </span>

                <input id="children" type="hidden" name="numofchild" value="0">

                <span class="field field-rooms">
                    <label class="label">ห้อง</label>
                    <select id="rooms" class="input-select">
                        <option value="1" selected>1 ห้อง</option>
                        <option value="2">2 ห้อง</option>
                        <option value="3">3 ห้อง</option>
                        <option value="4">4 ห้อง</option>
                        <option value="5">5 ห้อง</option>
                        <option value="6">6 ห้อง</option>
                        <option value="7">7 ห้อง</option>
                        <option value="8">8 ห้อง</option>
                        <option value="9">9 ห้อง</option>
                    </select>
                </span>

                <span class="field field-code">
                    <input id="accesscode" class="input-text" placeholder="Promo code" type="text" />
                </span>

                <span class="field field-submit">
                    <button id="submit" class="button" type="submit">จองพัก</button>
                </span>
            </form>
        </div>
    </div>
</div>