<ul class="menu list-menu">
    <li class="<?php echo get_current_class('home'); ?>"><a href="index.php">หน้าหลัก</a></li>
    <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">ห้องพัก</a>
        <ul class="sub-menu">
            <li class="<?php echo get_current_class('deluxe-room'); ?>"><a href="room-deluxe-room.php">ห้องสแตนดาร์ด</a></li>
            <li class="<?php echo get_current_class('grand-deluxe'); ?>"><a href="room-grand-deluxe.php">ห้องแกรนด์ดีลักซ์</a></li>
            <li class="<?php echo get_current_class('duplex'); ?>"><a href="room-duplex.php">ห้องดูเพล็กซ์</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">สิ่งอำนวยความสะดวก</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">สถานที่ท่องเที่ยว</a></li>
    <li class="<?php echo get_current_class('promotion'); ?>"><a href="<?php ibe_url('386', 'en'); ?>">โปรโมชั่น</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">รูปภาพ</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">ติดต่อเรา</a></li>
</ul>
