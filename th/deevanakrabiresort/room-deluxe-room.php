<?php
$title      = 'Standard Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Standard Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'Standard room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-deluxe-room';
$cur_page   = 'deluxe-room';
$par_page   = 'rooms';

$lang_en    = '/deevanakrabiresort/room-deluxe-room.php';
$lang_th    = '/th/deevanakrabiresort/room-deluxe-room.php';
$lang_zh    = '/zh/deevanakrabiresort/room-deluxe-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <!-- <img src="images/accommodations/deluxe-room/1500/deluxe-room-01.jpg" alt="Deluxe Room" width="1500" height="1013" />
                    <img src="images/accommodations/deluxe-room/1500/deluxe-room-06.jpg" alt="Deluxe Room" width="1500" height="1045" />
                    <img src="images/accommodations/deluxe-room/1500/deluxe-room-03.jpg" alt="Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/deluxe-room/1500/deluxe-room-04.jpg" alt="Deluxe Room" width="1500" height="937" /> -->
                    <img src="images/accommodations/standard-room/1500/Standard-Room-1.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-2.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-3.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-4.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-5.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-6.jpg" alt="Standard Room" width="1500" height="1045" />
                    <img src="images/accommodations/standard-room/1500/Standard-Room-7.jpg" alt="Standard Room" width="1500" height="1045" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องสแตนดาร์ด<span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
					<li class="current"><img src="images/accommodations/standard-room/600/Standard-Room-1.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-2.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-3.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-4.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-5.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-6.jpg" alt="Standard Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/standard-room/600/Standard-Room-7.jpg" alt="Standard Room" width="600" height="400" /></li>
					<!-- <li><img src="images/accommodations/deluxe-room/600/deluxe-room-06.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-03.jpg" alt="Deluxe Room" width="600" height="400" /></li>
					<li><img src="images/accommodations/deluxe-room/600/deluxe-room-04.jpg" alt="Deluxe Room" width="600" height="400" /></li> -->
			
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องสแตนดาร์ด</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe-room/600/deluxe-room-01.jpg" alt="Deluxe Room" width="600" height="400" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ผ่อนคลายอย่างง่ายๆในห้องพักชั้นบนซึ่งออกแบบให้ไม่ซ้ำกันและการตกแต่งที่ทำให้คุณรู้สึกอบอุ่น ห้องพักมีพื้นที่ 26 ตารางเมตร พร้อมด้วยระเบียงสำหรับการพักผ่อนอย่างสบายๆ มีทั้งเตียงขนาดคิงไซส์หรือเตียงคู่ให้คุณได้เลือก พร้อมด้วยเก้าอี้นั่งเล่นแบบไทย แยกส่วนบริเวณอาบน้ำฝักบัวจากห้องน้ำและอ่างล้างหน้า พร้อมกับอุปกรณ์อาบน้ำที่เป็นมิตรกับธรรมชาติ</p>
                            <p>จำนวนห้องพักทั้งหมด : 27 ห้อง</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>ทีวีดิจิตอลขนาด 40 นิ้ว</li>
                                        <li>ฟรี Wi-Fi</li>
                                        <li>อุปกรณ์สำหรับชงชาและกาแฟ</li>
                                        <li>กล่องนิรภัย</li>
                                        <li>มินิบาร์</li>
                                        <li>เครื่องเป่าผม</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>ทีวีดิจิตอลขนาด 40 นิ้ว</li>
											<li>ฟรีอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
											<li>อุปกรณ์สำหรับชงชาและกาแฟ</li>
											<li>กล่องนิรภัย</li>
											<li>มินิบาร์</li>
											<li>เครื่องเป่าผม</li>
											<li>น้ำร้อนและเย็น</li>
											<li>ร่ม</li>
											<li>ถุงชายหาด</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนข้อมูล</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>