<?php
$title      = 'Grand Deluxe Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Grand Deluxe Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'Grand Deluxe Room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-grand-deluxe';
$cur_page   = 'grand-deluxe';
$par_page   = 'rooms';

$lang_en    = '/deevanakrabiresort/room-duplex.php';
$lang_th    = '/th/deevanakrabiresort/room-duplex.php';
$lang_zh    = '/zh/deevanakrabiresort/room-duplex.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="1500" height="986" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="1500" height="925" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="1500" height="954" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">Grand Deluxe Room <span>King size or Twin bed</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">Grand Deluxe Room</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>Surround yourself with nature and tangibility of local flavor on handmade items for decoration. 30 Sqm with a large terrace containing a huge circle bathtub and relaxing chair on the ground floor for your convenience. King size bed or twin bed is available for your choice. Bathroom with the rain shower stand also available for hampered. Qualities of nature friendly amenities are provided.</p>
                            <p>Rooms available: 34</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">Book This Room Category</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>LCD 40” TV</li>
                                        <li>Free WIFI</li>
                                        <li>Tea &amp; Coffee making facility</li>
                                        <li>Safety Box</li>
                                        <li>Mini Bar</li>
                                        <li>Hair dryer</li>
                                        <li class="more clickable">
                                        	<a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>LCD 40” TV</li>
											<li>Free WIFI</li>
											<li>Tea &amp; Coffee making facility</li>
											<li>Safety Box</li>
											<li>Mini Bar</li>
											<li>Hair dryer</li>
											<li>Hot &amp; Cold water</li>
											<li>Bathrobes</li>
											<li>Slippers</li>
											<li>Umbrella</li>
											<li>Beach Bag</li>
											<li>Iron &amp; Iron board</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; Hide content</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>