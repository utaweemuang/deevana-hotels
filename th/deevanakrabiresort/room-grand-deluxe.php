<?php
$title      = 'Grand Deluxe Room | Deevana Krabi Resort | Official Hotel Group Website Thailand';
$desc       = 'Grand Deluxe Room: Enjoy best hotel rate of brand new 4 star beach resort; Deevana Krabi Resort is located on Aonang near Noppharat Thara Beach.';
$keyw       = 'Grand Deluxe Room, deevana krabi resort, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-grand-deluxe';
$cur_page   = 'grand-deluxe';
$par_page   = 'rooms';

$lang_en    = '/deevanakrabiresort/room-grand-deluxe.php';
$lang_th    = '/th/deevanakrabiresort/room-grand-deluxe.php';
$lang_zh    = '/zh/deevanakrabiresort/room-grand-deluxe.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="1500" height="986" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="1500" height="1001" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="1500" height="925" />
                    <img src="images/accommodations/grand-deluxe-room/1500/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="1500" height="954" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องแกรนด์ดีลักซ์ <span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-02.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-03.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-04.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-05.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-06.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                    <li><img src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-07.jpg" alt="Grand Deluxe Room" width="600" height="400" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องแกรนด์ดีลักซ์</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/grand-deluxe-room/600/grand-deluxe-room-01.jpg" alt="Grand Deluxe Room" width="600" height="400" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ห้องพักของเราจะทำให้คุณรู้สึกเหมือนถูกล้อมรอบด้วยธรรมชาติและยังสามารถสัมผัสความเป็นท้องถิ่นบนสิ่งของซึ่งทำด้วยมือที่เรานำมาใช้ตกแต่งห้องพัก ห้องพักมีขนาด 30 ตารางเมตร พร้อมด้วยระเบียงขนาดใหญ่ที่มีอ่างอาบน้ำรูปวงกลมขนาดใหญ่ และเก้าอี้สำหรับพักผ่อนบนพื้นเพื่อความสะดวกสบายของผู้เข้าพัก มีให้เลือกทั้งเตียงแบบคิงไซส์หรือเตียงคู่ ห้องน้ำยังมีฝักบัวแบบสายฝนอีกด้วย มีสิ่งอำนวยความสะดวกที่เป็นมิตรกับธรรมชาติคุณภาพดีให้บริการ</p>
                            <p>จำนวนห้องพักทั้งหมด : 34 ห้อง</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>ทีวีแอลซีดีขนาด 40 นิ้ว</li>
                                        <li>ฟรีอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                        <li>อุปกรณ์สำหรับชงชาและกาแฟ</li>
                                        <li>กล่องนิรภัย</li>
                                        <li>มินิบาร์</li>
                                        <li>เครื่องเป่าผม</li>
                                        <li class="more clickable">
                                        	<a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>ทีวีแอลซีดีขนาด 40 นิ้ว</li>
											<li>ฟรีอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                            <li>อุปกรณ์สำหรับชงชาและกาแฟ</li>
                                            <li>กล่องนิรภัย</li>
                                            <li>มินิบาร์</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>น้ำร้อนและเย็น</li>
                                            <li>เสื้อคลุมอาบน้ำ</li>
                                            <li>ร้องเท้าแตะ</li>
                                            <li>ร่ม</li>
                                            <li>ถุงชายหาด</li>
											<li>เตารีดและกระดานรองรีด</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนข้อมูล</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>