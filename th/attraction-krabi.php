<?php
$title = 'Krabi Attractions | Deevana Hotels & Resorts';
$desc = 'Krabi is the most relaxing part to be in all of Thailand, it is a province that has the most stunning scenery imaginable, beautiful white beaches that stretch on for miles, a jungle and over 200 islands just of the coast.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'attraction krabi';
$cur_page = 'attraction-krabi';

$lang_en = '/attraction-krabi.php';
$lang_th = '/th/attraction-krabi.php';
$lang_zh = '/zh/attraction-krabi.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/attraction/krabi/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        <div class="container">
		
			<h1 class="section-title underline">สถานที่ท่องเที่ยว กระบี่</h1>
            
            <div class="row row-content">
                <div class="col-w3 col-navigation">
                    <div class="post-tabs tabs-group">
                        <ul>
							<li class="tab active" data-tab="#emerald_pool">สระมรกต</li>
							<!-- <li class="tab" data-tab="#elephant_ride">นั่งช้าง</li> -->
							<li class="tab" data-tab="#phi_phi_island">เกาะพีพี</li>
							<li class="tab" data-tab="#thalay_wak">ทะเลแหวก</li>
                            <li class="tab" data-tab="#nong_thale_canel">คลองหนองทะเล</li>
							<li class="tab" data-tab="#koh_kai">เกาะไก่</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="emerald_pool" data-tab-name="Emerald Pool">
                        <img class="thumbnail force block" src="images/attraction/krabi/emerald_pool.png" />
                        <h1>สระมรกต</h1>
                        <p>สระมรกตคือสระน้ำที่เกิดจากธรรมชาติที่สวยงามอย่างแท้จริง เป็นสระน้ำสวยใสส่องประกายซึ่งซุกซ่อนอยู่กลางใจป่า สระมรกตตั้งอยู่ในเขตรักษาพันธุ์สัตว์ป่าเขาประ-บางคราม ซึ่งคนท้องถิ่นจะเรียกว่าป่าเขานอจู้จี้ ในเขตบ้านบางเตียว เดินทางจากที่ว่าการอำเภอคลองท่อมไปตามทางหลวงหมายเลขที่ 4038 ไปอีกประมาณ 18 กิโลเมตร</p>
                        <p>เมื่อผู้มาเยือนมาถึง ก็มักจะอดใจไม่ได้ที่กระโดดลงไปในสระน้ำใสสีเขียวมรกตและสนุกสนานกับการว่ายน้ำให้สดชื่น</p>
                        <p>สระมรกตมีความลึกประมาณ 1-2 เมตร และมีขนาดเส้นผ่านศูนย์กลางประมาณ 20-25 เมตร สีของน้ำในสระจะเปลี่ยนเป็นสีเขียวอ่อนหรือสีเขียวมรกตนั้นขึ้นอยู่กับการสะท้อนของแสงแดดในวันนั้นๆ</p>
                        <p>น้ำในสระมรกตนั้นมาจากบ่อน้ำซึ่งรู้จักกันดีในชื่อ "บ่อน้ำผุด" ซึ่งตั้งอยู่ห่างจากเนินเขาไปประมาณ 600 เมตร น้ำด่างจากกำมะถันที่ผุดขึ้นมาจากบ่อน้ำผุดนี้ไหลผ่านรอยแยกของชั้นหินไปยังสระมรกต น้ำที่ใสอยู่เสมอนั้นเพราะในน้ำมีแคลเซี่ยมคาร์บอเนตผสมอยู่มาก ทำให้สิ่งที่ปะปนอยู่ในน้ำตกตะกอนได้เร็วขึ้น และทำให้ตะไคร่น้ำไม่สามารถที่จะเจริญเติบโตในบ่อน้ำแห่งนี้ได้</p>
                        <p>กระท่อมสไตล์บาหลีได้ถูกสร้างขึ้นเมื่อปีที่ผ่านมาและถูกสงวนไว้ให้สำหรับพระบรมวงศานุวงศ์เท่านั้น</p>
                    </article>
                    
                    <!-- <article class="article" id="elephant_ride" data-tab-name="Elephant Ride">
                        <img class="thumbnail force block" src="images/attraction/krabi/elephant_ride.png" />
                        <h1>นั่งช้าง</h1>
                        <p>ช้างคือสัญลักษณ์ของชาติและถือว่าเป็นของขลังอย่างหนึ่งสำหรับคนไทย ด้วยผิวสีเทาที่ยับย่น และลำตัวที่ไหวเอนไปมา ช้างจึงเป็นการผสมผสานที่งดงามของกำลังที่ดุร้าย ความอ่อนโยนและความกระฉับกระเฉง ซึ่งทำให้เรารู้สึกทั้งเคารพและชื่นชอบไปพร้อมกัน</p>
                        <p>เนื่องจากช้างได้หายไปจากป่าในประเทศไทยอย่างรวดเร็ว ดังนั้นสถานที่ที่ดีที่สุดที่จะได้พบเห็นสิ่งมีชีวิตที่น่างดงามนี้คือบริเวณค่ายสำหรับเดินป่า ซึ่งมีเพียงไม่กี่แห่งในจังหวัดกระบี่ โดยสถานประกอบการดังกล่าวจะต้องเป็นไปตามหลักเกณฑ์ที่เข้มงวดที่กำหนดโดยกรมปศุสัตว์แห่งชาติเกี่ยวกับการให้อาหาร, น้ำ, และที่อยู่อาศัยสำหรับช้างที่เหมาะสม และเช่นเดียวกับการให้การดูแลสุขภาพที่เหมาะสม</p>
                        <p>โดยการเดินทางอย่างช้าๆบนหลังช้างนี้จะทำให้คุณได้สัมผัสกับสภาพแวดล้อมของสัตว์ป่าทางธรรมชาติโดยตรง การให้นักท่องเที่ยวขี่ช้างเพื่อความสนุกสนานอาจจะหมายถึงการทำให้ช้างได้ถูกอนุรักษ์ไว้ ในระหว่างที่การอยู่อย่างอิสระก็อาจจะเป็นไปได้ แต่เนื่องจากไม่มีสถานที่ที่เพียงพอที่จะปล่อยช้างเหล่านี้สู่ป่า และต้นทุนสำหรับการเลี้ยงดูที่ค่อนข้างสูง (โดยช้างจะบริโภคอาหารราว 200-300 กิโลกรัมต่อวัน) ทางเลือกเดียวสำหรับยักษ์ใหญ่ใจดีนี้คือการร้องขออาหาร เข้าร่วมกับการลักลอบตัดไม้ หรืออยู่เฉยๆในสวนสัตว์</p>
                        <p>ทัวร์เดินป่า - โดยปกติจะมีการนั่งช้างประมาณหนึ่งชั่วโมง, รวมไปถึงกิจกรรมเที่ยวชมสถานที่ต่างๆ สามารถจองได้ด้านล่างนี้ หรือจองผ่านตัวแทนท้องถิ่น คุณยังสามารถไปเยี่ยมชมค่ายเหล่านี้ด้วยตัวเองเพื่อเดินป่า หรือเพียงแค่คอยดูและถ่ายรูปช้าง โดยไม่จำเป็นต้องขี่ช้าง</p>
                    </article> -->
                    
                    <article class="article" id="phi_phi_island" data-tab-name="Phi Phi Island">
                        <img class="thumbnail force block" src="images/attraction/krabi/phi_phi_island.png" />
                        <h1>เกาะพีพี</h1>
                        <p>เกาะพีพีเป็นดาวเด่นของประเทศไทย เกาะแห่งนี้เคยถูกใช้เป็นสถานที่ถ่ายทำภาพยนตร์ และมักถูกพูดถึงโดยนักท่องเที่ยวทั่วประเทศไทย สำหรับนักท่องเที่ยวบางกลุ่ม เหตุผลที่พวกเขาจำเป็นต้องบินมายังภูเก็ตก็เพียงเพื่อผ่านไปสู่เกาะแห่งนี้ ถึงแม้จะมีการโฆษณาเกินจริงบ้าง แต่มันก็ไม่น่าผิดหวังเลย เกาะพีพีมีความงดงามและน่าดึงดูดใจ คุณสามารถเดินทางมายังเกาะแห่งนี้โดยทางเรือ ตัวเกาะมองดูเหมือนว่ามันลอยขึ้นมาจากทะเล มีลักษณะคล้ายป้อมปราการ มีหอหน้าผาสูงชันอยู่เหนือศรีษะ ซึ่งมีเส้นทางไปสู่ป่าบริเวณหน้าชายหาด เกาะแห่งนี้อาจทำให้คุณรู้สึกเหมือนรักแรกพบเลยทีเดียว</p>
                        <p>อีกเหตุผลที่ทำให้เราหลงรักเกาะแห่งนี้ นั่นคือเกาะแห่งนี้เป็นหนึ่งในไม่กี่ที่ในโลกที่เราจะสามารถเอนตัวลงพักผ่อนได้อย่างสบายใจ เกาะพีพีมีสองส่วนด้วยกัน ส่วนแรกของเกาะนั้นไม่มีคนอาศัยอยู่ (พีพีเล) และอีกส่วนของเกาะนั้นไม่มีถนน (พีพีดอน) จีงไม่มีตาราง ไม่มีความเร่งรีบ จึงไม่มีเหตุผลใดที่ต้องเร่งรีบ</p>
                    </article>

                    <article class="article" id="thalay_wak" data-tab-name="Thalay Wak">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/talay-wak.png" />
                        <h1>ทะเลแหวก</h1>
                        <p>ทะเลแหวกเป็นสถานที่ท่องเที่ยวทางธรรมชาติที่โด่งดังของ จ.กระบี่ ที่ถูกขนานนามให้เป็น Unseen Thailand อันเนื่องมาจากความมหัศจรรย์ของธรรมชาติ ยามระดับน้ำลด เผยให้เห็นส่วนของสันทรายเชื่อมต่อกันถึง ๓ เกาะ ได้แก่ เกาะไก่ เกาะทับ เกาะหม้อ</p>
                    </article>

                    <article class="article" id="nong_thale_canel" data-tab-name="Nong Thale Canal">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/Klongnongtale.png" />
                        <h1>คลองหนองทะเล</h1>
                        <p>คลองหนองทะเล ตั้งอยู่ที่เขตรอยต่อระหว่าง ม.1 กับ ม.4 ตำบลหนองทะเล อำเภอเมือง จังหวัดกระบี่ ในอดีตคลองแห่งนี้เป็นคลองน้ำจืดสายเล็กๆ ที่มีลักษณะคล้ายๆ กับป่าพรุ เต็มไปด้วยเหล่าต้นไม้น้อยใหญ่ ต่อมาเมื่อราว 10 ปีก่อน ได้มีก่อสร้างฝายน้ำล้นที่บริเวณกลางลำคลอง เพื่อชะลอน้ำไว้ใช้ในช่วงหน้าแล้ง และนำน้ำมาผลิตน้ำประปาแก้ปัญหาความเดือดร้อนของประชาชน ส่งผลให้ด้านบนของฝาย จากลำคลองเล็กๆ และป่าพรุ ก็กลายเป็นแอ่งน้ำขนาดใหญ่มีเนื้อที่ประมาณ 100 ไร่ ความลึกประมาณ 5 เมตร กว้างประมาณ 200 เมตร จนชาาวบ้านพูดติดปากกันว่า “คลองหนองทะเล”</p>
                        <p>ปัจจุบัน “คลองหนองทะเล” หรือ “คลองหรูด” ได้กลายเป็นแหล่งท่องเที่ยวเชิงนิเวศแห่งใหม่ของจ.กระบี่ โดยองค์การบริหารส่วนตำบลหนองทะเล ร่วมกับกำนัน ผู้ใหญ่บ้าน และชาวบ้านช่วยกันพัฒนาตกแต่งเส้นทางเพื่อรองรับนักท่องเที่ยวที่ชื่นชอบการเที่ยวเชิงนิเวศ โดยการพายเรือคายัค (เรือแคนู)</p>
                        <p>ขึ้นไปชมต้นน้ำของคลองหนองทะเล ระหว่างเส้นทาง ได้เที่ยวชมโขดหินน้อยใหญ่ คล้ายกับภูเขาเล็กๆ โผล่พ้นผิวน้ำสลับเรียงราย กับตอไม้</p>
                    </article>

                    <article class="article" id="koh_kai" data-tab-name="Koh Kai">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/koh-kai.png" />
                        <h1>เกาะไก่</h1>
                        <p>เกาะไก่ อยู่ในทะเลกระบี่หน้าอ่าวนาง ห่างจากฝั่งประมาณ 8 กิโลเมตร อยู่ใกล้ๆ กับเกาะปอดะห่างไปทางทิศใต้เล็กน้อย เกาะไก่เป็นหนึ่งในสามเกาะที่ทำให้เกิดสันทรายที่เราเรียกว่า "ทะเลแหวก " สันหทรายด้านทิศตะวันตกที่มีสันฐานมาจากเกาะใหญ่นั่นคือเกาะไก่</p>
                        <p>ที่เรียกว่าเกาะไกก็เพราะว่าทางด้านปลายสุดของเกาะมีหินแหลมๆ เมื่อมองขึ้นไปแล้วคล้ายคอไก่ บ้างก็ว่าคล้ายๆ ไก่งวง จะคล้ายอะไรก็แล้วแต่คนมองจะจินตนาการไปกัน</p>
                    </article>
                </div>
            </div>
            
        </div>
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
            
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
			console.log(offset);
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    @media (max-width: 768px) {
        .row-content [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin: 0 -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
    }
</style>

<?php include_once('_footer.php'); ?>