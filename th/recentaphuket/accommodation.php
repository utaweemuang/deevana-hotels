<?php
$title = 'Accommodaton | Recenta Phuket Suanluang l Official Hotel Group Website Thailand';
$desc = 'Accommodation: Guarantee best direct hotel rate starting from USD 30 per night; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'accommodation, recenta phuket, recenta suanluang, phuket, suan luang, Suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'accommodation room';
$cur_page = 'accommodation';

$lang_en = '/recentaphuket/accommodation.php';
$lang_th = '/th/recentaphuket/accommodation.php';
$lang_zh = '/zh/recentaphuket/accommodation.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/1500/room-01.jpg" alt="Room 01" />
                    <img src="images/accommodations/1500/room-02.jpg" alt="Room 02" />
                    <img src="images/accommodations/1500/room-03.jpg" alt="Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องพัก <span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/600/room-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/600/room-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องพัก</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/600/room-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ห้องดีลักซ์ของเรามีขนาด 32 ตารางเมตร ในห้องพักมีเตียงแบบคิงไซส์ขนาด 7 ฟุตที่นุ่มสบาย ห้องน้ำส่วนตัวมีสิ่งอำนวยความสะดวกเต็มรูปแบบ และบริการอินเตอร์เน็ตความเร็วสูงโดยไม่คิดค่าใช้จ่าย</p>
                            <p><span style="color: #1a355e;">จำนวนห้องพักทั้งหมด : 66 ห้อง</span></p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">อุปกรณ์ภายในห้องพัก</h2>
                                    <ul class="amenities-list">

                                        <li>เครื่องปรับอากาศ ที่เลือกปรับอุณหภูมิเองได้</li>
                                        <li>ทีวีแอลอีดี พร้อมด้วยช่องเคเบิ้ลทีวี</li>
                                        <li>ห้องน้ำส่วนตัว พร้อมด้วยฝักบัวน้ำอุ่นและน้ำเย็น</li>
                                        <li>เต้าเสียบปลั๊กไฟแบบสากล</li>
                                        <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>

                                        <ul class="list-columns-2">
                                            <li>เครื่องปรับอากาศ ที่เลือกปรับอุณหภูมิเองได้</li>
                                            <li>สิ่งอำนวยความสะดวกในห้องน้ำ ประกอบด้วย : แชมพู, สบู่, เจลอาบน้ำ, หมวกอาบน้ำ</li>
                                            <li>ทีวีแอลอีดี พร้อมด้วยช่องเคเบิ้ลทีวี</li>
                                            <li>ห้องน้ำส่วนตัว พร้อมด้วยฝักบัวน้ำอุ่นและน้ำเย็น</li>
                                            <li>เต้าเสียบปลั๊กไฟแบบสากล</li>
                                            <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                            <li>อุปกรณ์สำหรับชงชาและกาแฟ</li>
                                            <li>ฟรีน้ำดื่ม 2 ขวดต่อวัน</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>รองเท้าแตะ</li>
                                            <li>ตู้นิรภัย</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                            <li>ระเบียง</li>
                                            <li>ตู้เย็น</li>
                                            <li>กาต้มน้ำ</li>
                                            <li>ร่ม</li>
                                            <li>ถุงชายหาด</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>