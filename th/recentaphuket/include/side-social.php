<div id="social_side">
    <ul>
        <li><a class="icon fb" href="<?php echo get_info('facebook'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-facebook"></i></a></li>
        <li><a class="icon ig" href="<?php echo get_info('instagram'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-instagram"></i></a></li>
    </ul>
</div>