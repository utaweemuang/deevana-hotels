<script>
    ;(function ($) {
        var popup = {
            enabled: false,
            src: '',
            link: {
                enabled: flase,
                href: '',
                target: '_blank',
            }
        }
        if (popup.enabled === true) {
            $.magnificPopup.open({
                items: {
                    src: popup.src,
                    type: 'image',
                },
                mainClass: 'mfp-fade',
                removalDelay: 300,
                callbacks: {
                    open: function () {
                        if (popup.link.enabled === true) {
                            var $img = $(this.content).find('.mfp-img');
                            $img.wrap('<a href="' + popup.link.href + '" target="' + popup.link.target + '" />');
                        }
                    }
                }
            });
        }
    })(jQuery);
</script>