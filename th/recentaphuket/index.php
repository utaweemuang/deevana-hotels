<?php

session_start();

$title = 'Recenta Phuket Suanluang | Official Hotel Group Website Thailand';
$desc = 'Guarantee best direct hotel rate starting from USD 30 per night; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'recenta phuket, recenta suanluang, phuket, suan luang, Suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/recentaphuket';
$lang_th = '/th/recentaphuket';
$lang_zh = '/zh/recentaphuket';

include_once('_header.php');
?>

<main class="site-main">
    <section class="page-cover">
        <div id="home_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/home/home-slide-01.jpg" alt="Recenta Phuket Suanluang, 3-star hotel" /></div>
        </div>

<!--
        <div id="promotion_board" class="promotion get-center">
            <a href="<?php //ibe_url( get_info('ibeID'), 'en' ); ?>" target="_blank">
                <img class="block responsive" src="images/home/bann_recenta_suanluang_20160725.png" width="806" height="295" alt="Special Rate 10% Discount" />
            </a>
        </div>-->
        <?php //include_once('include/tl-sticky-banner.php'); ?>
        <div class="custom-hero-slide-nav"></div>
    </section>

    <?php include_once('include/booking_bar.php'); ?>

    <section class="site-content">

        <section id="intro" class="section pattern-fibers">
            <div class="container">
                <div class="row row-intro d-flex align-items-center">
                    <div class="col-12 col-lg-6">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ยินดีต้อนรับสู่ <br><span style="color:#707270;">โรงแรมรีเซ็นต้า ภูเก็ต สวนหลวง</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
                        <p>โรงแรมรีเซ็นต้าเป็นโรงแรมขนาดกลางที่ออกแบบมาให้เหมาะสมกับทุกโอกาสของคุณ คุณสามารถใช้วันหยุดอย่างสะดวกสบายกับราคาที่ไม่แพงและคุ้มค่าสำหรับเงินที่จ่าย ห้องพักทุกห้องมีขนาดใหญ่และออกแบบอย่างดีให้ใช้งานได้เต็มที่ ดังนั้นโรงแรมรีเซ็นต้าจึงมีการให้บริการและสิ่งอำนวยความสะดวกที่มีคุณภาพสูง เหมาะสำหรับทุกเพศและทุกวัย คุณสามารถเชื่อมั่นในแบรนด์ของเรา</p>
                        <div class="clearfix"></div>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="resp-container">
                            <iframe width="100%" height="370" src="https://www.youtube.com/embed/a6AYRy4ezWA?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <!-- <div class="row row-intro">
                    <div class="col-content">
                        <h1 class="section-title deco-underline style-left">
                            <span style="font-size: 26px;">ยินดีต้อนรับสู่ <br><span style="color:#707270;">โรงแรมรีเซ็นต้า ภูเก็ต สวนหลวง</span></span>
                        </h1>

                        <p><img class="align-left" style="border: 3px solid #fff;" src="images/home/content.jpg" width="220" /></p>
                        <p>โรงแรมรีเซ็นต้าเป็นโรงแรมขนาดกลางที่ออกแบบมาให้เหมาะสมกับทุกโอกาสของคุณ คุณสามารถใช้วันหยุดอย่างสะดวกสบายกับราคาที่ไม่แพงและคุ้มค่าสำหรับเงินที่จ่าย ห้องพักทุกห้องมีขนาดใหญ่และออกแบบอย่างดีให้ใช้งานได้เต็มที่ ดังนั้นโรงแรมรีเซ็นต้าจึงมีการให้บริการและสิ่งอำนวยความสะดวกที่มีคุณภาพสูง เหมาะสำหรับทุกเพศและทุกวัย คุณสามารถเชื่อมั่นในแบรนด์ของเรา</p>
                        <div class="clearfix"></div>
                        <p><span class="button luxury-style">THE ONLY ALL INCLUSIVE<br><span style="font-size: 0.7em;">TO INCLUDE IT ALL LOOK CLOSER.</span></span></p>
                    </div>

                    <div class="col-countdown">
                        <?php //include('modules/widget-countdown/countdown.php'); ?>
                    </div>
                </div> -->
            </div>
        </section>
        
        <section id="offers" class="section">
            <div class="container text-center">
                <h1 class="section-title deco-underline"><span style="color:#ffffff;">Special Deals</h1>
                <div id="offers_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/roompromotion?propertyid=294&group=23&width=450&height=300&imageid=13970&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>BOOK DIRECT GET 10% DISCOUNT (10% DISCOUNT)</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>Late check-out by 14.00</li>
                                        <li>Welcome drink upon arrival</li>
                                        <li>Daily tea and coffee in room</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=294&onlineId=4&pid=MDcyNzI0Ng%3D%3D&lang=th&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/ratetype?propertyid=294&group=21&width=450&height=300&imageid=1710&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>BEST AVAILABLE RATE - NO BREAKFAST</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>In-room internet</li>
                                        <li>Welcome drink upon arrival</li>
                                        <li>Daily tea and coffee in room</li>
                                        <li>Free Late check-out until 14.00 hrs.</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=294&onlineId=4&pid=MDY3MzE%3D&lang=th&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <img class="force" src="https://images.travelanium.net/crs-file-manager/images/ratetype?propertyid=294&group=21&width=450&height=300&imageid=1710&type=jpg" />
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="block-content-wrapper">
                                    <h2 class="title"><b>BEST AVAILABLE RATE - WITH BREAKFAST</b></h2>
                                    <p class="description" style="color:yellow;">Min stay 1 night</p>
                                    <ul>
                                        <li>Daily breakfast</li>
                                        <li>In-room internet</li>
                                        <li>Welcome drink upon arrival</li>
                                        <li>Daily tea and coffee in room</li>
                                        <li>Free Late check-out until 14.00 hrs.</li>
                                    </ul>
                                    <a target="_blank" class="button" href="https://reservation.travelanium.net/hotelpage/rates/?propertyId=294&onlineId=4&pid=MDY4ODE%3D&lang=th&currency=THB">BOOK NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="activities" class="section section-activities">
            <div class="container">
                <h1 class="section-title deco-underline"><span style="color:#707270;">The only all-inclusive TO INCLUDE IT ALL</h1>

                <div id="activities_slider" class="owl-carousel has-nav force-nav fx-scale">
                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-fitness.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">ฟิตเนส</h2>
                            <p class="description">ห้องฟิตเนสของเราตั้งอยู่บนชั้นแรก สามารถมองเห็นสระว่ายน้ำและสวน</p>
                            <p><a class="button" href="facilities.php#fitness">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <!-- <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/facility-spa.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">สปาโอเรียลทาล่า</h2>
                            <p class="description">สปาเป็นสถานที่สำหรับเพิ่มความสดชื่นและผ่อนคลายให้แก่ร่างกายและจิตใจของคุณ</p>
                            <p><a class="button" href="facilities.php#orientala_wellness_spa">READ MORE <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div> -->

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/slide-facilities/swimming_pool.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">สระว่ายน้ำ</h2>
                            <p class="description">ครอบครัวสามารถใช้เวลาด้วยกันในสระว่ายน้ำสำหรับผู้ใหญ่และสระว่ายน้ำสำหรับเด็กๆ หรือจะบริเวณริมสระน้ำซึ่งมีเก้าอี้อาบแดดให้บริการ ในระหว่างที่เด็กๆเล่นน้ำกัน </p>
                            <p><a class="button" href="facilities.php#swimming_pool">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a></p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="thumbnail">
                            <img class="force" src="images/home/slide-facilities/room_service.jpg" />
                        </div>
                        <div class="caption">
                            <h2 class="title">สะดวกสบายไปกับการบริการ 24 ชั่วโมงของเรา</h2>
                            <p class="description">พนักงานของเราพร้อมให้บริการตลอด 24 ชั่วโมงเพื่อตอบสนองความต้องการของคุณ</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="location" class="section">
            <div class="container">
                <h1 class="section-title"><span class="deco-map deco-underline" style="display: inline-block;">สถานที่ท่องเที่ยว ภูเก็ต</span></h1>

                <div class="row row-locations">
                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-old_phuket_town.jpg" alt="Old Phuket Town"/></div>
                            <h2 class="title">เมืองเก่าภูเก็ต</h2>
                            <a class="more" href="attraction.php#old_phuket_town">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phromthep_cape.jpg" alt="Phromthep Cape"/></div>
                            <h2 class="title">แหลมพรหมเทพ</h2>
                            <a class="more" href="attraction.php#phromthep_cape">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-phuket_walking_street.jpg" alt="Phuket Walking Street"/></div>
                            <h2 class="title">ถนนคนเดินภูเก็ต</h2>
                            <a class="more" href="attraction.php#phuket_walking_street">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>

                    <div class="col-w3 col-location">
                        <div class="location">
                            <div class="thumbnail"><img class="block force" src="images/home/location-big_buddha.jpg" alt="Phuket Big Buddha"/></div>
                            <h2 class="title">วัดพระใหญ่</h2>
                            <a class="more" href="attraction.php#big_buddha">อ่านต่อ <i class="icon fa fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="show-awards">
            <div class="container">
                <ul class="list-awards">
                    <li><img src="http://www.deevanahotels.com/images/awards/green-silver-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="http://www.deevanahotels.com/images/awards/SHA-logo-recenta-phuket-01.png" alt="" width="128" height="128"></li>

                </ul>           
            </div>
        </section>
    </section>
</main>

<style>
    #offers {
        background-color : #b56200;
    }
    #offers .deco-underline:after {
        background-color : #fff;
    }
    .block-thumbnail {
        position: relative;
    }
    .block-content {
        position: relative;
    }
    .block-content-wrapper {
        position: relative;
        background-color: transparent;
        padding: 25px 15px;
        color: #fff;
        z-index: 1;
    }
    .block-content-wrapper .button {
        display: inline-block;
        background-color: #93b006;
        color: #fff;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    .block-content-wrapper .button:hover,
    .block-content-wrapper .button:active {
        opacity: .9;
    }
    .block-position {
        position: relative;
    }
    @media(min-width: 768px){
        .block-thumbnail {
            width: 50%;
        }
        .block-content {
            width: 50%;
        }
        .block-content-wrapper {
            padding: 25px 15px;
            text-align: left;
        }
        .block-content .triangle {
            display: block;
            position: absolute;
            top: -2px;
            left: -85px;
            width: 120%;
            height: 100%;
            z-index: 0;
            border-left: 6rem solid transparent;
            border-bottom: 13rem solid #000;
        }
        .block-position {
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-align:center;
            -ms-flex-align:center;
            align-items:center;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center
        }
    }
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .resp-container {
        width: 100%;
        height: 100%;
    }
    .img-cover, .video-cover{
        width:100%;
        height:100%;
        -o-object-fit:cover;
        object-fit:cover;
        font-family:'object-fit: cover;'
    }
    [class*="ratio-"] {
        position: relative;
        width: 100%;
        height: 0;
    }
    .ratio-wide {
        padding-top: 56.25%;
    }
    .ratio-square {
        padding-top: 100%;
    }
    .ratio-item {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    #promotion_board {
        position: absolute;
        bottom: 5%;
        left: 50%;
        z-index: 2;
        max-width: 810px;
        width: 70%;
    }
    .site-content .section {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .site-content .section-title {
        text-align: center;
    }
    #intro {
        min-height: 500px;
    }
    #intro .section-title {
        text-align: left;
    }
    .row-intro .col-content {
        width: 100%;
        padding-right: 340px;
    }
    .row-intro .col-countdown {
        position: absolute;
        top: 0;
        right: 15px;
        width: 290px;
    }
    #activities {
        background-image: url(images/home/bg-activities.jpg);
        background-position: center;
        background-size: cover;
        padding-top: 50px;
        padding-bottom: 40px;
    }
    #activities_slider .thumbnail {
        border: 5px solid #fff;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.3);
        box-shadow: 0 1px 3px rgba(0,0,0,0.3);
    }
    #activities_slider .caption {
        margin-top: 30px;
        text-align: center;
    }
    #activities_slider .caption .title {
        color: #5c4d33;
        font-size: 22px;
        text-transform: uppercase;
    }
    #activities_slider .caption .description {
        margin: 0;
        font-size: 13px;
    }
    #activities_slider .caption .button {
        display: inline-block;
        background-color: #ebebeb;
        padding: 0 10px;
        font-family: 'Cinzel', serif;
        font-size: 12px;
        color: #111;
        line-height: 2;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
        box-shadow: 0 1px 2px rgba(0,0,0,.4);
    }
    #activities_slider .caption .button:hover {
        opacity: 0.9;
    }
    #activities_slider .center .caption .button {
        background-color: #707270;
        color: #fff;
    }
    #location {
        background-image: url(images/home/bg-attraction.jpg);
        background-position: center;
        background-size: cover;
        padding: 20px 0 80px;
    }
    #location .section-title {
        margin: 50px 0;
    }
    #location .location {
        text-align: center;
    }
    #location .location .thumbnail {
        position: relative;
        border: 4px solid #fff;
        border-radius: 2px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px rgba(0,0,0,.3);
        box-shadow: 0 1px 3px rgba(0,0,0,.3);
        z-index: 3;
    }
    #location .location .title {
        position: relative;
        background-color: #e5e5e5;
        color: #333;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        line-height: 32px;
        margin-left: 10px;
        margin-right: 10px;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.5);
        box-shadow: 0 0 2px rgba(0,0,0,.5);
        z-index: 2;
    }
    #location .location .more {
        display: block;
        margin-left: 20px;
        margin-right: 20px;
        font-size: 11px;
        font-weight: 500;
        line-height: 24px;
        color: #fff;
        background-color: #adadad;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 1px rgba(0,0,0,.3);
        box-shadow: 0 0 1px rgba(0,0,0,.3);
    }
    #location .location .more:hover {
        opacity: 0.9;
    }
    @media (max-width: 720px) {
        .row-intro .col-content,
        .row-intro .col-countdown {
            float: none;
        }

        .row-intro .col-content {
            padding-right: 10px;
        }

        .row-intro .col-countdown {
            position: static;
            margin: 30px auto 0;
        }
        #location .section-title {
            margin: 30px 0;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            position: relative;
            top: 0;
            left: 50%;
            margin-left: -80px;
        }
        .row-locations .col-w3 {
            width: 50%;
            margin-bottom: 30px;
        }
    }
    @media (max-width: 640px) {
        .owl-carousel.has-nav {
            padding-left: 0;
            padding-right: 0;
        }
        .owl-carousel.has-nav .owl-nav {
            display: none;
        }
    }
</style>

<script>
    $(function() {
        $('#offers_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 800,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 1, },
                600: { items: 1, },
            },
        });
        $('#activities_slider').owlCarousel({
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            navText: ['<span class="sprite slide-nav-left"></span>', '<span class="sprite slide-nav-right"></span>'],
            dots: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });

        $.fn.calcMarginLeft = function() {
            var $this = $(this);

            $(window).on('resize', function() {
                var width = $this.outerWidth();
                var height = $this.outerHeight();
                $this.css({
                    marginLeft: -width/2,
                });
            }).trigger('resize');
        }

        $('.get-center').calcMarginLeft();

		if( $('#countdown').length ) {
			var getHeight = $('#countdown').outerHeight();
			$('#intro').css({
				'min-height': getHeight,
			});
		}
        var $banner = $('.tl-sticky-banner'),
            $toggle = $('.tl-sticky-banner-toggle'),
            $close = $('.tl-sticky-banner .content-close');

        $toggle.on('click', function() {
            $toggle.addClass('show');
            $banner.addClass('show');
        });

        $close.on('click', function() {
            $toggle.removeClass('show');
            $banner.removeClass('show');
        });

        if( window.innerWidth >= 320 ) {
            $toggle.addClass('show');
            $banner.addClass('show');
        }

        $(window).on('load', function() {
            setTimeout(function() {
                $banner.addClass('ready');
                $toggle.addClass('ready');
            }, 1000);
        });

        $('.sticky-banner-carousel').owlCarousel({
            items: 1,
            loop: 1,
            autoplay: 1,
            smartSpeed: 800,
            margin: 10,
            nav: 1,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            dots: false,
        });
    });
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>

<?php include 'include/popup-image.php'; ?>
<?php include_once('_footer.php'); ?>
