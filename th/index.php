<?php

session_start();

$title = 'Phuket & Krabi Resort | Deevana Hotels & Resorts provide comfortable accommodation in Phuket and Krabi. The ideal choice of professionalism quality service, completely amenities and luxury wellness spa.';
$desc = 'Deevana Hotels & Resorts provide comfortable accommodation in Phuket and Krabi. The ideal choice of professionalism quality service, completely amenities and luxury wellness spa.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'home';
$cur_page = 'home';

$lang_en = '/';
$lang_th = '/th';
$lang_zh = '/zh';

include_once('_header.php');
?>

<style>
    .mfp-zoom-out-cur,
    .mfp-zoom-out-cur
    .mfp-image-holder
    .mfp-close {
        cursor: pointer !important;
    }
</style>

<div id="home_slider" class="slider home-slider owl-carousel">
    <div class="item col-phuket" data-bg="images/home/hero_phuket-3.jpg">
        <a href="attraction-phuket.php"><img class="book" src="images/home/book_phuket.png" /></a>
    </div>

    <div class="item col-krabi" data-bg="images/home/hero_krabi-3.jpg">
        <a href="attraction-krabi.php"><img class="book" src="images/home/book_krabi.png" /></a>
    </div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">

        <section id="offers" class="section oval-shad">
            <div class="container">
                <h1 class="section-title underline"><span style="color:#766b58;">โปรโมชั่น</span></h1>

                <div id="offer_slider" class="owl-carousel slider has-nav">

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../deevanakrabiresort/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/deevanakrabiresort_homepromo.jpg" alt="Deevana Krabi Resort" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมดีวาน่า กระบี่ รีสอร์ท<br/>
                                <span>ราคา 1,080 บาท<b>ราคาเริ่มต้น</b></span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../deevanaplazakrabi/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/02_deevana_plaza_krabi_aonang.jpg" alt="Deevana Plaza Krabi Ao Nang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมดีวาน่า พลาซ่า กระบี่ อ่าวนาง<br/>
                                <span>ราคา 1,299 บาท</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../deevanapatong/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/04_deevana_patong_resort_spa.jpg" alt="Deevana Patong Resort &amp; Spa" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา<br/>
                                <span>ราคา 1,000 บาท</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../deevanaplazaphuket/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/01_deevana_plaza_phuket_patong.jpg" alt="Deevana Plaza Phuket Patong" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมดีวาน่า พลาซ่า ภูเก็ต ป่าตอง<br/>
                                <span>ราคา 1,460 บาท</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="https://www.deevanahotels.com/ramadaphuketdeevana/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/ramada-phuket-deevana.jpg" alt="Ramada Phuket Deevana" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p> โรงแรมรามาด้า ภูเก็ต ดีวาน่า
                                <br/>
                                <span>ราคา 1,300 บาท</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../recentasuitephuket/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/recenta_suite.jpg" alt="Recenta Suite Phuket Suanluang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมรีเซ็นต้า สวีท ภูเก็ต สวนหลวง<br/>
                                <span><!-- ราคา 990 บาท -->Temporality closed</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../recentaphuket/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/07_recenta_suanluang.jpg" alt="Recenta Phuket Suanluang" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมรีเซ็นต้า ภูเก็ต สวนหลวง<br/>
                                <span><!-- ราคา 720 บาท -->Temporality closed</span>
                            </p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <a href="../recentastyle/" target="_blank"><img src="https://www.deevanahotels.com/images/home/slide_offers/08_recenta_express.jpg" alt="Recenta Style Phuket Town" width="250" height="220"/></a>
                        </div>

                        <figcaption class="caption">
                            <p>
                                โรงแรมรีเซ็นต้า สไตล์ ภูเก็ตทาวน์<br/>
                                <span><!-- ราคา 720 บาท -->Temporality closed</span>
                            </p>
                        </figcaption>
                    </figure>
                </div><!--#offer_slider-->
            </div>
        </section>

        <section id="awards" class="section pattern-noise">

            <!-- <h2 class="title"><span>E-Calendar</span></h2>
            <a target="_blank" href="https://online.fliphtml5.com/wdroo/pqxe/"><img src="https://www.deevanahotels.com/images/home/calendar2020.jpg" alt="Deevana Hotels Calendar 2020" width="250" height="250"></a>
            <br><br>
            <h3 class="header">Download</h3><a href="download/2020-calendar_edit_page.pdf" target="_blank" rel="noopener">
            <div class="btn btn-secondary"><i class="fa fa-file" aria-hidden="true"></i> Calendar </div></a><br> -->

            <div class="container">
                <h1 class="title">รางวัลที่ได้รับ</h1>
                <p>โรงแรมและรีสอร์ทของดีวาน่า เราใช้ความคิดสร้างสรรค์ ความมุ่งมั่นและปรับปรุงเปลี่ยนแปลงสิ่งต่างๆอยู่เสมอ เพื่อที่จะเป็นโรงแรมที่ดีที่สุด นั่นคือเหตุผลว่าทำไมปีแล้วปีเล่าเราจีงภูมิใจกับรางวัลอันทรงคุณค่าที่เราได้รับจากบรรดาผู้บริโภคที่มีชื่อเสียงและสิ่งพิมพ์ที่เกี่ยวกับการท่องเที่ยวต่างๆ</p>
                <ul class="list-awards">
                    <li><img src="https://www.deevanahotels.com/images/awards/green-gold-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="https://www.deevanahotels.com/images/awards/tceb-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="https://www.deevanahotels.com/images/awards/atta-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="https://www.deevanahotels.com/images/awards/tha-awards.png" alt="" width="128" height="128"></li>
                    <li><img src="https://www.deevanahotels.com/images/awards/SHA_Logo.png" alt="" width="128" height="128"></li>
                </ul>
            </div>
        </section>

        <section id="moments" class="section">
            <div class="container">
                <h1 class="section-title underline"><span style="color: #0072bb;">ช่วงเวลาดีๆ</span>กับดีวาน่า</h1>

                <div id="moment_slider" class="owl-carousel fx-scale slider has-nav">
                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/cooking_class.jpg" alt="Cooking Class" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">เรียนทำอาหาร</h2>
                            <p class="excerpt">ให้คุณเพลิดเพลินในการเรียนทำอาหารกับพ่อครัวของเราอย่างใกล้ชิดและเป็นส่วนตัว</p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/romantic_private_dinner.jpg" alt="Romantic Private Dinner" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">ทานอาหารค่ำแบบโรแมนติกและเป็นส่วนตัว</h2>
                            <p class="excerpt">อิ่มอร่อยกับอาหารมื้อค่ำอันน่าจดจำและดื่มด่ำไปกับประสบการณ์โรแทนติกในการรับประทานอาหารภายใต้แสงดวงดาว หรือทานอาหรมื้อพิเศษของคุณด้วยการตกแต่งพิเศษตามคำขอของคุณ</p>
                        </figcaption>
                    </figure>

                    <figure class="item">
                        <div class="thumbnail">
                            <img src="images/home/moments/spa.jpg" alt="Spa" />
                        </div>
                        <figcaption class="caption">
                            <h2 class="title">สปา</h2>
                            <p class="excerpt">ช่วยสร้างความสุขในการท่องเที่ยวของคุณ และฟื้นฟูความสมดุลระหว่างร่างกายและจิตใจ</p>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </section>

        <section id="attraction" class="section wing-shad">
            <div class="container">
                <h1 class="section-title underline">
                    <span class="deco-map"><span style="color:#809a00;">สถานที่ท่องเที่ยว</span>ภูเก็ต &amp; กระบี่ </span>
                </h1>

                <div class="row row-attraction">
                    <div class="col-w6 phuket">
                        <h2>สถานที่ท่องเที่ยว ภูเก็ต</h2>

                        <div class="row row-phuket">
                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Old Phuket Town" class="thumbnail force" src="../images/home/Phuket-Old-Town.jpg" />
                                    <h3 class="title">เมืองเก่าภูเก็ต</h3>
                                    <a class="more" href="attraction-phuket.php#old_phuket_town">อ่านต่อ</a>
                                </div>
                            </div>

                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Big Buddha Phuket" class="thumbnail force" src="../images/home/Promthep-cape.jpg" />
                                    <h3 class="title">แหลมพรหมเทพ</h3>
                                    <a class="more" href="attraction-phuket.php#promthep_cape">อ่านต่อ</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-w6 krabi">
                        <h2>สถานที่ท่องเที่ยว กระบี่</h2>

                        <div class="row row-krabi">
                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Emerald Pool" class="thumbnail force" src="../images/home/Talay-Wak.jpg" />
                                    <h3 class="title">ทะเลแหวก</h3>
                                    <a class="more" href="attraction-krabi.php#thalay_wak">อ่านต่อ</a>
                                </div>
                            </div>

                            <div class="col-w6 col-item">
                                <div class="inner">
                                    <img alt="Elephant Ride" class="thumbnail force" src="../images/home/Nong_Thale_Canal.jpg" />
                                    <h3 class="title">คลองหนองทะเล</h3>
                                    <a class="more" href="attraction-krabi.php#nong_thale_canel">อ่านต่อ</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php

if (!isset($_SESSION['visited'])) { ?>
<?php }

$_SESSION['visited'] = "true";
?>
<div style="display: none;" class="header-div <?=$visited?'':'first-visit'?>">...</div>

<script>
    $(function() {
        $('#home_slider').owlCarousel({
            dots: false,
            nav: false,
            autoplay: 5000,
            pullDrag: false,
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, loop: true, },
                720: { items: 2, loop: false, },
            },
        });

        $('#offer_slider').owlCarousel({
            items: 4,
            smartSpeed: 300,
            loop: true,
            dots: false,
            nav: true,
            navText: ['<span class="sprite-arrow-left"></span>', '<span class="sprite-arrow-right"></span>'],
            responsiveRefreshRate: 200,
            responsive: {
                0: { autoWidth: true, autoplay: 5000 },
                641: { items: 3, },
                720: { items: 4 },
            },
        });

        $('#moment_slider').owlCarousel({
            items: 3,
            autoplay: true,
            center: true,
            loop: true,
            smartSpeed: 300,
            nav: true,
            dots: false,
            navText: ['<span class="sprite-arrow-left"></span>', '<span class="sprite-arrow-right"></span>'],
            responsiveRefreshRate: 200,
            responsive: {
                0: { items: 1, },
                480: { items: 2, },
                600: { items: 3, },
            },
        });
    });
</script>

<style>
    .list-awards {
        padding-left: 0;
        list-style: none;
        -webkit-columns: 4 128px;
        columns: 4 128px;
        -webkit-column-gap: 10px;
        column-gap: 10px
    }
    @media(min-width: 1200px){
        .list-awards {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items:center;
        }
    }
    .list-awards>li {
        text-align: center;
        padding: 0 10px;
    }
    .site-main > .inner {
        padding: 0;
    }
    .section {
        padding: 50px 0;
    }
    .slider .item {
        margin: 0;
        padding: 0 10px;
    }
    #offers.oval-shad {
        display: block;
        margin: auto;
        position: relative;
        background: #fff;
        z-index: 1;
    }
    #offers.oval-shad:before {
        content: '';
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-image: url(images/home/bg_special_offers.jpg);
        background-position: center;
        background-size: cover;
        position: absolute;
        z-index: 0;
    }
    #offers.oval-shad:after {
        content: '';
        width: 80%;
        height: 10%;
        border-radius: 50%;
        box-shadow: 0 0 10px rgba(0,0,0,.5);
        display: block;
        position: absolute;
        z-index: -1;
        left: 10%;
        bottom: 0;
    }
    #offer_slider .thumbnail {
        border: 2px solid #f5f5f5;
    }
    #offer_slider .caption {
        font-size: 13px;
    }
    #offer_slider .caption span {
        font-family: 'Cinzel', serif;
        font-size: 15px;
        color: #304446;
    }
    #awards {
        text-align: center;
        color: #797266;
    }
    #awards .title {
        font-family: 'Roboto Condensed', sans-serif;
        font-size: 20px;
        font-weight: 300;
    }
    #awards .title span {
        font-family: 'Cinzel', serif;
        font-size: 1.8em;
        color: #373737;
    }
    #moments {
        background-image: url(images/home/bg_moment.png);
        background-position: center;
        background-size: cover;
    }
	#moment_slider .owl-nav.disabled {
		display: block; /*force show nav arrows*/
	}
    #moment_slider .thumbnail {
        border: 5px solid #f5f5f5;
        box-shadow: 0 0 3px rgba(0,0,0,.3);
    }
    #moment_slider .item {
        padding: 0;
    }
    #moment_slider .caption {
        font-size: 12px;
        padding: 8px;
    }
    #moment_slider .title {
        font-family: 'Cinzel', serif;
        font-weight: 400;
        font-size: 18px;
        color: #5c4d33;
        margin: 0;
    }
    #moment_slider .excerpt {
        margin: 5px 0;
    }
    #attraction {
        background-image: url(images/home/bg_attraction.jpg);
        background-position: center;
        background-size: cover;
        position: relative;
        padding: 60px 0 70px;
    }
    #attraction .section-title:after {
        margin-left: 40px;
    }
    .row-attraction:before {
        content: '';
        background-image: url(assets/elements/seperate_columns.png);
        width: 33px;
        height: 402px;
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -15px;
        margin-top: -140px;
    }
    .row-attraction .row-phuket {
        padding-right: 15px;
    }
    .row-attraction .row-krabi {
        padding-left: 15px;
    }
    .row-attraction h2 {
        margin-top: 0;
        margin-bottom: 15px;
        font-size: 18px;
        color: #304446;
    }
    .row-attraction .krabi h2 {
        text-align: right;
    }
    .row-attraction .inner {
       text-align: center;
    }
    .row-attraction .inner .thumbnail {
        border: 4px solid #fff;
        border-radius: 3px;
        overflow: hidden;
        display: block;
        position: relative;
        z-index: 2;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
    }
    .row-attraction .inner .title {
        background-color: #e5e5e5;
        margin: 0;
        font-family: 'Roboto', sans-serif;
        font-size: 14px;
        color: #ab8205;
        line-height: 18px;
        margin-left: 10px;
        margin-right: 10px;
        padding: 5px;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
        border-radius: 0 0 2px 2px;
        position: relative;
        z-index: 1;
    }
    .row-attraction .inner .more {
        display: block;
        background-color: #63b4d8;
        font-size: 12px;
        color: #fff;
        margin: 0 24px;
        border-radius: 0 0 2px 2px;
        -webkit-box-shadow: 0 0 3px rgba(0,0,0,.3);
        box-shadow: 0 0 3px rgba(0,0,0,.3);
        padding: 3px;
    }
    .row-attraction .inner .more:hover {
        background-color: #333;
    }
    .deco-map {
        position: relative;
        left: 80px;
    }
    .deco-map:before {
        content: '';
        position: absolute;
        top: -18px;
        left: -160px;
        width: 160px;
        height: 78px;
        background-image: url(assets/elements/deco-map.png);
    }
    @media (max-width: 960px) {
        .row-attraction:before {
            content: none;
        }
        .row-attraction > .phuket,
        .row-attraction > .krabi {
            padding-bottom: 30px;
        }
        .row-attraction > .phuket {
            border-right: 1px solid #ccc;
        }
        .row-attraction > .krabi {
            border-left: 1px solid #fff;
        }
    }
    @media (max-width: 720px) {
        #attraction .section-title {
            margin-top: 50px;
        }
        #attraction .section-title:after {
            margin-left: -40px;
            *top: 180%;
        }
        .row-attraction > [class*="col-"] {
            width: 100%;
            border: 0;
        }
        .row-attraction > .phuket,
        .row-attraction > .krabi {
            padding-bottom: 0;
        }
        .row-attraction .row-phuket {
            margin-bottom: 30px;
            padding-right: 0;
        }
        .row-attraction .row-krabi {
            padding-left: 0;
        }
        .row-attraction .phuket h2,
        .row-attraction .krabi h2 {
            text-align: center;
        }
        .deco-map {
            left: 0;
        }
        .deco-map:before {
            display: block;
            top: -80px;
            left: 50%;
            margin-left: -80px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>
