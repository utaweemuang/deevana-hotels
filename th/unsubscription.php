<?php
$title = 'Unsubscribe Deevana Hotels & Resorts Emails';
$desc = 'Opt out for Deevana Hotels & Resorts newsletters';
$keyw = 'Thailand, Phuket beach, resort, accommodation, Phuket Hotels, Hotels Phuket, Hotel Patong, Patong Hotel, holiday Phuket, conferences, phuket vacations, phuket spa, krabi hotel';

$html_class = '';
$body_class = 'unsubscription';
$cur_page = 'unsubscription';

$lang_en = '/unsubscription.php';
$lang_th = '/th/unsubscription.php';
$lang_zh = '/zh/unsubscription.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/contact/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <section class="main-content sidebar-left">
            <div class="container">
                <div class="row">
                    <div class="col-w8 col-content">
                        <div class="site-content">
                            
                            <section class="section section-contact">
                        <header class="section-header">
                            <h1 class="section-title">Newsletter Unsubscription</h1>
                        </header>

                        <div class="content">
                            <form id="contact_form" class="form" action="unsubscription_form.php">
                                <div id="contact_result"></div>
                                <div class="row row-contact-form">
                                    <div class="col-w6">

                                        <div class="form-group">
                                            <span class="field field-email field-required">
                                                <label class="label" for>Email</label>
                                                <input class="input-text" id="email" name="email" type="email" required />
                                            </span>
                                        </div>

                                    </div>

                                    <div class="col-w6">
                               

                                        <div class="form-group">
                                            <span class="field field-recaptcha field-required">
                                                <div class="g-recaptcha" data-sitekey="6LeT9RkTAAAAAG5Xj-B4P_fvYQr5HMppJYW1FWup"></div>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-w12">
                                        <div class="form-group">
                                            <span class="field field-submit">
                                                <input type="hidden" name="sendto" value="<?php echo get_info('email'); ?>" />
                                                <button class="button" id="submit" type="submit">UNSUBSCRIBE</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                        </div>
                    </div>

                    <div class="col-w4 col-sidebar">
                        <div class="sidebar">
                            <aside class="aside">
                                <h3 class="title">Newsletter Opt-out</h3>
                                <p>If you wish to opt out from our mailing list, please enter your email here:</p>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $(function() {
        $('#detail-select').on('click', '[data-value]', function() {
            var $this = $(this);
            var tar = $this.data('value');
            $('.tabs-group').find(tar).fadeIn(300).siblings().hide();
            
            $this.addClass('active');
            $this.parents('ul.options').find('li.option').not($this).removeClass('active');
        });
        $('.tabs-group').find('.tab-content.default').show();
    });
</script>

<style>
    .section {
        border: 1px solid #ccc;
        margin-bottom: 30px;
    }
    .section-header {
        background-color: #1a355e;
        color: #fff;
        padding: 10px 30px;
        position: relative;
    }
    .section-title {
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        color: #fff;
        text-align: left;
    }
    .section .content {
        padding: 20px 30px 10px;
    }
    
    #contact_form .field {
        display: block;
    }
    #contact_form .input-text,
    #contact_form .input-select,
    #contact_form .input-textarea {
        border: 1px solid #ccc;
        border-radius: 3px;
    }
    #contact_form #submit {
        background-color: #9a7b12;
        border-radius: 4px;
        color: #fff;
        border: 0;
        height: 32px;
		padding:10px 30px ;
        line-height: 32px;
        text-align: center;
    }
    #contact_form #submit:hover {
        background-color: #333;
    }
    #detail-select {
        position: absolute;
        top: 50%;
        right: 30px;
        margin-top: -18px;
    }
	.gmap {
		margin-top: -20px;
		margin-left: -30px;
		margin-right: -30px;
		margin-bottom: 20px;
		border-bottom: 1px solid #ccc;
	}
	.gmap iframe {
		display: block;
		width: 100%;
		height: 200px;
	}
    .download-button {
        background-color: #333;
        padding: 0 12px;
        border-radius: 2px;
        display: inline-block;
        line-height: 2;
        color: #fff;
    }
    .download-button .icon {
        margin-right: 3px;
    }
    .download-button:hover {
        background-color: #444;
        color: #fff;
    }
    .tabs-group .tab-content {
        display: none;
    }
    @media (max-width: 640px) {
        .section-header {
            padding-left: 15px;
            padding-right: 15px;
        }
        .section .content {
            padding: 15px 15px 10px;
        }
        .row-contact-form .col-w6 {
            width: 100%;
        }
    }
    @media (max-width: 600px) {
        #detail-select {
            position: relative;
            top: inherit;
            right: inherit;
            margin-top: 10px;
            width: 100%;
        }
    }
</style>

<?php include_once('_footer.php'); ?>