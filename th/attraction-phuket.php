<?php
$title = 'Phuket Attractions | Deevana Hotels & Resorts';
$desc = 'Phuket is Thailand largest island and one of the most popular tourist destinations in Southeast Asia. This island is rich in natural resources and has the perfect weather for agriculture while providing colorful tropical vistas.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'attraction phuket';
$cur_page = 'attraction-phuket';

$lang_en = '/attraction-phuket.php';
$lang_th = '/th/attraction-phuket.php';
$lang_zh = '/zh/attraction-phuket.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/attraction/phuket/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
	
		<section class="main-content">
			<div class="container">

				<h1 class="section-title underline">สถานที่ท่องเที่ยว ภูเก็ต</h1>
				
				<div class="row row-content">
					<div class="col-w3 col-navigation">
						<div class="post-tabs tabs-group">
							<ul>
								<li class="tab active" data-tab="#big_buddha_phuket">วัดพระใหญ่</li>
								<li class="tab" data-tab="#old_phuket_town">เมืองเก่าภูเก็ต</li>
								<li class="tab" data-tab="#sino-portuguese_houses">สถาปัตยกรรมแบบชิโนโปรตุกีส</li>
								<li class="tab" data-tab="#phang_nga_road">ถนนพังงา</li>
								<li class="tab" data-tab="#thalang_road_and_soi_rommanee">ถนนถลางและซอยรมณีย์</li>
								<li class="tab" data-tab="#dibuk_road">ถนนดีบุก</li>
								<li class="tab" data-tab="#krabi_road">ถนนกระบี่</li>
								<li class="tab" data-tab="#ranong_road">ถนนระนอง</li>
                                <li class="tab" data-tab="#promthep_cape">แหลมพรหมเทพ</li>
								<li class="tab" data-tab="#patong_beach">หาดป่าตอง</li>
								<li class="tab" data-tab="#bangla_road">ถนนบางลา</li>
							</ul>
						</div>
					</div>
					
					<div class="col-w9 col-content tabs-content">
						<article class="article" id="big_buddha_phuket" data-tab-name="Big Buddha Phuket">
							<img class="thumbnail force block" src="images/attraction/phuket/big_buddha_phuket.png" />
							<h1>วัดพระใหญ่</h1>
							<p>พระใหญ่ของเกาะภูเก็ตเป็นสถานที่ที่ได้รับความเคารพนับถือและสำคัญแห่งหนึ่งของเกาะ พระพุทธรูปขนาดใหญ่ตั้งอยู่บนยอดเขานากเกิดซึ่งอยู่ระหว่างอ่าวฉลองและหาดกะตะ มีความสูงถึง 45 เมตร จึงสามารถมองเห็นมาจากสถานที่ที่อยู่ห่างออกไปได้อย่างง่ายดาย</p>
							<p>เนื่องจากตั้งอยู่บนทำเลที่สูง จึงทำให้เราสามารถมองเห็นวิวแบบ 360 องศาที่ดีที่สุดในเกาะภูเก็ต (คุณสามารถมองเห็นทิวทัศน์อันกว้างไกลของเมืองภูเก็ต หาดกะตะ หาดกะรน อ่าวฉลองและอื่นๆ) การเดินทางไปยังวัดพระใหญ่นั้นสะดวกสบาย เนื่องจากสถานที่แห่งนี้ห่างจากถนนสายหลักของเกาะภูเก็ตเพียง 6 กิโลเมตรเท่านั้น และยังเป็นหนึ่งในสถานที่ที่ต้องไปของเกาะภูเก็ตอีกด้วย</p>
							<p>บริเวณโดยรอบพระพุทธรูปเป็นสถานที่ที่เงียบสงบ ซึ่งคุณจะได้ยินเพียงเสียงจากการสั่นกระดิ่งเล็กๆ และเสียงจากการไหวต้องลมของธงสีเหลืองซึ่งเป็นสัญลักษ์ของศาสนาพุทธ พร้อมด้วยเสียงเพลงบทสวดมนต์แบบพระพุทธศาสนาเบาๆ</p>
							<p>โดยชื่อเรียกเต็มๆซึ่งเป็นที่รู้จักกันในหมู่ชาวไทยคือพระพุทธมิ่งมงคลเอกนาคคีรี โดยมีฐานกว้างถึง 25 เมตร องค์พระพุทธรูปทำจากหินอ่อนสีขาวที่สวยงามจากพม่า เรียงกันเป็นชั้นๆ ซึ่งเมื่อตกกระทบกับแสงจากดวงอาทิตย์ก็จะส่องประกายราวกับว่าเป็นสัญลักษณ์แห่งความหวัง ทิวทัศน์รวมถึงตัวพระพุทธรูปเองนั้นก็สวยงามและน่าทึ่งมากเช่นกัน </p>
						</article>
						
						<article class="article" id="old_phuket_town" data-tab-name="Old Phuket Town">
							<img class="thumbnail force block" src="images/attraction/phuket/old_phuket_town.png" />
							<h1>เมืองเก่าภูเก็ต</h1>
							<p>จังหวัดภูเก็ตมีความแตกต่างจากจังหวัดอื่นๆในประเทศไทย ตัวเมืองภูเก็ตมีลักษณะพิเศษโดดเด่นเฉพาะตัวแบบเมืองเก่าที่ไม่เหมือนใคร ในบริเวณเมืองจะเต็มไปด้วยเรื่องราวทางประวัติศาสตร์ คุณจะพบกับศาลเจ้าและวัดมากมาย (วัดไทยและศาลเจ้าจีน) อาคารพาณิชย์ที่ได้รับการอนุรักษ์และมีการตกแต่งอย่างสวยงามและหรูหรา ร้านกาแฟที่ดูแปลกตา โรงพิมพ์เล็กๆ พิพิทธภัณฑ์ของรัฐและเอกชน และสถานที่เล็กๆซึ่งเคยเป็นย่านโคมแดงในอดีต</p>
							<p>เมืองเก่าภูเก็ตได้ถูกสร้างจากทรัพย์สมบัติที่ได้จากยุคเฟื่องฟูของการทำเหมืองแร่ดีบุกเมื่อศตวรรษที่ผ่านมา ในยุคที่โลหะเป็นสินค้าที่มีราคาแพงมาก ดังนั้นในช่วงยุคนี้ของเมือง คุณจะได้พบเห็นคฤหาสน์สไตล์ชิโนโปรตุกีสมากมาย ซึ่งคฤหาสน์เหล่านี้ผู้เป็นเจ้าของล้วนมาจากเศรษฐีเหมืองดีบุกในอดีต เมืองเก่าภูเก็ตมีขาดเล็กพอที่คุณจะสามารถเดินเที่ยวรอบๆได้ในวันเดียว เวลาที่ดีที่สุดคือช่วงเช้าตรู่หรือในช่วงบ่ายที่อากาศไม่ร้อนมากนัก มีร้านอาหารและร้านกาแฟเพียงพอที่จะให้บริการ จึงไม่จำเป็นที่จะต้องเตรียมอาหารมา</p>
						</article>
						
						<article class="article" id="sino-portuguese_houses" data-tab-name="Sino-Portuguese Houses">
							<img class="thumbnail force block" src="images/attraction/phuket/sino-portuguese_house.png" />
							<h1>สถาปัตยกรรมแบบชิโนโปรตุกีส</h1>
							<p>สิ่งที่ต้องทำในภูเก็ตคือการเดินไปรอบๆตัวเมืองเก่า รอบๆถนนถลาง ถนนดีบุก และถนนกระบี่ สถาปัตยกรรมที่งดงามเหล่านี้จะตั้งอยู่ตลอดแนวถนน ซึ่งจะทำให้คุณรู้สึกเหมือนย้อนกลับไปสู่ความงดงามที่น่าหลงใหลเมื่อศตวรรษที่ผ่านมา</p>
							<p>ภุเก็ตเป็นเมืองที่มีการผสมผสานของผู้คนซึ่งเลือกที่จะใช้ชีวิตที่นี่จากหลากหลายเชื้อชาติได้อย่างน่าอัศจรรย์ มีทั้งไทย จีน มาเลย์ อินเดียและเนปาล เด็กๆลูกครึ่งยุโรปกับเอเชียซึ่งกำลังเติบโตขึ้นด้วยเช่น และยังรวมไปถึงผู้ซึ่งมีเชื้อสายผสมระหว่างชาวจีนฮกเกี้ยนและชาวไทยซึ่งถูกเรียกว่า "บ้าบ๋า" อีกด้วย</p>
							<p>ประเพณีของชาวบ้าบ๋านั้นสามารถพบเห็นได้ทั่วไปในเมืองเก่าของภูเก็ต ในสถาปัตยกรรม การค้าขาย การแต่งการและวิถีชีวิต แก่นแท้ของเมืองเก่านี้ได้ถูกสร้างจากถนนห้าสายและซอยเล็กๆ ซึ่งก็คือถนนรัษฎา ถนนพังงา ถนนถลาง ถนนดีบุกและถนนกระบี่ ย่านซึ่งเต็มไปด้วยประวัติศาสตร์ซึ่งถูกปล่อยปละละเลยมาหลายปีนี้กำลังอยู่ในระหว่างการบูรณะ</p>
							<p>เมื่อ 100 ปีก่อน ถนนถลางเป็นเหมือนศูนย์กลางของกิจกรรมทุกอย่าง เนื่องจากคนงานเหมืองแร่จะตรงไปที่นั่นเพื่อจับจ่ายใช้สอย ขายแร่ดีบุก และปรนเปรอตัวเองด้วยกิจกรรมที่ไม่น่าเอาเป็นแบบอย่างนัก พวกเขาเหล่านี้ล้วนมีชีวิตที่ยากลำบาก ฉะนั้นการผ่อนคลายด้วยเครื่องดื่มแอลกอฮอล์ ฝิ่น ผู้หญิงและเกมการพนัน จึงเป็นสิ่งที่ช่วยปลอบประโลมได้เป็นอย่างดีหลังจากการทำงานที่น่าเบื่อหน่ายในเหมืองแร่ดีบุก</p>
						</article>
						
						<article class="article" id="phang_nga_road" data-tab-name="Phang Nga Road">
							<img class="thumbnail force block" src="images/attraction/phuket/phang_nga_road.png" />
							<h1>ถนนพังงา</h1>
							<p>เลี้ยวขวาที่นี่ลงสู่ถนนเยาวราชและเลี้ยวขวาอีกครั้งไปสู่ถนนพังงา คุณจะพบกับร้านหนังสือมือสองเซาท์วินด์ทางซ้ายมือคุณ และหลังจากนั้นคุณจะพบตรอกซึ่งมีตัวอักษรภาษาจีนตรงบริเวณทางเข้า</p>
							<p>เส้นทางนี้จะตรงไปยังศาลเจ้าแสงธรรม ซึ่งถูกสร้างขึ้นเมื่อปี พ.ศ.2434 สวนหย่อมของศาลเจ้าเป็นสถานที่ที่เงียบสงบอย่างแท้จริง เหมาะที่จะนั่งพักผ่อนก่อนที่จะออกสำรวจการตกแต่งภายในที่มีสีสัน ทางซ้ายของสวนหย่อมจะมีแผ่นป้ายหินอ่อนสวยงามขนาดใหญ่ซึ่งเต็มไปด้วยชื่อของผู้ร่วมบริจาคและจำนวนเงินที่พวกเขาบริจาคเพื่อสร้างศาลเจ้าแห่งนี้ เยื้องจากทางออกของตรอกนี้ คุณจะพบกับตัวอย่างที่ดีของการบูรณะอาคารอย่างมีความคิดสร้างสรรค์ในรูปแบบของสยามอินดิโก้ ซึ่งเป็นร้านอาหารที่ปรับปรุงมาจากอาคารพานิชย์แบบจีนทั่วไปรวมเข้าด้วยกันเป็นภัตตาคารที่กว้างขวางซึ่งผสมผสานทั้งแบบเก่าและใหม่ได้อย่างสวยงาม</p>
							<p>ห่างไปเพียงไม่กี่เมตรทางฝั่งซ้ายมือ คือโรงแรมเดอะเมมโมรี แอท ออนออน ซึ่งอาจจะสวยน้อยกว่าแต่ก็เต็มไปด้วยบรรยากาศ โรงแรมออนออนเป็นสถานที่สำคัญแห่งหนึ่งของเมืองภูเก็ต และยังเป็นสถานที่ที่ถูกถ่ายทำเป็นโรงแรมเล็กๆในกรุงเทพฯในภาพยนตร์เรื่องเดอะ บีช (The Beach) ซึ่งตอนนี้โรงแรมแห่งนี้ได้ทำการบูรณะซ่อมแซมแล้ว แต่ในอดีต แม้จะมีเพียงห้องพักที่เรียบง่าย สิ่งอำนวยความสะดวกในห้องน้ำที่ไม่สมบูรณ์นัก พนักงานที่มักจะอารมณ์เสียมากที่สุดในเกาะ แต่คนมากมายก็ยังคงกลับมาที่นี่และมีจำนวนมากขึ้น อาคารของโรงแรมถูกสร้างในแบบชิโนโปรตุกีส มีซุ้มประตูทางเข้าสวยงามน่าทึ่งและห้องรับรองที่จัดเรียงด้วยพัดลมแบบใบพัด ซึ่งเป็นต้นแบบของแนวคิดของฮอลลีวู้ดเกี่ยวกับตะวันออกไกล</p>
						</article>
						
						<article class="article" id="thalang_road_and_soi_rommanee" data-tab-name="Thalang Road and Soi Rommanee">
							<img class="thumbnail force block" src="images/attraction/phuket/thalang_road_and_soi_rommanee.png" />
							<h1>ถนนถลางและซอยรมณีย์</h1>
							<p>เลี้ยวซ้ายที่สุดถนน มุ่งตรงไปยังถนนภูเก็ตและเดินผ่านสิ่งที่ดูเหมือนจะเป็นตึกแถวเรียงกันไปในบริเวณฝั่งตรงข้าม ซึ่งในความเป็นจริงแล้วเป็นโรงแรมหนึ่ง หลังจากนั้นเลี้ยวซ้ายไปยังถนนถลางและตรงไปทางทิศตะวันตก คุณจะพบกับใจกลางเมืองเก่าของภูเก็ตอย่างจัง บนถนนจะเต็มไปด้วยบรรยากาศของประวัติศาสตร์ ที่นี่คุณจะเดินผ่านทางเดินที่มีหลังคาเป็นรูปโค้ง ทางเดินเหล่านี้ถูกเรียกว่า"ทางเดินกว้างห้าฟุต" และส่วนใหญ่จะเชื่อมโยงกัน ทำให้สามารถสามารถหลบแสงแดดและฝนได้อย่างดี แต่บางจุดมีการก่อกำแพงขวางไว้และบางส่วนดูระเกะระกะเนื่องจากมีการวางสิ้นค้าไว้ แต่ถึงกระนั้นการผสมผสานระหว่างสีสันและการออกแบบที่เป็นรูปแบบเดียวกัน พร้อมด้วยการผสมผสานของรูปแบบการการค้าขาย ก็ทำให้ถนนเส้นนี้เป็นหนึ่งในการผสมผสานที่น่าประทับใจ</p>
							<p>บริเวณปลายสุดของถนนเส้นนี้จะมีร้านโรตีจำนวนสองร้าน คลินิกแพทย์แผนจีน ร้านค้าที่ทั้งร้านจะขายเพียงเสื้อสีขาวเท่านั้น ร้านจักรยานและร้านขายสิ่งทอที่ดูธรรมดาๆ ขึ้นไปด้านขวามือคือซอยรมณีย์ ถนนเส้นนี้มีอดีตที่น่าสนใจ มันเคยเป็นซอยแห่งสถานเริงรมย์ หรือย่านแห่งความเพลิดเพลิน ที่ซึ่งคนงานชาวจีนจะไปปลดปล่อยความต้องการของพวกเขา ในความเป็นจริงคำว่า "รมณีย์" แปลอย่างผิวเผินได้ว่า "สิ่งบันเทิงใจ" ทุกวันนี้ซอยนี้เป็นตัวอย่างหนึ่งของพื้นที่ที่มีศักยภาพ เต็มไปด้วยบ้านและร้านกาแฟซึ่งมีเอกลักษณ์เฉพาะตัว (ยังมีรถโบราณที่ได้รับการปรับแต่งอย่าง Ford Consul ปี 1960 มาคอยต้อนรับอีกด้วย)</p>
						</article>
						
						<article class="article" id="dibuk_road" data-tab-name="Dibuk Road">
							<img class="thumbnail force block" src="images/attraction/phuket/dibuk_road.png" />
							<h1>ถนนดีบุก</h1>
							<p>เลี้ยวขวาออกจากถนนถลางและข้ามไปอีกฝั่งแล้วเลี้ยวซ้ายมุ่งสู่ถนนดีบุก ถนนสายนี้เป็นตัวอย่างที่ปราดเปรื่องของการบูรณะบ้านเรือนแบบจีนได้อย่างดี ถนนมีความกว้างจึงสามารถจัดการจราจรแบบสองช่องทางได้ ไม่เหมือนกับระบบเมืองเก่าที่เป็นถนนแคบๆซึ่งสามารถเดินรถได้เพียงเส้นทางเดียว ความกว้างของถนนเส้นนี้ทำให้เราสามารถถ่ายรูปได้สะดวกขึ้น บริเวณสุดถนนคุณจะพบกับทางแยกรูปตัวที (T) ซึ่งตัดกับถนนสตูล เยื้องกับคุณจะเป็นร้านบะหมี่แป๊ะเถว ซึ่งตลอดทั้งสัปดาห์ในเวลาอาหารกลางวัน ที่ร้านจะเต็มไปด้วยลูกค้าซึ่งเป็นพนักงานบริษัทจำนวนมาก ซึ่งพวกเขาได้ค้นพบว่าบะหมี่แป๊ะเถวนั้นมีรสชาติดีแค่ไหน</p>
						</article>
						
						<article class="article" id="krabi_road" data-tab-name="Krabi Road">
							<img class="thumbnail force block" src="images/attraction/phuket/krabi_road.png" />
							<h1>ถนนกระบี่</h1>
							<p>ย้อนกลับไปยังถนนเยาวราช บริเวณทางแยกที่สามารถเดินย้อนกลับไปถนนถลาง คุณจะพบกับร้านอาหารจีนแบบเปิดโล่ง ซึ่งจะจำหน่ายอาหารพื้นเมืองภูเก็ต อย่างเช่นหมูสะเต๊ะ หมูฮ้อง โดยซอยถัดไปคือซอยสุ่นอุทิศก็มีรถเข็นขายอาหารพื้นเมืองอีกหลายเจ้าด้วยเช่นกัน บริเวณนี้ถือเป็นจุดที่ได้รับความนิยมมาก</p>
							<p>เมื่อไม่มีลูกค้า บรรดาพ่อค้าแม่ค้ารถเข็นจะพูดคุยหยอกล้อกันอย่างสนุกสนาน แต่เมื่อเวลาเลิกเรียนมาถึง พวกเขาแทบไม่มีเวลาว่างเลยทีเดียว เมื่อเดินต่อไปและเลี้ยวขวา คุณจะเข้าสู่ถนนกระบี่</p>
						</article>
						
						<article class="article" id="ranong_road" data-tab-name="Ranong Road">
							<img class="thumbnail force block" src="images/attraction/phuket/ranong_road.png?ver=20160517" />
							<h1>ถนนระนอง</h1>
							<p>สุดซอยของถนนระนองให้เลี้ยวขวาและเดินขึ้นไปจนถึงทางแยกตัววาย (Y) ที่นี่คุณจะพบกับศาลเจ้าจีนที่มีสีสันสดใส ซึ่งมีชื่อว่าศาลเจ้าจุ้ยตุ่ย ศาลเจ้าแห่งนี้คือสถานที่บูชาเทพเจ้าของลัทธิเต๋าของจีนและเป็นศูนย์กลางของประเพณีถือศีลกินผักประจำปีของจังหวัดภูเก็ตอีกด้วย</p>
							<p>ศาลเจ้าแห่งนี้ยังเป็นสถานที่ที่คนนิยมมาเสี่ยงทายด้วยเซียมซีอีกด้วย</p>
							<p>ถัดจากศาลเจ้าจุ้ยตุ่ยคือศาลเจ้าปุดจ้อ ซึ่งถูกสร้างเมื่อ 200 ปีก่อน และได้ทำการบูรณะหลังจากเหตุการณ์ไฟไหม้เมื่อ 100 ปีที่แล้ว ถือว่าเป็นศาลเจ้าที่เก่าแก่ที่สุดในภูเก็ต</p>
							<p>เลี้ยวกลับไปยังเส้นทางเดิม และเดินผ่านน้ำพุที่ปลายสุดของถนนระนอง คุณจะพบว่าตัวเองได้เดินกลับมาที่ถนนรัษฏา เมื่อเดินไปยังสุดถนนสายนี้ คุณจะเห็นว่ามันคือที่ที่คุณได้เริ่มเดินสำรวจเมืองเก่าภูเก็ตนั่นเอง</p>
						</article>

                        <article class="article" id="promthep_cape" data-tab-name="Promthep Cape">
							<img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/phuket/Promthep_Cape.png" />
							<h1>แหลมพรหมเทพ</h1>
							<p>แหลมพรหมเทพ เป็นสถานที่ท่องเที่ยวแห่งหนึ่งในจังหวัดภูเก็ต ตั้งอยู่ห่างจากหาดราไวย์ประมาณ 2 กิโลเมตร เป็นแหลมที่อยู่ตอนใต้สุดของจังหวัดภูเก็ต มีทัศนียภาพที่สวยงาม และเป็นจุดชมพระอาทิตย์ตกดินที่ได้รับความนิยม เป็นที่ตั้งของประภาคารกาญจนาภิเษก สุดปลายของแหลมพรหมเทพ มีชื่อว่าแหลมเจ้า บริเวณตัวแหลมซึ่งยื่นออกไปในทะเล มีลักษณะโดดเด่นเป็นเอกลักษณ์ด้วยต้นตาลที่ขึ้นอยู่กลุ่มใหญ่ แหลมพรหมเทพ ถูกจัดเป็นหนึ่งในโครงการมหัศจรรย์เมืองไทย 12 เดือน 7 ดาว 9 ตะวัน ของการท่องเที่ยวแห่งประเทศไทย จุดเด่นคือ "ชมพระอาทิตย์ตกทะเล สวยที่สุดในประเทศไทย"</p>
						</article>

						<article class="article" id="patong_beach" data-tab-name="Patong Beach">
							<img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/phuket/patong_beach.png" />
							<h1>หาดป่าตอง</h1>
							<p>ป่าตองคือหาดที่มีชื่อเสียงบนเกาะภูเก็ต ท่านสามารถเดินทางจากรีสอร์ทของเราสู่ซอยบางลา สถานบันเทิงยามค่ำคืนได้ในไม่กี่นาที รวมถึงห้างสรรพสินค้าจังซีลอน และสถานท่องเที่ยวอื่นๆอีกมากมาย รวมไปถึงชายหาดที่งดงามอย่างหาดป่าตองด้วยเช่นกัน</p>
						</article>

						<article class="article" id="bangla_road" data-tab-name="Bangla Road">
							<img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/phuket/Bang_La_Road.png" />
							<h1>ถนนบางลา</h1>
							<p>ถนนบางลา มีชีวิตชีวาขึ้นเมื่อพระอาทิตย์ตกดิน ถนนปิดการจราจรและกลายเป็นงานเทศกาลไฟนีออนความยาว 400 เมตร ที่เต็มไปด้วยเสียงเพลงและเบียร์ราคาถูก หนาแน่นไปด้วยผู้คนในช่วงกลางคืนตลอดทั้งปีซึ่งเป็นสถานที่ที่เป็นมิตรและมีชีวิตชีวาในการเดินไปรอบ ๆ เนื่องจากมีบาร์และคลับสำหรับนักท่องเที่ยว หากคุณกำลังมองหาสถานบันเทิงยามค่ำคืน ที่ภูเก็ตถนนบางลาควรเป็นจุดเริ่มต้นของคุณ </p>
						</article>
					</div>
				</div>
				
			</div>
		</section><!--.main-content-->
		
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
            
            var targetPos = $('.site-main').offset().top;
			var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    @media (max-width: 768px) {
        .row-content [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin: 0 -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>