<?php
$title = 'Deevana Hotels & Resorts – Special Offer and Promotion';
$desc = 'Get great discount on Phuket & Krabi hotels. Book direct with official website now.';
$keyw = 'phuket hotel deal, patong hotel promotion, phuket hotel promotion, krabi hotel deal, krabi hotel promotion, krabi hotel offer';

$html_class = '';
$body_class = 'offers';
$cur_page = 'special-offers';

$lang_en = '/special-offers.php';
$lang_th = '/th/special-offers.php';
$lang_zh = '/zh/special-offers.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/special_offers/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
        
        <section class="main-content">
            <div class="container">
                
                <section class="section section-phuket">
                    <h1 class="section-title underline">All Phuket Location</h1>
                    
                    <article class="promotion">
                        <h1 class="article-title">DEEVANA PLAZA PHUKET PATONG</h1>
                        
                        <div class="row">
                            <div class="col-w4 col-thumbnail">
                                <div class="thumbnail">
                                    <img alt="EARLY BIRD &quot;SUPER DEAL&quot;" class="responsive block" src="images/special_offers/promotion-stay_longer_and_save_more.jpg" />
                                </div>
                            </div>
                            
                            <div class="col-w4 col-detail">
                                <div class="content">
                                    <h2 class="title">STAY LONGER &amp; SAVE MORE - ROOM ONLY<br>
                                    <small><span style="color: #8e3020;">72% DISCOUNT</span></small></h2>

                                    <h3 class="list-heading">You will get...</h3>
                                    <ul>
                                        <li>In-room internet</li>
                                        <li>STAY minimum 3 nights receives THB 300 Food &amp; Beverage Voucher and a set of Panna Cotta</li>
                                        <li>STAY minimum 5 nights receives THB 400 Spa Voucher and one way transfer from Hotel to Phuket Airport</li>
                                        <li>STAY minimum 7 nights receives THB 300 Food &amp; Beverage Voucher and round trip transfer from/to Phuket Airport-Hotel</li>
                                        <li>STAY minimum 10 nights receives THB 500 Food &amp; Beverage Voucher, round trip transfer from/to Phuket Airport-Hotel and Late check-out 16:00 hrs.</li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="col-w4 col-booking">
                                <div class="booking clearfix">
                                    <h3 class="title">
                                        all taxes included<br>
                                        <span class="discount">THB 2,047</span> per night<br>
                                        please stay at least <strong><span style="color: #EAC23C;">3 nights</span></strong>
                                    </h3>
                                    <ul>
                                        <li><span class="label">Room type:</span>Deluxe City View</li>
                                        <li><span class="label">Preiod:</span>Stay before 30 Jun 2016</li>
                                    </ul>
                                    <a class="right button" href="<?php ibe_url(275, 'en', 'MDc5MzU1'); ?>" target="_blank">BOOK NOW !!</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article class="promotion">
                        <h1 class="article-title">RAMADA PHUKET DEEVANA</h1>
                        
                        <div class="row">
                            <div class="col-w4 col-thumbnail">
                                <div class="thumbnail">
                                    <img alt="One Week Sale" class="responsive block" src="images/special_offers/promotion-one_week_sale.jpg" />
                                </div>
                            </div>
                            
                            <div class="col-w4 col-detail">
                                <div class="content">
                                    <h2 class="title">1 WEEK SALE - ROOM ONLY<br>
                                        <small><span style="color: #8e3020;">18% DISCOUNT</span></small>
                                    </h2>

                                    <h3 class="list-heading">You will get...</h3>
                                    <ul>
                                        <li>In-room internet</li>
                                        <li>Late check-out by 14.00</li>
                                        <li>Limited Time Offer!</li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="col-w4 col-booking">
                                <div class="booking clearfix">
                                    <h3 class="title">
                                        all taxes included<br>
                                        <span class="discount">THB 1,834</span> per night
                                    </h3>
                                    <ul>
                                        <li><span class="label">Room type:</span>Deluxe Room with Balcony</li>
                                        <li><span class="label">Preiod:</span>Stay before 30 Jun 2016</li>
                                    </ul>
                                    <a class="right button" href="<?php ibe_url(278, 'en', 'MDc5MjYw'); ?>" target="_blank">BOOK NOW !!</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article class="promotion">
                        <h1 class="article-title">DEEVANA PATONG RESORT &amp; SPA</h1>
                        
                        <div class="row">
                            <div class="col-w4 col-thumbnail">
                                <div class="thumbnail">
                                    <img alt="Book Ahead Save More" class="responsive block" src="images/special_offers/promotion-book_ahead_save_more.jpg" />
                                </div>
                            </div>
                            
                            <div class="col-w4 col-detail">
                                <div class="content">
                                    <h2 class="title">BOOK AHEAD SAVE MORE<br>
                                        <small><span style="color: #8e3020;">42% DISCOUNT</span></small>
                                    </h2>

                                    <h3 class="list-heading">You will get...</h3>
                                    <ul>
                                        <li>1 night in Superior Garden Room</li>
                                        <li>Daily breakfast</li>
                                        <li>wifi</li>
                                        <li>Free late check-out until 16:00hrs</li>
                                        <li>Free fruit plate on arrival date</li>
                                        <li>Free cash voucher 300 THB per stay</li>
                                        <li>Free one way transfer from airport to hotel minimum stay 3 nights</li>
                                        <li>Free round trip airport transfer from/to hotel minimum stay 5 nights</li>
                                        <li>Free one dinner (buffet or set dinner, exclude beverage) for minimum stay 7 nights</li>
                                        <li>Free daily drinks in room (2 local beers &amp; 2 soft drinks)</li>
                                    </ul>
                                    
                                    <p class="note"><strong>Note:</strong> Free airport transfer available time from 06:00 am to 09:00 pm. Additional charge at 350 THB per way between 09:00 pm to 06:00 am.</p>
                                </div>
                            </div>
                            
                            <div class="col-w4 col-booking">
                                <div class="booking clearfix">
                                    <h3 class="title">
                                        all taxes included<br>
                                        <span class="discount">THB 1,668</span> per night
                                    </h3>
                                    <ul>
                                        <li><span class="label">Room type:</span>Superior Garden Room</li>
                                        <li><span class="label">Period:</span>20 May - 31 Oct 2016</li>
                                    </ul>
                                    <a class="right button" href="<?php ibe_url(277, 'en', 'MDc2MTg1'); ?>" target="_blank">BOOK NOW !!</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
                
                <section class="section section-krabi">
                    <h1 class="section-title underline">All Krabi Location</h1>
                    
                    <article class="promotion">
                        <h1 class="article-title">DEEVANA PLAZA KRABI AONANG</h1>
                        
                        <div class="row">
                            <div class="col-w4 col-thumbnail">
                                <div class="thumbnail">
                                    <img alt="Super Save" class="responsive block" src="images/special_offers/promotion-super_save.jpg" />
                                </div>
                            </div>
                            
                            <div class="col-w4 col-detail">
                                <div class="content">
                                    <h2 class="title">SUPER SAVE<br>
                                        <small><span style="color: #8e3020;">15% DISCOUNT</span></small></h2>

                                    <h3 class="list-heading">You will get...</h3>
                                    <ul>
                                        <li>Internet</li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="col-w4 col-booking">
                                <div class="booking clearfix">
                                    <h3 class="title">
                                        excluding taxes<br>
                                        <span class="discount">THB 1,767</span> per night
                                    </h3>
                                    <ul>
                                        <li><span class="label">Room type:</span>Deluxe Room</li>
                                        <li><span class="label">Preiod:</span>Stay before 31 Aug 2016</li>
                                    </ul>
                                    <a class="right button" href="<?php ibe_url(276, 'en', 'MDc3NDM3'); ?>" target="_blank">BOOK NOW !!</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </section>
            </div>
        </section>
    </div>
</main>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    $(function() {
        $('#detail-select').on('click', '[data-value]', function() {
            var $this = $(this);
            var tar = $this.data('value');
            $('.tabs-group').find(tar).fadeIn(300).siblings().hide();
        });
        $('.tabs-group').find('.tab-content.default').show();
    });
</script>

<style>
    .site-main > .inner {
        padding-bottom: 60px;
        background-color: #efefef;
    }
    .main-content .section {
        margin-bottom: 80px;
    }
    .main-content .section:last-of-type {
        margin-bottom: 0;
    }
    .section-title {
        color: #000;
    }
    .promotion .note {
        border: 2px solid #9A7B12;
        padding: 5px 10px;
        font-size: smaller;
        background-color: #eee;
        border-radius: 2px;
        color: #9A7B12;
    }
</style>

<?php include_once('_footer.php'); ?>