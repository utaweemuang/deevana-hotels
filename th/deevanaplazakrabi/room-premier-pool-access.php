<?php
$title = 'Premier Pool Access | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Premier Pool Access: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'premier pool access, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-premier-pool-access';
$cur_page = 'premier-pool-access';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-premier-pool-access.php';
$lang_th = '/th/deevanaplazakrabi/room-premier-pool-access.php';
$lang_zh = '/zh/deevanaplazakrabi/room-premier-pool-access.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <!-- <img src="images/accommodations/premier-pool/1500/premier-pool-01.jpg" alt="Premier Pool Access 01" /> -->
                    <img src="images/accommodations/premier-pool/1500/premier-pool-02.jpg" alt="Premier Pool Access 02" />
                    <img src="images/accommodations/premier-pool/1500/premier-pool-03.jpg" alt="Premier Pool Access 03" />
                    <img src="images/accommodations/premier-pool/1500/premier-pool-04.jpg" alt="Premier Pool Access 04" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องพรีเมียร์ พูล แอคเซส<span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <!-- <li><img src="images/accommodations/premier-pool/600/premier-pool-01.jpg" height="50" /></li> -->
                    <li class="current"><img src="images/accommodations/premier-pool/600/premier-pool-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier-pool/600/premier-pool-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier-pool/600/premier-pool-04.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องพรีเมียร์ พูล แอคเซส</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier-pool/600/premier-pool-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>ห้องพรีเมียร์ พูล แอคเซส ขนาด 34 ตารางเมตร ผู้เข้าพักสามารถลงว่ายน้ำได้จากระเบียงส่วนตัวในห้องพัก ห้องพักประกอบด้วยเตียงขนาดคิงไซส์หรือเตียงคู่ ห้องน้ำขนาดใหญ่พร้อมด้วยอ่างอาบน้ำและอุปกรณ์อาบน้ำระดับพรีเมียร์ ห้องพักแต่ละห้องจะมีการติดตั้งทีวีแอลซีดีจอแบนพร้อมช่องสัญญาณดาวเทียมและเครื่องเล่นดีวีดี บริการเชื่อต่ออินเตอร์เน็ตไร้สาย(Wi-Fi) โดยไม่คิดค่าใช้จ่าย โทรศัพท์สายตรงพร้อมด้วยบริการฝากข้อความ อุปกรณ์สำหรับชงชาและกาแฟ ตู้เซฟ มินิบาร์ นาฬิกาปลุก กระจกสำหรับแต่งหน้า เตารีดและที่รองรีด พร้อมด้วยระเบียงแบบพิเศษที่ริมสระน้ำ ซึ่งมีเก้าอี้ตัวใหญ่สำหรับคู่รักได้พักผ่อนทั้งในเวลากลางวันและกลางคืน</p>
                            <p>จำนวนห้องพักทั้งหมด : 28 ห้อง</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                      <!--  <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 34 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Pool View</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>-->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนข้อมูล</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>