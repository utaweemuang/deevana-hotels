<?php
$title = 'Meetings and Event | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Meetings and Event: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'meetings and event, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'meetings';
$cur_page = 'meetings';

$lang_en = '/deevanaplazakrabi/meetings-and-event.php';
$lang_th = '/th/deevanaplazakrabi/meetings-and-event.php';
$lang_zh = '/zh/deevanaplazakrabi/meetings-and-event.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/meetings/meeting-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">ห้องประชุมและจัดเลี้ยง</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#hornbill_grand_ballroom" class="tab active">ห้องฮอร์นบิล แกรนด์บอลรูม</span>
                    <span data-tab="#hornbill_ballroom_1" class="tab">ห้องฮอร์นบิล บอลรูม 1</span>
                    <span data-tab="#hornbill_ballroom_2" class="tab">ห้องฮอร์นบิล บอลรูม 2</span>
                    <span data-tab="#broadbill_meeting_room" class="tab">ห้องบอร์ดบิล มีตติ้ง </span>
                    <span data-tab="#pita_biz_meeting_room" class="tab">ห้องพิตต้า บิซ มิตติ้ง</span>
                    <span data-tab="#meeting_room_dimension" class="tab">ขนาดของห้องประชุม</span>
                </div>
                
                <div class="tabs-content">
                    <article id="hornbill_grand_ballroom" class="article" data-tab-name="Hornbill Grand Ballroom">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_grand_ballroom.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องฮอร์นบิล แกรนด์บอลรูม</h1>
                                    <p>ห้องฮอร์นบิล แกรนด์บอลรูมเป็นห้องจัดเลี้ยงขนาดใหญ่ขนาด 320 ตารางเมตรของรีสอร์ท ซึ่งมีการตกแต่งอย่างร่วมสมัยแบบไทยภาคใต้ สามารถรองรับผู้เข้าร่วมงานได้หลากหลายรูปแบบ เช่นรองรับผู้เข้าร่วมงานได้ถึง 300 ที่นั่งในรูปแบบการจัดห้องประชุมแบบโรงภาพยนตร์, รองรับผู้เข้าร่วมงานได้ 300 ที่นั่งในรูปแบบจัดเลี้ยงต้อนรับแบบค็อกเทล, รองรับผู้ร่วมงานได้ 180 ที่นั่งสำหรับงานเลี้ยงใหญ่แบบเป็นทางการและรองรับผู้ร่วมงานได้ 150 ท่านสำหรับงานเลี้ยงสังสรรค์</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_1" class="article" data-tab-name="Hornbill Ballroom 1">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_1.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องฮอร์นบิล บอลรูม 1</h1>
                                    <p>ห้องฮอร์นบิล บอลรูม 1 มีขนาด 160 ตารางเมตร เหมาะสำหรับรองรับแขกจำนวนไม่เกิน 150 ที่นั่งสำหรับการจัดห้องประชุมในรูปแบบโรงภาพยนตร์, รองรับแขกได้ 150 ท่านสำหรับงานเลี้ยงต้อนรับแบบค็อกเทล, รองรับผู้ร่วมงานได้ 90 ที่นั่งสำหรับงานเลี้ยงใหญ่แบบเป็นทางการและรองรับผู้ร่วมงานได้ 70 ท่านสำหรับงานเลี้ยงสังสรรค์</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="hornbill_ballroom_2" class="article" data-tab-name="Hornbill Ballroom 2">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_hornbill_ballroom_2.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องฮอร์นบิล บอลรูม 2</h1>
                                    <p>ห้องฮอร์นบิล บอลรูม 2 มีขนาด 160 ตารางเมตร เหมาะสำหรับรองรับผู้ร่วมงานจำนวนไม่เกิน 150 ที่นั่งสำหรับการจัดห้องประชุมในรูปแบบโรงภาพยนตร์, รองรับผู้ร่วมงานได้ 50 ท่านสำหรับงานเลี้ยงต้อนรับแบบค็อกเทล, รองรับผู้ร่วมงานได้ 90 ที่นั่งสำหรับงานเลี้ยงใหญ่แบบเป็นทางการและรองรับผู้ร่วมงานได้ 70 ท่านสำหรับงานเลี้ยงสังสรรค์</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="broadbill_meeting_room" class="article" data-tab-name="Broadbill Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_broadbill_meeting_room.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องบอร์ดบิล มีตติ้ง </h1>
                                    <p>ห้องบอร์ดบิล มีตติ้ง  มีขนาด 56 ตารางเมตร สามารถรองรับผู้ร่วมงานได้ถึง 40 ที่นั่งสำหรับการจัดห้องประชุมในรูปแบบโรงภาพยนตร์,  รองรับผู้ร่วมงานได้ 50 ท่านสำหรับงานเลี้ยงต้อนรับแบบค็อกเทล, รองรับผู้ร่วมงานได้ 30 ที่นั่งสำหรับงานเลี้ยงใหญ่แบบเป็นทางการและรองรับผู้ร่วมงานได้ 35 ท่านสำหรับงานเลี้ยงสังสรรค์</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pita_biz_meeting_room" class="article" data-tab-name="Pita Biz Meeting Room">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/meetings/meeting_pitta_biz.jpg" />
                                </div>                            
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องพิตต้า บิซ มิตติ้ง</h1>
                                    <p>ห้องประชุมพิตต้า บิซ มิตติ้งของเราเหมาะสำหรับการจัดประชุมผู้บริหาร รองรับผู้ร่วมงานได้ถึง 15 ท่าน และห้องประชุมยังมีไฟแบบเดย์ไลท์ให้บริการซึ่งจะทำให้ผู้เข้าร่วมประชุมเกิดความผ่อนคลาย ส่งผลให้การประชุมของท่านเป็นไปอย่างราบรื่น สำหรับงานเลี้ยงสังสรรค์</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="meeting_room_dimension" class="article" data-tab-name="Meeting Room Dimension">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w12 col-cap">
                                    <h1 class="title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ขนาดของห้องประชุม</h1>
                                    <table class="responsive-table">
                                        <thead>
                                            <tr>
                                                <th>ห้องประชุม</th>
                                                <th>พื้นที่ (sqm)</th>
                                                <th>ความสูง (m)</th>
                                                <th data-hide="phone, tablet">Table U-Shape</th>
                                                <th data-hide="phone, tablet">Style Rectangle</th>
                                                <th data-hide="phone, tablet">Style Theatre</th>
                                                <th data-hide="phone, tablet">Style Classroom</th>
                                                <th data-hide="phone, tablet">Banquet</th>
                                                <th data-hide="phone, tablet">Dinner &amp; Dance</th>
                                                <th data-hide="phone, tablet">Cocktail</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <td>ห้องฮอร์นบิล แกรนด์บอลรูม</td>
                                                <td>320</td>
                                                <td>3.2</td>
                                                <td>50</td>
                                                <td>50</td>
                                                <td>300</td>
                                                <td>180</td>
                                                <td>200</td>
                                                <td>150</td>
                                                <td>300</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>ห้องฮอร์นบิล บอลรูม 1</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>ห้องฮอร์นบิล บอลรูม 2</td>
                                                <td>160</td>
                                                <td>3.2</td>
                                                <td>25</td>
                                                <td>25</td>
                                                <td>150</td>
                                                <td>72</td>
                                                <td>90</td>
                                                <td>70</td>
                                                <td>150</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>ห้องบอร์ดบิล มีตติ้ง</td>
                                                <td>56</td>
                                                <td>2.8</td>
                                                <td>15</td>
                                                <td>-</td>
                                                <td>40</td>
                                                <td>24</td>
                                                <td>30</td>
                                                <td>-</td>
                                                <td>35</td>
                                            </tr>
                                            
                                            <tr>
                                                <td>ห้องพิตต้า บิซ มิตติ้ง</td>
                                                <td>24</td>
                                                <td>2.8</td>
                                                <td>12</td>
                                                <td>-</td>
                                                <td>18</td>
                                                <td>-</td>
                                                <td>20</td>
                                                <td>-</td>
                                                <td>20</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
        
        if( hash ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            $('.responsive-table').trigger('footable_resize');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
                
                $('.responsive-table').trigger('footable_resize');
            });
        });

        $('.accordion-tab').eq(indx).addClass('active');
        
        $('.responsive-table').footable({
            breakpoints: {
                phone: 480,
                tablet: 768,
            }
        });
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    #pita_biz_meeting_room {
        text-align: center;
    }
    .responsive-table {
        max-width: 1040px;
    }
    .responsive-table .footable-row-detail {
        text-align: left;
    }
    @media (max-width: 860px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>