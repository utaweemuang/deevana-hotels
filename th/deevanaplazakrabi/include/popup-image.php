<script>
    ;(function($) {
        var popup = {
            enabled: true,
            image: {
                src: 'http://www.deevanahotels.com/deevanaplazakrabi/images/banner/specialsave50.jpg',
            },
            link: {
                enabled: true,
                href: 'https://reservation.travelanium.net/hotelpage/rates/?propertyId=276&onlineId=5&pid=MDczMTA4NA%3D%3D&lang=th',
                target: '_blank',
            }
        }
        if (popup.enabled===true) {
            $.magnificPopup.open({
                items: {
                    type: 'image',
                    src: popup.image.src,
                },
                mainClass: 'mfp-fade',
                removalDelay: 150,
                callbacks: {
                    open: function() {
                        if (popup.link.enabled===true) {
                            $(this.content).find('.mfp-img').wrap('<a href="'+popup.link.href+'" target="'+popup.link.target+'" rel="noopener" />');
                        }
                    },
                },
            });
        }
    })(jQuery);
</script>