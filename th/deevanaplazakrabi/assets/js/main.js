(function($) {
    $(function() {
        
        /* =Simple Toggle
        --------------------------------------------------------------------- */
        $.fn.simpleToggle = function(activeClass, target, outFocus) {
            var $this = $(this);
            var $target = $(target);
            var aClass = activeClass;
            
            $this.on('click', function() {
                if( $this.hasClass(aClass) ) {
                    $this.removeClass(aClass);
                    $target.removeClass(aClass);
                } else {
                    $this.addClass(aClass);
                    $target.addClass(aClass);
                }
            });
            
            if( outFocus === true ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass(aClass);
                    }
                });
            }
        }
        
        /* =Swipe Menu
        ------------------------------------------------------------ */
        function swipeMenu( breakpoint ) {
            function openOffsideMenu() {
                $('html').addClass('open-offside-menu');1
            }

            function closeOffsideMenu() {
                $('html').removeClass('open-offside-menu');
            }

            $('#toggle_offside_menu').on('click', function() {
                openOffsideMenu();
            });
            
            $('.offside-menu-bg, #offside-menu .close-menu').on('click', function() {
                closeOffsideMenu();
            });
            
            var breakpoint = breakpoint || 0;
            
            $(window).on('resize', $.throttle( '200', function() {
                if( $(window).width() < breakpoint ) {
                    $(document).swipe('enable');
                    $(document).swipe({
                        swipeRight: function(event) {
                            var targ = event.target;
                            if( $(targ).closest('.owl-carousel, .disable-touch').length === 0 ) {
                                openOffsideMenu();
                            }
                        },
                        swipeLeft: function() {
                            closeOffsideMenu();
                        },
                    });
                } else {
                    $(document).swipe('disable');
                    closeOffsideMenu();
                }
            })).trigger('resize');
        }
        
        swipeMenu(860);
        
        /* =Room Scripts
        ---------------------------------------------------------------------- */
        
        if( $('body').hasClass('room') ) {
            var $slide = $('.room-slides');
            var $tnav = $('.room-slides-thumbs .thumbs');

            function slideFX( autoplay ) {
                var items = $slide.find('img').length;
                var autoplay = ( items > 1 ) ? autoplay : 0;
                $slide.superslides({
                    inherit_width_from: '.room-slides-wrap',
                    inherit_height_from: '.room-slides-wrap',
                    play: autoplay,
                    pagination: false,
                    animation: 'fade',
                });

                $slide.on('animated.slides', function() {
                    var i = $slide.superslides('current');
                    $tnav.find('li').eq(i).addClass('current').siblings().removeClass('current');;
                });

                $tnav.on('click', 'li', function() {
                    var $thm = $(this);
                    var i = $thm.index();
                    if( ! $thm.hasClass('current') ) {
                        $thm.addClass('current').siblings().removeClass('current');
                        $slide.superslides('animate', i);
                    }
                });
                
                $slide.swipe({
                    swipeRight: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('prev');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                    swipeLeft: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('next');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                });
            }

            slideFX(8000);

            function hideRoomContent() {
                $('html').addClass('hide-content');
            }
            
            function showRoomContent() {
                $('html').removeClass('hide-content');
            }
            
            $('#hide_content').on('click', function() {
                hideRoomContent();
            });

            $('#toggle_content').on('click', function() {
                $('html').toggleClass('hide-content');
            });

            function roomContentPos( breakpoint, offset ) {
                var breakpoint = breakpoint || 0;
                var $rcon = $('.room-info');
                var rconHi = $rcon.outerHeight();
                var rconWi = $rcon.outerWidth();
                var offset = offset || 0;

                if( $(window).width() > breakpoint ) {
                    $rcon.css({
                        marginLeft: -rconWi/2,
                        marginTop: -(rconHi/2) + offset,
                    });
                } else {
                    $rcon.removeAttr('style');
                }
            }

            function roomScript( breakpoint ) {
                var breakpoint = breakpoint || 0;
                $(window).on('resize', $.throttle( 100, function() {
                    var winWi = $(window).width();
                    if( winWi > breakpoint ) {
                        $('html').addClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                    } else {
                        $('html').removeClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                        showRoomContent();
                    }
                })).trigger('resize');
            }

            roomScript(960);
            
            // Room booking
            $('#room-booking-form').booking({
                checkInSelector: '#checkin',
                checkOutSelector: '#checkout',
                adultSelector: '#adults',
                childSelector: '#children',
                roomSelector: '#rooms',
                codeSelector: '#accesscode',
                submitSelector: '#submit',
                propertyId: 276,
				onlineId: 5,
                language: 'th',
            });

            // Amenities Popup
            $('.more').magnificPopup({
                delegate: 'a',
                type: 'inline',
                mainClass: 'mfp-fade',
                removalDelay: 300,
            });
        }
        
        /* =Validation
        ---------------------------------------------------------------------- */
        
        $.validate({
            form: '#contact_form',
            modules: 'location, html5',
            onModulesLoaded: function() {
                $('input[name="country"]').suggestCountry();
            },

            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                }).done(function(status) {
                   if (status === 'ok') {
                       //console.log('Sending Successful');

                       $(form).find(':focus').blur();
                       $(form)[0].reset();
                       grecaptcha.reset();
                       
                       //Display form result
                       $(form).find('#contact_result').addClass('success');
                       $('#contact_result').html('<i class="icon fa fa-check-circle"></i> Thank you for getting in touch!');
                       $('#contact_result').delay(200).slideDown(200);
                   } else if( status === 'not' ) {
                       //console.log('Sending Failed because Google Recaptcha');
                       
                       $(form).find('.g-recaptcha-error').text('Please check the recaptcha');
                   } else {
                       console.log('Unknowed error');
                       //$(form).addClass('form-error');
                       //$(form).find('#form_result').html('Sorry, There is something error, Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a>').slideDown(200);
                   }
                });

                return false;
            },
        });

        $.validate({
            form: '#newsletter-form',
            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.post(url, formData, function( data ) {
                    if( data == '' ) {
                        $(form)[0].reset();
                        $(form).find(':focus').blur();
                        $(form).addClass('completed').after('<div class="subscribe-form-result" />');
                        $(form).next('.subscribe-form-result').html('<i class="icon fa fa-check-circle"></i> Subscription Successful');
                    } else {
                        $(form).after('<div class="form-error-log"><span style="color: tomato;">Subscription Failed.</span><br/>Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a></div>');
                    }
                });
                return false;
            }
        });
        
        /**
         * Scroll FX
         */
        var $header = $('.site-header');
        var $bookingBar = $('.booking-bar');
        $(window).on('load scroll resize', $.throttle(16, function() {
            var winY = window.scrollY;
            var winW = window.innerWidth;
            if (winY >= 150 && winW >= 1200) {
                $header.addClass('mini');
            } else {
                $header.removeClass('mini');
            }

            if ($bookingBar.length) {
                var trigger = $bookingBar.offset().top - 60;
                if (winY >= trigger && winW >= 1200) {
                    $bookingBar.addClass('fixed');
                } else {
                    $bookingBar.removeClass('fixed');
                }
            }
        }));
        
        var $heroSlider = $('.hero-slider');
        var $loop = ( $heroSlider.find('.item').length > 1 ) ? true : false;
        $('.hero-slider').owlCarousel({
            items: 1,
            loop: $loop,
            smartSpeed: 800,
            pullDrag: false,
            dots: false,
            nav: false,
            navContainer: '.custom-hero-slide-nav',
            navText: ['<span class="sprite arrow-left"></span>', '<span class="sprite arrow-right"></span>'],
        });
        
        //Switch languages function
        var $langSelector = $('#language_select');
        var defaultLang = $('#language_select').find('.option.selected a').html();
        $langSelector.simpleToggle('visible', null, true);
        $langSelector.find('.active').html(defaultLang);
        
        $('.toggle-sub-menu').each(function() {
            var $this = $(this);
            
            $this.children('a').on('click', function(e) {
                e.preventDefault();
                if( $this.hasClass('visible') ) {
                    $this.removeClass('visible');
                } else {
                    $this.addClass('visible');
                }
            });
            
            //Run script in .site-navigation only
            if( $this.closest('.site-navigation').length === 1 ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass('visible');
                    }
                });
            }
        });
        
        var $currentMenu = $('#offside-menu').find('.current');
        if( $currentMenu.closest('.ancestor').length > 0 ) {
            $currentMenu.parents('.ancestor').addClass('visible');
        }

        /**
         * Booking
         */
        $('#booking-form').booking({
            checkInSelector: '#checkin',
            checkOutSelector: '#checkout',
            adultSelector: '#adults',
            childSelector: '#children',
            roomSelector: '#rooms',
            codeSelector: '#accesscode',
            propertyId: 276,
            onlineId: 5,
            language: 'th',
        });

        /**
         * Side Panel
         */
        $('[data-side-panel]').on('click', function(event) {
            event.preventDefault();
            var trigger = this.dataset.sidePanel;
            var methods = {
                show: function() {
                    $('body').addClass('side-panel-show');
                },
                hide: function() {
                    $('body').removeClass('side-panel-show');
                },
                toggle: function() {
                    if (this.state()) {
                        this.hide();
                    } else {
                        this.show();
                    }
                },
                state: function() {
                    var state = false;
                    if ($('body').hasClass('side-panel-show'))
                        state = true;
                    return state;
                }
            }
            methods[trigger]();
        });

        $(window).on('resize', $.debounce(250, function() {
            var winWidth = window.innerWidth;
            if (winWidth >= 768)
                $('body').removeClass('side-panel-show');
        }));

        $('.side-panel-drag').swipe({
            swipeRight: function() {
                $('body').addClass('side-panel-show');
            },
        });

        $('.side-panel-bg').swipe({
            swipeLeft: function() {
                $('body').removeClass('side-panel-show');
            }
        });

        /**
         * Member Panel
         */
        var $m_display  = $('.member-display');
        var $m_btn      = $('.member-panel-trigger');
        var $m_panel    = $('.member-panel');
        var $m_form     = $('#form_member');
        var $m_close    = $m_panel.find('.btn-close');

        var memb  = $('#member');
        var mcont = $('.member-content');
        var mtrig = $('.member-content-trigger');
        var mclos = memb.find('.btn-close');

        $m_btn.on('click', function(e) {
            e.preventDefault();
            if (memberPanel()) {
                memberPanel('hide');
            } else {
                memberPanel('show');
            }
        });

        $m_close.on('click keypress', function(e) {
            e.preventDefault();
            memberPanel('hide');
            if (e.type == 'keypress' && e.keyCode == 32 || e.keyCode == 13)
                memberPanel('hide');
        });

        function memberPanel(methods) {
            switch (methods) {
                case 'show':
                    $m_btn.addClass('show');
                    $m_panel.slideDown(350, function() {
                        $m_panel.find('input[type="email"]').focus();
                    });
                    break;
                case 'hide':
                    $m_btn.removeClass('show');
                    $m_panel.slideUp(350);
                    break;
                default:
                    return $m_btn.hasClass('show');
            }
        }

        /**
         * Member Form
         */
        var memberCode = 'member';

        if (memberChecker()) {
            memberDisplay('member');
            $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
        } else {
            memberDisplay('guest');
        }

        $m_form.on('submit', function(event) {
            event.preventDefault();
            var email = $(this).find('[type="email"]').val();
            memberCreateCookies({
                code: 'member',
                expire: 7,
                user: {
                    email: email,
                }
            });
            var url = this.baseURI;
            memberSendData({
                formAPI: '648466c4ac12000257e768ffedac2e73',
                formType: 2,
                customer: {
                    email: email,
                    message: 'This information send from Deevana Plaza Krabi Aonang (Group) by Member form',
                }
            });

            memberPanel('hide');
            memberDisplay('member');
            window.open( 'https://reservation.travelanium.net/propertyibe2/?propertyId=276&onlineId=5&lang=th&pgroup=QQMPUPYV&accesscode='+memberCode, '_blank' );

            memberAppendCode(memberCode);
            $('#booking-form, #room-booking-form').booking('update', 'secretCode', memberCode);
        });

        function memberDisplay(methods) {
            switch (methods) {
                case 'guest':
                    $m_display
                        .removeClass('is-memmber')
                        .addClass('is-guest');
                    break;
                case 'member':
                    $m_display
                        .removeClass('is-guest')
                        .addClass('is-member');
                    break;
            }
        }
    });
})(jQuery);