<?php
$title = 'Premier Room | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Premier Room: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'premier room, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'room room-premier-room';
$cur_page = 'premier-room';
$par_page = 'rooms';

$lang_en = '/deevanaplazakrabi/room-premier-room.php';
$lang_th = '/th/deevanaplazakrabi/room-premier-room.php';
$lang_zh = '/zh/deevanaplazakrabi/room-premier-room.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/premier/1500/premier-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/premier/1500/premier-02.jpg" alt="Deluxe Room 02" />
                    <img src="images/accommodations/premier/1500/premier-03.jpg" alt="Deluxe Room 03" />
                    <img src="images/accommodations/premier/1500/premier-04.jpg" alt="Deluxe Room 04" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">ห้องพรีเมียร์<span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/premier/600/premier-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/premier/600/premier-04.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องพรีเมียร์</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/premier/600/premier-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="#">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w9 col-info">
                            <p>ห้องพรีเมียร์ที่สวยงามของเราได้รับการออกแบบอย่างพิถีพิถัน ผู้เข้าพักจะเพลิดเพลินกับการตกแต่งแบบร่วมสมัย และเตียงขนาดคิงไซส์หรือเตียงคู่ซึ่งเหมาะอย่างยิ่งสำหรับการพักผ่อนในห้องพัก ห้องพักสว่างโปร่งสบายพร้อมด้วยห้องน้ำที่กวางขวางและอ่างอาบน้ำ สิ่งอำนวยความเพิ่มเติมได้แก่ ทีวีแอลซีดีจอแบนพร้อมช่องสัญญาณดาวเทียม และเครืองเล่นดีวีดี, โทรศัพท์สายตรงและบริการรับฝากข้อความ, บริการเชื่อมต่ออินเตอร์เน็ตไร้สาย (Wi-Fi) โดยไม่คิดค่าใช้จ่าย อุปกรณ์สำหรับชงชาและกาแฟ มินิบาร์ ตู้เซฟ เตารีดและที่รองรีด กระจกสำหรับแต่งหน้า ห้องพักยังตกแต่งให้มีระเบียงส่วนตัวด้วยเช่นกัน</p>
                            <p>จำนวนห้องพักทั้งหมด : 55 ห้อง</p>
                            
                            <a class="button clickable book-this-room-button mobile" href="#">จองห้องพัก</a>
                        </div>
                        
                    <!--    <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">Room Features</h2>
                                    <ul class="amenities-list">
                                        <li>Room size 33 Sq.m. including balcony / terrace</li>
                                        <li>King size bed with mattress topper (Twin bed on request)</li>
                                        <li>Pool view</li>
                                        <li>Day bed</li>
                                        <li>Air conditioning</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> more</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>Example List 1</li>
                                            <li>Example List 2</li>
                                            <li>Example List 3</li>
                                            <li>Example List 4</li>
                                            <li>Example List 5</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div> -->
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนข้อมูล</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>