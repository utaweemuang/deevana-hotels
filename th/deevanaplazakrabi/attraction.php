<?php
$title = 'Attractions | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Attractions: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'attractions, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'attraction';
$cur_page = 'attraction';

$lang_en = '/deevanaplazakrabi/attraction.php';
$lang_th = '/th/deevanaplazakrabi/attraction.php';
$lang_zh = '/zh/deevanaplazakrabi/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#emerald_pool">สระมรกต</li>
                            <!-- <li class="tab" data-tab="#elephant_ride">ขี่ช้าง</li> -->
                            <li class="tab" data-tab="#hong_island">เกาะห้อง</li>
                            <li class="tab" data-tab="#tiger_cave_temple">วัดถ้ำเสือ</li>
                            <li class="tab" data-tab="#phi_phi_island">เกาะพีพี</li>
							<li class="tab" data-tab="#thalay_wak">ทะเลแหวก</li>
                            <li class="tab" data-tab="#nong_thale_canel">คลองหนองทะเล</li>
							<li class="tab" data-tab="#koh_kai">เกาะไก่</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="emerald_pool" data-tab-name="Emerald Pool">
                        <img class="thumbnail force" src="images/attraction/emerald_pool.png" alt="Emerald Pool" />
                        <h1 class="title">สระมรกต</h1>
                        <p>สระมรกตคือสระน้ำที่เกิดจากธรรมชาติที่สวยงามอย่างแท้จริง เป็นสระน้ำสวยใสส่องประกายซึ่งซุกซ่อนอยู่กลางใจป่า สระมรกตตั้งอยู่ในเขตรักษาพันธุ์สัตว์ป่าเขาประ-บางคราม ซึ่งคนท้องถิ่นจะเรียกว่าป่าเขานอจู้จี้ ในเขตบ้านบางเตียว เดินทางจากที่ว่าการอำเภอคลองท่อมไปตามทางหลวงหมายเลขที่ 4038 ไปอีกประมาณ 18 กิโลเมตร</p>
                        <p>เมื่อผู้มาเยือนมาถึง ก็มักจะอดใจไม่ได้ที่กระโดดลงไปในสระน้ำใสสีเขียวมรกตและสนุกสนานกับการว่ายน้ำให้สดชื่น</p>
                        <p>สระมรกตมีความลึกประมาณ 1-2 เมตร และมีขนาดเส้นผ่านศูนย์กลางประมาณ 20-25 เมตร สีของน้ำในสระจะเปลี่ยนเป็นสีเขียวอ่อนหรือสีเขียวมรกตนั้นขึ้นอยู่กับการสะท้อนของแสงแดดในวันนั้นๆ</p>
                        <p>น้ำในสระมรกตนั้นมาจากบ่อน้ำซึ่งรู้จักกันดีในชื่อ "บ่อน้ำผุด" ซึ่งตั้งอยู่ห่างจากเนินเขาไปประมาณ 600 เมตร น้ำด่างจากกำมะถันที่ผุดขึ้นมาจากบ่อน้ำผุดนี้ไหลผ่านรอยแยกของชั้นหินไปยังสระมรกต น้ำที่ใสอยู่เสมอนั้นเพราะในน้ำมีแคลเซี่ยมคาร์บอเนตผสมอยู่มาก ทำให้สิ่งที่ปะปนอยู่ในน้ำตกตะกอนได้เร็วขึ้น และทำให้ตะไคร่น้ำไม่สามารถที่จะเจริญเติบโตในบ่อน้ำแห่งนี้ได้</p>
                        <p>กระท่อมสไตล์บาหลีได้ถูกสร้างขึ้นเมื่อปีที่ผ่านมาและถูกสงวนไว้ให้สำหรับพระบรมวงศานุวงศ์เท่านั้น</p>
                    </article>
                    
                    <!-- <article class="article" id="elephant_ride" data-tab-name="Elephant Ride">
                        <img class="thumbnail force" src="images/attraction/elephant_ride.png" alt="Elephant Ride" />
                        <h1 class="title">ขี่ช้าง</h1>
                        <p>ช้างคือสัญลักษณ์ของชาติและถือว่าเป็นของขลังอย่างหนึ่งสำหรับคนไทย ด้วยผิวสีเทาที่ยับย่น และลำตัวที่ไหวเอนไปมา ช้างจึงเป็นการผสมผสานที่งดงามของกำลังที่ดุร้าย ความอ่อนโยนและความกระฉับกระเฉง ซึ่งทำให้เรารู้สึกทั้งเคารพและชื่นชอบไปพร้อมกัน</p>
                        <p>เนื่องจากช้างได้หายไปจากป่าในประเทศไทยอย่างรวดเร็ว ดังนั้นสถานที่ที่ดีที่สุดที่จะได้พบเห็นสิ่งมีชีวิตที่น่างดงามนี้คือบริเวณค่ายสำหรับเดินป่า ซึ่งมีเพียงไม่กี่แห่งในจังหวัดกระบี่ โดยสถานประกอบการดังกล่าวจะต้องเป็นไปตามหลักเกณฑ์ที่เข้มงวดที่กำหนดโดยกรมปศุสัตว์แห่งชาติเกี่ยวกับการให้อาหาร, น้ำ, และที่อยู่อาศัยสำหรับช้างที่เหมาะสม และเช่นเดียวกับการให้การดูแลสุขภาพที่เหมาะสม</p>
                        <p>โดยการเดินทางอย่างช้าๆบนหลังช้างนี้จะทำให้คุณได้สัมผัสกับสภาพแวดล้อมของสัตว์ป่าทางธรรมชาติโดยตรง การให้นักท่องเที่ยวขี่ช้างเพื่อความสนุกสนานอาจจะหมายถึงการทำให้ช้างได้ถูกอนุรักษ์ไว้ ในระหว่างที่การอยู่อย่างอิสระก็อาจจะเป็นไปได้ แต่เนื่องจากไม่มีสถานที่ที่เพียงพอที่จะปล่อยช้างเหล่านี้สู่ป่า และต้นทุนสำหรับการเลี้ยงดูที่ค่อนข้างสูง (โดยช้างจะบริโภคอาหารราว 200-300 กิโลกรัมต่อวัน) ทางเลือกเดียวสำหรับยักษ์ใหญ่ใจดีนี้คือการร้องขออาหาร เข้าร่วมกับการลักลอบตัดไม้ หรืออยู่เฉยๆในสวนสัตว์</p>
                        <p>ทัวร์เดินป่า - โดยปกติจะมีการนั่งช้างประมาณหนึ่งชั่วโมง, รวมไปถึงกิจกรรมเที่ยวชมสถานที่ต่างๆ สามารถจองได้ด้านล่างนี้ หรือจองผ่านตัวแทนท้องถิ่น คุณยังสามารถไปเยี่ยมชมค่ายเหล่านี้ด้วยตัวเองเพื่อเดินป่า หรือเพียงแค่คอยดูและถ่ายรูปช้าง โดยไม่จำเป็นต้องขี่ช้าง</p>
                    </article> -->
                    
                    <article class="article" id="hong_island" data-tab-name="Hong Island">
                        <img class="thumbnail force" src="images/attraction/hong_island.png" alt="Hong Island" />
                        <h1 class="title">เกาะห้อง</h1>
                        <p>ตั้งอยู่ในเขตอุทยานแห่งชาติธารโบกขรณี คุณสามารถเดินทางไปยังเกาะอันสวยงามทั้งสี่เกาะของหมู่เกาะห้องได้โดยใช้บริการเรือเร็วจากอ่าวนาง โดยใช้เวลาเดินทางเพียง 20 นาที หรืออาจจะเดินทางโดยเรือหางยาว ซึ่งใช้เวลาประมาณ 45 นาที ตัวเกาะซึ่งตั้งอยู่เป็นฉากหลังที่สวยงามของอ่าวพังงา รวมเข้ากับแนวหิวนับไม่ถ้วนซึ่งโผล่ขึ้นมา ทำให้หมู่เกาะแห่งนี้นับได้ว่าเป็นหมู่เกาะที่สวยงามที่สุดในจังหวัดกระบี่</p>
                    </article>
                    
                    <article class="article" id="tiger_cave_temple" data-tab-name="Tiger Cave Temple">
                        <img class="thumbnail force" src="images/attraction/tiger_cave_temple.png" alt="Tiger Cave Temple" />
                        <h1 class="title">วัดถ้ำเสือ</h1>
                        <p>เสือได้หายสาบสูญไปจากสถานที่แห่งนี้นานแล้ว ภายในถ้ำเหลือทิ้งไว้เพียงร่องรอยของอุ้งเท้ามากมาย ซึ่งขณะนี้มีเพียงศาลเจ้าซึ่งมีรูปปั้นเสือและพระพุทธรูปจำนวนมาก กระโหลกศรีษะปลาวาฬที่วางอย่างแปลกๆ และโครงกระดูกมนุษย์ซึ่งนำมาใช้สำหรับใคร่ครวญถึงความไม่เที่ยงของชีวิต ทำให้วัดถ้ำเสือแห่งนี้มีความน่าสนใจมากขึ้น สถานที่แห่งนี้ยังได้รับอิทธิพลทางศาสนาจากจีน ภายในวัดมีเจดีย์ที่สูงตระหง่านซึ่งประดิษฐานรูปปั้นเจ้าแม่กวนอิมขนาดใหญ่ ซึ่งเป็นเทพธิดาแห่งความเมตตาในลัทธิมหายาน</p>
                        <p>จุดเด่นของวัดคือพระพุทธรูปปางสมาธิและเจดีย์ทองคำขนาดใหญ่ที่ตั้งอยู่บนยอดหน้าผา ทิวทัศน์ที่มองเห็นจากที่นี่สวยงามจนแทบลืมหายใจเลยทีเดียว - คุณสามารถมองเห็นเขาพนมเบญจารางๆไปทางทิศเหนือ, หน้าผาหินปูนที่สูงชันนี้ตัดขาดไร่เลย์ออกจากแผ่นดินใหญ่ไปทางตะวันตกและปากแม่น้ำกระบี่ซึ่งไหลลงสู่ทะเลลงไปทางทิศใต้ ในวันที่อากาศปลอดโปร่งคุณจะสามารถมองเห็นตลอดทั้งหมดไปจนถึงเกาะพีพี</p>
                    </article>

                    <article class="article" id="phi_phi_island" data-tab-name="Phi Phi Island">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/phi_phi_island.png" />
                        <h1>เกาะพีพี</h1>
                        <p>เกาะพีพีเป็นดาวเด่นของประเทศไทย เกาะแห่งนี้เคยถูกใช้เป็นสถานที่ถ่ายทำภาพยนตร์ และมักถูกพูดถึงโดยนักท่องเที่ยวทั่วประเทศไทย สำหรับนักท่องเที่ยวบางกลุ่ม เหตุผลที่พวกเขาจำเป็นต้องบินมายังภูเก็ตก็เพียงเพื่อผ่านไปสู่เกาะแห่งนี้ ถึงแม้จะมีการโฆษณาเกินจริงบ้าง แต่มันก็ไม่น่าผิดหวังเลย เกาะพีพีมีความงดงามและน่าดึงดูดใจ คุณสามารถเดินทางมายังเกาะแห่งนี้โดยทางเรือ ตัวเกาะมองดูเหมือนว่ามันลอยขึ้นมาจากทะเล มีลักษณะคล้ายป้อมปราการ มีหอหน้าผาสูงชันอยู่เหนือศรีษะ ซึ่งมีเส้นทางไปสู่ป่าบริเวณหน้าชายหาด เกาะแห่งนี้อาจทำให้คุณรู้สึกเหมือนรักแรกพบเลยทีเดียว</p>
                        <p>อีกเหตุผลที่ทำให้เราหลงรักเกาะแห่งนี้ นั่นคือเกาะแห่งนี้เป็นหนึ่งในไม่กี่ที่ในโลกที่เราจะสามารถเอนตัวลงพักผ่อนได้อย่างสบายใจ เกาะพีพีมีสองส่วนด้วยกัน ส่วนแรกของเกาะนั้นไม่มีคนอาศัยอยู่ (พีพีเล) และอีกส่วนของเกาะนั้นไม่มีถนน (พีพีดอน) จีงไม่มีตาราง ไม่มีความเร่งรีบ จึงไม่มีเหตุผลใดที่ต้องเร่งรีบ</p>
                    </article>

                    <article class="article" id="thalay_wak" data-tab-name="Thalay Wak">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/talay-wak.png" />
                        <h1>ทะเลแหวก</h1>
                        <p>ทะเลแหวกเป็นสถานที่ท่องเที่ยวทางธรรมชาติที่โด่งดังของ จ.กระบี่ ที่ถูกขนานนามให้เป็น Unseen Thailand อันเนื่องมาจากความมหัศจรรย์ของธรรมชาติ ยามระดับน้ำลด เผยให้เห็นส่วนของสันทรายเชื่อมต่อกันถึง ๓ เกาะ ได้แก่ เกาะไก่ เกาะทับ เกาะหม้อ</p>
                    </article>

                    <article class="article" id="nong_thale_canel" data-tab-name="Nong Thale Canal">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/Klongnongtale.png" />
                        <h1>คลองหนองทะเล</h1>
                        <p>คลองหนองทะเล ตั้งอยู่ที่เขตรอยต่อระหว่าง ม.1 กับ ม.4 ตำบลหนองทะเล อำเภอเมือง จังหวัดกระบี่ ในอดีตคลองแห่งนี้เป็นคลองน้ำจืดสายเล็กๆ ที่มีลักษณะคล้ายๆ กับป่าพรุ เต็มไปด้วยเหล่าต้นไม้น้อยใหญ่ ต่อมาเมื่อราว 10 ปีก่อน ได้มีก่อสร้างฝายน้ำล้นที่บริเวณกลางลำคลอง เพื่อชะลอน้ำไว้ใช้ในช่วงหน้าแล้ง และนำน้ำมาผลิตน้ำประปาแก้ปัญหาความเดือดร้อนของประชาชน ส่งผลให้ด้านบนของฝาย จากลำคลองเล็กๆ และป่าพรุ ก็กลายเป็นแอ่งน้ำขนาดใหญ่มีเนื้อที่ประมาณ 100 ไร่ ความลึกประมาณ 5 เมตร กว้างประมาณ 200 เมตร จนชาาวบ้านพูดติดปากกันว่า “คลองหนองทะเล”</p>
                        <p>ปัจจุบัน “คลองหนองทะเล” หรือ “คลองหรูด” ได้กลายเป็นแหล่งท่องเที่ยวเชิงนิเวศแห่งใหม่ของจ.กระบี่ โดยองค์การบริหารส่วนตำบลหนองทะเล ร่วมกับกำนัน ผู้ใหญ่บ้าน และชาวบ้านช่วยกันพัฒนาตกแต่งเส้นทางเพื่อรองรับนักท่องเที่ยวที่ชื่นชอบการเที่ยวเชิงนิเวศ โดยการพายเรือคายัค (เรือแคนู)</p>
                        <p>ขึ้นไปชมต้นน้ำของคลองหนองทะเล ระหว่างเส้นทาง ได้เที่ยวชมโขดหินน้อยใหญ่ คล้ายกับภูเขาเล็กๆ โผล่พ้นผิวน้ำสลับเรียงราย กับตอไม้</p>
                    </article>

                    <article class="article" id="koh_kai" data-tab-name="Koh Kai">
                        <img class="thumbnail force block" src="http://www.deevanahotels.com/images/attraction/krabi/koh-kai.png" />
                        <h1>เกาะไก่</h1>
                        <p>เกาะไก่ อยู่ในทะเลกระบี่หน้าอ่าวนาง ห่างจากฝั่งประมาณ 8 กิโลเมตร อยู่ใกล้ๆ กับเกาะปอดะห่างไปทางทิศใต้เล็กน้อย เกาะไก่เป็นหนึ่งในสามเกาะที่ทำให้เกิดสันทรายที่เราเรียกว่า "ทะเลแหวก " สันหทรายด้านทิศตะวันตกที่มีสันฐานมาจากเกาะใหญ่นั่นคือเกาะไก่</p>
                        <p>ที่เรียกว่าเกาะไกก็เพราะว่าทางด้านปลายสุดของเกาะมีหินแหลมๆ เมื่อมองขึ้นไปแล้วคล้ายคอไก่ บ้างก็ว่าคล้ายๆ ไก่งวง จะคล้ายอะไรก็แล้วแต่คนมองจะจินตนาการไปกัน</p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(t).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
            console.log(offset+', '+targetPos);
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: 100%;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>