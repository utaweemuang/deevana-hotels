<?php
$title = 'Facilities | Deevana Plaza Krabi Ao Nang | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate and Krabi holiday package; 4 star hotel near Aonang Beach and Noppharat Thara Beach.';
$keyw = 'facilities, deevana plaza aonang krabi, aonang beach, 4-star hotel, beach hotel, krabi, hotel aonang beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanaplazakrabi/facilities.php';
$lang_th = '/th/deevanaplazakrabi/facilities.php';
$lang_zh = '/zh/deevanaplazakrabi/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/facilities-slide-01.jpg" alt="Deevana Plaza Krabi Aonang, 4-star resort" /></div>
        </div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">สิ่งอำนวยความสะดวกและบริการ</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swan_spa" class="tab active">โอเรียลทาล่า สปา</span>
                    <span data-tab="#argus_fitness" class="tab">อากัสฟิตเนส</span>
                    <span data-tab="#myna_kids_club" class="tab">ไมน่า คิดส์ คลับ</span>
                    <span data-tab="#swimming_pool" class="tab">สระว่ายน้ำ</span>
                    <span data-tab="#restaurant" class="tab">ห้องอาหารคิงฟิชเชอร์</span>
                    <span data-tab="#pigeon_library_and_internet_corner" class="tab">พีเจ้น ไลบาร์รี่และมุมอินเตอร์เน็ต</span>
                </div>
                
                <div class="tabs-content">
                    <article id="swan_spa" class="article" data-tab-name="Swan Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swan_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">โอเรียลทาล่า สปา</h1>
                                    <p>ขอเชิญท่านได้สัมผัสกับประสบการณ์ความรื่นรมย์ของการนวดแผนไทยโบราณ การนวดน้ำมัน และการบำบัด ด้วยสมุนไพร ที่โอเรียลทาล่า สปา อันสง่างามและหรูหราของเรา โอเรียลทาล่า สปาเป็นสถานที่ที่ยอดเยี่ยมสำหรับการพักผ่อน ประกอบด้วยห้องเตียงคู่แบบส่วนตัวพร้อมด้วยอ่างจากุซซี่และห้องอบไอน้ำ ห้องเดี่ยวที่เป็นส่วนตัว 2 ห้อง และมีการจัดพื้นที่เปิดโล่งเป็นพิเศษไว้สำหรับบริการนวดกดจุดฝ่าเท้าและนวดแผนไทย</p>
                                    <p>โอเรียลทาล่า สปา ขอนำเสนอรายการที่หลากหลายของการนวดเพื่อฟื้นฟูผิวพรรณและการบริการดูแลร่างกาย แต่ละรายการได้ถูกสร้างสรรค์ขึ้นเพื่อการผ่อนคลายและการปรับปรุงสุขภาพผิวพรรณให้ดีขึ้น นักบำบัดที่เต็มไปด้วยทักษะของสปาจะนำเอาภูมิปัญญาพื้นบ้านที่ดีที่สุดมารวมเข้ากับวิธีการแบบร่วมสมัยชั้นนำ เพื่อที่จะรังสรรค์ประสบการณ์ด้านสุขภาพที่เหนือกว่า ท่านยังสามารถเพลิดเพลินไปกับความแปลกใหม่และความผ่อนคลายจากน้ำมันที่ทำจากสมุนไพรไทย รวมถึงผลิตภัณฑ์ด้านสุขภาพที่ดีเลิศอื่นๆซึ่งได้ถูกนำมาใช้ในการบำรุงรักษาแบบเฉพาะบุคคล หรือแพ็คเกจสปาแบบพิเศษ</p>
                                    <p><span style="color: #516819;">โอเรียลทาล่า สปา จะทำให้แน่ใจว่าท่านจะได้รับบริการสปาที่น่าจดจำอย่างที่ท่านต้องการอย่างแท้จริงในทุกวัน : เวลา 9.00 - 21.00 น.<br> ที่ตั้ง : ชั้นล็อบบี้ของอาคารต้อนรับ</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="argus_fitness" class="article" data-tab-name="Argus Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_argus_fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">อากัสฟิตเนส</h1>
                                    <p>สำหรับผู้เข้าพักที่ต้องการที่จะดูแลรักษารูปร่างในช่วงวันหยุด ที่โรงแรมดีวาน่า พลาซ่า กระบี่ อ่าวนาง เรามีศูนย์ออกกำลังกายพร้อมอุปกรณ์อย่างดีให้บริการโดยไม่เสียค่าใช้จ่าย นักวิ่งยังสามารถเพลิดเพลินไปกับการวิ่งในตอนเช้าไปตามชายหาดนพรัตน์ธาราที่สวยงาม</p>
                                    <p><span style="color: #516819;">ศูนย์ออกกำลังกายอากัส ฟิตเนสเปิดให้บริการทุกวัน : เวลา 7.00 - 21.00 น.<br>ที่ตั้ง : ชั้นหนึ่งในอาคาร 1</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="myna_kids_club" class="article" data-tab-name="Myna Kid's Club">
                        <div class="container">
                            <div class="row row-content-tab"> 
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_myna_kid's_club.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ไมน่า คิดส์ คลับ</h1>
                                    
                                    <p>โรงแรมดีวาน่า พลาซ่า กระบี่ อ่าวนาง เชิญชวนคุณหนูสู่ไมน่า คิดส์ คลับ ที่มีสีสันสดใสและเต็มไปด้วยจิตนการสร้างสรรค์  สำหรับเด็กอายุตั้งแต่ 4 -12 ปี สามารถเพลิดเพลินไปกับกิจกรรมที่สนุกสนานหลากหลายภายใต้การดูแลของพนักงานของรีสอร์ท
ซึ่งได้รับการฝึกฝนมาอย่างดี เรายังมีทีวี ตุ๊กตา และของเล่นเพื่อให้ความบันเทิงแก่เด็กๆ</p>
                                    <p>เด็ก ๆ ยังสามารถสนุกสนานไปกับสระว่ายน้ำสำหรับเด็กและในสนามเด็กเล่นกลางแจ้ง ในขณะที่แม่และพ่อจะสามารถใช้โอกาสนี้ในการใช้เวลาอยู่ด้วยกันอย่างเต็มที่</p>
                                    <p><span style="color:#516819">ไมน่า คิดส์ คลับ เปิดให้บริการทุกวัน : เวลา 9.00 - 18.00 น.<br>
                                       ที่ตั้ง : ชั้นหนึ่งขององอาคาร 1 (ติดกับอากัสฟิตเนส)</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_swimming_pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ</h1>
                                    <p>ที่ใจกลางของโรงแรมดีวาน่า พลาซ่า กระบี่ อ่าวนางคือที่ตั้งของสระน้ำที่มีลักษณะเหมือนบึงขนาดใหญ่ ประกอบด้วยสระจากุซซี่หนึ่งสระ สระว่ายน้ำขนาดใหญ่ 3 สระ และสระสำหรับเด็กที่สะดวกสบายอีก 1 สระ ให้บริการจากเช้าจรดค่ำ ซึ่งผู้เข้าพักในห้องชั้นล่างแบบห้องพรีเมียร์พูลแอ็กเสสสามารถเข้าไปยังสระว่ายน้ำนี้ของโรงแรมได้โดยตรงจากระเบียงส่วนตัวในห้องพัก และสระว่ายน้ำแห่งนี้ยังให้บริการแก่ผู้เข้าพักในรีสอร์ททั่วไปด้วยเช่นกัน บริเวณริมสระน้ำยังมีร่มกันแดดขนาดใหญ่ที่ให้ร่มเงาได้ทั่วถึงและเก้าอี้อาบแดดที่สะดวกสบาย มันเป็นจุดที่สมบูรณ์แบบสำหรับเติมความสดชื่นและการพักผ่อนตลอดทั้งวันและในช่วงเย็น บริเวณสระว่ายน้ำยังมีซันเบิร์ด พูล บาร์ ซึ่งให้บริการเครื่องดื่มและอาหารว่างแสนอร่อยอีกด้วย</p>
                                    <p><span style="color: #516819;">สระว่ายน้ำเปิดให้บริการทุกวัน : เวลา 7.00 - 21.00 น.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
<article id="restaurant" class="article" data-tab-name="ห้องอาหารและบาร์">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_kingfisher_restaurant.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_sunbird_pool_bar.jpg?ver=20160617" /><br/>
                                    <img class="force thumbnail" src="images/facilities/facilities_starling_lobby_lounge.jpg?ver=20160617" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ห้องอาหารคิงฟิชเชอร์</h1>
                                    <p>คิงฟิชเชอร์เป็นห้องอาหารชื่อดังของโรงแรมดีวาน่า พลาซ่า กระบี่ อ่าวนาง ที่นี่ผู้เข้าพักสามารถรับประทานอาหารที่มีสไตล์และค้นพบรสชาติของอาหารจากทั่วทุกมุมโลก แขกผู้มาใช้บริการสามารถเพลิดเพลินกับการเริ่มต้นวันด้วยอาหารเช้าแบบบุฟเฟต์ที่เอร็ดอร่อย ซึ่งมีอาหารแบบเอเชียและอาหารนานาชาติ โดยให้บริการในบริเวณส่วนเปิดโล่งของระเบียง ทั้งนี้ในมื้อเที่ยงและมื้อเย็น ร้านอาหารแห่งนี้จะนำเสนอรายการอาหารไทยและนานชาติซึ่งเป็นที่ชื่นชอบหลากหลายรายการ รสชาติอาหารที่อร่อยและบริการที่เอาใจใส่ของคิงฟิชเชอร์จะทำให้ท่านได้รับประสบการณ์การรับประทานอาหารที่แสนรื่นรมย์<br/>
ประเภทอาหาร : อาหารไทยและอาหารนานาชาติ<br/>
<!-- เปิดให้บริการทุกวัน : เวลา 6.00 - 23.00 น.<br/> -->
เปิดให้บริการทุกวัน รวมถึงรูมเซอร์วิส : เวลา 6.00 - 22.00 น. Last order 21.30 น.<br/>
ที่ตั้ง : ชั้นล่างของอาคารต้อนรับ</p>

									<br/><br/><br/><br/><br/>
                                    <h1 class="title">ซันเบิร์ด พูล บาร์</h1>
                                    <p>ซันเบิร์ด พูล บาร์ตั้งอยู่บริเวณริมสระน้ำที่เงียบสงบ เป็นสถานที่ที่เหมาะสำหรับการพักผ่อนและเพลิดเพลินไปกับเบียร์เย็นๆ เครื่องดื่มดับกระหาย กาแฟและชาที่ได้เลือกสรรมาอย่างดี และรวมไปถึงอาหารว่างจากเมนูอันหลากหลายของเรา
ประเภทอาหาร : อาหารว่างนานาชาติ และเครื่องดื่ม<br/>
เปิดให้บริการทุกวัน : เวลา 10.00 - 19.00 น.<br/>
ที่ตั้ง : ริมสระน้ำ ที่อาคารหมายเลข 6</p>

									<br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h1 class="title">สตาร์ลิ้ง ล็อบบี้ เลานจ์</h1>
                                    <p>สตาร์ลิ้ง ล็อบบี้ เลานจ์ของเราจะให้ความรู้สึกที่อบอุ่นและเป็นมิตร จึงเป็นสถานที่ที่เหมาะสมสำหรับการใช้เวลากับครอบครัวและเพื่อนฝูง ร่วมกันเพลิดเพลินไปกับเครื่องดื่มและของว่างทั้งในเวลากลางวันและกลางคืน และบทเพลงไพเราะในยามเย็นจะทำให้ท่านมีชีวิตชีวาเป็นพิเศษ<br/>
ประเภทอาหาร : อาหารว่างนานาชาติ และเครื่องดื่ม<br/>
เปิดให้บริการทุกวัน : เวลา 10.00 น. - เที่ยงคืน<br/>
ที่ตั้ง : ชั้นล็อบบี้ของอาคารต้อนรับ</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="pigeon_library_and_internet_corner" class="article" data-tab-name="Pigeon Library &amp; Internet Corner">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/facilities_pigeon_library_and_internet_corner.jpg?ver=20160525" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">พีเจ้น ไลบาร์รี่และมุมอินเตอร์เน็ต</h1>
                                    <p>ผู้เข้าพักสามารถเพลิดเพลินไปกับช่วงเวลาที่เงียบสงบในห้องสมุดของรีสอร์ทที่เต็มไปด้วยหนังสือดีๆมากมาย หนังสือหลากหลายประเภท รวมถึงหนังสือพิมพ์และนิตยสารในประเทศ ท่านยังสามารถยืมแผ่นดีวีดีเพื่อรับชมในห้องพักสำหรับแขกที่สะดวกสบาย ยังมีบริการเชื่อมต่ออินเตอร์เน็ตและอุปกรณ์คอมพิวเตอร์โดยไม่คิดค่าใช้จ่าย</p>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>