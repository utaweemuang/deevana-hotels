<div class="side-panel-drag"></div>
<div class="side-panel-bg"></div>
<div class="side-panel">
    <?php include 'menu-main.php'; ?>

    <div class="divider"></div>

    <div class="languages">
        <h3>LANGUAGE</h3>
        <ul>
            <li><a href="<?php echo $lang_en; ?>">English</a></li>
            <li><a href="<?php echo $lang_th; ?>">Thai</a></li>
            <li><a href="<?php echo $lang_zh; ?>">Chinese</a></li>
        </ul>
    </div>
</div>