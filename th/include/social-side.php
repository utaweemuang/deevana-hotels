<div id="social_side">
    <ul>
        <li><a class="icon fb" href="<?php echo get_info('facebook'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-facebook"></i></a></li>
        <li><a class="icon ig" href="<?php echo get_info('instagram'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-instagram"></i></a></li>
        <li><a class="icon yt" href="<?php echo get_info('youtube'); ?>" target="_blank" rel="nofollow"><i class="sprite-social-youtube"></i></a></li>
        <li><a class="icon tw" href="<?php echo get_info('twitter'); ?>" target="_blank" rel="nofollow" style="color:#41ABDD;"><i class="fa fa-twitter"></i></a>
        <li><a class="icon li" href="https://line.me/R/ti/p/%40ejt5213n" target="_blank" rel="nofollow"><img src="../../assets/elements/LINE_SOCIAL_Circle.png" width="16" height="16" alt="" style="vertical-align: middle;"></a></span>
    </ul>
</div>