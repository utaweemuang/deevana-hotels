<ul class="list-menu">
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">โรงแรมของเรา</a>
        <ul class="sub-menu level-1">
            <li class="label">ภูเก็ต</li>
            <li><a href="/th/deevanaplazaphuket/">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</a></li>
            <li><a href="/th/deevanapatong/">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</a></li>
            <li><a href="/th/ramadaphuketdeevana/">รามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า</a></li>
            <li><a href="/th/recentasuitephuket/">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</a></li>
            <li><a href="/th/recentaphuket/">รีเซ็นต้า ภูเก็ต สวนหลวง</a></li>
            <li><a href="/th/recentastyle/">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</a></li>
            <li class="label">กระบี่</li>
            <li><a href="/th/deevanaplazakrabi/">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</a></li>
            <li><a href="/th/deevanakrabiresort/">ดีวาน่า กระบี่ รีสอร์ท</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('our-brands'); ?>"><a href="our-brands.php">แบรนด์ของเรา</a></li>
    <li class="has-sub-menu">
        <a href="/th/meeting-events.php">ห้องประชุม</a>
        <ul class="sub-menu">
            <li class="label">Phuket</li>
            <li><a href="/th/deevanaplazaphuket/meetings-and-event.php" target="_blank">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</a></li>
            <li><a href="/th/deevanapatong/facilities.php#mice_facilities">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</a></li>
            <li><a href="/th/ramadaphuketdeevana/meetings-and-event.php" target="_blank">รามาด้า บายวินด์แฮม ภูเก็ต ดีวาน่า</a></li>
            <li class="label">Krabi</li>
            <li><a href="/th/deevanaplazakrabi/meetings-and-event.php" target="_blank">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">สถานที่ท่องเที่ยว</a>
        <ul class="sub-menu level-1">
            <li class="<?php echo get_current_class('attraction-phuket'); ?>"><a href="attraction-phuket.php">ภูเก็ต</a></li>
            <li class="<?php echo get_current_class('attraction-krabi'); ?>"><a href="attraction-krabi.php">กระบี่</a></li>
        </ul>
    </li>
    <li class="has-sub-menu">
        <a href="#" onClick="return false;">ข้อเสนอพิเศษ</a>
        <ul class="sub-menu level-1">
            <li class="label">ภูเก็ต</li>
            <li><a href="/th/deevanaplazaphuket/offers.php" target="_blank">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</a></li>
            <li><a href="/th/deevanapatong/offers.php" target="_blank">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</a></li>
            <li><a href="/th/ramadaphuketdeevana/offers.php" target="_blank">รามาด้า ภูเก็ต ดีวาน่า</a></li>
            <li><a href="<?php ibe_url( '299', 'th' ); ?>" target="_blank">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</a></li>
            <li><a href="<?php ibe_url( '294', 'th' ); ?>" target="_blank">รีเซ็นต้า ภูเก็ต สวนหลวง</a></li>
            <li><a href="<?php ibe_url( '310', 'th' ); ?>" target="_blank">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</a></li>
            <li class="label">กระบี่</li>
            <li><a href="<?php ibe_url( '276', 'th' ); ?>" target="_blank">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</a></li>
            <li class="hidden"><a href="#">ดีวาน่า กระบี่ รีสอร์ท</a></li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">ติดต่อเรา</a></li>
</ul>