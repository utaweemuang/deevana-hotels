<div id="m-ourhotels-menu" class="offside-menu">
    <h2>โรงแรมของเรา</h2>
    
    <h3>ภูเก็ต</h3>
    <ul class="sub-menu level-2">
		<li><a href="/th/deevanaplazaphuket/">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</a></li>
		<li><a href="/th/deevanapatong/">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</a></li>
		<li><a href="/th/ramadaphuketdeevana/">รามาด้า ภูเก็ต ดีวาน่า</a></li>
		<li><a href="/th/recentasuitephuket/">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</a></li>
		<li><a href="/th/recentaphuket/">รีเซ็นต้า ภูเก็ต สวนหลวง</a></li>
		<li><a href="/th/recentastyle/">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</a></li>
    </ul>
    
    <h3>กระบี่</h3>
    <ul class="sub-menu level-2">
        <li><a href="/th/deevanaplazakrabi/">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</a></li>
		<li><a href="/th/deevanakrabiresort/">ดีวาน่า กระบี่ รีสอร์ท</a></li>
    </ul>
    
    <span class="close">&times;</span>
</div>

<div id="m-offers-menu" class="offside-menu">
	<h2>ข้อเสนอพิเศษ</h2>
    
    <h3>ภูเก็ต</h3>
    <ul class="sub-menu level-2">
		<li><a href="/th/deevanaplazaphuket/offers.php" target="_blank">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</a></li>
		<li><a href="/th/deevanapatong/offers.php" target="_blank">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</a></li>
		<li><a href="/th/ramadaphuketdeevana/offers.php" target="_blank">รามาด้า ภูเก็ต ดีวาน่า</a></li>
		<li><a href="<?php ibe_url( '299', 'th' ); ?>" target="_blank">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</a></li>
		<li><a href="<?php ibe_url( '294', 'th' ); ?>" target="_blank">รีเซ็นต้า ภูเก็ต สวนหลวง</a></li>
		<li><a href="<?php ibe_url( '310', 'th' ); ?>" target="_blank">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</a></li>
    </ul>
    
    <h3>กระบี่</h3>
    <ul class="sub-menu level-2">
        <li><a href="<?php ibe_url( '276', 'th' ); ?>" target="_blank">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</a></li>
        <li class="hidden"><a href="#">ดีวาน่า กระบี่ รีสอร์ท</a></li>
    </ul>
    
    <span class="close">&times;</span>
</div>