<div class="booking-bar">
    <div class="inner">
        <div class="container">
            <form id="booking-form" class="form">
                <div class="table">

                    <div class="table-cell table-cell-header">
                        <span class="field field-header">
                            <img class="block responsive" src="assets/elements/book_online_best_rate_guarantee.png" alt="Book Online Best rate guarantee" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-hotel">
                        <span class="field field-hotel">
                            <select id="hotel" class="input-select" name="propertyId" required
                                    data-error-msg="please select hotel">
                                <option value="" selected>- เลือกโรงแรม -</option>
                                <optgroup label="ภูเก็ต">
                                    <option value="275">ดีวาน่า พลาซ่า ภูเก็ต ป่าตอง</option>
                                    <option value="277">ดีวาน่า ป่าตอง รีสอร์ท แอนด์ สปา</option>
                                    <option value="278">รามาด้า ภูเก็ต ดีวาน่า</option>
                                    <option value="299">รีเซ็นต้า สวีท ภูเก็ต สวนหลวง</option>
                                    <option value="294">รีเซ็นต้า ภูเก็ต สวนหลวง</option>
                                    <option value="310">รีเซ็นต้า สไตล์ ภูเก็ตทาวน์</option>
                                </optgroup>
                                <optgroup label="กระบี่">
                                    <option value="276">ดีวาน่า พลาซ่า กระบี่ อ่าวนาง</option>
                                    <option value="386">ดีวาน่า กระบี่ รีสอร์ท</option>
                                </optgroup>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkin">
                        <span class="field field-checkin">
                            <input id="checkin" class="input-text date" name="checkin" placeholder="Check-in" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-checkout">
                        <span class="field field-checkout">
                            <input id="checkout" class="input-text date" name="checkout" placeholder="Check-out" type="text" readonly />
                        </span>
                    </div>

                    <div class="table-cell table-cell-adults">
                        <span class="field field-adults">
                            <label class="label">ผู้ใหญ่</label>
                            <select id="adults" class="input-select" name="numofadult">
                                <option value="1">1 ผู้ใหญ่</option>
                                <option value="2" selected>2 ผู้ใหญ่</option>
                                <option value="3">3 ผู้ใหญ่</option>
                                <option value="4">4 ผู้ใหญ่</option>
                                <option value="5">5 ผู้ใหญ่</option>
                                <option value="6">6 ผู้ใหญ่</option>
                                <option value="7">7 ผู้ใหญ่</option>
                                <option value="8">8 ผู้ใหญ่</option>
                                <option value="9">9 ผู้ใหญ่</option>
                                <option value="10">10 ผู้ใหญ่</option>
                                <option value="11">11 ผู้ใหญ่</option>
                                <option value="12">12 ผู้ใหญ่</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-children">
                        <span class="field field-children">
                            <label class="label">เด็ก</label>
                            <select id="children" class="input-select" name="numofchild">
                                <option value="0" selected>0 เด็ก</option>
                                <option value="1">1 เด็ก</option>
                                <option value="2">2 เด็ก</option>
                                <option value="3">3 เด็ก</option>
                                <option value="4">4 เด็ก</option>
                                <option value="5">5 เด็ก</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-rooms">
                        <span class="field field-rooms">
                            <label class="label">ห้อง</label>
                            <select id="rooms" class="input-select" name="numofroom">
                                <option value="1" selected>1 ห้อง</option>
                                <option value="2">2 ห้อง</option>
                                <option value="3">3 ห้อง</option>
                                <option value="4">4 ห้อง</option>
                                <option value="5">5 ห้อง</option>
                                <option value="6">6 ห้อง</option>
                                <option value="7">7 ห้อง</option>
                                <option value="8">8 ห้อง</option>
                                <option value="9">9 ห้อง</option>
                            </select>
                        </span>
                    </div>

                    <div class="table-cell table-cell-code">
                        <span class="field field-code">
                            <input id="accesscode" class="input-text" name="accesscode" placeholder="Promo code" type="text" />
                        </span>
                    </div>

                    <div class="table-cell table-cell-submit">
                        <span class="field field-submit">
                            <button id="submit" class="button" type="submit">จองห้อง</button>
                        </span>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>