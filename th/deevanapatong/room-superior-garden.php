<?php
$title = 'Superior Garden Room | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Superior Garden Room: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'superior garden room, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-superior-garden';
$cur_page = 'superior-garden';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-superior-garden.php';
$lang_th = '/th/deevanapatong/room-superior-garden.php';
$lang_zh = '/zh/deevanapatong/room-superior-garden.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/superior/1500/superior-garden-01.jpg" alt="Superior Garden Room 01" />
                    <img src="images/accommodations/superior/1500/superior-garden-02.jpg" alt="Superior Garden Room 02" />
                    <img src="images/accommodations/superior/1500/superior-garden-03.jpg" alt="Superior Garden Room 03" />
                    <img src="images/accommodations/superior/1500/superior-garden-04.jpg" alt="Superior Garden Room 04" />
                    <img src="images/accommodations/superior/1500/superior-garden-05.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-06.jpg" alt="Superior Garden Room 05" />
                    <img src="images/accommodations/superior/1500/superior-garden-07.jpg" alt="Superior Garden Room 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs disable-touch">
            <h2 class="title">ห้องซูพีเรียการ์เด้น <span>เตียงคิงไซส์</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/superior/600/superior-garden-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-05.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-06.jpg" height="50" /></li>
                    <li><img src="images/accommodations/superior/600/superior-garden-07.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องซูพีเรียการ์เด้น</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/superior/600/superior-garden-02.jpg" />
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ห้องซุพีเรียการ์เด้นมีขนาด 27 ตารางเมตร พร้อมเตียงให้เลือกสองแบบ ทั้งแบบเตียงคู่หรือเตียงขนาดใหญ่ 6 ฟุตแบบคิงไซส์ เฟอร์นิเจอร์ไม้สีเข้มแบบคลาสสิคภายในห้องพักตัดกันได้อย่างลงตัวกับห้องพักที่สะอาดสะอ้านและตกแต่งแบบไทย พร้อมห้องน้ำในตัวที่มีฝักบัวอาบน้ำและตู้เสื้อผ้า</p>                            
                            
                            <p class="note">
                                <strong>หมายเหตุ</strong><br>
                                ผ้าเช็ดตัวสำหรับชายหาดจะมีให้บริการบริเวณโต๊ะให้บริการ<br>
                                ผ้าเช็ดตัวสำหรับสระน้ำจะมีให้บริการบริเวณสระน้ำ
                            </p>
                            
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                        <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                        <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                        <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                        <li>ตู้นิรภัยในห้องพัก</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i>อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                            <li>สิ่งอำนวยความสะดวกภายในห้องน้ำ : เจลสำหรับอาบน้ำและสระผม, หมวกอาบน้ำ, ผ้าเช็ดตัว, สำลีก้าน (คอตตอนบัด)</li>
                                            <li>อุปกรณ์เย็บผ้า</li>
                                            <li>รองเท้าแตะ</li>
                                            <li>ร่ม</li>
                                            <li>ถุงชายหาด</li>
                                            <li>ชั้นวางกระเป๋าเดินทาง</li>
                                            <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                            <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                            <li>ตู้เย็นและฟรีน้ำดื่ม 2 ขวดต่อวัน</li>
                                            <li>ห้องน้ำส่วนตัว พร้อมด้วยฝักบัวน้ำอุ่นและน้ำเย็น</li>
                                            <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                            <li>วิวสวน (การ์เด้นวิง)</li>
                                            <li>อุปกรณ์สำหรับชงชา/กาแฟ</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้นิรภัยในห้องพัก</li>
                                            <li>ห้องพักที่เชื่อมต่อกันได้</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>