<?php

$url        = 'http://www.deevanahotels.com/th/deevanapatong/';
$name       = 'Deevana Patong Resort &amp; Spa';
$version    = '20180403';
$email      = 'info@deevanapatong.com';
$debugEmail = 'frontend@travelanium.com';
$author     = 'Travelanium';

$facebook   = 'https://www.facebook.com/DeevanaPatongResortSpa/';
$twitter    = 'https://twitter.com/deevanapatong/';
$googleplus = 'https://plus.google.com/109934025382332169075/posts/';
$youtube    = '#';
$vimeo      = '#';
$instagram  = 'https://instagram.com/deevanapatongresortandspa/';
$flickr     = '#';
$pinterest  = '#';
$tripadvisor = 'http://www.tripadvisor.co.uk/Hotel_Review-g297930-d505067-Reviews-Deevana_Patong_Resort_Spa-Patong_kathu_phuket.html';
$glt        = 'http://www.greenleafthai.org/en/index.php';

$ibeID      = '277';
