<?php
$title = 'Facilities | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Facilities: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'facilities, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/deevanapatong/facilities.php';
$lang_th = '/th/deevanapatong/facilities.php';
$lang_zh = '/zh/deevanapatong/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <?php for($i = 1; $i <= 6; $i++) : ?>
            <?php $index = ($i<10) ? '0'.$i : $i; ?>
            <div class="item"><img src="images/facilities/facilities-slide-<?php echo $index; ?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
            <?php endfor; ?>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">สิ่งอำนวยความสะดวก</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#restaurant" class="tab active">ร้านอาหาร</span>
                    <span data-tab="#swimming_pool" class="tab">สระว่ายน้ำ</span>
                    <span data-tab="#leisure_and_tours" class="tab">การพักผ่อนและท่องเที่ยว</span>
                    <span data-tab="#mice_facilities" class="tab">ห้องประชุม</span>
                    <span data-tab="#wedding" class="tab">งานแต่งงาน</span>
                    <span data-tab="#cooking_class" class="tab">เรียนทำอาหารไทย</span>
                    <span data-tab="#orientala_wellness_spa" class="tab">โอเรียลทาล่า สปา</span>
                </div>
                
                <div class="tabs-content">
                    <article id="restaurant" class="article" data-tab-name="Restaurant">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-01.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-02.jpg" alt="Restaurent" /><br/>
                                    <img class="force thumbnail" src="images/facilities/restaurant/restaurant-03.jpg" alt="Restaurent" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">ร้านอาหาร</h1>
                                    
                                    <h2 class="sub-title">ห้องอาหารดาหลา</h2>
                                    <p>เริ่มต้นวันของท่านด้วยตัวเลือกอันยอดเยี่ยมทั้งอาหารเช้าแบบไทยและแบบตะวันตก มีทั้งอาหารนานาชาติแบบปรุงสุกที่เป็นที่ชื่นชอบ ซีเรียล ผลไม้ ชา กาแฟและอีกมากมายจากบุฟเฟ่ต์รถเข็นอาหารเช้าที่แสนอร่อยที่ห้องอาหารดาหลาของเรา การตกแต่งที่เหมาะเจาะ, ทิวทัศน์สระว่ายน้ำและส่วนที่เปิดโล่งทำให้แน่ใจว่าร้านอาหารของเราจะนำมาซึ่งการเริ่มต้นวันใหม่อย่างสดชื่น<br/>
                                        เวลาให้บริการ : 06:00 - 11:00 น.</p>
                                        <br/><br/><br/><br/><br/><br/><br/>
                                    
                                    <h2 class="sub-title">เดอะบัลโคนี่คาเฟ่แอนด์อินเตอร์เน็ต</h2>
                                    <p>เดอะบัลโคนี่คาเฟ่แอนด์อินเตอร์เน็ตตั้งอยู่ในส่วนดีลักซ์วิง ร้านอาหารที่มีระเบียงสวยงามนี้จะเปิดให้บริการสำหรับอาหารกลางวันและอาหารเย็น ท่านสามารถมองเห็นทิวทัศน์ที่สวยงามของสวนเขตร้อนเขียวชอุ่มและสระว่ายน้ำ มื้อเย็นแบบสบายๆที่ระเบียงกลางแจ้งซึ่งมีผนังเปิดโล่ง จะทำให้ท่านได้สัมผัสกับอากาศบริสุทธิ์ ท่านสามารถเลือกที่นั่งแบบเก้าอี้ซึ่งมีที่พักแขนอ่อนนุ่มหรือโต๊ะเข้าชุดแบบทันสมัย เดอะบัลโคนี่เป็นพื้นที่ที่ดีสำหรับการเพลิดเพลินไปกับการนั่งจิบกาแฟสดและเค้กโฮมเมด หรือมื้อเย็นที่แสนอร่อย <br/>
                                        เวลาให้บริการ : 18.00 - 22.00  น.</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">ดีลักซ์พูลบาร์</h2>
                                    <p>ท่านสามารถพักผ่อนบนเก้าอี้สำหรับเอนหลังหรือว่ายน้ำขึ้นไปที่ดีลักซ์พูลบาร์และเติมความสดชื่นด้วยการจิบน้ำผลไม้ปั่น น้ำมะพร้าวสดและเครื่องดื่มแช่เย็นอีกมากมาย นอกจากนี้ยังมีอาหารว่างและขนมขบเคี้ยวอื่นๆพร้อมให้บริการ<br/>
                                        เวลาให้บริการ : 10:00 - 20:00 น.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/pool/pool-01.jpg" alt="Swimming Pool"/>
                                    <br/>
                                    <img class="force thumbnail" src="images/facilities/pool/pool-02.jpg" alt="Swimming Pool" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">สระว่ายน้ำ</h1>
                                    
                                    <h2 class="sub-title">ดีลักซ์พูล</h2>
                                    <p>สระว่ายน้ำในส่วนดีลักซ์วิงนับเป็นจุดเด่นของรีสอร์ทแห่งหนึ่ง เนื่องจากระเบียงของห้องพักในรีสอร์ททั้งหมดจะหันหน้าสู่สระว่ายน้ำแห่งนี้ สระว่ายน้ำรูปสี่เหลี่ยมผืนผ้าขนาดใหญ่แห่งนี้ตั้งอยู่ด้านข้างสระน้ำตื้นสำหรับเด็ก และมีน้ำพุช้างที่มีเสน่ห์ถึงสองตัว ด้านข้างของสระน้ำมีที่ว่างมากมายสำหรับอาบแดดและยังบาร์ริมสระน้ำ ที่ซึ่งท่านสามารถว่ายน้ำขึ้นไปเพลิดเพลินกับเครื่องดื่มเย็นๆเพื่อเติมความสดชื่น</p>
                                    
                                    <br/><br/><br/><br/><br/><br/><br/><br/>
                                    <h2 class="sub-title">การ์เด้นพูล</h2>
                                    <p>สระว่ายน้ำในส่วนการ์เด้นวิงจะล้อมรอบไปด้วยต้นไม้เขียวขจีที่งดงาม เหมาะสำหรับเป็นสถานที่พักผ่อนยามกลางวันของท่าน เมื่อเร็วๆนี้การ์เด้นพูลของเราเพิ่งจะได้รับการตกแต่งใหม่ มีทางลาดซึ่งทำให้ง่ายต่อการเดินไปว่ายน้ำ สระว่ายน้ำของเรามีขนาดใหญ่ พร้อมด้วยที่นั่งด้านข้างแบบจากุซซี่และน้ำพุที่ทันสมัย สระว่ายน้ำเด็กจะมีน้ำพุที่น่ารักซึ่งสามารถสร้างความสุขความบันเทิงเล็กๆน้อยๆให้กับเด็กๆได้</p>
                                    <p>เวลาให้บริการ : 10:00 - 20:00 น.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="leisure_and_tours" class="article" data-tab-name="Leisure &amp; Tours">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tours/tours-01.jpg" alt="Leisure &amp; Tours" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">การพักผ่อนและท่องเที่ยว</h1>
                                    
                                    <p>นอกจากการพักผ่อนบนชายหาดบนเกาะที่โด่งดังแล้ว ภูเก็ตยังมีสิ่งที่น่าชมน่าทำอื่นๆอีกมากมาย ท่านสามารถสำรวจบางส่วนของเกาะด้วยการดำน้ำดูปะการังที่จะทำให้ท่านตื่นตาตื่นใจไปกับโลกใต้น้ำ เยี่ยมชมบางส่วนของสถานที่ที่เกี่ยวกับศาสนาพร้อมด้วยวัดที่มีชื่อเสียงเช่นวัดพระใหญ่และวัดฉลอง ย้อนเวลากลับไปในอดีตของเมืองเก่าที่มีอาคารและพิพิธภัณฑ์ทางประวัติศาสตร์มากมาย นั่งช้างหรือชมเกาะจากจุดชมวิวที่สวยงามมากมาย และนี่คือบางส่วนของสถานที่ท่องเที่ยวที่เราแนะนำ</p>
                                    
                                    <h2 class="sub-title">ภูเก็ตเมืองเก่า</h2>
                                    <p>ย้อนกลับไปเมื่อประมาณ 100 ปีก่อน ภูเก็ตเป็นชุมชนชาวเหมืองที่เจริญรุ่งเรืองด้วยการตั้งถิ่นฐานของชาวจีนจำนวนมากเพื่อทำงานในอุตสาหกรรมเหมืองแร่ ในช่วงที่เศรษฐกิจดี การก่อสร้างคฤหาสน์ บ้าน อาคารพานิชย์ล้วนเป็นแบบชิโนโปรตุกีส โดยอาคารเหล่านี้จะมีลักษณะแคบๆแต่มีความยาว ตกแต่งด้วยประตูไม้แกะสลักฉลุแบบจีน คฤหาสน์มากมายในเมืองยังคงได้รับการดูแลรักษาและบูรณะเป็นร้านกาแฟที่แปลกตา, ร้านอาหาร, ห้องภาพและอื่นๆ ท่านสามารถเดินรอบๆถนนดีบุก, พังงา, เยาวราช, ถลางและกระบี่ เพื่อเยี่ยมชมสถาปัตยกรรมที่สวยงาม อาคารเก่าสไตล์ยุโรปอื่นๆได้แก่ ศาลากลาง, ศาลจังหวัด, และธนาคารนครหลวงไทย</p>
                                    
                                    <h2 class="sub-title">วัดฉลอง</h2>
                                    <p>วัดแห่งนี้ได้รับการดูแลรักษาอย่างดี เป็นสถานที่ที่เป็นที่เคารพบูชาของคนภูเก็ตแห่งหนึ่งและเป็นที่นิยมสำหรับนักท่องเที่ยวทั่วไป โดยวัดแห่งนี้จะมีรูปปั้นจำลองของหลวงพ่อแช่มและหลวงพ่อช่วงประดิษฐ์สถานอยู่ โดยในอดีตหลวงพ่อทั้งสองรูปนี้คือผู้ให้การช่วยเหลือรักษาชาวภูเก็ตด้วยความรู้ด้านยาสมุนไพรในระหว่างการปฏิวัติของคนงานเหมืองดีบุกในปีพ.ศ. 2419</p>
                                    
                                    <h2 class="sub-title">จุดชมวิวกะรน</h2>
                                    <p>จุดชมวิวกะรนตั้งอยู่บนเนินเขาซึ่งอยู่ระหว่างกะตะและในหาน มีศาลาที่สร้างขึ้นเพื่อชมทิวทัศน์ที่งดงาม นักท่องเที่ยวสามารถนั่งพักและเพลิดเพลินไปกับทิวทัศน์ของหาดกะตะน้อย, หาดกะตะ, และหาดกะรน รวมไปถึงเกาะปูเล็กๆ มีห้องสุขาและเครื่องดื่มให้บริการ</p>
                                    
                                    <h2 class="sub-title">บริการนำเที่ยว</h2>
                                    <p>ที่รีสอร์ท เรามีบริการนำเที่ยวต่างๆให้เลือกเต็มรูปแบบ ทั้งชมเมืองเก่าภูเก็ต, เกาะพีพี, เกาะตะปู(เกาะเจมส์บอนด์), ดำน้ำลึก, ดำน้ำตื้น, ตกปลา, ซาฟารีและขี่ช้าง, ท่องเที่ยงด้วยเรือเร็วส่วนตัว, เพลิดเพลินกับการพายเรือแคนู, ภูเก็ตแฟนตาซี, และอื่นๆอีกมากมาย เจ้าหน้าที่ของเราซึ่งเป็นผู้มีความรู้เกี่ยวกับท้องถิ่นเป็นอย่างดีจะช่วยแนะนำทัวร์ที่เหมาะสมกับท่านซึ่งรวมถึง :</p>
                                    <ul class="list-columns-3 custom-list-dashed">
                                        <li>กีฬาทางน้ำ</li>
<li>มินิกอล์ฟ</li>
<li>กอล์ฟ</li>
<li>สนามกอล์ฟนานาชาติ 4 สนาม</li>
<li>โกคาร์ต</li>
<li>ปีนหน้าผาจำลอง</li>
<li>จักรยานเสือภูเขา</li>
<li>บันจี้จัมพ์</li>
<li>ท่องเที่ยวเชิงนิเวศ</li>
<li>เดินป่าและนั่งช้าง</li>
<li>ล่องแพ</li>
<li>เที่ยวชมเกาะและเมืองภูเก็ต</li>
<li>ขี่ม้า</li>
<li>สนายิงปืน</li>
<li>เกาะพีพี</li>
<li>เกาะเจมส์บอนด์ (อ่าวพังงา)</li>
<li>เกาะเฮ (Coral Island)</li>
<li>เกาะราชา(รายา)</li>
<li>เกาะไข่</li>
<li>เกาะสิมิลัน</li>
<li>เกาะรังใหญ่</li>
<li>ท่องเที่ยวด้วยเรือเร็วแบบส่วนตัว</li>
<li>ดำน้ำลึก</li>
<li>ดำน้ำตื้น</li>
<li>ตกปลา</li>
<li>ล่องเรือสำราญและเรือใบ</li>
<li>พายเรือแคนู</li>
<li>ล่องเรือสำเภาโบราณ</li>
                                    </ul>
                                    <p>และอื่นๆอีกมากมาย</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="mice_facilities" class="article" data-tab-name="MICE Facilities">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                <img class="force thumbnail" src="images/facilities/mice/meeting-01.jpg" alt="MICE Facilities"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <!--<h1 class="title">สิ่งอำนวยความสะดวกสำหรับการประชุมนานาชาติ (MICE)</h1>-->
                                    
                                    <h2 class="sub-title">การประชุมและสัมมนา</h2>
                                    <p>โรงแรมดีวาน่า ป่าตอง รีสอร์ทแอนด์สปาให้บริการสิ่งอำนวยความสะดวกที่ดีที่สุดสำหรับการจัดงานต่างๆ ด้วยห้องโถงที่สวยงาม สวนที่กว้างขวาง และสิ่งอำนวยความสะดวกอย่างมืออาชีพที่ห้องแวนด้า คุณสามารถเป็นเจ้าภาพจัดกิจกรรมและการประชุมที่หลากหลายได้ที่รีสอร์ทของเรา</p>
                                    
                                    <h2 class="sub-title">ห้องแวนด้า</h2>
                                    <p>"ห้องประชุมแวนด้าตั้งอยู่บริเวณชั้นล่าง ท่านสามารถมาที่ห้องแวนด้าได้โดยผ่านล็อบบี้จากดีลักซ์วิงและลานจอดรถ
"</p>
                                    
                                    <ul class="list-description">
                                        <li><span class="label">ขนาด :   7.70 x 14.40 เมตร (110 ตารางเมตร)</li>
                                        <li><span class="label">ความสูงถึงเพดาน : 2.60 เมตร</li>
                                    </ul>
                                    
                                    <table style="margin: 0;" width="100%">
                                        <thead>
                                            <tr>
                                                <th>ติดตั้ง</th>
                                                <th>โรงภาพยนตร์</th>
                                                <th>ห้องเรียน</th>
                                                <th>ห้องประชุมรูปตัวยู</th>
                                                <th>กระดาน</th>
                                                <th>บุฟเฟ่ต์</th>
                                                <th>ค็อกเทล</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            <tr>
                                                <th>จำนวนคน</th>
                                                <td>60</td>
                                                <td>40</td>
                                                <td>20</td>
                                                <td>10</td>
                                                <td>50</td>
                                                <td>80</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <h2 class="sub-title">แสง</h2>
                                    <p>มีตัวควบคุม/สิ่งอำนวยความสะดวกที่สามารถปรับแสงให้บริการ เครื่องควบคุมการหรี่แสงตั้งอยู่ที่ห้องประชุมวันดา</p>
                                    
                                    <h2 class="sub-title">เสียง-อุปกรณ์ภาพและเสียง</h2>
                                    <p>
                                        โต๊ะ, ขาตั้งแบบปรับได้ และ ไมโครโฟนไร้สาย<br>
                                        ระบบพีเอพร้อมด้วยลำโพงแบบพกพา<br>
                                        ทีวีระบบมัลติซิสเต็ม<br>
                                        เครื่องเล่นดีวีดี,ซีดี<br>
                                        มีจอแอลซีดีโปรเจ็กเตอร์สำหรับเช่า
                                    </p>
                                    
                                    <h2 class="sub-title">การนำเสนอในที่ประชุม</h2>
                                    <p>กระดานไวท์บอร์ด, ฟลิปชาร์ท, กระดานข้อความ, หน้าจอโปรเจคเตอร์</p>
                                    
                                    <h2 class="sub-title">ระบบอินเตอร์เน็ตไร้สาย</h2>
                                    <p>อินเตอร์เน็ตผ่านสายโทรศัพท์สามารถใช้ได้ฟรีเมื่อเชื่อมต่อกับเซิร์ฟเวอร์ในจังหวัดภูเก็ต<br>
                                        อินเตอร์เน็ตที่เชื่อมต่อกับเซิร์ฟเวอร์ในพื้นที่อื่นจะคิดค่าบริการตามอัตราของพื้นที่โทรศัพท์นั้นๆ</p>
                                    
                                    <h2 class="sub-title">เจ้าหน้าที่ให้บริการในห้องประชุม</h2>
                                    <p>
                                        เจ้าหน้าที่ส่งเอกสาร<br>
                                        ช่างไฟฟ้า, คนงาน, ช่างซ่อม <br>
                                        ผู้ประกอบการด้านอุปกรณ์ภาพและเสียง<br>
                                        เจ้าหน้าที่อาวุโสและผู้บริหารของเราจะอยู่เพื่อให้แน่ใจว่างานของท่านจะประสบความสำเร็จ
                                    </p>
                                    
                                    <h2 class="sub-title">อุปกรณ์สำหรับการจัดงาน</h2>
                                    <p>(ตามใบเสนอราคา) : เครื่องฉายสไลด์, เครื่องฉายแผ่นใส, จอแอลซีดีโปรเจ็กเตอร์, คอมพิวเตอร์, เครื่องพิมพ์, คอมพิวเตอร์และเครื่องพิมพ์, ช่างภาพ และ เครื่องแฟกซ์</p>
                                    
                                    <h2 class="sub-title">การสร้างทีมงาน (Team Building)</h2>
                                    <p>บริษัทและองค์กรสามารถใช้ประโยชน์จากสิ่งอำนวยความสะดวกด้านการประชุมของโรงแรมดีวาน่า รีสอร์ทแอนด์สปา นำมาเป็นส่วนหนึ่งในการจัดกิจกรรมการสร้างทีมงาน (Team Building) ผู้เชี่ยวชาญด้านการจัดงานกิจกรรมของรีสอร์ทสามารถสร้างวันที่สนุกสนานและกิจกรรมที่น่าจดจำ เพื่อส่งเสริมการทำงานเป็นทีมและเพิ่มความเชื่อใจ เพื่อสร้างความสัมพันธ์ที่ดีในการทำงาน ห้องประชุมแวนด้าซี่งมีสิ่งอำนวยความสะดวกที่ทันสมัยและบริเวณสนามในรีสอร์ทคือสถานที่สำหรับการจัดกิจกรรมการสร้างทีมงาน (Team Building) สำหรับข้อมูลเพิ่มเติม กรุณาติดต่อทีมงานจัดกิจกรรมของเราที่ sales@deevanapatong.com</p>
                                    
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="wedding" class="article" data-tab-name="Wedding">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/wedding/wedding-01.jpg" alt="wedding"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">งานแต่งงาน</h1>
                                    <p>เรามีนักวางแผนงานแต่งงานผู้มีประสบการณ์ของโรงแรมดีวาน่า รีสอร์ทแอนด์สปาจะสร้างสรรค์วันที่สวยงาม เพื่อให้คู่รักและแขกผู้มาร่วมแสดงความยินดีได้เก็บวันนี้ไว้เป็นความทรงจำที่งดงามตลอดไป บริเวณสนามของรีสอร์ทที่สวยงามและกว้างขวางจะเป็นฉากหลังที่สมบูรณ์แบบสำหรับงานแต่งงานที่แสนพิเศษ ด้วยงานเลี้ยงอาหารเย็นที่สวยงามทันสมัย และส่วนต้อนรับที่สามารถให้ความบันเทิงแก่แขกผู้ร่วมงานทั้งกลุ่มเล็กและกลุ่มใหญ่ ทีมงานที่ดีว่าน่าสามารถจัดงานแต่งงานทั้งในรูปแบบงานแต่งงานแบบพิธีไทย ซึ่งรวมถึงการสวดมนต์ให้พรจากพระสงฆ์ หรืองานแต่งงานแบบตะวันตกที่มีทั้งครอบครัวและเพื่อนฝูง สำหรับข้อมูลเพิ่มเติม กรุณาติดต่อทีมงานของเราที่ sales@deevanapatong.com</p>
                                </div>
                            </div>
                        </div>
                    </article>

                    <article id="cooking_class" class="article" data-tab-name="Thai Cooking Class">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/cooking_class/cooking_class-01.jpg" alt="Thai Cooking Class"/>
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">เรียนทำอาหารไทย</h1>
                                    <p>เรียนรู้เคล็ดลับรสชาติความอร่อยของอาหารไทยโดยเชฟผู้เชี่ยวชาญและมีประสบการณ์กว่า 10 ปี ท่านจะได้รับการฝึกฝนและเรียนรู้สูตรการปรุงอาหารของเมนูอาหารไทยที่เป็นที่ชื่นชอบ เพลิดเพลินกับความพยายามในการทำอาหารของคุณด้วยการชิมฝีมือการปรุงของคุณหลังจบบทเรียนสำหรับมื้อเที่ยงหรือมื้อเย็น</p>
                                </div>
                            </div>
                        </div>
                    </article>
                                        
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/spa/spa-01.jpg" alt="Orientala Wellness Spa" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">โอเรียลทาล่า สปา</h1>
                                    <p>บำบัดความรู้สึกของคุณสู่ความรู้สึกใหม่ที่แสนรื่นรมย์ที่โอเรียลทาล่า สปา นักบำบัดที่ถูกฝึกสอนมาอย่างดีของเราจะนำท่านเดินทางสู่การพักผ่อนและฟิ้นฟู โดยการใช้ทั้งเทคนิคโบราณและทันสมัย การบำบัดจะบรรเทาความเครียดต่างๆให้จางหายไป ให้คุณรู้สึกดีอย่างน่าเหลือเชื่อ ห้องทรีทเมนต์ของเราเต็มไปด้วยอุปกรณ์อย่างดีมากมายและตกแต่งอย่างมีรสนิยมด้วยสระว่ายน้ำธรรมชาติที่จะทำให้บรรยากาศผ่อนคลาย คู่รักสามารถเพลิดเพลินไปกับห้องส่วนตัวที่แสนโรแมนติก พร้อมด้วยเตียงสำหรับทำทรีทเมนต์สองเตียงเพื่อที่ท่านจะได้มีประสบการณ์ที่ยอดเยี่ยมด้วยกัน</p>
                                    <p>ดื่มด่ำไปกับแพคเกจพิเศษถึง 180 นาทีของการทรีทเมนต์ที่แสนสุขของเรา หรือเลือกจากเมนูตามสั่งที่มีความหลากหลายของการนวด ทั้งนวดไทยโบราณ, สวีเดน, เวชกรรมไทย, นวดอโรมาและอื่นๆ ลองการทำความสะอาดผิวหน้า, การพอกตัวเพื่อล้างสารพิษ, ขัดผิวหรือบอดี้แรพ นอกจากนี้ท่านยังสามารถใช้สอยสิ่งอำนวยความสะดวกอื่นๆ ทั้งห้องอบไอน้ำ, อ่างจากุซซี่, ร้านเสริมสวยและห้องฟิตเนสในบริเวณใกล้เคียงได้อีกด้วย</p>
                                    <h2 class="sub-header">Orientala Spa Facilities</h2>
                                <ul>
                                    <li>5 Treatment rooms (3 single and 2 twin rooms with Jacuzzi)</li>
                                    <li>Ladies and Gents private lockers and steamed bath</li>
                                    <li>2 Facial treatment stations</li>
                                    <li>Orientala Wellness Spa Menu (<a style="color:#ff0000; font-weight:bold" target="_blank" href="http://www.deevanapatong.com/download/Spa-Menu.pdf">Download</a>)</li>
                                  </ul><a style="margin-bottom: 2rem;display: block;" href="mailto:info.clusterphuket@deevanahotels.com" target="_blank" rel="noopener">For more information please contact</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #24467b;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #78a321;
    }
    .col-cap .sub-title {
        color: #236198;
        font-size: 14px;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        margin-top: 1em;
        margin-bottom: 0;
    }
    .col-cap .sub-title + p {
        margin-top: 3px;
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>