<?php
$title = 'Junior Suite with Jacuzzi | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Junior Suite with Jacuzzi: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'junior suite with jacuzzi, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-junior-jacuzzi';
$cur_page = 'junior-with-jacuzzi';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-junior-with-jacuzzi.php';
$lang_th = '/th/deevanapatong/room-junior-with-jacuzzi.php';
$lang_zh = '/zh/deevanapatong/room-junior-with-jacuzzi.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-01.jpg" alt="Junior Suite with Jacuzzi 01" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-02.jpg" alt="Junior Suite with Jacuzzi 02" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-03.jpg" alt="Junior Suite with Jacuzzi 03" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-04.jpg" alt="Junior Suite with Jacuzzi 04" />
                    <img src="images/accommodations/junior_jacuzzi/1500/junior-jacuzzi-05.jpg" alt="Junior Suite with Jacuzzi 05" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องจูเนียร์สวีทจากุซซี่ <span>เตียงคิงไซส์หรือเตียงคู่</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-03.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-04.jpg" height="50" /></li>
                    <li><img src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-05.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องจูเนียร์สวีทจากุซซี่</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/junior_jacuzzi/600/junior-jacuzzi-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ดื่มด่ำไปกับห้องจูเนียร์สวีทจากุซซี่ที่สมบูรณ์แบบด้วยอ่างจากุซซี่ส่วนตัว ห้องสวีทขนาด 55 ตารางเมตรเป็นตัวเลือกที่ดีสำหรับครอบครัวได้พักผ่อนด้วยกันหรือคู่รักที่มองหาความหรูหราเล็กๆน้อยๆสำหรับวันหยุด ห้องพักที่กว้างขวางด้วยเตียงใหญ่ขนาด 7 ฟุต ตู้เย็น อุปกรณ์สำหรับชงชาและกาแฟ เตียงสำหรับนอนกลางวัน ห้องนั่งเล่นพร้อมด้วยทีวีแอลซีดีขนาด 42 นิ้ว ที่นั่งกลางแจ้ง โต๊ะเขียนหนังสือ/โต๊ะเครื่องแป้ง และห้องน้ำขนาดใหญ่พร้อมด้วยอ่างอาบน้ำและฝักบัวอาบน้ำ สามารถขอเตียงเสริมเมื่อเข้าพักรวมสี่ท่าน</p>
                            <p class="note">
                                <strong>หมายเหตุ</strong><br>
                                ผ้าเช็ดตัวสำหรับชายหาดจะมีให้บริการบริเวณโต๊ะให้บริการ<br>
                                ผ้าเช็ดตัวสำหรับสระน้ำจะมีให้บริการบริเวณสระน้ำ
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                   <h2 class="title">สิ่งอำนวยความสะดวกในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                        <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                        <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                        <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                        <li>ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>All Amenities</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                            <li>อ่างอาบน้ำ</li>
                                            <li>สิ่งอำนวยความสะดวกภายในห้องน้ำ : เจลสำหรับอาบน้ำและสระผม, หมวกอาบน้ำ, ผ้าเช็ดตัว, สำลีก้าน (คอตตอนบัด)</li>
                                            <li>อุปกรณ์เย็บผ้า</li>
                                            <li>รองเท้าแตะ</li>
                                            <li>ร่ม</li>
                                            <li>ถุงชายหาด</li>
                                            <li>ชั้นวางกระเป๋าเดินทาง</li>
                                            <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                            <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                            <li>ตู้เย็นและฟรีน้ำดื่ม 2 ขวดต่อวัน</li>
                                            <li>ห้องน้ำส่วนตัว พร้อมด้วยฝักบัวน้ำอุ่นและน้ำเย็น</li>
                                            <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                            <li>วิวสระน้ำจากระเบียง (ดีลักซ์วิง)</li>
                                            <li>อุปกรณ์สำหรับชงชา/กาแฟ</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้นิรภัยในห้องพัก</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                            <li>ห้องพักที่เชื่อมต่อกันได้</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>