<?php
$title = 'Gallery | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Gallery: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'gallery, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$title = 'Gallery';
$body_class = 'gallery';
$cur_page = 'gallery';

$lang_en = '/deevanapatong/gallery.php';
$lang_th = '/th/deevanapatong/gallery.php';
$lang_zh = '/zh/deevanapatong/gallery.php';

include_once( '_header.php' );
?>

<main class="site-main no-head">
    <section class="site-content">
        <div class="container">
            <section class="section section-gallery">
                
                <div class="gal-types">
                    <span class="tab active" data-tab="#photo"><i class="icon fa fa-picture-o"></i>PHOTOS</span>
                    <!--<span class="tab" data-tab="#video"><i class="icon fa fa-film"></i>VIDEO</span>-->
                </div>
                
                <div class="gal-conts">
                    <section id="photo" class="gal-cont">
                        <div class="cat-tabs">
                            <span class="tab active" data-cat="*">ALL</span>
                            <span class="tab" data-cat=".rooms">ROOMS</span>
                            <span class="tab" data-cat=".restaurant">RESTAURANT</span>
                            <span class="tab" data-cat=".spa">SPA</span>
                            <span class="tab" data-cat=".facilities">FACILITIES</span>
                        </div>
                        
                        <div class="content">
                            <div class="masonry-gal">
                            <?php 
								//room
								for ($i = 1; $i <= 31; $i++) {
							?>
                                <div class="item rooms"><a href="images/gallery/room/1500/room_<?php echo $i;?>.jpg"><img src="images/gallery/room/600/room_<?php echo $i;?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></a></div>
                            <?php }?>

                            <?php 
								//facilities
								for ($i = 1; $i <= 24; $i++) {
							?>
                                <div class="item facilities"><a href="images/gallery/facilities/1500/facility_<?php echo $i;?>.jpg"><img src="images/gallery/facilities/600/facility_<?php echo $i;?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></a></div>
							<?php }?>
                            
							<?php 
								//spa
								for ($i = 1; $i <= 13; $i++) {
							?>
                                <div class="item spa"><a href="images/gallery/spa/1500/spa_<?php echo $i;?>.jpg"><img src="images/gallery/spa/600/spa_<?php echo $i;?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></a></div>
							<?php }?>

							<?php 
								//restaurant
								for ($i = 1; $i <= 17; $i++) {
							?>                                                        
                                <div class="item restaurant"><a href="images/gallery/restaurant/1500/restaurant_<?php echo $i;?>.jpg"><img src="images/gallery/restaurant/600/restaurant_<?php echo $i;?>.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></a></div>
                             <?php }?>   
                            </div>
                        </div>
                    </section>
                    
					<!--
                    <section id="video" class="gal-cont">
                        <div class="content">
                            Video
                        </div>
                    </section>
					-->
                </div>
                
            </section>
        </div>
    </section>
    
    <?php include_once('include/booking_bar.php') ?>
</main>

<!--Isotopr-->
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.min.js"></script>
<script>
    $(function() {
        var $gCont = $('.gal-conts');
        var $gType = $('.gal-types');
        var $ctabs = $('.cat-tabs');
        
        function masonryFX( cat = '*' ) {
            var $mgal = $('.masonry-gal');
            $mgal.isotope({
                itemSelector: '.item',
                filter: cat,
            }).imagesLoaded().progress( function() {
                $mgal.isotope('layout');
            });
        }
        
        function galleryFX( cat = '.item' ) {
            $gCont.find(cat).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    preload: [1, 2],
                },
                callbacks: {
                    open: function() {
                        $('html').addClass('gallery-open');
                    },
                    close: function() {
                        $('html').removeClass('gallery-open');
                    },
                },
                removalDelay: 300,
                mainClass: 'mfp-fade',
            });
        }
        
        // Define active tab
        var active = $gType.find('.active').data('tab');
        $gCont.find( active ).show();
        
        $gType.on('click', '.tab', function(e) {
            e.preventDefault();
			if( $(this).hasClass('active') ) return;
			
            var $this = $(this);
            var data = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $gCont.find(data).fadeIn(300).siblings().hide();
            
            masonryFX();
        });
        
        $ctabs.on('click', '.tab', function(e) {
            var $this = $(this);
            var cat = $this.data('cat');
            $this.addClass('active').siblings().removeClass('active');
            
            galleryFX(cat);
            masonryFX(cat);
        });
        
        galleryFX();
        masonryFX();
    });
</script>

<style>
    .site-main {
        background-image: url(images/gallery/bg-gallery.jpg);
        background-attachment: fixed;
        background-position: center;
        background-size: cover;
    }
    .gal-conts {
        width: 100%;
        clear: both;
        background-color: #fff;
        margin-bottom: 30px;
        border-top: 2px solid #1a355e;
        position: relative;
    }
    .gal-cont {
        display: none;
        position: relative;
    }
    .gal-cont .content {
        overflow: hidden;
        padding: 10px 10px 0;
    }
    .gal-types,
    .cat-tabs {
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: pointer;
    }
    .gal-types {
        float: right;
    }
    .gal-types .tab {
        background-color: #fff;
        color: #333;
        border-radius: 3px 3px 0 0;
        display: inline-block;
        text-align: center;
        padding-top: 10px;
        width: 70px;
        height: 50px;
    }
    .gal-types .tab.active {
        background-color: #1A355E;
        color: #fff;
    }
    .gal-types .tab .icon {
        display: block;
    }
    .cat-tabs {
        position: absolute;
        top: -30px;
        left: 0;
    }
    .cat-tabs .tab {
        background-color: transparent;
        color: #333;
        line-height: 28px;
        font-size: 16px;
        padding: 0 8px;
        display: inline-block;
        border-radius: 3px 3px 0 0;
    }
    .cat-tabs .tab.active {
        background-color: #1a355e;
        color: #fff;
    }
    .masonry-gal {
        margin-left: -5px;
        margin-right: -5px;
    }
    .masonry-gal .item {
        width: 33.33%;
        margin-bottom: 10px;
        padding: 0 5px;
    }
    .masonry-gal .item img {
        display: block;
        width: 100%;
        height: auto;
    }
    #video {
        padding-bottom: 10px;
    }
    
    html .site-header {
        transition-property: background-color, height, right;
        transition-duration: 300ms, 300ms, 0ms;
    }
    html.gallery-open .site-header {
        right: 17px;
    }
    @media (max-width: 768px) {
        .section-gallery {
            padding-top: 0;
        }
        .masonry-gal .item {
            width: 50%;
        }
        .gal-conts {
            margin-top: 42px;
        }
        .gal-types {
            float: none;
            border-radius: 3px;
            overflow: hidden;
            margin-bottom: 10px;
        }
        .gal-types:after {
            content: '';
            display: block;
            clear: both;
        }
        .gal-types .tab {
            width: 50%;
            float: left;
            padding: 6px;
            height: auto;
            border-radius: 0;
        }
        .gal-types .tab .icon {
            display: inline-block;
            margin-right: 3px;
        }
        .cat-tabs {
            overflow-x: auto;
            display: flex;
            flex-flow: row nowrap;
            background-color: #fff;
            top: -34px;
            left: 0;
            right: 0;
            border-radius: 3px 3px 0 0;
        }
        .cat-tabs .tab {
            border-radius: 0;
            font-size: 12px;
            line-height: 32px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>