<?php
$title = 'Attractions | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Attractions: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'attractions, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'attraction';
$cur_page = 'attraction';

$lang_en = '/deevanapatong/attraction.php';
$lang_th = '/th/deevanapatong/attraction.php';
$lang_zh = '/zh/deevanapatong/attraction.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/attraction/hero_slide_01.jpg" alt="Deevana Patong Resort &amp; SpaPhuket, 4-star hotel" /></div>
        </div>
        
        <div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content">
        <div class="container">
            <div class="row">
                <div class="col-w3 col-sidebar">
                    <div class="tabs-nav">
                        <ul>
                            <li class="tab active" data-tab="#patong_beach">หาดป่าตอง</li>
                            <li class="tab" data-tab="#phromthep_cape">แหลมพรหมเทพ</li>
                            <li class="tab" data-tab="#kata_and_karon_beaches">หากกะตะและหาดกะรน</li>
                            <li class="tab" data-tab="#big_buddha">วัดพระใหญ่</li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-w9 col-content tabs-content">
                    <article class="article" id="patong_beach" data-tab-name="Patong Beach">
                        <img class="thumbnail force" src="images/attraction/patong_beach.png" alt="Patong Beach" />
                        <h1 class="title">หาดป่าตอง</h1>
                        <p>หาดป่าตองเป็นชายหาดที่มีชื่อเสียงที่สุดบนเกาะภูเก็ต เนื่องจากชายหาดแห่งนี้มีกิจกรรมและสถานบันเทิงยามค่ำคืนที่หลากหลาย ในยามค่ำคืนเมืองแห่งนี้จะเต็มไปด้วยสถานบันเทิงที่คึกคัก ซึ่งรวมถึงร้านอาหารนับร้อย, บาร์เบียร์ และดิสโก้ </p>
                    </article>
                    
                    <article class="article" id="phromthep_cape" data-tab-name="Phromthep Cape">
                        <img class="thumbnail force" src="images/attraction/promthep_cape.png" alt="Phromthep Cape" />
                        <h1 class="title">แหลมพรหมเทพ</h1>
                        <p>แหลมพรหมเทพตั้งอยู่บนยอดเขาซึ่งอยู่บริเวณส่วนปลายสุดของเกาะ เป็นจุดชมวิวที่สวยงาม เหมาะสำหรับชมพระอาทิตย์ตก พระอาทิตย์สีส้มที่กำลังตกจากท้องฟ้าสีครามเป็นสิ่งที่ชวนให้น่าหลงใหลอย่างไม่ต้องสงสัย</p>
                    </article>
                    
                    <article class="article" id="kata_and_karon_beaches" data-tab-name="Kata and Karon Beaches">
                        <img class="thumbnail force" src="images/attraction/karon_kata_beach.png" alt="Kata and Karon Beaches" />
                        <h1 class="title">หากกะตะและหาดกะรน</h1>
                        <p>หาดกะตะและหาดกะรน เป็นที่รู้กันดีว่าเป็นชายหาดที่อบอุ่นที่สุด, น้ำทะเลใสที่สุด และเหมาะสำหรับการพักผ่อนเป็นครอบครัวมากที่สุดบนเกาะภูเก็ต ชายหาดเหล่านี้มีชื่อเสียงในหมู่ครอบครัวที่เลือกที่จะเพลิดเพลินไปกับกิจกรรมบนหาดทรายที่ทอดตัวเป็นแนวยาวอยู่ภายใต้ความอบอุ่นของแสงจากพระอาทิตย์ ในช่วงระหว่างเดือนพฤษภาคมถึงเดือนตุลาคม ผู้ที่ชื่นชอบการเล่นกระดานโต้คลื่นจะแห่กันมาที่นี่เพื่อเล่นกระดานโต้คลื่น ในระหว่างที่ในเดือนพฤศจิกายนถึงเดือนเมษายน ชายหาดจะเต็มไปด้วยผู้ที่มาเสาะหาแสงแดดอันอบอุ่น</p>
                    </article>
                    
                    <article class="article" id="big_buddha" data-tab-name="Big Buddha">
                        <img class="thumbnail force" src="images/attraction/big_buddha_phuket.png" alt="Big Budhha" />
                        <h1 class="title">วัดพระใหญ่</h1>
                        <p>พระใหญ่ของเกาะภูเก็ตเป็นสถานที่ที่ได้รับความเคารพนับถือและสำคัญแห่งหนึ่งของเกาะ พระพุทธรูปขนาดใหญ่ตั้งอยู่บนยอดเขานากเกิดซึ่งอยู่ระหว่างอ่าวฉลองและหาดกะตะ มีความสูงถึง 45 เมตร จึงสามารถมองเห็นมาจากสถานที่ที่อยู่ห่างออกไปได้อย่างง่ายดาย</p>
                        <p>เนื่องจากตั้งอยู่บนทำเลที่สูง จึงทำให้เราสามารถมองเห็นวิวแบบ 360 องศาที่ดีที่สุดในเกาะภูเก็ต (คุณสามารถมองเห็นทิวทัศน์อันกว้างไกลของเมืองภูเก็ต หาดกะตะ หาดกะรน อ่าวฉลองและอื่นๆ) การเดินทางไปยังวัดพระใหญ่นั้นสะดวกสบาย เนื่องจากสถานที่แห่งนี้ห่างจากถนนสายหลักของเกาะภูเก็ตเพียง 6 กิโลเมตรเท่านั้น และยังเป็นหนึ่งในสถานที่ที่ต้องไปของเกาะภูเก็ตอีกด้วย</p>
                        <p>บริเวณโดยรอบพระพุทธรูปเป็นสถานที่ที่เงียบสงบ ซึ่งคุณจะได้ยินเพียงเสียงจากการสั่นกระดิ่งเล็กๆ และเสียงจากการไหวต้องลมของธงสีเหลืองซึ่งเป็นสัญลักษ์ของศาสนาพุทธ พร้อมด้วยเสียงเพลงบทสวดมนต์แบบพระพุทธศาสนาเบาๆ</p>
                        <p>โดยชื่อเรียกเต็มๆซึ่งเป็นที่รู้จักกันในหมู่ชาวไทยคือพระพุทธมิ่งมงคลเอกนาคคีรี โดยมีฐานกว้างถึง 25 เมตร องค์พระพุทธรูปทำจากหินอ่อนสีขาวที่สวยงามจากพม่า เรียงกันเป็นชั้นๆ ซึ่งเมื่อตกกระทบกับแสงจากดวงอาทิตย์ก็จะส่องประกายราวกับว่าเป็นสัญลักษณ์แห่งความหวัง ทิวทัศน์รวมถึงตัวพระพุทธรูปเองนั้นก็สวยงามและน่าทึ่งมากเช่นกัน </p>
                    </article>
                </div>
            </div>
        </div>
    </section>
        
</main>

<script>
    $(function() {
        var $ts = $('.tabs-nav');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
			
			var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(hash).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var offset = ($(window).width() > 1070) ? 195 : 20;
            var targetPos = $(t).offset().top - offset;
            scrollTo( 0, Math.round(targetPos) );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .site-content {
        padding: 60px 0;
    }
    .article .thumbnail {
        margin-bottom: 20px;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .tabs-nav .tab {
        position: relative;
        border-top: 1px solid #ccc;
        padding: 5px 0;
        cursor: pointer;
        padding-right: 15px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab:after {
        content: '\f105';
        font-family: 'FontAwesome';
        line-height: 1;
        position: absolute;
        top: 50%;
        right: 0;
        font-size: 14px;
        margin-top: -7px;
    }
    .tabs-nav .tab.active {
        color: #1A355E;
    }
    .tabs-nav .tab:last-child {
        border-bottom: 1px solid #ccc;
    }
    @media (max-width: 1070px) {
        .site-content {
            padding: 20px 0;
        }
    }
    @media (max-width: 740px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
            width: auto;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            margin-left: -15px;
            margin-right: -15px;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>