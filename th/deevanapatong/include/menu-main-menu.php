<ul class="menu list-menu level-0">
    <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
        <a href="#">ห้องพัก</a>
        <ul class="sub-menu level-1">
            <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a class="label" href="#">การ์เด้นวิง</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('superior-garden'); ?>"><a href="room-superior-garden.php">ห้องซูพีเรียการ์เด้น</a></li>
                </ul>
            </li>
            <li class="has-sub-menu <?php echo get_current_class('rooms'); ?>">
                <a class="label" href="#">ดีลักซ์วิง</a>
                <ul class="sub-menu level-2">
                    <li class="<?php echo get_current_class('deluxe'); ?>"><a href="room-deluxe.php">ห้องดีลักซ์</a></li>
                    <li class="<?php echo get_current_class('deluxe-with-jacuzzi'); ?>"><a href="room-deluxe-with-jacuzzi.php">ห้องดีลักซ์จากุซซี่</a></li>
                    <li class="<?php echo get_current_class('junior-suite'); ?>"><a href="room-junior-suite.php">ห้องจูเนียร์สวีท</a></li>
                    <li class="<?php echo get_current_class('junior-with-jacuzzi'); ?>"><a href="room-junior-with-jacuzzi.php">ห้องจูเนียร์สวีทจากุซซี่</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="<?php echo get_current_class('spa'); ?>"><a href="facilities.php#orientala_wellness_spa">สปา</a></li>
    <li class="<?php echo get_current_class('facilities'); ?>"><a href="facilities.php">สิ่งอำนวยความสะดวก</a></li>
    <li class="<?php echo get_current_class('attraction'); ?>"><a href="attraction.php">สถานที่ท่องเที่ยว</a></li>
    <li class="<?php echo get_current_class('offers'); ?>"><a href="offers.php">โปรโมชั่น</a></li>
    <li class="<?php echo get_current_class('gallery'); ?>"><a href="gallery.php">รูปภาพ</a></li>
    <li class="<?php echo get_current_class('contact'); ?>"><a href="contact.php">ติดต่อเรา</a></li>
</ul>
