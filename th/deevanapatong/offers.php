<?php
$title = 'Special Offers | Deevana Patong Resort & Spa Phuket';
$desc = 'Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'special offers, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'offers';
$cur_page = 'offers';

$lang_en = '/deevanapatong/offers.php';
$lang_th = '/th/deevanapatong/offers.php';
$lang_zh = '';

include_once('_header.php');
?>
<style>
    .site-content > .container {
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .section-header {
        text-align: center;
        padding-top: 40px;
        padding-bottom: 40px;
    }
</style>

<style>
    :root {
    	--accent-color: #027BC0;
    	--accent-color-darker: hsl(217, 55%, 19%);
    }
    .offer-item {
    	background-color: #fff;
    	border-radius: .25rem;
    	overflow: hidden;
    	border: 1px solid #ddd;
    	padding: 20px 25px;
    	margin-bottom: 30px;
    }
    .offer-item figure {
    	margin: -20px -25px 20px;
    }
    .offer-item p,
    .offer-item ul {
    	margin: 0 0 1rem;
    }
    .offer-item__heading {
    	font-size: 1.5rem;
    	font-weight: 700;
    	margin: 0 0 .5rem;
    	color: var(--accent-color);
    }
    .offer-item__content {
    	font-size: 0.875rem;
    	font-weight: 300;
    }
    .offer-item__content>*:last-child {
    	margin-bottom: 0;
    }
    .offer-item__cta {
    	margin-top: 1rem;
    }
    .offer-item a {
    	color: var(--accent-color);
    }
    .offer-item a:hover,
    .offer-item a:active {
    	color: var(--accent-color-darker);
    }
    .offer-item .btn-cta {
    	background-color: var(--accent-color);
    	color: #fff;
    	transition: .2s ease-in-out;
    	display: block;
    	padding: 0.375rem 0.75rem 0.425rem;
    	text-decoration: none;
    	text-align: center;
    	text-transform: uppercase;
    	border-radius: 0.25rem;
    }
    .offer-item .btn-cta:hover,
    .offer-item .btn-cta:active {
    	background-color: var(--accent-color-darker);
    	color: #fff;
    }
</style>

<main class="site-main">
    <?php include_once 'include/booking_bar.php'; ?>

    <section class="site-content">
        <div class="container">
            <div class="section-header">
                <h1>โปรโมชัน</h1>
            </div>

            <div class="row" id="offer_query">
                <div class="text-cneter">Loading...</div>
            </div>
        </div>
    </section>
</main>

<script src="https://cdn.jsdelivr.net/npm/template7@1.4.2/dist/template7.min.js" integrity="sha256-AR7mrm2lYY5i4wWxxAznbMCTVOCMD5kvqLUmtW/6iCY=" crossorigin="anonymous"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/object-fit-images/3.2.3/ofi.min.js'></script>
<script id="offer_template" type="text/template7">
    {{#each posts}}
    	<div class="col-12 col-md-6 col-xl-4 mb-4">
    		<div class="offer-item">
    			{{#if image}}
    				<figure class="m-0">
    					{{img className="w-100 h-auto d-block" image}}
    				</figure>
    			{{/if}}
    			<div class="p-4">
    				<h2 class="offer-item__heading">{{title_th}}</h2>
    				<div class="offer-item__content">
    					{{content_th}}
    				</div>
    				{{#if button_th.enable}}
    					<div class="offer-item__cta">
    						<a class="btn-cta" href="{{button_th.link_url}}" target="{{button_th.link_target}}">{{button_th.link_text}}</a>
    					</div>
    				{{/if}}
    			</div>
    		</div>
    	</div>
    {{/each}}
</script>
<script>
    (function($) {
        var origin = 'https://webdemo2.travelanium.net/deevana/cockpit';
        var token = '3fa7cf86aa69b4028f4d0adbe6c735';
        var collection = 'deevanaPatongOffers';
        var path = '%base%/api/collections/get/%collection%?token=%token%&rspc=1'
            .replace('%base%',origin)
            .replace('%collection%',collection)
            .replace('%token%',token);

        var data = $.getJSON(path);
        Template7.registerHelper('img', function(image, options) {
            var imgApi = origin + '/api/cockpit/image';
            var imgPath = origin + '/storage/uploads' + image.path;
            var url = '%base%?token=%token%&src=%path%&rspc=1&w=600&m=bestFit&q=82&o=true'
                .replace('%base%', imgApi)
                .replace('%token%', token)
                .replace('%path%', imgPath);

            return '<img class="%className%" src="%src%" loading="lazy" />'
                .replace('%className%', options.hash.className)
                .replace('%src%', url);
        });

        data.then(function(res) {
            var template = $('#offer_template').html();
            var items = res.entries.filter(function(item) {
                return item.published
            });
            if (items.length) {
                var compiled = Template7.compile(template);
                var html = compiled({
                    posts: items,
                });
                $('#offer_query').html(html);
            }
        });
    })(jQuery);
</script>
<?php include '_footer.php'; ?>