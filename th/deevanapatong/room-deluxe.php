<?php
$title = 'Deluxe Room | Deevana Patong Resort & Spa Phuket | Official Hotel Group Website Thailand';
$desc = 'Deluxe Room: Guarantee best direct hotel rate starting from USD 50 per night; 4-star resort near patong beach, phuket';
$keyw = 'deluxe room, deevana, deevana patong resort, deevana phuket, patong beach, 4-star hotel, beach hotel, phuket, hotel patong beach';

$html_class = '';
$body_class = 'room room-deluxe';
$cur_page = 'deluxe';
$par_page = 'rooms';

$lang_en = '/deevanapatong/room-deluxe.php';
$lang_th = '/th/deevanapatong/room-deluxe.php';
$lang_zh = '/zh/deevanapatong/room-deluxe.php';

include_once('_header.php');
?>

<main class="site-main">
    <div class="room-content">
        
        <div class="room-slides-wrap disable-touch">
            <div class="room-slides">
                <div class="slides-container">
                    <img src="images/accommodations/deluxe/1500/deluxe-01.jpg" alt="Deluxe Room 01" />
                    <img src="images/accommodations/deluxe/1500/deluxe-02.jpg" alt="Deluxe Room 02" />
                    <img src="images/accommodations/deluxe/1500/deluxe-03.jpg" alt="Deluxe Room 03" />
                </div>
            </div>
        </div>
        
        <div class="room-slides-thumbs">
            <h2 class="title">ห้องดีลักซ์ <span>เตียงคิงไซส์</span></h2>
            <div class="thumbs">
                <ul>
                    <li class="current"><img src="images/accommodations/deluxe/600/deluxe-01.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-02.jpg" height="50" /></li>
                    <li><img src="images/accommodations/deluxe/600/deluxe-03.jpg" height="50" /></li>
                </ul>
            </div>
            <span id="toggle_content" class="toggle clickable"><i class="icon fa fa-angle-up"></i></span>
        </div>

        <div class="room-info">
            <div class="table">
                <div class="content table-cell">
                    <h1 class="title">ห้องดีลักซ์</h1>
                    
                    <div class="row">
                        <div class="col-w3 col-thumbnail">
                            <img class="room-preview force" src="images/accommodations/deluxe/600/deluxe-01.jpg" />
                            
                            <a class="button clickable book-this-room-button desktop" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w5 col-info">
                            <p>ห้องพักมีขนาด 36 ตารางเมตร เหมาะสมสำหรับคู่รักหรือเพื่อนที่มาพักผ่อนร่วมกัน ตกแต่งด้วยสถาปัตยกรรมเอเชียแบบร่วมสมัย มีทั้งเตียงเดี่ยวหรือเตียงคู่ ภายในห้องพักประกอบไปด้วยโต๊ะกาแฟ ทีวีจอแบนพร้อมด้วยช่องต่างประเทศ และห้องน้ำในตัวพร้อมหน้าต่างบานเลื่อนขนาดใหญ่ ระเบียงส่วนตัวที่ให้ท่านได้ชื่นชมกับทิวทัศน์ที่สวยงามเหนือสระว่ายน้ำ</p>
                            <p class="note">
                                <strong>หมายเหตุ</strong><br>
                                ผ้าเช็ดตัวสำหรับชายหาดจะมีให้บริการบริเวณโต๊ะให้บริการ<br>
                                ผ้าเช็ดตัวสำหรับสระน้ำจะมีให้บริการบริเวณสระน้ำ
                            </p>
                            <a class="button clickable book-this-room-button mobile" href="<?php ibe_url(277, 'en'); ?>" target="_blank">จองห้องพัก</a>
                        </div>
                        
                        <div class="col-w4 col-amenities">
                            <div class="amenities">
                                <div class="shad-over">
                                    <h2 class="title">สิ่งอำนวยความสะดวกในห้องพัก</h2>
                                    <ul class="amenities-list">
                                        <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                        <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                        <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                        <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                        <li>ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                        <li class="more clickable">
                                            <a href="#all_amenities"><i class="fa fa-plus-circle"></i> อื่นๆ</a>
                                        </li>
                                    </ul>

                                    <div id="all_amenities" class="mfp-hide popup">
                                        <h2>สิ่งอำนวยความสะดวกทั้งหมด</h2>
                                        <ul class="list-columns-2">
                                            <li>เครื่องปรับอากาศ ที่สามารถปรับอุณหภูมิได้ตามต้องการ</li>
                                            <li>สิ่งอำนวยความสะดวกภายในห้องน้ำ : เจลสำหรับอาบน้ำและสระผม, หมวกอาบน้ำ, ผ้าเช็ดตัว, สำลีก้าน (คอตตอนบัด)</li>
                                            <li>อุปกรณ์เย็บผ้า</li>
                                            <li>รองเท้าแตะ</li>
                                            <li>ร่ม</li>
                                            <li>ถุงชายหาด</li>
                                            <li>ชั้นวางกระเป๋าเดินทาง</li>
                                            <li>โทรศัพท์ทางไกลระหว่างประเทศ</li>
                                            <li>โทรทัศน์สี พร้อมด้วยช่องดาวเทียมและช่องข่าวต่างประเทศ</li>
                                            <li>ตู้เย็นและฟรีน้ำดื่ม 2 ขวดต่อวัน</li>
                                            <li>ห้องน้ำส่วนตัว พร้อมด้วยฝักบัวน้ำอุ่นและน้ำเย็น</li>
                                            <li>ปลั๊กเสียบเครื่องใช้ไฟฟ้าขนาด 220 โวลต์</li>
                                            <li>วิวสระน้ำจากระเบียง (ดีลักซ์วิง)</li>
                                            <li>อุปกรณ์สำหรับชงชา/กาแฟ</li>
                                            <li>เครื่องเป่าผม</li>
                                            <li>ตู้นิรภัยในห้องพัก</li>
                                            <li>ฟรีบริการอินเตอร์เน็ตไร้สาย (Wi-Fi)</li>
                                            <li>ห้องพักที่เชื่อมต่อกันได้</li>
                                        </ul>
                                    </div>
                                </div>
                                
                                <span class="shad-left"></span>
                                <span class="shad-right"></span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="booking table-cell">
                    <?php include('include/room-booking-form.php'); ?>
                </div>
            </div>
            
            <span id="hide_content" class="close clickable">&times; ซ่อนเนื้อหา</span>
        </div>
        
    </div>
</main>

<?php include_once('_footer.php'); ?>