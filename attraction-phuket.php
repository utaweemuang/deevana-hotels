<?php
$title = 'Phuket Attractions | Deevana Hotels & Resorts';
$desc = 'Phuket is Thailand largest island and one of the most popular tourist destinations in Southeast Asia. This island is rich in natural resources and has the perfect weather for agriculture while providing colorful tropical vistas.';
$keyw = 'phuket hotel,phuket resort,phuket resorts patong, patong beach hotel resort phuket,phuket resort near airport, phuket resort spa hotel,phuket town hotel,krabi hotel,krabi resort,Krabi resort spa hotel, aonang hotel Krabi';

$html_class = '';
$body_class = 'attraction phuket';
$cur_page = 'attraction-phuket';

$lang_en = '/attraction-phuket.php';
$lang_th = '/th/attraction-phuket.php';
$lang_zh = '/zh/attraction-phuket.php';

include_once('_header.php');
?>
        
<div id="contact_slider" class="slider hero-slider owl-carousel">
    <div class="item"><img src="images/attraction/phuket/hero_slide_01.jpg" /></div>
</div>

<?php include('include/booking-bar.php'); ?>

<main class="site-main">
    <div class="inner">
	
		<section class="main-content">
			<div class="container">

				<h1 class="section-title underline">Phuket Attractions</h1>
				
				<div class="row row-content">
					<div class="col-w3 col-navigation">
						<div class="post-tabs tabs-group">
							<ul>
								<li class="tab active" data-tab="#big_buddha_phuket">Big Buddha Phuket</li>
								<li class="tab" data-tab="#old_phuket_town">Old Phuket Town</li>
								<li class="tab" data-tab="#sino-portuguese_houses">Sino-Portuguese Houses</li>
								<li class="tab" data-tab="#phang_nga_road">Phang Nga Road</li>
								<li class="tab" data-tab="#thalang_road_and_soi_rommanee">Thalang Road and Soi Rommanee</li>
								<li class="tab" data-tab="#dibuk_road">Dibuk Road</li>
								<li class="tab" data-tab="#krabi_road">Krabi Road</li>
								<li class="tab" data-tab="#ranong_road">Ranong Road</li>
								<li class="tab" data-tab="#promthep_cape">Promthep Cape</li>
								<li class="tab" data-tab="#patong_beach">Patong Beach</li>
								<li class="tab" data-tab="#bangla_road">Bangla Road</li>
							</ul>
						</div>
					</div>
					
					<div class="col-w9 col-content tabs-content">
						<article class="article" id="big_buddha_phuket" data-tab-name="Big Buddha Phuket">
							<img class="thumbnail force block" src="images/attraction/phuket/big_buddha_phuket.png" />
							<h1>Big Buddha</h1>
							<p>Phuket's Big Buddha is one of the island's most important and revered landmarks. The huge image sits on top of the Nakkerd Hills between Chalong and Kata and at 45 metres high it is easily seen from far away.</p>
							<p>The lofty site offers the best 360-degree views of the island (think sweeping vistas of Phuket Town, Kata, Karon beaches, Chalong Bay and more.) Easily reachable via a six-kilometre road leading from Phuket's main artery, it's a must-visit island destination.</p>
							<p>Close up to the image itself it is very peaceful and the only noises you will hear are the tinkling of small bells and the yellow Buddhist flags in the compound flapping in the wind plus soft background dharma music.</p>
							<p>Known among Thais as the Phra Puttamingmongkol Akenakkiri Buddha in full, it is 25 meters across at the base. The whole body is layered with beautiful white Burmese marble that shines in the sun, making it a natural symbol of hope. The views, and the actual image itself are all breathtaking.</p>
						</article>
						
						<article class="article" id="old_phuket_town" data-tab-name="Old Phuket Town">
							<img class="thumbnail force block" src="images/attraction/phuket/old_phuket_town.png" />
							<h1>Old Phuket Town</h1>
							<p>Unlike many Thai provincial capitals, Phuket Town fairly shines with personality and nowhere more so than its Old Town. In this historically rich part of town you will find shrines, temples (Buddhist and Chinese), ornate and beautifully preserved 'shophouses', quaint cafés, tiny printing shops, impromptu private and public museums and even a mini ex-red light district.</p>
							<p>Phuket Old Town was built on riches reaped from Phuket's tin boom of last century, when the metal was an extremely valuable commodity. In this quarter of the town you will see grandiose Sino-colonial mansions, once occupied by Phuket's tin barons of 100 years' ago. Phuket Old Town is compact enough to stroll around in. The best time to do this is early in the morning or after the day has lost its heat. There are enough restaurants and cafés to provide you with refreshments so don't bother taking a picnic along!</p>
						</article>
						
						<article class="article" id="sino-portuguese_houses" data-tab-name="Sino-Portuguese Houses">
							<img class="thumbnail force block" src="images/attraction/phuket/sino-portuguese_house.png" />
							<h1>Sino-Portuguese Houses</h1>
							<p>A must-do in Phuket is a walk in the old part of Phuket City, around Thalang, Dibuk and Krabi roads. The beautiful architecture along these roads will take you back to the charm of a century ago.</p>
							<p>Phuket plays host to a wonderful mix of nationalities who have chosen to live here: it has Thais, Chinese, Malays, Indians and Nepalese, a young and growing Eurasian community and a unique mix of Hokkien Chinese and Thais called 'Baba'.</p>
							<p>The Baba community's heritage can be seen in Phuket's Old Town in its architecture, commerce, dress and way of life. The core of the Old Town essentially is made up of five roads and several 'sois' (small streets), these are Rasada Rd, Phang Nga Rd, Thalang Rd, Dibuk Rd, and Krabi Rd. This quarter teems with history and after years of neglect is currently being renovated.</p>
							<p>One hundred years ago, Thalang Rd was a hive of activity as tin mine workers would head there to buy essentials, sell tin ore and indulge themselves in less-than-exemplary activities. Theirs was a hard life and the comforts of alcohol, opium, women, and the chance to win some extra cash through gambling provided a heady contrast to the drudgery of tin prospecting.</p>
						</article>
						
						<article class="article" id="phang_nga_road" data-tab-name="Phang Nga Road">
							<img class="thumbnail force block" src="images/attraction/phuket/phang_nga_road.png" />
							<h1>Phang Nga Road</h1>
							<p>Turn right here down Yaowarat Rd and right again into Phang Nga Rd. On your left you will see the South Wind secondhand bookstore and just after that an alleyway with Chinese characters at the entrance.</p>
							<p>This leads to the Shrine of the Serene Light. Built in 1889, the garden is indeed a serene place to rest your legs before exploring the colourful interior. On the left of the garden is a large polished marble plaque with the names of the donors who helped set the temple up - along with how much they donated. On exiting the alley, diagonally opposite, you will see a good example of imaginative renovation in the form of Siam Indigo, a restaurant that has taken typical Chinese shophouses and joined them together to create a spacious eatery which artfully mixes the old with the new.</p>
							<p>Less artful but perhaps more atmospheric is the Memory at On On Hotel, a few metres down on the left. The On On is a Phuket institution and played the part of a Bangkok flophouse in the movie, 'The Beach'. It has now been renovated but in the past, despite spartan rooms, questionable toilet facilities and the grumpiest staff on the island, people kept coming back for more. Built in Sino-Colonial hotel style, it features a dramatic entrance archway and the sort of fan-wafted lobby that typifies Hollywood's idea of the Far East.</p>
						</article>
						
						<article class="article" id="thalang_road_and_soi_rommanee" data-tab-name="Thalang Road and Soi Rommanee">
							<img class="thumbnail force block" src="images/attraction/phuket/thalang_road_and_soi_rommanee.png" />
							<h1>Thalang Road and Soi Rommanee</h1>
							<p>Turn left at the end of this block into Phuket Rd and pass what looks like a rundown tenement on the opposite side - this is, in fact, a hotel of questionable repute - then turn left into Thalang Rd and head west. You are now smack-bang in the heart of Phuket's Old Town on a street teeming with history and atmosphere. Here, a system of archways begins. These are dubbed 'five-footways' and most are linked, affording an easy stroll along the road out of the sun and the rain but some are blocked and still others are cluttered with shop merchandise. Still, the mix of colours and uniform design along with the eclectic blend of commerce makes for an impressive combination.</p>
							<p>At this end of the street there are two roti shops, a Chinese clinic, along with a shop devoted entirely to the sale of white shirts, a bicycle shop and the usual sprinkle of textile outlets. Further up on the right is Soi Rommanee. This back lane has an interesting past - it used to be the red light, or 'pleasure' district where Chinese labourers would go to let off steam. In fact, the word 'romanee' translates roughly as 'naughty with the ladies'. Nowadays the soi is an example of what the area has potential for as the houses and cafes are colour coded and the street is full of character (a reconditioned 1960's Ford Consul adds a welcome touch).</p>
						</article>
						
						<article class="article" id="dibuk_road" data-tab-name="Dibuk Road">
							<img class="thumbnail force block" src="images/attraction/phuket/dibuk_road.png" />
							<h1>Dibuk Road</h1>
							<p>Take a right turn out of Thalang Rd and cross over to turn left into Dibuk Rd. This road features some dazzling examples of well-renovated Chinese-style houses and has a wider throughway which handles two-way traffic, unlike the narrow one-way Old Town system. The relative wideness of the road allows for better photographic opportunities. At the end of Dibuk you will come to a T-junction with Satun Rd. Diagonally opposite you is Pheteow noodle shop. This place is crowded every week lunchtime with the many office workers who have discovered just how well Pheteow prepares its dishes.</p>
						</article>
						
						<article class="article" id="krabi_road" data-tab-name="Krabi Road">
							<img class="thumbnail force block" src="images/attraction/phuket/krabi_road.png" />
							<h1>Krabi Road</h1>
							<p>Backtrack to Yaowarat Rd. and head back towards Thalang Rd. At the junction there's a Chinese open-air garden-like eatery selling slow boiled sweet pork. A very popular spot, the 'moo hong' is a brisk seller, even among the sweet vendors along the next soi, Soon Utis Lane.</p>
							<p>Even though there are absolutely no customers in sight, the sweet vendors are laughing and joking amongst themselves - school will be out in ten minutes' time and they'll be busy enough then. Carry on and turn right into Krabi Rd.</p>
						</article>
						
						<article class="article" id="phuket_market" data-tab-name="Phuket Market">
							<img class="thumbnail force block" src="images/attraction/phuket/phuket_market.png" />
							<h1>Phuket Market</h1>
							<p>Backtrack to Yaowarat Rd. and head back towards Thalang Rd. At the junction there's a Chinese open-air garden-like eatery selling slow boiled sweet pork. A very popular spot, the 'moo hong' is a brisk seller, even among the sweet vendors along the next soi, Soon Utis Lane.</p>
							<p>Even though there are absolutely no customers in sight, the sweet vendors are laughing and joking amongst themselves - school will be out in ten minutes' time and they'll be busy enough then. Carry on and turn right into Krabi Rd.</p>
							<p>Fifty metres in on the right there's an ancient-looking secondhand bookstore and after that the Thai Hua Museum. Once a Chinese language school, nowadays it is used as a museum and an exhibition space. This beautiful building is set back from the road in its own garden. A few shophouses down, an entire shop front is obliterated under several tons of green coconuts.</p>
								<p>Three doors later and you can buy as much watermelon as you want and after that comes a mixed fruit shop. Next to that is the Old Town Guest House - this must be the most charming location to base from when exploring the area.</p>
						</article>
						
						<article class="article" id="ranong_road" data-tab-name="Ranong Road">
							<img class="thumbnail force block" src="images/attraction/phuket/ranong_road.png?ver=20160517" />
							<h1>Ranong Road</h1>
							<p>Take a right turn at the end of this soi at Ranong Rd and walk up to a Y-Junction. Here, you will find a colourful Chinese shrine called Jui Tui. This shrine is dedicated to the vegetarian Chinese-Taoist God, Kui Wong In and is the centre of activities during Phuket's annual Vegetarian Festival.</p>
							<p>This is also where people use bamboo blocks to obtain advice from the shrine's oracle. Ask a 'yes or no' question then throw the blocks gently in the air. If both blocks land on the same side the answer is 'no'. If one lands up and the other down the answer is 'yes'. A small donation to the shrine is appreciated.</p>
							<p>Next to the Jui Tui shrine is Pud Jow ('God Talks') Chinese Taoist Temple. Built 200 years ago and renovated after a fire 100 years ago, it is the oldest of its sort in Phuket.</p>
							<p>Now, simply turn back where you came from and walk past the fountain at the end of Ranong Rd and you will find yourself back on Rasada Rd. Walk to the end and you will be where you started your tour of Phuket's Old Town.</p>
						</article>

						<article class="article" id="promthep_cape" data-tab-name="Promthep Cape">
							<img class="thumbnail force block" src="images/attraction/phuket/Promthep_Cape.png" />
							<h1>Promthep Cape</h1>
							<p>Promthep Cape is one of the island's most photographed and perhaps best-known locations. Every evening, large tour buses, scooters and private cars sweep through Rawai Beach and up the island's southernmost hill in order to watch the sunset at its peak. On the top of the hill stands a busy car park where vehicles disgorge crowds of people from every corner of the world. Cameras flash, fingers point and lovers cuddle as Phuket's most fabulous free show is re-enacted nightly – the sunset.</p>
						</article>

						<article class="article" id="patong_beach" data-tab-name="Patong Beach">
							<img class="thumbnail force block" src="images/attraction/phuket/patong_beach.png" />
							<h1>Patong Beach</h1>
							<p>Patong is the most famous beach on Phuket resort is just minutes away from the famous Bangla Road’s nightlife, Jungceylon shopping mall and of course the wonderful sand of the glorious Patong Beach.</p>
						</article>

						<article class="article" id="bangla_road" data-tab-name="Bangla Road">
							<img class="thumbnail force block" src="images/attraction/phuket/Bang_La_Road.png" />
							<h1>Bangla Road</h1>
							<p>Bangla Road really comes to life once the sun sets. The road is closed to vehicle traffic and becomes a 400 metre festival of neon lights, loud music and cheap beer. Jammed most nights of the year, it is quite a friendly and lively place to walk around as bars and clubs compete with each other for customers. If you’re looking for a fun night out in Phuket, Bangla Road should be your first (and, often, only) stop. </p>
						</article>
					</div>
				</div>
				
			</div>
		</section><!--.main-content-->
		
    </div>
</main>

<script>
    $(function() {
        var $ts = $('.tabs-group');
        var $cs = $('.tabs-content');
        var at = $ts.find('.tab.active');
        var atd = at.data('tab')
        var ati = at.index();
        
        var hash = window.location.hash;
        if( hash && $(hash).length ) {
            $ts.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
            $cs.find(hash).show();
            
            var targetPos = $('.site-main').offset().top;
			var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        } else {
            $cs.find(atd).show();
        }

        $ts.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $cs.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $cs.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
            
            window.location.hash = t;
            var targetPos = $('.site-main').offset().top;
            var offset = ($(window).width() > 960) ? 144 : 0 ;
            scrollTo( 0, Math.round(targetPos) - offset );
        });

        $cs.find('article').each(function() {
            var $this = $(this);
            var tabID = $this.attr('id');
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $ts.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(ati).addClass('active');
    });
</script>

<style>
    .tabs-content .article { display: none; }
    .article .thumbnail { margin-bottom: 20px; }
    .post-tabs ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }
    .post-tabs li {
        border-top: 1px solid #ddd;
        color: #888;
        position: relative;
        display: block;
        padding: 5px 10px 5px 0;
        cursor: pointer;
    }
    .post-tabs li:after {
        content: '\f105';
        font-family: 'FontAwesome';
        position: absolute;
        top: 50%;
        font-size: 14px;
        line-height: 1;
        margin-top: -7px;
        right: 0;
    }
    .post-tabs li:last-child {
        border-bottom: 1px solid #ddd;
    }
    .post-tabs li:hover,
    .post-tabs li.active {
        color: #9a7b12;
    }
    .accordion-tab {
        display: none;
    }
    @media (max-width: 768px) {
        .row-content [class*="col-"] {
            width: 100%;
        }
        .col-navigation {
            display: none;
        }
        .post-tabs li:after {
            content: none;
        }
        .accordion-tab {
            background-color: #eee;
            margin: 0 -15px;
            padding: 6px 12px;
            display: block;
            border-bottom: 1px solid #ccc;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .article {
            padding-top: 15px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>