<?php
$title = 'Facilities | Recenta Suite Phuket SuanLuang | Official Hotel Group Website Thailand';
$desc = 'Facilities: Enjoy best direct hotel rate; 3 star chic hotel in Phuket town near Suanluang park and city center.';
$keyw = 'facilities, recenta suite, phuket, suan luang, suan luang, 3 star hotel, phuket town, recenta hotel';

$html_class = '';
$body_class = 'facilities';
$cur_page = 'facilities';

$lang_en = '/recentasuitephuket/facilities.php';
$lang_th = '/th/recentasuitephuket/facilities.php';
$lang_zh = '/zh/recentasuitephuket/facilities.php';

include_once('_header.php');
?>

<main class="site-main">
    
    <section class="page-cover">
        <div id="contact_slider" class="owl-carousel hero-slider">
            <div class="item"><img src="images/facilities/slide-hero/facility-slide-01.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-02.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-03.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
			<div class="item"><img src="images/facilities/slide-hero/facility-slide-04.jpg" alt="Recenta Suite Phuket Suanluang, 3-star hotel" /></div>
        </div>
		
		<div class="custom-hero-slide-nav"></div>
    </section>
    
    <?php include('include/booking_bar.php'); ?>
        
    <section class="site-content pattern-fibers">
        <section class="section">
            <header class="section-header">
                <h1 class="section-title">FACILITIES &amp; SERVICES</h1>
            </header>
            
            <div class="tabs-group">
                <div class="tabs-nav">
                    <span data-tab="#swimming_pool" class="tab active">Swimming Pool</span>
                    <span data-tab="#fitness" class="tab">Fitness</span>
                    <span data-tab="#tour_desk" class="tab">Tour Desk</span>
                    <span data-tab="#playground" class="tab">Playground</span>
                    <span data-tab="#transportation" class="tab">Transportation</span>
                    <!-- <span data-tab="#orientala_wellness_spa" class="tab">Message &amp; Spa</span> -->
                    <span data-tab="#car_park" class="tab">Car Park</span>
                </div>
                
                <div class="tabs-content">
                    <article id="swimming_pool" class="article" data-tab-name="Swimming Pool">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/swimming-pool.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Swimming Pool</h1>
                                    
                                    <p>Families can spend time socializing in our adult and children size swimming pools, or poolside on sun loungers. While the kid’s splash in the water, you can lie back and enjoy a cool drink under the sunshine, while still keeping an eye on them.</p>
                                    <p><span style="color: #516819;">Service hours: between 07.00hrs – 19.00hrs.<br>
                                    Please Note: No lifeguard on duty</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="fitness" class="article" data-tab-name="Fitness">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/fitness.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Fitness</h1>
                                    <p>Our fitness room is located on the first floor overlooking the action in the pool and garden, For fitness fanatics can come along anytime as the fitness room is open daily from 07:00 – 21.00hrs.</p>
                                    <p><span style="color: #516819;">Children below 12 years old are not allowed.<br>
                                        Children between 12-16 years old must be accompanied by an adult.</span></p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="tour_desk" class="article" data-tab-name="Tour Desk">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/tour_desk.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Tour Desk</h1>
                                    <p>Explore Phuket and its surrounding waters with an exciting day trip to gorgeous destinations. And learn about Thai culture at Phuket Old Town or cabaret show and more. We can arrange everything for you including pick up from the hotel.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="playground" class="article" data-tab-name="Playground">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/playground.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Playground</h1>
                                    <p>Kids want to have fun and relaxation</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="transportation" class="article" data-tab-name="Transportation">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/transportation.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Transportation</h1>
                                    <p>Our Phuket airport transfer service is safe and can be trusted. Please contact guest service team to arrange taxi services and airport transfers and other place as we have reliable drivers on standby to take you to your destination safely.</p>
                                </div>
                            </div>
                        </div>
                    </article>
					
                    <article id="orientala_wellness_spa" class="article" data-tab-name="Message &amp; Spa: by Orientala Wellness Spa">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/orientala_wellness_spa.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Orientala Spa</h1>
                                    <p>Orientala Spa is a place for refreshing and relaxing your body and mind. We are proudly to recommend our service as Healthy Massage that will help you relieve stress and relax your life</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                    <article id="car_park" class="article" data-tab-name="Car Park">
                        <div class="container">
                            <div class="row row-content-tab">
                                <div class="col-w5 col-pic">
                                    <img class="force thumbnail" src="images/facilities/car_park.jpg" />
                                </div>
                                <div class="col-w7 col-cap">
                                    <h1 class="title">Car Park</h1>
                                    <p>Free private parking is possible on site easy to in and out.</p>
                                </div>
                            </div>
                        </div>
                    </article>
                    
                </div>
            </div>
        </section>
    </section>
        
</main>

<script>
    $(function() {
        var $tnav = $('.tabs-nav');
        var $tcon = $('.tabs-content');
        var $tact = $tnav.find('.tab.active');
        var data = $tact.data('tab');
        var indx = $tact.index();
        var hash = window.location.hash;
		
		function preferPos(pos) {
			scrollTo(0, pos);
		}
        
        if( hash && $(hash).length ) {
            $tcon.find(hash).show();
            $tnav.find('[data-tab="'+hash+'"]').addClass('active').siblings().removeClass('active');
			
			var pos = $('.booking-bar').offset().top;
			preferPos(pos);
        } else {
            $tcon.find(data).show();
        }

        $tnav.on('click', '[data-tab]', function(e) {
            e.preventDefault();
            var $this = $(this);
            var i = $this.index();
            var t = $this.data('tab');
            $this.addClass('active').siblings().removeClass('active');
            $tcon.find(t).fadeIn(300).siblings().not('.accordion-tab').hide();
            $tcon.find('.accordion-tab').eq(i).addClass('active').siblings().removeClass('active');
        });

        $tcon.find('article').each(function() {
            var $this = $(this);
            var tabName = $this.data('tab-name');
            $this.before('<span class="accordion-tab">'+tabName+'</span>');
            $this.prev('.accordion-tab').on('click', function() {
                var i = $(this).index('.accordion-tab');
                $(this).addClass('active').siblings().removeClass('active');
                $this.slideDown(300, function() {
                    var pos = $(this).offset().top;
                    var offset = 50;
                    $('html, body').animate({
                        scrollTop: pos - offset,
                    }, 800);
                }).siblings().not('.accordion-tab').slideUp(300);
                $tnav.find('[data-tab]').eq(i).addClass('active').siblings().removeClass('active');
            });
        });

        $('.accordion-tab').eq(indx ).addClass('active');
    });
</script>

<style>
    .section-header {
        text-align: center;
        padding: 50px 0;
        color: #707270;
    }
    .tabs-content .article,
    .tabs-content .accordion-tab {
        display: none;
    }
    .tabs-nav {
        text-align: center;
    }
    .tabs-nav .tab {
        display: inline-block;
        padding: 0 16px;
        background-color: #c3c3c3;
        line-height: 36px;
        border-radius: 4px 4px 0 0;
        color: #fff;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .tabs-nav .tab.active {
        color: #1a355e;
        background-color: #fff;
    }
    .tabs-content {
        background-color: #fff;
        padding-top: 60px;
        padding-bottom: 40px;
    }
    .tabs-content .container {
        max-width: 1200px;
    }
    .row-content-tab {
        margin-left: -15px;
        margin-right: -15px;
    }
    .row-content-tab > [class*="col-"] {
        padding-left: 15px;
        padding-right: 15px;
    }
    .col-pic .thumbnail {
        border-radius: 12px;
        border: 6px solid #fff;
        -webkit-box-shadow: 0 0 2px rgba(0,0,0,.3);
        box-shadow: 0 0 2px rgba(0,0,0,.3);
    }
    .col-cap .title {
        color: #707270;
    }
    @media (max-width: 840px) {
        .tabs-nav {
            display: none;
        }
        .tabs-content {
            padding: 0;
        }
        .tabs-content .accordion-tab {
            position: relative;
            display: block;
            background-color: #eee;
            padding: 5px 15px;
            border-bottom: 1px solid #ccc;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            cursor: pointer;
        }
        .tabs-content .accordion-tab:after {
            content: '\f055';
            font-family: 'FontAwesome';
            float: right;
        }
        .tabs-content .accordion-tab.active:after {
            content: '\f056';
        }
        .tabs-content .row {
            padding-top: 20px;
            padding-bottom: 20px;
        }
    }
    
    @media (max-width: 740px) {
        .col-pic,
        .col-cap {
            width: 100%;
        }
        .col-pic {
            margin-bottom: 20px;
        }
    }
</style>

<?php include_once('_footer.php'); ?>