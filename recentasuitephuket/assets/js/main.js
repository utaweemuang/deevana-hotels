(function($) {
    //FastClick.attach(document.body);

    $(function() {

        /* =Simple Toggle
        --------------------------------------------------------------------- */

        $.fn.simpleToggle = function(activeClass, target, outFocus) {
            var $this = $(this);
            var $target = $(target);
            var aClass = activeClass;

            $this.on('click', function() {
                if( $this.hasClass(aClass) ) {
                    $this.removeClass(aClass);
                    $target.removeClass(aClass);
                } else {
                    $this.addClass(aClass);
                    $target.addClass(aClass);
                }
            });

            if( outFocus === true ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass(aClass);
                    }
                });
            }
        }

        /* =Scroll Effect
        --------------------------------------------------------------------- */

        $.fn.scrollFX = function( options ) {
            var defaults = $.extend({
                toggleClass: 'scroll-fx',
                moreTarget: null,
                trigger: window,
                offset: 0,
                disableOnWidth: 0,
                debug: false,
            }, options);

            var $this = $(this);
            var tClass = defaults.toggleClass;
            var trg = defaults.trigger;
            var off = defaults.offset;
            var tar = defaults.moreTarget;

            if( $this.length > 0 ) {
                var type = $.type(trg);

                function getPosition() {
                    var val;
                    switch(type) {
                        case 'number': val = trg; break;
                        case 'string': val = $(trg).offset().top; break;
                        case 'object': val = $(trg).scrollTop(); break;
                    }
                    return val;
                }

                function runFX() {
                    var objPos = Math.floor( $this.offset().top );
                    var trgPos = Math.floor( getPosition() );
                    var winWi = Math.floor( $(window).width() );

                    if( type == 'number' ) {
                        if( winWi > defaults.disableOnWidth ) {
                            if( objPos >= (trgPos - off) ) {
                                $this.addClass(tClass);
                                $(tar).addClass(tClass);
                            } else {
                                $this.removeClass(tClass);
                                $(tar).removeClass(tClass);
                            }
                        } else {
                            $this.removeClass(tClass);
                            $(tar).removeClass(tClass);
                        }
                    }

                    if( type == 'object' ) {
                        if( winWi > defaults.disableOnWidth ) {
                            if(trgPos >= (objPos - off)) {
                                $this.addClass(tClass);
                                $(tar).addClass(tClass);
                            } else {
                                $this.removeClass(tClass);
                                $(tar).removeClass(tClass);
                            }
                        } else {
                            $this.removeClass(tClass);
                            $(tar).removeClass(tClass);
                        }
                    }

                    if( defaults.debug == true ) {
                        console.log( 'Trigger: '+trgPos+', Object: '+objPos+', with Offset: '+(objPos - off)+', Offset only: '+off);
                    }
                }

                $(window).on('load', function() {
                    runFX();
                });

                $(window).on('resize', function() {
                    runFX();
                });

                $(window).on('scroll',function() {
                    runFX();
                });

            };
        }

        /* =Swipe Menu
        ------------------------------------------------------------ */

        function swipeMenu( breakpoint ) {
            function openOffsideMenu() {
                $('html').addClass('open-offside-menu');1
            }

            function closeOffsideMenu() {
                $('html').removeClass('open-offside-menu');
            }

            $('#toggle_offside_menu').on('click', function() {
                openOffsideMenu();
            });

            $('.offside-menu-bg, #offside-menu .close-menu').on('click', function() {
                closeOffsideMenu();
            });

            var breakpoint = breakpoint || 0;

            $(window).on('resize', $.throttle( '200', function() {
                if( $(window).width() < breakpoint ) {
                    $(document).swipe('enable');
                    $(document).swipe({
                        swipeRight: function(event) {
                            var targ = event.target;
                            if( $(targ).closest('.owl-carousel, .disable-touch').length === 0 ) {
                                openOffsideMenu();
                            }
                        },
                        swipeLeft: function() {
                            closeOffsideMenu();
                        },
                    });
                } else {
                    $(document).swipe('disable');
                    closeOffsideMenu();
                }
            })).trigger('resize');
        }

        swipeMenu(860);

        /* =Room Scripts
        ---------------------------------------------------------------------- */

        if( $('body').hasClass('room') ) {
            var $slide = $('.room-slides');
            var $tnav = $('.room-slides-thumbs .thumbs');
            // Room Gallery Preview
            var $preview = $('.room-preview');
            $preview.css({
                cursor:'pointer'
            }).attr({
                'tabindex':'0'
            });
            $preview.after('<div style="display:none;" class="room-preview-gallery"/>');

            $slide.find('.slides-container img').each(function (params) {
                var href = $(this).attr('src'),
                    img = '<img src="'+href+'">',
                    $template = $('<a href="'+href+'">'+img+'</a>');
                    $template.appendTo('.room-preview-gallery');
            });

            $preview.on('click',function(e){
                e.preventDefault();
                $('.room-preview-gallery').magnificPopup({
                    type:'image',
                    delegate: 'a',
                    gallery: {
                        enabled: true,
                    }
                }).magnificPopup('open');
            })
            function slideFX( autoplay ) {  
                var items = $slide.find('img').length;
                var autoplay = ( items > 1 ) ? autoplay : 0;
                $slide.superslides({
                    inherit_width_from: '.room-slides-wrap',
                    inherit_height_from: '.room-slides-wrap',
                    play: autoplay,
                    pagination: false,
                    animation: 'fade',
                });

                $slide.on('animated.slides', function() {
                    var i = $slide.superslides('current');
                    $tnav.find('li').eq(i).addClass('current').siblings().removeClass('current');;
                });

                $tnav.on('click', 'li', function() {
                    var $thm = $(this);
                    var i = $thm.index();
                    if( ! $thm.hasClass('current') ) {
                        $thm.addClass('current').siblings().removeClass('current');
                        $slide.superslides('animate', i);
                    }
                });

                $slide.swipe({
                    swipeRight: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('prev');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                    swipeLeft: function(event) {
                        if( items > 1 ) {
                            var i = $slide.superslides('next');
                            $tnav.eq(i).addClass('current').siblings().removeClass('current');
                            $slide.superslides('animate', i);
                        }
                    },
                });
            }

            slideFX(8000);

            function hideRoomContent() {
                $('html').addClass('hide-content');
            }

            function showRoomContent() {
                $('html').removeClass('hide-content');
            }

            $('#hide_content').on('click', function() {
                hideRoomContent();
            });

            $('#toggle_content').on('click', function() {
                $('html').toggleClass('hide-content');
            });

            function roomContentPos( breakpoint, offset ) {
                var breakpoint = breakpoint || 0;
                var $rcon = $('.room-info');
                var rconHi = $rcon.outerHeight();
                var rconWi = $rcon.outerWidth();
                var offset = offset || 0;

                if( $(window).width() > breakpoint ) {
                    $rcon.css({
                        marginLeft: -rconWi/2,
                        marginTop: -(rconHi/2) + offset,
                    });
                } else {
                    $rcon.removeAttr('style');
                }
            }

            function roomScript( breakpoint ) {
                var breakpoint = breakpoint || 0;
                $(window).on('resize', $.throttle( 100, function() {
                    var winWi = $(window).width();
                    if( winWi > breakpoint ) {
                        $('html').addClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                    } else {
                        $('html').removeClass('disable-scroll');
                        roomContentPos( breakpoint, 60 );
                        showRoomContent();
                    }
                })).trigger('resize');
            }

            roomScript(960);

            // Room booking
            $('#room-booking-form').booking({
                checkInSelector: '#checkin',
                checkOutSelector: '#checkout',
                adultSelector: '#adults',
                childSelector: '#children',
                roomSelector: '#rooms',
                codeSelector: '#accesscode',
                submitSelector: '#submit',
                propertyId: '299',
                onlineId: 5,
            });

            // Amenities Popup
            $('.more').magnificPopup({
                delegate: 'a',
                type: 'inline',
                mainClass: 'mfp-fade',
                removalDelay: 300,
            });
        }

        /* =Validation
        ---------------------------------------------------------------------- */

        $.validate({
            form: '#contact_form',
            modules: 'location, html5',
            onModulesLoaded: function() {
                $('input[name="country"]').suggestCountry();
            },

            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                }).done(function(status) {
                   if (status === 'ok') {
                       //console.log('Sending Successful');

                       $(form).find(':focus').blur();
                       $(form)[0].reset();
                       grecaptcha.reset();

                       //Display form result
                       $(form).find('#contact_result').addClass('success');
                       $('#contact_result').html('<i class="icon fa fa-check-circle"></i> Thank you for getting in touch!');
                       $('#contact_result').delay(200).slideDown(200);
                   } else if( status === 'not' ) {
                       //console.log('Sending Failed because Google Recaptcha');

                       $(form).find('.g-recaptcha-error').text('Please check the recaptcha');
                   } else {
                       console.log('Unknowed error');
                       //$(form).addClass('form-error');
                       //$(form).find('#form_result').html('Sorry, There is something error, Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a>').slideDown(200);
                   }
                });

                return false;
            },
        });

        $.validate({
            form: '#newsletter-form',
            onSuccess: function(form) {
                var url = $(form).attr('action');
                var formData = $(form).serialize();
                $.post(url, formData, function( data ) {
                    if( data == '' ) {
                        $(form)[0].reset();
                        $(form).find(':focus').blur();
                        $(form).addClass('completed').after('<div class="subscribe-form-result" />');
                        $(form).next('.subscribe-form-result').html('<i class="icon fa fa-check-circle"></i> Subscription Successful');
                    } else {
                        $(form).after('<div class="form-error-log"><span style="color: tomato;">Subscription Failed.</span><br/>Please contact directly to <a href="mailto:info@deevanaplazakrabi.com">info@deevanaplazakrabi.com</a></div>');
                    }
                });
                return false;
            }
        });

        var $hero = $('.hero-slider');
        var $loop = ( $hero.find('.item').length > 1 ) ? true : false;

        $('.hero-slider').owlCarousel({
            items: 1,
            loop: $loop,
            smartSpeed: 800,
            pullDrag: false,
            dots: false,
            nav: true,
            navContainer: '.custom-hero-slide-nav',
            navText: ['<span class="sprite arrow-left"></span>', '<span class="sprite arrow-right"></span>'],
        });

        //Switch languages function
        var $langSelector = $('#language_select');
        var defaultLang = $('#language_select').find('.option.selected a').html();
        $langSelector.simpleToggle('visible', null, true);
        $langSelector.find('.active').html(defaultLang);

        $('.toggle-sub-menu').each(function() {
            var $this = $(this);

            $this.children('a').on('click', function(e) {
                e.preventDefault();
                if( $this.hasClass('visible') ) {
                    $this.removeClass('visible');
                } else {
                    $this.addClass('visible');
                }
            });

            //Run script in .site-navigation only
            if( $this.closest('.site-navigation').length === 1 ) {
                $(document).on('click', function(e) {
                    var target = e.target;
                    if( $(target).closest($this).length == 0 ) {
                        $this.removeClass('visible');
                    }
                });
            }
        });

        var $currentMenu = $('#offside-menu').find('.current');
        if( $currentMenu.closest('.ancestor').length > 0 ) {
            $currentMenu.parents('.ancestor').addClass('visible');
        }

        /**
         * New Scroll FX
         */
        var $header = $('.site-header');
        var $bookingBar = $('.booking-bar');
        $(window).on('scroll resize', $.throttle(16, function() {
            var winY = window.scrollY;
            var winW = window.innerWidth;

            if (winY >= 150 && winW >= 1140) {
                $header.addClass('mini');
            } else {
                $header.removeClass('mini');
            }

            var trigger = $bookingBar.offset().top - 60;
            if (winY >= trigger && winW >= 1140) {
                $bookingBar.addClass('fixed');
            } else {
                $bookingBar.removeClass('fixed');
            }
        }));

        /**
         * Booking
         */
        $('#booking-form').booking({
            checkInSelector: '#checkin',
            checkOutSelector: '#checkout',
            adultSelector: '#adults',
            childSelector: '#children',
            roomSelector: '#rooms',
            codeSelector: '#accesscode',
            propertyId: 299,
            onlineId: 5,
        });
    });

    $(window).on('load', function() {
        $('html').removeClass('preload');
    });

    $(window).on('resize', function() {
        $('html').addClass('resizing');
    });

    $(window).on('resizeend', function() {
        $('html').removeClass('resizing');
    });

})(jQuery);
